<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payment Process..</title>
    <link href="{{ asset('web/css/template.css') }}" rel="stylesheet" />
    <link href="{{ asset('web/css/payment.form.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="wrap_process">
        <h2 class="section-title">Preparing <span>Please wait</span></h2>
        <div class="loding-icon"><img src="{{asset('/images/web/icon/loading.gif')}}" width="200px"></div>
    </div>
    <div class="hidden">
        @if(!empty($init))
        {!! Form::open(['url' => $init['payment_url'],'method'=>'post','id'=>'form']) !!}
            {!! Form::hidden('version',$init['version']) !!}
            {!! Form::hidden('merchant_id',$init['merchant_id']) !!}
            {!! Form::hidden('currency',$init['currency']) !!}
            {!! Form::hidden('pay_category_id',$init['pay_category_id']) !!}
            {!! Form::hidden('result_url_1',$init['result_url_1']) !!}
            {!! Form::hidden('result_url_2',$init['result_url_2']) !!}
            {!! Form::hidden('payment_option',$init['payment_option']) !!}
            {!! Form::hidden('hash_value',$init['hash_value']) !!}
            {!! Form::hidden('payment_description',$init['payment_description']) !!}
            {!! Form::hidden('order_id',$init['order_id']) !!}
            {!! Form::hidden('amount',$init['amount']) !!}
        {!! Form::close() !!}
        <script type="text/javascript">
            document.getElementById("form").submit();
        </script>
        @endif
    </div>
</body>
</html>