@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="{{ asset('web/css/payment_fail.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/template.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')

<div class="wrap_container">
    <div class="container-fluid">
        <div class="wrap-bg">
            <div class="container">
                <div class="response-box">
                    <h2>@lang('fail.topic')</h2>
                    <div class="reason-box">
                        <div class="body">
                            <h4>{{$reason}}</h4>
                        </div>
                        <div class="foot">
                            @lang('fail.try_again') <a class="mat-btn btn" href="{!! url('/#section-banner') !!}">@lang('fail.click') </a><br/><br/>
                            @lang('fail.tel') 02 119 3088
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</div>


@endsection