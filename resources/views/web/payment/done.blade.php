@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="{{ asset('web/css/insurance.min.css?v=1.1') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance_thank.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')

<section id="section-insurance">
    <div class="container-fluid">
        <div class="row wrap-insurance-header">
            <div class="col-xs-12 col-title">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="sec-title">@lang('thank.thankyou')</h1>
                        <h2 class="sec-sub-title">@lang('thank.thankyou2')</h2>
                    </div>
                </div><!--/row-->
            </div>
            <div class="col-xs-12">
                <div class="container">
                    <div class="row block-insurance-header">
                        <div class="col-md-3">
                            <img class="item-main" src="{{ asset('/images/web/icon/Sompony_Step5.png') }}" />
                        </div>
                        <div class="col-md-9">
                            <div class="row row-box">
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step-5-1.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="text-color-red-light">{{ !empty($init['header']['plan_name'])?$init['header']['plan_name']:null }}</p>
                                        <p class="txt-detail-light">@lang('thank.sum_insured') {{ !empty($init['header']['ft_si'])?number_format($init['header']['ft_si']):null }} @lang('common.baht')</p>
                                        <p class="txt-detail-light">@lang('thank.insurance_start') {{ !empty($init['header']['start_insurance'])?$init['header']['start_insurance']:null }}</p>
                                        <p class="txt-detail-light">หมดอายุวันที่ {{ !empty($init['header']['end_insurance'])?$init['header']['end_insurance']:null }}</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-2.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="txt-detail-light">@lang('thank.car')</p>
                                        <p class="text-color-red-light">{{ !empty($init['header']['full_car'])?strtoupper($init['header']['full_car']):null }}</p>
                                        <p class="txt-detail-light">
                                            @if($init['header']['motor_code']=="320")
                                            รถคันนี้ใช้สำหรับเพื่อการพาณิชย์ที่ไม่ใช่รถรับจ้างหรือให้เช่า
                                            @else
                                            รถคันนี้ใช้สำหรับบุคคลเท่านั้น
                                            @endif
                                        </p>
                                    
                                    </div>
                                </div>
                            </div><!--/row-->
                            <div class="row row-box">
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-3.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="text-color-red-light">@lang('thank.addition_coverage')</p>
                                        @if($init['header']['addon']!=null)
                                            @if(!empty($init['header']['addon']['compulsory']))
                                            <p class="txt-detail-light">+ @lang('thank.compulsory_start') {{ $init['header']['addon']['compulsory'] }}</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_flood']) && $init['header']['addon']['is_flood']==1)
                                            <p class="txt-detail-light">+ @lang('thank.flood_coverage')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_theft']) && $init['header']['addon']['is_theft']==1)
                                            <p class="txt-detail-light">+ @lang('thank.robbery_coverage')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_taxi']) && $init['header']['addon']['is_taxi']==1)
                                            <p class="txt-detail-light">+ @lang('thank.taxi_coverage')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_hb']) && $init['header']['addon']['is_hb']==1)
                                            <p class="txt-detail-light">+ @lang('step5.hb_coverage')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_carloss']) && $init['header']['addon']['is_carloss']==1)
                                            <p class="txt-detail-light">+ @lang('step5.carloss_coverage')</p>
                                            @endif
                                        @else
                                            <p class="txt-detail-light">-</p>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-4.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="txt-detail-light">@lang('thank.payment_amount')</p>
                                        <p class="text-color-red-light">{{ number_format($init['header']['payment_result'],2) }} @lang('common.baht')</p>
                                    </div>
                                </div>
                            </div><!--/row-->
                        </div>
                    </div><!--/row block-insurance-header-->
                </div><!--/container-->
            </div>
        </div>
    </div> <!-- /container-fluid -->
    

    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="item-title">
                    <img src="{{ asset('/images/web/icon/thank-icon.png') }}" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="wrap-detail text-center">
                    <p>@lang('thank.detail1')</p>
                    @if($init['header']['is_compulsory'])
                        <p>@lang('thank.detail2')</p>
                    @endif
                </div>
                <div class="wrap-sub-footer text-center">
                    <p><b>*</b> กรุณาตรวจสอบกรมธรรม์ตาม e-mail และ SMS ที่ท่านให้ไว้</p>
                </div>
                <!-- <div class="wrap-sub-footer text-center">
                    <p><b>*</b> @lang('thank.detail6')</p>
                </div> -->
                
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row wrap-summary">
            <div class="col-xs-12 block-summary">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title">@lang('thank.summary_gross')</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <img src="{{ asset('/images/web/icon/step5-5.png') }}" />
                    </div>
                    <div class="col-sm-9 col-xs-12">
                        <div>
                            <ul>
                                <li>
                                    <div class="txt-box txt-box-left"><p>@lang('thank.gross') {{strtoupper($init['header']['plan_name'])}}</p></div>
                                    <div class="txt-box txt-box-right "><p class="txt-price">{{ number_format($init['summary']['gross_premium'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p>@lang('thank.compulsory')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price">{{ number_format($init['summary']['compulsory'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p>@lang('thank.addition_coverage')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price">{{ number_format($init['summary']['flood']+$init['summary']['theft_gross_premium']+$init['summary']['taxi_gross_premium']+$init['summary']['hb_gross_premium']+$init['summary']['carloss_gross_premium'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p class="text-color-red-dark">@lang('thank.promotion_discount')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price text-color-red-dark">{{ number_format($init['summary']['discount'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p class="txt-detail">@lang('thank.payment_amount')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price txt-detail">{{ number_format($init['summary']['payment_result'],2) }} @lang('common.baht')</p></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="wrap-detail-footer text-center">
                    <p>@lang('thank.detail3')</p>
                    <p>@lang('thank.detail4')</p>
                    <p>@lang('thank.detail5')</p>
                </div>
            </div>
        </div>
    </div>


    

    

</section>

@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.rule.js') }}"></script>
<script src="{{ asset('component/moment/moment.js') }}"></script>
<script src="{{ asset('component/bootstrap-dialog/bootstrap-dialog.js') }}"></script>
<script src="{{ asset('component/material-datepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>
<script src="{{ asset('web/js/insurance.min.js') }}"></script>
<script src="{{ asset('web/js/insurance_5.min.js') }}"></script>
@endsection

@section('hidden')

@endsection