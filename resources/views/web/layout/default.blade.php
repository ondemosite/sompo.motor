<!DOCTYPE html>
<html>
<head>
<title>@yield('title','บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด มหาชน | Sompo Thailand')</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Cache-control" content="public">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta property="og:url"  content="{{ Request::url() }}" />
<meta property="og:type" content="@yield('og-type','website')" />
<meta property="og:title"  content="@yield('og-title','บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด มหาชน | Sompo Thailand')" />
<meta property="og:description" content="@yield('og-description','บริษัทประกันภัยอันดับ 1 จากญี่ปุ่น ผู้นำด้านประกันภัยธุรกิจและประกันรายบุคคล | ประกันภัยรถยนต์ ประกันสุขภาพและอุบัติเหตุ ประกันภัยเดินทาง ประกันภัยบ้านและอื่นๆ')" />
<meta property="og:image" content="@yield('og-image',getMaskupImage())" />
<meta property="fb:app_id" content="262880424175902" />
@yield('header')
<link rel="shortcut icon" href="{{ asset('images/web/icon/fav.ico') }}" type="text/css" >
<link href="{{ asset('component/bootstrap3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('component/font-awesome/css/font-awesome-4.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('component/animate/animate.min.css') }}" rel="stylesheet" type="text/css" type="text/css" >
<link href="{{ asset('component/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('component/select2/dist/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('web/css/myanimate.min.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('web/css/template.min.css') }}" rel="stylesheet" type="text/css" >
@yield('css')
<!-- Google Tag Manager !-->

<script>
(function (w, d, s, l, i) {
w[l] = w[l] || [];
w[l].push({'gtm.start':new Date().getTime(), event: 'gtm.js'});
var f = d.getElementsByTagName(s)[0],j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
j.async = true;
j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-PZWXM82');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<div class="web-container">
    <!-- Menu Bar -->
    <section id="section_menu">
        @yield('menu')
    </section>
    
    

    <!-- content section -->
    @yield('content')
    <!-- / content section -->

    <!-- Section footer -->
    <div class="container-fluid footer-container">
        <div class="web-footer">
            <div class="container">
                <div class="row web-footer-row">
                    <div class="col-md-4 paragraph">
                        <p>
                            @lang('footer.company')<br>
                            @lang('footer.company2')<br>
                            @lang('footer.company3')					
                        </p>
                        <div class="right-line"></div>
                    </div>
                    <div class="col-md-3 paragraph">
						<p>@lang('footer.customer')</p>
						<a class="tel" href="tel:021193000">02-119-3000</a>
                        <p>@lang('footer.every_day')</p>
                        <p>E-mail: CS@sompo.co.th</p>
                        <div class="right-line"></div>
                    </div>
                    <div class="col-md-3 paragraph">
                        <p>@lang('footer.call_me')</p>
                        <a class="tel" href="tel:021187400">02-118-7400</a>
                        <p>@lang('footer.every_hour')</p>
                        <div class="right-line"></div>
                    </div>
                </div>
                <div class="row web-footer-row">
                    <div class="col-xs-12 paragraph">
                        <span class="social-icon"><a href="https://www.facebook.com/SompoThailand/" title="SOMPO Facebook Page" target="_blank"><img class="svg" src="{{ asset('/images/web/logo/facebook.svg') }}" width="30px"/></a></span>
                        <span class="social-icon"><a href="http://line.me/ti/p/@sompothailand" title="SOMPO LINE@" target="_blank"><img class="svg" src="{{ asset('/images/web/logo/line.svg') }}" width="30px"/></a></span>
                    </div>
                </div>
                <div class="row web-footer-row footer-copy-right">
                    <div class="col-xs-12 copy-right-paragraph">
                        <div>
                            <p>© 2019 SOMPO INSURANCE (THAILAND) PUBLIC COMPANY LIMITED. ALL RIGHTS RESERVED.</p>
                        </div>
                    </div>
                </div>
                <div class="backtotop">
                    <div class="fb-customerchat"
                        page_id="1815429938705433"
                        theme_color="#ff0838"
                        logged_in_greeting="สวัสดี"
                        logged_out_greeting="ลาก่อน">
                    </div>
                    <a href="javascript:"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></a>
                </div>
                <!--<button class="test" id="test_button">Test</button>-->
            </div>
        </div>
    </div>
    
</div><!-- / Web containner -->
@yield('hidden')
{!! Form::hidden('hd_current_lang',App::getLocale()) !!}

<!-- Facebook App -->
<script>
window.fbAsyncInit = function() {
    FB.init({
        appId            : '262880424175902',
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v2.12'
    });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</body>

<script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/bootstrap3/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/bootstrap-notify/bootstrap-notify.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/dragable/dragable/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/select2/dist/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/waypoint/jquery.waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/template.min.js') }}"></script>
@if ($errors->any())
<script>
var message = "";
@foreach ($errors->all() as $error)
message += "{{ $error }}<br/>";
@endforeach
notify(message,"danger");
</script>
@endif
@yield('javascript')
</html>
