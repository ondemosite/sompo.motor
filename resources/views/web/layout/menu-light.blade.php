<nav class="navbar web-navbar web-navbar-light navbar-fixed-top">
    <div class="nav-container">
        <div class="navbar-header">
            <button id="collapsed_btn" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('web.dashboard') }}"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            
            <div class="nav-row row navbar-language">
                <ul class="nav navbar-nav navbar-right ">
                    <li>
                        @if(App::getLocale()=='en')
                        <a class="active" href="{!! url('language_switch/en') !!}">EN</a> | <a href="{!! url('language_switch/th') !!}">TH</a>
                        @else
                        <a href="{!! url('language_switch/en') !!}">EN</a> | <a href="{!! url('language_switch/th') !!}" class="active">TH</a>
                        @endif
                    </li>
                </ul>
            </div>
            <div class="nav-row row navbar-menu">
                <ul class="nav navbar-nav navbar-right ">
                    <li class="active no-bucket"><a href="/">@lang('menu.home')</a></li>
                    <li><a href="{{ route('web.dashboard').'#section-product' }}">@lang('menu.product')</a></li>
                    <li><a href="{{ route('web.garage') }}">@lang('menu.garage')</a></li>
                    <li><a href="{{ route('web.dashboard').'#section_faq' }}">@lang('menu.faq')</a></li>
                    <li><a href="{{ route('web.dashboard').'#section_footer' }}">@lang('menu.contactus')</a></li>
                    <li class="nav-btn"><a class="mat-btn btn" href="{{ route('web.dashboard').'#section-banner' }}">@lang('menu.calculate')</a></li>
                    <li class="social-icon no-bucket">
                        <a class="svg-facebook" href="https://www.facebook.com/SompoThailand/" title="SOMPO Facebook Page" target="_blank"><img class="svg" src="{{ asset('/images/web/logo/facebook.svg') }}"/></a>
                        <a class="svg-line" href="http://line.me/ti/p/@sompothailand" title="SOMPO LINE@" target="_blank"><img class="svg" src="{{ asset('/images/web/logo/line.svg') }}"/></a>
                    </li>
                    <li class="li-mobile-foot">
                        <div class="mobile-foot">
                            <div class="lang">
                                @if(App::getLocale()=='en')
                                <a class="active" href="{!! url('language_switch/en') !!}">EN</a>
                                <a href="{!! url('language_switch/th') !!}">TH</a>
                                @else
                                <a href="{!! url('language_switch/en') !!}">EN</a>
                                <a href="{!! url('language_switch/th') !!}" class="active">TH</a>
                                @endif
                            </div>
                            <div class="social">
                                <a href="https://www.facebook.com/SompoThailand/" title="SOMPO Facebook Page" target="_blank"><img class="svg" src="{{ asset('/images/web/logo/facebook.svg') }}"/></a>
                                <a href="http://line.me/ti/p/@sompothailand" title="SOMPO LINE@" target="_blank"><img class="svg" src="{{ asset('/images/web/logo/line.svg') }}"/></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div> <!-- /#navbar -->
        <div class="border-bottom"></div>
    </div>
</nav>