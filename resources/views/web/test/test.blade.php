<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/component/bootstrap3/css/bootstrap.min.css">
</head>
<body>

    
<header id="menu" class="body">
    <h1><a href="#">Smashing HTML5! <strong>HTML5 in the year <del>2022</del> <ins>2009</ins></strong></a></h1>
    <nav><ul>
    <li class="active"><a href="#">home</a></li>
    <li><a href="#">portfolio</a></li>
    <li><a href="#">blog</a></li>
    <li><a href="#">contact</a></li>
    </ul></nav>
</header>
<!-- /Header -->

<main id="content">
    <div class="container-fluid">
        <article>
            <section></section>
            <section></section>
            <section></section>
        </article>
    </div>
</main>
<!-- /Main -->

<footer>
    <address></address>
</footer>
<!-- Footer -->
    
</body>
</html>