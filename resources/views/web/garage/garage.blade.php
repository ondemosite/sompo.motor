@extends('web.layout.default')
@section('title') Sompo @endsection
@section('og-title') ค้นหาศูนย์ซ่อมรถยนต์ บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด มหาชน @endsection
@section('og-description') บริการค้นหาศูนย์ซ่อมรถยนต์ทั่วประเทศไทย ทั้งศูนย์มาตราฐานและศูนย์ในเครือ บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด มหาชน @endsection
@section('og-image'){{ getMaskupImage('garage') }}@endsection
@section('css')
<link href="{{ asset('web/css/garage.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="section_garage">
    <div class="container-fluid">
        <div class="row">
            <div class="wrap-garage col-xs-12">
                <div class="container">
                    <div class="row">
                        <h1 class="section-title text-center"><i class="fa fa-search" aria-hidden="true"></i> <span>@lang('garage.find')</span></h1>         
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="garage-box">
                                <div class="tab-content ">
                                    <div class="tab-pane active" id="tab-place">
                                    {!! Form::open(['url' => route('web.find_garage'),'name'=>'form_garage','class'=>'form_garage','id'=>'form_garage']) !!}
                                    {{ Form::hidden('selected_province',!empty($init['selected_province'])?$init['selected_province']:null) }} 
                                    {{ Form::hidden('selected_district',!empty($init['selected_district'])?$init['selected_district']:null) }} 
                                    {{ Form::hidden('selected_name',!empty($init['selected_name'])?$init['selected_name']:null) }} 
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-12 col-input">
                                            <div class="form-group">
                                                {{ Form::select('input_garage_prov',!empty($init['province'])?$init['province']:null,null,['class' => 'form-control select2','data-rule-required'=>'true',
                                                'placeholder' => trans('placeholder.province')]) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-xs-12 p-l-0 col-input">
                                            <div class="form-group">
                                                {{ Form::select('input_garage_district',[],null, ['class' => 'form-control select2','data-rule-required'=>'true',
                                                'placeholder' => trans('placeholder.district')]) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12 p-l-0 col-input">
                                            <div class="form-group">
                                                {{ Form::text('input_name',"", ['class' => 'form-control input-text','data-rule-required'=>'true',
                                                'placeholder' => trans('placeholder.type_name'),'autocomplete'=>'off']) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 p-l-0 col-input">
                                            <div class="form-group">
                                                <button type="button" class="mat-btn btn" id="butmit_search">@lang('common.find')</button>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </div>
                                </div>
                                <div class="result-box">
                                    <ul></ul>
                                </div>
                            </div><!-- /garage-box -->
                        </div>
                    </div>
                </div> <!-- /container -->
            </div> <!-- /wrap-garage -->
        </div>
        <div class="row row-result">
            <div id="map" class="garage-map pull-right">
            </div>
            <div class="garage-list pull-left">
                <div class="search-title">@lang('garage.find_result')<b class="search-name"></b> @lang('garage.all') <b class="search-amount"></b> @lang('garage.record')</div>
                <div class="search-title-loading">@lang('garage.finding')</div>
                <div class="loding-icon"><img src="{{ asset('/images/web/icon/loading.gif') }}"/></div>
                <div class="search-result">
                    <ul>
                        <li class="list-item clone">
                            <div class="col-xs-1 p-a-0"><a class="tag">A</a></div>
                            <div class="col-xs-11">
                                <div class="list-names"></div>
                                <div class="list-address">
                                    <p></p>                    
                                </div>
                            </div>
                            <div style="clear:both"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('javascript')
<script src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script src="{{ asset('component/jquery-scroll/scroll.js') }}"></script>
<script src="{{ asset('web/js/garage.min.js') }}"></script>
<!-- <script src="{{ asset('component/googlemap/marker-clustering.js') }}"></script> -->
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzM3nYEKB1x7ZqpQPvoj89c2m7tFla13I&callback=findGarage"></script> -->


@endsection
@section('hidden')
{{ Form::hidden('hd_get_data_district',route('web.get_data_district')) }} 
{{ Form::hidden('hd_get_data_garage',route('web.get_data_garage')) }} 
@endsection