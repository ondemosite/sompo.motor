@extends('web.layout.default')
@section('title') Sompo Thailand Promotion @endsection
@section('og-title') โปรโมชั่น ซมโปะ ประกันภัย @endsection
@section('og-description') โปรโมชั่น ซมโปะ ประกันภัย ลงทะเบียนรับสิทธิ์ล่วงหน้า @endsection
@section('css')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link href="{{ asset('component/material-datepicker/css/bootstrap-material-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/promotion.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="section_banner">
    <div class="container-fluid">
    @if($init['status']=="ACTIVE")
        <div class="row">
            @if(!empty($init['cover']))
            @section('og-image'){{ asset($init['cover']) }}@endsection
            <div class="wrap-banner col-sm-12 col-md-7">
                <img src="{{ asset($init['cover']) }}" alt="banner">
            </div>
            @endif
            <div class="wrap-form col-sm-12 col-md-5">
                <div class="container-fluid">
                    
                        <!-- Block -->
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('message'))
                        <div class="box-error">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                            <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                            </svg>
                            <h1 class="title text-center text-active">{{ session()->get('message') }}</h1>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h1 class="title">ลงทะเบียนเพื่อรับโปรโมชั่น</h1>
                                    </div>
                                </div>
                                {!! Form::open(['url' => route('web.promotion_register'),'name'=>'form_register','class'=>'form_register','id'=>'form_register']) !!}
                                {{ Form::hidden('promotion_id',!empty($init['promotion_id'])?$init['promotion_id']:'') }}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('common.name')</label>
                                            {{ Form::text('input_name',null, ['class' => 'form-control input-text',
                                            'placeholder' => trans('common.name')]) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('common.lastname')</label>
                                            {{ Form::text('input_lastname',null, ['class' => 'form-control input-text',
                                            'placeholder' => trans('common.lastname')]) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('common.phone')</label>
                                            {{ Form::text('input_tel',null, ['class' => 'form-control input-text',
                                            'placeholder' => trans('common.phone')]) }}        
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('common.email')</label>
                                            {{ Form::text('input_email',null, ['class' => 'form-control input-text',
                                            'placeholder' => trans('common.email')]) }}    
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @if($init['fix_brand'] != null)
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>ยี่ห้อรถยนต์</label>
                                            <select class="form-control select2" name="input_brand">
                                            <option value="" default>@lang('common.brand')</option>
                                            <?php 
                                                $flag = false;
                                            ?>
                                            @foreach ($init['brand'] as $brand)
                                                @if(empty($brand['top_order']) && $flag==false)
                                                <option disabled="disabled" value="" >-------A-Z-------</option>
                                                <?php $flag = true; ?>
                                                @endif
                                                <option value="{{ $brand['id'] }}" default>{{ $brand['name'] }}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{ Form::hidden('fix_brand',$init['fix_brand']) }}    
                                    @endif
                                    @if($init['fix_model'] != null)
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>รุ่นรถยนต์</label>
                                            {{ Form::select('input_model',[],null, ['class' => 'form-control select2','placeholder' => trans('admin_promotion.select_vehicle_model')]) }}
                                        </div>
                                    </div>
                                    {{ Form::hidden('fix_model',$init['fix_model']) }}    
                                    @endif

                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <button type="button" id="button-submit" class="mat-btn btn">@lang('promotion.let')</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        @endif
                </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-xs-12">
                <div class="box-error">
                    @if($init['status']=="INACTIVE")
                    <h1 class="title text-center text-inactive">โปรโมชั่นนี้ยังไม่เปิดใช้งาน หรือหมดอายุแล้ว <br/>ขอบคุณค่ะ</h1>
                    @elseif($init['status']=="PROMOTION_EXPIRE")
                    <h1 class="title text-center text-inactive">@lang('promotion.promotion_expired')</h1>
                    @elseif($init['status']=="REGISTER_EXPIRE")
                    <h1 class="title text-center text-inactive">@lang('promotion.promotion_register_expired')</h1>
                    @elseif($init['status']=="MAX_GRANT")
                    <h1 class="title text-center text-inactive">@lang('promotion.promotion_max_grant')</h1>
                    @elseif($init['status']=="NOTFOUND")
                    <h1 class="title text-center text-inactive">@lang('promotion.promotion_max_grant')</h1>
                    @endif
                </div>
            </div>
        </div>
        @endif
    </div>

    <div class="wrap-popup">
        <div class="box-popup animated">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
            </svg>        
            <h1 class="title text-center text-active">{{ $init['ending_message'] }}</h1>                        
        </div>
    </div>



</section>
@endsection

@section('javascript')
<script src="{{ asset('component/moment/moment.js') }}"></script>
<script>
$(document).ready(function(){
    moment.locale($("input[name='hd_current_lang']").val());
});
</script>
<script src="{{ asset('component/material-datepicker/js/bootstrap-material-datetimepicker.th.min.js') }}"></script>
<script src="{{ asset('component/loader/loader.web.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.rule.js') }}"></script>
<script src="{{ asset('web/js/promotion.min.js') }}"></script>
@endsection
@section('hidden')
{{ Form::hidden('hd_lang_required',trans('validation.custom.required')) }}
{{ Form::hidden('hd_lang_number',trans('validation.custom.number')) }}
{{ Form::hidden('hd_lang_email',trans('validation.custom.email')) }}
{{ Form::hidden('hd_get_vehicle_model',route('web.promotion_get_vehicle_model')) }} 
{{ Form::hidden('hd_get_vehicle_brand',route('web.promotion_get_vehicle_brand')) }} 
{{ Form::hidden('hd_lang_model',trans('admin_promotion.select_vehicle_model')) }} 
{{ Form::hidden('hd_lang_brand',trans('admin_promotion.select_vehicle_brand')) }} 
@endsection