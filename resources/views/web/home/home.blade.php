@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('component/slick-1.8.0/slick/slick.min.css') }}">
<link rel="stylesheet" href="{{ asset('component/slick-1.8.0/slick/slick-theme.min.css') }}">
<link href="{{ asset('web/css/home.min.css?v=1.2') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="section-banner" class="target-detect" data-target="target-home">
    <div class="container-fluid">
        <div class="row">
            <div class="wrap-banner col-xs-12">
                <div class="container calculate-box">
                    <div class="row">
                        <div class="col-xs-12 col-input">
                            <div class="title">
                                <!--
                                <h1><i class="fa fa-car icon" aria-hidden="true"></i>&nbsp;&nbsp;@lang('home.find_insurance')</h1>-->
                                <h1><img src="{{ asset('/images/web/icon/Sompony_search.png') }}" alt="">&nbsp;&nbsp;@lang('home.find_insurance')</h1>
                                
                            </div>
                        </div>
                    </div>
                    {!! Form::open(['url' => route('web.form_input'),'name'=>'form_calculate','class'=>'form_calculate','id'=>'form_calculate']) !!}
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-input">
                            <div class="form-group form-step1">
                                <select class="form-control select2" name="input_brand">
                                <option value="" default>@lang('common.brand')</option>
                                <?php 
                                    $flag = false;
                                ?>
                                @foreach ($init['brand'] as $brand)
                                    @if(empty($brand['top_order']) && $flag==false)
                                    <option disabled="disabled" value="" >-------A-Z-------</option>
                                    <?php $flag = true; ?>
                                    @endif
                                    <option value="{{ $brand['id'] }}" default>{{ $brand['name'] }}</option>
                                @endforeach
                                </select>
                                <label id="input_brand-error" class="error" for="input_brand" style="display: none;"></label>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-input">
                            <div class="form-group form-model form-step1">
                                {{ Form::select('input_model',[],null, ['class' => 'form-control select2','placeholder' => trans('placeholder.model')]) }}
                                <label id="input_model-error" class="error" for="input_model" style="display: none;"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-md-4 col-sm-6 col-input">
                            <div class="form-group form-model-year form-step1">
                                {{ Form::select('input_model_year',[],null, ['class' => 'form-control select2','placeholder' => trans('placeholder.model_year')]) }}
                                <label id="input_model_year-error" class="error" for="input_model_year" style="display: none;"></label>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-input">
                            <div class="form-group form-sub-model form-step1">
                                {{ Form::select('input_sub_model',[],null, ['class' => 'form-control select2','placeholder' => trans('placeholder.sub_model')]) }}
                                <label id="input_sub_model-error" class="error" for="input_sub_model" style="display: none;"></label>
                            </div>
                        </div>               
                    </div>
                    <div class="row">
                         <div class="col-md-4 col-sm-6 col-input">
                            <div class="form-group form-step1">
                                {{ Form::select('input_is_bkk',!empty($init['is_bkk'])?$init['is_bkk']:[],null, ['class' => 'form-control select2','placeholder' => 'จังหวัดที่จดทะเบียนรถยนต์']) }}
                                <label id="input_is_bkk-error" class="error" for="input_is_bkk" style="display: none;"></label>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-input">
                            <div class="form-group form-step1">
                                {{ Form::select('input_plan',!empty($init['plan'])?$init['plan']:[],1, ['class' => 'form-control select2']) }}
                            </div>
                        </div>               
                    </div>
                    <div class="row footer-row">
                        <!-- <div class="col-xs-12 col-md-3 col-sm-6 col-input">
                            {{ Form::text('input_cost',null, ['class' => 'form-control input-number','placeholder' => trans('placeholder.type_cost')]) }}
                        </div> -->
                        <div class="col-xs-12 col-md-3 col-sm-6 col-input col-submit">
                            <button type="submit" class="mat-btn btn">@lang('home.submit_find')</button>   
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div> <!--/container -->
                <!--
                <div class="itembg">
                    <img src="{{ asset('/images/web/icon/banner-item1.png') }}" alt="">
                </div>
                <div class="line"></div>
                <div class="itembg_mobile">
                    <img src="{{ asset('/images/web/icon/banner-item2.png') }}" alt="">
                </div>-->
            </div> <!-- /wrap-banner -->
        </div>
    </div>
</section>

@if(!empty($init['whysompo']))
<section id="section-why">
    <div class="container-fluid">
        <div class="row">
            <div class="wrap-why col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="section-title animated wp-animate myfade" data-wptype="fadeInUp-20" data-wpdelay="100">
                            @if(App::isLocale('en'))
                            WHY <span>SOMPO</span>
                            @else
                            ทำไมต้องเลือก <span>ซมโปะ</span>
                            @endif
                            </h1>         
                        </div> 
                    </div>
                    <div class="row slide-row">
                        @foreach($init['whysompo'] as $item)
                        <div class="col-md-4">
                            <div class="article animated wp-animate myfade" data-wptype="fadeInUp-20" data-wpdelay="200">
                                <article>
                                    <div class="article_cover"><img src="{{ App::isLocale('en')?asset($item['cover_path_en']):asset($item['cover_path_th']) }}" width="134"/></div>
                                    <div class="article_title">
                                        <h1>{{ App::isLocale('en')?$item['title_en']:$item['title_th']}}</h1>
                                    </div>
                                    <div class="article_paragraph">
                                        <p>{!! App::isLocale('en')?$item['detail_en']:$item['detail_th'] !!}</p>
                                    </div>
                                </article>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>                 
            </div> <!--/wrap-why -->
        </div>
    </div><!--/container-fluid -->
</section>
@endif

@if(!empty($init['ourproduct']))
<section id="section-product" class="target-detect" data-target="target-product">
    <div class="container-fluid">
        <div class="row">
            <div class="wrap-product col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="section-title animated wp-animate myfade" data-wptype="fadeInUp-20" data-wpdelay="200">
                            @if(App::isLocale('en'))
                            OUR <span>PRODUCT</span>
                            @else
                            <span>ผลิตภัณฑ์</span> ของเรา
                            @endif
                            
                            </h1>         
                        </div> 
                    </div>
                    <div class="row slide-row">
                        @foreach($init['ourproduct'] as $item)
                        <div class="col-md-4">
                            <div class="article animated wp-animate myfade" data-wptype="fadeInUp-20" data-wpdelay="200">
                                <article>
                                    <div class="article_cover">
                                        <img src="{{ App::isLocale('en')?asset($item['cover_path_en']):asset($item['cover_path_th']) }}" />
                                        <span class="product-toggle-show"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                    </div>
                                    <div class="article_title">
                                        <h1>{{ App::isLocale('en')?$item['title_en']:$item['title_th'] }}</h1>
                                    </div>
                                    <div class="article_paragraph">
                                        {!! App::isLocale('en')?$item['detail_en']:$item['detail_th'] !!}
                                    </div>
                                    <!-- Detail -->
                                    <div class="article_detail">
                                        <span class="product-toggle-hide"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
                                        <h2>{{ App::isLocale('en')?$item['title_en']:$item['title_th'] }}</h2>
                                        <p class="tag-header">@lang('step2.owner_damage')</p>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td><p class="tag-detail">@lang('step2.accident_have') (@lang('step2.car_crash'))</p></td>
                                                <td width="20%">
                                                    @if($item['title_en']=="SOMPO 2+" || $item['title_th']=="SOMPO 2+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @elseif($item['title_en']=="SOMPO 3+" || $item['title_th']=="SOMPO 3+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @else
                                                    <span class="tag-result tag-danger"><i class="fa fa-close"></i></span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><p class="tag-detail">@lang('step2.car_rob')</p></td>
                                                <td width="20%">
                                                    @if($item['title_en']=="SOMPO 2+" || $item['title_th']=="SOMPO 2+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @elseif($item['title_en']=="SOMPO 3+" || $item['title_th']=="SOMPO 3+")
                                                    <span class="tag-result tag-danger"><i class="fa fa-close"></i></span>
                                                    @else
                                                    <span class="tag-result tag-danger"><i class="fa fa-close"></i></span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><p class="tag-detail">@lang('step2.car_fire')</p></td>
                                                <td width="20%">
                                                    @if($item['title_en']=="SOMPO 2+" || $item['title_th']=="SOMPO 2+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @elseif($item['title_en']=="SOMPO 3+" || $item['title_th']=="SOMPO 3+")
                                                    <span class="tag-result tag-danger"><i class="fa fa-close"></i></span>
                                                    @else
                                                    <span class="tag-result tag-danger"><i class="fa fa-close"></i></span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <!-- <tr>
                                                <td><p class="tag-detail">@lang('step2.car_terrorism')</p></td>
                                                <td width="20%">
                                                    @if($item['title_en']=="SOMPO 2+" || $item['title_th']=="SOMPO 2+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @elseif($item['title_en']=="SOMPO 3+" || $item['title_th']=="SOMPO 3+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @else
                                                    <span class="tag-result tag-danger"><i class="fa fa-close"></i></span>
                                                    @endif
                                                </td>
                                            </tr> -->
                                            <!-- <tr>
                                                <td><p class="tag-detail">@lang('step2.24hour')</p></td>
                                                <td width="20%">
                                                    @if($item['title_en']=="SOMPO 2+" || $item['title_th']=="SOMPO 2+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @elseif($item['title_en']=="SOMPO 3+" || $item['title_th']=="SOMPO 3+")
                                                    <span class="tag-result tag-success"><i class="fa fa-check"></i></span>
                                                    @else
                                                    <span class="tag-result tag-danger"><i class="fa fa-close"></i></span>
                                                    @endif
                                                </td>
                                            </tr> -->
                                        </table>
                                        <p class="tag-header">@lang('step2.garanty_out')</p>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td><p class="tag-detail">ความรับผิดต่อทรัพย์สิน</p></td>
                                                <td width="20%"><span class="tag-result tag-success"><i class="fa fa-check"></i></span></td>
                                            </tr>
                                            <tr>
                                                <td><p class="tag-detail">ความเสียหายต่อการบาดเจ็บหรือเสียชีวิต</p></td>
                                                <td width="20%"><span class="tag-result tag-success"><i class="fa fa-check"></i></span></td>
                                            </tr>
                                           
                                        </table>
                                        <p class="tag-header">@lang('step2.coverage_addition')</p>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td><p class="tag-detail">@lang('step2.private_accident')</p></td>
                                                <td width="20%"><span class="tag-result tag-success"><i class="fa fa-check"></i></span></td>
                                            </tr>
                                            <tr>
                                                <td><p class="tag-detail">@lang('step2.medical_fee')</p></td>
                                                <td width="20%"><span class="tag-result tag-success"><i class="fa fa-check"></i></span></td>
                                            </tr>
                                            <tr>
                                                <td><p class="tag-detail">@lang('step2.bail_driver')</p></td>
                                                <td width="20%"><span class="tag-result tag-success"><i class="fa fa-check"></i></span></td>
                                            </tr>
                                        </table>
                                    </div>
                                </article>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    
                </div>                 
            </div> <!--/wrap-why -->
        </div>
    </div><!--/container-fluid -->
</section>
@endif

@if(!empty($init['banner']))
<section id="section-promotion">
    <div class="container-fluid animated wp-animate myfade" data-wptype="fadeInUp-20" data-wpdelay="200">
        <div class="wrap-promotion">
            <div class="row promotion-slider">
            @php 
                $agent = new Jenssegers\Agent\Agent;
            @endphp
            @foreach($init['banner'] as $item)
            <div class="col-xs-12 p-a-0">
                @php
                    $promotion = $item->promotion()->first();
                    $path = null;
                    $link = null;
                    if($agent->isMobile() || $agent->isTablet()){
                        $path = App::isLocale('th')?$item->cover_th_mobile:$item->cover_en_mobile;
                    }else{
                        $path = App::isLocale('th')?$item->cover_th:$item->cover_en;
                    }
                    if(!empty($promotion)){
                        $link = route('web.promotion')."?ref=".$promotion->formular;
                    }else{
                        $link = $item->link;
                    }
                @endphp
                <a href="{{ $link }}">
                    <div class="slide-item">
                        <img src="{{ asset($path) }}" alt="{{ $item->title }}">
                    </div>
                </a>
            </div>
            @endforeach
            </div>
        </div>
    </div>
</section>
@endif

<section id="garage">
    <div class="container-fluid">
        <div class="row">
            <div class="wrap-garage col-xs-12">
                <div class="container">
                    <div class="row">
                        <h1 class="section-title animated wp-animate myfade" data-wptype="fadeInUp-20">@lang('home.find') <span>@lang('home.garage')</span></h1>         
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="garage-box animated wp-animate myfade" data-wptype="fadeInUp-20">
                                <div class="tab-content clearfix">
                                    <div class="tab-pane active" id="tab-place">
                                        <form>
                                        <div class="row">
                                            <div class="col-xs-12 col-input">
                                                <div class="form-group">
                                                    {{ Form::select('input_garage_prov',!empty($init['province'])?$init['province']:null,null, ['class' => 'form-control select2','data-rule-required'=>'true',
                                                    'placeholder' => trans('common.province')]) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::text('input_garage_name',null, ['class' => 'form-control input-text','data-rule-required'=>'true',
                                                    'placeholder' => trans('placeholder.type_name'),'autocomplete'=>'off']) }}
                                                    {{ Form::hidden('selected_name',null) }} 
                                                </div><!-- /input-group -->  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-input">
                                                <div class="form-group">
                                                    <button type="button" class="mat-btn btn search-btn" id="searchGarageSubmit">@lang('home.find')</button>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div><!-- tab-content -->
                                <div class="result-box">
                                    <ul></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="itemcar"><img src="{{ asset('/images/web/icon/banner-item2.png') }}" alt=""></div>-->
            </div>
        </div>
    </div>
</section>

@if(!empty($init['faqs']))
<section id="section_faq" class="target-detect" data-target="target-faq">
    <div class="container-fluid">
        <div class="row">
            <div class="wrap-faq col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="title animated wp-animate myfade" data-wptype="fadeInUp-20"><img src="{{ asset('/images/web/icon/icon_faqs.png') }}" /><h1>@lang('home.faq')</h1></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="faq_box" class="faq-box animated wp-animate myfade" data-wptype="fadeInUp-20">
                                @foreach($init['faqs'] as $key => $value)   
                                <div class="faq-item">
                                    <div class="faq-header" data-toggle="collapse" data-target="#faq_{{$key}}" data-parent="#faq_box">
                                        <span class="faq-order">{{$key+1}}</span>
                                        <h1 class="faq-title">{{$init['faqs'][$key]['question']}}</h1>
                                        <span class="faq-btn">
                                            <i class="fa" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="accordion-group">
                                        <div id="faq_{{$key}}" class="faq-body collapse indent">
                                            <div>{!! $init['faqs'][$key]['answer'] !!}</div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach                     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif


<section id="section_footer" class="target-detect" data-target="target-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="wrap-footer col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12 secure-box">
                            <div class="title animated wp-animate myfade" data-wptype="fadeInUp-20">@lang('home.cert')</div>
                            <div class="detail animated wp-animate myfade" data-wptype="fadeInUp-20" data-wpdelay="200">
                                <div class="col-md-4 col-xs-12"><img src="{{ asset('/images/web/logo/kpp.jpg') }}" /></div>
                                <div class="col-md-4 col-xs-12"><img src="{{ asset('/images/web/logo/sompo.jpg') }}" /></div>
                                <div class="col-md-4 col-xs-12"><img src="{{ asset('/images/web/logo/geo.jpg') }}" /></div>                   
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12 payment-box">
                            <div class="title animated wp-animate myfade" data-wptype="fadeInUp-20">@lang('home.channel')</div>      
                            <div class="detail animated wp-animate myfade" data-wptype="fadeInUp-20">
                                <div class="col-md-3 col-xs-12"><img src="{{ asset('/images/web/logo/secure-1.jpg') }}" /></div>
                                <div class="col-md-3 col-xs-12"><img src="{{ asset('/images/web/logo/secure-2.jpg') }}" /></div>
                                <div class="col-md-3 col-xs-12"><img src="{{ asset('/images/web/logo/secure-3.jpg') }}" /></div>  
                                <div class="col-md-3 col-xs-12"><img src="{{ asset('/images/web/logo/secure-4.jpg') }}" /></div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
   
@endsection

@section('javascript')
<script src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script src="{{ asset('component/jquery-validate/jquery.validate.rule.js') }}"></script>
<script src="{{ asset('component/slick-1.8.0/slick/slick.min.js') }}"></script>
<script src="{{ asset('component/jquery-money-format/jquery-money-format.js') }}"></script>
<script src="{{ asset('web/js/home.min.js') }}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_data_district',route('web.get_data_district')) }} 
{{ Form::hidden('hd_get_data_model',route('web.get_vehicle_data_model')) }} 
{{ Form::hidden('hd_get_data_model_type',route('web.get_vehicle_data_model_type')) }} 
{{ Form::hidden('hd_get_data_model_year',route('web.get_vehicle_data_model_year')) }} 

{{ Form::hidden('hd_lang_model',trans('placeholder.model')) }} 
{{ Form::hidden('hd_lang_model_year',trans('placeholder.model_year')) }} 
{{ Form::hidden('hd_lang_sub_model',trans('placeholder.sub_model')) }} 

{{ Form::hidden('hd_lang_required_brand',trans('home.required_brand')) }}
{{ Form::hidden('hd_lang_required_model',trans('home.required_model')) }}
{{ Form::hidden('hd_lang_required_year',trans('home.required_year')) }}
{{ Form::hidden('hd_lang_required_submodel',trans('home.required_submodel')) }}
{{ Form::hidden('hd_lang_currentcy',trans('validation.custom.number')) }}
@endsection