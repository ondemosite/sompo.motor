@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link href="{{ asset('component/bootstrap-dialog/bootstrap-dialog.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('component/material-datepicker/css/bootstrap-material-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance.min.css?v=1.1') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance_3.min.css?v=1') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="stepbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="inside-step-bar">
                    <ul>
                        <li class="pass">
                            <a href="{{ route('web.dashboard') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">ข้อมูลรถ</div>
                            </a>
                        </li>
                        <li class="pass">
                            <a href="{{ route('web.insurance_step2') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">แผนประกัน</div>
                            </a>
                        </li>
                        <li class="active">
                            <div class="step-circle">3</div>
                            <div class="step-detail">ปรับแต่งแผน</div>
                        </li>
                        <li>
                            <div class="step-circle">4</div>
                            <div class="step-detail">ข้อมูลผู้เอาประกัน</div>
                        </li>
                        <li>
                            <div class="step-circle">5</div>
                            <div class="step-detail">ชำระเงิน</div>
                        </li>
                    </ul>
                    <div class="stregth-bar"></div>
                </div>
            </div>
        </div>
  </div> <!--/container -->
</section>

<section id="section-insurance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 wrap-section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 page-title"><h1>@lang('step3.insurance_detail')</h1></div>
                    </div>
                    <div class="row wrap-insurance-detail">
                        <div class="col-md-3 col-sm-2 col-icon hidden-xs"><img src="{{ asset('/images/web/icon/Sompony_03.png') }}"></div>
                        <div class="col-md-7 col-sm-10 text-center">
                            <div class="form-title"><h1>ระยะเวลาเอาประกันภัย</h1></div>
                            <form>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="" >@lang('step3.insurance_start')</label>
                                            {{ Form::text('input_start_insurance',$init['input_data']['start_insurance'], ['class' => 'form-control form-input datepicker','placeholder' => 'กรุณาเลือกวันที่']) }}
                                            <label class="error-text" for="input_start_insurance"></label>
                                            <span class="add-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="">@lang('step3.insurance_expire')</label>
                                            {{ Form::text('input_end_insurance',$init['input_data']['end_insurance'], ['class' => 'form-control form-input','placeholder' => '','disabled'=>'disabled']) }}
                                        </div>
                                    </div>
                                </div>
                                @if($init['coverage_id']!='3')
                                    <!-- <div class="row">
                                        <div class="col-xs-12">
                                            <label for="" >@lang('step3.driver_amount') <a href="#" class="help-modal" data-title="@lang('help.driver_amount_title')" data-detail="@lang('help.driver_amount_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i></a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                @if($init['input_data']['define_name']!="UNNAMED" && $init['input_data']['define_name']!=null)
                                                <label class="select-radio active" data-target="specify-age" data-type="show" data-group="driver" data-for="input_define_name" data-value="1">@lang('step3.specify')</label>
                                                @else
                                                <label class="select-radio" data-target="specify-age" data-type="show" data-group="driver" data-for="input_define_name" data-value="1">@lang('step3.specify')</label>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                @if($init['input_data']['define_name']=="UNNAMED")
                                                <label class="select-radio active select-radio-second" data-target="specify-age" data-type="hidden" data-group="driver" data-for="input_define_name" data-value="0">@lang('step3.none')</label>
                                                @elseif(isset($init['define_name_amount']) && $init['define_name_amount']==0)
                                                <label class="select-radio active select-radio-second" data-target="specify-age" data-type="hidden" data-group="driver" data-for="input_define_name" data-value="0">@lang('step3.none')</label>
                                                @else
                                                <label class="select-radio select-radio-second" data-target="specify-age" data-type="hidden" data-group="driver" data-for="input_define_name" data-value="0">@lang('step3.none')</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @if($init['input_data']['define_name']!="UNNAMED" && $init['input_data']['define_name']!=null)
                                        <div class="row specify-row" id="specify-age" style="display:block">
                                    @else
                                        <div class="row specify-row" id="specify-age">
                                    @endif
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="" >@lang('step3.type_driver1')</label>
                                                {{ Form::select('input_define_name_value1',["18-24Y"=>"18-24 ".trans('common.year'),"25-28Y"=>"25-28 ".trans('common.year'),"29-35Y"=>"29-35 ".trans('common.year'),"36-50Y"=>"36-50 ".trans('common.year'),"50UP"=>"50 ".trans('common.year_up')],!empty($init['input_data']['define_name'])?$init['input_data']['define_name']:'',['class' => 'form-control select2-local detect-change carloss-change hb-change theft-change taxi-change flood-change','placeholder' => trans('placeholder.please_select')]) }}
                                                <label class="error-text" for="input_define_name_value1">@lang('step3.type_driver1')</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="" >@lang('step3.type_driver2')</label>
                                                {{ Form::select('input_define_name_value2',["18-24Y"=>"18-24 ".trans('common.year'),"25-28Y"=>"25-28 ".trans('common.year'),"29-35Y"=>"29-35 ".trans('common.year'),"36-50Y"=>"36-50 ".trans('common.year'),"50UP"=>"50 ".trans('common.year_up')],!empty($init['input_data']['define_name2'])?$init['input_data']['define_name2']:'',['class' => 'form-control select2-local detect-change carloss-change hb-change theft-change taxi-change flood-change','placeholder' => trans('placeholder.please_select')]) }}
                                            </div>
                                        </div>
                                    </div> -->
                                @endif
                                <!-- <div class="row row_focus_driver">
                                    <div class="col-xs-12">
                                        <label class="error-text" for="input_define_name_value">@lang('step3.please_select') @lang('step3.driver_amount')</label>
                                    </div>
                                </div> -->
                                <!-- Update Car -->
                                <div class="form-title"><h1>@lang('step4.car_data')</h1></div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            {{ Form::text('input_info_car',!empty($init['full_car'])?$init['full_car']:null, ['class' => 'form-control form-input warning-text input-readonly','placeholder' => '','readonly'=>'readonly']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 dash-after">
                                        <div class="form-group">
                                            <label>ทะเบียนรถยนต์</label>
                                            {{ Form::text('input_info_prefix',!empty($init['input_data']['car_info']['prefix'])?$init['input_data']['car_info']['prefix']:'', ['class' => 'form-control form-input detect-change','placeholder' => 'กข','maxlength'=>'3']) }}
                                            <label class="error-text" for="input_info_prefix">@lang('step3.error_specify')</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="hidden-sm hidden-xs">&nbsp;</label>
                                            {{ Form::text('input_info_licence',!empty($init['input_data']['car_info']['licence'])?$init['input_data']['car_info']['licence']:'', ['class' => 'form-control form-input detect-change allownumericwithoutdecimal','placeholder' => '9999','maxlength'=>'4']) }}
                                            <label class="error-text" for="input_info_licence">@lang('step3.error_specify')</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>@lang('common.province')</label>
                                            {{ Form::select('input_info_car_province',!empty($init['province'])?$init['province']:null,!empty($init['input_data']['car_info']['province'])?$init['input_data']['car_info']['province']:'',['class' => 'form-control select2 detect-change','placeholder' => trans('placeholder.select_province')]) }}
                                            <label class="error-text" for="input_info_car_province">@lang('step3.error_specify')</label>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row row_focus_car">
                                    <div class="col-xs-12 warning-text">** @lang('step3.make_sure') **</div>
                                </div> -->
                                <!-- <br/> -->
                                <!-- -->

                                <!-- <div class="row">
                                    <div class="col-xs-12">
                                        <label >@lang('step3.is_cctv') (CCTV) <a href="#" class="help-modal" data-title="@lang('help.cctv_title')" data-detail="@lang('help.cctv_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio {{ $init['input_data']['cctv']=='1'?'active':'' }}" data-group="cctv" data-for="input_cctv" data-value="1">@lang('step3.cctv_yes')</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio {{ $init['input_data']['cctv']=='0'?'active':'' }}" data-group="cctv" data-for="input_cctv" data-value="0">@lang('step3.cctv_no')</label>
                                        </div>
                                    </div>
                                </div> -->
                                @if($init['coverage_id']!='3')
                                {!! Form::hidden('input_enable_garage_dealer',$init['enable_garage_dealer']) !!}
                                <!-- <div class="row">
                                    <div class="col-xs-12">
                                        <label for="" >@lang('step3.select_garage') <a href="#" class="help-modal" data-title="@lang('help.garage_type_title')" data-detail="@lang('help.garage_type_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio {{ $init['input_data']['garage_type']=='DEALER'?'active':'' }}" data-group="garage" data-for="input_garage" data-value="DEALER">@lang('step3.premium_garage')</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio {{ $init['input_data']['garage_type']=='GENERAL'?'active':'' }}" data-group="garage" data-for="input_garage" data-value="GENERAL">@lang('step3.general_garage')</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="" >@lang('step3.excess') <a href="#" class="help-modal" data-title="@lang('help.service_charge_title')" data-detail="@lang('help.service_charge_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                            @if($init['input_data']['define_name']=="UNNAMED")
                                            {{ Form::select('input_deductible',["0"=>trans('step3.none'),"2000"=>"2,000"],$init['input_data']['deductible'],['class' => 'form-control select2-local detect-change flood-change','disabled'=>'disabled']) }}
                                            @else
                                            {{ Form::select('input_deductible',["0"=>trans('step3.none'),"2000"=>"2,000"],$init['input_data']['deductible'],['class' => 'form-control select2-local detect-change flood-change']) }}
                                            @endif
                                            <p class="sub-detail"><small>@lang('step3.excess_small')</small></p>
                                        </div>
                                    </div>
                                </div> -->
                                @else
                                <!-- <div class="row">
                                    <div class="col-xs-12">
                                        <label for="" >@lang('step3.select_garage') <a href="#" class="help-modal" data-title="@lang('help.garage_type_title')" data-detail="@lang('help.garage_type_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio disable" data-disable="แผนประกันประเภท 3 ไม่สามารถเลือกประเภทอู่ซ่อมได้">@lang('step3.premium_garage')</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio active" data-group="garage" data-for="input_garage" data-value="GENERAL">@lang('step3.general_garage')</label>
                                        </div>
                                    </div>
                                </div> -->
                                @endif
                                <!-- <div class="row">
                                    <div class="col-xs-12">
                                        <label for="" >@lang('step3.is_personal')</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio {{ $init['input_data']['is_personal']=='1'?'active':'' }}" data-group="carpersonal" data-for="input_personal" data-value="1">@lang('common.yes')</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio select-radio-second help-modal alert-carpersonal" data-group="carpersonal" 
                                            data-title="@lang('help.carpersonal_title')" data-detail="@lang('help.carpersonal_detail')" data-for="input_personal" data-value="0">@lang('common.no')</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row_focus_personal">
                                    <div class="col-xs-12">
                                        <label class="error-text" for="input_personal">@lang('step3.please_select') @lang('step3.is_personal')</label>
                                    </div>
                                </div> -->
                            </form>
                        </div><!-- col-md-6 -->
                        <div class="col-md-2"></div>
                    </div> <!-- row -->
                </div> <!--/container-->
            </div><!-- /wrap-section -->

            <div class="col-xs-12 wrap-section" style="display:block;">
                <div class="container">
                    @if($init['coverage_id']!='3')
                    <div class="row wrap-insurance-addition">
                        <div class="col-md-3 col-sm-2 "></div>
                        <div class="col-md-7 col-sm-10 text-left">
                            <div class="form-title my-tooltip {{ $init['input_data']['is_select_addition']=='YES'?'collapsed':'' }}" data-toggle="collapse" data-target="#wrap-addon"><h1>@lang('step3.addition_coverage') (@lang('step3.select_more_one')) <span class="tooltiptext">คลิกเลือกซื้อความคุ้มครองเพิ่มเติม</span></h1></div>
                            <div class="wrap-addon collapse {{ $init['input_data']['is_select_addition']=='YES'?'in':'' }}" id="wrap-addon" >
                                <div class="hidden-xs icon-addon"><img src="{{ asset('/images/web/icon/iconplan-pink.svg') }}"></div>
                                <form>
                                    <!-- เงินชดเชยกรณีรถยนต์สูญหายหรือเสียหายสื้นเชิงจากอุบัติเหตุ -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label>@lang('step3.carloss_coverage')<a href="#" class="help-modal" data-title="เงินชดเชยกรณีรถยนต์สูญหายหรือเสียหายสิ้นเชิงจากอุบัติเหตุ" data-detail="เงินชดเชยกรณีรถยนต์สูญหายหรือเสียหายสิ้นเชิงจากอุบัติเหตุ" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio my-tooltip {{ $init['input_data']['is_carloss']=='1'?'active':'' }}" data-target="specify-carloss" data-group="carloss" data-for="input_carloss" data-value="1">@lang('common.buy') (+<span class="specify-carloss">{{ !empty($init['carloss_button'])?number_format($init['carloss_button'],2):'' }}</span>) <span class="tooltiptext">คลิกปุ่มเพื่อต้องการซื้อ</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio select-radio-second my-tooltip {{ $init['input_data']['is_carloss']!='1'?'active':'' }}" data-target="specify-carloss" data-group="carloss" data-for="input_carloss" data-value="0">@lang('common.no_buy') <span class="tooltiptext">ไม่ซื้อ</span></label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- เงินชดเชยรายได้รายวันกรณีรถยนต์เกิดอุบัติเหตุ จนต้องรักษาตัวเป็นผู้ป่วยใน ของโรงพยาบาล -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label>@lang('step3.hb_coverage')<a href="#" class="help-modal" data-title="เงินชดเชยรายได้รายวันกรณีรถยนต์เกิดอุบัติเหตุ" data-detail="เงินชดเชยรายได้รายวันกรณีรถยนต์เกิดอุบัติเหตุ จนต้องรักษาตัวเป็นผู้ป่วยใน ของโรงพยาบาล" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio my-tooltip {{ $init['input_data']['is_hb']=='1'?'active':'' }}" data-target="specify-hb" data-type="show" data-group="hb" data-for="input_is_hb" data-value="1">@lang('common.buy') (+<span class="specify-hb">{{ !empty($init['hb_button'])?number_format($init['hb_button'],2):'' }}</span>) <span class="tooltiptext">คลิกปุ่มเพื่อต้องการซื้อ</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio select-radio-second my-tooltip {{ $init['input_data']['is_hb']!='1'?'active':'' }}" data-target="specify-hb" data-type="hidden" data-group="hb" data-for="input_is_hb" data-value="0">@lang('common.no_buy') <span class="tooltiptext">ไม่ซื้อ</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row specify-row" id="specify-hb" {!! !empty($init['input_data']['input_hb'])?'style="display:block"':'' !!}>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <select class="form-control select2-local detect-change hb-change" name="input_hb" >
                                                    @if(!empty($init['hb']))
                                                        @foreach($init['hb'] as $value)
                                                            <option value="{{ $value }}" {{ $value==$init['input_data']['input_hb']?'selected':'' }}  >@lang('step3.hb_sub') {{ number_format($value,0) }} @lang('common.baht')</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- คุ้มครองน้ำท่วม -->
                                    @if(!empty($init['flood_button']))
                                    <!-- <div class="row">
                                        <div class="col-xs-12">
                                            <label>@lang('step3.flood_coverage') <a href="#" class="help-modal" data-title="@lang('help.flood_title')" data-detail="@lang('help.flood_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio my-tooltip {{ $init['input_data']['is_flood']=='1'?'active':'' }}" data-group="flood" data-for="input_flood" data-value="1">@lang('common.buy') (+<span class="specify-flood">{{ !empty($init['flood_button'])?number_format($init['flood_button'],2):'' }}</span>) <span class="tooltiptext">คลิกปุ่มเพื่อต้องการซื้อ</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio select-radio-second my-tooltip" data-group="flood" data-for="input_flood" data-value="0">@lang('common.no_buy') <span class="tooltiptext">ไม่ซื้อ</span></label>
                                            </div>
                                        </div>
                                    </div> -->
                                    @endif

                                    <!-- คุ้มครองโจรกรรมทรัพย์สินภายในรถยนต์ -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                                <label for="" >@lang('step3.robbery_stuff') <a href="#" class="help-modal" data-title="@lang('help.robbery_title')" data-detail="@lang('help.robbery_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio my-tooltip {{ $init['input_data']['is_robbery']=='1'?'active':'' }}" data-target="specify-robbery" data-type="show" data-group="robbery" data-for="input_robbery" data-value="1">@lang('common.buy') (+<span class="specify-theft">{{ !empty($init['theft_button'])?number_format($init['theft_button'],2):'' }}</span>) <span class="tooltiptext">คลิกปุ่มเพื่อต้องการซื้อ</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio select-radio-second my-tooltip {{ $init['input_data']['is_robbery']!='1'?'active':'' }}" data-target="specify-robbery" data-type="hidden" data-group="robbery" data-for="input_robbery" data-value="0">@lang('common.no_buy') <span class="tooltiptext">ไม่ซื้อ</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row specify-row" id="specify-robbery" {!! !empty($init['input_data']['input_theft'])?'style="display:block"':'' !!}>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <select class="form-control select2-local detect-change theft-change" name="input_theft" >
                                                    @if(!empty($init['theft']))
                                                        @foreach($init['theft'] as $value)
                                                            <option value="{{ $value }}" {{ $value==$init['input_data']['input_theft']?'selected':'' }} >@lang('step3.coverage_stuff') {{ number_format($value,0) }} @lang('common.baht')</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- ชดเชยค่าเดินทางต่อการเข้าซ่อม -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label for="" >@lang('step3.taxi_coverage') <a href="#" class="help-modal" data-title="@lang('help.travel_title')" data-detail="@lang('help.travel_detail')" data-img="{{ asset('/images/web/icon/Sompony_popup.png') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio my-tooltip {{ $init['input_data']['is_travel']=='1'?'active':'' }}" data-target="specify-travel" data-type="show" data-group="travel" data-for="input_travel" data-value="1">@lang('common.buy') (+<span class="specify-taxi">{{ !empty($init['taxi_button'])?number_format($init['taxi_button'],2):'' }}</span>) <span class="tooltiptext">คลิกปุ่มเพื่อต้องการซื้อ</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="select-radio select-radio-second my-tooltip {{ $init['input_data']['is_travel']!='1'?'active':'' }}" data-target="specify-travel" data-type="hidden" data-group="travel" data-for="input_travel" data-value="0">@lang('common.no_buy') <span class="tooltiptext">ไม่ซื้อ</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row specify-row" id="specify-travel" {!! !empty($init['input_data']['input_taxi'])?'style="display:block"':'' !!}>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <select class="form-control select2-local detect-change taxi-change" name="input_taxi" >
                                                    @if(!empty($init['taxi']))
                                                        @foreach($init['taxi'] as $value)
                                                            <option value="{{ $value }}" {{ $value==$init['input_data']['input_taxi']?'selected':'' }}>@lang('step3.taxi_cost') {{ number_format($value,0) }} บาท</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><!--/wrap-addon-->
                        </div>
                        <div class="col-md-2"></div>
                    </div> <!-- row -->
                    @endif
                </div>
            </div><!-- /wrap-section -->
        </div>
    </div>


    <div class="container-fluid">
        <!-- <div class="row wrap-prb wrap-section"> -->
        <div class="row wrap-section" style="padding-top: 20px">
            <div class="col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-2 col-icon hidden-xs"></div>
                        <div class="col-md-7 col-sm-10 text-left">
                            <form>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label for="" >@lang('step3.is_compulsory')</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio my-tooltip {{ $init['input_data']['is_com']=='1'?'active':'' }}" data-target="specify-prb" data-type="show" data-group="prb" data-for="input_prb" data-value="1">@lang('common.buy') (+{{ !empty($init['compulsory'])?number_format((float)$init['compulsory'], 2):'' }}) <span class="tooltiptext">คลิกปุ่มเพื่อต้องการซื้อ พรบ.</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="select-radio select-radio-second my-tooltip {{ $init['input_data']['is_com']=='0'?'active':'' }}" data-target="specify-prb" data-type="hidden" data-group="prb" data-for="input_prb" data-value="0">@lang('common.no_buy') <span class="tooltiptext">ไม่ซื้อ</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row specify-row" id="specify-prb" {!! !empty($init['input_data']['is_com'])?'style="display:block;"':''  !!}>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="" >@lang('step3.insurance_start')</label>
                                            {{ Form::text('input_start_compulsory',$init['input_data']['start_compulsory'], 
                                            ['class' => 'form-control form-input','disabled'=>'disabled','placeholder' => trans('placeholder.select_birthdate')]) }}
                                            <label class="error-text" for="input_start_compulsory">@lang('step3.please_correct_date')</label>
                                            <!-- <span class="add-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span> -->
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">@lang('step3.insurance_expire')</label>
                                            {{ Form::text('input_end_compulsory',$init['input_data']['end_compulsory'], ['class' => 'form-control form-input','placeholder' => '','disabled'=>'disabled']) }}
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row wrap-promotion">
            <div class="col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-2 col-icon hidden-xs"></div>
                        <div class="col-md-7 col-sm-10">
                            <div class="box-promotion">
                                <h3>กรุณากรอกรหัสส่วนลด (ถ้ามี)</h3>
                                {{ Form::text('input_promotion',!empty($init['input_data']['promotion_code'])?$init['input_data']['promotion_code']:'', ['class' => 'form-control form-input detect-change','placeholder' => '']) }}
                                <div class="promotion-icon"><i class="fa" aria-hidden="true"></i></div>
                                <div class="alert alert-danger promotion-alert hide"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="calulate_bar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon1.png') }}" alt="car-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('common.car')</div>
                            <div class="infouser">{{ strtoupper($init['summary']['car']) }}</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon2.png') }}" alt="plan-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('common.plan')</div>
                            <div class="infouser" id="plan_choose">{{ !empty($init['plan_name'])?$init['plan_name']:'' }}</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon3.png') }}" alt="capital-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_insure">@lang('common.ft_si')</div>
                            <div class="infouser" id="plan_price">{{ number_format($init['input_data']['ft_si'],0) }}</div>
                        </div>
                    </li>
                    <li class="fill-active">
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon4.png') }}" alt="pay-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_pay">@lang('common.payment_cost')</div>
                            <div class="infouser" id="plan_pay">{{ !empty($init['payment_result'])?number_format($init['payment_result']):'-' }}</div>
                        </div>
                    </li>
                    <li class="btnrow nextpage fill-active">
                       
                        
                        {!! Form::open(['url' => route('web.insurance_step3_submit'),'name'=>'form_insurance','class'=>'form_insurance','id'=>'form_insurance']) !!}
                        {!! Form::hidden('input_coverage_id',!empty($init['coverage_id'])?$init['coverage_id']:'') !!}

                        @php
                            $defineNameValue = 0;
                            if(!empty($init['input_data']['define_name']) && $init['input_data']['define_name']!="UNNAMED"){
                                $defineNameValue = 1;
                            }else if(isset($init['define_name_amount'])){
                                $defineNameValue = 0;
                            }
                        @endphp

                        {!! Form::hidden('input_define_name',$defineNameValue,['class' => 'detect-change carloss-change hb-change theft-change taxi-change flood-change','id'=>'input_define_name']) !!}
                        @if($init['coverage_id']=='3')
                        {!! Form::hidden('input_define_name_value1','UNNAMED',['class' => 'detect-change','id'=>'input_define_name_value1']) !!}
                        {!! Form::hidden('input_define_name_value2','UNNAMED',['class' => 'detect-change','id'=>'input_define_name_value2']) !!}
                        @endif
                        {!! Form::hidden('input_cctv',$init['input_data']['cctv'],['class' => 'detect-change carloss-change hb-change theft-change taxi-change flood-change','id'=>'input_cctv']) !!}
                        {!! Form::hidden('input_garage',$init['input_data']['garage_type'],['class' => 'detect-change flood-change','id'=>'input_garage']) !!}
                        {!! Form::hidden('input_personal',$init['input_data']['is_personal'],['class' => 'detect-change','id'=>'input_personal']) !!}
                        {!! Form::hidden('input_flood',$init['input_data']['is_flood'],['class' => 'detect-change','id'=>'input_flood']) !!}
                        {!! Form::hidden('input_robbery',$init['input_data']['is_robbery'],['class' => 'detect-change','id'=>'input_robbery']) !!}
                        {!! Form::hidden('input_travel',$init['input_data']['is_travel'],['class' => 'detect-change','id'=>'input_travel']) !!}
                        {!! Form::hidden('input_carloss',$init['input_data']['is_carloss'],['class' => 'detect-change','id'=>'input_carloss']) !!}
                        {!! Form::hidden('input_is_hb',$init['input_data']['is_hb'],['class' => 'detect-change','id'=>'input_is_hb']) !!}
                        {!! Form::hidden('input_prb',$init['input_data']['is_com'],['class' => 'detect-change','id'=>'input_prb']) !!}
                        {!! Form::hidden('hd_max_pre',!empty($init['max_pre'])?$init['max_pre']:0) !!}
                        @if($init['input_data']['is_personal']!=0)
                        <button type="submit" class="btn" id="btn-submit" disabled="disabled">@lang('step3.confirm_buy')</button>
                        @else
                        <button type="submit" class="btn active" id="btn-submit">ซื้อเลย</button>
                        @endif
                        {!! Form::close() !!}
                    </li>
                </ul>
                <a class="show-detail"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                <a class="hidden-detail"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>
@include('web.insurance.modal.custom-alert')
@endsection

@section('javascript')
<script src="{{ asset('component/moment/moment.js') }}"></script>
<script>
$(document).ready(function(){
    moment.locale($("input[name='hd_current_lang']").val());
});
</script>

<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.rule.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/bootstrap-dialog/bootstrap-dialog.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/material-datepicker/js/bootstrap-material-datetimepicker.th.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/loader/loader.web.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/insurance.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/insurance_3.min.js') }}"></script>
@endsection

@section('hidden')
{!! Form::hidden('hd_input_api_hb',route('api.web.hb_gloss_premium')) !!}
{!! Form::hidden('hd_input_api_carloss',route('api.web.carloss_gloss_premium')) !!}
{!! Form::hidden('hd_input_api_taxi',route('api.web.taxi_gloss_premium')) !!}
{!! Form::hidden('hd_input_api_theft',route('api.web.theft_gloss_premium')) !!}
{!! Form::hidden('hd_input_api_flood',route('api.web.flood_gloss_premium')) !!}
{!! Form::hidden('hd_check_promotion_code',route('web.check_promotion_code')) !!}
{!! Form::hidden('hd_calculate_step3',route('web.calculate_modified')) !!}
{!! Form::hidden('hd_lang_alert',trans('step3.alert_topic')) !!}
{!! Form::hidden('hd_lang_garage',trans('step3.alert_garage')) !!}
{!! Form::hidden('hd_lang_only_number',trans('validation.custom.number')) !!}
{!! Form::hidden('hd_lang_error_specify',trans('step3.error_specify')) !!}
{!! Form::hidden('hd_lang_cctv_detai',trans('step3.cctv_detail')) !!}
{!! Form::hidden('hd_lang_excess_detai',trans('step3.excess_detail')) !!}
{!! Form::hidden('hd_lang_invalid_form',trans('step3.invalid_form')) !!}
{!! Form::hidden('hd_check_max_pre',route('api.web.check_max_preorder')) !!}

@endsection