@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('web/css/confirm_policy.min.css') }}">
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="section_confirm">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="wrap-confirm">
                    @if($errors->any())
                    <h4>{{$errors->first()}}</h4>
                    @elseif(session('success'))
                    <h4>{{session('success')}}</h4>
                    @else
                        @if($init['status']=="SUCCESS")
                        <p>ข้าพเจ้า <b>{{ $init['data']['name'] }}</b> ขอยืนยันว่าเป็นบุคคลผู้เอาประกันภัยรถยนต์ภาคสมัครใจฉบับนี้  และยืนยันในการซื้อกรมธรรม์รถยนต์ภาคสมัครใจ</p>
                        <p>ซึ่งข้าพเจ้าได้รับกรมธรรม์เรียบร้อยแล้ว</p>
                        
                        <div style="margin-top:20px;">
                            {{ Form::open(['url' => route("web.confirm_policy_submit")])}}
                            {{ Form::hidden('code',$init['data']['code']) }}
                            <button type="submit" id="submit_policy" class="mat-btn btn">ยืนยันการซื้อกรมธรรม์</button>
                            {{ Form::close() }}
                        </div>
                        <div class="detail">
                            <p>บริษัท ซมโปะ ประกันภัย(ประเทศไทย) จำกัด(มหาชน) ขอขอบคุณท่านที่ไว้วางใจให้บริษัทเป็นผู้ให้ความคุ้มครองท่านสำหรับประกันภัยรถยนต์ครั้งนี้  หากท่านต้องการสอบถามข้อมูลเพิ่มเติมหรือเปลี่ยนแปลงข้อมูลใดๆ กรุณาติดต่อ ศูนย์บริการลูกค้าสัมพันธ์ โทร 02-119-3000 ในเวลาทำการ ทุกวัน เวลา 8.30 – 17.00 หรือ cs@sompo.co.th</p>
                        </div>
                        @else
                        <div class="faild-text">
                            <p>{{ $init['reason'] }}</p>
                        </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('javascript')

@endsection
@section('hidden')

@endsection