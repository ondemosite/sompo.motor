<div class="wrap-popup-alert">
    <div class="box-popup-alert animated">
        <div class="close-popup-alert"><a class="button-close-popup-alert"><i class="fa fa-close"></i></a></div>
        <div class="body-popup-alert">
            <img data-img />
            <div data-detail></div>
        </div>
        <div class="footer-popup-alert">
            <div data-title></div>
        </div>
    </div>
</div>