<style>
#loading{
    position: fixed;
    width: 400px;
    height: 100px;
    margin: 5% auto;
    left: 0;
    right: 0;
    top: 25%;
    border-radius: 5px;
    background-color: #FF4343;
    margin: 0 auto;
    z-index: 1000;
    color: #FFFFFF;
    font-size: 24px;
    text-align: center;
    padding-top: 20px;
    display:none;
}
</style>
<div id="loading">
    <p>@lang('common.verifing') <img src="{{asset('/images/web/icon/loading-verify.gif')}}" width="70px" /></p>
</div>