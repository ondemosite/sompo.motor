<!-- Modal -->
<div class="modal fade bootstrap-dialog" id="otp_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="bootstrap-dialog-title">@lang('step4.otp_header')</div>
            </div>
            <div class="modal-body">
                <h2>@lang('step4.otp_1')</h2>
                <!-- <h3>@lang('step4.otp_2') <span class="otp-to"></span></h3> -->
                <div class="correct-box">
                    <div class="check_mark">
                        <div class="sa-icon sa-success animate">
                            <span class="sa-line sa-tip animateSuccessTip"></span>
                            <span class="sa-line sa-long animateSuccessLong"></span>
                            <div class="sa-placeholder"></div>
                            <div class="sa-fix"></div>
                        </div>
                    </div>
                    <p>@lang('step4.otp_success')</p>
                </div>
                <div class="input-box">
                    <input type="text" pattern="\d*" class="input_otp" name="otp_1" data-index="1" maxlength="1" disabled>
                    <input type="text" pattern="\d*" class="input_otp" name="otp_2" data-index="2" maxlength="1" disabled>
                    <input type="text" pattern="\d*" class="input_otp" name="otp_3" data-index="3" maxlength="1" disabled>
                    <input type="text" pattern="\d*" class="input_otp" name="otp_4" data-index="4" maxlength="1" disabled>
                    <input type="text" pattern="\d*" class="input_otp" name="otp_5" data-index="5" maxlength="1" disabled>
                    <input type="text" pattern="\d*" class="input_otp" name="otp_6" data-index="6" maxlength="1" disabled>
                </div>
                <div class="input-box counting-box">
                    <p>รหัส OTP จะไปถึงภายใน <span id="counting-down"></span> วินาที</p>
                </div>
                <div class="input-box sent-box">
                    <p>@lang('step4.otp_2') <span class="otp-to"></span> แล้ว</p>
                </div>
                <div class="input-box sending-box">
                    <i class="fa fa-circle-o-notch fa-spin"></i>
                    <p>กำลังดำเนินการ</p>
                </div>
                <div class="input-box result-box">
                    <p></p>
                </div>
                <div class="input-box">
                    <div><button type="button" class="btn" id="btn-submit-otp" disabled="disabled">@lang('step4.otp_submit') <i class="fa fa-circle-o-notch fa-spin"></i><span class="counter-submit"><a></a></span></button></div>
                    <div><button type="button" class="btn active" id="btn-resend-otp">ส่งรหัสอีกครั้ง <i class="fa fa-circle-o-notch fa-spin"></i><span class="counter-resend"><a></a></span></button></div>
                    <div><button type="button" class="btn active" data-dismiss="modal" id="btn-cancel-otp">ยกเลิก</button></div>
                </div>
            </div>
        </div>
    </div>
</div>