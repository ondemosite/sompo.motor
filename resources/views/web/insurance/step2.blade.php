@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="{{ asset('web/css/insurance.min.css?v=1.1') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance_2.min.css?v=1.1') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="stepbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="inside-step-bar">
                    <ul>
                        <li class="pass">
                            <a href="{{ route('web.dashboard') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">ข้อมูลรถ</div>
                            </a>
                        </li>
                        <li class="active">
                            <div class="step-circle">2</div>
                            <div class="step-detail">แผนประกัน</div>
                        </li>
                        <li>
                            <div class="step-circle">3</div>
                            <div class="step-detail">ปรับแต่งแผน</div>
                        </li>
                        <li>
                            <div class="step-circle">4</div>
                            <div class="step-detail">ข้อมูลผู้เอาประกัน</div>
                        </li>
                        <li>
                            <div class="step-circle">5</div>
                            <div class="step-detail">ชำระเงิน</div>
                        </li>
                    </ul>
                    <div class="stregth-bar"></div>
                </div>
                
                
            </div>
        </div>
    
  </div> <!--/container -->
</section>

<section id="section-insurance">
    <div class="container">
        @if(!empty($init['notfound']))
        <div class="row row-title-header-danger">
            <div class="col-xs-12"><h1 class="row-title">ไม่พบข้อมูลในระบบ</h1></div>
        </div>
        @else

        <div class="row row-title-header">
            <div class="col-xs-12"><h1 class="row-title">@lang('step2.yourplan')</h1></div>
        </div>
        <div class="row plan-row">
            @if(sizeof($init['plan_info']) > 1)
            <div class="item-car">
                <img src="{{ asset('/images/web/icon/step2-sompony.png') }}">
                <p>เลือกความคุ้มครองนี้</p>
            </div>
            @endif
            <!-- Loop Mobile-->
            
            @foreach($init['plan_info'] as $plan)
            <div class="col-md-4 plan-topic-item plan-topic-item-{{ $loop->index }} {{ $loop->last&&$loop->first?'box-center':'' }}">
                @if(!empty($init['input_ft_si']))
                    <div class="box-plan {{ $plan['ft_si']==$init['input_ft_si']?'active':'' }}"  >
                    <a class="active-tag {{ $plan['ft_si']==$init['input_ft_si']?'active':'' }}" href="#target-{{ $loop->index }}" data-target="target-{{ $loop->index }}" 
                    data-name="{{ $plan['coverage_name'] }}" data-gloss="{{ (!empty($plan['gross_premium'])?number_format($plan['gross_premium'],2):'') }}" 
                    data-fi-form="{{ number_format($plan['ft_si'],0) }}"
                    data-fi="{{ $plan['ft_si'] }}"
                    data-id="{{ $loop->index }}">
                @else
                    <div class="box-plan {{ $loop->last?'active recommend':'' }}"  >
                    <a class="active-tag {{ $loop->last?'active':'' }}" href="#target-{{ $loop->index }}" data-target="target-{{ $loop->index }}" 
                    data-name="{{ $plan['coverage_name'] }}" data-gloss="{{ (!empty($plan['gross_premium'])?number_format($plan['gross_premium'],2):'') }}" 
                    data-fi-form="{{ number_format($plan['ft_si'],0) }}"
                    data-fi="{{ $plan['ft_si'] }}"
                    data-id="{{ $loop->index }}">
                @endif
                    
                        <h1 class="title">{{ number_format($plan['ft_si'],0) }}</h1>
                        <p class="sub-title">ซ่อมอู่ทั่วไป</p>
                        <div class="price">
                            <h1><span>@lang('step2.start')</span>{{ (!empty($plan['gross_premium'])?number_format($plan['gross_premium'],2):'') }}</h1>
                        </div>
                    </a>
                </div>
                @if(!empty($init['input_ft_si']))
                    <div class="box-detail box-detail-tablet {{ $plan['ft_si']==$init['input_ft_si']?'active render':'' }}" data-dest="target-{{ $loop->index }}">
                @else
                     <div class="box-detail box-detail-tablet {{ $loop->last?'active render':'' }}" data-dest="target-{{ $loop->index }}">
                @endif
                    <div class="box-detail-header">
                        <span><img src="{{ asset('/images/web/icon/iconplan.svg') }}" class="svg" alt=""></span>
                        <span><p class="title">{{ number_format($plan['ft_si'],0) }}</p></span>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-detail-box">
                        <p class="text-detail">{{ $plan['garage_type']=="GENERAL"?'ซ่อมอู่ทั่วไป':'ซ่อมอู่แนะนำ' }}</p>
                        <p class="text-detail">ไม่ระบุผู้ขับขี่</p>
                        <p class="text-detail">ไม่มีค่าความรับผิดส่วนแรก</p>
                    </div>
                    <div class="box-detail-box">
                        <p class="text-tag">@lang('step2.owner_damage')</p>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.accident_have') <span class="text-italic">(@lang('step2.car_crash'))</span></p>
                            <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.car_rob')</p>
                            <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.car_fire')</p>
                            <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <!-- <div class="detail-row">
                            <p class="text-detail">@lang('step2.car_terrorism')</p>
                            <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                        </div> -->
                        @if($plan['coverage_id']!=3)
                        <!-- <div class="detail-row">
                            <p class="text-detail">@lang('step2.24hour') <span class="text-italic">(@lang('step2.so_accident'))</span></p>
                            <p class="text-price">@lang('step2.20_percent_fix')</p>
                        </div> -->
                        @endif
                    </div>
                    <div class="box-detail-box">
                        <p class="text-tag">@lang('step2.garanty_out')</p>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.garanty_stuff')</p>
                            <p class="text-price">{{ (!empty($init['coverage']['stuff_damage'])?number_format($init['coverage']['stuff_damage'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <!-- <div class="detail-row">
                            <p class="text-detail">@lang('step2.garanty_stuff') (@lang('step2.per_once'))</p>
                            <p class="text-price">{{ (!empty($init['coverage']['person_damage_once'])?number_format($init['coverage']['person_damage_once'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.death_disable') (@lang('step2.per_person'))</p>
                            <p class="text-price">{{ (!empty($init['coverage']['person_damage_person'])?number_format($init['coverage']['person_damage_person'],0):'') }} @lang('common.baht')</p>
                        </div> -->
                        <div class="detail-row">
                            <p class="text-detail">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (ต่อครั้ง)</p>
                            <p class="text-price">{{ (!empty($init['coverage']['person_damage_once'])?number_format($init['coverage']['person_damage_once'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <div class="detail-row">
                            <p class="text-detail">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (ต่อคน)</p>
                            <p class="text-price">{{ (!empty($init['coverage']['person_damage_person'])?number_format($init['coverage']['person_damage_person'],0):'') }} @lang('common.baht')</p>
                        </div>
                    </div>
                    <div class="box-detail-box">
                        <p class="text-tag">@lang('step2.coverage_addition')</p>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.private_accident')</p>
                            <p class="text-price">{{ (!empty($init['coverage']['death_disabled'])?number_format($init['coverage']['death_disabled'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.medical_fee')</p>
                            <p class="text-price">{{ (!empty($init['coverage']['medical_fee'])?number_format($init['coverage']['medical_fee'],0):'') }} @lang('common.baht')</p>
                        </div>
                        <div class="detail-row">
                            <p class="text-detail">@lang('step2.bail_driver') (@lang('step2.per_once'))</p>
                            <p class="text-price">{{ (!empty($init['coverage']['bail_driver'])?number_format($init['coverage']['bail_driver'],0):'') }} @lang('common.baht')</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- /Loop -->
            
        </div> <!-- /row -->

        <div class="wrap-detail-desktop">
            <div class="row">
                <div class="col-xs-12"><h1 class="row-title">@lang('step2.cover_detail')</h1></div>
            </div>
            <div class="row detail-row">
                @foreach($init['plan_info'] as $plan)
                <div class="col-md-4 wrap-box-detail {{ $loop->last&&$loop->first?'box-center':'' }}" data-id="{{ $loop->index }}">
                @if(!empty($init['input_ft_si']))
                    <div class="box-detail box-detail-desktop {{ $plan['ft_si']==$init['input_ft_si']?'active render':'' }}" id="target-{{ $loop->index }}">
                @else
                    <div class="box-detail box-detail-desktop {{ $loop->last?'active':'' }}" id="target-{{ $loop->index }}">
                @endif
                        <div class="box-detail-header">
                            <span><img src="{{ asset('/images/web/icon/iconplan.svg') }}" class="svg" alt=""></span>
                            <span><p class="title">{{ number_format($plan['ft_si'],0) }}</p></span>
                            <div style="clear:both"></div>
                        </div>
                        <div class="box-detail-box">
                            <p class="text-detail">{{ $plan['garage_type']=="GENERAL"?'ซ่อมอู่ทั่วไป':'ซ่อมอู่แนะนำ' }}</p>
                            <p class="text-detail">ไม่ระบุผู้ขับขี่</p>
                            <p class="text-detail">ไม่มีค่าความรับผิดส่วนแรก</p>
                        </div>
                        <div class="box-detail-box">
                            <p class="text-tag">@lang('step2.owner_damage')</p>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.accident_have') <span class="text-italic">(@lang('step2.car_crash'))</span></p>
                                <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.car_rob')</p>
                                <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.car_fire')</p>
                                <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <!-- <div class="detail-row">
                                <p class="text-detail">@lang('step2.car_terrorism')</p>
                                <p class="text-price">{{ (!empty($plan['ft_si'])?number_format($plan['ft_si'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.24hour') <span class="text-italic">(@lang('step2.so_accident'))</span></p>
                                <p class="text-price">@lang('step2.20_percent_fix')</p>
                            </div> -->
                        </div>
                        <div class="box-detail-box">
                            <p class="text-tag">@lang('step2.garanty_out')</p>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.garanty_stuff')</p>
                                <p class="text-price">{{ (!empty($init['coverage']['stuff_damage'])?number_format($init['coverage']['stuff_damage'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <!-- <div class="detail-row">
                                <p class="text-detail">@lang('step2.garanty_stuff') (@lang('step2.per_once'))</p>
                                <p class="text-price">{{ (!empty($init['coverage']['person_damage_once'])?number_format($init['coverage']['person_damage_once'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.death_disable') (@lang('step2.per_person'))</p>
                                <p class="text-price">{{ (!empty($init['coverage']['person_damage_person'])?number_format($init['coverage']['person_damage_person'],0):'') }} @lang('common.baht')</p>
                            </div> -->
                            <div class="detail-row">
                                <p class="text-detail">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย <br/>(ต่อครั้ง)</p>
                                <p class="text-price">{{ (!empty($init['coverage']['person_damage_once'])?number_format($init['coverage']['person_damage_once'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <div class="detail-row">
                                <p class="text-detail">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย <br/>(ต่อคน)</p>
                                <p class="text-price">{{ (!empty($init['coverage']['person_damage_person'])?number_format($init['coverage']['person_damage_person'],0):'') }} @lang('common.baht')</p>
                            </div>

                        </div>
                        <div class="box-detail-box">
                            <p class="text-tag">@lang('step2.coverage_addition')</p>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.private_accident')</p>
                                <p class="text-price">{{ (!empty($init['coverage']['death_disabled'])?number_format($init['coverage']['death_disabled'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.medical_fee')</p>
                                <p class="text-price">{{ (!empty($init['coverage']['medical_fee'])?number_format($init['coverage']['medical_fee'],0):'') }} @lang('common.baht')</p>
                            </div>
                            <div class="detail-row">
                                <p class="text-detail">@lang('step2.bail_driver') (@lang('step2.per_once'))</p>
                                <p class="text-price">{{ (!empty($init['coverage']['bail_driver'])?number_format($init['coverage']['bail_driver'],0):'') }} @lang('common.baht')</p>
                            </div>
                        </div>
                    </div> <!-- /box-detail -->
                </div> <!-- /col-md-4 -->
                @endforeach
            </div>
        </div><!--/wrap-detail-desktop-->
        
        <div class="wrap-condition">
            <div class="row">
                <div class="col-sm-12"><p class="text-condition-header">ข้อกำหนดและเงื่อนไขการรับประกันภัย</p></div>
                <div class="col-sm-12"><p class="text-condition">1. เบี้ยประกันภัยนี้เป็นเบี้ยสำหรับรถใช้ส่วนบุคคล ไม่ใช้รับจ้างหรือให้เช่า</p></div>
                <div class="col-sm-12"><p class="text-condition">2. เบี้ยประกันภัยข้างต้นรวมส่วนลดกล้อง CCTV และไม่สามารถให้ส่วนลดอื่นๆ</p></div>
            </div>
        </div>
        
        @endif

    </div><!--/container-->
</section>

@if(empty($init['notfound']))
<section id="calulate_bar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon1.png') }}" alt="car-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('step2.car')</div>
                            <div class="infouser"><a title="{{ strtoupper($init['summary']['car']) }}">{{ strtoupper($init['summary']['car']) }}</a></div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon2.png') }}" alt="plan-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('step2.plan')</div>
                            <div class="infouser" id="plan_choose">SOMPO 2+</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon3.png') }}" alt="capital-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_insure">@lang('step2.begin_insurance')</div>
                            <div class="infouser" id="plan_price"></div>
                        </div>
                    </li>
                    <li class="fill-active">
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon4.png') }}" alt="pay-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_pay">@lang('step2.begin_payment')</div>
                            <div class="infouser" id="plan_pay"></div>
                        </div>
                    </li>
                    <li class="btnrow nextpage fill-active">
                        {!! Form::open(['url' => route('web.insurance_step2_submit'),'id'=>'form_submit']) !!}
                        {!! Form::hidden('input_ft_si','') !!}
                        <button type="submit" class="btn active">ซื้อแผนนี้</button>
                        {!! Form::close() !!}
                    </li>
                </ul>
                <a class="show-detail"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                <a class="hidden-detail"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>
@endif

@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.rule.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/insurance.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/insurance_2.min.js') }}"></script>
@endsection

@section('hidden')

@endsection