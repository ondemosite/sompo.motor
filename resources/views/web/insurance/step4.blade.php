@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link href="{{ asset('component/bootstrap-dialog/bootstrap-dialog.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('component/material-datepicker/css/bootstrap-material-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('component/checkbox-nojs/checkbox-nojs.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('component/lightbox2-master/src/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance.min.css?v=1.1') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance_4.min.css?v=1') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="stepbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="inside-step-bar">
                    <ul>
                        <li class="pass">
                            <a href="{{ route('web.dashboard') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">ข้อมูลรถ</div>
                            </a>
                        </li>
                        <li class="pass">
                            <a href="{{ route('web.insurance_step2') }}" class="back-action">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">แผนประกัน</div>
                            </a>
                        </li>
                        <li class="pass">
                            <a href="{{ route('web.insurance_step3') }}" class="back-action">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">ปรับแต่งแผน</div>
                            </a>
                        </li>
                        <li class="active">
                            <div class="step-circle">4</div>
                            <div class="step-detail">ข้อมูลผู้เอาประกัน</div>
                        </li>
                        <li>
                            <div class="step-circle">5</div>
                            <div class="step-detail">ชำระเงิน</div>
                        </li>
                    </ul>
                    <div class="stregth-bar"></div>
                </div>
            </div>
        </div>
  </div> <!--/container -->
</section>
<section id="section-insurance">
{!! Form::open(['route'=>'web.insurance_step4_submit','files'=>true,'name'=>'form_insurance','class'=>'form_insurance','id'=>'form_insurance']) !!}
    <div class="container">
        <div class="row">
            <div class="col-md-5 wrap-summary">
                <div class="box-summary">
                    <div class="summary-header"><h1>@lang('step4.summary_insurance')</h1></div>
                    <div class="summary-body">
                        <div class="summary-form">
                            <div class="row">
                                <div class="col-xs-9">
                                    <div class="row form-title">
                                        <div class="col-xs-12"><h1>@lang('step4.car')</h1></div>
                                    </div>
                                    <div class="row form-detail">
                                        <table class="form-detail-table" width="100%" border="0">
                                            <tr>
                                                <td valign="top"><p>{{ $init['summary']['car_brand'] }} / {{ $init['summary']['car_model'] }} / {{ $init['summary']['car_year'] }}</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="item-car"><img src="{{ asset('/images/web/icon/Sompony_Step4.png') }}" /></div>
                                </div>
                            </div>
                        </div><!--/summary-form -->
                        <div class="summary-form">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row form-title">
                                        <div class="col-xs-12"><h1>@lang('step4.driver')</h1></div>
                                    </div>
                                    <div class="row form-detail">
                                        <table class="form-detail-table" width="100%" border="0">
                                            <tr>
                                                <td width="70%" valign="top"><p>@lang('step4.driver_amount')</p></td>
                                                <td valign="top" class="value"><p>{{ ($init['summary']['driver_amount']==0?"ไม่ระบุ":$init['summary']['driver_amount']." ".trans('step4.person')) }}</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div><!--/summary-form -->
                        <div class="summary-form">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row form-title">
                                        <div class="col-xs-12"><h1>@lang('step4.main_plan')</h1></div>
                                    </div>
                                    <div class="row form-detail">
                                        <table class="form-detail-table" width="100%" border="0">
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>@lang('step4.type')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ $init['summary']['plan_name'] }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>@lang('step4.insurance_start')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ $init['summary']['start_insurance'] }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>หมดอายุ</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ $init['summary']['end_insurance'] }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>@lang('step4.garage_type')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ $init['summary']['garage_type']=="DEALER"?(trans('common.garage_dealer')):($init['summary']['garage_type']=="GENERAL"?trans('common.garage_general'):"-") }}</p></td>
                                            </tr>
                                            @if($init['summary']['plan']!=3)
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>@lang('step4.sum_insured')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ number_format($init['summary']['ft_si'],0) }} @lang('common.baht')</p></td>
                                            </tr>
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>ความเสียหายต่อรถยนต์จากการชน</p></td>
                                                <td valign="top" class="value br-bt"><p>-</p></td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>@lang('step4.sum_insured')</p></td>
                                                <td valign="top" class="value br-bt"><p>-</p></td>
                                            </tr>
                                            <tr>
                                                <td width="60%" valign="top" class="br-bt"><p>จำนวนคุ้มครองการชน</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ number_format($init['summary']['od_si'],0) }} @lang('common.baht')</p></td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td width="60%" valign="top"><p>@lang('step4.excess')</p></td>
                                                <td valign="top" class="value"><p>{{ !empty($init['summary']['deductible'])?number_format($init['summary']['deductible'],0).trans('common.baht'):'-' }}</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div><!--/summary-form -->
                        <div class="summary-form">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row form-title">
                                        <div class="col-xs-12"><h1>@lang('step4.addition_insurance')</h1></div>
                                    </div>
                                    <div class="row form-detail">
                                        <table class="form-detail-table" width="100%" border="0">
                                            <!-- <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.flood_coverage')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary']['flood_cost'])?(number_format($init['summary']['flood_cost'],0)." ".trans('common.baht')):("-") }}</p></td>
                                            </tr> -->
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.robbery') <br/>@lang('step4.inside_car_cost')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary']['theft_cost'])?(number_format($init['summary']['theft_cost'],0)." ".trans('common.baht')):("-") }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.taxi_coverage') <br/>@lang('step4.peronce_cost')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary']['taxi_cost'])?number_format($init['summary']['taxi_cost'],0)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.hb_coverage')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary']['hb_cost'])?number_format($init['summary']['hb_cost'],0)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top"><p>@lang('step4.carloss_coverage')</p></td>
                                                <td valign="top" class="value"><p>{{ !empty($init['summary']['carloss_cost'])?number_format($init['summary']['carloss_cost'],0)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div><!--/summary-form -->
                        <div class="summary-form">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row form-title">
                                        <div class="col-xs-12"><h1>@lang('step4.summary_gross')</h1></div>
                                    </div>
                                    <div class="row form-detail">
                                        <table class="form-detail-table" width="100%" border="0">
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>{{ $init['summary']['plan_name'] }}</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ number_format($init['summary_gross']['insurance'],2) }} @lang('common.baht')</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.compulsory')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary_gross']['compulsory'])?number_format($init['summary_gross']['compulsory'],2)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                            <!-- <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.flood_coverage')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary_gross']['flood'])?number_format($init['summary_gross']['flood'],2)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr> -->
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.robbery_short')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary_gross']['theft_gross_premium'])?number_format($init['summary_gross']['theft_gross_premium'],2)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.taxi_coverage')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary_gross']['taxi_gross_premium'])?number_format($init['summary_gross']['taxi_gross_premium'],2)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.hb_coverage')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary_gross']['hb_gross_premium'])?number_format($init['summary_gross']['hb_gross_premium'],2)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top" class="br-bt"><p>@lang('step4.carloss_coverage')</p></td>
                                                <td valign="top" class="value br-bt"><p>{{ !empty($init['summary_gross']['carloss_gross_premium'])?number_format($init['summary_gross']['carloss_gross_premium'],2)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                            <tr>
                                                <td width="70%" valign="top"><p>@lang('step4.promotion_cost')</p></td>
                                                <td valign="top" class="value"><p>{{ !empty($init['summary_gross']['discount'])?number_format($init['summary_gross']['discount'],2)." ".trans('common.baht'):"-" }}</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div><!--/summary-form -->
                        <div class="summary-form footer">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row form-detail">
                                        <table class="form-detail-table" width="100%" border="0">
                                            <tr>
                                                <td width="70%" valign="top"><p>@lang('step4.total_amount')</p></td>
                                                <td valign="top" class="value"><p>{{ number_format($init['summary_gross']['payment_result'],2) }} @lang('common.baht')</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div><!-- summary-body -->
                </div>
            </div>
            <div class="col-md-6 wrap-detail">
                <div class="form-header">
                    <h1 class="text-header-form">@lang('step4.insurance_detail')</h1>
                    <img class="image-header-form" src="{{ asset('/images/web/icon/step4-1.png') }}" />
                </div>
                <div class="form-sub-header"><h1 class="text-sub-header-form">@lang('step4.owner_insurance')</h1></div>
                <div class="row-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-select-gray">
                                <label for="input_info_title" >คำนำหน้า</label>
                                @php
                                    $prefixDefault = '';
                                    if(!empty($init['inf']['info_main_prefix'])){
                                        $prefixDefault = $init['inf']['info_main_prefix'];
                                    }else{
                                        if(!empty($init['prefix_default'])){
                                            $prefixDefault = $init['prefix_default']->id;
                                        }
                                    }
                                @endphp
                                <select name="input_info_title" class="form-control select2-local">
                                    <option selected="selected" value="">เลือกคำนำหน้า</option>
                                    @foreach($init['prefix'] as $index => $value)
                                    <option value="{{ $index }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                               
                                {{ Form::hidden('input_info_title_inf',!empty($init['inf']['info_main_prefix'])?$init['inf']['info_main_prefix']:'') }}
                                <div class="warning-text gender-text hide">* กรุณาเลือกคำนำหน้า</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="input_info_name">@lang('common.name')</label>
                                {{ Form::text('input_info_name',!empty($init['inf']['info_main_name'])?$init['inf']['info_main_name']:'', ['class' => 'form-control form-input input-gray on-focus-out','placeholder' => '']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">@lang('common.lastname')</label>
                                {{ Form::text('input_info_lastname',!empty($init['inf']['info_main_lastname'])?$init['inf']['info_main_lastname']:'', ['class' => 'form-control form-input input-gray on-focus-out','placeholder' => '']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">@lang('common.idcard')</label>
                                {{ Form::text('input_info_id',!empty($init['inf']['info_main_id_card'])?$init['inf']['info_main_id_card']:'', ['class' => 'form-control form-input input-gray on-focus-out','placeholder' => '','maxlength'=>13]) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group no-margin">
                                <label>@lang('common.sex')</label>
                                <div class="error_info_gender"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row row_focus_gender">
                        {{ Form::hidden('input_info_gender',!empty($init['inf']['info_main_gender'])?$init['inf']['info_main_gender']:'', ['id' => 'input_info_gender','class' => 'on-change-value']) }}
                        <div class="col-xs-6">
                            <div class="form-group">
                                @php
                                    $main_male = "";
                                    if(!empty($init['inf']['info_main_gender'])){
                                        $main_male = $init['inf']['info_main_gender'];
                                    }
                                @endphp
                                <label class="select-radio {{ $main_male=='MALE'?'active':'' }}" data-type="show" data-group="info_gender" data-for="input_info_gender" data-value="MALE">@lang('common.male')</label>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="select-radio {{ $main_male=='FEMALE'?'active':'' }}" data-type="hidden" data-group="info_gender" data-for="input_info_gender" data-value="FEMALE">@lang('common.female')</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" >@lang('common.birthdate')</label>
                                {{ Form::text('input_info_birth',!empty($init['inf']['info_main_birthdate'])?$init['inf']['info_main_birthdate']:'', ['class' => 'form-control form-input input-gray datepicker-main on-change-value','placeholder' => trans('placeholder.select_birthdate')]) }}
                                <span class="add-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ข้อมูลสำหรับติดต่อ -->
                <div class="form-header">
                    <h1 class="text-header-form">@lang('step4.contact_date')</h1>
                    <img class="image-header-form" src="{{ asset('/images/web/icon/step4-2.png') }}" />
                </div>
                <div class="form-sub-header"><h1 class="text-sub-header-form">@lang('step4.currect_address')</h1></div>
                <div class="row-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">@lang('common.address')</label>
                                {{ Form::text('input_info_address',!empty($init['inf']['info_main_address'])?$init['inf']['info_main_address']:'', ['class' => 'form-control form-input input-gray on-focus-out','placeholder' => '']) }}
                                <div class="warning-text">* กรุณากรอกที่อยู่เป็นภาษาไทยเท่านั้น</div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-select-gray">
                                <label for="" >@lang('common.province')</label>
                                {{ Form::select('input_info_province',!empty($init['province'])?$init['province']:[],!empty($init['inf']['info_main_province'])?$init['inf']['info_main_province']:'',['class' => 'form-control select2-local','placeholder' => trans('placeholder.select_province')]) }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-select-gray">
                                <label for="" >@lang('common.district')</label>
                                {{ Form::select('input_info_district',[],null,['class' => 'form-control select2-local','placeholder' => trans('placeholder.select_district')]) }}
                                {{ Form::hidden('input_info_district_inf',!empty($init['inf']['info_main_district'])?$init['inf']['info_main_district']:'') }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-select-gray">
                                <label for="" >@lang('common.sub_district')</label>
                                {{ Form::select('input_info_subdistrict',[],null,['class' => 'form-control select2-local on-change-value','placeholder' => trans('placeholder.select_sub_district')]) }}
                                {{ Form::hidden('input_info_subdistrict_inf',!empty($init['inf']['info_main_subdistrict'])?$init['inf']['info_main_subdistrict']:'') }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">@lang('common.zip_code')</label>
                                {{ Form::text('input_info_code',!empty($init['inf']['info_main_postcode'])?$init['inf']['info_main_postcode']:'', ['class' => 'form-control form-input input-gray on-focus-out','placeholder' => '']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">@lang('common.mobile_phone')</label>
                                {{ Form::text('input_info_tel',!empty($init['inf']['info_main_tel'])?$init['inf']['info_main_tel']:'', ['class' => 'form-control form-input input-gray on-focus-out','placeholder' => '','maxlength'=>'10']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">@lang('common.email')</label>
                                {{ Form::text('input_info_email',!empty($init['inf']['info_main_email'])?$init['inf']['info_main_email']:'', ['class' => 'form-control form-input input-gray on-focus-out','placeholder' => '']) }}
                            </div>
                        </div>
                    </div>
                </div><!-- row-form-->
            </div>
        </div>
    </div> <!-- container -->
    <div class="container-fluid">
        <div class="row wrap-driver">
            <div class="col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 wrap-driver-detail">
                            @if($init['summary']['plan']!=3 && $init['summary']['driver_amount']>0)
                            <div class="form-header">
                                <h1 class="text-header-form m-b-20">@lang('step4.driver_information')</h1>
                                <img class="image-header-form" src="{{ asset('/images/web/icon/itemcar-insurance-2.png') }}" />
                            </div>
                            <div class="row-form">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group no-margin">
                                            <label for="" >@lang('step4.is_main_owner')</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            @php
                                            $is_main = 0;
                                            if(!empty($init['inf']['info_is_main_driver'])){
                                                $is_main = $init['inf']['info_is_main_driver'];
                                            }
                                            @endphp
                                            <label class="select-radio {{ $is_main==1?'active':'' }}" data-group="is-main-driver" data-value="1">@lang('common.yes')</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="select-radio {{ $is_main==0?'active':'' }}" data-group="is-main-driver" data-value="0">@lang('common.no')</label>
                                        </div>
                                    </div>
                                    {{ Form::hidden('input_is_main_driver',$is_main) }}
                                </div>
                            </div><!-- /row-form -->
                            @endif

                            @if($init['summary']['driver_amount']>0)
                            {{ Form::hidden('driver1_age_value',!empty($init['summary']['driver1_age_val'])?$init['summary']['driver1_age_val']:null) }}
                            <div class="box-driver-info">
                                <div class="form-header">
                                    <h1 class="text-header-form m-b-20">@lang('step4.driver1_data')</h1>
                                </div>
                                <div class="row-form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.name')</label>
                                                {{ Form::text('input_driver1_name',!empty($init['inf']['info_driver1_name'])?$init['inf']['info_driver1_name']:'', ['class' => 'form-control form-input','placeholder' => '']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.lastname')</label>
                                                {{ Form::text('input_driver1_lastname',!empty($init['inf']['info_driver1_lastname'])?$init['inf']['info_driver1_lastname']:'', ['class' => 'form-control form-input','placeholder' => '']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.idcard')</label>
                                                {{ Form::text('input_driver1_id',!empty($init['inf']['info_driver1_id_card'])?$init['inf']['info_driver1_id_card']:'', ['class' => 'form-control form-input','placeholder' => '','maxlength'=>13]) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.driver_licence')</label>
                                                {{ Form::text('input_driver1_licence',!empty($init['inf']['info_driver1_licence'])?$init['inf']['info_driver1_licence']:'', ['class' => 'form-control form-input','placeholder' => '','maxlength'=>20]) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group no-margin">
                                                <label>@lang('common.sex')</label>
                                                <div class="error_driver1_gender"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_focus_gender_1">
                                        {{ Form::hidden('input_driver1_gender',!empty($init['inf']['info_driver1_gender'])?$init['inf']['info_driver1_gender']:'',['id'=>'driver1_gender']) }}
                                        @php
                                            $driver1_male = "";
                                            if(!empty($init['inf']['info_driver1_gender'])){
                                                $driver1_male = $init['inf']['info_driver1_gender'];
                                            }
                                        @endphp
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label class="select-radio {{ $driver1_male=='MALE'?'active':'' }}" data-group="driver1_gender" data-for="driver1_gender" data-value="MALE">@lang('common.male')</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label class="select-radio {{ $driver1_male=='FEMALE'?'active':'' }}" data-group="driver1_gender" data-for="driver1_gender" data-value="FEMALE">@lang('common.female')</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" >@lang('common.birthdate')</label>
                                                {{ Form::text('input_driver1_birth',!empty($init['inf']['info_driver1_birthdate'])?$init['inf']['info_driver1_birthdate']:'', ['class' => 'form-control form-input datepicker-driver1','placeholder' =>  trans('placeholder.select_birthdate') ]) }}
                                                <span class="add-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                <label for="input_driver1_birth" class="error-text birth-alert">
                                                    * กรณีผู้ขับขี่หลัก คือเจ้าของกรมธรรม์</br> กรุณาตรวจสอบวันเกิดให้ตรงกับข้อมูลผู้เอาประกันภัย
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /row-form -->
                            </div>
                            @endif
                            @if($init['summary']['driver_amount']>1)
                            {{ Form::hidden('driver2_age_value',!empty($init['summary']['driver2_age_val'])?$init['summary']['driver2_age_val']:null) }}
                            <div class="box-driver-info">
                                <div class="form-header">
                                    <h1 class="text-header-form m-b-20">@lang('step4.driver2_data')</h1>
                                </div>
                                <div class="row-form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.name')</label>
                                                {{ Form::text('input_driver2_name',!empty($init['inf']['info_driver2_name'])?$init['inf']['info_driver2_name']:'', ['class' => 'form-control form-input','placeholder' => '']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.lastname')</label>
                                                {{ Form::text('input_driver2_lastname',!empty($init['inf']['info_driver2_lastname'])?$init['inf']['info_driver2_lastname']:'', ['class' => 'form-control form-input','placeholder' => '']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.idcard')</label>
                                                {{ Form::text('input_driver2_id',!empty($init['inf']['info_driver2_id_card'])?$init['inf']['info_driver2_id_card']:'', ['class' => 'form-control form-input','placeholder' => '','maxlength'=>13]) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">@lang('common.driver_licence')</label>
                                                {{ Form::text('input_driver2_licence',!empty($init['inf']['info_driver2_licence'])?$init['inf']['info_driver2_licence']:'', ['class' => 'form-control form-input','placeholder' => '','maxlength'=>'20']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group no-margin">
                                                <label>@lang('common.sex')</label>
                                                <div class="error_driver2_gender"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_focus_gender_2">
                                        {{ Form::hidden('input_driver2_gender',!empty($init['inf']['info_driver2_gender'])?$init['inf']['info_driver2_gender']:'',['id'=>'driver2_gender']) }}
                                        @php
                                            $driver2_male = "";
                                            if(!empty($init['inf']['info_driver2_gender'])){
                                                $driver2_male = $init['inf']['info_driver2_gender'];
                                            }
                                        @endphp
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label class="select-radio {{ $driver2_male=='MALE'?'ACTIVE':'' }}" data-group="driver2_gender" data-for="driver2_gender" data-value="MALE">@lang('common.male')</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label class="select-radio {{ $driver2_male=='FEMALE'?'ACTIVE':'' }}" data-group="driver2_gender" data-for="driver2_gender" data-value="FEMALE">@lang('common.female')</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" >@lang('common.birthdate')</label>
                                                {{ Form::text('input_driver2_birth',!empty($init['inf']['info_driver2_birthdate'])?$init['inf']['info_driver2_birthdate']:'', ['class' => 'form-control form-input datepicker-driver2','placeholder' => trans('placeholder.select_birthdate')]) }}
                                                <span class="add-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /row-form -->
                            </div>
                            @endif

                            <div class="form-header">
                                <h1 class="text-header-form m-b-20">@lang('step4.car_data')</h1>
                            </div>
                            <div class="row-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::text('input_info_car',!empty($init['summary']['full_car'])?$init['summary']['full_car']:null, ['class' => 'form-control form-input input-text-red input-readonly','placeholder' => '','readonly'=>'readonly']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">@lang('step4.prefix_car_licence')</label>
                                            {{ Form::text('input_info_prefix',!empty($init['summary']['car_info_prefix'])?$init['summary']['car_info_prefix']:'', ['class' => 'form-control form-input on-focus-out','placeholder' => 'กข','maxlength'=>'3']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">@lang('common.car_licence')</label>
                                            {{ Form::text('input_info_licence',!empty($init['summary']['car_info_licence'])?$init['summary']['car_info_licence']:'', ['class' => 'form-control form-input on-focus-out','placeholder' => '9999','maxlength'=>'10']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" >@lang('common.province')</label>
                                            {{ Form::select('input_info_car_province',!empty($init['province'])?$init['province']:null,!empty($init['summary']['car_info_province'])?$init['summary']['car_info_province']:'',['class' => 'form-control select2-local on-change-value','placeholder' => trans('placeholder.select_province')]) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">@lang('step4.chassis_number')</label>
                                            {{ Form::text('input_info_car_number',!empty($init['inf']['info_car_chassis'])?$init['inf']['info_car_chassis']:'', ['class' => 'form-control form-input on-focus-out','placeholder' => '','maxlength'=>20]) }}
                                            <p class="sub-detail"><small>@lang('step4.company_may_require')</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /row-form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container-fluid -->
    <div class="container-fluid">
        <div class="row wrap-addition">
            <div class="col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 wrap-addition-detail">
                            <div class="form-header">
                                <h1 class="text-header-form m-b-20">@lang('step4.document')</h1>
                                <img class="image-header-form" src="{{ asset('/images/web/icon/step4-3.png') }}" />
                            </div>
                            <div class="row-form">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="" >@lang('step4.doc_id_card')</label>
                                            @if(!empty($init['inf']['document']['personal_id']))
                                            <div class="box-doc">
                                                <a href="{{ $init['inf']['document']['personal_id'] }}" target="_blank"> @lang('step4.last_upload') <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                            </div>
                                            @endif
                                            <div class="input-group input-file on-file-change" data-name="input_doc_id_val">
                                                {{ Form::text('input_doc_id','', ['class' => 'form-control form-input input-gray','placeholder' => 'Choose a file...']) }}   		
                                                {{ Form::hidden('input_doc_id_check',!empty($init['inf']['info_path_personal_id'])?$init['inf']['info_path_personal_id']:'',['class' => 'on-file-change']) }}
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-choose" type="button">@lang('step4.Browse')</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/wrap-addition-detail -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row-form">
                                @php
                                    $is_post = "NO";
                                    $is_email = "YES";
                                    $is_disclosure = "YES";
                                    $is_advice = "NO";
                                    $is_post_disable = false;
                                    
                                    if(!empty($init['inf']['info_is_post'])){
                                        $is_post = $init['inf']['info_is_post'];
                                    }
                                    if(!empty($init['inf']['info_is_email'])){
                                        $is_email = $init['inf']['info_is_email'];
                                    }
                                    if(!empty($init['inf']['info_is_disclosure'])){
                                        $is_disclosure = $init['inf']['info_is_disclosure'];
                                    }
                                    if(!empty($init['inf']['info_is_advice'])){
                                        $is_advice = $init['inf']['info_is_advice'];
                                    }

                                @endphp
                                            
                                <div class="row row_focus_personal">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                            <input type="checkbox" class="on-change-value" name="is_personal" checked>
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span class="tx">เบี้ยประกันภัยนี้เป็นเบี้ยสำหรับรถใช้ส่วนบุคคล ไม่ใช้รับจ้างหรือให้เช่า</span>
                                            <span class="tx"><div class="error_is_personal"></div></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row_focus_policy_channel">
                                    <div class="col-xs-12">
                                        <label>ช่องทางการรับกรมธรรม์ประกันภัย</label>
                                    </div>
                                    <div class="col-xs-12 padLeft">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="on-change-value" name="is_email" {{ $is_email=="YES"?"CHECKED":"" }}>
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                <span class="tx">ผ่านจดหมายอิเล็กทรอนิกส์ (E-Mail)</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 padLeft">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="on-change-value" name="is_post" {{ $is_post_disable?'disabled':'' }} {{ $is_post=="YES"?"CHECKED":"" }}>
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                <span class="tx">ผ่านช่องทางไปรษณีย์ <br/>พรบ.จะทำการจัดส่งให้ทางไปรษณีย์เนื่องจากต้องใช้กระดาษ HOLOGRAM ในการต่อภาษีเท่านั้น</span>
                                            </label>
                                        </div>
                                        <input type="hidden" name="is_post_disable" value="{{ $is_post_disable?'TRUE':'FALSE' }}">
                                    </div>
                                    <div class="col-xs-12">
                                        <span class="tx"><div class="error_is_policy_channel"></div></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                            <input type="checkbox" class="on-change-value" name="is_disclosure" {{ $is_disclosure=="YES"?"CHECKED":"" }}>
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span class="tx">ข้าพเจ้ายินยอมให้ บริษัทฯ จัดเก็บ ใช้ และเปิดเผยข้อเท็จจริงเกี่ยวกับข้อมูลของข้าพเจ้าต่อสำนักงานคณะกรรมการกำกับและส่งเสริมการประกอบธุรกิจประกันภัยเพื่อประโยชน์ในการกำกับดูแลธุรกิจประกันภัย</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row_focus_true">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                            <input type="checkbox" class="on-change-value" name="is_true" checked>
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span class="tx">ข้าพเจ้าขอรับรองว่า คำบอกกล่าวตามรายการข้างบนเป็นความจริงและให้ถือเป็นส่วนหนึ่งของสัญญาระหว่างข้าพเจ้ากับบริษัท</span>
                                            <span class="tx"><div class="error_is_true"></div></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                            <input type="checkbox" class="on-change-value" name="is_advice" {{ $is_advice=="YES"?"CHECKED":"" }}>
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span class="tx">ข้าพเจ้ายินยอมให้บริษัทใช้ข้อมูลของข้าพเจ้าตามเอกสารนี้ เพื่อวัตถุประสงค์ในการแนะนำผลิตภัณฑ์อื่น และส่งข้อมูลข่าวสารเกี่ยวกับการนำเสนอผลิตภัณฑ์ที่น่าสนใจและเหมาะสมกับข้าพเจ้าในอนาคต</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row_focus_agree">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                            <input type="checkbox" class="on-change-value" name="is_agree" checked>
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span class="tx"><div class="error_is_agree"></div>@lang('step4.must_agree')</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-alert">
                                    <div class="col-xs-12">
                                        <span class="tx"><span class="input-text-red">* </span> @lang('step4.must_agree2')</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div> <!-- container-fluid --> 
{!! Form::close() !!}
</section>


<section id="calulate_bar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon1.png') }}" alt="car-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('common.find_car')</div>
                            <div class="infouser">{{ $init['summary']['car_year']." ".strtoupper($init['summary']['car_brand'])." ".strtoupper($init['summary']['car_model']) }}</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon2.png') }}" alt="plan-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('common.plan')</div>
                            <div class="infouser" id="plan_choose">{{ $init['summary']['plan_name'] }}</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon3.png') }}" alt="capital-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_insure">@lang('common.ft_si')</div>
                            <div class="infouser" id="plan_price">{{ number_format($init['summary']['ft_si'],0) }}</div>
                        </div>
                    </li>
                    <li class="fill-active">
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon4.png') }}" alt="pay-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_pay">@lang('common.payment_cost')</div>
                            <div class="infouser" id="plan_pay">{{ number_format($init['summary']['payment_result'],2) }}</div>
                        </div>
                    </li>
                    <li class="btnrow nextpage fill-active">
                        <button type="submit" class="btn active" id="btn-submit">@lang('step4.confirm')</button>
                    </li>
                </ul>
                <a class="show-detail"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                <a class="hidden-detail"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>
@include('web.insurance.modal.custom-alert')
@endsection

@section('javascript')
<script src="{{ asset('component/moment/moment.js') }}"></script>
<script>
$(document).ready(function(){
    moment.locale($("input[name='hd_current_lang']").val());
});
</script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.rule.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/bootstrap-dialog/bootstrap-dialog.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/material-datepicker/js/bootstrap-material-datetimepicker.th.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/lightbox2-master/src/js/lightbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/insurance.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/insurance_4.min.js?v=1.3') }}"></script>
@endsection

@section('hidden')


{{ Form::hidden('hd_save_inform',route('web.save_information')) }}
{{ Form::hidden('hd_get_gender',route('api.get_gender')) }}
{{ Form::hidden('hd_get_prefix',route('api.web.get_prefix')) }}
{{ Form::hidden('hd_get_district',route('api.web.get_district')) }}
{{ Form::hidden('hd_get_subdistrict',route('api.web.get_subdistrict')) }}
{{ Form::hidden('hd_get_postalcode',route('api.web.get_postalcode')) }}
{{ Form::hidden('hd_submit_otp',route('web.submit_otp')) }}
{{ Form::hidden('hd_request_otp',route('web.request_otp')) }}
{{ Form::hidden('hd_check_otp',route('web.check_otp')) }}
{{ Form::hidden('hd_round_otp',route('web.round_otp')) }}
{{ Form::hidden('hd_check_webservice',route('api.web.check_webservice')) }}
{{ Form::hidden('hd_lang_is_agree',trans('step4.is_agree')) }}
{{ Form::hidden('hd_lang_required',trans('validation.custom.required')) }}
{{ Form::hidden('hd_lang_number',trans('validation.custom.number')) }}
{{ Form::hidden('hd_lang_idcard',trans('validation.custom.idcard')) }}
{{ Form::hidden('hd_lang_tel',trans('validation.custom.tel_length')) }}
{{ Form::hidden('hd_lang_email',trans('validation.custom.email')) }}
{{ Form::hidden('hd_lang_alpha',trans('validation.custom.alpha')) }}
{{ Form::hidden('hd_lang_max20',trans('validation.custom.max_20')) }}
{{ Form::hidden('hd_lang_chassis',trans('validation.custom.uniqe_chassis')) }}
{{ Form::hidden('hd_lang_invalid_form',trans('step3.invalid_form')) }}
{{ Form::hidden('hd_lang_district',trans('step4.select_district')) }}
{{ Form::hidden('hd_lang_subdistrict',trans('step4.select_subdistrict')) }}
@include('web.insurance.modal.loading')
@include('web.insurance.modal.otp')
@endsection