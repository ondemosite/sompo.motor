@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="{{ asset('component/checkbox-nojs/checkbox-nojs.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance.min.css?v=1.1') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance_5.min.css?v=1') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="stepbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="inside-step-bar">
                    <ul>
                        <li class="pass">
                            <a href="{{ route('web.dashboard') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">ข้อมูลรถ</div>
                            </a>
                        </li>
                        <li class="pass">
                            <a href="{{ route('web.insurance_step2') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">แผนประกัน</div>
                            </a>
                        </li>
                        <li class="pass">
                            <a href="{{ route('web.insurance_step3') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">ปรับแต่งแผน</div>
                            </a>
                        </li>
                        <li class="pass">
                            <a href="{{ route('web.insurance_step4') }}">
                            <div class="step-circle"><img src="{{ asset('images/web/icon/checkmark.png') }}"/></div>
                            <div class="step-detail">ข้อมูลผู้เอาประกัน</div>
                            </a>
                        </li>
                        <li class="active">
                            <div class="step-circle">5</div>
                            <div class="step-detail">ชำระเงิน</div>
                        </li>
                    </ul>
                    <div class="stregth-bar"></div>
                </div>
            </div>
        </div>
  </div> <!--/container -->
</section>

<section id="section-insurance">
    <div class="container-fluid">
        <div class="row wrap-insurance-header">
            <div class="col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h1 class="sec-title">@lang('step5.your_insurance')</h1>
                        </div>
                    </div><!--/row-->
                    <div class="row block-insurance-header">
                        <div class="col-md-3">
                            <img class="item-main" src="{{ asset('/images/web/icon/Sompony_Step5.png') }}" />
                        </div>
                        <div class="col-md-9">
                            <div class="row row-box">
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step-5-1.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="text-color-red-light">{{ !empty($init['header']['plan_name'])?$init['header']['plan_name']:null }}</p>
                                        @if($init['detail']['plan_id']!=3)
                                        <p class="txt-detail-light">@lang('step5.sum_insured') {{ !empty($init['header']['ft_si'])?number_format($init['header']['ft_si']):null }} @lang('common.baht')</p>
                                        @else
                                        <p class="txt-detail-light">คุ้มครองการชน {{ !empty($init['header']['od_si'])?number_format($init['header']['od_si']):null }} @lang('common.baht')</p>
                                        @endif
                                        <p class="txt-detail-light">@lang('step5.insurance_start') {{ !empty($init['header']['start_insurance'])?$init['header']['start_insurance']:null }}</p>
                                        <p class="txt-detail-light">หมดอายุวันที่ {{ !empty($init['header']['end_insurance'])?$init['header']['end_insurance']:null }}</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-2.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="txt-detail-light">@lang('step5.car')</p>
                                        <p class="text-color-red-light">{{ !empty($init['header']['full_car'])?strtoupper($init['header']['full_car']):null }}</p>
                                        <p class="txt-detail-light">
                                            @if($init['header']['motor_code']=="320")
                                            รถคันนี้ใช้สำหรับเพื่อการพาณิชย์ที่ไม่ใช่รถรับจ้างหรือให้เช่า
                                            @else
                                            รถคันนี้ใช้สำหรับบุคคลเท่านั้น
                                            @endif
                                        </p>
                                        <!-- <p class="txt-detail-light"><small>{{ (!empty($init['header']['is_cctv']) && $init['header']['is_cctv']==1 )?"ติดกล้อง CCTV":"" }}</small></p> -->
                                    </div>
                                </div>
                            </div><!--/row-->
                            <div class="row row-box">
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-3.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="text-color-red-light">@lang('step5.addition_coverage')</p>
                                        @if($init['header']['addon']!=null)
                                            @if(!empty($init['header']['addon']['compulsory']))
                                            <p class="txt-detail-light">+ @lang('step5.compulsory_start') {{ $init['header']['addon']['compulsory'] }}</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_flood']) && $init['header']['addon']['is_flood']==1)
                                            <p class="txt-detail-light">+ @lang('step5.flood_coverage')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_theft']) && $init['header']['addon']['is_theft']==1)
                                            <p class="txt-detail-light">+ @lang('step5.robbery_coverage')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_taxi']) && $init['header']['addon']['is_taxi']==1)
                                            <p class="txt-detail-light">+ @lang('step5.taxi_cost')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_hb']) && $init['header']['addon']['is_hb']==1)
                                            <p class="txt-detail-light">+ @lang('step5.hb_coverage')</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_carloss']) && $init['header']['addon']['is_carloss']==1)
                                            <p class="txt-detail-light">+ @lang('step5.carloss_coverage')</p>
                                            @endif
                                        @else
                                            <p class="txt-detail-light">-</p>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-4.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="txt-detail-light">@lang('step5.total_amount')</p>
                                        <p class="text-color-red-light">{{ number_format($init['header']['payment_result'],2) }} @lang('common.baht')</p>
                                    </div>
                                </div>
                            </div><!--/row-->
                            <div class="row row-box">
                                <div class="col-xs-6 note-text"><p><b>หมายเหตุ</b> ผู้ซื้อควรทำความเข้าใจในรายละเอียดความคุ้มครอง เงื่อนไข และข้อยกเว้น ก่อนตัดสินใจทำประกันภัยทุกครั้ง <a href="{{ asset('files/pdf/attachment.pdf') }}" target="_blank">คลิกที่นี่</a></p></div>
                            </div>
                        </div>
                    </div><!--/row block-insurance-header-->
                </div><!--/container-->
            </div>
        </div>
    </div> <!-- /container-fluid -->

    <div class="container-fluid">
        <div class="row wrap-insurance-detail">
            <div class="col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h1 class="sec-title">@lang('step5.insurance_detail')</h1>
                        </div>
                    </div><!--/row-->
                    <div class="row block-insurance-detail">
                        <div class="col-md-3 col-xs-12 row-header-title">
                            <p class="txt-detail">@lang('step5.coverage')</p>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row-title"><p class="txt-detail">@lang('step5.yourcar_damage')</div>
                                </div>
                            </div><!--row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.coverage_accident')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            @if($init['detail']['plan_id']!=3)
                                            <p class="txt-detail">{{ number_format($init['detail']['ft_si']) }} @lang('common.baht')</p>
                                            @else
                                            <p class="txt-detail">-</p>
                                            @endif
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.coverage_carlost')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            @if($init['detail']['plan_id']==1)
                                            <p class="txt-detail">{{ number_format($init['detail']['ft_si']) }} @lang('common.baht')</p>
                                            @else
                                            <p class="txt-detail">-</p>
                                            @endif

                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.coverage_fire')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            @if($init['detail']['plan_id']==1)
                                            <p class="txt-detail">{{ number_format($init['detail']['ft_si']) }} @lang('common.baht')</p>
                                            @else
                                            <p class="txt-detail">-</p>
                                            @endif
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            @if(!empty($init['header']['addon']['is_flood']) && $init['header']['addon']['is_flood']==1)
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.coverage_flood')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ !empty($init['detail']['flood_si'])?number_format($init['detail']['flood_si'])." ".trans('common.baht'):"-" }} </p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            @endif
                            <div class="row cut-line"></div> <!-- Cut-line -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row-title"><p class="txt-detail">@lang('step5.support_outside')</div>
                                </div>
                            </div><!--row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.damage_outside')</p>
                                            <p class="txt-detail-light">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย ต่อครั้ง</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ number_format($init['detail']['person_damage_person']) }} @lang('common.baht')</p>
                                            <p class="txt-detail">{{ number_format($init['detail']['person_damage_once']) }} @lang('common.baht')</p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.stuff_damage_outside')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ number_format($init['detail']['stuff_damage']) }} @lang('common.baht')</p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row cut-line"></div> <!-- Cut-line -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row-title"><p class="txt-detail">@lang('step5.addition_coverage')</div>
                                </div>
                            </div><!--row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.personal_accident')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ number_format($init['detail']['death_disabled']) }} @lang('common.baht')</p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.medical_fee')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ number_format($init['detail']['medical_fee']) }} @lang('common.baht')</p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.bail_driver')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ number_format($init['detail']['bail_driver']) }} @lang('common.baht')</p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            @if(!empty($init['detail']['compulsory']) && $init['detail']['compulsory']!=0)
                            <div class="row cut-line"></div> <!-- Cut-line -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row-title"><p class="txt-detail">@lang('step5.compulsory')</div>
                                </div>
                            </div><!--row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.real_medical')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ number_format($init['detail']['compulsory_medical_free']) }} @lang('common.baht_person')</p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.death_disable')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ number_format($init['detail']['compulsory_death_disable']) }} @lang('common.baht_times')</p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            @endif
                            <div class="row cut-line"></div> <!-- Cut-line -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row-title"><p class="txt-detail">@lang('step5.accident_cost')</div>
                                </div>
                            </div><!--row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.robbery_cost')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ $init['detail']['theft_cost']==0?"-":number_format($init['detail']['theft_cost'])." ".trans('common.baht_person') }} </p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.taxi_money_cost')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ $init['detail']['taxi_cost']==0?"-":number_format($init['detail']['taxi_cost'])." ".trans('common.baht') }} </p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.hb_coverage')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ $init['detail']['hb_cost']==0?"-":number_format($init['detail']['hb_cost'])." ".trans('common.baht') }} </p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-box">
                                        <div class="box-text box-text-left">
                                            <p class="txt-detail-light">@lang('step5.carloss_coverage')</p>
                                        </div>
                                        <div class="box-text box-text-right">
                                            <p class="txt-detail">{{ $init['detail']['carloss_cost']==0?"-":number_format($init['detail']['carloss_cost'])." ".trans('common.baht') }} </p>
                                        </div>
                                    </div><!--/col-box -->
                                </div>
                            </div><!-- /row-->
                        </div>
                    </div><!--/row block-insurance-detail-->
                    
                    <div class="row block-insurance-table">
                        <div class="col-xs-12">
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-6"><p class="txt-detail">@lang('step5.insurance_begin')</p></div>
                                <div class="col-sm-5 col-xs-6"><p class="txt-detail">{{ $init['detail']['start_insurance'] }}</p></div>
                            </div>
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-6"><p class="txt-detail">@lang('step5.insurance_expire')</p></div>
                                <div class="col-sm-5 col-xs-6"><p class="txt-detail">{{ $init['detail']['end_insurance'] }}</p></div>
                            </div>
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-6"><p class="txt-detail">@lang('step5.sum_insured')</p></div>
                                <div class="col-sm-5 col-xs-6"><p class="txt-detail">{{ number_format($init['detail']['ft_si']) }} @lang('common.baht')</p></div>
                            </div>
                            @if(!empty($init['detail']['compulsory']) && $init['detail']['compulsory']!=0)
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-6"><p class="txt-detail">@lang('step5.compulsory_short')</p></div>
                                <div class="col-sm-5 col-xs-6">
                                    @if(!empty($init['detail']['compulsory']))
                                    <p class="txt-detail">@lang('step5.buy') (+{{ number_format($init['detail']['compulsory'],2) }} @lang('common.baht'))</p>
                                    <p class="txt-detail">@lang('step5.begin') {{ $init['detail']['compulsory_start'] }}</p>
                                    <p class="txt-detail">สิ้นสุด {{ $init['detail']['compulsory_end'] }}</p>
                                    @else
                                    <p class="txt-detail">-</p>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-6"><p class="txt-detail">@lang('step5.driver_amount')</p></div>
                                <div class="col-sm-5 col-xs-6"><p class="txt-detail">{{ $init['detail']['define_name_amount']==0?trans('common.no_specify'):$init['detail']['define_name_amount']." ".trans('common.person') }}</p></div>
                            </div>
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-6"><p class="txt-detail">@lang('step5.garage_type')</p></div>
                                <div class="col-sm-5 col-xs-6"><p class="txt-detail">
                                    @if($init['detail']['garage_type']=="DEALER")
                                    @lang('common.garage_dealer')
                                    @elseif($init['detail']['garage_type']=="GENERAL")
                                    @lang('common.garage_general')
                                    @else
                                    -
                                    @endif
                                </p></div>
                            </div>
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-6"><p class="txt-detail">@lang('step5.excess')</p></div>
                                <div class="col-sm-5 col-xs-6"><p class="txt-detail">{{ $init['detail']['deduct']==0?"-":number_format($init['detail']['deduct'],2)." ".trans('common.baht') }} </p></div>
                            </div>
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail txt-rs">@lang('step5.main_driver_information')</p></div>
                                <div class="col-sm-4 col-xs-6">
                                    <p class="txt-detail-light">คำนำหน้า :</p>
                                    <p class="txt-detail-light">@lang('common.name_lastname') :</p>
                                    <p class="txt-detail-light">@lang('common.idcard') :</p>
                                    <p class="txt-detail-light">@lang('common.birthdate') :</p>
                                    <p class="txt-detail-light">@lang('common.sex') :</p>
                                </div>
                                <div class="col-sm-5 col-xs-6">
                                    <p class="txt-detail">{{ $init['detail']['main_prefix'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['main_name']." ".$init['detail']['main_lastname'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['main_id_card'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['main_birthdate'] }} (@lang('common.age') {{ $init['detail']['main_age'] }} @lang('common.year'))</p>
                                    <p class="txt-detail">{{ $init['detail']['main_gender']=="MALE"?trans('common.male'):trans('common.female') }}</p>
                                </div>
                            </div>
                            <!--
                            <div class="row row-box row-gray">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail">ที่อยู่ตามทะเบียนบ้าน</p></div>
                                <div class="col-sm-5 col-xs-12"><p class="txt-detail txt-rs-light">503 ถนนศรีอยุธยา แขวงถนนพญาไท เขตราชเทวี กรุงเทพฯ 10400</p></div>
                            </div> -->
                            <div class="row row-box row-gray">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail">@lang('common.address')</p></div>
                                <div class="col-sm-5 col-xs-12"><p class="txt-detail txt-rs-light">{{ $init['detail']['main_address'] }}</p></div>
                            </div>
                            <div class="row row-box row-gray">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail txt-rs">@lang('step5.contact')</p></div>
                                <div class="col-sm-4 col-xs-6">
                                    <p class="txt-detail-light">@lang('common.mobile_phone') :</p>
                                    <p class="txt-detail-light">@lang('common.email') :</p>
                                </div>
                                <div class="col-sm-5 col-xs-6">
                                    <p class="txt-detail">{{ $init['detail']['main_tel'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['main_email'] }}</p>
                                </div>
                            </div>
                            @if(!empty($init['detail']['driver1_name']))
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail txt-rs">@lang('step5.driver1_data')</p></div>
                                <div class="col-sm-4 col-xs-6">
                                    <p class="txt-detail-light">@lang('common.name_lastname') :</p>
                                    <p class="txt-detail-light">@lang('common.idcard') :</p>
                                    <p class="txt-detail-light">@lang('common.birthdate') :</p>
                                    <p class="txt-detail-light">@lang('common.sex') :</p>
                                </div>
                                <div class="col-sm-5 col-xs-6">
                                    <p class="txt-detail">{{ $init['detail']['driver1_name']." ".$init['detail']['driver1_lastname'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['driver1_id_card'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['driver1_birthdate'] }} (@lang('common.age') {{ $init['detail']['driver1_age'] }} @lang('common.year'))</p>
                                    <p class="txt-detail">{{ $init['detail']['driver1_gender']=="MALE"?trans('common.male'):trans('common.female') }}</p>
                                </div>
                            </div>
                            @endif
                            @if(!empty($init['detail']['driver2_name']))
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail txt-rs">@lang('step5.driver2_data')</p></div>
                                <div class="col-sm-4 col-xs-6">
                                    <p class="txt-detail-light">@lang('common.name_lastname') :</p>
                                    <p class="txt-detail-light">@lang('common.idcard') :</p>
                                    <p class="txt-detail-light">@lang('common.birthdate') :</p>
                                    <p class="txt-detail-light">@lang('common.sex') :</p>
                                </div>
                                <div class="col-sm-5 col-xs-6">
                                    <p class="txt-detail">{{ $init['detail']['driver2_name']." ".$init['detail']['driver2_lastname'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['driver2_id_card'] }}</p>
                                    <p class="txt-detail">{{ $init['detail']['driver2_birthdate'] }} (@lang('common.age') {{ $init['detail']['driver2_age'] }} @lang('common.year'))</p>
                                    <p class="txt-detail">{{ $init['detail']['driver2_gender']=="MALE"?trans('common.male'):trans('common.female') }}</p>
                                </div>
                            </div>
                            @endif
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail txt-rs">@lang('step5.car_information')</p></div>
                                <div class="col-sm-9 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('common.brand') :</p>
                                                </div>
                                                <div class="box-text box-text-right">
                                                    <p class="txt-detail">{{ strtoupper($init['car']['brand']) }}</p>
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="col-xs-8 box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('common.model') :</p>
                                                </div>
                                                <div class="col-xs-4 box-text box-text-right">
                                                    <p class="txt-detail">{{ strtoupper($init['car']['model']) }}</p>
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="col-xs-8 box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('common.sub_model') :</p>
                                                </div>
                                                <div class="col-xs-4 box-text box-text-right">
                                                    <p class="txt-detail">{{ strtoupper($init['car']['model_type']) }}</p>
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="col-xs-8 box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('common.year') :</p>
                                                </div>
                                                <div class="col-xs-4 box-text box-text-right">
                                                    <p class="txt-detail">{{ $init['car']['year'] }}</p>
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="col-xs-8 box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('common.car_licence') :</p>
                                                </div>
                                                <div class="col-xs-4 box-text box-text-right">
                                                    <p class="txt-detail">{{ str_replace(" ","-",$init['car']['licence']) }}</p>
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="col-xs-8 box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('common.province') :</p>
                                                </div>
                                                <div class="col-xs-4 box-text box-text-right">
                                                    <p class="txt-detail">{{ $init['car']['province'] }}</p>
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="col-xs-8 box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('common.car_chassis') :</p>
                                                </div>
                                                <div class="col-xs-4 box-text box-text-right">
                                                    <p class="txt-detail">{{ $init['car']['chassis'] }}</p>
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                </div>
                            </div> <!--/row-box -->
                            <!--
                            <div class="row row-box">
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail txt-rs">ข้อมูลกรมธรรม์เดิม</p></div>
                                <div class="col-sm-9 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="box-text box-text-left">
                                                    <p class="txt-detail-light">บริษัทประกันภัย :</p>
                                                </div>
                                                <div class="box-text box-text-right">
                                                    <p class="txt-detail">วิริยะประกันภัย</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="box-text box-text-left">
                                                    <p class="txt-detail-light">วันหมดอายุ :</p>
                                                </div>
                                                <div class="box-text box-text-right">
                                                    <p class="txt-detail">22 ตุลาคม 2560</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            @php
                                $checkIsEmptyDoc = true;
                                if(!empty($init['document']['personal_id'])) $checkIsEmptyDoc = false;
                                if(!empty($init['document']['car_licence'])) $checkIsEmptyDoc = false;
                                if(!empty($init['document']['driver1_licence'])) $checkIsEmptyDoc = false;
                                if(!empty($init['document']['driver2_licence'])) $checkIsEmptyDoc = false;
                                if(!empty($init['document']['cctv_inside'])) $checkIsEmptyDoc = false;
                                if(!empty($init['document']['cctv_outside'])) $checkIsEmptyDoc = false;
                            @endphp
                            @if(!$checkIsEmptyDoc)
                            <div class="row row-box" >
                                <div class="col-sm-3 col-xs-12"><p class="txt-detail txt-rs">@lang('step5.document')</p></div>
                                <div class="col-sm-9 col-xs-12">
                                    @if(!empty($init['document']['personal_id']))
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('step5.doc_id_card')</p>
                                                </div>
                                                <div class="box-text box-text-right box-img">
                                                    <a href="{{ $init['document']['personal_id'] }}" target="_blank"><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>
                                                    <!--<a data-lightbox="image-personal_id" href="{{ $init['document']['personal_id'] }}" title=""><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>-->
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    @endif
                                    @if(!empty($init['document']['car_licence']))
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('step5.doc_car_licence')</p>
                                                </div>
                                                <div class="box-text box-text-right box-img">
                                                    <a href="{{ $init['document']['car_licence'] }}" target="_blank"><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>
                                                    <!--<a data-lightbox="image-car_licence" href="{{ $init['document']['car_licence'] }}" title=""><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>-->
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    @endif
                                    @if(!empty($init['document']['driver1_licence']))
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-box">
                                                <div class="box-text box-text-left">
                                                    <p class="txt-detail-light">@lang('step5.doc_driver1_licence')</p>
                                                </div>
                                                <div class="box-text box-text-right box-img">
                                                    <a href="{{ $init['document']['driver1_licence'] }}" target="_blank"><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>
                                                    <!--<a data-lightbox="image-driver1_licence" href="{{ $init['document']['driver1_licence'] }}" title=""><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>-->
                                                </div>
                                            </div><!--/col-box -->
                                        </div>
                                    </div><!-- /row-->
                                    @endif
                                    @if(!empty($init['document']['driver2_licence']))
                                    <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-box">
                                                    <div class="box-text box-text-left">
                                                        <p class="txt-detail-light">@lang('step5.doc_driver2_licence')</p>
                                                    </div>
                                                    <div class="box-text box-text-right box-img">
                                                        <a href="{{ $init['document']['driver2_licence'] }}" target="_blank"><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>
                                                        <!--<a data-lightbox="image-driver2_licence" href="{{ $init['document']['driver2_licence'] }}" title=""><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>-->
                                                    </div>
                                                </div><!--/col-box -->
                                            </div>
                                        </div><!-- /row-->
                                    @endif
                                    @if(!empty($init['document']['cctv_inside']))
                                    <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-box">
                                                    <div class="box-text box-text-left">
                                                        <p class="txt-detail-light">@lang('step5.doc_cctv_inside')</p>
                                                    </div>
                                                    <div class="box-text box-text-right box-img">
                                                        <a href="{{ $init['document']['cctv_inside'] }}" target="_blank"><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>
                                                        <!--<a data-lightbox="image-cctv_inside" href="{{ $init['document']['cctv_inside'] }}" title=""><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>-->
                                                    </div>
                                                </div><!--/col-box -->
                                            </div>
                                        </div><!-- /row-->
                                    @endif
                                    @if(!empty($init['document']['cctv_outside']))
                                    <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-box">
                                                    <div class="box-text box-text-left">
                                                        <p class="txt-detail-light">@lang('step5.doc_cctv_outside')</p>
                                                    </div>
                                                    <div class="box-text box-text-right box-img">
                                                        <a href="{{ $init['document']['cctv_outside'] }}" target="_blank"><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>
                                                        <!--<a data-lightbox="image-cctv_outside" href="{{ $init['document']['cctv_outside'] }}" title=""><img class="img-preview" src="{{ asset('/images/web/icon/step5-preview.png') }}" /></a>-->
                                                    </div>
                                                </div><!--/col-box -->
                                            </div>
                                        </div><!-- /row-->
                                    @endif

                                </div>
                            </div> <!--/row-box -->
                            @endif
                            <div class="row row-box row-accept">
                                <div class="col-xs-12"><p class="txt-detail txt-rs">ข้อตกลงในการสมัครประกัน</p></div>
                                <div class="col-xs-12">
                                    <div class="checkbox">
                                        <label>
                                        <input type="checkbox" name="is_agree">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        <span class="tx"><div class="error_is_agree"></div>ข้าพเจ้าได้อ่านและยอมรับข้อตกลง ความคุ้มครองของประกัน รวมถึงเงื่อนไขและข้อยกเว้นของกรมธรรม์ทุกประการ</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/container-->
            </div>
        </div>
    </div> <!-- /container-fluid -->

    <div class="container">
        <div class="row wrap-summary">
            <div class="col-xs-12 block-summary">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title">@lang('step5.summary_gross_premium')</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <img src="{{ asset('/images/web/icon/step5-5.png') }}" />
                    </div>
                    <div class="col-sm-9 col-xs-12">
                        <div>
                            <ul>
                                <li>
                                    <div class="txt-box txt-box-left"><p>@lang('step5.gross_premium') {{strtoupper($init['header']['plan_name'])}}</p></div>
                                    <div class="txt-box txt-box-right "><p class="txt-price">{{ number_format($init['summary']['gross_premium'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p>@lang('step5.compulsory_short')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price">{{ number_format($init['summary']['compulsory'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p>@lang('step5.addition_coverage')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price">{{ number_format($init['summary']['flood']+$init['summary']['theft_gross_premium']+$init['summary']['taxi_gross_premium']+$init['summary']['hb_gross_premium']+$init['summary']['carloss_gross_premium'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p class="text-color-red-dark">@lang('step5.promotion')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price text-color-red-dark">{{ number_format($init['summary']['discount'],2) }} @lang('common.baht')</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p class="txt-detail">@lang('step5.total_payment')</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price txt-detail">{{ number_format($init['summary']['payment_result'],2) }} @lang('common.baht')</p></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>


<section id="calulate_bar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon1.png') }}" alt="car-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('common.car')</div>
                            <div class="infouser">{{ strtoupper($init['header']['full_car']) }}</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon2.png') }}" alt="plan-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">@lang('common.plan')</div>
                            <div class="infouser" id="plan_choose">{{ $init['header']['plan_name'] }}</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon3.png') }}" alt="capital-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_insure">@lang('common.ft_si')</div>
                            <div class="infouser" id="plan_price">{{ number_format($init['header']['ft_si']) }}</div>
                        </div>
                    </li>
                    <li class="fill-active">
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon4.png') }}" alt="pay-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_pay">@lang('common.payment_cost')</div>
                            <div class="infouser" id="plan_pay">{{ number_format($init['header']['payment_result'],2) }}</div>
                        </div>
                    </li>
                    <li class="btnrow nextpage fill-active">
                        {!! Form::open(['url' => route('web.insurance_step5_submit'),'name'=>'form_insurance','class'=>'form_insurance','id'=>'form_insurance']) !!}
                        <button type="submit" class="btn" id="btn-submit">@lang('step5.payment')</button>
                        {!! Form::close(); !!}
                    </li>
                </ul>
                <a class="show-detail"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                <a class="hidden-detail"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('component/jquery-validate/jquery.validate.rule.js') }}"></script>
<script src="{{ asset('web/js/insurance.min.js') }}"></script>
<script src="{{ asset('web/js/insurance_5.min.js') }}"></script>
@endsection

@section('hidden')

@endsection