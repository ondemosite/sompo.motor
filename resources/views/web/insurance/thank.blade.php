@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="{{ asset('web/css/insurance.min.css?v=1') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/insurance_thank.min.css?v=1') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')

<section id="section-insurance">
    <div class="container-fluid">
        <div class="row wrap-insurance-header">
            <div class="col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h1 class="sec-title">ขอบคุณที่ไว้วางใจ SOMPO THAILAND</h1>
                        </div>
                    </div><!--/row-->
                    <div class="row block-insurance-header">
                        <div class="col-md-3">
                            <img class="item-main" src="{{ asset('/images/web/icon/step-5-main.png') }}" />
                        </div>
                        <div class="col-md-9">
                            <div class="row row-box">
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step-5-1.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="text-color-red-light">{{ !empty($init['header']['plan_name'])?$init['header']['plan_name']:null }}</p>
                                        <p class="txt-detail-light">ทุนประกัน {{ !empty($init['header']['ft_si'])?number_format($init['header']['ft_si']):null }} บาท</p>
                                        <p class="txt-detail-light">เริ่มคุ้มครองวันที่ {{ !empty($init['header']['start_insurance'])?$init['header']['start_insurance']:null }}</p>
                                        <p class="txt-detail-light">หมดอายุวันที่ {{ !empty($init['header']['end_insurance'])?$init['header']['end_insurance']:null }}</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-2.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="txt-detail-light">รถยนต์</p>
                                        <p class="text-color-red-light">{{ !empty($init['header']['full_car'])?strtoupper($init['header']['full_car']):null }}</p>
                                        <p class="txt-detail-light">รถคันนี้ใช้สำหรับบุคคลเท่านั้น</p>
                                        <p class="txt-detail-light"><small>{{ (!empty($init['header']['is_cctv']) && $init['header']['is_cctv']==1 )?"ติดกล้อง CCTV":"" }}</small></p>
                                    </div>
                                </div>
                            </div><!--/row-->
                            <div class="row row-box">
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-3.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="text-color-red-light">ความคุ้มครองเสริม</p>
                                        @if($init['header']['addon']!=null)
                                            @if(!empty($init['header']['addon']['compulsory']))
                                            <p class="txt-detail-light">+ พ.ร.บ. เริ่มคุ้มครอง {{ $init['header']['addon']['compulsory'] }}</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_flood']) && $init['header']['addon']['is_flood']==1)
                                            <p class="txt-detail-light">+ คุ้มครองน้ำท่วม</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_theft']) && $init['header']['addon']['is_theft']==1)
                                            <p class="txt-detail-light">+ คุ้มครองการโจรกรรมทรัพย์สินภายในรถยนต์</p>
                                            @endif
                                            @if(!empty($init['header']['addon']['is_taxi']) && $init['header']['addon']['is_taxi']==1)
                                            <p class="txt-detail-light">+ ชดเชยค่าเดินทางระหว่างรถยนต์เข้าซ่อมอู่</p>
                                            @endif
                                        @else
                                            <p class="txt-detail-light">-</p>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="col-xs-6 col-box">
                                    <div class="box icon-box"><img src="{{ asset('/images/web/icon/step5-4.png') }}" /></div>
                                    <div class="box detail-box">
                                        <p class="txt-detail-light">ยอดที่ต้องชำระ</p>
                                        <p class="text-color-red-light">{{ number_format($init['header']['payment_result'],2) }} บาท</p>
                                    </div>
                                </div>
                            </div><!--/row-->
                        </div>
                    </div><!--/row block-insurance-header-->
                </div><!--/container-->
            </div>
        </div>
    </div> <!-- /container-fluid -->

    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="item-title">
                    <img src="{{ asset('/images/web/icon/thank-icon.png') }}" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="wrap-detail text-center">
                    <p>เราได้จัดส่งกรมธรรม์ & ePASS* ไปให้ท่านตามอีเมลที่ระบุเรียบร้อยแล้ว </p>
                    <p>กรมธรรม์ พ.ร.บ. จะจัดส่งให้ทางไปรษณีย์</p>
                </div>
                <div class="wrap-detail-footer text-center">
                    <p>ช่องทางการให้บริการหลังการขาย</p>
                    <p>โทรศัพท์แจ้งที่สายด่วน 24 ช.ม. โทร. 02 118 7400</p>
                    <p>เจ้าหน้าที่ของ SOMPO พร้อมให้บริการแก่ท่านตลอดเวลา</p>
                </div>
                <div class="wrap-sub-footer text-center">
                    <p><b>*</b> เทคโนโลยี ePass ให้ทุกอย่าง "ง่าย" และสะดวกสบายตั้งแต่ขั้นตอนการซื้อจนถึงการเคลมประกัน ผ่านระบบออนไลน์</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row wrap-summary">
            <div class="col-xs-12 block-summary">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title">สรุปเบี้ยประกัน</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <img src="{{ asset('/images/web/icon/step5-5.png') }}" />
                    </div>
                    <div class="col-sm-9 col-xs-12">
                        <div>
                            <ul>
                                <li>
                                    <div class="txt-box txt-box-left"><p>เบี้ยประกัน SOMPO 2+</p></div>
                                    <div class="txt-box txt-box-right "><p class="txt-price">{{ number_format($init['summary']['gross_premium'],2) }} บาท</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p>พ.ร.บ.</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price">{{ number_format($init['summary']['compulsory'],2) }} บาท</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p>ความคุ้มครองเสริม</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price">{{ number_format($init['summary']['flood']+$init['summary']['theft_gross_premium']+$init['summary']['taxi_gross_premium'],2) }} บาท</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p class="text-color-red-dark">ส่วนลด Promotion</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price text-color-red-dark">{{ number_format($init['summary']['discount'],2) }} บาท</p></div>
                                </li>
                                <li>
                                    <div class="txt-box txt-box-left"><p class="txt-detail">รวมเบี้ยที่ต้องชำระ</p></div>
                                    <div class="txt-box txt-box-right"><p class="txt-price txt-detail">{{ number_format($init['summary']['payment_result'],2) }} บาท</p></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<!--
<section id="calulate_bar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon1.png') }}" alt="car-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">รถยนต์</div>
                            <div class="infouser">2015 TOYOTA CAMRY</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon2.png') }}" alt="plan-insurance">
                        </div>
                        <div class="detail">
                            <div class="title">แผนประกัน</div>
                            <div class="infouser" id="plan_choose">SOMPO 2+</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon3.png') }}" alt="capital-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_insure">ทุนประกันเริ่มต้น</div>
                            <div class="infouser" id="plan_price">730,000</div>
                        </div>
                    </li>
                    <li class="fill-active">
                        <div class="icon">
                            <img src="{{ asset('images/web/icon/baricon4.png') }}" alt="pay-insurance">
                        </div>
                        <div class="detail">
                            <div class="title" id="title_pay">ยอดที่ต้องชำระเริ่มต้น</div>
                            <div class="infouser" id="plan_pay">26,904.08</div>
                        </div>
                    </li>
                    <li class="btnrow nextpage fill-active">
                        <a href="javascript:;" class="btn">ชำระเงิน</a>
                    </li>
                </ul>
                <a class="show-detail"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                <a class="hidden-detail"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>
-->
@endsection

@section('javascript')
<script src="{{ asset('web/js/insurance.min.js') }}"></script>
<script src="{{ asset('web/js/insurance_5.min.js') }}"></script>
@endsection

@section('hidden')

@endsection