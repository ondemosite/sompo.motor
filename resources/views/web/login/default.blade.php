<link href="{{ asset('web/css/login.css') }}" rel="stylesheet" type="text/css" />
<div id="wrap_login">
<span class="close-form close-form-login">
<i class="fa fa-times" aria-hidden="true"></i>
</span>
<div class="block-login">
{!! Form::open(['url' => route('web.login.submit'),'class'=>'login-form','id'=>'loginForm']) !!}
<div class="row">
        <div class="block-login-inside">
            
            <div class="account-box">
                <div class="logo">
                    <h3>เข้าสู่ระบบ</h3>
                </div>
                <div class="alert my-alert"></div>
                <div class="form-group">
                    <input type="text" class="form-control my-control" name="email" placeholder="Email" required autofocus />
                </div>
                <div class="form-group">
                    <input type="password" class="form-control my-control" name="password" placeholder="Password" required />
                </div>
                <button class="btn btn-block purple-bg my-control" type="submit">เข้าสู่ระบบ</button>
                <a class="forgotLnk" href="http://www.jquery2dotnet.com">ลืมรหัสผ่าน?</a>
                <div class="or-box">
                    <span class="or">OR</span>
                    <div class="row">
                        <div class="col-md-12 row-block">
                            <a href="{!! url('login/social/facebook') !!}" class="btn btn-facebook btn-block my-control"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Login with Facebook</a>
                        </div>
                        <div class="col-md-12 row-block">
                            <a href="{!! url('login/social/google') !!}" class="btn btn-google btn-block my-control"><i class="fa fa-google-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Login with Google</a>
                        </div>
                    </div>
                    <div class="row secure-row">
                        <p><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;&nbsp;เรารักษาข้อมูลของคุณเป็นความลับสูงสุด</p>
                        <p><i class="fa fa-shield" aria-hidden="true"></i>&nbsp;&nbsp;ระบบความปลอดภัย SHA-256 ที่ธนาคารทั่วโลกยอมรับ</p>
                    </div>
                </div>
                <div class="or-box row-block">
                    <div class="row">
                        <div class="col-md-12 row-block text-center">
                            <span>ยังไม่ได้ลงทะเบียน?</span>&nbsp;&nbsp;<span><a href="javascript:showRegisterForm()">ลงทะเบียนที่นี่</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
</div>
</div> <!--/wrap login -->

<div id="wrap_register"> <!--/wrap register -->
<span class="close-form close-form-register">
<i class="fa fa-times" aria-hidden="true"></i>
</span>
<div class="block-register">
{!! Form::open(['url' => route('web.register'),'class'=>'register-form','id'=>'registerForm']) !!}
<div class="row">
        <div class="block-register-inside">
            <div class="account-box">
                <div class="logo">
                    <h3>สมัครสมาชิก</h3>
                </div>
                <div class="alert my-alert-register"></div>
                
                <div class="form-group">
                    <input type="text" class="form-control my-control" name="email" placeholder="Email" required autofocus />
                </div>
                <div class="form-group">
                    <input id="register_password" type="password" class="form-control my-control" name="password" placeholder="Password" required />
                </div>
                <div class="form-group">
                    <input id="register_re_password" type="password" class="form-control my-control" name="retype_password" placeholder="Retype Password" required />
                </div>
                <button class="btn btn-block purple-bg my-control" type="submit">สมัครสมาชิก</button>
                <div class="or-box">
                    <span class="or">OR</span>
                    <div class="row">
                        <div class="col-md-12 row-block">
                            <a href="{!! url('login/social/facebook') !!}" class="btn btn-facebook btn-block my-control"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Register with Facebook</a>
                        </div>
                        <div class="col-md-12 row-block">
                            <a href="{!! url('login/social/google') !!}" class="btn btn-google btn-block my-control"><i class="fa fa-google-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Register with Google</a>
                        </div>
                    </div>
                    <div class="row secure-row">
                        <p><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;&nbsp;เรารักษาข้อมูลของคุณเป็นความลับสูงสุด</p>
                        <p><i class="fa fa-shield" aria-hidden="true"></i>&nbsp;&nbsp;ระบบความปลอดภัย SHA-256 ที่ธนาคารทั่วโลกยอมรับ</p>
                    </div>
                </div>
                <div class="or-box row-block">
                    <div class="row">
                        <div class="col-md-12 row-block text-center">
                            <span>เคยลงทะเบียนไว้แล้ว?</span>&nbsp;&nbsp;<span><a href="javascript:showLogin()">เข้าสู่ระบบที่นี่</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
</div>
</div> <!--/Block login -->