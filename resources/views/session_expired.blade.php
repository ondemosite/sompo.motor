@extends('web.layout.default')
@section('title') Sompo @endsection
@section('css')
<link href="{{ asset('web/css/session_expire.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('menu')
@include('web.layout.menu-light2')
@endsection
@section('content')
<section id="section-expire" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>เซสชั่นของคุณหมดอายุ</h2>

            </div> 
        </div>
    </div>
</section>

@endsection
