<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        .container{
            width:100%;
            
        }
        .topic{
            font-family:"Tahoma","sans-serif";
            font-size:12pt;
            margin-bottom:10px;
        }
        p{
            font-size:11pt;
            margin-bottom:10px;
        }
        .alert{
            color:red;
            margin-top:10px;
        }
    </style>
</head>
<body>
    <div class="container">
        @php
            $order = $init['policy']->order()->first();
            $main_driver = $order->main_driver()->first();
        @endphp
       <div class="topic">{{ $init['subject'] }}</div>
       <div class="topic">วันเวลาที่ : {{ $init['date'] }}</div>
       <div class="topic">ข้อมูลการทำรายการ</div>
       <p>Policy Number: {{ $init['policy']->policy_number }}</p>
       <p>Order Number: {{ $order->order_number }}</p>
       <p>Policy Name: {{ $main_driver->name." ".$main_driver->lastname }}</p>
       <p class="alert">*Please check the issue and resend endorse API on Policy Management motoradmins</p>
    </div>
</body>
</html>