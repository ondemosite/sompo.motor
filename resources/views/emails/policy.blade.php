<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        .container{
            width:100%;
            
        }
        .topic{
            font-family:"Tahoma","sans-serif";
            font-size:12pt;
            margin-bottom:10px;
        }
        p{
            font-size:11pt;
            margin-bottom:10px;
        }
    </style>
</head>
<body>
    <div class="container">
       <span class="topic">เรียน {{ $init['fullname_th'] }}</span>
       <p>บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด (มหาชน) ขอขอบคุณท่านที่ไว้วางใจให้บริษัทฯเป็นผู้รับประกันรถยนต์ ภาคสมัครใจ{{ $init['is_compulsory']?'และภาคบังคับ':'' }} หมายเลขทะเบียน {{ str_replace(" ","-",$init['car_licence']) }} ของท่าน</p>
       <p>พร้อมกันนี้บริษัทฯได้แนบตารางกรมธรรม์ประกันภัยรถยนต์ของท่านมาเพื่อเป็นหลักฐานยืนยันความคุ้มครองแก่ท่าน<br/>
        เพื่อความปลอดภัยของข้อมูล กรุณาระบุรหัสผ่าน (Password) เพื่อเรียกดูตารางกรมธรรม์ประกันภัยรถยนต์ ตามตัวอย่างด้านล่าง<br/>
        ตัวอย่าง : หากวันเดือนปีเกิดของท่านคือ 1 กุมภาพันธ์ ค.ศ. 1980 ตามด้วยตามด้วยรหัสบัตรประจำตัวประชาชน 4 หลักแรก    รหัสผ่านของท่านคือ 01Feb19801007</p>
        <p>กรณีต้องการรับกรมธรรม์ทางไปรษณีย์ หรือต้องการแก้ไขข้อมูลซึ่งระบุในกรมธรรม์ประกันภัยรถยนต์ฉบับนี้ หรือ ต้องการสอบถามข้อมูลเพิ่มเติม กรุณาติดต่อ ศูนย์บริการลูกค้าสัมพันธ์ โทร 02-119-3000 ในเวลาทำการ จันทร์ - ศุกร์ เวลา 8.30-17.00 หรือ cs@sompo.co.th</p>
        <p>หากท่านต้องการความช่วยเหลือฉุกเฉินระหว่างเดินทาง กรุณาติดต่อ SOMPO ASSIT ศูนย์บริการช่วยเหลือฉุกเฉิน ตลอด 24 ชั่วโมง ที่ +662-118-7400</p>
        <p>บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด (มหาชน)</p>
        <p>------------------------------------------------------------------------------------------------------</p>
        <span class="topic">To {{ $init['fullname_en'] }}</span>
        <p>Thank you for choosing Sompo Insurance Thailand to be the insurer taking care of voluntary {{ $init['is_compulsory']?'and compulsory':'' }} Motor insurance for the car plat No. {{ str_replace(" ","-",$init['car_licence']) }} Please find enclosed your Motor  insurance policy with coverage details for your reference.<br/>
        For your security, please enter your password to view your Motor insurance policy as instructed below..<br/>
        For example: If your date of birth is 1st February 1980 with first 4 digis of ID card no., your password is 01Feb19801007</p>
        <p>For more information or any policy amendments, please contact our customer services at 02-119-3000 Mon - Fri during 8.30-17.00</p>
        <p>In case of emergency during the trip, please contact our SOMPO ASSIST at +662-118-7400 everyday 24 hours</p>
        <p>Sompo Insurance (Thailand) Pcl.</p>
        
        @if($init['confirm_link']!=false)
        <p><a href="{{ $init['confirm_link'] }}" target="_blank">กรุณาคลิก เพื่อทำการระบุตัวตนของลูกค้าและยืนยันการซื้อกรมธรรม์ประกันภัยฉบับนี้</a></p>
        <p>กรณีไม่ดำเนินการระบุตัวตนของลูกค้าและยืนยันการซื้อกรมธรรม์ฉบับนี้ ภายในระยะเวลา 7 วัน บริษัทขอดำเนินการติดต่อท่านผ่านทางโทรศัพท์เพื่อดำเนินกระบวนการระบุตัวตนของลูกค้าและยืนยันการซื้อกรมธรรม์ต่อไป</p>
        @endif
 </div>
</body>
</html>