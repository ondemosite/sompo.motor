@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/vehicle_form.css') !!}">
@endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_vehicle.add') <span class="text-muted font-weight-light">@lang('admin_vehicle.vehicle')</span></h1>
@else
<h1>@lang('admin_vehicle.edit') <span class="text-muted font-weight-light">@lang('admin_vehicle.vehicle')</span></h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_vehicle.add')/@lang('admin_vehicle.edit') @lang('admin_vehicle.vehicle')</span>
                </div>
                {{ Form::open(['url' => route("admin.save_vehicle"),'id'=>'form_vehicle','name'=>'form_vehicle','class'=>'form_vehicle'])}}
                {{ Form::hidden('vehicle_id',!empty($data['id'])?$data['id']:'') }}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label class="required">@lang('admin_vehicle.brand')</label>
                            {{ Form::select('brand_id',!empty($init['select_brand'])?$init['select_brand']:[],!empty($data['brand_id'])?$data['brand_id']:null, 
                            ['class' => 'form-control','placeholder' => trans('admin_vehicle.brand') ]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label class="required">@lang('admin_vehicle.model')</label>
                            {{ Form::select('model_id',!empty($init['type'])?$init['type']:[],!empty($data['type'])?$data['type']:null, 
                            ['class' => 'form-control','placeholder' => trans('admin_vehicle.model')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label class="required">@lang('admin_vehicle.year')</label>
                            {{ Form::select('year',!empty($init['select_year'])?$init['select_year']:[],!empty($data['year'])?$data['year']:null, 
                            ['class' => 'form-control','placeholder' => trans('admin_vehicle.year') ]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label class="required">@lang('admin_vehicle.body_type')</label>
                            {{ Form::select('body_type',!empty($init['select_body_type'])?$init['select_body_type']:[],!empty($data['body_type'])?$data['body_type']:null, 
                            ['class' => 'form-control','placeholder' => trans('admin_vehicle.body_type') ]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label class="required">@lang('admin_vehicle.model_type')</label>
                            {{ Form::text('model_type',!empty($data['model_type'])?$data['model_type']:'', ['class' => 'form-control']) }}
                        </div>      
                        <div class="col-sm-6 form-group">
                            <label class="required">@lang('admin_vehicle.model_type2')</label>
                            {{ Form::text('model_type_full',!empty($data['model_type_full'])?$data['model_type_full']:'', ['class' => 'form-control']) }}
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.mortor_av')</label>
                            {{ Form::text('mortor_code_av',!empty($data['mortor_code_av'])?$data['mortor_code_av']:'', ['class' => 'form-control','placeholder' => trans('admin_vehicle.mortor_av')]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.motor_ac')</label>
                            {{ Form::text('mortor_code_ac',!empty($data['mortor_code_ac'])?$data['mortor_code_ac']:'', ['class' => 'form-control','placeholder' => trans('admin_vehicle.motor_ac')]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label class="required">CC</label>
                            {{ Form::text('cc',!empty($data['cc'])?$data['cc']:'', ['class' => 'form-control','placeholder' => 'CC']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.tons')</label>
                            {{ Form::text('tons',!empty($data['tons'])?$data['tons']:'', ['class' => 'form-control','placeholder' => trans('admin_vehicle.tons')]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.car_seat')</label>
                            {{ Form::text('car_seat',!empty($data['car_seat'])?$data['car_seat']:'', ['class' => 'form-control','placeholder' => trans('admin_vehicle.car_seat')]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.driver_passenger')</label>
                            {{ Form::text('driver_passenger',!empty($data['driver_passenger'])?$data['driver_passenger']:'', ['class' => 'form-control','placeholder' => trans('admin_vehicle.driver_passenger').' (1+4)']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.red_plate')</label>
                            {{ Form::text('red_plate',!empty($data['red_plate'])?$data['red_plate']:'', ['class' => 'form-control input-money','placeholder' => trans('admin_vehicle.red_plate')]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.used_car')</label>
                            {{ Form::text('used_car',!empty($data['used_car'])?$data['used_car']:'', ['class' => 'form-control input-money','placeholder' => trans('admin_vehicle.used_car') ]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label class="required">@lang('admin_vehicle.car_age')</label>
                            {{ Form::text('car_age',!empty($data['car_age'])?$data['car_age']:'', ['class' => 'form-control','placeholder' => trans('admin_vehicle.car_age') ]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>&nbsp;</label>
                            <label class="switcher switcher-lg switcher-primary">
                                @if(isset($data['status']))
                                <input type="checkbox" name="status" {{ $data['status']==1?'checked':'' }} >
                                @else
                                <input type="checkbox" checked name="status">
                                @endif
                                <div class="switcher-indicator">
                                    <div class="switcher-yes">@lang('common.yes')</div>
                                    <div class="switcher-no">@lang('common.no')</div>
                                </div>                     
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">@lang('admin_vehicle.submit')</button>
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{{ asset('component/jquery-money-format/jquery-money-format.js') }}"></script>
<script src="{{ asset('component/loader/loader.js') }}"></script>
<script src="{{ asset('admin/js/vehicle_form.js') }}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_data_vehicle',route('admin.get_data_vehicle')) }} 
{{ Form::hidden('hd_get_select_model',route('admin.get_select_model')) }} 

@endsection


