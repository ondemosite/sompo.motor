<table>
    <tr>
        <td>MAKE</td>
        <td>MODEL</td>
        <td>YEAR</td>
        <td>BODY TYPE</td>
        <td>MODEL_TYPE</td>
        <td>MODEL_TYPE_2</td>
        <td>MOTOR_CODE_AV</td>
        <td>MOTOR_CODE_AC</td>
        <td>CC</td>
        <td>TONS</td>
        <td>CAR SEAT</td>
        <td>PASSENGER</td>
        <td>RED PLATE</td>
        <td>USED CAR</td>
        <td>CAR AGE</td>
    </tr>
    @foreach($data as $item)
    <tr>
        <td>{{ $item->brand_name()->first()->name }}</td>
        <td>{{ $item->model_name()->first()->name }}</td>
        <td>{{ $item->year }}</td>
        <td>{{ $item->body_type }}</td>
        <td>{{ $item->model_type }}</td>
        <td>{{ $item->model_type_full }}</td>
        <td>{{ $item->mortor_code_av }}</td>
        <td>{{ $item->mortor_code_ac }}</td>
        <td>{{ $item->cc }}</td>
        <td>{{ $item->tons }}</td>
        <td>{{ $item->car_seat }}</td>
        <td>{{ $item->driver_passenger }}</td>
        <td>{{ $item->red_plate }}</td>
        <td>{{ $item->used_car }}</td>
        <td>{{ $item->car_age }}</td>
    </tr>
    @endforeach

    
</table> 