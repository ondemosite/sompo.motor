@php
    $store_brand = [];
    $store_model = [];
    $brand_name = [];
    $model_name = [];
@endphp

<table>
    <tr><td>&nbsp;</td></tr>
    @foreach($init as $item)
    @php
        $brand = "";
        $model = "";
        if(!in_array($item->brand_id,$store_brand)){
            $brand = $item->brand_name()->first()->name;
            array_push($store_brand,$item->brand_id);
            $brand_name["id_".$item->brand_id] = $brand;
        }else{
            $brand = $brand_name["id_".$item->brand_id];
        }
        if(!in_array($item->model_id,$store_model)){
            $model = $item->model_name()->first()->name;
            array_push($store_model,$item->model_id);
            $model_name["id_".$item->model_id] = $model;
        }else{
            $model = $model_name["id_".$item->model_id];
        }
    @endphp
    <tr>
        <td>{{ $brand }}</td>
        <td>{{ $model }}</td>
        <td>{{ $item->year }}</td>
        <td>{{ $item->body_type }}</td>
        <td>{{ $item->model_type }}</td>
        <td>{{ $item->mortor_code_av }}</td>
        <td>{{ $item->mortor_code_ac }}</td>
        <td>{{ $item->cc }}</td>
        <td>{{ $item->tons }}</td>
        <td>{{ $item->car_seat }}</td>
        <td>{{ $item->driver_passenger }}</td>
        <td>{{ $item->red_plate }}</td>
        <td>{{ $item->used_car }}</td>
        <td>{{ $item->car_age }}</td>
    </tr>
    @endforeach
    
</table> 