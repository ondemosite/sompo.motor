<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-car p-r-1"></i><span>@lang('admin_vehicle.add') @lang('admin_vehicle.topic_brand')</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_vehicle_brand"),'name'=>'form_brand','class'=>'form_brand','id'=>'form_brand')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_vehicle.brand_name')</label>
                    {{ Form::text('name',null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.brand_name')]) }}                   
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">@lang('admin_vehicle.top_order')</label>
                    {{ Form::number('top_order',null, ['class' => 'form-control']) }}
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">@lang('admin_vehicle.close')</button>
                <button type="button" class="btn btn-primary" id="submit_button">@lang('admin_vehicle.save')</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>