<div class="modal fade" id="modal-imported" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-upload p-r-1"></i><span>ตรวจสอบข้อมูลและยืนยัน</span></h4>
            </div> 
            <div class="modal-body">
                <div class="table-light">
                    <table class="table table-striped table-bordered" id="datatablesImported">
                        <thead>
                        <tr>
                            <th>@lang('admin_vehicle.no')</th>
                            <th>@lang('admin_vehicle.brand')</th>
                            <th>@lang('admin_vehicle.model')</th>
                            <th>@lang('admin_vehicle.year')</th>
                            <th>@lang('admin_vehicle.body_type')</th>
                            <th>@lang('admin_vehicle.model_type')</th>
                            <th>@lang('admin_vehicle.mortor_av')</th>
                            <th>@lang('admin_vehicle.motor_ac')</th>
                            <th>CC</th>
                            <th>TONS</th>
                            <th>CAR SEAT</th>
                            <th>DRIVER PASSENGERS</th>
                            <th>RED PLATE</th>
                            <th>USED CAR</th>
                            <th>CAR AGE</th>
                        </tr>
                        </thead>
                    </table>
                </div> <!-- /table-light -->    
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <span class="import-status pull-left"></span>
                <button type="button" class="btn" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="submit_import_change">ยืนยัน</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>