@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/vehicle_model.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.vehicle.modal.vehicle_model')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> 
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_vehicle.topic_model')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Vehicle Model"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_vehicle.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_name">@lang('admin_vehicle.model_name')</label>
                                {{ Form::text('search_model_name',null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.model_name') ]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type">@lang('admin_vehicle.brand_name')</label>
                                {{ Form::select('search_brand',!empty($init['brand'])?$init['brand']:[],null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.brand_name')]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_vehicle.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_vehicle.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_vehicle.no')</th>
                                <th>@lang('admin_vehicle.model_name')</th>
                                <th>@lang('admin_vehicle.brand_name')</th>
                                <th>@lang('admin_vehicle.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/vehicle_model.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_vehicle_model_list',route('admin.get_vehicle_model_list')) }} 
{{ Form::hidden('hd_get_data_vehicle_model',route('admin.get_data_vehicle_model')) }} 
{{ Form::hidden('hd_save_vehicle_model',route('admin.save_vehicle_model')) }} 
{{ Form::hidden('hd_delete_model',route('admin.delete_model')) }} 
@endsection
