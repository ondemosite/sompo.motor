@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/vehicle.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.vehicle.modal.import')
@include('admin.vehicle.modal.imported')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_vehicle.topic')</span>
                    <div class="panel-heading-controls">
                        <a data-toggle="modal" data-target="#modal-import" title="Import CSV"><i class="fa fa-upload"></i></a>
                        <a href="{{ route('admin.vehicle_form') }}" title="Add Garage"><i class="fa fa-plus"></i></a>
                        <!--<button id="test">TEST</button>-->
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_vehicle.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_body_type">@lang('admin_vehicle.body_type')</label>
                                {{ Form::select('search_body_type',!empty($init['select_body_type'])?$init['select_body_type']:[],null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.body_type')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_year">@lang('admin_vehicle.year')</label>
                                {{ Form::select('search_year',!empty($init['select_year'])?$init['select_year']:[],null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.year')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_brand">@lang('admin_vehicle.brand_name')</label>
                                {{ Form::select('search_brand',!empty($init['select_brand'])?$init['select_brand']:[],null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.brand_name')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_model">@lang('admin_vehicle.model_name')</label>
                                {{ Form::select('search_model',!empty($init['select_model'])?$init['select_model']:[],null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.model_name')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_name">@lang('admin_vehicle.model_type')</label>
                                {{ Form::text('search_model_type',null, ['class' => 'form-control','placeholder' => trans('admin_vehicle.model_type')]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_vehicle.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_vehicle.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_vehicle.no')</th>
                                <th>@lang('admin_vehicle.brand')</th>
                                <th>@lang('admin_vehicle.model')</th>
                                <th>@lang('admin_vehicle.year')</th>
                                <th>@lang('admin_vehicle.body_type')</th>
                                <th>@lang('admin_vehicle.model_type')</th>
                                <th>@lang('admin_vehicle.mortor_av')</th>
                                <th>@lang('admin_vehicle.motor_ac')</th>
                                <th>@lang('admin_vehicle.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/vehicle.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_vehicle_list',route('admin.get_vehicle_list')) }} 
{{ Form::hidden('hd_delete_vehicle',route('admin.delete_vehicle')) }} 
{{ Form::hidden('hd_get_select_model',route('admin.get_select_model')) }} 
{{ Form::hidden('hd_chunks',route('admin.vehicle_chunks')) }} 
{{ Form::hidden('hd_backup',route('admin.vehicle_backup')) }} 
{{ Form::hidden('hd_import',route('admin.vehicle_import')) }} 
{{ Form::hidden('hd_imported_list',route('admin.vehicle_imported_list')) }} 
{{ Form::hidden('hd_imported_submit',route('admin.vehicle_submit_import')) }} 
@endsection
