@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/vehicle_brand.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.vehicle.modal.vehicle_brand')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_vehicle.topic_brand')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Vehicle Brand"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_vehicle.no')</th>
                                <th>@lang('admin_vehicle.brand_name')</th>
                                <th>@lang('admin_vehicle.top_order')</th>
                                <th>@lang('admin_vehicle.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/vehicle_brand.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_vehicle_brand_list',route('admin.get_vehicle_brand_list')) }} 
{{ Form::hidden('hd_get_data_vehicle_brand',route('admin.get_data_vehicle_brand')) }} 
{{ Form::hidden('hd_save_vehicle_brand',route('admin.save_vehicle_brand')) }} 
{{ Form::hidden('hd_delete_brand',route('admin.delete_brand')) }} 
@endsection
