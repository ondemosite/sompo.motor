<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-file-text-o p-r-1"></i><span>Add/Edit Addon Taxi</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.addon_carloss_save"),'name'=>'form_addon','class'=>'form_addon','id'=>'form_addon')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label required" for="sum_insured">Sum Insured</label>
                    {{ Form::text('sum_insured',null, ['class' => 'form-control','placeholder' => 'Sum Insured']) }} 
                </div>
                <!-- Normal -->
                <div class="form-group">
                    <label class="control-label required" for="required-input">Normal - Net Premium</label>
                    {{ Form::text('net_normal',null, ['class' => 'form-control','placeholder' => 'Normal - Net Premium']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Normal - Stamp</label>
                    {{ Form::text('stamp_normal',null, ['class' => 'form-control','placeholder' => 'Normal - Stamp']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Normal - VAT</label>
                    {{ Form::text('vat_normal',null, ['class' => 'form-control','placeholder' => 'Normal - VAT']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Normal - Gross Premium</label>
                    {{ Form::text('normal',null, ['class' => 'form-control input-recal','placeholder' => 'Normal - Gross Premium']) }} 
                </div>
                <!-- under29 -->
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age<29 - Net Premium </label>
                    {{ Form::text('net_under29',null, ['class' => 'form-control','placeholder' => 'Age<29 Net Premium']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age<29 - Stamp</label>
                    {{ Form::text('stamp_under29',null, ['class' => 'form-control','placeholder' => 'Age<29 - Stamp']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age<29 - VAT</label>
                    {{ Form::text('vat_under29',null, ['class' => 'form-control','placeholder' => 'Age<29 - VAT']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age<29 - Gross Premium</label>
                    {{ Form::text('under29',null, ['class' => 'form-control','placeholder' => 'Age<29 - Gross Premium']) }} 
                </div>
                <!-- over29 -->
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age>29 - Net Premium </label>
                    {{ Form::text('net_over29',null, ['class' => 'form-control','placeholder' => 'Age>29 - Net Premium']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age>29 - Stamp</label>
                    {{ Form::text('stamp_over29',null, ['class' => 'form-control','placeholder' => 'Over29 - Stamp']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age>29 - VAT</label>
                    {{ Form::text('vat_over29',null, ['class' => 'form-control','placeholder' => 'Over29 - VAT']) }} 
                </div>           
                <div class="form-group">
                    <label class="control-label required" for="required-input">Age>29 - Gross Premium</label>
                    {{ Form::text('over29',null, ['class' => 'form-control','placeholder' => 'Age>29 - Gross Premium']) }} 
                </div>
                <!-- cctv -->
                <div class="form-group">
                    <label class="control-label required" for="required-input">CCTV - Net Premium </label>
                    {{ Form::text('net_cctv',null, ['class' => 'form-control','placeholder' => 'CCTV - Net Premium']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">CCTV - Stamp</label>
                    {{ Form::text('stamp_cctv',null, ['class' => 'form-control','placeholder' => 'CCTV - Stamp']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">CCTV - VAT</label>
                    {{ Form::text('vat_cctv',null, ['class' => 'form-control','placeholder' => 'CCTV - VAT']) }} 
                </div>         
                <div class="form-group">
                    <label class="control-label required" for="required-input">CCTV - Gross Premium</label>
                    {{ Form::text('cctv',null, ['class' => 'form-control','placeholder' => 'CCTV - Gross Premium']) }} 
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_button">Save changes</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>