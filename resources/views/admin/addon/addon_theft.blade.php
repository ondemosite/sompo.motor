@extends('admin.layout.default')
@section('title') Admins @endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.addon.modal.addon_theft')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-text-o"></i>@lang('admin_addon.topic4')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Addon Car Loss Type"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_addon.no')</th>
                                <th>@lang('admin_addon.sum_insured')</th>
                                <th>@lang('admin_addon.normal')</th>
                                <th>@lang('admin_addon.age_min_29')</th>
                                <th>@lang('admin_addon.age_max_29')</th>
                                <th>@lang('admin_addon.cctv')</th>
                                <th>@lang('admin_addon.admin')</th>
                                <th>@lang('admin_addon.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/addon_theft.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_list',route('admin.addon_theft_list')) }} 
{{ Form::hidden('hd_save',route('admin.addon_theft_save')) }} 
{{ Form::hidden('hd_get',route('admin.addon_theft_get')) }} 
{{ Form::hidden('hd_delete',route('admin.addon_theft_delete')) }} 
@endsection
