@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/report_policy.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-edit"></i>@lang('admin_report.topic_order')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-export"><i class="fa fa-file-excel-o"></i> @lang('admin_report.export')</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_report.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_order_number">@lang('admin_report.order_number')</label>
                                {{ Form::text('search_order_number',null, ['class' => 'form-control','placeholder' => trans('admin_report.order_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_driver_name">@lang('admin_report.fullname')</label>
                                {{ Form::text('search_driver_name',null, ['class' => 'form-control','placeholder' => trans('admin_report.fullname')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_insurance_ft_si">@lang('admin_report.insurance_amount')</label>
                                {{ Form::number('search_insurance_ft_si',null, ['class' => 'form-control','placeholder' => trans('admin_report.insurance_amount')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_car_licence">@lang('admin_report.car_licence')</label>
                                {{ Form::text('search_car_licence',null, ['class' => 'form-control','placeholder' => trans('admin_report.car_licence')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_report.create_date_from')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_report.create_date_from')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_report.create_date_to')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_report.create_date_to')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_status">@lang('admin_report.status')</label>
                                {{ Form::select('search_status',["WAITING"=>'รอชำระเงิน',"EXPIRE"=>'หมดอายุ','CANCEL'=>'ยกเลิก','INACTIVE'=>'INACTIVE','PAID'=>'ชำระเงินแล้ว'],null, ['class' => 'form-control','placeholder' => trans('admin_report.status')]) }}
                            </div>

                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_report.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_report.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light table-reponsive">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_report.no')</th>
                                <th>@lang('admin_report.order_number')</th>
                                <th>@lang('admin_report.fullname')</th>
                                <th>@lang('admin_report.vehicle')</th>
                                <th>@lang('admin_report.insurance')</th>
                                <th>@lang('admin_report.addon')</th>
                                <th>@lang('admin_report.compulsory')</th>
                                <th>@lang('admin_report.payment_amount')</th>
                                <th>@lang('admin_report.status')</th>
                                <th>@lang('admin_report.create_date')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/report_order.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_list',route('admin.report.order_list')) }} 
@include('admin.report.modal.order')
@endsection
