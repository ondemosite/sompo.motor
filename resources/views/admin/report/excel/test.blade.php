<table>
    <tr>
        <td><h2>{{$header['topic']}}</h2></td>
    </tr>
    <tr>
        <td><h3>Export Date:  <span style="font-weight:normal">{{$header['date']}}</span></td>
    </tr>
    <tr>
        <td><h4>Page : {{$header['page']}}</h4></td>
    </tr>
    <tr>
        <td>NO</td>
        @if(!empty($input['policy_number']))
            <td width="50px">POLICY NUMBER</td>
        @endif
    </tr>
</table>