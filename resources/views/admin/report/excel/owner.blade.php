    <table>
        <tr>
            <td><h2>{{$header['topic']}}</h2></td>
        </tr>
        <tr>
            <td><h3>Export Date:  <span style="font-weight:normal">{{$header['date']}}</span></td>
        </tr>
        <tr>
            <td><h4>Page : {{$header['page']}}</h4></td>
        </tr>
    </table>
    <table>
        <tr style="background-color:#C80027;color:#FFFFFF">
            <td width="10">NO</td>
            @if(!empty($input['policy_number']))
                <td width="50">POLICY NUMBER</td>
            @endif
            @if(!empty($input['policy_status']))
                <td width="20">POLICY STATUS</td>
            @endif
            @if(!empty($input['car_licence']))
                <td width="50">CAR LICENCE</td>
            @endif
            @if(!empty($input['is_advice']))
                <td width="10">AGREE ADVICE</td>
            @endif
            @if(!empty($input['name']) || !empty($input['lastname']))
                <td width="50">FULL NAME</td>
            @endif
            @if(!empty($input['idcard']))
                <td width="50">ID CARD</td>
            @endif
            @if(!empty($input['licence']))
                <td width="50">DRIVER LICENCE</td>
            @endif
            @if(!empty($input['birthdate']))
                <td width="50">BIRTH DATE</td>
            @endif
            @if(!empty($input['gender']))
                <td width="20">GENDER</td>
            @endif
            @if(!empty($input['address']))
                <td width="50">ADDRESS</td>
            @endif
            @if(!empty($input['tel']))
                <td width="30">TEL</td>
            @endif
            @if(!empty($input['email']))
                <td width="50">EMAIL</td>
            @endif
            @if(!empty($input['created_at']))
                <td width="50">CREATED AT</td>
            @endif
            @if(!empty($input['updated_at']))
                <td width="50">LASTEST UPDATE</td>
            @endif
        </tr>
        @foreach($data as $item)
        @php
            if($header['start']%2==0) $bg = "#FFFFFF";
            else $bg = "#FBDEE4";
        @endphp
        <tr style="background-color:{{$bg}}">
            <td>{{$header['start']++}}</td>
            @if(!empty($input['policy_number']))
                <td>{{$item->policy_number}}</td>
            @endif
            @if(!empty($input['policy_status']))
                <td>{{$item->status}}</td>
            @endif
            @if(!empty($input['car_licence']))
                <td>{{$item->car_licence." ".$item->car_province}}</td>
            @endif
            @if(!empty($input['is_advice']))
                <td>{{$item->is_advice==1?"YES":"NO"}}</td>
            @endif
            @if(!empty($input['name']) || !empty($input['lastname']))
                <td>{{ (!empty($input['name'])?$item->name:"").(!empty($input['lastname'])?" ".$item->lastname:"") }}</td>
            @endif         
            @if(!empty($input['idcard']))
                <td>{{$item->idcard}}</td>
            @endif
            @if(!empty($input['licence']))
                <td>{{$item->licence}}</td>
            @endif
            @if(!empty($input['birthdate']))
                <td>{{$item->birth_date}}</td>
            @endif
            @if(!empty($input['gender']))
                <td>{{$item->gender}}</td>
            @endif
            @if(!empty($input['address']))
                <td>{{$item->address}}</td>
            @endif
            @if(!empty($input['tel']))
                <td>{{$item->tel}}</td>
            @endif
            @if(!empty($input['email']))
                <td>{{$item->email}}</td>
            @endif
            @if(!empty($input['created_at']))
                <td>{{getLocaleDate($item->created_at,true)}}</td>
            @endif
            @if(!empty($input['updated_at']))
                <td>{{getLocaleDate($item->updated_at,true)}}</td>
            @endif
        </tr>
        @endforeach
    </table>