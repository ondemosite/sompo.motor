<table>
    <tr>
        <td><h2>{{$header['topic']}}</h2></td>
    </tr>
    <tr>
        <td><h3>Export Date:  <span style="font-weight:normal">{{$header['date']}}</span></td>
    </tr>
    <tr>
        <td><h4>Page : {{$header['page']}}</h4></td>
    </tr>
</table>
<table>
    <tr style="background-color:#C80027;color:#FFFFFF">
        <td width="10">NO</td>
        @if(!empty($input['order_number']))
            <td width="50">ORDER NUMBER</td>
        @endif
        @if(!empty($input['status']))
            <td width="50">ORDER STATUS</td>
        @endif
        @if(!empty($input['insurance_plan_name']))
            <td width="50">INSURANCE PLAN</td>
        @endif
        @if(!empty($input['insurance_ft_si']))
            <td width="50">SUM INSURED</td>
        @endif
        @if(!empty($input['main_driver']))
            <td width="30">NAME LASTNAME</td>
        @endif
        @if(!empty($input['compulsory_net_premium']))
            <td width="20">COMPULSORY</td>
        @endif
        @if(!empty($input['promotion_id']))
            <td width="40">PROMOTION</td>
        @endif
        @if(!empty($input['discount']))
            <td width="50">DISCOUNT</td>
        @endif
        @if(!empty($input['created_at']))
            <td width="50">CREATED AT</td>
        @endif
        @if(!empty($input['order_expire']))
            <td width="20">ORDER EXPIRE</td>
        @endif
        @if(!empty($input['payment_result']))
            <td width="30">BALANCE REQUIRED</td>
        @endif
        @if(!empty($input['gross_premium']))
            <td width="30">GROSS PREMIUM</td>
        @endif
        @if(!empty($input['theft_gross_premium']))
            <td width="30">THEFT GROSS PREMIUM</td>
        @endif
        @if(!empty($input['taxi_gross_premium']))
            <td width="50">TAXI GROSS PREMIUM</td>
        @endif
        @if(!empty($input['hb_gross_premium']))
            <td width="50">HB GROSS PREMIUM</td>
        @endif
        @if(!empty($input['carloss_gross_premium']))
            <td width="30">CARLOSS GROSS PREMIUM</td>
        @endif
        @if(!empty($input['vehicle_info']))
            <td width="30">VEHICLE INFO</td>
        @endif
        @if(!empty($input['car_licence']))
            <td width="30">CAR LICENCE</td>
        @endif
        @if(!empty($input['car_chassis_number']))
            <td width="30">CAR CHASSIS</td>
        @endif
        @if(!empty($input['car_cctv']))
            <td width="30">CAR CCTV</td>
        @endif
    </tr>
    @foreach($data as $item)
    @php
        if($header['start']%2==0) $bg = "#FFFFFF";
        else $bg = "#FBDEE4";
    @endphp
    <tr style="background-color:{{$bg}}">
        <td>{{$header['start']++}}</td>

        @if(!empty($input['order_number']))
            <td width="50">{{$item->order_number}}</td>
        @endif
        @if(!empty($input['status']))
            <td width="50">{{$item->status}}</td>
        @endif
        @if(!empty($input['insurance_plan_name']))
            <td width="50">{{$item->insurance_plan_name}}</td>
        @endif
        @if(!empty($input['insurance_ft_si']))
            <td width="50">{{$item->insurance_ft_si}}</td>
        @endif
        @if(!empty($input['main_driver']))
            <td width="30">{{$item->name." ".$item->lastname}}</td>
        @endif
        @if(!empty($input['compulsory_net_premium']))
            <td width="20">{{ !empty($item->compulsory_net_premium)?"YES":"NO" }}</td>
        @endif
        @if(!empty($input['promotion_id']))
            <td width="40">{{$item->promotion_id}}</td>
        @endif
        @if(!empty($input['discount']))
            <td width="50">{{$item->discount}}</td>
        @endif
        @if(!empty($input['created_at']))
            <td width="50">{{getLocaleDate($item->created_at,true)}}</td>
        @endif
        @if(!empty($input['order_expire']))
            <td width="20">{{getLocaleDate($item->order_expire,true)}}</td>
        @endif
        @if(!empty($input['payment_result']))
            <td width="30">{{ number_format($item->payment_result,2) }}</td>
        @endif
        @if(!empty($input['gross_premium']))
            <td width="30">{{number_format($item->gross_premium,2)}}</td>
        @endif
        @if(!empty($input['theft_gross_premium']))
            <td width="30">{{number_format($item->theft_gross_premium,2)}}</td>
        @endif
        @if(!empty($input['taxi_gross_premium']))
            <td width="50">{{number_format($item->taxi_gross_premium,2)}}</td>
        @endif
        @if(!empty($input['hb_gross_premium']))
            <td width="50">{{number_format($item->hb_gross_premium,2)}}</td>
        @endif
        @if(!empty($input['carloss_gross_premium']))
            <td width="30">{{number_format($item->carloss_gross_premium,2)}}</td>
        @endif
        @if(!empty($input['vehicle_info']))
            <td width="30">{{$item->brand." ".$item->model." ".$item->year}}</td>
        @endif
        @if(!empty($input['car_licence']))
            <td width="30">{{$item->car_licence}}</td>
        @endif
        @if(!empty($input['car_chassis_number']))
            <td width="30">{{$item->car_chassis_number}}</td>
        @endif
        @if(!empty($input['car_cctv']))
            <td width="30">{{$item->car_cctv==1?"YES":"NO"}}</td>
        @endif
    </tr>
    @endforeach
</table>