<table>
    <tr>
        <td><h2>{{$header['topic']}}</h2></td>
    </tr>
    <tr>
        <td><h3>Export Date:  <span style="font-weight:normal">{{$header['date']}}</span></td>
    </tr>
    <tr>
        <td><h4>Page : {{$header['page']}}</h4></td>
    </tr>
</table>
@php
$channel = config('payment_code.payment_channel_code');
$scheme = config('payment_code.payment_scheme');
@endphp

<table>
    <tr style="background-color:#C80027;color:#FFFFFF">
        <td width="10">NO</td>
        @if(!empty($input['policy_number']))
            <td width="30">POLICY NUMBER</td>
        @endif
        @if(!empty($input['policy_addon_number']))
            <td width="30">POLICY ADDON NUMBER</td>
        @endif
        @if(!empty($input['tax_note_number']))
            <td width="30">TAX NOTE NUMBER</td>
        @endif
        @if(!empty($input['order_number']))
            <td width="30">ORDER NUMBER</td>
        @endif
        @if(!empty($input['payment_number']))
            <td width="30">PAYMENT NUMBER</td>
        @endif
        @if(!empty($input['payment_channel']))
            <td width="20">PAYMENT CHANNEL</td>
        @endif
        @if(!empty($input['payment_status']))
            <td width="20">PAYMENT STATUS</td>
        @endif
        @if(!empty($input['endorsement']))
            <td width="10">ENDORSEMENT</td>
        @endif
        @if(!empty($input['status']))
            <td width="20">STATUS</td>
        @endif
        @if(!empty($input['created_at']))
            <td width="20">CREATED AT</td>
        @endif
        @if(!empty($input['payment_date']))
            <td width="20">PAYMENT DATE</td>
        @endif
        @if(!empty($input['mask_pan']))
            <td width="20">MASK PAN</td>
        @endif
        @if(!empty($input['total_premium']))
            <td width="20">TOTAL PREMIUM</td>
        @endif
        @if(!empty($input['net_premium']))
            <td width="20">NET PREMIUM</td>
        @endif
        @if(!empty($input['stamp']))
            <td width="20">STAMP</td>
        @endif
        @if(!empty($input['vat']))
            <td width="20">VAT</td>
        @endif
        @if(!empty($input['com_total_premium']))
            <td width="20">COMPULSORY TOTAL PREMIUM</td>
        @endif
        @if(!empty($input['com_net_premium']))
            <td width="20">COMPULSORY NET PREMIUM</td>
        @endif
        @if(!empty($input['com_stamp']))
            <td width="20">COMPULSORY STAMP</td>
        @endif
        @if(!empty($input['com_vat']))
            <td width="20">COMPULSORY VAT</td>
        @endif
        @if(!empty($input['net_discount']))
            <td width="20">NET PREMIUM DISCOUNT</td>
        @endif
        @if(!empty($input['product']))
            <td width="20">NET PREMIUM DISCOUNT</td>
        @endif
        @if(!empty($input['vehicle_brand']))
            <td width="20">VEHICLE BRAND</td>
        @endif
        @if(!empty($input['vehicle_model']))
            <td width="20">VEHICLE MODEL</td>
        @endif
        @if(!empty($input['vehicle_submodel']))
            <td width="20">VEHICLE SUB MODEL</td>
        @endif
        @if(!empty($input['vehicle_year']))
            <td width="20">VEHICLE YEAR</td>
        @endif
        @if(!empty($input['promo_code']))
            <td width="20">PROMOTION CODE</td>
        @endif

        @if(!empty($input['owner_prefix']))
            <td width="10">PREFIX</td>
        @endif
        @if(!empty($input['owner_name']))
            <td width="40">FIRSTNAME</td>
        @endif
        @if(!empty($input['owner_lastname']))
            <td width="40">LASTNAME</td>
        @endif
        @if(!empty($input['nationality']))
            <td width="20">NATIONALITY</td>
        @endif
        @if(!empty($input['id_card']))
            <td width="30">ID CARD</td>
        @endif
        @if(!empty($input['birth_date']))
            <td width="30">BIRTH DATE</td>
        @endif
        @if(!empty($input['tel']))
            <td width="30">TEL</td>
        @endif
        @if(!empty($input['email']))
            <td width="30">EMAIL</td>
        @endif
        @if(!empty($input['address']))
            <td width="50">ADDRESS</td>
        @endif
        @if(!empty($input['subdistrict']))
            <td width="30">SUBDISTRICT</td>
        @endif
        @if(!empty($input['district']))
            <td width="30">DISTRICT</td>
        @endif
        @if(!empty($input['province']))
            <td width="30">PROVINCE</td>
        @endif
        @if(!empty($input['postcode']))
            <td width="30">POSTCODE</td>
        @endif
    </tr>
    @foreach($data as $item)
    @php
        if($header['start']%2==0) $bg = "#FFFFFF";
        else $bg = "#FBDEE4";
    @endphp
    <tr style="background-color:{{$bg}}">
        <td>{{ ++$header['start'] }}</td>
        @if(!empty($input['policy_number']))
            <td>{{$item->policy_number}}</td>
        @endif
        @if(!empty($input['policy_addon_number']))
            <td>{{$item->policy_addon_number}}</td>
        @endif
        @if(!empty($input['tax_note_number']))
            <td>{{$item->tax_note_number}}</td>
        @endif
        @if(!empty($input['order_number']))
            <td>{{$item->order_number}}</td>
        @endif
        @if(!empty($input['payment_number']))
            <td>{{$item->payment_number}}</td>
        @endif
        @if(!empty($input['payment_channel']))
            <td>{{ $scheme[$item->payment_scheme ] }}</td>
        @endif
        @if(!empty($input['payment_status']))
            <td>'SUCCESS'</td>
        @endif
        @if(!empty($input['endorsement']))
            <td>{{$item->endorse_no}}</td>
        @endif
        @if(!empty($input['status']))
            <td>{{$item->status}}</td>
        @endif
        @if(!empty($input['created_at']))
            <td>{{getLocaleDate($item->created_at,true)}}</td>
        @endif
        @if(!empty($input['payment_date']))
            <td>{{getLocaleDate($item->paid_date,true)}}</td>
        @endif
        @if(!empty($input['mask_pan']))
            <td>{{ $item->credit_number }}</td>
        @endif
        @if(!empty($input['total_premium']))
            <td>{{ $item->b_net_premium + $item->b_stamp + $item->b_vat }}</td>
        @endif
        @if(!empty($input['net_premium']))
            <td>{{ $item->b_net_premium }}</td>
        @endif
        @if(!empty($input['stamp']))
            <td>{{ $item->b_stamp }}</td>
        @endif
        @if(!empty($input['vat']))
            <td>{{ $item->b_vat }}</td>
        @endif
        @if(!empty($input['com_total_premium']))
            <td>{{ $item->compulsory_net_premium + $item->compulsory_stamp + $item->compulsory_vat }}</td>
        @endif
        @if(!empty($input['com_net_premium']))
            <td>{{ $item->compulsory_net_premium }}</td>
        @endif
        @if(!empty($input['com_stamp']))
            <td>{{ $item->compulsory_stamp }}</td>
        @endif
        @if(!empty($input['com_vat']))
            <td>{{ $item->compulsory_vat }}</td>
        @endif
        @if(!empty($input['net_discount']))
            <td>{{ $item->net_discount }}</td>
        @endif
        @if(!empty($input['product']))
            <td>{{$item->insurance_plan_name}}</td>
        @endif
        @if(!empty($input['vehicle_brand']))
            <td>{{$item->brand}}</td>
        @endif
        @if(!empty($input['vehicle_model']))
            <td>{{$item->model}}</td>
        @endif
        @if(!empty($input['vehicle_submodel']))
            <td>{{$item->model_type}}</td>
        @endif
        @if(!empty($input['vehicle_year']))
            <td>{{$item->year}}</td>
        @endif
        @if(!empty($input['promo_code']))
            <td>{{$item->promotion_code}}</td>
        @endif

        @if(!empty($input['owner_prefix']))
            <td>{{ $item->prefix_name }}</td>
        @endif
        @if(!empty($input['owner_name']))
            <td>{{ $item->name }}</td>
        @endif
        @if(!empty($input['owner_lastname']))
            <td>{{ $item->lastname }}</td>
        @endif
        @if(!empty($input['nationality']))
            <td>Thai</td>
        @endif
        @if(!empty($input['id_card']))
            <td>{{ strval($item->idcard) }}</td>
        @endif
        @if(!empty($input['birth_date']))
            <td>{{ getLocaleDate($item->birth_date) }}</td>
        @endif
        @if(!empty($input['tel']))
            <td>{{ $item->tel }}</td>
        @endif
        @if(!empty($input['email']))
            <td>{{ $item->email }}</td>
        @endif
        @if(!empty($input['address']))
            <td>{{ $item->address }}</td>
        @endif
        @if(!empty($input['subdistrict']))
            <td>{{ $item->subdistrict_name }}</td>
        @endif
        @if(!empty($input['district']))
            <td>{{ $item->district_name }}</td>
        @endif
        @if(!empty($input['province']))
            <td>{{ $item->province_name }}</td>
        @endif
        @if(!empty($input['postcode']))
            <td>{{ $item->postalcode }}</td>
        @endif

    </tr>
    @endforeach
</table>