<div class="modal fade" id="modal-export" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-file-text-o p-r-1"></i><span>@lang('admin_report.export_order')</span></h4>
            </div> 
            <div class="modal-body">
               {{ Form::open(array('url' => route("admin.report.excel_order"),'name'=>'form_export','class'=>'form_export','id'=>'form_export')) }}
               <p><b>@lang('admin_report.select_column')</b></p>
               <div class="row">
                   <div class="col-xs-6 border-right">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="order_number" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.order_number')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="status" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.order_status')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="insurance_plan_name" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.insurance_plan')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="insurance_ft_si" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.sum_insured')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="main_driver" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.fullname')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="compulsory_gross_premium" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.compulsory')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="promotion_id" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.promotion_id')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="discount" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.discount')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="created_at" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.create_date')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="order_expire" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.order_expire')
                        </label>
                   </div>
                   <div class="col-xs-6">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="payment_result" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.balance_required')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="gross_premium" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.policy_gross')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="theft_gross_premium" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.theft_gross')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="taxi_gross_premium" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.taxi_gross')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="hb_gross_premium" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.hb_gross')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="carloss_gross_premium" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.carloss_gross')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="vehicle_info" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.vehicle')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="car_licence" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.car_licence')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="car_chassis_number" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.car_chasis_number')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="car_cctv" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.cctv')
                        </label>
                   </div>
               </div>
               <hr/>
               <p><b>@lang('admin_report.select_record')</b></p>
               <div class="row">
                   <div class="col-xs-12">
                        <div class="form-group">
                            <label>@lang('admin_report.item_per_page')</label>
                            {{ Form::select('item_per_page',['100'=>'100','500'=>'500','1000'=>'1,000','2000'=>'2,000','4000'=>'4,000','6000'=>'6,000',
                                '8000'=>'8,000','10000'=>'10,000','20000'=>'20,000','30000'=>'30,000','50000'=>'50,000'
                            ],null, ['class' => 'form-control','placeholder' => trans('admin_report.item_per_page')]) }}
                        </div>
                        <div class="form-group">
                            <label>@lang('admin_report.select_page')</label>
                            {{ Form::select('page_number',[],null, ['class' => 'form-control','placeholder' => trans('admin_report.select_page')]) }}
                        </div>
                   </div>
               </div>
               <div class="text-right">
                    <button type="button" class="btn" data-dismiss="modal">@lang('admin_report.close')</button>
                    <button type="button" class="btn btn-primary" id="submit_button">@lang('admin_report.export')</button>
                </div> <!-- /modal-footer -->
                {{ Form::hidden('total_item',null) }} 
                {{ Form::hidden('total_page',null) }} 
                {{ Form::hidden('input_order_number',null) }}
                {{ Form::hidden('input_driver_name',null) }}
                {{ Form::hidden('input_insurance_ft_si',null) }}
                {{ Form::hidden('input_car_licence',null) }}
                {{ Form::hidden('input_from_date',null) }}
                {{ Form::hidden('input_to_date',null) }}
                {{ Form::hidden('input_status',null) }}
               {{ Form::close() }}
            </div> <!-- /modal-body -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>