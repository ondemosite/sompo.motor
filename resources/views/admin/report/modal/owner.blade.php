<div class="modal fade" id="modal-export" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-file-text-o p-r-1"></i><span>@lang('admin_report.export_owner')</span></h4>
            </div> 
            <div class="modal-body">
               {{ Form::open(array('url' => route("admin.report.excel_owner"),'name'=>'form_export','class'=>'form_export','id'=>'form_export')) }}
               <p><b>@lang('admin_report.select_column')</b></p>
               <div class="row">
                   <div class="col-xs-6 border-right">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="policy_number" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.policy_number')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="policy_status" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.policy_status')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="car_licence" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.car_licence')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="is_advice" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.advice_state')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="name" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.name')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="lastname" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.lastname')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="idcard" checked>
                            <span class="custom-control-indicator"></span>@lang('common.idcard')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="licence" checked>
                            <span class="custom-control-indicator"></span>@lang('common.driver_licence')
                        </label>
                   </div>
                   <div class="col-xs-6">
                        
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="birthdate" checked>
                            <span class="custom-control-indicator"></span>@lang('common.birthdate')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="gender" checked>
                            <span class="custom-control-indicator"></span>@lang('common.sex')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="address" checked>
                            <span class="custom-control-indicator"></span>@lang('common.address')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="tel" checked>
                            <span class="custom-control-indicator"></span>@lang('common.phone')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="email" checked>
                            <span class="custom-control-indicator"></span>@lang('common.email')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="created_at" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.created_date')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="updated_at" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.lastest_update')
                        </label>
                   </div>
               </div>
               <hr/>
               <p><b>@lang('admin_report.select_record')</b></p>
               <div class="row">
                   <div class="col-xs-12">
                        <div class="form-group">
                            <label>@lang('admin_report.')Items Per Page</label>
                            {{ Form::select('item_per_page',['100'=>'100','500'=>'500','1000'=>'1,000','2000'=>'2,000','4000'=>'4,000','6000'=>'6,000',
                                '8000'=>'8,000','10000'=>'10,000','20000'=>'20,000','30000'=>'30,000','50000'=>'50,000'
                            ],null, ['class' => 'form-control','placeholder' => trans('admin_report.select_record')]) }}
                        </div>
                        <div class="form-group">
                            <label>@lang('admin_report.')Select Page</label>
                            {{ Form::select('page_number',[],null, ['class' => 'form-control','placeholder' => trans('admin_report.select_page')]) }}
                        </div>
                   </div>
               </div>
               <div class="text-right">
                    <button type="button" class="btn" data-dismiss="modal">@lang('admin_report.close')</button>
                    <button type="button" class="btn btn-primary" id="submit_button">@lang('admin_report.export')</button>
                </div> <!-- /modal-footer -->
                {{ Form::hidden('total_item',null) }} 
                {{ Form::hidden('total_page',null) }} 
                {{ Form::hidden('input_policy_number',null) }}
                {{ Form::hidden('input_driver_name',null) }}
                {{ Form::hidden('input_email',null) }}
                {{ Form::hidden('input_tel',null) }}
                {{ Form::hidden('input_car_licence',null) }}
                {{ Form::hidden('input_advice',null) }}
                {{ Form::hidden('input_from_date',null) }}
                {{ Form::hidden('input_to_date',null) }}
                {{ Form::hidden('input_status',null) }}
               {{ Form::close() }}
            </div> <!-- /modal-body -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>