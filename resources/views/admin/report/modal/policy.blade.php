<div class="modal fade" id="modal-export" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-file-text-o p-r-1"></i><span>@lang('admin_report.topic')</span></h4>
            </div> 
            <div class="modal-body">
               {{ Form::open(array('url' => route("admin.report.excel_policy"),'name'=>'form_export','class'=>'form_export','id'=>'form_export')) }}
               <p><b>@lang('admin_report.select_column')</b></p>
               <div class="row">
                   
                   <div class="col-xs-6 border-right">
                   <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="policy_number" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.policy_number')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="tax_note_number" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.tax_note')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="order_number" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.order_number')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="payment_number" checked>
                            <span class="custom-control-indicator"></span>Payment Number
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="payment_channel" checked>
                            <span class="custom-control-indicator"></span>Payment Channel
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="payment_status" checked>
                            <span class="custom-control-indicator"></span>Payment Status
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="endorsement" checked>
                            <span class="custom-control-indicator"></span>Endorse Ment
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="status" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.status')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="created_at" checked>
                            <span class="custom-control-indicator"></span>@lang('admin_report.created_date')
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="payment_date" checked>
                            <span class="custom-control-indicator"></span>Payment Date
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="mask_pan" checked>
                            <span class="custom-control-indicator"></span>Mask Pan
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="total_premium" checked>
                            <span class="custom-control-indicator"></span>Total Premium
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="net_premium" checked>
                            <span class="custom-control-indicator"></span>Net Premium
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="stamp" checked>
                            <span class="custom-control-indicator"></span>Stamp
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="vat" checked>
                            <span class="custom-control-indicator"></span>Vat
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="com_total_premium" checked>
                            <span class="custom-control-indicator"></span>Compulsory Total Premium
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="com_net_premium" checked>
                            <span class="custom-control-indicator"></span>Compulsory Net Premium
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="com_stamp" checked>
                            <span class="custom-control-indicator"></span>Compulsory Stamp
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="com_vat" checked>
                            <span class="custom-control-indicator"></span>Compulsory Vat
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="net_discount" checked>
                            <span class="custom-control-indicator"></span>Net Discount
                        </label>
                    </div>
                    <div class="col-xs-6 border-right">
                        
                        
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="product" checked>
                            <span class="custom-control-indicator"></span>Product
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="vehicle_brand" checked>
                            <span class="custom-control-indicator"></span>Vehicle Brand
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="vehicle_model" checked>
                            <span class="custom-control-indicator"></span>Vehicle Model
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="vehicle_submodel" checked>
                            <span class="custom-control-indicator"></span>Vehicle Sub Model
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="vehicle_year" checked>
                            <span class="custom-control-indicator"></span>Vehicle Year
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="promo_code" checked>
                            <span class="custom-control-indicator"></span>Promotion Code
                        </label>

                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="owner_prefix" checked>
                            <span class="custom-control-indicator"></span>Prefix
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="owner_name" checked>
                            <span class="custom-control-indicator"></span>Name
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="owner_lastname" checked>
                            <span class="custom-control-indicator"></span>Lastname
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="nationality" checked>
                            <span class="custom-control-indicator"></span>Nationality
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="id_card" checked>
                            <span class="custom-control-indicator"></span>ID Card
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="birth_date" checked>
                            <span class="custom-control-indicator"></span>Birth Date
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="tel" checked>
                            <span class="custom-control-indicator"></span>Tel
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="email" checked>
                            <span class="custom-control-indicator"></span>Email
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="address" checked>
                            <span class="custom-control-indicator"></span>Address
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="subdistrict" checked>
                            <span class="custom-control-indicator"></span>Subdistrict
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="district" checked>
                            <span class="custom-control-indicator"></span>District
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="province" checked>
                            <span class="custom-control-indicator"></span>Province
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="postcode" checked>
                            <span class="custom-control-indicator"></span>Postcode
                        </label>
                   </div>
               </div>
               <hr/>
               <p><b>@lang('admin_report.select_record')</b></p>
               <div class="row">
                   <div class="col-xs-12">
                        <div class="form-group">
                            <label>@lang('admin_report.item_per_page')</label>
                            {{ Form::select('item_per_page',['100'=>'100','500'=>'500','1000'=>'1,000','2000'=>'2,000','4000'=>'4,000','6000'=>'6,000',
                                '8000'=>'8,000','10000'=>'10,000','20000'=>'20,000','30000'=>'30,000','50000'=>'50,000'
                            ],null, ['class' => 'form-control','placeholder' => trans('admin_report.select_item_page')]) }}
                        </div>
                        <div class="form-group">
                            <label>@lang('admin_report.select_page')</label>
                            {{ Form::select('page_number',[],null, ['class' => 'form-control','placeholder' => trans('admin_report.select_page')]) }}
                        </div>
                   </div>
               </div>
               <div class="text-right">
                    <button type="button" class="btn" data-dismiss="modal">ปิด</button>
                    <button type="button" class="btn btn-primary" id="submit_button">@lang('admin_report.export')</button>
                </div> <!-- /modal-footer -->
                {{ Form::hidden('total_item',null) }} 
                {{ Form::hidden('total_page',null) }} 
                {{ Form::hidden('input_policy_number',null) }}
                {{ Form::hidden('input_driver_name',null) }}
                {{ Form::hidden('input_email',null) }}
                {{ Form::hidden('input_tel',null) }}
                {{ Form::hidden('input_car_licence',null) }}
                {{ Form::hidden('input_from_date',null) }}
                {{ Form::hidden('input_to_date',null) }}
                {{ Form::hidden('input_status',null) }}
               {{ Form::close() }}
            </div> <!-- /modal-body -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>