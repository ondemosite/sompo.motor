    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        @page {
            margin-top: 0.13in;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        @font-face {
            font-family: 'THSaraban';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSaraban';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSaraban';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSaraban';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body{
            font-family: 'THSaraban';
            font-size:12pt;
            width:100%;
        }
        .page-break {
            page-break-after: always;
        }

        /*  Content */
        .h-padding{
            padding:0pt 20pt;
        }
        .g-padding{
            padding:0px 5pt;
        }
        .no-border-left{
            border-left:0pt !important;
        }
        .no-border-right{
            border-right:0pt !important;
        }
        .no-border-bottom{
            border-bottom:0pt !important;
        }
        .no-border-top{
            border-top:0pt !important;
        }
        .border-top{
            border-top:0.1pt solid #000000 !important;
        }
        .large-text{
            font-weight:bold;
            font-size:36pt;
        }
        .header-text{
            font-weight:bold;
            font-size:16pt;
        }
        .line-br{
            padding-top:2pt;
            padding-bottom:2pt;
            line-height:9pt;
        }
        .text-area{
            height:30pt;
            vertical-align:text-top;
        }
        .head{
            margin-left:-5pt;
        }
        /* element */
        p{
            padding:0pt;
            margin:0pt;
            font-size:12pt;
        }
        h1{
            padding:0px;
            margin:0px;
        }
        td,th {
            border:0.1pt solid #000000;
            border-top: 0px;
            padding:0pt 6pt;
            font-weight:100;
        }
        td > *,th > *{
            margin-top:-4pt;
            padding-bottom:3pt;
        }
        table.sub-table{
            padding-top:10pt;
        }
        table.sub-table td{
            border:0px;
            padding-left:0px;
        }
        .checkbox {
            position:absolute;
            width:9pt;
            height:8pt;
            border: 1px solid #000;
            margin-top:5pt;
        }
        .label-check{
            margin-left:15pt;
            line-height:13pt;
        }
        .text-underline{
            border-bottom:1px solid;
        }
        
        /* This is what simulates a checkmark icon */
        .checkbox.checked:after {
            content: '';
            display: block;
            width: 3pt;
            height: 7pt;
            
            /* "Center" the checkmark */
            position:relative;
            top:-1pt;
            left:3pt;
            
            border: solid #000;
            border-width: 0 1.5pt 1.5pt 0;
            transform: rotate(45deg);
        }
        /* Content */
        .stamp{
            padding-top:-6pt;
            padding-bottom:-1pt;
            margin:2pt 0pt 2pt 0pt;
            border:1px solid #FF0000;
            color:#FF0000;
            text-align:center;
            width:60pt;
        }
        div.alert-text{
            position:absolute;
            width:100%;
            text-align:center;
            margin-top:130pt;
        }
        .sign{
            position:relative;
            width:120pt;
            border-bottom:1pt dashed #000000;
            margin:0 auto;
        }
        .sign-detail{
            text-align:center;
        }
        .sign > img{
            text-align:center;
            width:110pt;
        }
        .company-stamp{
            position:absolute;
            margin-top:-40pt;
            top:-15pt;
            margin-left:90pt;
        }
        .company-stamp > img{
            width:170pt;
        }
        </style>
    </head>
    <body>
    <div class="head">
        <img src="{{ asset('images/company/jF463JbDq31gdVYhPm3d/w4EMxrbjyuzSUKKgzHMR.png') }}" width="435pt" />
    </div>
    <div class="body">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="border-top h-padding" width="68%" align="center"><h1 class="header-text">ตารางกรมธรรม์ประกันภัยรถยนต์แบบคุ้มครองเฉพาะภัย</h1></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="15.3%"><p>รหัสบริษัท : SIT</p></td>
                <td class="no-border-right no-border-left" width="17%"><p>กรมธรรม์ประกันภัยเลขที่</p></td>
                <td class="no-border-right no-border-left" width="40.8%"><p>{{ $init->policy_number }}</p></td>
                <td class="no-border-right no-border-left" width="14%" align="right"><p>อาณาเขตคุ้มครอง</p></td>
                <td class="no-border-left" width="10%" align="left"><p>ประเทศไทย</p></td>
            </tr>
            <tr>
                <td class="no-border-right no-border-bottom" ><p>ผู้เอาประกันภัย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ชื่อ</p></td>
                <td class="no-border-right no-border-left no-border-bottom" colspan="2"><p>{{ $init->policy_name()->first()->name." ".$init->policy_name()->first()->lastname }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left no-border-bottom" align="left"><p>-</p></td>
            </tr>
            <tr>
                <td class="no-border-right text-area" align="right"><p>ที่อยู่</p></td>
                <td class="no-border-left no-border-right text-area" valign="top" colspan="2" style="line-height:10pt;padding-top:5pt;"><p>{{ $init->policy_name()->first()->address }}</p></td>
                <td class="no-border-left no-border-right">&nbsp;</td>
                <td class="no-border-left">&nbsp;</td>
            </tr>
        </table>


        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right no-border-bottom" width="11%"><p>ผู้ขับขี่ 1</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="36%"><p>{{ !empty($init->driver1)?($init->driver_1()->first()->name." ".$init->driver_1()->first()->lastname):'' }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="10%" align="right"><p>วัน/เดือน/ปีเกิด</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="18%"><p>{{ !empty($init->driver1)?(getLocaleDate($init->driver_1()->first()->birth_date)):'-'  }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="7%" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left no-border-bottom" align="left" width="18%"><p>-</p></td>
            </tr>
            <tr>
                <td class="no-border-right" width="11%"><p>ผู้ขับขี่ 2</p></td>
                <td class="no-border-right no-border-left" width="36%"><p>{{ !empty($init->driver2)?($init->driver_2()->first()->name." ".$init->driver_2()->first()->lastname):'' }}</p></td>
                <td class="no-border-right no-border-left" width="10%" align="right"><p>วัน/เดือน/ปีเกิด</p></td>
                <td class="no-border-right no-border-left" width="18%"><p>{{ !empty($init->driver2)?(getLocaleDate($init->driver_2()->first()->birth_date)):'-'  }}</p></td>
                <td class="no-border-right no-border-left" width="7%" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left" align="left" width="18%"><p>-</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="8.7%" align="left"><p>ผู้รับประโยชน์</p></td>
                <td class="no-border-left no-border-right" width="60%" align="left"><p>-</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" witdh="22%"><p>ระยะเวลาประกันภัย : เริ่มต้นวันที่</p></td>
                <td class="no-border-left no-border-right" align="left" width="18%"><p>{{ getLocaleDate($init->insurance_start) }}</p></td>
                <td class="no-border-left no-border-right" width="11%"><p>เวลา 00:00 น.</p></td>
                <td class="no-border-left no-border-right" align="right" width="19%"><p>สิ้นสุดวันที่</p></td>
                <td class="no-border-left no-border-right" width="17%"><p>{{ getLocaleDate($init->insurance_expire) }}</p></td>
                <td class="no-border-left" align="left" width="13%"><p>เวลา 00:00 น.</p></td>
            </tr>
            <tr>
                <td colspan="6"><p>รายการรถยนต์ที่เอาประกันภัย</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ลำดับ</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">รหัส</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ชื่อรถยนต์/รุ่น</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">เลขทะเบียน</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">เลขตัวถัง</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ปีรุ่น</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">แบบตัวถัง</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">จำนวนที่นั่ง/ขนาด/น้ำหนัก</td>
                </tr>
                <tr>
                    <td align="center" width="5%"><p>001</p></td>
                    <td align="center" width="5%"><p>{{ $init->vehicle_info()->first()->mortor_code_av }}</p></td>
                    <td class="line-br" align="center" width="16%"><p>{{ strtoupper($init->vehicle_info()->first()->brand." ".$init->vehicle_info()->first()->model) }}</p></td>
                    <td class="line-br" align="center" width="10%"><p>{{ $init->car_licence }}<br/>{{ $init->car_province()->first()->name_th }}</p></td>
                    <td align="center" width="19%"><p>{{ $init->car_chassis_number }}</p></td>
                    <td align="center" width="5%"><p>{{ $init->vehicle_info()->first()->year }}</p></td>
                    <td align="center" width="10%"><p>{{ $init->vehicle_info()->first()->body_type }}</p></td>
                    <td align="center" width="15%"><p>{{$init->vehicle_info()->first()->car_seat."/".number_format($init->vehicle_info()->first()->cc,0)}}cc/-</p></td>
                </tr>
                <tr>
                    <td colspan="8">
                        <p>จำนวนเงินเอาประกันภัย : กรมธรรม์ประกันภัยนี้ให้การคุ้มครองเฉพาะข้อตกลงคุ้มครองที่มีจำนวนเงินเอาประกันภัยระบุไว้เท่านั้น</p>
                    </td>
                </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" width="20%"><p>ความคุ้มครองหลัก(1)</p></td>
                <td style="padding-top:3pt;padding-bottom:0pt;" class="line-br" width="30.5%" align="center"><p>ความคุ้มครองความเสียหายต่อตัวรถยนต์ตาม<br/>เอกสารแนบท้ายแบบคุ้มครองเฉพาะภัย (2)</p></td>
                <td style="padding-top:3pt;padding-bottom:0pt;" class="line-br" width="39%" align="center"><p>ความคุ้มครองอื่นๆ ตามเอกสารแนบท้าย<br/>และเอกสารแนบท้ายแบบคุ้มครองเฉพาะภัย (3)</p></td>
            </tr>
            <tr>
                <td class="line-br" align="left">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left"><p>ความคุ้มครองความรับผิดต่อบุคคลภายนอก</p></td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding-top:1.4pt"><p>1.1)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ.</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_person_damage_person,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คน</p></td>
                        </tr>
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_person_damage_once,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1.2)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายต่อทรัพย์สิน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_stuff_damage,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1.2)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายส่วนแรก</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_deduct,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>2)</p></td>
                            <td valign="top" align="left"><p>ความคุ้มครองความเสียหายต่อตัวรถยนต์</p></td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding-top:0pt"><p>2.1)</p></td>
                            <td valign="top" align="left"><p>รถยนต์สูญหาย/ไฟไหม้</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            @if($init->insurance_plan_id==1)
                                <td width="80%" align="right"><p>{{number_format($init->insurance_ft_si,2)}}</p></td>
                            @else
                                <td width="80%" align="right"><p>-</p></td>
                            @endif
                            <td width="20%" align="right"><p>บาท</p></td>
                        </tr>
                    </table>
                </td>
                <td class="line-br" valign="top">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left" style="padding-top:-2pt;"><p>ความคุ้มครองความเสียหายต่อตัวรถยนต์ เนื่องจากการชนกับยานพาหนะทางบก (ร.ย.ภ. 10)</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_ft_si,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <div class="alert-text">
                        <h1 class="large-text">ไม่รวม พ.ร.บ.</h1>
                    </div>
                </td>
                <td class="line-br" align="center" width="25%" valign="top">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left"><p>อุบัติเหตุส่วนบุคคล</p></td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding-top:1.4pt"><p>1.1)</p></td>
                            <td valign="top" style="padding-top:1.4pt" align="left"><p>เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="45%" align="left"><p>ก) ผู้ขับขี่ 1 คน</p></td>
                            <td width="30%" align="left"><p>{{number_format($init->insurance_death_disabled,2)}}</p></td>
                            <td width="15%" align="left"><p>บาท</p></td>
                        </tr>
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="45%" align="left"><p>ข) ผู้โดยสาร 4 คน</p></td>
                            <td width="30%" align="left"><p>{{number_format($init->insurance_death_disabled,2)}}</p></td>
                            <td width="15%" align="left"><p>บาท/คน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1.2)</p></td>
                            <td valign="top" align="left"><p>ทุพพลภาพชั่วคราว</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="35%" align="left"><p>ก) ผู้ขับขี่ 1 คน</p></td>
                            <td width="25%" align="right"><p>0.00</p></td>
                            <td align="right" style="padding-right:0px;"><p>บาท/คน/สัปดาห์</p></td>
                        </tr>
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="35%" align="left"><p>ข) ผู้โดยสาร - คน</p></td>
                            <td width="25%" align="right"><p>0.00</p></td>
                            <td align="right" style="padding-right:0px;"><p>บาท/คน/สัปดาห์</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>2)</p></td>
                            <td valign="top" align="left"><p>ค่ารักษาพยาบาล</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="77%" align="right"><p>{{number_format($init->insurance_medical_fee,2)}}</p></td>
                            <td width="23%" align="right"><p>บาท/คน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>3)</p></td>
                            <td valign="top" align="left"><p>การประกันตัวผู้ขับขี่</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="77%" align="right"><p>{{number_format($init->insurance_bail_driver,2)}}</p></td>
                            <td width="23%" align="right"><p>บาท/ครั้ง</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"><p>เบี้ยประกันภัย</p></td>
            </tr>
            <tr>
                <td class="no-border-right" width="46.5%"><p>เบี้ยประกันภัยตามความคุ้มครอง (1) และ (3)</p></td>
                <td class="no-border-left no-border-right" align="right" width="10%"><p>{{number_format($init->order()->first()->basic_premium_cover+$init->order()->first()->additional_premium_cover,2)}}</p></td>
                <td class="no-border-left" align="left"><p>บาท (เบี้ยประกันภัยนี้ได้หักส่วนลดกรณีระบุชื่อผู้ขับขี่แล้ว)</p></td>
            </tr>
        </table>
        <table class="unknow" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="18%"><p>ส่วนลด/ส่วนเพิ่มเติม</p></td>
                <td class="no-border-left no-border-right" width="10%"><p>ความเสียหายส่วนแรก</p></td>
                <td class="no-border-left no-border-right" width="11%" align="center"><p>{{number_format($init->order()->first()->deduct,2)}} บาท</p></td>
                <td class="no-border-left no-border-right" width="8%"><p>ส่วนลดกลุ่ม</p></td>
                <td class="no-border-left no-border-right" width="8%" align="center"><p>{{number_format($init->order()->first()->fleet,2)}} บาท</p></td>
                <td class="no-border-left no-border-right" width="5%"><p>ประวัติดี</p></td>
                <td class="no-border-left no-border-right" width="12%" align="center"><p>{{number_format($init->order()->first()->ncb,2)}} บาท</p></td>
                <td class="no-border-left"><p>รวมส่วนลด -{{number_format(abs($init->order()->first()->deduct)+abs($init->order()->first()->fleet)+abs($init->order()->first()->ncb)+abs($init->order()->first()->cctv_discount)+abs($init->order()->first()->direct)+abs($init->order()->first()->discount),2)}} บาท</p></td>
            </tr>
            <tr>
                <td class="no-border-right" width="18%"><p>ความคุ้มครอง (1) และ (3)</p></td>
                <td colspan="7" align="left"><p>ประวัติเพิ่ม 0.00 บาท</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="23%"><p>เบี้ยประกันภัยตามความคุ้มครอง (2)</p></td>
                <td class="no-border-right no-border-left" width="14.7%" align="right"><p>{{number_format($init->order()->first()->od_total_premium,2)}}</p></td>
                <td class="no-border-left"><p>บาท</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right"><p>ส่วนลดอื่นๆ</p></td>
                <td class="no-border-right no-border-left" align="right"><p>-{{number_format(abs($init->order()->first()->cctv_discount)+abs($init->order()->first()->direct)+abs($init->order()->first()->discount),2)}}</p></td>
                <td class="no-border-left no-border-right" align="left"><p>บาท</p></td>
                <td class="no-border-left " align="right" colspan="6"><p><span class="text-underline" style="font-weight:bold;">หมายเหตุ</span> เอกสารแนบท้ายข้อยกเว้นภัยก่อการร้าย(ร.ย.30)</p></td>
            </tr>
            <tr>
                <td class="no-border-right"><p>เบี้ยประกันภัยสุทธิ</p></td>
                <td class="no-border-right no-border-left" align="right"><p>{{number_format($init->order()->first()->insurance_net_premium,2)}}</p></td>
                <td class="no-border-left" align="left"><p>บาท</p></td>
                <td class="no-border-left no-border-right" align="left"><p>อากรแสตมป์</p></td>
                <td class="no-border-left" align="right"><p>{{number_format($init->order()->first()->stamp,2)}} บาท</p></td>
                <td class="no-border-left no-border-right" align="left"><p>ภาษีมูลค่าเพิ่ม</p></td>
                <td class="no-border-left" align="right"><p>{{number_format($init->order()->first()->vat,2)}} บาท</p></td>
                <td class="no-border-left no-border-right" align="left"><p>รวม</p></td>
                <td class="no-border-left" align="right"><p>{{number_format($init->order()->first()->insurance_net_premium+$init->order()->first()->stamp+$init->order()->first()->vat,2)}} บาท</p></td>
            </tr>
            <tr>
                <td colspan="9"><p>การใช้รถยนต์ : ใช้ส่วนบุคคล ไม่ใช้รับจ้างหรือให้เช่า</p></td>
            </tr>
        </table>
        
    </div>
    
    </body>
    </html>
    
    

    