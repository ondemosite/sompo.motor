@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/report_owner.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-text-o"></i>@lang('admin_report.topic_owner')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-export"><i class="fa fa-file-excel-o"></i>@lang('admin_report.export')</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_report.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_policy_number">@lang('admin_report.policy_number')</label>
                                {{ Form::text('search_policy_number',null, ['class' => 'form-control','placeholder' => trans('admin_report.policy_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_driver_name">@lang('admin_report.fullname')</label>
                                {{ Form::text('search_driver_name',null, ['class' => 'form-control','placeholder' => trans('admin_report.fullname')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_email">@lang('admin_report.email')</label>
                                {{ Form::text('search_email',null, ['class' => 'form-control','placeholder' => trans('admin_report.email')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_tel">@lang('admin_report.tel')</label>
                                {{ Form::text('search_tel',null, ['class' => 'form-control','placeholder' => trans('admin_report.tel')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_car_licence">@lang('admin_report.car_licence')</label>
                                {{ Form::text('search_car_licence',null, ['class' => 'form-control','placeholder' => trans('admin_report.car_licence')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_report.create_date_from')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_report.create_date_from')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_report.create_date_to')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_report.create_date_to')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_advice">@lang('admin_report.agree')</label>
                                {{ Form::select('search_advice',["YES"=>'Yes',"NO"=>'No'],null, ['class' => 'form-control','placeholder' => trans('admin_report.agree')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_status">@lang('admin_report.policy_status')</label>
                                {{ Form::select('search_status',["NORMAL"=>'ปกติ',"EDIT"=>'แก้ไข','CANCEL'=>'ยกเลิก','EXPIRE'=>'หมดอายุ','INACTIVE'=>'INACTIVE'],null, ['class' => 'form-control','placeholder' => trans('admin_report.policy_status')]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_report.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_report.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light table-reponsive">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_report.no')</th>
                                <th>@lang('admin_report.policy_number')</th>
                                <th>@lang('admin_report.car_licence')</th>
                                <th>@lang('admin_report.fullname')</th>
                                <th>@lang('common.sex')</th>
                                <th>@lang('admin_report.email')</th>
                                <th>@lang('admin_report.tel')</th>
                                <th>@lang('admin_report.insurance_status')</th>
                                <th>@lang('admin_report.created_date')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/report_owner.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_list',route('admin.report.owner_list')) }} 
@include('admin.report.modal.owner')
@endsection
