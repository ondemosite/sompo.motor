@extends('admin.layout.default')
@section('title') Admins @endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-edit"></i>@lang('admin_order.topic')</span>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_order.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_order_number">@lang('admin_order.order_number')</label>
                                {{ Form::text('search_order_number',null, ['class' => 'form-control','placeholder' => trans('admin_order.order_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_driver_name">@lang('admin_order.fullname')</label>
                                {{ Form::text('search_driver_name',null, ['class' => 'form-control','placeholder' => trans('admin_order.fullname')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_insurance_ft_si">@lang('admin_order.insurance_ft')</label>
                                {{ Form::number('search_insurance_ft_si',null, ['class' => 'form-control','placeholder' => trans('admin_order.insurance_ft')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_car_licence">@lang('admin_order.car_licence')</label>
                                {{ Form::text('search_car_licence',null, ['class' => 'form-control','placeholder' => trans('admin_order.car_licence')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_order.created_date_from')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_order.created_date_from')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_order.created_date_to')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_order.created_date_to')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_status">@lang('admin_order.status')</label>
                                {{ Form::select('search_status',["WAITING"=>'รอชำระเงิน',"EXPIRE"=>'หมดอายุ','CANCEL'=>'ยกเลิก','INACTIVE'=>'INACTIVE','PAID'=>'ชำระเงินแล้ว'],null, ['class' => 'form-control','placeholder' => trans('admin_order.status')]) }}
                            </div>

                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_order.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_order.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_order.no')</th>
                                <th>@lang('admin_order.order_number')</th>
                                <th>@lang('admin_order.fullname')</th>
                                <th>@lang('admin_order.vehicle')</th>
                                <th>@lang('admin_order.insurance')</th>
                                <th>@lang('admin_order.addon')</th>
                                <th>@lang('admin_order.compulsory')</th>
                                <th>@lang('admin_order.payment_result')</th>
                                <th>@lang('admin_order.status')</th>
                                <th>@lang('admin_order.created_date')</th>
                                <th>@lang('admin_order.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->

    <div id="modal-success" class="modal fade modal-alert modal-success">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><i class="fa fa-check-circle"></i></div>
            <div class="modal-title">Some alert title</div>
            <div class="modal-body">Some alert text</div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
            </div>
        </div>
        </div>
    </div>
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/order.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_order_list',route('admin.get_order_list')) }} 
{{ Form::hidden('hd_order_create',route('admin.order_create_policy')) }} 
@endsection
