@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link href="{{ asset('component/lightbox2-master/src/css/lightbox.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{!! asset('admin/css/order_detail.css') !!}">
@endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_order.topic_detail')</h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-edit"></i>@lang('admin_order.topic_detail')</span>
                    <div style="clear:both;"></div>
                </div>
                {{ Form::open(['url' => route("admin.save_banner"), 'id'=>'form_banner'])}}
                {{ Form::hidden('order_id',!empty($data['id'])?$data['id']:'') }}
                <input name="cover" id="select_file" type="file" style="display:none;" />
                <div class="panel-body">
                    <div class="order-container">
                        <div class="container-fluid">
                            <div class="panel-body p-a-4 b-b-4 bg-white darken">
                                @if($init->status=="WAITING")
                                <div class="ribbon gray"><span>{{$init->status}}</span></div>
                                @elseif($init->status=="EXPIRE")
                                <div class="ribbon red"><span>{{$init->status}}</span></div>
                                @elseif($init->status=="CANCEL")
                                <div class="ribbon light-gray"><span>{{$init->status}}</span></div>
                                @elseif($init->status=="INACTIVE")
                                <div class="ribbon orange"><span>{{$init->status}}</span></div>
                                @elseif($init->status=="PAID")
                                <div class="ribbon"><span>{{$init->status}}</span></div>
                                @endif
                                
                                <div class="img-header pull-left">
                                    <img src="{{asset('/images/web/icon/step-5-main.png')}}" width="100px"/>
                                </div>
                                <div class="text-header pull-right">
                                    <table>
                                        <tr>
                                            <td><h3>@lang('admin_order.order_number') :</h3></td>
                                            <td><p>{{$init->order_number}}</p></td>
                                        </tr>
                                        <tr>
                                            <td><h3>@lang('admin_order.created_date') :</h3></td>
                                            <td><p>{{getLocaleDate($init->created_at,true)}}</p></td>
                                        </tr>
                                        <tr>
                                            <td><h3>@lang('admin_order.name') :</h3></td>
                                            <td><p>{{$init->main_driver()->first()->name." ".$init->main_driver()->first()->lastname}}</p></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><h3 class="text-red">{{strtoupper($init->insurance_plan_name)}}</h3></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="box-panel">
                                <div class="box-header">
                                    <h3>@lang('admin_order.topic_detail')</h3>
                                </div>
                                <div class="box-body">
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.order_number')</td>
                                            <td width="5%">:</td>
                                            <td class="td-value">{{$init->order_number}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.created_date')</td>
                                            <td width="5%">:</td>
                                            <td class="td-value">{{getLocaleDate($init->created_at,true)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.order_expire')</td>
                                            <td width="5%">:</td>
                                            <td class="td-value">{{getLocaleDate($init->insurance_expire,true)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.order_status')</td>
                                            <td width="5%">:</td>
                                            <td class="td-value">
                                                @if($init->status=="WAITING")
                                                <span class="text-muted font-weight-bold">{{$init->status}}</span>
                                                @elseif($init->status=="EXPIRE")
                                                <span class="text-warning font-weight-bold">{{$init->status}}</span>
                                                @elseif($init->status=="CANCEL")
                                                <span class="text-light font-weight-bold">{{$init->status}}</span>
                                                @elseif($init->status=="INACTIVE")
                                                <span class="text-danger font-weight-bold">{{$init->status}}</span>
                                                @elseif($init->status=="PAID")
                                                <span class="text-success font-weight-bold">{{$init->status}}</span>
                                                @endif
                                                &nbsp;<a class="btn btn-default" href="{{ route('admin.compile').'/'.$init->id }}">Compile</a>
                                            </td>
                                        </tr>                                      
                                    </table>
                                </div>
                            </div>
                            <div class="box-panel">
                                <div class="box-header">
                                    <h3>@lang('admin_order.plan_offering')</h3>
                                </div>
                                <div class="box-body">
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.plan')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{strtoupper($init->insurance_plan_name)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.si')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{number_format($init->insurance_ft_si,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.theft_coverage')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">
                                                @if(!empty($init->theft_gross_premium) && $init->theft_gross_premium!=0)
                                                <i class="fa fa-check text-success p-r-1"></i> @lang('common.buy')
                                                @else
                                                <i class="fa fa-close text-danger p-r-1"></i> @lang('common.no_buy')
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.taxi_coverage')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">
                                                @if(!empty($init->taxi_gross_premium) && $init->taxi_gross_premium!=0)
                                                <i class="fa fa-check text-success p-r-1"></i> @lang('common.buy')
                                                @else
                                                <i class="fa fa-close text-danger p-r-1"></i> @lang('common.no_buy')
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.hb_coverage')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">
                                                @if(!empty($init->hb_gross_premium) && $init->hb_gross_premium!=0)
                                                <i class="fa fa-check text-success p-r-1"></i> @lang('common.buy')
                                                @else
                                                <i class="fa fa-close text-danger p-r-1"></i> @lang('common.no_buy')
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.carloss_coverage')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">
                                                @if(!empty($init->carloss_gross_premium) && $init->carloss_gross_premium!=0)
                                                <i class="fa fa-check text-success p-r-1"></i> @lang('common.buy')
                                                @else
                                                <i class="fa fa-close text-danger p-r-1"></i> @lang('common.no_buy')
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.compulsory')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">
                                                @if(!empty($init->compulsory_net_premium) && $init->compulsory_net_premium!=0)
                                                <i class="fa fa-check text-success p-r-1"></i> @lang('common.buy') (@lang('admin_order.start_date') : {{getLocaleDate($init->compulsory_start)}})
                                                @else
                                                <i class="fa fa-close text-danger p-r-1"></i> @lang('common.no_buy')
                                                @endif
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.garage_type')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->insurance_garage_type}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name">@lang('admin_order.deductible')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">
                                                @if(!empty($init->insurance_deduct))
                                                {{$init->insurance_deduct}}
                                                @else
                                                -
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- Car -->
                            <div class="box-panel">
                                <div class="box-header">
                                    <h3>@lang('admin_order.car_info')</h3>
                                </div>
                                <div class="box-body">
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.brand')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{strtoupper($init->vehicle_info()->first()->brand)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.model')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{strtoupper($init->vehicle_info()->first()->model)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.year')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->vehicle_info()->first()->year}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.sub_model')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{strtoupper($init->vehicle_info()->first()->model_type)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.car_licence')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->car_licence}} {{$init->car_province()->first()->name_th}}</td>
                                        </tr>
                                         <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.cctv')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">
                                                <i class="fa fa-check text-success p-r-1"></i> @lang('common.specify')
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="box-panel">
                                <div class="box-header">
                                    <h3>@lang('admin_order.profile_detail')</h3>
                                </div>
                                <div class="box-body">
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.main_driver')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->main_driver()->first()->name." ".$init->main_driver()->first()->lastname}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.birthdate')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{getLocaleDate($init->main_driver()->first()->birth_date)}} (อายุ {{ getAge($init->main_driver()->first()->birth_date) }} @lang('common.year'))</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.idcard')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->main_driver()->first()->idcard}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.driver_licence')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->main_driver()->first()->licence}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.sex')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->main_driver()->first()->gender=="MALE"?trans('common.male'):trans('common.female')}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.address')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->main_driver()->first()->address}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.phone')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->main_driver()->first()->tel}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.email')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->main_driver()->first()->email}}</td>
                                        </tr>
                                    </table>
                                    
                                    @if(!empty($init->driver1))
                                    <hr />
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.first_driver')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_1()->first()->name." ".$init->driver_1()->first()->lastname}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.birthdate')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{getLocaleDate($init->driver_1()->first()->birth_date)}} (อายุ {{ getAge($init->driver_1()->first()->birth_date) }} @lang('common.year'))</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.idcard')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_1()->first()->idcard}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.driver_licence')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_1()->first()->licence}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.sex')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_1()->first()->gender=="MALE"?trans('common.male'):trans('common.female')}}</td>
                                        </tr>
                                    </table>
                                    @endif
                                    @if(!empty($init->driver2))
                                    <hr />
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.second_driver')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_2()->first()->name." ".$init->driver_2()->first()->lastname}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.birthdate')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{getLocaleDate($init->driver_2()->first()->birth_date)}} (อายุ {{ getAge($init->driver_2()->first()->birth_date) }} @lang('common.year'))</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.idcard')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_2()->first()->idcard}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.driver_licence')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_2()->first()->licence}}</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('common.sex')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{$init->driver_2()->first()->gender=="MALE"?trans('common.male'):trans('common.female')}}</td>
                                        </tr>
                                    </table>
                                    @endif
                                </div>
                            </div>

                            <div class="box-panel">
                                <div class="box-header">
                                    <h3>Document Attachment</h3>
                                </div>
                                <div class="box-body">
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.copy_personalcard')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value"><a href="{{ $document['personal_id'] }}" title="@lang('admin_order.copy_personalcard')" target="_blank"><i class="fa fa-search"></i> @lang('admin_order.view')</a></td>
                                        </tr>
                                        <!-- <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.copy_register')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value"><a href="{{ $document['car_licence'] }}" title="@lang('admin_order.copy_register')" target="_blank"><i class="fa fa-search"></i>  @lang('admin_order.view')</a></td>
                                        </tr> -->
                                        @if(!empty($init->driver1))
                                        <!-- <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.copy_driver1_licence')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value"><a href="{{ $document['driver1_licence'] }}" title="@lang('admin_order.copy_driver1_licence')" target="_blank"><i class="fa fa-search"></i>  @lang('admin_order.view')</a></td>
                                        </tr> -->
                                        @endif
                                        @if(!empty($init->driver2))
                                        <!-- <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.copy_driver2_licence')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value"><a href="{{ $document['driver2_licence'] }}" title="@lang('admin_order.copy_driver2_licence')" target="_blank"><i class="fa fa-search"></i>  @lang('admin_order.view')</a></td>
                                        </tr> -->
                                        @endif
                                        @if($init->car_cctv==1)
                                        <!-- <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.cctv_inside')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value"><a href="{{ $document['cctv_inside'] }}" title="@lang('admin_order.cctv_inside')" target="_blank"><i class="fa fa-search"></i> @lang('admin_order.view')</a></td>
                                        </tr>
                                         <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.cctv_outside')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value"><a href="{{ $document['cctv_outside'] }}" title="@lang('admin_order.cctv_outside')" target="_blank"><i class="fa fa-search"></i> @lang('admin_order.view')</a></td>
                                        </tr> -->
                                        @endif
                                    </table>
                                </div>
                            </div>

                            <!-- Order Summary -->
                            <div class="box-panel">
                                <div class="box-header">
                                    <h3>@lang('admin_order.order_summary')</h3>
                                </div>
                                <div class="box-body">
                                    <table width="100%">
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.premium')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">+ {{number_format($init->gross_premium,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.flood_coverage')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">+ {{number_format($init->flood_gross_premium,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.theft_coverage')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">+ {{number_format($init->theft_gross_premium,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.taxi_coverage')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">+ {{number_format($init->taxi_gross_premium,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">เงินชดเชยกรณีรถยนต์สูญหายหรือเสียหายสิ้นเชิงจากอุบัติเหตุ</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">+ {{number_format($init->carloss_gross_premium,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">เงินชดเชยรายได้รายวันกรณีรถยนต์เกิดอุบัติเหตุ</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">+ {{number_format($init->hb_gross_premium,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.compulsory')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">+ {{number_format($init->compulsory_net_premium+$init->compulsory_stamp+$init->compulsory_vat,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name" width="30%">@lang('admin_order.discount')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value">{{number_format($init->discount,2)}} @lang('common.baht')</td>
                                        </tr>
                                        <tr>
                                            <td class="td-name text-underlined" width="30%">@lang('admin_order.payment_result')</td>
                                            <td class="td-name" width="5%">:</td>
                                            <td class="td-value font-weight-bold text-underlined">{{number_format($init->payment_result,2)}} @lang('common.baht')</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </div>
                <div class="panel-footer text-right">
                    <!--<button type="submit" class="btn btn-primary">Submit</button> -->
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{{ asset('component/lightbox2-master/src/js/lightbox.js') }}"></script>
<script src="{!! asset('admin/js/order_detail.js') !!}"></script>
@endsection



