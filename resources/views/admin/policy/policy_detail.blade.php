@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link href="{{ asset('component/lightbox2-master/src/css/lightbox.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{!! asset('admin/css/policy_detail.css') !!}">
@endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_policy.topic_detail')</h1>
@endif
@endsection


@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-edit"></i>@lang('admin_policy.topic_detail')</span>
                    <div style="clear:both;"></div>
                </div>
                {{ Form::hidden('id',!empty($init['id'])?$init['id']:'') }}
                {{ Form::hidden('policy_number',!empty($init['policy_number'])?$init['policy_number']:'') }}
                <div class="panel-body">
                    <div class="policy-container">
                        <div class="container-fluid">
                            <div class="panel-body p-a-4 b-b-4 bg-white darken">
                                <div class="img-header pull-left">
                                    <img src="{{asset('/images/web/logo/logomain.light.png')}}" width="150px"/>
                                    <h3 class="header-h3">@lang('admin_policy.policy_detail')</h3>
                                </div>
                                <div class="text-header pull-right">
                                    <table>
                                        <tr>
                                            <td><h3>@lang('admin_policy.policy_number')</h3></td>
                                            <td width="5%" align="center">:</td>
                                            <td><p>{{$init->policy_number}}</p></td>
                                        </tr>
                                        <tr>
                                            <td><h3>@lang('admin_policy.plan')</h3></td>
                                            <td align="center">:</td>
                                            <td><p>{{strtoupper($init->insurance_plan_name)}}</p></td>
                                        </tr>
                                        <tr>
                                            <td><h3>@lang('admin_policy.start_date')</h3></td>
                                            <td align="center">:</td>
                                            <td><p>{{ getLocaleDate($init->insurance_start,true) }}</p></td>
                                        </tr>
                                        <tr>
                                            <td><h3>@lang('admin_policy.end_date')</h3></td>
                                            <td align="center">:</td>
                                            <td><p>{{ getLocaleDate($init->insurance_expire,true) }}</p></td>
                                        </tr>
                                        <tr>
                                            <td><h3>@lang('admin_policy.name')</h3></td>
                                            <td align="center">:</td>
                                            <td><p>{{ $init->endorse()->first()->owner()->first()->name.' '.$init->endorse()->first()->owner()->first()->lastname }}</p></td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="clear:both;"></div>
                                <br/>
                                <!-- ข้อมูลกรมธรรม์ -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box-panel">
                                            <div class="box-header">
                                                <h3>@lang('admin_policy.information')</h3>
                                            </div>
                                            <div class="box-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td class="td-name" width="40%">@lang('admin_policy.plan')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{strtoupper($init->insurance_plan_name)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.inception_date')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{getLocaleDate($init->insurance_start,true)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.conclusion_date')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{getLocaleDate($init->insurance_expire,true)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.ft_si')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{number_format($init->insurance_ft_si,2)}} @lang('common.baht')</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.compulsory')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                            @if(!empty($init->order()->first()->compulsory_net_premium) && $init->order()->first()->compulsory_net_premium!=0)
                                                            <i class="fa fa-check text-success p-r-1"></i> @lang('common.buy') (@lang('admin_policy.start_date') : {{getLocaleDate($init->compulsory_start)}})
                                                            @else
                                                            <i class="fa fa-close text-danger p-r-1"></i> @lang('common.no_buy')
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.driver_amount')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{$init->insurance_driver_amount}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.garage_type')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                            {{$init->insurance_garage_type}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.insurance_owner')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{ $init->endorse()->first()->owner()->first()->name.' '.$init->endorse()->first()->owner()->first()->lastname }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.deductible')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                            @if(!empty($init->insurance_deduct) && $init->insurance_deduct!=0)
                                                            {{$init->insurance_deduct}}
                                                            @else
                                                            -
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.order_number')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value"><a href="{{route('admin.get_order_data')."/".$init->order_id}}" target="_blank">{{$init->order()->first()->order_number}}</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.receipt_number')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value"><a id="view-receipt-modal" class="cursor-pointer" data-id="{{$init->tax_note_number}}" data-toggle="modal" data-target="#modal-receipt">{{$init->tax_note_number}}</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.otp_verify')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->order()->first()->is_otp) && $init->order()->first()->is_otp!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.email_verify')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->confirmation()->first()->status) && $init->confirmation()->first()->status!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.update_core')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value" id="text-update-core">
                                                            <span class="update-btn">
                                                                @php
                                                                    $endorse_link = "";
                                                                    if($init->update_core_status==1){
                                                                        $endorse_link = route('admin.endorse_log_detail')."/".$init->endorseLog()->first()->id;
                                                                    }
                                                                    $endorseHtml = "<label class=\"label label-success font-size-14\">Yes</label>";
                                                                    $endorseHtml.= "<a class=\"btn btn-default\" href=\"".$endorse_link."\"><i class=\"fa fa-search\"></i> Log</a>";
                                                                    
                                                                @endphp
                                                                {!! $init->update_core_status==1?$endorseHtml:'<label class="label label-danger font-size-14">No</label>' !!}
                                                            </span>
                                                         <a class="bt-update-endorse">@lang('admin_policy.click_update')</a> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('admin_policy.note')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{ Form::textarea('note',!empty($init->note)?$init->note:'', ['class' => 'form-control','placeholder' => 'Note Something','rows'=>'4']) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">&nbsp;</td>
                                                        <td class="td-name">&nbsp;</td>
                                                        <td class="td-value"><button type="button" class="btn btn-primary" id="save_note">@lang('admin_policy.save')</button></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div><!-- /box-panel -->
                                    </div>
                                </div>

                                <!-- Check Box -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box-panel">
                                            <div class="box-header">
                                                <h3>ข้อมูล Checkbox</h3>
                                            </div>
                                            <div class="box-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td class="td-name"  class="td-name" width="40%">เบี้ยประกันภัยนี้เป็นเบี้ยสำหรับรถใช้ส่วนบุคคลหรือเพื่อการพาณิชย์ ไม่ใช้รับจ้างหรือให้เช่า</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->is_personal) && $init->is_personal!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name"  class="td-name" width="40%">ช่องทางการรับกรมธรรม์ประกันภัย - ผ่านจดหมายอิเล็กทรอนิกส์ (E-Mail)</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->is_email) && $init->is_email!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name"  class="td-name" width="40%">ช่องทางการรับกรมธรรม์ประกันภัย - ผ่านทางไปรษณีย์</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->is_post) && $init->is_post!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name"  class="td-name" width="40%">ข้าพเจ้ายินยอมให้ บริษัทฯ จัดเก็บ ใช้ และเปิดเผยข้อเท็จจริงเกี่ยวกับข้อมูลของข้าพเจ้าต่อสำนักงานคณะกรรมการกำกับและส่งเสริมการประกอบธุรกิจประกันภัยเพื่อประโยชน์ในการกำกับดูแลธุรกิจประกันภัย</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->is_disclosure) && $init->is_disclosure!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name"  class="td-name" width="40%">ข้าพเจ้าขอรับรองว่า คำบอกกล่าวตามรายการข้างบนเป็นความจริงและให้ถือเป็นส่วนหนึ่งของสัญญาระหว่างข้าพเจ้ากับบริษัท</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->is_true) && $init->is_true!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name"  class="td-name" width="40%">ข้าพเจ้ายินยอมให้บริษัทส่งข้อมูลข่าวสารเพื่อนำเสนอผลิตภัณฑ์อื่น ที่เป็นประโยชน์ในโอกาสต่อไปหรือไม่</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->is_advice) && $init->is_advice!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name"  class="td-name" width="40%">ท่านยอมรับข้อตกลงคุ้มครองและ ข้อยกเว้นของกรมธรรม์ประกันภัยทุกประการ</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                        @if(!empty($init->is_agree) && $init->is_agree!=0)
                                                        <i class="fa fa-check text-success p-r-1"></i>
                                                        @else
                                                        <i class="fa fa-close text-danger p-r-1"></i>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ข้อมูลผู้ขับขี่ -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box-panel">
                                            <div class="box-header">
                                                <h3>@lang('admin_policy.driver_info') <div class="pull-right"><a class="text-white bt-edit-info"><i class="fa fa-pencil"></i></a></div></h3>
                                            </div>
                                            <div class="box-body">
                                                <ul class="nav nav-tabs nav-tabs-simple" id="tab-resize-info"></ul>
                                                <div class="tab-content-info tab-content"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <!-- ข้อมูลรถยนต์ -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box-panel">
                                            <div class="box-header">
                                                <h3>@lang('admin_policy.vehicle_info')</h3>
                                            </div>
                                            <div class="box-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td class="td-name" width="40%">@lang('common.brand')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{strtoupper($init->vehicle_info()->first()->brand)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('common.model')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{strtoupper($init->vehicle_info()->first()->model)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('common.year')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{$init->vehicle_info()->first()->year}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('common.sub_model')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{strtoupper($init->vehicle_info()->first()->model_type)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-name">@lang('common.car_licence')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">{{str_replace(" ","-",$init->car_licence)}} {{$init->car_province()->first()->name_th}}</td>
                                                    </tr>
                                                        <tr>
                                                        <td class="td-name">@lang('admin_policy.cctv')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                            @if($init->car_cctv==1)
                                                            <i class="fa fa-check text-success p-r-1"></i> @lang('common.specify')
                                                            @else
                                                            <i class="fa fa-close text-danger p-r-1"></i> @lang('common.no_specify')
                                                            @endif
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div><!-- /box-panel -->
                                    </div>
                                </div>

                                <!-- เอกสารประกอบ -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box-panel">
                                            <div class="box-header">
                                                <div><h3>@lang('admin_policy.document') <div class="pull-right"><a class="text-white bt-edit-doc"><i class="fa fa-pencil"></i></a></div></h3></div>
                                            </div>
                                            <div class="box-body">
                                                {{ Form::open(['url' => route("admin.save_policy_doc"),'files'=>true,'id'=>'form_edit_doc','name'=>'form_edit_doc'])}}
                                                {{ Form::hidden('id',!empty($init['id'])?$init['id']:'') }}
                                                <table width="100%">
                                                    <tr>
                                                        <td class="td-name" width="40%">@lang('admin_policy.copy_personal')</td>
                                                        <td class="td-name" width="5%">:</td>
                                                        <td class="td-value">
                                                            <a href="{{ $document['personal_id'] }}" target="_blank"><i class="fa fa-search"></i> @lang('common.view')</a>
                                                            <!--<a class="lb-doc" data-lightbox="image-personal" href="{{ asset("/storage/".$init->document_path_personal_id) }}" title="@lang('admin_policy.copy_personal')"><i class="fa fa-search"></i> @lang('common.view')</a>-->
                                                            <label class="custom-file px-file disable input-doc" for="input-doc-1">
                                                                <input type="file" id="input-doc-1" name="doc_main_idcard" class="custom-file-input">
                                                                <span class="custom-file-control form-control">Choose file...</span>
                                                                <div class="px-file-buttons">
                                                                    <button type="button" class="btn btn-xs px-file-clear">@lang('admin_policy.clear')</button>
                                                                    <button type="button" class="btn btn-primary btn-xs px-file-browse">@lang('admin_policy.browse')</button>
                                                                </div>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    
                                                </table>
                                                <table width="100%" id="row_save_doc">
                                                    <tr>
                                                        <td class="td-name" width="40%">&nbsp;</td>
                                                        <td class="td-name" width="5%">&nbsp;</td>
                                                        <td class="td-value" align="right"><button type="button" id="save-edit-doc" class="btn btn-primary">@lang('admin_policy.save')</button></td>
                                                    </tr>
                                                </table>
                                                {{ Form::close() }}
                                            </div>
                                        </div><!-- /box-panel -->
                                        </div>
                                    </div>

                                    <!-- ไฟล์กรมธรรม์ -->
                                    @php
                                        $endorse = $init->endorse()->first();
                                    @endphp
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box-panel">
                                                <div class="box-header box-header">
                                                    <h3>@lang('admin_policy.files_policy')</h3>
                                                </div>
                                                <div class="box-body">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="td-name" width="40%">Voluntary Schedule</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if(!empty($files['policy_document_path']))
                                                                <a class="btn btn-primary" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'policy_document_path' !!}" target="_blank"><i class="fa fa-download"></i> @lang('admin_policy.download') (เข้ารหัส)</a> 
                                                                @else
                                                                    ไม่พบไฟล์
                                                                @endif
                                                            </td>
                                                            <td class="td-value">
                                                                @if($init->is_post == 1 || $init->policy_com_number!=null)
                                                                    @if(!empty($files['policy_document_path']))
                                                                        <a class="btn btn-success btn-clean" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'policy_document_path'.'/clean' !!}" target="_blank" data-printed="print_1">
                                                                            <i class="fa fa-download"></i> @lang('admin_policy.download') (ไม่เข้ารหัส)
                                                                        </a> 
                                                                        <div id="print_1">
                                                                            <div class="print_icon print_enable {{ $endorse->is_print_policy == 1?'active':'' }}">
                                                                                <i class="fa fa-check text-success p-r-1"> Print แล้ว</i>
                                                                            </div>
                                                                            <div class="print_icon print_disable {{ $endorse->is_print_policy == 0?'active':'' }}">
                                                                                <i class="fa fa-check text-danger p-r-1"> ยังไม่ได้ print</i>
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                    ไม่พบไฟล์
                                                                    @endif
                                                                @else
                                                                    ไม่รับการส่งทางไปรษณีย์
                                                                @endif
                                                            </td>
                                                            <td class="td-value">
                                                            @if(!empty($files['policy_document_path']))
                                                                <a class="btn btn-warning" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'policy_document_path'.'/copy' !!}" target="_blank">
                                                                    <i class="fa fa-download"></i> @lang('admin_policy.download') (สำเนา)
                                                                </a> 
                                                            @else
                                                                ไม่พบไฟล์
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name">Voluntary Tax / Invoice (Original)</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value"> 
                                                                @if(!empty($files['policy_original_invoice_path']))
                                                                <a class="btn btn-primary" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'policy_original_invoice_path' !!}" target="_blank"><i class="fa fa-download"></i> @lang('admin_policy.download') (เข้ารหัส)</a> 
                                                                @else
                                                                ไม่พบไฟล์
                                                                @endif
                                                            </td>
                                                            <td class="td-value">
                                                                @if($init->is_post == 1 || $init->policy_com_number!=null)
                                                                    @if(!empty($files['policy_original_invoice_path']))
                                                                        <a class="btn btn-success btn-clean" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'policy_original_invoice_path'.'/clean' !!}" target="_blank" data-printed="print_2">
                                                                            <i class="fa fa-download"></i> @lang('admin_policy.download') (ไม่เข้ารหัส)
                                                                        </a> 
                                                                        <div id="print_2">
                                                                            <div class="print_icon print_enable {{ $endorse->is_print_original_invoice == 1?'active':'' }}">
                                                                                <i class="fa fa-check text-success p-r-1"> Print แล้ว</i>
                                                                            </div>
                                                                            <div class="print_icon print_disable {{ $endorse->is_print_original_invoice == 0?'active':'' }}">
                                                                                <i class="fa fa-check text-danger p-r-1"> ยังไม่ได้ print</i>
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                    ไม่พบไฟล์
                                                                    @endif
                                                                @else
                                                                    ไม่รับการส่งทางไปรษณีย์
                                                                @endif
                                                            </td>
                                                            <td class="td-value">
                                                                @if(!empty($files['policy_copy_invoice_path']))
                                                                <a class="btn btn-warning" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'policy_copy_invoice_path'.'/copy' !!}" target="_blank" >
                                                                    <i class="fa fa-download"></i> @lang('admin_policy.download') (สำเนา)
                                                                </a> 
                                                                @else
                                                                    ไม่พบไฟล์
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name">Compulsory Schedule</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if(!empty($files['compulsory_document_path']))
                                                                <a class="btn btn-primary" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'compulsory_document_path' !!}" target="_blank"><i class="fa fa-download"></i> @lang('admin_policy.download') (เข้ารหัส)</a> 
                                                                @else
                                                                    @if(empty($init->policy_com_number))
                                                                        ไม่ได้ซื้อ พรบ.
                                                                    @else
                                                                        ไม่พบไฟล์
                                                                    @endif
                                                                @endif
                                                            </td>
                                                            <td class="td-value">
                                                                @if(!empty($files['compulsory_document_path']))
                                                                    <a class="btn btn-success btn-clean" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'compulsory_document_path'.'/clean' !!}" target="_blank" data-printed="print_4">
                                                                        <i class="fa fa-download"></i> @lang('admin_policy.download') (ไม่เข้ารหัส)
                                                                    </a> 
                                                                    <div id="print_4">
                                                                        <div class="print_icon print_enable {{ $endorse->is_print_compulsory == 1?'active':'' }}">
                                                                            <i class="fa fa-check text-success p-r-1"> Print แล้ว</i>
                                                                        </div>
                                                                        <div class="print_icon print_disable {{ $endorse->is_print_compulsory == 0?'active':'' }}">
                                                                            <i class="fa fa-check text-danger p-r-1"> ยังไม่ได้ print</i>
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    @if(empty($init->policy_com_number))
                                                                        ไม่ได้ซื้อ พรบ.
                                                                    @else
                                                                        ไม่พบไฟล์
                                                                    @endif
                                                                @endif
                                                            </td>
                                                            <td class="td-value">
                                                                @if(!empty($files['compulsory_document_path']))
                                                                    <a class="btn btn-warning" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'compulsory_document_path'.'/copy' !!}" target="_blank">
                                                                        <i class="fa fa-download"></i> @lang('admin_policy.download') (สำเนา)
                                                                    </a> 
                                                                @else
                                                                    @if(empty($init->policy_com_number))
                                                                        ไม่ได้ซื้อ พรบ.
                                                                    @else
                                                                        ไม่พบไฟล์
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <!-- <tr>
                                                            <td class="td-name">Compulsory Tax / Invoice (Original)</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if(!empty($files['compulsory_original_invoice_path']))
                                                                <a class="btn btn-primary" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'compulsory_original_invoice_path' !!}" target="_blank"><i class="fa fa-download"></i> @lang('admin_policy.download')</a> 
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name">Compulsory Tax / Invoice (Copy)</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if(!empty($files['compulsory_copy_invoice_path']))
                                                                <a class="btn btn-primary" href="{!! route('admin.download_policy').'/'.$init->id.'/'.'compulsory_copy_invoice_path' !!}" target="_blank"><i class="fa fa-download"></i> @lang('admin_policy.download')</a> 
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                        </tr> -->

                                                    </table>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- บันทึกแก้ไขเอกสาร -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box-panel">
                                                <div class="box-header box-header-gray">
                                                    <h3>@lang('admin_policy.edit_document_log')</h3>
                                                </div>
                                                <div class="box-body">
                                                    <table width="100%" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td>@lang('admin_policy.no')</td>
                                                                <td>@lang('admin_policy.date')</td>
                                                                <td>@lang('admin_policy.old_data')</td>
                                                                <td>@lang('admin_policy.admin')</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="edit_documment_body"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- ข้อมูลการจัดส่งเอกสาร -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box-panel">
                                                <div class="box-header">
                                                    <h3>@lang('admin_policy.action_record')</h3>
                                                </div>
                                                <div class="box-body">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.action_date')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{getLocaleDate($init->created_at)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.send_document_date')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if(!empty($emailingLog))
                                                                    {{ getLocaleDate($emailingLog->created_at)}}
                                                                @else
                                                                    ยังไม่ได้จัดส่ง
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <button id="resend_email" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-resend">@lang('admin_policy.resend')</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">จัดส่ง SMS ล่าสุด</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value" width="10%">
                                                                @if(!empty($smsLog))
                                                                    {{ getLocaleDate($smsLog->created_at)}}
                                                                @else
                                                                    ยังไม่ได้จัดส่ง
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <button id="bt-resend-sms" type="button" class="btn btn-primary">@lang('admin_policy.resend')</button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <!-- บันทึกจัดส่ง E-mail -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box-panel">
                                                <div class="box-header box-header-gray">
                                                    <h3>@lang('admin_policy.email_send_log')</h3>
                                                </div>
                                                <div class="box-body">
                                                    <table width="100%" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td>@lang('admin_policy.no')</td>
                                                                <td>@lang('admin_policy.date')</td>
                                                                <td width="30%">@lang('admin_policy.files')</td>
                                                                <td>@lang('admin_policy.status')</td>
                                                                <td>@lang('admin_policy.admin')</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="email_log_body">
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- บันทึกจัดส่ง E-Pass -->
                                    <!-- <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box-panel">
                                                <div class="box-header box-header-gray">
                                                    <h3>@lang('admin_policy.epass_send_log')</h3>
                                                </div>
                                                <div class="box-body">
                                                    <table width="100%" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td>@lang('admin_policy.no')</td>
                                                                <td>@lang('admin_policy.date')</td>
                                                                <td>@lang('admin_policy.endorse_no')</td>
                                                                <td>@lang('admin_policy.status')</td>
                                                                <td>@lang('admin_policy.admin')</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="epass_log_body">
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->


                                    <!-- ข้อมูลความคุ้มครอง -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box-panel">
                                                <div class="box-header">
                                                    <h3>@lang('admin_policy.coverage_info')</h3>
                                                </div>
                                                <div class="box-body">
                                                    <h4>@lang('admin_policy.car_damage')</h4>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.accident')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if($init->insurance_plan_id!=3)
                                                                {{number_format($init->insurance_ft_si,2)}} @lang('common.baht')
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.robbery')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if($init->insurance_plan_id==1)
                                                                {{number_format($init->insurance_ft_si,2)}} @lang('common.baht')
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.car_fire')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if($init->insurance_plan_id==1)
                                                                {{number_format($init->insurance_ft_si,2)}} @lang('common.baht')
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <!-- <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.isis')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if($init->insurance_plan_id!=3)
                                                                {{number_format($init->insurance_ft_si,2)}} @lang('common.baht')
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.flood')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($flood->sum_insured,2)}} @lang('common.baht')</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.service_car')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if($init->insurance_plan_id!=3)
                                                                @lang('admin_policy.20_percent')
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                        </tr> -->
                                                    </table>
                                                    <br/>
                                                    <hr/>
                                                    <h4>@lang('admin_policy.outside_person')</h4>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="td-name" width="40%">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย ต่อคน</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{ number_format($init->insurance_person_damage_person,2) }} @lang('common.baht')</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย ต่อครั้ง</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{ number_format($init->insurance_person_damage_once,2) }} @lang('common.baht')</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">ความเสียหายต่อทรัพย์สิน ต่อครั้ง</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">
                                                                @if($init->insurance_plan_id!=3)
                                                                {{number_format($init->insurance_stuff_damage,2)}} @lang('common.baht')
                                                                @else
                                                                {{number_format($init->insurance_ft_si,2)}} @lang('common.baht')
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br/>
                                                    <hr/>
                                                    <h4>คุ้มครองตามเอกสารแนบท้าย</h4>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="td-name" width="40%">อุบัติเหตุส่วนบุคคล/เสียชีวิต สูญเสียอวัยวะทุพพลภาพถาวร ต่อคน</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($init->insurance_death_disabled,2)}} @lang('common.baht')</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">ค่ารักษาพยาบาลจากอุบัติเหตุ ต่อคน</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($init->insurance_medical_fee,2)}} @lang('common.baht')</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">การประกันตัวผู้ขับขี่กรณีอาญา</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($init->insurance_bail_driver,2)}} @lang('common.baht')</td>
                                                        </tr>
                                                    </table>
                                                    @if(!empty($init->order()->first()->compulsory_net_premium) && $init->order()->first()->compulsory_net_premium!=0)
                                                    <br/>
                                                    <hr/>
                                                    <h4>@lang('admin_policy.coverage_compulsory')</h4>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.hospital_cost')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($static['compulsory_medical_fee'],2)}} @lang('common.baht_person')</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.permanent_insurance')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($static['compulsory_death_disable'],2)}} @lang('common.baht_times')</td>
                                                        </tr>
                                                    </table>
                                                    @endif
                                                    @if(
                                                    (!empty($init->addon_theft) && $init->addon_theft!=0) || 
                                                    (!empty($init->addon_taxi) && $init->addon_taxi!=0)  || 
                                                    (!empty($init->addon_hb) && $init->addon_hb!=0) ||
                                                    (!empty($init->addon_carloss) && $init->addon_carloss!=0) 
                                                    )
                                                    <br/>
                                                    <hr/>
                                                    <h4>@lang('admin_policy.accident_benefit')</h4>
                                                    <table width="100%">
                                                        @if(!empty($init->addon_theft) && $init->addon_theft!=0)
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.stuff_coverage')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($init->addon_theft,2)}} @lang('common.baht_person')</td>
                                                        </tr>
                                                        @endif
                                                        @if(!empty($init->addon_taxi) && $init->addon_taxi!=0)
                                                        <tr>
                                                            <td class="td-name" width="40%">@lang('admin_policy.taxi_coverage')</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($init->addon_taxi,2)}} @lang('common.baht')</td>
                                                        </tr>
                                                        @endif
                                                        @if(!empty($init->addon_hb) && $init->addon_hb!=0)
                                                        <tr>
                                                            <td class="td-name" width="40%">เงินชดเชยรายได้รายวันกรณีรถยนต์เกิดอุบัติเหตุ</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($init->addon_hb,2)}} @lang('common.baht')</td>
                                                        </tr>
                                                        @endif
                                                        @if(!empty($init->addon_carloss) && $init->addon_carloss!=0)
                                                        <tr>
                                                            <td class="td-name" width="40%">เงินชดเชยกรณีรถยนต์เสียหายสิ้นเชิงจากอุบัติเหตุ</td>
                                                            <td class="td-name" width="5%">:</td>
                                                            <td class="td-value">{{number_format($init->addon_carloss,2)}} @lang('common.baht')</td>
                                                        </tr>
                                                        @endif
                                                    </table>
                                                    @endif
                                                </div>
                                            </div><!--box-panel-->
                                        </div>
                                    </div>


                                        
                                    </div>
                                </div><!-- /row -->
                            </div> <!-- /panel-body -->
                        </div> <!-- /container-fluid -->
                    </div>
                    
                </div>
                <div class="panel-footer text-right">
                    <!--<button type="submit" class="btn btn-primary">Submit</button> -->
                </div>
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{{ asset('component/lightbox2-master/src/js/lightbox.js') }}"></script>
<script src="{!! asset('admin/js/policy_detail.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_district',route('api.web.get_district')) }}
{{ Form::hidden('hd_get_subdistrict',route('api.web.get_subdistrict')) }}
{{ Form::hidden('hd_policy_edit_log',route('admin.get_policy_edit_log')) }} 
{{ Form::hidden('hd_policy_note',route('admin.policy_save_note')) }} 
{{ Form::hidden('hd_policy_edit_log_data',route('admin.get_edit_policy_log')) }} 
{{ Form::hidden('hd_policy_doc_log',route('admin.get_policy_doc')) }} 
{{ Form::hidden('hd_policy_doc_log_data',route('admin.get_policy_doc_data')) }} 
{{ Form::hidden('hd_policy_receipt_data',route('admin.get_policy_receipt')) }} 
{{ Form::hidden('hd_policy_email_log',route('admin.get_email_log')) }} 
{{ Form::hidden('hd_policy_epass_log',route('admin.get_epass_log')) }} 
{{ Form::hidden('hd_policy_resend_doc',route('admin.policy_resend_doc')) }} 
{{ Form::hidden('hd_endorse_get',route('admin.policy_endorse_get')) }} 
{{ Form::hidden('hd_endorse_first',route('admin.policy_endorse_first')) }} 
{{ Form::hidden('hd_endorse_save',route('admin.save_policy_endorse')) }} 
{{ Form::hidden('hd_resend_sms',route('admin.policy_resend_sms')) }} 
{{ Form::hidden('hd_update_core',route('admin.policy_update_core')) }} 
{{ Form::hidden('hd_get_update_core',route('admin.policy_get_update_core')) }} 
{{ Form::hidden('hd_download_policy',route('admin.download_policy')) }} 

@include('admin.policy.modal.policy_doc_log') 
@include('admin.policy.modal.receipt') 
@include('admin.policy.modal.policy_info') 
@include('admin.policy.modal.policy_resend') 
@endsection



    