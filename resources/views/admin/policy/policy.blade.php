@extends('admin.layout.default')
@section('title') Admins @endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-edit"></i>@lang('admin_policy.topic')</span>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_policy.search')</h6>
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="sr-only" for="search_policy_number">@lang('admin_policy.policy_number')</label>
                                        {{ Form::text('search_policy_number',null, ['class' => 'form-control','placeholder' => trans('admin_policy.policy_number')]) }}
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="sr-only" for="search_policy_com_number">Compulsory Policy Number</label>
                                        {{ Form::text('search_policy_com_number',null, ['class' => 'form-control','placeholder' => trans('admin_policy.policy_com_number')]) }}
                                    </div> 
                                    <!--
                                    <div class="form-group">
                                        <label class="sr-only" for="search_driver_name">@lang('admin_policy.fullname')</label>
                                        {{ Form::text('search_driver_name',null, ['class' => 'form-control','placeholder' => trans('admin_policy.fullname')]) }}
                                    </div>-->
                                    <div class="form-group">
                                        <label class="sr-only" for="search_insurance_ft_si">@lang('admin_policy.insurance_amount')</label>
                                        {{ Form::number('search_insurance_ft_si',null, ['class' => 'form-control','placeholder' => trans('admin_policy.insurance_amount')]) }}
                                    </div>
                                    
                                </div>
                                <div class="col-xs-12 m-t-1">
                                    <div class="form-group">
                                        <label class="sr-only" for="search_car_licence">@lang('admin_policy.car_licence')</label>
                                        {{ Form::text('search_car_licence',null, ['class' => 'form-control','placeholder' => trans('admin_policy.car_licence')]) }}
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="search_from_date">@lang('admin_policy.create_date_from')</label>
                                        {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_policy.create_date_from')]) }}
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="search_to_date">@lang('admin_policy.create_date_to')</label>
                                        {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_policy.create_date_to')]) }}
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="search_status">@lang('admin_policy.status')</label>
                                        {{ Form::select('search_status',["NORMAL"=>'ปกติ',"EDIT"=>'แก้ไข','CANCEL'=>'ยกเลิก','EXPIRE'=>'หมดอายุ','INACTIVE'=>'INACTIVE'],null, ['class' => 'form-control','placeholder' => trans('admin_policy.status')]) }}
                                    </div>
                                </div>
                                <div class="col-xs-12 m-t-1 m-b-1">
                                    <div class="form-group">
                                        <label class="sr-only" for="search_status">เลือกรับกรมธรรม์ทางไปรษณีย์</label>
                                        {{ Form::select('search_post',["YES"=>'ใช่',"NO"=>'ไม่ใช่'],null, ['class' => 'form-control','placeholder' => 'เลือกรับกรมธรรม์ทางไปรษณีย์']) }}
                                    </div>
                                </div>
                            </div>
                            
                            

                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_policy.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_policy.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_policy.no')</th>
                                <th>@lang('admin_policy.policy_number')</th>
                                <th>@lang('admin_policy.policy_com_number')</th>
                                <th>@lang('admin_policy.fullname')</th>
                                <th>@lang('admin_policy.vehicle')</th>
                                <th>@lang('admin_policy.insurance')</th>
                                <th>@lang('admin_policy.status')</th>
                                <th>@lang('admin_policy.created_date')</th>
                                <th>@lang('admin_policy.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/policy.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_policy_list',route('admin.get_policy_list')) }} 
@endsection
