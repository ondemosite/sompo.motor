<div class="modal fade" id="modal-receipt" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-"></i>@lang('admin_policy.receipt_data')</h4>
            </div>
            <div class="modal-body">
                <div class="modal-topic"><b>@lang('admin_policy.receipt_number') :</b> <span id="tax_number"></span>
                    <span class="pull-right"><b>@lang('admin_policy.created_date') :</b> <span id="receipe_created_at"></span></span>
                </div>
                <table width="100%" class="table table-striped">
                    <tbody>
                        <tr>
                            <td>@lang('admin_policy.order_number')</td>
                            <td id="field_rc_order"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.payment_channel')</td>
                            <td id="field_rc_channel"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.credit_number')</td>
                            <td id="field_rc_credit_number">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.credit_type')</td>
                            <td id="field_rc_scheme">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.payment_result')</td>
                            <td id="field_rc_amount">-</td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
    </div>