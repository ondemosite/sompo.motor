<div class="modal fade" id="modal-edit-log" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel">@lang('admin_policy.old_data')</h4>
            </div>
            <div class="modal-body">
                <h4>@lang('admin_policy.main_driver')</h4>
                <table width="100%" class="table table-striped">
                    <thead>
                        <th width="30%">@lang('admin_policy.field')</th>
                        <th>@lang('admin_policy.data')</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>@lang('common.name')</td>
                            <td id="field_name"></td>
                        </tr>
                        <tr>
                            <td>@lang('common.lastname')</td>
                            <td id="field_lastname"></td>
                        </tr>
                        <tr>
                            <td>@lang('common.idcard')</td>
                            <td id="field_idcard">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.driver_licence')</td>
                            <td id="field_licence">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.birthdate')</td>
                            <td id="field_birthdate">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.sex')</td>
                            <td id="field_gender">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.address')</td>
                            <td id="field_address">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.phone')</td>
                            <td id="field_tel">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.email')</td>
                            <td id="field_email">-</td>
                        </tr>
                    </tbody>
                </table>
                <h4>@lang('admin_policy.first_driver')</h4>
                <table width="100%" class="table table-striped">
                    <thead>
                        <th width="30%">@lang('admin_policy.field')</th>
                        <th>@lang('admin_policy.data')</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>@lang('common.name')</td>
                            <td id="field_driver1_name"></td>
                        </tr>
                        <tr>
                            <td>@lang('common.lastname')</td>
                            <td id="field_driver1_lastname"></td>
                        </tr>
                        <tr>
                            <td>@lang('common.idcard')</td>
                            <td id="field_driver1_idcard">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.driver_licence')</td>
                            <td id="field_driver1_licence">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.birthdate')</td>
                            <td id="field_driver1_birthdate">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.sex')</td>
                            <td id="field_driver1_gender">-</td>
                        </tr>
                    </tbody>
                </table>
                <h4>@lang('admin_policy.second_driver')</h4>
                <table width="100%" class="table table-striped">
                    <thead>
                        <th width="30%">@lang('admin_policy.field')</th>
                        <th>@lang('admin_policy.data')</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>@lang('common.name')</td>
                            <td id="field_driver2_name"></td>
                        </tr>
                        <tr>
                            <td>@lang('common.lastname')</td>
                            <td id="field_driver2_lastname"></td>
                        </tr>
                        <tr>
                            <td>@lang('common.idcard')</td>
                            <td id="field_driver2_idcard">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.driver_licence')</td>
                            <td id="field_driver2_licence">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.birthdate')</td>
                            <td id="field_driver2_birthdate">-</td>
                        </tr>
                        <tr>
                            <td>@lang('common.sex')</td>
                            <td id="field_driver2_gender">-</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>