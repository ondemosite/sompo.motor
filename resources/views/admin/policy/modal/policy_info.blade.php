<div class="modal fade" id="modal-info" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel">@lang('admin_policy.driver_info')</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['id'=>'form_info'])}}
                    {{ Form::hidden('id',!empty($init['id'])?$init['id']:'') }}
                    <div><label class="font-size-16 text-muted">ผู้ขับขี่หลัก</label></div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="field_name">ชื่อ</label>
                            <input type="text" id="field_name" class="form-control" name="main_name">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="field_lastname">นามสกุล</label>
                            <input type="text" id="field_lastname" class="form-control" name="main_lastname">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="field_birthdate">วันเกิด</label>
                            <input type="text" id="field_birthdate" class="form-control datepicker" name="main_birthdate">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="field_idcard">บัตรประจำตัวประชาชน</label>
                            <input type="text" id="field_idcard" class="form-control" name="main_idcard">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="field_tel">เบอร์โทรศัพท์</label>
                            <input type="text" id="field_tel" class="form-control" name="main_tel">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="field_email">อีเมล</label>
                            <input type="text" id="field_email" class="form-control" name="main_email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label for="grid-input-20">เพศ</label><br/>
                            <label class="custom-control custom-radio radio-inline">
                                <input type="radio" name="main_gender" class="custom-control-input" value="MALE">
                                <span class="custom-control-indicator"></span>
                                ชาย
                            </label>
                            <label class="custom-control custom-radio radio-inline">
                                <input type="radio" name="main_gender" class="custom-control-input" value="FEMALE">
                                <span class="custom-control-indicator"></span>
                                หญิง
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label for="field_address">ที่อยู่</label>
                            <input type="text" id="field_address" class="form-control" name="main_address">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="field_province">จังหวัด</label>
                            {{ Form::select('main_province',$static['province'],null,['class' => 'form-control','placeholder' => trans('placeholder.select_province')]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="field_district">อำเภอ</label>
                            {{ Form::select('main_district',[],null,['class' => 'form-control','placeholder' => trans('placeholder.select_district')]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="field_subdistrict">ตำบล</label>
                            {{ Form::select('main_subdistrict',[],null,['class' => 'form-control','placeholder' => trans('placeholder.select_sub_district')]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="field_postalcode">รหัสไปรษณีย์</label>
                            <input type="text" id="field_postalcode" class="form-control" name="main_postalcode">
                        </div>
                    </div>


                    <!-- Driver1 -->
                    <div id="info-box-1">
                    <hr/>
                        <div><label class="font-size-16 text-muted">ผู้ขับขี่ที่ 1</label></div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="field_driver1_name">ชื่อ</label>
                                <input type="text" id="field_driver1_name" class="form-control" name="driver1_name">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="field_driver1_lastname">นามสกุล</label>
                                <input type="text" id="field_driver1_lastname" class="form-control" name="driver1_lastname">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="field_driver1_birthdate">วันที่</label>
                                <input type="text" id="field_driver1_birthdate" class="form-control datepicker" name="driver1_birthdate">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="field_driver1_idcard">บัตรประจำตัวประชาชน</label>
                                <input type="text" id="field_driver1_idcard" class="form-control" name="driver1_idcard">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="field_driver1_licence">เลขที่ใบขับขี่</label>
                                <input type="text" id="field_driver1_licence" class="form-control" name="driver1_licence">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="field_driver1_gender">เพศ</label> <label id="driver1_gender-error" class="error" for="driver1_gender"></label><br/>
                                <label class="custom-control custom-radio radio-inline">
                                    <input type="radio" name="driver1_gender" class="custom-control-input" value="MALE">
                                    <span class="custom-control-indicator"></span>
                                    ชาย
                                </label>
                                <label class="custom-control custom-radio radio-inline">
                                    <input type="radio" name="driver1_gender" class="custom-control-input" value="FEMALE">
                                    <span class="custom-control-indicator"></span>
                                    หญิง
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Driver2 -->
                    <div id="info-box-2">
                        <hr/>
                        <div><label class="font-size-16 text-muted">ผู้ขับขี่ที่ 2</label></div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="field_driver2_name">ชื่อ</label>
                                <input type="text" id="field_driver2_name" class="form-control" name="driver2_name">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="field_driver2_lastname">นามสกุล</label>
                                <input type="text" id="field_driver2_lastname" class="form-control" name="driver2_lastname">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="field_driver2_birthdate">วันเกิด</label>
                                <input type="text" id="field_driver2_birthdate" class="form-control datepicker" name="driver2_birthdate">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="field_driver2_idcard">บัตรประจำตัวประชาชน</label>
                                <input type="text" id="field_driver2_idcard" class="form-control" name="driver2_idcard">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="field_driver2_licence">เลขที่ใบขับขี่</label>
                                <input type="text" id="field_driver2_licence" class="form-control" name="driver2_licence">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="field_driver2_gender">เพศ</label> <label id="driver2_gender-error" class="error" for="driver2_gender"></label><br/>
                                <label class="custom-control custom-radio radio-inline">
                                    <input type="radio" name="driver2_gender" class="custom-control-input" value="MALE">
                                    <span class="custom-control-indicator"></span>
                                    ชาย
                                </label>
                                <label class="custom-control custom-radio radio-inline">
                                    <input type="radio" name="driver2_gender" class="custom-control-input" value="FEMALE">
                                    <span class="custom-control-indicator"></span>
                                    หญิง
                                </label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="is_core" value="NO">
                {{ Form::close()}}
            </div>
            <div class="modal-footer">
                <button type="button" class="submit-info btn btn-success" data-mode="YES">Save & Save to Core System</button>
                <button type="button" class="submit-info btn btn-warning" data-mode="NO">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    </div>