<div class="modal fade" id="modal-resend" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel">Resend Document</h4>
            </div>
            <div class="modal-body">
                @php
                    $endorse = $init->endorse()->first();
                @endphp
                {{ Form::open(['id'=>'form_info'])}}
                    {{ Form::hidden('id',!empty($init['id'])?$init['id']:'') }}
                    <div><label class="font-size-16 text-muted">Select Files</label></div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @if(!empty($endorse->policy_document_path))
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="resend_files" value="policy">
                                <span class="custom-control-indicator"></span>
                                Policy Motor
                            </label>
                            @endif
                            @if(!empty($endorse->policy_original_invoice_path))
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="resend_files" value="policy_ori_invoice">
                                <span class="custom-control-indicator"></span>
                                Policy Motor - Original Invoice
                            </label>
                            @endif
                            @if(!empty($endorse->compulsory_document_path))
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="resend_files" value="compulsory">
                                <span class="custom-control-indicator"></span>
                                Policy Compulsory
                            </label>
                            @endif                
                        </div>                   
                    </div>
                {{ Form::close()}}
            </div>
            <div class="modal-footer">
                <span class="resend-status pull-left"></span>
                <button type="button" id="bt-resend-submit" class="btn btn-primary">Send</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    </div>