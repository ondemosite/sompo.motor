<div class="modal fade" id="modal-doc-log" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel">@lang('admin_policy.before_data')</h4>
            </div>
            <div class="modal-body">
                <table width="100%" class="table table-striped">
                    <thead>
                        <th width="60%">@lang('admin_policy.field')</th>
                        <th>@lang('admin_policy.data')</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>@lang('admin_policy.copy_personal')</td>
                            <td id="field_doc_id_card"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.copy_licence')</td>
                            <td id="field_doc_licence"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.copy_licence_1')</td>
                            <td id="field_doc_driver1_licence"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.copy_licence_2')</td>
                            <td id="field_doc_driver2_licence"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.cctv_inside')</td>
                            <td id="field_doc_cctv_inside"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_policy.cctv_outside')</td>
                            <td id="field_doc_cctv_outside"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>