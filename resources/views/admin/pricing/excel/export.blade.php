<table>
    <tr>
        <td>PLAN</td>
        <td>FT SI</td>
        <td>CAR CODE</td>
        <td>CAR ENGINE</td>
        <td>DEFINE NAME</td>
        <td>CCTV</td>
        <td>GARAGE TYPE</td>
        <td>DEDUCTIBLE</td>
        <td>ADDITIONAL COVERAGE</td>
        <td>TYPE2 BASED PREM</td>
        <td>TYPE2 NAME POLICY PERCENT</td>
        <td>TYPE2 NAME POLICY</td>
        <td>TYPE2 BASIC PREMIUM COVER</td>
        <td>TYPE2 ADDITIONAL PREMIUM COVER</td>
        <td>TYPE2 FLEET PERCENT</td>
        <td>TYPE2 FLEET</td>
        <td>TYPE2 NCB PERCENT</td>
        <td>TYPE2 NCB</td>
        <td>TYPE2 TOTAL PREMIUM</td>
        <td>OD SI</td>
        <td>OD BASED PREM</td>
        <td>OD TOTAL PREMIUM</td>
        <td>DEDUCT PERCENT</td>
        <td>DEDUCT</td>
        <td>CCTV PERCENT</td>
        <td>CCTV</td>
        <td>DIRECT PERCENT</td>
        <td>DIRECT</td>
        <td>TYPE2 OD NET PREMIUM</td>
        <td>TYPE2 OD STAMP</td>
        <td>TYPE2 OD VAT</td>
        <td>TYPE2 OD GROSS PREMIUM</td>
        <td>ADDITIONAL NET PREMIUM</td>
        <td>ADDITIONAL STAMP</td>
        <td>ADDITIONAL VAT</td>
        <td>ADDITIONAL GROSS PREMIUM</td>
        <td>TOTAL</td>
        <!-- <td>TPPD</td> -->
        <td>BKK</td>
    </tr>
    @foreach($data as $item)
    <tr>
        <td>
            @if($item->coverage_id == 1)
            SOMPO2+
            @elseif($item->coverage_id == 2)
            SOMPO3+
            @else
            SOMPO3
            @endif
        </td>
        <td>{{ $item->ft_si }}</td>
        <td>{{ $item->car_code }}</td>
        <td>{{ $item->car_engine }}</td>
        <td>{{ $item->define_name }}</td>
        <td>{{ $item->cctv==1?'YES':'NO' }}</td>
        <td>{{ $item->garage_type }}</td>
        <td>{{ $item->deductible }}</td>
        <td>{{ $item->additional_coverage }}</td>
        <td>{{ $item->based_prem }}</td>
        <td>{{ $item->based_prem_percent."%" }}</td>
        <td>{{ $item->name_policy }}</td>
        <td>{{ $item->basic_premium_cover }}</td>
        <td>{{ $item->add_premium_cover }}</td>
        <td>{{ $item->fleet_percent."%" }}</td>
        <td>{{ $item->fleet }}</td>
        <td>{{ $item->ncb_percent."%" }}</td>
        <td>{{ $item->ncb }}</td>
        <td>{{ $item->total_premium }}</td>
        <td>{{ $item->od_si }}</td>
        <td>{{ $item->od_based_prem }}</td>
        <td>{{ $item->od_total_premium }}</td>
        <td>{{ $item->deduct_percent."%" }}</td>
        <td>{{ $item->deduct }}</td>
        <td>{{ $item->cctv_discount_percent."%" }}</td>
        <td>{{ $item->cctv_discount }}</td>
        <td>{{ $item->direct_percent."%" }}</td>
        <td>{{ $item->direct }}</td>
        <td>{{ $item->net_premium }}</td>
        <td>{{ $item->stamp }}</td>
        <td>{{ $item->vat }}</td>
        <td>{{ $item->gross_premium }}</td>
        <td>{{ $item->flood_net_premium }}</td>
        <td>{{ $item->flood_stamp }}</td>
        <td>{{ $item->flood_vat }}</td>
        <td>{{ $item->flood_gross_premium }}</td>
        <td>{{ $item->gross_premium + $item->flood_gross_premium }}</td>
        <td>{{ $item->is_bangkok==1?'Y':'N' }}</td>

    </tr>
    @endforeach

    
</table> 