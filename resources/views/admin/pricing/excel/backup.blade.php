<table>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    @foreach($pricing as $item)
    <tr>
        <td>{{ $item['coverage_id'] }}</td>
        <td>{{ $item['ft_si'] }}</td>
        <td>{{ $item['car_code'] }}</td>
        <td>{{ $item['car_engine'] }}</td>
        <td>{{ $item['define_name'] }}</td>
        <td>{{ ($item['cctv']==1?"YES":"NO") }}</td>
        <td>{{ $item['garage_type'] }}</td>
        <td>{{ $item['deductible'] }}</td>
        <td>{{ $item['additional_coverage'] }}</td>
        <td>{{ $item['mortor_package_code'] }}</td>
        <td>{{ $item['based_prem'] }}</td>
        <td>{{ $item['based_prem_percent']<=1?($item['based_prem_percent']*100):$item['based_prem_percent'] }}%</td>
        <td>{{ $item['name_policy'] }}</td>
        <td>{{ $item['basic_premium_cover'] }}</td>
        <td>{{ $item['add_premium_cover'] }}</td>
        <td>{{ $item['fleet_percent']<=1?($item['fleet_percent']*100):$item['fleet_percent'] }}%</td>
        <td>{{ $item['fleet'] }}</td>
        <td>{{ $item['ncb_percent']<=1?($item['ncb_percent']*100):$item['ncb_percent'] }}%</td>
        <td>{{ $item['ncb'] }}</td>
        <td>{{ $item['total_premium'] }}</td>
        <td>{{ $item['od_si'] }}</td>
        <td>{{ $item['od_based_prem'] }}</td>
        <td>{{ $item['od_total_premium'] }}</td>
        <td>{{ $item['deduct_percent']<=1?($item['deduct_percent']*100):$item['deduct_percent'] }}%</td>
        <td>{{ $item['deduct'] }}</td>
        <td>{{ $item['cctv_discount_percent']<=1?($item['cctv_discount_percent']*100):$item['cctv_discount_percent'] }}%</td>
        <td>{{ $item['cctv_discount'] }}</td>
        <td>{{ $item['direct_percent']<=1?($item['direct_percent']*100):$item['direct_percent'] }}%</td>
        <td>{{ $item['direct'] }}</td>
        <td>{{ $item['net_premium'] }}</td>
        <td>{{ $item['stamp'] }}</td>
        <td>{{ $item['vat'] }}</td>
        <td>{{ $item['gross_premium'] }}</td>
        <td>{{ $item['flood_net_premium'] }}</td>
        <td>{{ $item['flood_stamp'] }}</td>
        <td>{{ $item['flood_vat'] }}</td>
        <td>{{ $item['flood_gross_premium'] }}</td>
    </tr>
    @endforeach
    
</table> 