@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/pricing.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.pricing.modal.import')
@include('admin.pricing.modal.imported')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-text-o"></i>@lang('admin_pricing.topic')</span>
                    <div class="panel-heading-controls">
                        <a data-toggle="modal" data-target="#modal-import" title="Import CSV"><i class="fa fa-upload"></i></a>
                        <a href="{{ route('admin.pricing_form') }}" title="Add Pricing Plan"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_pricing.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_body_type">@lang('admin_pricing.select_plan')</label>
                                {{ Form::select('coverage_id',['1'=>'SOMPO 2+','2'=>'SOMPO 3+','3'=>'SOMPO 3'],null, ['class' => 'form-control','placeholder' => trans('admin_pricing.select_plan')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_year">@lang('admin_pricing.ft_si')</label>
                                {{ Form::text('ft_si',null, ['class' => 'form-control','placeholder' => trans('admin_pricing.ft_si')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_brand">@lang('admin_pricing.car_code')</label>
                                {{ Form::select('car_code',['110'=>'110','320'=>'320'],null, ['class' => 'form-control','placeholder' => trans('admin_pricing.car_code')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_model">@lang('admin_pricing.car_engine')</label>
                                {{ Form::select('car_engine',["<=2000CC"=>"<=2000CC",">2000CC"=>">2000CC","<=4TONS"=>"<=4TONS",">4TONS"=>">4TONS"],null, ['class' => 'form-control','placeholder' => trans('admin_pricing.car_engine')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_name">@lang('admin_pricing.define_name')</label>
                                {{ Form::select('define_name',["UNNAMED"=>"UNNAMED","18-24Y"=>"18-24Y","25-28Y"=>"25-28Y","29-35Y"=>"29-35Y","36-50Y","50UP"],null, ['class' => 'form-control','placeholder' => trans('admin_pricing.define_name')]) }}
                            </div>
                            <!-- <div class="form-group">
                                <label class="sr-only" for="search_year">Mortor Package Code</label>
                                {{ Form::text('mortor_package_code',null, ['class' => 'form-control','placeholder' => 'Mortor Package Code']) }}
                            </div> -->
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_pricing.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_pricing.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_pricing.no')</th>
                                <th>@lang('admin_pricing.sompo_plan')</th>
                                <th>@lang('admin_pricing.ft_si')</th>
                                <th>@lang('admin_pricing.car_code')</th>
                                <th>@lang('admin_pricing.car_engine')</th>
                                <th>@lang('admin_pricing.define_name')</th>
                                <th>@lang('admin_pricing.garage_type')</th>
                                <th>@lang('admin_pricing.gross_premium')</th>
                                <th>@lang('admin_pricing.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/pricing.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_pricing_list',route('admin.get_pricing_list')) }} 
{{ Form::hidden('hd_delete_pricing',route('admin.delete_pricing')) }} 
{{ Form::hidden('hd_chunks',route('admin.pricing_chunks')) }} 
{{ Form::hidden('hd_backup',route('admin.pricing_backup')) }} 
{{ Form::hidden('hd_import',route('admin.pricing_import')) }} 
{{ Form::hidden('hd_imported_list',route('admin.pricing_imported_list')) }} 
{{ Form::hidden('hd_imported_submit',route('admin.pricing_submit_import')) }} 
@endsection
