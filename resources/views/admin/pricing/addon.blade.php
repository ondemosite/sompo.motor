@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/addon.css') !!}" />
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.pricing.modal.addon')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-text-o"></i>Addon Pricing List</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Pricing"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Coverage</th>
                                <th>Misc Package Code</th>
                                <th>Sum Insured</th>
                                <th>Net Premium</th>
                                <th>Stamp</th>
                                <th>VAT</th>
                                <th>Gross Premium</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/addon.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_addon_list',route('admin.get_addon_list')) }} 
{{ Form::hidden('hd_save_addon',route('admin.save_addon')) }} 
{{ Form::hidden('hd_get_data_addon',route('admin.get_data_addon')) }} 
{{ Form::hidden('hd_delete_addon',route('admin.delete_addon')) }} 
@endsection
