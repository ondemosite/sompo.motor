<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-file-text-o p-r-1"></i><span>Add Addon Pricing</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_addon"),'name'=>'form_addon','class'=>'form_addon','id'=>'form_addon')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label" for="required-input">Taxi</label>
                    {{ Form::text('taxi',null, ['class' => 'form-control','placeholder' => 'Taxi']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">Theft</label>
                    {{ Form::text('theft',null, ['class' => 'form-control','placeholder' => 'Theft']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Misc Package Code</label>
                    {{ Form::text('misc_package_code',null, ['class' => 'form-control input-recal','placeholder' => 'Misc Package Code']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">Sum Insured</label>
                    {{ Form::text('sum_insured',null, ['class' => 'form-control','placeholder' => 'Sum Insured']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">Net Premium</label>
                    {{ Form::text('net_premium',null, ['class' => 'form-control','placeholder' => 'Net Premium']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">Stamp</label>
                    {{ Form::text('stamp',null, ['class' => 'form-control','placeholder' => 'Stamp']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">VAT</label>
                    {{ Form::text('vat',null, ['class' => 'form-control','placeholder' => 'VAT']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">Gross Premium</label>
                    {{ Form::text('gross_premium',null, ['class' => 'form-control','placeholder' => 'Gross Premium']) }} 
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_button">Save changes</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>