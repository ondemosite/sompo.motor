<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-file-text-o p-r-1"></i><span>@lang('admin_compulsory.add_compulsory')</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_compulsory"),'name'=>'form_compulsory','class'=>'form_compulsory','id'=>'form_compulsory')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_compulsory.body_type')</label>
                    {{ Form::text('body_type',null, ['class' => 'form-control','placeholder' => 'Body Type']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Motor Code AC</label>
                    {{ Form::text('motor_code_ac',null, ['class' => 'form-control','placeholder' => 'Motor Code AC']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_compulsory.net_premium')</label>
                    {{ Form::text('net_premium',null, ['class' => 'form-control input-recal','placeholder' => trans('admin_compulsory.net_premium') ]) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">@lang('admin_compulsory.stamp')</label>
                    {{ Form::text('stamp',null, ['class' => 'form-control','placeholder' => 'Stamp','readonly']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">@lang('admin_compulsory.vat')</label>
                    {{ Form::text('vat',null, ['class' => 'form-control','placeholder' => 'vat','readonly']) }} 
                </div>
                <div class="form-group">
                    <label class="control-label" for="required-input">@lang('admin_compulsory.gross_premium')</label>
                    {{ Form::text('gross_premium',null, ['class' => 'form-control','placeholder' => 'Gross Premium','readonly']) }} 
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">@lang('admin_compulsory.close')</button>
                <button type="button" class="btn btn-primary" id="submit_button">@lang('admin_compulsory.save')</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>