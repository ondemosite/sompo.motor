<div class="modal fade" id="modal-imported" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-upload p-r-1"></i><span>ตรวจสอบข้อมูลและยืนยัน</span></h4>
            </div> 
            <div class="modal-body">
                <div class="table-light">
                    <table class="table table-striped table-bordered" id="datatablesImported">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Coverage</th>
                            <th>FT SI</th>
                            <th>CAR CODE</th>
                            <th>CAR ENGINE</th>
                            <th>DEFINE NAME</th>
                            <th>CCTV</th>
                            <th>GARAGE TYPE</th>
                            <th>DEDUCTIBLE</th>
                            <th>ADDITIONAL COVERAGE</th>
                            <th>MOTOR PACKAGE CODE</th>
                            <th>BASED PREM</th>
                            <th>BASED PREM PERCENT</th>
                            <th>NAME POLICY</th>
                            <th>BASED PREMIUM COVER</th>
                            <th>ADD PREMIUM COVER</th>
                            <th>FLEET PERCENT</th>
                            <th>FLEET</th>
                            <th>NCB PERCENT</th>
                            <th>NCB</th>
                            <th>TOTAL PREMIUM</th>
                            <th>OD SI</th>
                            <th>OD BASED PREMIUM</th>
                            <th>OD TOTAL PREMIUM</th>
                            <th>DEDUCT PERCENT</th>
                            <th>DEDUCT</th>
                            <th>CCTV DISCOUNT PERCENT</th>
                            <th>CCTV DISCOUNT</th>
                            <th>DIRECT PERCENT</th>
                            <th>DIRECT</th>
                            <th>NET PREMIUM</th>
                            <th>STAMP</th>
                            <th>VAT</th>
                            <th>GROSS PREMIUM</th>
                            <th>FLOOD NET PREMIUM</th>
                            <th>FLOOD STAMP</th>
                            <th>FLOOD VAT</th>
                            <th>FLOOD GROSS PREMIUM</th>
                            <th>IS BANGKOK</th>
                        </tr>
                        </thead>
                    </table>
                </div> <!-- /table-light -->    
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <span class="import-status pull-left"></span>
                <button type="button" class="btn" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="submit_import_change">ยืนยัน</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>