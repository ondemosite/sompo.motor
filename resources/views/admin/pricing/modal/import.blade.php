<div class="modal fade" id="modal-import" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-upload p-r-1"></i><span>Import XLS</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.pricing_import"),'name'=>'form_import','class'=>'form_import','id'=>'form_import','files'=>true)) }}
                <div class="row">
                    <div class="col-xs-12">
                        <label class="custom-file m-b-2">
                            <input type="file" id="csv_file" class="custom-file-input" name="csv_file" accept=".xls">
                            <span class="custom-file-control form-control">Choose file...</span>
                        </label>
                    </div>
                </div>
                <hr/>
                <p><b>คำแนะนำ</b></p>
                <p>1. ดาวน์โหลดแบบฟอร์มได้ที่นี่ <a href="{{ route('admin.pricing.export') }}">ดาวน์โหลดแบบฟอร์ม</a></p>
                <p>2. เมื่อแก้ไขข้อมูลเสร็จแล้วบันทึกไฟล์</p>
                <p>3. อัพโหลดไฟล์ XLS ที่ฟอร์มอัพโหลดด้านบน จากนั้นกด Import</p>
                <!--<p style="color:#006DCA">เพิ่มเติม ระบบมีการ Backup ข้อมูลเดิมหากมีการ Import ล้มเหลวระบบจะ Rollback กลับคืนค่าเดิม</p>-->
                <p style="color:#F3020D">ปล.หากต้องการแก้ไขแบบฟอร์มข้อมูล กรุณาแจ้งทางผู้พัฒนาโปรแกรม</p>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <span class="import-status pull-left"></span>
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_import">Import</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>