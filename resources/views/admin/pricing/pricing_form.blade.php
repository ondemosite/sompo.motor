@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/pricing_form.css') !!}">
@endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_pricing.add') <span class="text-muted font-weight-light">@lang('admin_pricing.pricing_plan')</span></h1>
@else
<h1>@lang('admin_pricing.edit') <span class="text-muted font-weight-light">@lang('admin_pricing.pricing_plan')</span></h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-text-o"></i>@lang('admin_pricing.add')/@lang('admin_pricing.edit') @lang('admin_pricing.pricing_plan')</span>
                </div>
                {{ Form::open(['url' => route("admin.save_pricing"),'id'=>'form_pricing','name'=>'form_pricing','class'=>'form_pricing'])}}
                {{ Form::hidden('pricing_id',!empty($data['id'])?$data['id']:'') }}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 ci_box">
                            <fieldset class="col-md-12">    	
                            <legend class="dark-orange">@lang('admin_pricing.standard')</legend>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.pricing_plan')</label>
                                    {{ Form::select('coverage_id',!empty($init['coverage'])?$init['coverage']:[],!empty($data['coverage_id'])?$data['coverage_id']:null, 
                                    ['class' => 'form-control','placeholder' => trans('admin_pricing.pricing_plan')]) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.ft_si')</label>
                                    {{ Form::text('ft_si',!empty($data['ft_si'])?$data['ft_si']:'', ['class' => 'form-control input-money','placeholder' => trans('admin_pricing.ft_si')]) }}
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.car_engine')</label>
                                    {{ Form::select('car_engine',!empty($init['car_engine'])?$init['car_engine']:[],!empty($data['car_engine'])?$data['car_engine']:null, 
                                    ['class' => 'form-control','placeholder' => trans('admin_pricing.car_engine')]) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.car_code')</label>
                                    {{ Form::select('car_code',!empty($init['car_code'])?$init['car_code']:[],!empty($data['car_code'])?$data['car_code']:null, 
                                    ['class' => 'form-control','placeholder' => trans('admin_pricing.car_code')]) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.define_name')</label>
                                    {{ Form::select('define_name',!empty($init['define_name'])?$init['define_name']:[],!empty($data['define_name'])?$data['define_name']:null, 
                                    ['class' => 'form-control','placeholder' => trans('admin_pricing.define_name')]) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label class="required">CCTV</label><br/>
                                    <label class="custom-control custom-radio radio-inline">
                                        {{ Form::radio('cctv',1,(!empty($data['cctv']) && $data['cctv']==1)?true:false,['class' => 'custom-control-input']) }}
                                        <span class="custom-control-indicator"></span>
                                        YES
                                    </label>
                                    <label class="custom-control custom-radio radio-inline">
                                        {{ Form::radio('cctv',1,(empty($data['cctv']) || $data['cctv']==0)?true:false,['class' => 'custom-control-input']) }}
                                        <span class="custom-control-indicator"></span>
                                        NO
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.garage_type')</label>
                                    {{ Form::select('garage_type',!empty($init['garage_type'])?$init['garage_type']:[],!empty($data['garage_type'])?$data['garage_type']:null, 
                                    ['class' => 'form-control','placeholder' => trans('admin_pricing.garage_type')]) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.deductible')</label>
                                    {{ Form::text('deductible',!empty($data['deductible'])?$data['deductible']:0, ['class' => 'form-control input-money input-recal','placeholder' => trans('admin_pricing.deductible')]) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">@lang('admin_pricing.addition_coverge')</label>
                                    {{ Form::select('additional_coverage',!empty($init['additional_coverage'])?$init['additional_coverage']:[],!empty($data['additional_coverage'])?$data['additional_coverage']:null, 
                                    ['class' => 'form-control','placeholder' => trans('admin_pricing.addition_coverge')]) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label class="required">Mortor Package Code</label>
                                    {{ Form::text('mortor_package_code',!empty($data['mortor_package_code'])?$data['mortor_package_code']:'', ['class' => 'form-control input-money','placeholder' => 'Mortor Package Code']) }}
                                </div>
                            </div>
                            </fieldset>		
                            <fieldset class="col-md-12">    	
                            <legend class="dark-orange">@lang('admin_pricing.type')</legend>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">Based Prem</label>
                                    {{ Form::text('based_prem',!empty($data['based_prem'])?$data['based_prem']:0, ['class' => 'form-control input-money','placeholder' => 'Based Prem']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">Based Prem (%)</label>
                                    <div class="input-group">
                                        {{ Form::text('based_prem_percent',!empty($data['based_prem_percent'])?$data['based_prem_percent']:0, ['class' => 'form-control input-money','placeholder' => 'Based Prem (%)']) }}
                                        <div class="input-group-addon">%</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Name Policy</label>
                                    {{ Form::text('name_policy',!empty($data['name_policy'])?$data['name_policy']:0, ['class' => 'form-control input-money','placeholder' => 'Name Policy']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">Basic Premium Cover</label>
                                    {{ Form::text('basic_premium_cover',!empty($data['basic_premium_cover'])?$data['basic_premium_cover']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'Basic Premium Cover']) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label class="required">Additional premium cover</label>
                                    {{ Form::text('add_premium_cover',!empty($data['add_premium_cover'])?$data['add_premium_cover']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'Additional premium cover']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Fleet (%)</label>
                                    <div class="input-group">
                                        {{ Form::text('fleet_percent',!empty($data['fleet_percent'])?$data['fleet_percent']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'Fleet (%)']) }}
                                        <div class="input-group-addon">%</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Fleet</label>
                                    {{ Form::text('fleet',!empty($data['fleet'])?$data['fleet']:0, ['class' => 'form-control','readonly']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>NCB (%)</label>
                                    <div class="input-group">
                                        {{ Form::text('ncb_percent',!empty($data['ncb_percent'])?$data['ncb_percent']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'NCB (%)']) }}
                                        <div class="input-group-addon">%</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>NCB</label>
                                    {{ Form::text('ncb',!empty($data['ncb'])?$data['ncb']:0, ['class' => 'form-control','readonly']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Total Premium</label>
                                    {{ Form::text('total_premium',!empty($data['total_premium'])?$data['total_premium']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                            </div>
                            </fieldset>	
                            <fieldset class="col-md-12">    	
                            <legend class="dark-orange">OD Plus</legend>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>OD SI</label>
                                    {{ Form::text('od_si',!empty($data['od_si'])?$data['od_si']:0, ['class' => 'form-control input-money','placeholder' => 'od_si']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>OD Based Prem</label>
                                    {{ Form::text('od_based_prem',!empty($data['od_based_prem'])?$data['od_based_prem']:0, ['class' => 'form-control input-money','placeholder' => 'OD Based Prem']) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>OD Total Premium</label>
                                    {{ Form::text('od_total_premium',!empty($data['od_total_premium'])?$data['od_total_premium']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'OD Total Premium']) }}
                                </div>
                            </div>
                            </fieldset>	
                        </div>
                        <div class="col-md-6 ci_box">
                            <fieldset class="col-md-12">    	
                            <legend class="dark-orange">Deduct & Discount</legend>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Deduct (%)</label>
                                    <div class="input-group">
                                        {{ Form::text('deduct_percent',!empty($data['deduct_percent'])?$data['deduct_percent']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'Deduct (%)']) }}
                                        <div class="input-group-addon">%</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Deduct</label>
                                    {{ Form::text('deduct',!empty($data['deduct'])?$data['deduct']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>CCTV Discount (%)</label>
                                    <div class="input-group">
                                        {{ Form::text('cctv_discount_percent',!empty($data['cctv_discount_percent'])?$data['cctv_discount_percent']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'CCTV Discount (%)']) }}
                                        <div class="input-group-addon">%</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>CCTV Discount</label>
                                    {{ Form::text('cctv_discount',!empty($data['cctv_discount'])?$data['cctv_discount']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Direct Discount (%)</label>
                                    <div class="input-group">
                                        {{ Form::text('direct_percent',!empty($data['direct_percent'])?$data['direct_percent']:0, ['class' => 'form-control input-money input-recal','placeholder' => 'Direct Discount (%)']) }}
                                        <div class="input-group-addon">%</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Direct</label>
                                    {{ Form::text('direct',!empty($data['direct'])?$data['direct']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                            </div>
                            </fieldset>	
                            <fieldset class="col-md-12">    	
                            <legend class="dark-orange">OD Calculate</legend>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Net Premium</label>
                                    {{ Form::text('net_premium',!empty($data['net_premium'])?$data['net_premium']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Stamp</label>
                                    {{ Form::text('stamp',!empty($data['stamp'])?$data['stamp']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>VAT</label>
                                    {{ Form::text('vat',!empty($data['vat'])?$data['vat']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Gross Premium</label>
                                    {{ Form::text('gross_premium',!empty($data['gross_premium'])?$data['gross_premium']:0, ['class' => 'form-control input-money','readonly']) }}
                                </div>
                            </div>
                            </fieldset>	
                            <fieldset class="col-md-12">    	
                            <legend class="dark-orange">Flood Add-on</legend>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Flood Net Premium</label>
                                    {{ Form::text('flood_net_premium',!empty($data['flood_net_premium'])?$data['flood_net_premium']:0, ['class' => 'form-control input-money','placeholder' => 'Flood Net Premium']) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Flood Stamp</label>
                                    {{ Form::text('flood_stamp',!empty($data['flood_stamp'])?$data['flood_stamp']:0, ['class' => 'form-control input-money','placeholder' => 'Flood Stamp']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Flood VAT</label>
                                    {{ Form::text('flood_vat',!empty($data['flood_vat'])?$data['flood_vat']:0, ['class' => 'form-control input-money','placeholder' => 'Flood VAT']) }}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Flood Gross Premium</label>
                                    {{ Form::text('flood_gross_premium',!empty($data['flood_gross_premium'])?$data['flood_gross_premium']:0, ['class' => 'form-control input-money','placeholder' => 'Flood Gross Premium']) }}
                                </div>
                            </div>
                            </fieldset>
                            <fieldset class="col-md-12">    	
                            <legend class="dark-orange">Is Bangkok</legend>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label class="required">is bangkok</label><br/>
                                    <label class="custom-control custom-radio radio-inline">
                                        {{ Form::radio('is_bangkok',1,(!empty($data['is_bangkok']) && $data['is_bangkok']==1)?true:false,['class' => 'custom-control-input']) }}
                                        <span class="custom-control-indicator"></span>
                                        YES
                                    </label>
                                    <label class="custom-control custom-radio radio-inline">
                                        {{ Form::radio('is_bangkok',0,(empty($data['is_bangkok']) || $data['is_bangkok']==0)?true:false,['class' => 'custom-control-input']) }}
                                        <span class="custom-control-indicator"></span>
                                        NO
                                    </label>
                                </div>
                            </div>
                            </fieldset>	
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{{ asset('component/jquery-money-format/jquery-money-format.js') }}"></script>
<script src="{{ asset('component/loader/loader.js') }}"></script>
<script src="{{ asset('admin/js/pricing_form.js') }}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_data_vehicle',route('admin.get_data_vehicle')) }} 
{{ Form::hidden('hd_get_select_model',route('admin.get_select_model')) }} 

@endsection


