@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/compulsory.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.pricing.modal.compulsory')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-text-o"></i>@lang('admin_compulsory.compulsory_topic')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Compulsory"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_compulsory.no')</th>
                                <th>@lang('admin_compulsory.body_type')</th>
                                <th>Motor Code AC</th>
                                <th>@lang('admin_compulsory.net_premium')</th>
                                <th>@lang('admin_compulsory.stamp')</th>
                                <th>@lang('admin_compulsory.vat')</th>
                                <th>@lang('admin_compulsory.gross_premium')</th>
                                <th>@lang('admin_compulsory.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/compulsory.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_compulsory_list',route('admin.get_compulsory_list')) }} 
{{ Form::hidden('hd_save_compulsory',route('admin.save_compulsory')) }} 
{{ Form::hidden('hd_get_data_compulsory',route('admin.get_data_compulsory')) }} 
{{ Form::hidden('hd_delete_compulsory',route('admin.delete_compulsory')) }} 
@endsection
