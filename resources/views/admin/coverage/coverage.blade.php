@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/coverage.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group panel-group-danger panel-group-dark">
                <div class="panel">
                    <div class="panel-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#panel-body-2plus">
                        <h1 class="title">SOMPO <b> 2+</b></h1>
                        <p>@lang('admin_coverage.2plus_detail')</p>
                    </a>
                    </div>
                    <div id="panel-body-2plus" class="panel-collapse collapse">
                        {{ Form::open(['url' => route("admin.save_coverage"),'class'=>'form_coverage'])}}
                        {{ Form::hidden('coverage_id',1) }}
                        <div class="panel-body">
                            <fieldset class="col-md-12">    	
                                <legend class="dark-red">@lang('admin_coverage.outside_coverage')</legend>
                                <div class="field-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">@lang('admin_coverage.damage') @lang('admin_coverage.per_person')</label>
                                            {{ Form::text('person_damage_person',!empty($data['2plus']['person_damage_person'])?$data['2plus']['person_damage_person']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">@lang('admin_coverage.damage') @lang('admin_coverage.per_once')</label>
                                            {{ Form::text('person_damage_once',!empty($data['2plus']['person_damage_once'])?$data['2plus']['person_damage_once']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">@lang('admin_coverage.stuff_damage')</label>
                                            {{ Form::text('stuff_damage',!empty($data['2plus']['stuff_damage'])?$data['2plus']['stuff_damage']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                        </div>
                                    </div>
                                </div>
                            </fieldset>				
                            <div class="clearfix"></div>
                            <fieldset class="col-md-12">    	
                                <legend class="dark-red">@lang('admin_coverage.under_coverage')</legend>
                                <div class="field-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">@lang('admin_coverage.death_disable')</label>
                                            {{ Form::text('death_disabled',!empty($data['2plus']['death_disabled'])?$data['2plus']['death_disabled']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">@lang('admin_coverage.medical_free')</label>
                                            {{ Form::text('medical_fee',!empty($data['2plus']['medical_fee'])?$data['2plus']['medical_fee']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">@lang('admin_coverage.bail_driver')</label>
                                            {{ Form::text('bail_driver',!empty($data['2plus']['bail_driver'])?$data['2plus']['bail_driver']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                        </div>
                                    </div>
                                </div>
                            </fieldset>		
                            <div class="clearfix"></div>		
                            <fieldset class="col-md-12">    	
                                <legend class="dark-red">@lang('admin_coverage.general')</legend>
                                <div class="field-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">รับประกันรถที่มีอายุไม่เกิน (ปี)</label>
                                            {{ Form::number('max_age',!empty($data['2plus']['max_age'])?$data['2plus']['max_age']:0, 
                                            ['class' => 'form-control','placeholder' => 'รับประกันรถที่มีอายุไม่เกิน']) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">รับประกันรถที่มีอายุไม่ต่ำกว่า (ปี)</label>
                                            {{ Form::number('min_age',!empty($data['2plus']['min_age'])?$data['2plus']['min_age']:0, 
                                            ['class' => 'form-control','placeholder' => 'รับประกันรถที่มีอายุไม่ต่ำกว่า']) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="text-muted">@lang('admin_coverage.max_pre')</label>
                                            {{ Form::number('max_pre',!empty($data['2plus']['max_pre'])?$data['2plus']['max_pre']:0, 
                                            ['class' => 'form-control','placeholder' => trans('admin_coverage.max_age')]) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label>&nbsp;</label>
                                            <label class="switcher switcher-lg switcher-primary">
                                                <input type="checkbox" name="status" {{ $data['2plus']['status']=='ACTIVE'?'checked':'' }}>
                                                <div class="switcher-indicator">
                                                    <div class="switcher-yes">@lang('common.active')</div>
                                                    <div class="switcher-no">@lang('common.inactive')</div>
                                                </div>                     
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>		
                            <div class="clearfix"></div>		
                        </div>
                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-primary">@lang('admin_coverage.submit')</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div><!-- panel-group -->
            
            <div class="panel-group panel-group-success panel-group-dark">
                <div class="panel">
                    <div class="panel-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#panel-body-3plus">
                        <h1 class="title">SOMPO <b> 3+</b></h1>
                        <p>@lang('admin_coverage.3plus_detail')</p>
                    </a>
                    </div>
                    <div id="panel-body-3plus" class="panel-collapse collapse">
                        <div class="panel-body">
                            {{ Form::open(['url' => route("admin.save_coverage"),'class'=>'form_coverage'])}}
                            {{ Form::hidden('coverage_id',2) }}
                            <div class="panel-body">
                                <fieldset class="col-md-12">    	
                                    <legend class="dark-green">@lang('admin_coverage.outside_coverage')</legend>
                                    <div class="field-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.damage') @lang('admin_coverage.per_person')</label>
                                                {{ Form::text('person_damage_person',!empty($data['3plus']['person_damage_person'])?$data['3plus']['person_damage_person']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.damage') @lang('admin_coverage.per_once')</label>
                                                {{ Form::text('person_damage_once',!empty($data['3plus']['person_damage_once'])?$data['3plus']['person_damage_once']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.stuff_damage')</label>
                                                {{ Form::text('stuff_damage',!empty($data['3plus']['stuff_damage'])?$data['3plus']['stuff_damage']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>				
                                <div class="clearfix"></div>
                                <fieldset class="col-md-12">    	
                                    <legend class="dark-green">@lang('admin_coverage.under_coverage')</legend>
                                    <div class="field-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.death_disable')</label>
                                                {{ Form::text('death_disabled',!empty($data['3plus']['death_disabled'])?$data['3plus']['death_disabled']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.medical_free')</label>
                                                {{ Form::text('medical_fee',!empty($data['3plus']['medical_fee'])?$data['3plus']['medical_fee']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.bail_driver')</label>
                                                {{ Form::text('bail_driver',!empty($data['3plus']['bail_driver'])?$data['3plus']['bail_driver']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>		
                                <div class="clearfix"></div>			
                                <fieldset class="col-md-12">    	
                                    <legend class="dark-green">@lang('admin_coverage.general')</legend>
                                    <div class="field-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">รับประกันรถที่มีอายุไม่เกิน (ปี)</label>
                                                {{ Form::number('max_age',!empty($data['3plus']['max_age'])?$data['3plus']['max_age']:0, 
                                                ['class' => 'form-control','placeholder' => 'รับประกันรถที่มีอายุไม่เกิน']) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">รับประกันรถที่มีอายุไม่ต่ำกว่า (ปี)</label>
                                                {{ Form::number('min_age',!empty($data['3plus']['min_age'])?$data['3plus']['min_age']:0, 
                                                ['class' => 'form-control','placeholder' => 'รับประกันรถที่มีอายุไม่ต่ำกว่า']) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.max_pre')</label>
                                                {{ Form::number('max_pre',!empty($data['3plus']['max_pre'])?$data['3plus']['max_pre']:0, 
                                                ['class' => 'form-control','placeholder' => trans('admin_coverage.max_age')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 form-group">
                                                <label>&nbsp;</label>
                                                <label class="switcher switcher-lg switcher-success">
                                                    <input type="checkbox" name="status" {{ $data['3plus']['status']=='ACTIVE'?'checked':'' }}>
                                                    <div class="switcher-indicator">
                                                        <div class="switcher-yes">@lang('common.active')</div>
                                                        <div class="switcher-no">@lang('common.inactive')</div>
                                                    </div>                     
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>			
                            </div>
                            <div class="panel-footer text-right">
                                <button type="submit" class="btn btn-success">@lang('admin_coverage.submit')</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-group panel-group-warning panel-group-dark">
                <div class="panel">
                    <div class="panel-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-example-danger" href="#panel-body-plus">
                        <h1 class="title">SOMPO <b> 3</b></h1>
                        <p>@lang('admin_coverage.type3_detail')</p>
                    </a>
                    </div>
                    <div id="panel-body-plus" class="panel-collapse collapse">
                        <div class="panel-body">
                            {{ Form::open(['url' => route("admin.save_coverage"),'class'=>'form_coverage'])}}
                            {{ Form::hidden('coverage_id',3) }}
                            <div class="panel-body">
                                <fieldset class="col-md-12">    	
                                    <legend class="dark-orange">@lang('admin_coverage.outside_coverage')</legend>
                                    <div class="field-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.damage') @lang('admin_coverage.per_person')</label>
                                                {{ Form::text('person_damage_person',!empty($data['type3']['person_damage_person'])?$data['type3']['person_damage_person']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.damage') @lang('admin_coverage.per_once')</label>
                                                {{ Form::text('person_damage_once',!empty($data['type3']['person_damage_once'])?$data['type3']['person_damage_once']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.stuff_damage')</label>
                                                {{ Form::text('stuff_damage',!empty($data['type3']['stuff_damage'])?$data['type3']['stuff_damage']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>				
                                <div class="clearfix"></div>
                                <fieldset class="col-md-12">    	
                                    <legend class="dark-orange">@lang('admin_coverage.under_coverage')</legend>
                                    <div class="field-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.death_disable')</label>
                                                {{ Form::text('death_disabled',!empty($data['type3']['death_disabled'])?$data['type3']['death_disabled']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.medical_free')</label>
                                                {{ Form::text('medical_fee',!empty($data['type3']['medical_fee'])?$data['type3']['medical_fee']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.bail_driver')</label>
                                                {{ Form::text('bail_driver',!empty($data['type3']['bail_driver'])?$data['type3']['bail_driver']:0, ['class' => 'form-control input-money','placeholder' => trans('common.specify_coverage')]) }}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>		
                                <div class="clearfix"></div>			
                                <fieldset class="col-md-12">    	
                                    <legend class="dark-orange">@lang('admin_coverage.general')</legend>
                                    <div class="field-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">รับประกันรถที่มีอายุไม่เกิน (ปี)</label>
                                                {{ Form::number('max_age',!empty($data['type3']['max_age'])?$data['type3']['max_age']:0, 
                                                ['class' => 'form-control','placeholder' => 'รับประกันรถที่มีอายุไม่เกิน']) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">รับประกันรถที่มีอายุไม่ต่ำกว่า (ปี)</label>
                                                {{ Form::number('min_age',!empty($data['type3']['min_age'])?$data['type3']['min_age']:0, 
                                                ['class' => 'form-control','placeholder' => 'รับประกันรถที่มีอายุไม่ต่ำกว่า']) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-muted">@lang('admin_coverage.max_pre')</label>
                                                {{ Form::number('max_pre',!empty($data['type3']['max_pre'])?$data['type3']['max_pre']:0, 
                                                ['class' => 'form-control','placeholder' => trans('admin_coverage.max_age')]) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 form-group">
                                                <label>&nbsp;</label>
                                                <label class="switcher switcher-lg switcher-warning">
                                                    <input type="checkbox" name="status" {{ $data['type3']['status']=='ACTIVE'?'checked':'' }}>
                                                    <div class="switcher-indicator">
                                                        <div class="switcher-yes">@lang('common.active')</div>
                                                        <div class="switcher-no">@lang('common.inactive')</div>
                                                    </div>                     
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>		
                            </div>
                            <div class="panel-footer text-right">
                                <button type="submit" class="btn btn-warning">@lang('admin_coverage.submit')</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{{ asset('component/jquery-money-format/jquery-money-format.js') }}"></script>
<script src="{!! asset('admin/js/coverage.js') !!}"></script>
@endsection

@section('hidden')

@endsection
