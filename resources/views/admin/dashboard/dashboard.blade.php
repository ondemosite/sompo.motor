@extends('admin.layout.default')

@section('css')
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
@endsection
@section('hidden')
@endsection

@section('breadcrumb')
<h1 class="pull-xs-left"><span class="text-muted font-weight-light"><i class="page-header-icon fa fa-th-large"></i>@lang('admin_dashboard.dashboard')</span></h1>
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-sm-9">
            <div class="row">
                @if(!empty($init['earned_today']) || $init['earned_today']==0)
                <div class="col-sm-4">
                    <div class="box bg-danger darken">
                        <div class="box-cell p-x-3 p-y-1">
                            <div class="font-weight-semibold font-size-12">@lang('admin_dashboard.earned_today')</div>
                            <div class="font-weight-bold font-size-20">{{ number_format($init['earned_today'],2) }} <small class="font-weight-light">@lang('common.baht')</small></div>
                            <i class="box-bg-icon middle right font-size-52 ion-arrow-graph-up-right"></i>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($init['earned_all']) || $init['earned_all']==0)
                <div class="col-sm-4">
                    <div class="box bg-success darken">
                        <div class="box-cell p-x-3 p-y-1">
                            <div class="font-weight-semibold font-size-12">@lang('admin_dashboard.total_income')</div>
                            <div class="font-weight-bold font-size-20">{{ number_format($init['earned_all'],2) }} <small class="font-weight-light">@lang('common.baht')</small></div>
                            <i class="box-bg-icon middle right font-size-52 ion-arrow-graph-up-right"></i>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($init['active_insurance']) || $init['active_insurance']==0)
                <div class="col-sm-4">
                    <div class="box bg-info darken">
                        <div class="box-cell p-x-3 p-y-1">
                            <div class="font-weight-semibold font-size-12">@lang('admin_dashboard.active_insurance')</div>
                            <div class="font-weight-bold font-size-20">{{ $init['active_insurance']}}</div>
                            <i class="box-bg-icon middle right font-size-52 ion-ios-people"></i>
                        </div>
                    </div>
                </div>
                @endif
            </div><!-- /row -->
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-title">@lang('admin_dashboard.insurance_popular')</div>
                        <hr>
                        <div class="panel-body">
                            <canvas id="chart-polar-area" height="250"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-title">@lang('admin_dashboard.monthly_summary')</div>
                        <hr>
                        <div class="panel-body">
                            <canvas id="chart-graph" width="400" height="153"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /col-sm-8 -->
        <div class="col-sm-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box bg-warning">
                        <div class="box-row">
                            <div class="box-cell p-a-2">
                            <div class="font-weight-semibold font-size-17">{{ number_format($init['visitor']['percent'],0)}}% @lang('admin_dashboard.more')</div>
                            <div class="font-size-12">@lang('admin_dashboard.monthly_visitor')</div>
                            </div>
                        </div>
                        <div class="box-row">
                            <div class="box-cell p-x-2 p-b-1 valign-bottom text-xs-center">
                            <span id="sparkline-2"><canvas width="478" height="42" style="display: inline-block; width: 478px; height: 42px; vertical-align: top;"></canvas></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel box">
                        <div class="box-row">
                            <div class="box-cell p-x-2 p-y-1 bg-black text-xs-center font-size-11 font-weight-semibold">
                            <i class="fa fa-cloud"></i>&nbsp;&nbsp;@lang('admin_dashboard.used_ram')
                            </div>
                        </div>
                        <div class="box-row">
                            <div class="box-cell p-y-2">
                            <div class="easy-pie-chart" data-suffix="%" id="easy-pie-chart-3"><span class="font-size-14 font-weight-light"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel box">
                        <div class="box-row">
                            <div class="box-cell p-x-2 p-y-1 bg-black text-xs-center font-size-11 font-weight-semibold">
                            <i class="fa fa-flash"></i>&nbsp;&nbsp;@lang('admin_dashboard.hard_disk')
                            </div>
                        </div>
                        <div class="box-row">
                            <div class="box-cell p-y-2">
                            <div class="easy-pie-chart" data-suffix="%" id="easy-pie-chart-2"><span class="font-size-14 font-weight-light"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /col-sm-4 -->
    </div><!-- /row -->
    <div class="row">
    <div class="panel panel-danger panel-dark">
          <div class="panel-heading" style="padding:15px;">
            <span class="panel-title"><i class="panel-title-icon fa fa-edit"></i>@lang('admin_dashboard.new_orders')</span>
          </div>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>@lang('admin_dashboard.order_number')</th>
                  <th>@lang('admin_dashboard.plan_name')</th>
                  <th>@lang('admin_dashboard.full_name')</th>
                  <th>@lang('admin_dashboard.contact')</th>
                  <th>@lang('admin_dashboard.amount')</th>
                  <th>@lang('admin_dashboard.created_at')</th>
                  <th>@lang('admin_dashboard.status')</th>
                </tr>
              </thead>
              <tbody class="valign-middle">
                @foreach($init['order'] as $index => $item)
                @php
                    $driver = $item->main_driver()->first();
                    $bg_color = "#FFFFFF";
                    if($index%2!=0) $bg_color = "#F1F1F1";
                @endphp
                    <tr style="background-color:{{$bg_color}}">
                        <td>{{($index+1)}}</td>
                        <td>{{$item['order_number']}}</td>
                        <td>{{$item['insurance_plan_name']}}</td>
                        <td>{{$driver->name." ".$driver->lastname}}</td>
                        <td>{!! "E-mail: ".$driver->email."<br/>Tel: ".$driver->tel !!}</td>
                        <td>{{number_format($item['payment_result'],2)}}  Baht</td>
                        <td>{{ getLocaleDate($item['created_at'],true) }}</td>
                        <td>
                            @if($item['status']=="PAID")
                                <span class="text-success font-weight-bold">{{$item['status']}}</span>
                            @else
                                <span>{{$item['status']}}</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script type="text/javascript">

    var COLORS = [
      '#0288D1',
      '#FF4081',
      '#4CAF50',
      '#D32F2F',
      '#FFC107',
      '#673AB7',
      '#FF5722',
      '#CDDC39',
      '#795548',
      '#607D8B',
      '#009688',
      '#E91E63',
      '#9E9E9E',
      '#E040FB',
      '#00BCD4',
    ];

    function shuffle(a) {
        var j;
        var x;
        var i;

        for (i = a.length; i; i -= 1) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    }

    function getRandomData(max, min) {
        return Math.floor(Math.random() * ((max || 100) - (min || 0))) + (min || 0);
    }

    function getRandomColors(count) {
        if (count && count > COLORS.length) {
            throw new Error('Have not enough colors');
        }
        var clrLeft = count || COLORS.length;
        var source  = [].concat(COLORS);
        var result  = [];

        while (clrLeft-- > 0) {
            result.unshift(source[source.length > 1 ? getRandomData(source.length - 1) : 0]);
            source.splice(source.indexOf(result[0]), 1);
        }
        shuffle(result);
        return result;
    }

    $(document).ready(function(){
        //  Graph
        var colors = getRandomColors(2);
        var data = {
            labels: {!! json_encode($init['monthly']['month']) !!},
            datasets: [
                {
                    label:           'Monthly Earned Statics (Baht / Month)',
                    data:            {!! json_encode($init['monthly']['value']) !!},
                    borderWidth:     1,
                    backgroundColor: pxUtil.hexToRgba(colors[1], 0.3),
                    borderColor:     colors[1],
                }
            ],
        };
        new Chart(document.getElementById('chart-graph').getContext("2d"), {
            type: 'line',
            data: data,
        });

        // ************ Pie Chart **************
        var data = {
            datasets: [
                {
                    data:            {!! json_encode($init['popular']) !!},
                    backgroundColor: [ '#FF6384', '#4BC0C0', '#FFCE56'],
                }
            ],
            labels: [ 'SOMPO 2+', 'SOMPO 3+', 'SOMPO 3']
        };
        new Chart(document.getElementById('chart-polar-area').getContext("2d"), {
            type: 'polarArea',
            data: data,
        });


        // Easy Pie Chart
        var data = [
            getRandomData(1000, 1),
            getRandomData(100, 1),
            getRandomData(64, 1),
        ];
        var colors = getRandomColors();
        var config = {
            animate: 2000,
            scaleColor: false,
            lineWidth: 4,
            lineCap: 'square',
            size: 90,
            trackColor: 'rgba(0, 0, 0, .09)',
            onStep: function(_from, _to, currentValue) {
            var value = $(this.el).attr('data-max-value') * currentValue / 100;

            $(this.el)
                .find('> span')
                .text(Math.round(value) + $(this.el).attr('data-suffix'));
            },
        }

        // Ram ****************
        $('#easy-pie-chart-2')
        .attr('data-percent', (data[1] / 100) * 100)
        .attr('data-max-value', 100)
        .easyPieChart($.extend({}, config, { barColor: colors[1] }));

        // Hardisk **************
        $('#easy-pie-chart-3')
        .attr('data-percent', (data[2] / 64) * 100)
        .attr('data-max-value', 64)
        .easyPieChart($.extend({}, config, { barColor: colors[2] }));

        // ******************** Visitor ******************* 
        var visitor_data = {!! json_encode($init['visitor']['value']) !!}
        $("#sparkline-2").pxSparkline(visitor_data, {
            type: 'bar',
            height: '42px',
            width: '100%',
            barSpacing: 2,
            zeroAxis: false,
            barColor: '#ffffff',
        });


    });
</script>
@endsection




