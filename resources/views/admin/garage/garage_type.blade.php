@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/garage_type.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.garage.modal.garage_type')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_garage.topic_type')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Garage Type"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_garage.no')</th>
                                <th>@lang('admin_garage.name')</th>
                                <th>@lang('admin_garage.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/garage_type.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_garage_type',route('admin.get_garage_type')) }} 
{{ Form::hidden('hd_get_data_garage_type',route('admin.get_data_garage_type')) }} 
{{ Form::hidden('hd_save_garage_type',route('admin.save_garage_type')) }} 
{{ Form::hidden('hd_delete_garage_type',route('admin.delete_garage_type')) }} 
@endsection
