@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/garage.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_garage.topic')</span>
                    <div class="panel-heading-controls">
                        <a href="{{ route('admin.garage_form') }}" title="@lang('admin_garage.add_garage')"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_garage.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_type_class">@lang('admin_garage.type_class')</label>
                                {{ Form::select('search_type_class',!empty($init['type_class'])?$init['type_class']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.type_class')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type">@lang('admin_garage.type')</label>
                                {{ Form::select('search_type',!empty($init['type'])?$init['type']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.type')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type"></label>
                                {{ Form::select('search_provice',!empty($init['province'])?$init['province']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.province')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type"></label>
                                {{ Form::select('search_district',!empty($init['district'])?$init['district']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.district')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type"></label>
                                {{ Form::select('search_subdistrict',!empty($init['subdistrict'])?$init['subdistrict']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.sub_district')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_name"></label>
                                {{ Form::text('search_name',null, ['class' => 'form-control','placeholder' => trans('admin_garage.name')]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_garage.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_garage.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_garage.no')</th>
                                <th>@lang('admin_garage.type_class')</th>
                                <th>@lang('admin_garage.type')</th>
                                <th>@lang('admin_garage.name')</th>
                                <th>@lang('admin_garage.province')</th>
                                <th>@lang('admin_garage.district')</th>
                                <th>@lang('admin_garage.sub_district')</th>
                                <th>@lang('admin_garage.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/garage.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_garage',route('admin.get_garage')) }} 
{{ Form::hidden('hd_get_data_garage',route('admin.get_data_garage')) }} 
{{ Form::hidden('hd_delete_garage',route('admin.delete_garage')) }} 
{{ Form::hidden('hd_get_data_district',route('admin.get_data_district')) }} 
{{ Form::hidden('hd_get_data_subdistrict',route('admin.get_data_subdistrict')) }} 
@endsection
