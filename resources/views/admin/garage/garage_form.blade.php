@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/garage_form.css') !!}">
@endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_garage.add') <span class="text-muted font-weight-light">@lang('admin_garage.topic')</span></h1>
@else
<h1>@lang('admin_garage.edit') <span class="text-muted font-weight-light">@lang('admin_garage.topic')</span></h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_garage.add')/@lang('admin_garage.edit') @lang('admin_garage.topic')</span>
                </div>
                {{ Form::open(['url' => route("admin.save_garage"),'id'=>'form_edit_garage','name'=>'form_edit_garage','class'=>'form_edit_garage'])}}
                {{ Form::hidden('garage_id',!empty($data['id'])?$data['id']:'') }}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_garage.type_class')</label>
                            {{ Form::select('type_class',!empty($init['type_class'])?$init['type_class']:[],!empty($data['type_class'])?$data['type_class']:null, 
                            ['class' => 'form-control','placeholder' => trans('admin_garage.type_class')]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_garage.type')</label>
                            {{ Form::select('type',!empty($init['type'])?$init['type']:[],!empty($data['type'])?$data['type']:null, 
                            ['class' => 'form-control','placeholder' => trans('admin_garage.type')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_garage.garage_name')</label>
                            {{ Form::text('name',!empty($data['name'])?$data['name']:'', ['class' => 'form-control','placeholder' => trans('admin_garage.garage_name')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-group">
                            <label>@lang('admin_garage.province')</label>
                            {{ Form::select('province',!empty($init['province'])?$init['province']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.province')]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>@lang('admin_garage.district')</label>
                            {{ Form::select('district',!empty($init['district'])?$init['district']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.district')]) }}
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>@lang('admin_garage.sub_district')</label>
                            {{ Form::select('sub_district',!empty($init['sub_district'])?$init['sub_district']:[],null, ['class' => 'form-control','placeholder' => trans('admin_garage.sub_district')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_garage.telephone')</label>
                            {{ Form::text('tel',!empty($data['tel'])?$data['tel']:'', ['class' => 'form-control','placeholder' => '02xxxxxxx,084xxxxxxx']) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_garage.fax')</label>
                            {{ Form::text('fax',!empty($data['fax'])?$data['fax']:'', ['class' => 'form-control','placeholder' => '02xxxxxxx']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_garage.lattitude')</label>
                            {{ Form::text('lat',!empty($data['lat'])?$data['lat']:'', ['class' => 'form-control','placeholder' => 'Google Map Latitude']) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_garage.longitude')</label>
                            {{ Form::text('long',!empty($data['long'])?$data['long']:'', ['class' => 'form-control','placeholder' => 'Google Map Longitude']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_garage.address')</label>
                            {{ Form::textarea('address',!empty($data['address'])?$data['address']:'', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>&nbsp;</label>
                            <label class="switcher switcher-lg switcher-primary">
                                @if(isset($data['status']))
                                <input type="checkbox" name="status" {{ $data['status']==1?'checked':'' }}>
                                @else
                                <input type="checkbox" checked name="status">
                                @endif
                                <div class="switcher-indicator">
                                    <div class="switcher-yes">@lang('common.yes')</div>
                                    <div class="switcher-no">@lang('common.no')</div>
                                </div>                     
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">@lang('admin_garage.submit')</button>
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('admin/js/garage_form.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_data_district',route('admin.get_data_district')) }} 
{{ Form::hidden('hd_get_data_subdistrict',route('admin.get_data_subdistrict')) }} 
{{ Form::hidden('hd_get_data_garage',route('admin.get_data_garage')) }} 
@endsection


