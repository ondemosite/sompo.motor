<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-car p-r-1"></i><span>@lang('admin_garage.add_type')</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_garage_type"),'name'=>'form_garage','class'=>'form_garage','id'=>'form_garage')) }}
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_garage.garage_name')</label>
                    {{ Form::text('name',null, ['class' => 'form-control','placeholder' => 'privilege name...']) }}
                    {{ Form::hidden('id',null) }} 
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">@lang('admin_garage.close')</button>
                <button type="button" class="btn btn-primary" id="submit_button">@lang('admin_garage.save')</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>