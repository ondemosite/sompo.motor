@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/admin.css') !!}">
@endsection

@section('breadcrumb')
@if($data['mode']=="add")
<h1>@lang('admin_profile.create') <span class="text-muted font-weight-light">@lang('admin_profile.topic')</span></h1>
@else
<h1>@lang('admin_profile.profile') <span class="text-muted font-weight-light">@lang('admin_profile.setting')</span></h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header m-b-0 p-b-0 b-b-0">
                <ul class="nav nav-tabs page-block" id="account-tabs">
                    <li class="active"><a href="#account-profile" data-toggle="tab">@lang('admin_profile.topic')</a></li>
                    @if($data['mode']=="edit")
                    <li><a href="#account-password" data-toggle="tab">@lang('admin_profile.password')</a></li>
                    @endif
                </ul>
            </div> <!-- /page-header -->
            <div class="tab-content p-y-4">
                <!-- Profile tab -->
                <div class="tab-pane fade in active" id="account-profile">
                    <div class="row">
                        {{ Form::open(['url' => route("admin.save_profile"), 'files' => true,'id'=>'form_profile',
                        'class'=>'col-md-8 col-lg-9'])}}
                        {{ Form::hidden('profile_id',!empty($data['id'])?$data['id']:'') }}
                        {{ Form::hidden('clear_cover',"false") }}
                        <input name="cover" id="select_file" type="file" style="display:none;">
                            <div class="p-x-1">

                                <fieldset class="form-group form-group-lg">
                                    @if(empty($data['role_disabled']))
                                    <label class="required" for="privilege">@lang('admin_profile.privilege')</label>
                                    {{ Form::select('privilege',$select_privilege,!empty($data['privilege'])?$data['privilege']:'', 
                                    ['class' => 'form-control','placeholder'=> trans('admin_profile.privilege')]) }}
                                    @else
                                    <label class="font-size-18">@lang('admin_profile.privilege'): <span class="font-weight-normal font-size-14">{{ !empty($data['privilege_name'])?$data['privilege_name']:'' }}</span></label>
                                    {{ Form::hidden('privilege',!empty($data['privilege'])?$data['privilege']:'') }}
                                    @endif
                                </fieldset>
                            
                                <fieldset class="form-group form-group-lg">
                                    <label class="required" for="name">@lang('admin_profile.name')</label>
                                    {{ Form::text('name',!empty($data['name'])?$data['name']:'', ['class' => 'form-control','placeholder' => trans('admin_profile.name')]) }}
                                </fieldset>
                                <fieldset class="form-group form-group-lg">
                                    <label class="required" for="lastname">@lang('admin_profile.lastname')</label>
                                    {{ Form::text('lastname',!empty($data['lastname'])?$data['lastname']:'', ['class' => 'form-control','placeholder' => trans('admin_profile.lastname')]) }}
                                </fieldset>
                                <fieldset class="form-group form-group-lg">
                                    <label class="required" for="username">@lang('admin_profile.username')</label>
                                    {{ Form::text('username',!empty($data['username'])?$data['username']:'', ['class' => 'form-control','placeholder' => trans('admin_profile.username')]) }}
                                    <small class="text-muted">@lang('admin_profile.small-y')</small>
                                </fieldset>
                                <fieldset class="form-group form-group-lg">
                                    <label for="email">@lang('admin_profile.email')</label>
                                    {{ Form::email('email',!empty($data['email'])?$data['email']:'', ['class' => 'form-control','placeholder' => 'Email@domain.com']) }}
                                </fieldset>
                                @if($data['mode']=="add")
                                <fieldset class="form-group form-group-lg">
                                    <label class="required" for="password">@lang('admin_profile.password')</label>
                                    {{ Form::password('password', ['class' => 'form-control','id'=>'password']) }}
                                    <small class="text-muted">@lang('admin_profile.mini')</small>
                                </fieldset>
                                <fieldset class="form-group form-group-lg">
                                    <label class="required" for="verify_password">@lang('admin_profile.verify')</label>
                                    {{ Form::password('verify_password', ['class' => 'form-control']) }}
                                </fieldset>
                                @endif
                                
                                <button type="submit" class="btn btn-lg btn-primary m-t-3">@lang('admin_profile.submit')</button>
                                <label class="switcher switcher-lg switcher-primary pull-xs-right">
                                    @if(isset($data['status']))
                                    <input type="checkbox" name="status" {{ $data['status']==1?'checked':'' }}>
                                    @else
                                    <input type="checkbox" checked name="status">
                                    @endif
                                    <div class="switcher-indicator">
                                        <div class="switcher-yes">@lang('common.yes')</div>
                                        <div class="switcher-no">@lang('common.no')</div>
                                    </div>                     
                                    Active
                                </label>
                            </div>
                        {{ Form::close() }}

                        <!-- Spacer -->
                        <div class="m-t-4 visible-xs visible-sm"></div>

                        <!-- Avatar -->
                        <div class="col-md-4 col-lg-3">
                            <div class="panel bg-transparent">
                            <div class="panel-body text-xs-center">
                                @if(!empty($data['cover']))
                                <img id="image-preview" src="{!! asset($data['cover']) !!}" alt="" class="" style="max-width: 100%;">
                                @else
                                <img id="image-preview" src="{!! asset('images/icon-user-default.png') !!}" alt="" class="" style="max-width: 100%;">
                                @endif
                            </div>
                            <hr class="m-y-0">
                            <div class="panel-body text-xs-center">
                                <button type="button" class="btn btn-primary" id="btn-change">@lang('admin_profile.change')</button>&nbsp;
                                <button type="button" class="btn" id="btn-remove"><i class="fa fa-trash"></i></button>
                                <div class="m-t-2 text-muted font-size-12">JPG, GIF or PNG. Max size of 1MB</div>
                            </div>
                            </div>
                        </div>
                        
                    </div> <!--/row -->
                </div> <!-- /account-profile -->
                <!-- / Profile tab -->

                @if($data['mode']=="edit")
                <!-- Password tab -->
                <div class="tab-pane fade" id="account-password">
                    {{ Form::open(['url' => route("admin.edit_password"), 'files' => true,'id'=>'form_edit_password','class'=>'p-x-1'])}}
                    {{ Form::hidden('profile_id',!empty($data['id'])?$data['id']:'') }}
                    <fieldset class="form-group form-group-lg">
                        <label for="old_password">@lang('admin_profile.old_password')</label>
                        {{ Form::password('old_password', ['class' => 'form-control']) }}
                    </fieldset>
                    <fieldset class="form-group form-group-lg">
                        <label for="new-password">@lang('admin_profile.new_password')</label>
                        {{ Form::password('new_password', ['class' => 'form-control','id'=>'new_password']) }}
                        <small class="text-muted">@lang('admin_profile.mini')</small>
                    </fieldset>
                    <fieldset class="form-group form-group-lg">
                        <label for="verify_password">@lang('admin_profile.verify')</label>
                        {{ Form::password('verify_password', ['class' => 'form-control']) }}
                    </fieldset>
                    <button type="submit" class="btn btn-lg btn-primary m-t-3">@lang('admin_profile.change_password')</button>
                    {{ Form::close() }}
                </div>
                <!-- / Password tab -->
                @endif

            </div>
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('component/custom-upload/custom-upload.js') !!}"></script>
<script src="{!! asset('admin/js/profile.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_admin_list',route('admin.get_admin_list')) }} 
@endsection

