@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/admin.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-user"></i>@lang('admin_admin.topic')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="{{ route('admin.get_profile') }}" title="@lang('admin_admin.add')"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_admin.no')</th>
                                <th>@lang('admin_admin.name')</th>
                                <th>@lang('admin_admin.username')</th>
                                <th>@lang('admin_admin.email')</th>
                                <th>@lang('admin_admin.privilege')</th>
                                <th>@lang('admin_admin.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/admin.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_admin_list',route('admin.get_admin_list')) }} 
{{ Form::hidden('hd_delete_admin',route('admin.delete_admin')) }} 
@endsection
