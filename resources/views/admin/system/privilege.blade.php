@extends('admin.layout.default')
@section('title') Privilege @endsection
@section('css')
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.system.modal.privilege')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-group"></i>@lang('admin_privilege.topic')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="@lang('admin_privilege.add')"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_privilege.no')</th>
                                <th>@lang('admin_privilege.name')</th>
                                <th>@lang('admin_privilege.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>   
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/privilege.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_privilege',route('admin.get_privilege_list')) }} 
{{ Form::hidden('hd_save_privilege',route('admin.save_privilege')) }} 
{{ Form::hidden('hd_get_data_privilege',route('admin.get_data_privilege')) }} 
{{ Form::hidden('hd_delete_privilege',route('admin.delete_privilege')) }} 
@endsection
