<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-group p-r-1"></i>@lang('admin_privilege.add')</h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_privilege"),'name'=>'form_privilege','class'=>'form_privilege','id'=>'form_privilege')) }}
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_privilege.privilege')</label>
                    {{ Form::text('name',null, ['class' => 'form-control','placeholder' => 'privilege name...']) }}
                    {{ Form::hidden('id',null) }} 
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">@lang('admin_privilege.close')</button>
                <button type="button" class="btn btn-primary" id="submit_button">@lang('admin_privilege.save')</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>