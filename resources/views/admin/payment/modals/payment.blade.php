<div class="modal fade" id="modal-payment" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-"></i>@lang('admin_payment2.payment_data')</h4>
            </div>
            <div class="modal-body">
                <div class="modal-topic"><b>@lang('admin_payment2.payment_no') :</b> <span id="payment_no"></span>
                    <span class="pull-right"><b>@lang('admin_payment2.created_date') :</b> <span id="created_date"></span></span>
                </div>
                <table width="100%" class="table table-striped">
                    <tbody>
                        <tr>
                            <td>@lang('admin_payment2.order_number')</td>
                            <td id="field_rc_order"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.policy_number')</td>
                            <td id="field_rc_policy"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.merchant_id')</td>
                            <td id="field_rc_mc"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.currency_code')</td>
                            <td id="field_rc_cc"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.description')</td>
                            <td id="field_rc_dc"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.amount')</td>
                            <td id="field_rc_am"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.status')</td>
                            <td id="field_rc_st"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.status_detail')</td>
                            <td id="field_rc_stt"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.paid_date')</td>
                            <td id="field_rc_pd"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.updated_at')</td>
                            <td id="field_rc_up"></td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
    </div>