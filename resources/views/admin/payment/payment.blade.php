@extends('admin.layout.default')
@section('title') Payment @endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-credit-card-alt"></i>@lang('admin_payment2.topic')</span>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_payment2.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_payment_number">@lang('admin_payment2.payment_no')</label>
                                {{ Form::text('search_payment_number',null, ['class' => 'form-control','placeholder' => trans('admin_payment2.payment_no')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_order_number">@lang('admin_order.order_number')</label>
                                {{ Form::text('search_order_number',null, ['class' => 'form-control','placeholder' => trans('admin_order.order_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_policy_number">@lang('admin_payment2.policy_number')</label>
                                {{ Form::text('search_policy_number',null, ['class' => 'form-control','placeholder' => trans('admin_payment2.policy_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_payment2.paid_date_from')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_payment2.paid_date_from')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_payment2.paid_date_to')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_payment2.paid_date_to')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_status">@lang('admin_order.status')</label>
                                {{ Form::select('search_status',["PROCESSING"=>'PROCESSING',"FAILED"=>'FAILED','CANCELED'=>'CANCELED','PAID'=>'PAID'],null, ['class' => 'form-control','placeholder' => trans('admin_order.status')]) }}
                            </div>

                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_order.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_order.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_payment2.no')</th>
                                <th>@lang('admin_payment2.payment_no')</th>
                                <th>@lang('admin_payment2.order_number')</th>
                                <th>@lang('admin_payment2.policy_number')</th>
                                <th>@lang('admin_payment2.amount')</th>
                                <th>@lang('admin_payment2.status')</th>
                                <th>@lang('admin_payment2.paid_date')</th>
                                <th>@lang('admin_payment2.created_date')</th>
                                <th>@lang('admin_payment2.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/payment.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_link_policy',route('admin.get_policy_data')) }} 
{{ Form::hidden('hd_link_order',route('admin.get_order_data')) }} 
{{ Form::hidden('hd_list',route('admin.get_payment_list')) }} 
{{ Form::hidden('hd_data',route('admin.get_payment_data')) }} 
@include('admin.payment.modals.payment') 
@endsection
