@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/banner_form.css') !!}">
@endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_banner.add') <span class="text-muted font-weight-light">@lang('admin_banner.topic')</span></h1>
@else
<h1>@lang('admin_banner.edit') <span class="text-muted font-weight-light">@lang('admin_banner.topic')</span></h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-picture-o"></i>@lang('admin_banner.add')/@lang('admin_banner.edit') @lang('admin_banner.topic')</span>
                    <span class="pull-right"><strong>@lang('admin_banner.view')</strong> : {{ !empty($data['view'])?$data['view']:'' }}</span>
                    <div style="clear:both;"></div>
                </div>
                {{ Form::open(['url' => route("admin.save_banner"), 'files' => true,'id'=>'form_banner'])}}
                {{ Form::hidden('banner_id',!empty($data['id'])?$data['id']:'') }}
                {{ Form::hidden('clear_cover_en',"false") }}
                {{ Form::hidden('clear_cover_th',"false") }}
                {{ Form::hidden('clear_cover_en_mobile',"false") }}
                {{ Form::hidden('clear_cover_th_mobile',"false") }}
                <input name="cover_en" class="select_file" type="file" style="display:none;" />
                <input name="cover_th" class="select_file" type="file" style="display:none;" />
                <input name="cover_en_mobile" class="select_file" type="file" style="display:none;" />
                <input name="cover_th_mobile" class="select_file" type="file" style="display:none;" />
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 box-image-preview">
                            <label class="required">EN Image <br/><span class="m-t-2 text-muted font-size-12">Fixed Size 1920x500 : JPG, GIF ,PNG  </span></label>
                            <div><img class="image-preview" data-belong="cover_en" src="{!! asset(!empty($data['cover_en'])?$data['cover_en']:'/images/admin/background/preview-banner_cover_en.jpg') !!}"></div>
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-change" data-target="cover_en">@lang('admin_banner.change')</button>&nbsp;
                            <button type="button" class="btn btn-remove" data-target="cover_en"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12 box-image-preview">
                            <label class="required">TH Image <br/><span class="m-t-2 text-muted font-size-12">Fixed Size 1920x500 : JPG, GIF ,PNG  </span></label>
                            <div><img class="image-preview" data-belong="cover_th" src="{!! asset(!empty($data['cover_th'])?$data['cover_th']:'/images/admin/background/preview-banner_cover_th.jpg') !!}"></div>
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-change" data-target="cover_th">@lang('admin_banner.change')</button>&nbsp;
                            <button type="button" class="btn btn-remove" data-target="cover_th"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12 box-image-preview">
                            <label class="required">EN Image For Mobile <br/><span class="m-t-2 text-muted font-size-12">Fixed Size 770x500 : JPG, GIF ,PNG  </span></label>
                            <div><img class="image-preview" data-belong="cover_en_mobile" src="{!! asset(!empty($data['cover_en_mobile'])?$data['cover_en_mobile']:'/images/admin/background/preview-banner_cover_en_mobile.jpg') !!}"></div>
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-change" data-target="cover_en_mobile">@lang('admin_banner.change')</button>&nbsp;
                            <button type="button" class="btn btn-remove" data-target="cover_en_mobile"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12 box-image-preview">
                            <label class="required">TH Image For Mobile <br/><span class="m-t-2 text-muted font-size-12">Fixed Size 770x500 : JPG, GIF ,PNG  </span></label>
                            <div><img class="image-preview" data-belong="cover_th_mobile" src="{!! asset(!empty($data['cover_th_mobile'])?$data['cover_th_mobile']:'/images/admin/background/preview-banner_cover_th_mobile.jpg') !!}"></div>
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-change" data-target="cover_th_mobile">@lang('admin_banner.change')</button>&nbsp;
                            <button type="button" class="btn btn-remove" data-target="cover_th_mobile"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label class="required">@lang('admin_banner.title')</label>
                            {{ Form::text('title',!empty($data['title'])?$data['title']:'', ['class' => 'form-control','placeholder' => trans('admin_banner.title')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_banner.link')</label>
                            <div>
                                <label class="radio-inline">
                                    @php
                                        $type = "";
                                        if(!empty($data['sync_type'])){
                                            $type = $data['sync_type'];
                                        }
                                    @endphp
                                    <input type="radio" class="sync_type" name="sync_type" value="promotion" {{ ($type=="PROMOTION"?"checked":"") }} > โปรโมชั่น
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" class="sync_type" name="sync_type" value="link" {{ ($type=="LINK"?"checked":"") }} > Link
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row box-link box-append {{ ($type=="LINK"?"active":"") }}">
                        <div class="col-sm-6 form-group">
                            {{ Form::text('link',!empty($data['link'])?$data['link']:'', ['class' => 'form-control','placeholder' => 'Full Path Ex:www.google.com']) }}
                        </div>
                    </div>
                    <div class="row box-promotion box-append {{ ($type=="PROMOTION"?"active":"") }}">
                        <div class="col-sm-6 form-group">
                            {{ Form::select('promotion_id',$init['promotions'],!empty($data['promotion_id'])?$data['promotion_id']:'', ['class' => 'form-control','placeholder' => 'Select Promotion']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_banner.description')</label>
                            {{ Form::textarea('description',!empty($data['description'])?$data['description']:'', ['class' => 'form-control','placeholder' => trans('admin_banner.description')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_banner.order')</label>
                            {{ Form::number('orders',!empty($data['orders'])?$data['orders']:'', ['class' => 'form-control','placeholder' => trans('admin_banner.order')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>&nbsp;</label>
                            <label class="switcher switcher-lg switcher-primary">
                                @if(isset($data['status']))
                                <input type="checkbox" name="status" {{ $data['status']==1?'checked':'' }}>
                                @else
                                <input type="checkbox" checked name="status">
                                @endif
                                <div class="switcher-indicator">
                                    <div class="switcher-yes">@lang('common.yes')</div>
                                    <div class="switcher-no">@lang('common.no')</div>
                                </div>                     
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">@lang('admin_banner.submit')</button>
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('component/custom-upload/custom-upload.js') !!}"></script>
<script src="{!! asset('admin/js/banner_form.js') !!}"></script>
@endsection



