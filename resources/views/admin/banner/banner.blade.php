@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/banner.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-picture-o"></i>@lang('admin_banner.topic')</span>
                    <div class="panel-heading-controls">
                        <a href="{{ route('admin.banner_form') }}" title="Add Banner"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_banner.no')</th>
                                <th>@lang('admin_banner.topic')</th>
                                <th>@lang('admin_banner.title')</th>
                                <th>@lang('admin_banner.description')</th>
                                <th>@lang('admin_banner.link')</th>
                                <th>@lang('admin_banner.view')</th>
                                <th>@lang('admin_banner.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/banner.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_asset_url',asset('')) }} 
{{ Form::hidden('hd_get_banner_list',route('admin.get_banner_list')) }} 
{{ Form::hidden('hd_save_banner',route('admin.save_banner')) }} 
{{ Form::hidden('hd_delete_banner',route('admin.delete_banner')) }} 
@endsection
