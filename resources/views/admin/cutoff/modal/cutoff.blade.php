<div class="modal fade" id="modal-cutoff" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-calendar p-r-1"></i><span>Cutoff</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_cutoff"),'name'=>'form_cutoff','class'=>'form_cutoff','id'=>'form_cutoff')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_cutoff.years')</label>
                    {{ Form::select('years',!empty($init['years'])?$init['years']:[],null, ['class' => 'form-control','placeholder' => trans('admin_cutoff.years')]) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_cutoff.months')</label>
                    {{ Form::select('months',!empty($init['months'])?$init['months']:[],null, ['class' => 'form-control','placeholder' => trans('admin_cutoff.months')]) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="cutoff_date">@lang('admin_cutoff.cutoff')</label>
                    {{ Form::text('cutoff_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_cutoff.cutoff'),'autocomplete'=>"off","disabled"=>"disabled"]) }}
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">@lang('common.close')</button>
                <button type="button" class="btn btn-primary" id="submit_button">@lang('common.save')</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>