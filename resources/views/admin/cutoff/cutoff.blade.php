@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')

@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.cutoff.modal.cutoff')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> 
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-calendar"></i>@lang('admin_cutoff.topic')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-cutoff" title="Add Faq"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_order.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_order_number">@lang('admin_cutoff.years')</label>
                                {{ Form::text('search_years',null, ['class' => 'form-control','placeholder' => trans('admin_cutoff.years') ]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_driver_name">@lang('admin_cutoff.months')</label>
                                {{ Form::text('search_months',null, ['class' => 'form-control','placeholder' => trans('admin_cutoff.months') ]) }}
                            </div>                    
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_order.search')</button>
                            <button id="clear" type="reset" class="btn"><i class="fa fa-refresh"></i> @lang('admin_order.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_cutoff.no')</th>
                                <th>@lang('admin_cutoff.years')</th>
                                <th>@lang('admin_cutoff.months')</th>
                                <th>@lang('admin_cutoff.cutoff')</th>
                                <th>@lang('admin_cutoff.last_update')</th>
                                <th>@lang('admin_cutoff.updated_by')</th>
                                <th>@lang('admin_cutoff.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('component/moment/moment.js') !!}"></script>
<script src="{!! asset('admin/js/cutoff.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_list',route('admin.cutoff_list')) }} 
{{ Form::hidden('hd_get',route('admin.get_cutoff')) }} 
{{ Form::hidden('hd_save',route('admin.save_cutoff')) }} 
{{ Form::hidden('hd_delete',route('admin.delete_cutoff')) }} 
@endsection
