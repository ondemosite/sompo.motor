<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Sompo Backoffice - @yield('title')</title>
  
  <!-- External css -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- Local css -->
  <link href="{!! asset('theme/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css">
  <link href="{!! asset('theme/css/pixeladmin.min.css') !!}" rel="stylesheet" type="text/css">
  <link href="{!! asset('theme/css/widgets.min.css') !!}" rel="stylesheet" type="text/css">
  <link href="{!! asset('theme/css/themes/candy-red.min.css') !!}" rel="stylesheet" type="text/css">
  <link href="{!! asset('admin/css/app.css') !!}" rel="stylesheet" type="text/css">
  @yield('css')
  <!-- Pace.js -->
  <script src="{!! asset('theme/pace/pace.min.js') !!}" type="text/javascript"></script>
</head>
<body>
  <!-- Nav -->
  <nav class="px-nav px-nav-left">
    <button type="button" class="px-nav-toggle" data-toggle="px-nav">
      <span class="px-nav-toggle-arrow"></span>
      <span class="navbar-toggle-icon"></span>
      <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
    </button>

    <ul class="px-nav-content">
      <!-- Generate Menu -->
      {!! Menu::genMenu(); !!}
      <!--
      <li class="px-nav-item px-nav-dropdown">
        <a href=""><i class="px-nav-icon fa fa-cog"></i><span class="px-nav-label">System</span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="{!! route('admin.admin_list') !!}"><span class="px-nav-label">Admins</span></a></li>
          <li class="px-nav-item"><a href="{!! route('admin.privilege') !!}"><span class="px-nav-label">Privilege</span></a></li>
          <li class="px-nav-item"><a href="{!! route('admin.menu') !!}"><span class="px-nav-label">Menu & Permission</span></a></li>
        </ul>
      </li> -->


    </ul>
  </nav>

  <!-- Navbar -->
  <nav class="navbar px-navbar">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
    </div>

    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

    <div class="collapse navbar-collapse" id="px-navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="{{ !empty(Auth::guard('admin')->user()->cover)?asset(Auth::guard('admin')->user()->cover):asset('/images/icon-user-default.png') }}" alt="user" class="px-navbar-image" />
            <span class="hidden-md">{{ !empty(Auth::guard('admin')->user()->name)?Auth::guard('admin')->user()->name.' '.Auth::guard('admin')->user()->lastname:'' }}</span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('admin.get_profile').'/me' }}"><span class="label label-warning pull-xs-right"><i class=""></i></span>Profile</a></li>
            <li class="divider"></li>
            <li><a href="{{ route('admin.logout') }}"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <div class="switch-language-box">
            @if(App::isLocale('en'))
            <a class="language-toggle" data-target="language-data"><img src="<?=asset('/images/admin/icon/lang_uk.png')?>" width="25" /></a>
            <div class="language-data">
              <a href="<?=url("language_switch/th")?>"><img src="<?=asset('/images/admin/icon/lang_th.png')?>" width="25" /></a>
            </div>
            @else
            <a class="language-toggle" data-target="language-data"><img src="<?=asset('/images/admin/icon/lang_th.png')?>" width="25" /></a>
            <div class="language-data">
              <a href="<?=url("language_switch/en")?>"><img src="<?=asset('/images/admin/icon/lang_uk.png')?>" width="25" /></a>
            </div>
            @endif
          </div>
        </li>
      </ul>
    </div>
  </nav>

  <!-- Content -->
  <div class="px-content">
        <div class="px-alert m-b-2"></div>
        @if ($errors->any())
          <div class="alert alert-danger alert-dark">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        @if (session('status'))
          <div class="alert alert-success alert-dark">
              {{ session('status') }}
          </div>
        @endif
        <div class="page-header">
            @yield('breadcrumb')
        </div>
        @yield('content')
        @yield('hidden')
  </div>

  <!-- Footer -->
  <footer class="px-footer px-footer-bottom">
    Copyright © 2017 Sompo Insurance (Thailand) Public Company Limited. All Rights Reserved.
  </footer>

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="{!! asset('theme/js/jquery.min.js') !!}" type="text/javascript"></script>

  <!-- Core scripts -->
  <script src="{!! asset('theme/js/bootstrap.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('theme/js/pixeladmin.min.js') !!}" type="text/javascript"></script>

  <!-- Your scripts -->
  <script src="{!! asset('theme/js/app.js') !!}" type="text/javascript"></script>
  @yield('javascript')
</body>
</html>
