@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
@endsection

@section('breadcrumb')
<h1>@lang('admin_tax.edit') <span class="text-muted font-weight-light">@lang('admin_tax.tax')</span></h1>
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon"></i>@lang('admin_tax.topic')</span>
                </div>
                {{ Form::open(['url' => route("admin.save_tax"),'id'=>'form_tax','name'=>'form_tax','class'=>'form_tax'])}}
                {{ Form::hidden('garage_id',!empty($data['id'])?$data['id']:'') }}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_tax.stamp')</label>
                            <div class="input-group">
                                {{ Form::number('stamp',!empty($init['tax']["stamp"])?$init['tax']["stamp"]:0, ['class' => 'form-control input-money','placeholder' => trans('admin_tax.stamp').' (%)']) }}
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_tax.vat')</label>
                            <div class="input-group">
                                {{ Form::number('vat',!empty($init['tax']['vat'])?$init['tax']['vat']:0, ['class' => 'form-control input-money','placeholder' => trans('admin_tax.vat').' (%)']) }}
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_tax.medical_free')</label>
                            <div class="input-group">
                                {{ Form::number('compulsoty_medical_fee',!empty($init['tax']["compulsoty_medical_fee"])?$init['tax']["compulsoty_medical_fee"]:0, ['class' => 'form-control input-money','placeholder' => trans('admin_tax.medical_free')]) }}
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_tax.disable')</label>
                            <div class="input-group">
                                {{ Form::number('compulsory_death_disable',!empty($init['tax']['compulsory_death_disable'])?$init['tax']['compulsory_death_disable']:0, ['class' => 'form-control input-money','placeholder' => trans('admin_tax.disable')]) }}
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">@lang('admin_tax.submit')</button>
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('admin/js/tax.js') !!}"></script>
@endsection

@section('hidden')

@endsection


