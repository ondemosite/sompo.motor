@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/faq.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.faq.modal.faq')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_faq.topic')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Faq"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_faq.no')</th>
                                <th>@lang('admin_faq.question')</th>
                                <th>@lang('admin_faq.status')</th>
                                <th>@lang('admin_faq.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/faq.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_faq_list',route('admin.get_faq_list')) }} 
{{ Form::hidden('hd_get_data_faq',route('admin.get_data_faq')) }} 
{{ Form::hidden('hd_save_faq',route('admin.save_faq')) }} 
{{ Form::hidden('hd_delete_faq',route('admin.delete_faq')) }} 
{{ Form::hidden('hd_set_status',route('admin.set_status_faq')) }} 
@endsection
