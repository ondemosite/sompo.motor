<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-car p-r-1"></i><span>Add Faq</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_faq"),'name'=>'form_faq','class'=>'form_faq','id'=>'form_faq')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label required" for="required-input">Question</label>
                    {{ Form::text('question',null, ['class' => 'form-control','placeholder' => 'Question']) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">Question</label>
                    {{ Form::textarea('answer',null, ['class' => 'form-control summernote','placeholder' => 'Answer...']) }}
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_button">Save changes</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>