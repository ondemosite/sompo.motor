@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('admin/css/promotion_form.css') }}">
@endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_promotion.add') <span class="text-muted font-weight-light">@lang('admin_promotion.topic')</span></h1>
@else
<h1>@lang('admin_promotion.edit') <span class="text-muted font-weight-light">@lang('admin_promotion.topic')</span></h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-bookmark"></i>@lang('admin_promotion.add')/@lang('admin_promotion.edit') @lang('admin_promotion.topic')</span>
                </div>
                {{ Form::open(['url' => route("admin.save_promotion"),'id'=>'form_promotion','name'=>'form_promotion','class'=>'form_promotion'])}}
                {{ Form::hidden('promotion_id',!empty($data['id'])?$data['id']:'') }}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_promotion.promotion_title')</label>
                            {{ Form::text('title',!empty($data['title'])?$data['title']:'', ['class' => 'form-control','placeholder' => trans('admin_promotion.promotion_title')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_promotion.promotion_code')</label>
                            {{ Form::text('code',!empty($data['code'])?$data['code']:'', ['class' => 'form-control','placeholder' => trans('admin_promotion.promotion_code')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_promotion.discount')</label>
                            <div class="input-group">
                                {{ Form::number('discount',!empty($data['discount'])?$data['discount']:'', ['class' => 'form-control','placeholder' => trans('admin_promotion.discount')]) }}
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_promotion.start_date')</label>
                            {{ Form::text('start_at',!empty($data['start_at'])?$data['start_at']:'', ['class' => 'form-control datepicker','placeholder' => trans('admin_promotion.start_date')]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_promotion.expire_date')</label>
                            {{ Form::text('expire_at',!empty($data['expire_at'])?$data['expire_at']:'', ['class' => 'form-control datepicker','placeholder' => trans('admin_promotion.expire_date')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-4 form-group">
                            <label>จำนวนคูปองสูงสุด</label>
                            {{ Form::number('maximum_grant',!empty($data['maximum_grant'])?$data['maximum_grant']:'', ['class' => 'form-control','placeholder' => trans('admin_promotion.maximum_grant')]) }}
                            <label class="coupon-alert hide">*จำนวนคูปองสูงสุดต้องไม่น้อยกว่าจำนวนที่ถูกใช้</label>
                        </div>
                        <div class="col-md-2 col-sm-4 form-group">
                            <label>จำนวนที่ถูกใช้</label>
                            {{ Form::number('used_coupon',!empty($data['receive_amount'])?$data['receive_amount']:0, ['class' => 'form-control','disabled' => 'disabled']) }}
                        </div>
                        <div class="col-md-2 col-sm-4 form-group">
                            <label>จำนวนคงเหลือ</label>
                            {{ Form::number('remaining','', ['class' => 'form-control','disabled' => 'disabled']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_promotion.active_plan')</label>
                            <select class="form-control select2" name="coverage_id[]" multiple="multiple">
                                @foreach($init['coverage'] as $item)
                                    @if(in_array($item['id'],$data['coverage_id']))
                                    <option value="{{ $item['id'] }}" selected>{{ $item['plan'] }}</option>
                                    @else
                                    <option value="{{ $item['id'] }}">{{ $item['plan'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_promotion.vehicle_brand')</label>
                            <select class="form-control select2" name="vehicle_brand[]" multiple="multiple">
                                @foreach($init['vehicle_brand'] as $item)
                                    @if(!empty($data['vehicle_brand']))
                                        @if(in_array($item['id'],$data['vehicle_brand']))
                                        <option value="{{ $item['id'] }}" selected>{{ $item['name'] }}</option>
                                        @else
                                        <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_promotion.vehicle_model')</label>
                             <select class="form-control select2" name="vehicle_model[]" multiple="multiple">
                                @foreach($init['vehicle_model'] as $item)
                                    @if(!empty($data['vehicle_model']))
                                        @if(in_array($item['id'],$data['vehicle_model']))
                                        <option value="{{ $item['id'] }}" selected>{{ $item['name'] }}</option>
                                        @else
                                        <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_promotion.ending_message')</label>
                            {{ Form::text('ending_message',!empty($data['ending_message'])?$data['ending_message']:'', ['class' => 'form-control','placeholder' => trans('admin_promotion.ending_message')]) }}
                        </div>
                    </div>

                   
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>&nbsp;</label>
                            <label class="switcher switcher-lg switcher-primary" style="width: 200px;">
                                @if(isset($data['status']))
                                <input type="checkbox" name="status" {{ $data['status']==1?'checked':'' }}>
                                @else
                                <input type="checkbox" checked name="status">
                                @endif
                                <div class="switcher-indicator">
                                    <div class="switcher-yes">@lang('common.yes')</div>
                                    <div class="switcher-no">@lang('common.no')</div>
                                </div>  
                                Active Status              
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">@lang('admin_promotion.submit')</button>
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->

    @if(!empty($data['id']))
    <!-- Register List -->
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-user"></i>@lang('admin_promotion.booking_register')</span>
                    <div class="pull-right" id="com-register">
                        <label for="switcher-rounded" class="switcher switcher-rounded switcher-danger switcher-lg">
                            <input type="checkbox" id="switcher-rounded" name="is_register" {{ $data['is_register']==1?'checked':'' }}>
                            <div class="switcher-indicator">
                                <div class="switcher-yes">@lang('common.open')</div>
                                <div class="switcher-no">@lang('common.close')</div>
                            </div>
                        </label>
                    </div>
                </div><!-- panel-heading -->
                {{ Form::open(['url' => route("admin.save_promotion"),'id'=>'form_register','name'=>'form_register','class'=>'form_register','files' => true])}}
                <div class="panel-body register-body {{ $data['is_register']==1?'active':'' }}">
                    <div class="row">
                        <div class="col-md-6 col-sm-4 form-group">
                            <label>จำนวนสิทธิ์สูงสุด</label>
                            {{ Form::number('max_register',!empty($data['max_register'])?$data['max_register']:'', ['class' => 'form-control','placeholder' =>trans('admin_promotion.register_amount') ]) }}
                            <label class="register-alert hide">*จำนวนสิทธิ์สูงสุดต้องไม่น้อยกว่าจำนวนที่ลงทะเบียนไปแล้ว</label>
                        </div>
                        <div class="col-md-2 col-sm-4 form-group">
                            <label>จำนวนที่ลงทะเบียนไปแล้ว</label>
                            {{ Form::number('used_register',!empty($data['register_amount'])?$data['register_amount']:0, ['class' => 'form-control','disabled' => 'disabled']) }}
                        </div>
                        <div class="col-md-2 col-sm-4 form-group">
                            <label>จำนวนคงเหลือ</label>
                            {{ Form::number('remaining_register',!empty($data['remaining_register'])?$data['remaining_register']:'', ['class' => 'form-control','disabled' => 'disabled']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_promotion.start_date')</label>
                            {{ Form::text('register_start_at',!empty($data['register_start_at'])?$data['register_start_at']:'', ['class' => 'form-control datepicker','placeholder' => trans('admin_promotion.start_date')]) }}
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>@lang('admin_promotion.expire_date')</label>
                            {{ Form::text('register_expire_at',!empty($data['register_expire_at'])?$data['register_expire_at']:'', ['class' => 'form-control datepicker','placeholder' => trans('admin_promotion.expire_date')]) }}
                        </div>
                    </div>
                    <div class="row">
                        {{ Form::hidden('clear_cover',"false") }}
                        <input name="cover_register" class="select_file" type="file" style="display:none;" />
                        <div class="col-xs-12 box-image-preview">
                            <label class="required">Cover <br/><span class="m-t-2 text-muted font-size-12">Fixed Size 1280x1037 : JPG, GIF ,PNG  </span></label>
                            <div><img class="image-preview" data-belong="cover_register" src="{!! asset(!empty($data['cover_register'])?$data['cover_register']:'/images/admin/background/preview-landing.jpg') !!}"></div>
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-change" data-target="cover_register">@lang('admin_banner.change')</button>&nbsp;
                            <button type="button" class="btn btn-remove" data-target="cover_register"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    
                    <div class="pull-right">
                        <button type="button" id="submit_register" class="btn btn-primary">@lang('admin_promotion.submit')</button>
                    </div>
                    <div style="clear:both"></div>
                    <div>
                        <hr class="panel-wide-block">
                    </div>
                    
                    <div class="row box-list">
                        <div class="col-sm-12 form-group">
                            <h5 class="m-t-0 m-b-2 font-weight-semibold text-muted">@lang('admin_promotion.register_list')</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-light">
                                <table class="table table-striped table-bordered" id="datatables">
                                    <thead>
                                    <tr>
                                        <th>@lang('admin_promotion.no')</th>
                                        <th>@lang('admin_promotion.regis_name')</th>
                                        <th>@lang('admin_promotion.regis_email')</th>
                                        <th>@lang('admin_promotion.regis_tel')</th>
                                        <th>@lang('admin_promotion.created_at')</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div> <!-- /table-light -->    
                        </div>
                    </div>
                </div><!-- register-body -->
                {{ Form::close() }}
            </div>
        </div>
    </div>
    @endif


</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('component/custom-upload/custom-upload.js') !!}"></script>
<script src="{!! asset('admin/js/promotion_form.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_data_district',route('admin.get_data_district')) }} 
{{ Form::hidden('hd_get_data_subdistrict',route('admin.get_data_subdistrict')) }} 
{{ Form::hidden('hd_get_data_garage',route('admin.get_data_garage')) }} 
{{ Form::hidden('hd_set_register_status',route('admin.set_promotion_register')) }} 
{{ Form::hidden('hd_save_register_status',route('admin.save_promotion_register')) }} 
{{ Form::hidden('hd_get_register_list',route('admin.get_promotion_register_list')) }} 
{{ Form::hidden('hd_get_vehicle_model',route('admin.promotion_get_vehicle_model')) }} 
{{ Form::hidden('hd_get_vehicle_brand',route('admin.promotion_get_vehicle_brand')) }} 
{{ Form::hidden('hd_lang_model',trans('admin_promotion.select_vehicle_model')) }} 
{{ Form::hidden('hd_lang_brand',trans('admin_promotion.select_vehicle_brand')) }} 
@endsection


