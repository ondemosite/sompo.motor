@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/promotion.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-bookmark"></i>@lang('admin_promotion.topic')</span>
                    <div class="panel-heading-controls">
                        <a href="{{ route('admin.promotion_form') }}" title="Add Promotion"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_promotion.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_title">ชื่อโปรโมชั่น</label>
                                {{ Form::text('search_title',null, ['class' => 'form-control','placeholder' => 'ชื่อโปรโมชั่น']) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_title">โปรโมชั่นโค๊ด</label>
                                {{ Form::text('search_code',null, ['class' => 'form-control','placeholder' => 'โปรโมชั่นโค๊ด']) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_title">มูลค่าส่วนลด</label>
                                <div class="input-group">
                                    {{ Form::number('search_discount',null, ['class' => 'form-control','placeholder' => 'มูลค่าส่วนลด']) }}
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type_class">@lang('admin_promotion.start_date')</label>
                                {{ Form::text('search_start_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_promotion.start_date')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type_class">@lang('admin_promotion.expire_date')</label>
                                {{ Form::text('search_expire_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_promotion.expire_date')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type">@lang('admin_promotion.status')</label>
                                {{ Form::select('search_status',["1"=>'ACTIVE',"0"=>'INACTIVE'],null, ['class' => 'form-control','placeholder' => trans('admin_promotion.status')]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_promotion.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_promotion.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_promotion.no')</th>
                                <th>ชื่อโปรโมชั่น</th>
                                <th>โปรโมชั่นโค๊ด</th>
                                <th>ประเภทการลดราคา</th>
                                <th>มูลค่าส่วนลด</th>
                                <th>จำนวนคูปองสูงสุด</th>
                                <th>จำนวนคูปองคงเหลือ</th>
                                <th>@lang('admin_promotion.start_date')</th>
                                <th>@lang('admin_promotion.expire_date')</th>
                                <th>@lang('admin_promotion.status')</th>
                                <th>@lang('admin_promotion.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/promotion.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_promotion_list',route('admin.get_promotion_list')) }} 
{{ Form::hidden('hd_delete_promotion',route('admin.delete_promotion')) }} 
@endsection
