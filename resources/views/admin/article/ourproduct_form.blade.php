@extends('admin.layout.default')
@section('title') Admins @endsection

@section('breadcrumb')
@if(empty($data['id']))
<h1>@lang('admin_article.add') <span class="text-muted font-weight-light">@lang('admin_article.our_product')</span></h1>
@else
<h1>@lang('admin_article.edit') <span class="text-muted font-weight-light">@lang('admin_article.our_product')</span></h1>
@endif
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-picture-o"></i>@lang('admin_article.add')/@lang('admin_article.edit') @lang('admin_article.article')</span>
                    <span class="pull-right"><strong>@lang('admin_article.view')</strong> : {{ !empty($data['view'])?$data['view']:'' }}</span>
                    <div style="clear:both;"></div>
                </div>
                {{ Form::open(['url' => route("admin.ourproduct_save"), 'files' => true,'id'=>'form_article'])}}
                {{ Form::hidden('article_id',!empty($data['id'])?$data['id']:'') }}
                {{ Form::hidden('clear_cover_th',"false") }}
                {{ Form::hidden('clear_cover_en',"false") }}
                <input name="cover_path_th" id="select_file_th" type="file" style="display:none;" />
                <input name="cover_path_en" id="select_file_en" type="file" style="display:none;" />
                <div class="panel-body">
                <div class="row">
                        <div class="col-xs-12">
                            <label>@lang('admin_article.cover_th')</label>
                        </div>
                        <div class="col-xs-12 box-image-preview">
                            <img id="image-preview-th" src="{!! asset(!empty($data['cover_path_th'])?$data['cover_path_th']:'/images/admin/background/preview-whysompo.jpg') !!}" alt="" class="">
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary" id="btn-change-th">@lang('admin_article.change')</button>&nbsp;
                            <button type="button" class="btn" id="btn-remove-th"><i class="fa fa-trash"></i></button>
                            <div class="m-t-2 text-muted font-size-12">JPG, GIF or PNG : Not Fixed Size (1:1)</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label>@lang('admin_article.cover_en')</label>
                        </div>
                        <div class="col-xs-12 box-image-preview">
                            <img id="image-preview-en" src="{!! asset(!empty($data['cover_path_en'])?$data['cover_path_en']:'/images/admin/background/preview-whysompo.jpg') !!}" alt="" class="">
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary" id="btn-change-en">@lang('admin_article.change')</button>&nbsp;
                            <button type="button" class="btn" id="btn-remove-en"><i class="fa fa-trash"></i></button>
                            <div class="m-t-2 text-muted font-size-12">JPG, GIF or PNG : Not Fixed Size (1:1)</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label class="required">@lang('admin_article.title_th')</label>
                            {{ Form::text('title_th',!empty($data['title_th'])?$data['title_th']:'', ['class' => 'form-control','placeholder' => trans('admin_article.title_th')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label class="required">@lang('admin_article.title_en')</label>
                            {{ Form::text('title_en',!empty($data['title_en'])?$data['title_en']:'', ['class' => 'form-control','placeholder' => trans('admin_article.title_en')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_article.link')</label>
                            {{ Form::text('link',!empty($data['link'])?$data['link']:'', ['class' => 'form-control','placeholder' => 'Full Path Ex:www.google.com']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_article.description_th')</label>
                            {{ Form::textarea('detail_th',!empty($data['detail_th'])?$data['detail_th']:'', ['class' => 'form-control summernote','placeholder' => trans('admin_article.description_th')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_article.description_en')</label>
                            {{ Form::textarea('detail_en',!empty($data['detail_en'])?$data['detail_en']:'', ['class' => 'form-control summernote','placeholder' => trans('admin_article.description_en')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>@lang('admin_article.order')</label>
                            {{ Form::number('order',!empty($data['order'])?$data['order']:'', ['class' => 'form-control','placeholder' => trans('admin_article.order')]) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>&nbsp;</label>
                            <label class="switcher switcher-lg switcher-primary">
                                @if(isset($data['status']))
                                <input type="checkbox" name="status" {{ $data['status']==1?'checked':'' }} >
                                @else
                                <input type="checkbox" checked name="status">
                                @endif
                                <div class="switcher-indicator">
                                    <div class="switcher-yes">@lang('common.yes')</div>
                                    <div class="switcher-no">@lang('common.no')</div>
                                </div>                     
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">@lang('admin_article.submit')</button>
                </div>
                {{ Form::close() }}
            </div><!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('component/custom-upload/custom-upload.js') !!}"></script>
<script src="{!! asset('admin/js/ourproduct_form.js') !!}"></script>
@endsection



