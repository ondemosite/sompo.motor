@extends('admin.layout.default')
@section('title') Admins @endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-file-picture-o"></i>@lang('admin_article.our_product')</span>
                    <div class="panel-heading-controls">
                        <a href="{{ route('admin.ourproduct_form') }}" title="Add Article"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_article.no')</th>
                                <th>@lang('admin_article.cover_th')</th>
                                <th>@lang('admin_article.cover_en')</th>
                                <th>@lang('admin_article.title_th')</th>
                                <th>@lang('admin_article.title_en')</th>
                                <th>@lang('admin_article.link')</th>
                                <th>@lang('admin_article.view')</th>
                                <th>@lang('admin_article.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/ourproduct.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_asset_url',asset('')) }} 
{{ Form::hidden('hd_get_article_list',route('admin.get_ourproduct_list')) }} 
{{ Form::hidden('hd_save_article',route('admin.ourproduct_save')) }} 
{{ Form::hidden('hd_delete_article',route('admin.delete_ourproduct')) }} 
@endsection
