<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <title>Sompo Backoffice Signin</title>
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

  <!-- Custom styling -->
  <link rel="stylesheet" href="{!! asset('theme/css/bootstrap.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('theme/css/pixeladmin.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('theme/css/themes/candy-red.min.css') !!}" >
  <link rel="stylesheet" href="{!! asset('theme/css/demo.css') !!}">
  <link rel="stylesheet" href="{!! asset('admin/css/login.css') !!}">
  <!-- / Custom styling -->
</head>
<body>
  <div class="page-signin-header p-a-2 text-sm-center bg-white">
    <a class="px-demo-brand px-demo-brand-lg text-default" href="index.html"><img src="{{ asset('/images/admin/logo/logo.gif') }}" width="200px" /></a>
    <!--<a href="pages-signup-v2.html" class="btn btn-primary">Sign Up</a>-->
  </div>

  <!-- Sign In form -->

  <div class="page-signin-container" id="page-signin-form">
    <h2 class="m-t-0 m-b-4 text-xs-center font-weight-semibold font-size-20 text-white font-weight-bold">Admin Login Section</h2>
    {!! Form::open(['url' => route('admin.login.submit'),'class'=>'panel p-a-4','id'=>'form_login']) !!}
      @if ($errors->any())
        <div class="alert alert-danger alert-dark">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <fieldset class=" form-group form-group-lg">
        {{ Form::text('username','', ['class' => 'form-control','placeholder' => 'Username']) }}
      </fieldset>

      <fieldset class=" form-group form-group-lg">
        {{ Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) }}
      </fieldset>

      <div class="clearfix">
        <label class="custom-control custom-checkbox pull-xs-left">
          <input type="checkbox" class="custom-control-input">
          <span class="custom-control-indicator"></span>
          Remember me
        </label>
        <!--<a href="#" class="font-size-12 text-muted pull-xs-right" id="page-signin-forgot-link">Forgot your password?</a>-->
      </div>
      <button type="submit" class="btn btn-block btn-lg btn-danger m-t-3">Sign In</button>
    {!! Form::close() !!}

    <!--
    <h4 class="m-y-3 text-xs-center font-weight-semibold text-muted">or sign in with</h4>

    
    <div class="text-xs-center">
      <a href="index.html" class="page-signin-social-btn btn btn-success btn-rounded" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a>&nbsp;&nbsp;&nbsp;
      <a href="index.html" class="page-signin-social-btn btn btn-info btn-rounded" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a>&nbsp;&nbsp;&nbsp;
      <a href="index.html" class="page-signin-social-btn btn btn-danger btn-rounded" data-toggle="tooltip" title="Google+"><i class="fa fa-google-plus"></i></a>
    </div> -->
  </div>

  <div class="px-responsive-bg">
    <div class="px-responsive-bg-overlay" ></div>
  </div>

  <!-- / Sign In form -->

  <!-- jQuery -->
  <script src="{!! asset('theme/js/jquery.min.js') !!}"></script>
  <script src="{!! asset('theme/js/bootstrap.min.js') !!}"></script>
  <script src="{!! asset('theme/js/pixeladmin.min.js') !!}"></script>
  <script src="{!! asset('admin/js/login.js') !!}"></script>
</body>
</html>
