    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        @page {
            margin-top: 0.25in;
            margin-left: 0.35in;
            margin-right: 0.35in;
            margin-bottom: 0.1in;
        }
        @font-face {
            font-family: 'AngsanaUPC';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/AngsanaUPC.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'AngsanaUPC';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/AngsanaUPCBold.ttf') }}") format('truetype');
        }
        body{
            font-family: 'AngsanaUPC';
            font-size:12pt;
            width:100%;
        }
        .page-break {
            page-break-after: always;
        }
        .head{
            margin-bottom: 28pt;
        }
        td,th {
            border:0.1pt solid #000000;
            font-weight:100;
        }
        table.invoice-table td > *,table.invoice-table th > *{
            padding-left:0pt;
            padding-top:-2pt;
            padding-bottom:-8pt;
            line-height: 1pt;
        }
        .n-b-t{
            border-top:0px;
        }
        .n-b-r{
            border-right:0px;
        }
        .n-b-b{
            border-bottom:0px;
        }
        .n-b-l{
            border-left:0px;
        }
        .b-t{
            border-top:0.1pt solid #000000 !important;
        }
        .b-r{
            border-right:0.1pt solid #000000 !important;
        }
        .b-b{
            border-bottom:0.1pt solid #000000 !important;
        }
        .b-l{
            border-left:0.1pt solid #000000 !important;
        }


        /*  Content */
        .border{
            text-align: center;
            font-size:14pt;
            border:1pt solid #000000;
            margin: 10pt 0pt;
            padding-bottom: -6pt;
        }
        .border p{
            font-weight: bold;
            line-height: 1pt;
            padding-bottom:-2pt;
        }
        .document-issue{
            text-align: right;
            font-size:10pt;
            margin: 19pt 0pt 0pt 0pt;
            padding-bottom: -15pt;
        }
        .document-issue p{
            padding-right:6pt;
            font-weight: bold;
            line-height: 1pt;
            padding-bottom:0pt;
        }
        .table-detail{
            margin-top: 5pt;
        }
        
        table.normal-table td{
            padding-left:6pt;
            padding-top:-1pt;
            padding-bottom:-7pt;
            padding-right:6pt;
            line-height: 1pt;
        }
        .noborder-table td{
            border:0px;
        }
        .wrap-dotted{
            position: relative;
            display: inline-block;
        }
        .dotted{
            position: absolute;
            border-bottom: 0.7pt dotted #000000;
            text-decoration: none;   
            width:100%;
            top:2pt;
            text-align:center;    
            padding-bottom:2pt;        
        }

        .checkbox {
            position:absolute;
            width:12pt;
            height:12pt;
            border: 1px solid #000;
            margin-top:3pt;
            text-align:center;
        }
        .checkbox img{
            margin-top: 1.2pt;
        }

        .label-check{
            margin-left:18pt;
            line-height:1pt;
        }
        
        
        .foot-p p{
            line-height: 0.4pt;
        }
        .sign{
            position:relative;
            width:120pt;
            border-bottom:1pt dashed #000000;
            margin:0 auto;
        }
        .sign-detail{
            margin-top: -17pt;
            padding-bottom: -8pt;
            text-align:center;
        }
        .sign > img{
            text-align:center;
            height:35pt;
            margin-top: -2pt;
        }

        .payment-type{
            margin-top:10pt;
        }
        .payment-type .box-left{
            float:left;
         }
        .payment-type .box-right{
            display: block;
            float:left;
            padding-left:18pt;
        }
        .water-mask{
            position: absolute;
            top:0px;
            left: 0px;
            width: 21cm;
            height: 29.7cm;
            text-align: center;
        }
        .water-mask img{
            width: 100%;
        }

        </style>
    </head>
    <body>
    @php
        $order = $init->order()->first();
        $payment = $init->payment()->first();
        $receipt = $init->receipt()->first();
        $vehicle = $init->vehicle_info()->first();
        $endorse = $init->endorse()->first();
        $driver = $endorse->owner()->first();
        $driver1 = $endorse->driver1()->first();
        $driver2 = $endorse->driver2()->first();
        
        $province = $driver->province()->first();
        $district = $driver->district()->first();
        $subdistrict = $driver->subdistrict()->first();
        $province_prefix = trans('step5.province_prefix');
        $district_prefix = ($province->id=="00"?trans('step5.district_prefix_c'):trans('step5.district_prefix'));
        $subdistrict_prefix = ($province->id=="00"?trans('step5.subdistrict_prefix_c'):trans('step5.subdistrict_prefix'));
        $full_address = $driver->address." ".$subdistrict_prefix.$subdistrict->name_th." ".$district_prefix.$district->name_th." ".$province_prefix.$province->name_th." ".$driver->postalcode;
    @endphp

    @if(!empty($copy))
        @if($copy)
        <div class="water-mask">
            <img src="{{ public_path('images/admin/background/watermask.png') }}" />
        </div>
        @endif
    @endif
    
    <div class="head">
        <table class="noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="60%"><img src="{{ public_path('images/company/jF463JbDq31gdVYhPm3d/w4EMxrbjyuzSUKKgzHMR.png') }}" width="435pt" /></td>
                <td width="40%" style="padding:0px;padding-left:35pt;padding-top:6pt;">
                    {!! '<img width="240pt" src="data:image/png;base64,' . DNS1D::getBarcodePNG($init->policy_com_number, "C128",1,48) . '" />' !!}
                </td>
            </tr>
        </table>
    </div>
    <div class="body">
        <table class="invoice-table noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
               <td width="42%"><p>เลขประจำตัวผู้เสียภาษี (Tax ID No.) {{$fix['tax_id_no']}}</p></td>
               <td width="25%"></td>
               <td width="10%"><p>เลขที่</p></td>
               <td><p>{{$init->com_tax_note_number}}</p></td>
           </tr>
           <tr>
               <td><p>&nbsp;</p></td>
               <td align="left"><p>สำนักงานใหญ่</p></td>
               <td><p>วันที่</p></td>
               <td><p>{{getLocaleDate(date('Y-m-d'))}}</p></td>
           </tr>
           <tr>
               <td><p>&nbsp;</p></td>
               <td><p>&nbsp;</p></td>
               <td><p>เลขที่อ้างอิง</p></td>
               <td><p>&nbsp;</p></td>
           </tr>
        </table>
        <table class="noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="25%"><p>&nbsp;</p></td>
                <td width="42%">
                    <div class="border">
                        <p>ใบเสร็จรับเงิน / ใบกำกับภาษี (สำเนา)</p>
                        <p>RECEIPT / TAX INVOICE (COPY)</p>
                    </div>
                </td>
                <td>
                    <div class="document-issue" style="margin-top:-15pt">
                        
                    </div>
                </td>
            </tr>
        </table>

        <div class="table-detail">
            <table  class="normal-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" width="65%"><p>ชื่อและที่อยู่ผู้เอาประกันภัย</p></td>
                    <td class="n-b-b" align="right" width="15%"><p>เบี้ยประกันภัย</p></td>
                    <td class="n-b-b" align="right"><p>{{number_format($order->compulsory_net_premium,2)}} บาท</p></td>
                </tr>
                <tr>
                    <td class="n-b-b" align="left" width="65%" rowspan="4" style="padding-top:-6.1pt">
                        <p>{{ $driver->name." ".$driver->lastname }}</p>
                        <p>{{ $full_address }}</p>
                        <p>เลขประจำตัวผู้เสียภาษี {{ $driver->idcard }}</p>
                    </td>
                </tr>
                <tr>
                    <td class="n-b-b" align="right"><p>อากรแสตมป์</p></td>
                    <td class="n-b-b" align="right" ><p>{{number_format($order->compulsory_stamp,2)}} บาท</p></td>
                </tr>
                <tr>
                    <td class="n-b-b" align="right"><p>รวมเงิน</p></td>
                    <td class="n-b-b" align="right" ><p>{{number_format($order->compulsory_net_premium+$order->compulsory_stamp,2)}} บาท</p></td>
                </tr>
                <tr>
                    <td class="n-b-b" align="right"><p>ภาษีมูลค่าเพิ่ม</p></td>
                    <td class="n-b-b" align="right" ><p>{{number_format($order->compulsory_vat,2)}} บาท</p></td>
                </tr>
                <tr>
                    <td class="n-b-t" align="left"><p>อาชีพ -</p></td>
                    <td align="right"><p>รวมเป็นเงิน</p></td>
                    <td align="right" ><p>{{number_format($order->compulsory_net_premium+$order->compulsory_stamp+$order->compulsory_vat,2)}} บาท</p></td>
                </tr>
            </table>
            <table  class="normal-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="n-b-t n-b-r" width="60%"><p>กรมธรรม์ประกันภัยเลขที่&nbsp;&nbsp;{{$init->policy_com_number}}</p></td>
                    <td class="n-b-t n-b-l n-b-r" align="right" width="20%"><p><b>วันครบกำหนดชำระเงิน:</b></p></td>
                    <td class="n-b-t n-b-l" align="right" width="20%"><p><b>{{getLocaleDate($order->order_expire)}}</b></p></td>
                </tr>
            </table>
            <table  class="normal-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center"><p>รหัส</p></td>
                    <td align="center" width="30%"><p>ชื่อรถยนต์</p></td>
                    <td align="center"><p>เลขทะเบียน</p></td>
                    <td align="center"><p>เลขตัวถัง</p></td>
                    <td align="center" width="20%"><p>จำนวนที่นั่ง/ขนาด/น้ำหนัก</p></td>
                </tr>
                <tr>
                    <td align="center"><p>{{ $vehicle->mortor_code_av }}</p></td>
                    <td align="center"><p>{{ strtoupper($vehicle->brand." ".$vehicle->model) }}</p></td>
                    <td align="center">
                        <p>{{ str_replace(' ','-',$init->car_licence) }}</p>
                        <p>{{ $init->car_province()->first()->name_th }}</p>
                    </td>
                    <td align="center"><p>{{ $init->car_chassis_number }}</p></td>
                    <td align="center"><p>{{$vehicle->car_seat."/".number_format($vehicle->cc,0)}}cc/-</p></td>
                </tr>
            </table>
            <table  class="normal-table noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="b-l" width="17%"><p>ระยะเวลาเอาประกันภัย:</p></td>
                    <td width="43%"><p>เริ่มต้นวันที่&nbsp;<span class="wrap-dotted" style="width:100pt;height:3pt;"><div class="dotted" >{{ getLocaleDate($init->compulsory_start) }}</div></span> เวลา {{ date('H:i',strtotime($init->compulsory_start)) }} น.
                    </p>
                    </td>
                    <td class="b-r"><p>สิ้นสุดวันที่&nbsp;<span class="wrap-dotted" style="width:100pt;height:3pt;"><div class="dotted" >{{ getLocaleDate($init->compulsory_expire) }}</div></span>
                        เวลา {{ date('H:i',strtotime($init->compulsory_expire)) }} น.
                        </p>
                    </td>
                </tr>
            </table>
            <table  class="normal-table noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="b-l" width="17%"><p>ประเภทการประกันภัย</p></td>
                    <td width="12%">
                        <div class="checkbox checked"><img src="{{ public_path('images/web/icon/x-bar.png') }}" width="12pt" /></div><span class="label-check">พ.ร.บ.</span>
                    </td>
                    <td width="13.5%">
                        <div class="checkbox"></div><span class="label-check">ประเภท 1</span>
                    </td>
                    <td width="17.5%">
                        <div class="checkbox"></div><span class="label-check">ประเภท 2</span>
                    </td>    
                    <td class="b-r">
                        <div class="checkbox"></div><span class="label-check">ประเภท 3</span>
                    </td>
                </tr>
            </table>
            <table  class="normal-table noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="b-l" width="8.8%"><p>จำนวนเงินเอาประกันภัย</p></td>
                    <td class="b-r" width="43%"><p><span class="wrap-dotted" style="width:100pt;height:3pt;"><div class="dotted" >{{number_format(304000,2)}}</div></span>
                        บาท (รถยนต์รวมอุปกรณ์ตกแต่งเพิ่มเติม)
                    </p>
                    </td>
                </tr>
            </table>
            <table  class="normal-table noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="b-l" width="14.3%"><p>อุปกรณ์ตกแต่งเพิ่มเติม</p></td>
                    <td class="b-r" width="70%"><p>
                        <span class="wrap-dotted" style="width:289pt;height:3pt;"><div class="dotted" >-</div></span>
                        &nbsp;ราคา&nbsp;
                        <span class="wrap-dotted" style="width:100pt;height:3pt;"><div class="dotted" >0.00</div></span>
                        &nbsp;บาท&nbsp;
                    </p>
                    </td>
                </tr>
            </table>
            <table  class="normal-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>                  
                    <td class="n-b-t n-b-b n-b-r" width="50%" align="left">
                        <div class="checkbox"><img src="{{ public_path('images/web/icon/x-bar.png') }}" width="12pt" /></div><span class="label-check">ไม่ระบุชื่อผู้ขับขี่</span>
                    </td>
                    <td class="n-b-t n-b-b n-b-l" width="17%"><p>&nbsp;</p></td>
                </tr>
            </table>
            <table  class="normal-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="n-b-t n-b-b n-b-r" width="11%">
                        <div class="checkbox"></div><span class="label-check">ระบุชื่อผู้ขับขี่</span>
                    </td>
                    <td class="n-b-t n-b-l n-b-b">
                        <p>1)<span class="wrap-dotted" style="width:135pt;height:3.2pt;"><div class="dotted" style="text-align:left;padding-left:3pt;"></div></span> 
                        วัน เดือน ปีเกิด<span class="wrap-dotted" style="width:70pt;height:3.2pt;"><div class="dotted" ></div></span> 
                        อาชีพ<span class="wrap-dotted" style="width:30pt;height:3.2pt;"><div class="dotted" ></div></span> 
                        เลขประจำตัวประชาชน<span class="wrap-dotted" style="width:70pt;height:3.2pt;"><div class="dotted" ></div></span> 
                    </p>
                    </td>
                </tr>
                <tr>
                    <td class="n-b-t n-b-b n-b-r" width="11%">
                       &nbsp;
                    </td>
                    <td class="n-b-t n-b-b n-b-l">
                        <p>2)<span class="wrap-dotted" style="width:135pt;height:3pt;"><div class="dotted" style="text-align:left;padding-left:3pt;"></div></span> 
                        วัน เดือน ปีเกิด<span class="wrap-dotted" style="width:70pt;height:3pt;"><div class="dotted" ></div></span> 
                        อาชีพ<span class="wrap-dotted" style="width:30pt;height:3pt;"><div class="dotted" ></div></span> 
                        เลขประจำตัวประชาชน<span class="wrap-dotted" style="width:70pt;height:3pt;"><div class="dotted" ></div></span> 
                    </p>
                    </td>
                 
                </tr>
            </table>
            <table  class="normal-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>                  
                    <td class="n-b-t n-b-b" align="left">
                        @if($vehicle->mortor_code_av != 320)
                        <p>การใช้รถยนต์&nbsp;<span class="wrap-dotted" style="width:486pt;height:3.2pt;"><div class="dotted" style="text-align:left;padding-left:10pt;">ใช้ส่วนบุคคล ไม่ใช่รับจ้างหรือให้เช่า</div></span> 
                        @else
                        <p>การใช้รถยนต์&nbsp;<span class="wrap-dotted" style="width:486pt;height:3.2pt;"><div class="dotted" style="text-align:left;padding-left:10pt;">ใช้เพื่อการพาณิชย์ไม่ใช้เพื่อการบรรทุกและขนส่งสินค้าที่มีความเสี่ยงภัยสูง เช่น เชื้อเพลิง กรด แก๊ส</div></span> 
                        @endif
                        
                    </td>
                </tr>
                <tr>                  
                    <td class="n-b-t n-b-b" align="left">
                        <p>วันทำสัญญาประกันภัย&nbsp;<span class="wrap-dotted" style="width:458pt;height:3.2pt;"><div class="dotted" style="text-align:left;padding-left:13pt;margin-top:-2pt;">{{ getLocaleDate($payment->paid_date) }}</div></span> 
                    </td>
                </tr>
            </table>
            <table  class="normal-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>                  
                    <td class="n-b-r" align="left" width="16%">
                        <p>Broker/ Agent Code</p>
                    </td>
                    <td class="n-b-l">
                        <p>โดยตรง</p>
                    </td>
                </tr>
            </table>
             <table  class="normal-table foot-p noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:5pt;">
                <tr>                  
                    <td align="left">
                        <p>เพื่อเป็นการตอบแทนเบี้ยประกันภัยที่ผู้เอาประกันภัยได้ชำระตามจำนวนข้างต้น บริษัทตกลงให้ความคุ้มครองผู้เอาประกันภัยโดยมีรายละเอียดความคุ้มครอง เงื่อนไข </p>
                        <p>ข้อยกเว้น ตามกรมธรรม์ประกันภัย</p>
                    </td>
                </tr>
            </table>

        </div>
        
    </div>
    <div class="foot">
        <table class="noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ public_path('images/company/28ee55ql69kus34wp5zs/wrbe9kr91pvqf8b1rf57.png') }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ - Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ public_path('images/company/9ov6o7mja0emyock1qd8/x8qlck2omvloo8irrk45.png') }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ - Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                       <img src="{{ public_path('images/company/xalkje11s15asd9adsxdeq9/xlkiyhd124sda84ewq6ax.png') }}" />
                    </div>
                    <div class="sign-detail">
                        <p>ผู้รับเงิน</p>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="payment-type">
        <table  class="normal-table noborder-table" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="15%">
                    <p>การชำระเงิน</p>
                    <p>Form of Payment</p>
                </td>
                <td width="20%">
                    <div class="box-right">
                        <p>บัตรเครดิต</p><p>Credit Card</p>
                    </div>
                    <div style="clear:both;"></div>
                </td>
                <td >
                    <div class="box-right">
                        <p>หมายเลขบัตรเครดิต</p><p>Credit card no
                            <span class="wrap-dotted" style="width:132pt;height:1.2pt;"><div class="dotted" style="text-align:left;padding-left:5pt;margin-top:-3pt;">{{ $receipt->credit_number }}</div></span> 
                        </p>
                    </div>
                    <div style="clear:both;"></div>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </td>
                <td>
                    <div class="box-right">
                        <p>ลงวันที่</p><p>Date
                            <span class="wrap-dotted" style="width:100pt;height:1.2pt;"><div class="dotted" style="text-align:left;padding-left:5pt;margin-top:-3pt;">{{ getLocaleDate(date('Y-m-d H:i'),true) }}</div></span> 
                        </p>
                    </div>
                    <div style="clear:both;"></div>
                </td>
                <td >
                    <div class="box-right">
                        <p>เลขที่ใบชำระเงิน</p><p>Payment no
                            <span class="wrap-dotted" style="width:140pt;height:1.2pt;"><div class="dotted" style="text-align:left;padding-left:5pt;margin-top:-3pt;">{{ $payment->payment_no }} </div></span> 
                        </p>
                    </div>
                    <div style="clear:both;"></div>
                </td>
            </tr>
        </table>
    </div>


    </body>
    </html>
    
    

    