    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        @page {
            margin-top: 0.13in;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        @font-face {
            font-family: 'AngsanaNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/AngsanaNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'AngsanaNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/AngsanaNewBold.ttf') }}") format('truetype');
        }
        body{
            font-family: 'AngsanaNew';
            font-size:12pt;
            width:100%;
        }
        .page-break {
            page-break-after: always;
        }

        /*  Content */
        .g-padding{
            padding:0px 5pt;
        }
        .no-border-left{
            border-left:0pt !important;
        }
        .no-border-right{
            border-right:0pt !important;
        }
        .no-border-bottom{
            border-bottom:0pt !important;
        }
        .no-border-top{
            border-top:0pt !important;
        }
        .border-top{
            border-top:0.1pt solid #000000 !important;
        }
        .large-text{
            font-weight:bold;
            font-size:36pt;
        }
        .header-text{
            font-weight:bold;
            font-size:16pt;
            padding-top: -2pt;
        }
        .line-br{
            padding-top:1pt;
            padding-bottom:2pt;
            line-height:10pt;
        }
        .text-area{
            height:32pt;
            vertical-align:text-top;
        }
        .head{
            margin-left:-5pt;
            margin-bottom:8pt;
        }
        /* element */
        p{
            padding:0pt;
            margin:0pt;
            font-size:12.04pt;
        }
        h1{
            padding:0px;
            margin:0px;
        }
        td,th {
            border:0.1pt solid #000000;
            border-top: 0px;
            padding:0pt 6pt;
            font-weight:100;
        }
        td > *,th > *{
            margin-top:-2pt;
            padding-bottom:1pt;
        }
        .checkbox {
            position:absolute;
            width:9pt;
            height:8pt;
            border: 1px solid #000;
            margin-top:5pt;
        }
        .label-check{
            margin-left:15pt;
            line-height:11pt;
        }
        
        /* This is what simulates a checkmark icon */
        .checkbox.checked:after {
            content: '';
            display: block;
            width: 3pt;
            height: 7pt;
            
            /* "Center" the checkmark */
            position:relative;
            top:-1pt;
            left:3pt;
            
            border: solid #000;
            border-width: 0 1.5pt 1.5pt 0;
            transform: rotate(45deg);
        }
        /* Content */
        table.sub-table{
            padding-top:4pt;
        }
        table.sub-table td{
            border:0px;
            padding-left:0px;
            padding-bottom:3pt;
        }
        table.sub-table2{
            padding-top:2pt;
        }
        table.sub-table2 td{
            border:0px;
            padding-left:2pt;
            padding-bottom:2pt;
        }
        .stamp{
            padding-top:-2pt;
            margin:1pt 0pt 1pt 0pt;
            border:1px solid #FF0000;
            color:#FF0000;
            text-align:center;
            width:60pt;
        }
        div.alert-text{
            position:absolute;
            width:100%;
            text-align:center;
            margin-top:15pt;
        }
        .sign{
            position:relative;
            width:120pt;
            border-bottom:1pt dashed #000000;
            margin:0 auto;
        }
        .sign-detail{
            text-align:center;
        }
        .sign > img{
            text-align:center;
            width:110pt;
        }
        .company-stamp{
            position:absolute;
            margin-top:-40pt;
            top:-15pt;
            margin-left:90pt;
        }
        .company-stamp > img{
            width:170pt;
        }
        .unknow td{
            padding-top:-4pt;
            padding-bottom:2pt;
        }
        </style>
    </head>
    <body>
    @php
        $order = $init->order()->first();
        $payment = $init->payment()->first();
        $vehicle = $init->vehicle_info()->first();
        $endorse = $init->endorse()->first();
        $driver = $endorse->owner()->first();
        $driver1 = $endorse->driver1()->first();
        $driver2 = $endorse->driver2()->first();
        
        $province = $driver->province()->first();
            $district = $driver->district()->first();
            $subdistrict = $driver->subdistrict()->first();
            $province_prefix = trans('step5.province_prefix');
            $district_prefix = ($province->id=="00"?trans('step5.district_prefix_c'):trans('step5.district_prefix'));
            $subdistrict_prefix = ($province->id=="00"?trans('step5.subdistrict_prefix_c'):trans('step5.subdistrict_prefix'));
            $full_address = $driver->address." ".$subdistrict_prefix.$subdistrict->name_th." ".$district_prefix.$district->name_th." ".$province_prefix.$province->name_th." ".$driver->postalcode;
    @endphp
    <div class="head">
        <img src="{{ public_path('images/company/jF463JbDq31gdVYhPm3d/w4EMxrbjyuzSUKKgzHMR.png') }}" width="435pt" />
    </div>
    <div class="body">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="border-top no-border-right" width="20%" align="left"><p>รหัสบริษัท : SIT</p></td>
                <td class="border-top no-border-left no-border-right " width="48%" align="center"><h1 class="header-text">ตารางกรมธรรม์ประกันภัยรถยนต์</h1></td>
                <td class="border-top no-border-left" width="32%" style="padding:0px;padding-left:5pt;padding-top:3pt;">
                    {!! '<img width="220pt" src="data:image/png;base64,' . DNS1D::getBarcodePNG($init->policy_number, "C128",1,48) . '" />' !!}
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr style="padding:0">
                <td class="no-border-right" width="15%"><p>กรมธรรม์ประกันภัยเลขที่</p></td>
                <td class="no-border-right no-border-left" width="42.8%"><p>{{ $init->policy_number }}</p></td>
                <td class="no-border-right no-border-left" width="14%" align="right"><p>อาณาเขตคุ้มครอง</p></td>
                <td class="no-border-left" width="10%" align="left"><p>ประเทศไทย</p></td>
            </tr>
            <tr>
                <td class="no-border-right no-border-bottom" ><p>ผู้เอาประกันภัย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ชื่อ</p></td>
                <td class="no-border-right no-border-left no-border-bottom"><p>{{ $driver->name." ".$driver->lastname }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left no-border-bottom" align="left"><p>-</p></td>
            </tr>
            <tr>
                <td class="no-border-right text-area" ><p style="padding-left:62pt;">ที่อยู่</p></td>
                <td class="no-border-left no-border-right text-area"><p>{{ $full_address }}</p></td>
                <td class="no-border-left no-border-right">&nbsp;</td>
                <td class="no-border-left">&nbsp;</td>
            </tr>
        </table>
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right no-border-bottom" width="11%"><p>ผู้ขับขี่ 1</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="36%"><p>{{ !empty($driver1)?($driver1->name." ".$driver1->lastname):'' }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="10%" align="right"><p>วัน/เดือน/ปีเกิด</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="18%"><p>{{ !empty($driver1)?(getLocaleDate($driver1->birth_date)):'-'  }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="7%" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left no-border-bottom" align="left" width="18%"><p>-</p></td>
            </tr>
            <tr>
                <td class="no-border-right " width="11%"><p>ผู้ขับขี่ 2</p></td>
                <td class="no-border-right no-border-left" width="36%"><p>{{ !empty($driver2)?($driver2->name." ".$driver2->lastname):'' }}</p></td>
                <td class="no-border-right no-border-left" width="10%" align="right"><p>วัน/เดือน/ปีเกิด</p></td>
                <td class="no-border-right no-border-left" width="18%"><p>{{ !empty($driver2)?(getLocaleDate($driver2->birth_date)):'-'  }}</p></td>
                <td class="no-border-right no-border-left" width="7%" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left" align="left" width="18%"><p>-</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="8.7%" align="left"><p>ผู้รับประโยชน์</p></td>
                <td class="no-border-left no-border-right" width="60%" align="left"><p>-</p></td>
                <td class="no-border-left" width="10%" style="padding-right:0px;"><div class="stamp">ชำระอากรแล้ว</div></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" witdh="22%"><p>ระยะเวลาประกันภัย : เริ่มต้นวันที่</p></td>
                <td class="no-border-left no-border-right" align="left" width="18%"><p>{{ getLocaleDate($init->insurance_start) }}</p></td>
                <td class="no-border-left no-border-right" width="11%"><p>เวลา {{ date('H:i',strtotime($init->insurance_start)) }} น.</p></td>
                <td class="no-border-left no-border-right" align="right" width="19%"><p>สิ้นสุดวันที่</p></td>
                <td class="no-border-left no-border-right" width="17%"><p>{{ getLocaleDate($init->insurance_expire) }}</p></td>
                <td class="no-border-left" align="left" width="13%"><p>เวลา {{ date('H:i',strtotime($init->insurance_expire)) }} น.</p></td>
            </tr>
            <tr>
                <td colspan="6"><p>รายการรถยนต์ที่เอาประกันภัย</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ลำดับ</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">รหัส</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ชื่อรถยนต์/รุ่น</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">เลขทะเบียน</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">เลขตัวถัง</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ปีรุ่น</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">แบบตัวถัง</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">จำนวนที่นั่ง/ขนาด/น้ำหนัก</td>
                </tr>
                <tr>
                    <td align="center" width="5%"><p>001</p></td>
                    <td align="center" width="5%"><p>{{ $vehicle->mortor_code_av }}</p></td>
                    <td class="line-br" align="center" width="16%"><p>{{ strtoupper($vehicle->brand." ".$vehicle->model) }}</p></td>
                    <td class="line-br" align="center" width="10%"><p>{{ $init->car_licence }}<br/>{{ $init->car_province()->first()->name_th }}</p></td>
                    <td align="center" width="19%"><p>{{ $init->car_chassis_number }}</p></td>
                    <td align="center" width="5%"><p>{{ $vehicle->year }}</p></td>
                    <td align="center" width="10%"><p>{{ $vehicle->body_type }}</p></td>
                    <td align="center" width="15%">
                    <p>
                        @if($vehicle->mortor_code_av == 110)
                            {{ $vehicle->car_seat."/".number_format($vehicle->cc,0) }}cc/- 
                        @else
                            {{ $vehicle->car_seat."/-/"."ไม่เกิน 3 ตัน" }}
                        @endif
                    </p>
                    </td>
                </tr>
                <tr>
                    <td style="height:18pt;" colspan="8">
                        <p>จานวนเงินเอาประกันภัย : กรมธรรม์ประกันภัยนี้ให้การคุ้มครองเฉพาะข้อตกลงคุ้มครองที่มีจานวนเงินเอาประกันภัยระบุไว้เท่านั้น</p>
                    </td>
                </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" width="30.5%"><p>ความรับผิดต่อบุคคลภายนอก</p></td>
                <td style="padding-top:3pt;padding-bottom:0pt;" class="line-br" width="27.5%" align="center"><p>รถยนต์เสียหาย สูญหาย ไฟไหม้</p></td>
                <td style="padding-top:3pt;padding-bottom:0pt;" class="line-br" width="42%" align="center"><p>ความคุ้มครองตามเอกสารแนบท้าย</p></td>
            </tr>
            <tr>
                <td class="line-br" align="left" valign="top" style="padding-top:5pt;padding-bottom:15pt;">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" style="padding-top:1.4pt"><p>1)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ.</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_person_damage_person,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คน</p></td>
                        </tr>
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_person_damage_once,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>2)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายต่อทรัพย์สิน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_stuff_damage,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%" style="padding-left:11pt;"><p>2.1)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายส่วนแรก</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>0.00</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    
                    <div class="alert-text">
                        <h1 class="large-text">ไม่รวม พ.ร.บ.</h1>
                    </div>
                </td>
                <td class="line-br" valign="top" style="padding-top:5pt;padding-bottom:15pt;">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายต่อรถยนต์</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>0.00</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%" style="padding-left:11pt;"><p>1.1)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายส่วนแรก</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>0.00</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>2)</p></td>
                            <td valign="top" align="left"><p>รถยนต์สูญหาย/ไฟไหม้</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>0.00</p></td>
                            <td width="20%" align="left"><p>บาท</p></td>
                        </tr>
                    </table>
                </td>
                <td class="line-br" align="center" width="25%" valign="top" style="padding-top:5pt;padding-bottom:15pt;">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left"><p>อุบัติเหตุส่วนบุคคล</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="4%" style="padding-left:11pt;" align="left"><p>1.1)</p></td>
                            <td align="left"><p>เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="35%" align="left" style="padding-left:7pt;"><p>ก) ผู้ขับขี่ 1 คน</p></td>
                            <td width="30%" align="right"><p>{{number_format($init->insurance_death_disabled,2)}}</p></td>
                            <td width="25%" align="left"><p>บาท</p></td>
                        </tr>
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="35%" align="left" style="padding-left:7pt;"><p>ข) ผู้โดยสาร 4 คน</p></td>
                            <td width="30%" align="right"><p>{{number_format($init->insurance_death_disabled,2)}}</p></td>
                            <td width="25%" align="left"><p>บาท/คน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%" style="padding-left:11pt;"><p>1.2)</p></td>
                            <td valign="top" align="left"><p>ทุพพลภาพชั่วคราว</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="35%" align="left" style="padding-left:7pt;"><p>ก) ผู้ขับขี่ 1 คน</p></td>
                            <td width="30%" align="right"><p>0.00</p></td>
                            <td width="25%" align="left"><p>บาท/สัปดาห์</p></td>
                        </tr>
                        <tr>
                            <td width="10%"><p>&nbsp;</p></td>
                            <td width="35%" align="left" style="padding-left:7pt;"><p>ข) ผู้โดยสาร - คน</p></td>
                            <td width="30%" align="right"><p>0.00</p></td>
                            <td width="25%" align="left"><p>บาท/คน/สัปดาห์</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>2)</p></td>
                            <td width="34%" align="left"><p>ค่ารักษาพยาบาล</p></td>
                            <td width="35%" align="right"><p>{{number_format($init->insurance_medical_fee,2)}}</p></td>
                            <td align="left" ><p>บาท/คน</p></td>
                        </tr>
                        <tr>
                            <td valign="top" width="5%"><p>3)</p></td>
                            <td width="34%" align="left"><p>การประกันตัวผู้ขับขี่</p></td>
                            <td width="35%" align="right"><p>{{number_format($init->insurance_bail_driver,2)}}</p></td>
                            <td align="left" ><p>บาท/ครั้ง</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="no-border-right no-border-bottom"><p style="padding-top:0pt;">เบี้ยประกันภัยต่อความคุ้มครองหลัก</p></td>
                <td class="no-border-left no-border-right no-border-bottom">
                    <table class="sub-table2" width="100%" border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td class="no-border-left no-border-right no-border-bottom" width="80%" align="right"><p style="padding-top:1pt;">{{number_format($order->basic_premium_cover,2)}}</p></td>
                            <td class="no-border-left no-border-right no-border-bottom" width="20%" align="left"><p style="padding-top:1pt;">บาท</p></td>
                        </tr>
                    </table>
                </td>
                <td class="no-border-bottom">
                    <table class="sub-table2" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="no-border-left no-border-right no-border-bottom no-border-top" width="60%" align="left"><p style="padding-top:2pt;">เบี้ยประกันภัยตามเอกสารแนบท้าย</p></td>
                            <td class="no-border-left no-border-right no-border-bottom no-border-top" width="20%" align="right"><p style="padding-top:1pt;">{{number_format($order->additional_premium_cover,2)}} บาท</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="no-border-right"><p style="padding-top:-8pt;">(เบี้ยประกันภัยนี้ได้หักส่วนลดกรณีระบุผู้ขับขี่</p></td>
                <td class="no-border-left">
                    <table class="sub-table2" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="no-border-left no-border-right no-border-bottom no-border-bottom" width="80%" align="right"><p style="padding-top:-5pt;">0.00</p></td>
                            <td class="no-border-left no-border-right no-border-bottom no-border-bottom" width="20%" align="left"><p style="padding-top:-5pt;">บาทแล้ว)</p></td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table class="unknow" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="3%">ส่วนลด</td>
                <td class="no-border-left no-border-right" width="5%">ความเสียหายส่วนแรก</td>
                <td class="no-border-left no-border-right" width="8%" align="center">0.00 บาท</td>
                <td class="no-border-left no-border-right" width="8%">ส่วนลดกลุ่ม</td>
                <td class="no-border-left no-border-right" width="8%" align="center">0.00 บาท</td>
                <td class="no-border-left no-border-right" width="5%">ประวัติดี</td>
                <td class="no-border-left no-border-right" width="11%" align="center">{{number_format($order->ncb,2)}} บาท</td>
                <td class="no-border-left no-border-right" width="3%">อื่นๆ</td>
                <td class="no-border-left no-border-right" width="11%" align="center">-{{number_format(abs($order->cctv_discount)+abs($order->direct)+abs($order->net_discount),2)}} บาท</td>
                <td class="no-border-left">รวมส่วนลด -{{number_format(abs($order->ncb)+abs($order->cctv_discount)+abs($order->direct)+abs($order->net_discount),2)}} บาท</td>
            </tr>
            <tr>
                <td class="no-border-right" width="3%">ส่วนเพิ่ม</td>
                <td colspan="9" align="center">ประวัติเพิ่ม 0.00 บาท</td>
            </tr>
        </table>
        <table class="unknow" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center">เบี้ยประกันภัยสุทธิ</td>
                <td align="center">อากรแสตมป์</td>
                <td align="center">ภาษีมูลค่าเพิ่ม</td>
                <td align="center">รวม</td>
            </tr>
            <tr>
                <td align="center">{{number_format($order->insurance_net_premium,2)}}</td>
                <td align="center">{{number_format($order->stamp,2)}}</td>
                <td align="center">{{number_format($order->vat,2)}}</td>
                <td align="center">{{number_format($order->insurance_net_premium+$order->stamp+$order->vat,2)}}</td>
            </tr>
        </table>
        <table class="unknow" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" align="left" width="20%">รายการเอกสารท้ายที่แนบ</td>
                <td class="no-border-left" align="left">เอกสารแนบท้ายข้อยกเว้นภัยก่อการร้าย(ร.ย.30)</td>
            </tr>
            <tr>
                <td align="left" colspan="2">การใช้รถยนต์ : ใช้ส่วนบุคคล ไม่ใช้รับจ้างหรือให้เช่า</td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding-bottom:2pt;" class="no-border-right" width="20%">
                    <div class="checkbox"><img src="{{ public_path('images/web/icon/x-bar.png') }}" width="12pt" /></div><span class="label-check">ประกันภัยโดยตรง</span>
                </td>
                <td style="padding-bottom:2pt;" class="no-border-left no-border-right" width="20%">
                    <div class="checkbox"></div><span class="label-check">ตัวแทนประกันภัยรายนี้</span>
                </td>
                <td style="padding-bottom:2pt;" class="no-border-left no-border-right" align="left" width="30%">
                    <div class="checkbox"></div><span class="label-check">นายหน้าประกันภัยรายนี้</span>
                </td>
                <td style="padding-bottom:0pt;" class="no-border-left no-border-right" align="left" width="12%">
                    <p>ใบอนุญาตเลขที่</p>
                </td>
                <td style="padding-bottom:0pt;" class="no-border-left" align="left">
                    <p>ว00175/2522</p>
                </td>
            </tr>
             <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="20%">
                    <p>วันทำสัญญาประกันภัย</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="20%">
                    <p>{{ getLocaleDate($payment->paid_date) }}</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="left" width="35%">
                    <p>วันทำกรมธรรม์ประกันภัย</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="left" width="20%">
                    <p>{{ getLocaleDate($payment->paid_date) }}</p>
                </td>
                <td class="no-border-left no-border-bottom no-border-right" align="left">
                    <p>&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" colspan="5">
                    <p>เพื่อเป็นหลักฐาน บริษัท โดยบุคคลผู้มีอำนาจได้ลงลายมือชื่อและประทับตราของบริษัทไว้เป็นสำคัญ ณ สำนักงานของบริษัท</p>
                </td>
            </tr>
        </table>
    </div>
    <div class="foot" style="padding-top:10pt;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ public_path('images/company/28ee55ql69kus34wp5zs/wrbe9kr91pvqf8b1rf57.png') }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ - Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ public_path('images/company/9ov6o7mja0emyock1qd8/x8qlck2omvloo8irrk45.png') }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ - Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="center">
                    <div class="sign">
                        <img src="{{ public_path('images/company/x8qlck2omvloo8irrk45/obgej86xonldnrxa7fxh.png') }}" />
                    </div>
                    <div class="sign-detail">
                        <p>ผู้รับมอบอำนาจ – Authorized Signature</p>
                    </div>
                </td>
            </tr>
        </table>
        <div class="company-stamp">
            <img src="{{ public_path('images/company/aneasuuyiwl2u4wnh1ap/9ov6o7mja0emyock1qd8.png') }}" />
        </div>
    </div>

    </body>
    </html>
    
    

    