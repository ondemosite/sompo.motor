<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        @page {
            margin-top: 0.13in;
            margin-left: 0.42in;
            margin-right: 0.3in;
        }
        @font-face {
            font-family: 'AngsanaNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/AngsanaNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'AngsanaNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/AngsanaNewBold.ttf') }}") format('truetype');
        }
        body{
            font-family: 'AngsanaNew';
            font-size:12pt;
            width:100%;
        }
        .page-break {
            page-break-after: always;
        }

        /*  Content */
        .h-padding{
            padding:0pt 20pt;
        }
        .g-padding{
            padding:0px 5pt;
        }
        .no-border-left{
            border-left:0pt !important;
        }
        .no-border-right{
            border-right:0pt !important;
        }
        .no-border-bottom{
            border-bottom:0pt !important;
        }
        .no-border-top{
            border-top:0pt !important;
        }
        .border-top{
            border-top:0.1pt solid #000000 !important;
        }
        .large-text{
            font-weight:bold;
            font-size:36pt;
        }
        .header-text{
            font-weight:bold;
            font-size:13pt;
            line-height: 9pt;
            padding-top: 5pt;
        }
        .line-br{
            padding-top:2pt;
            padding-bottom:2pt;
            line-height:9pt;
        }
        .text-area{
            height:30pt;
            vertical-align:text-top;
        }
        .head{
            margin-left:-5pt;
        }
        /* element */
        p{
            padding:0pt;
            margin:0pt;
            font-size:10.5pt;
        }
        h1{
            padding:0px;
            margin:0px;
        }
        td,th {
            border:0.1pt solid #000000;
            border-top: 0px;
            padding:0pt 5pt;
            font-weight:100;
        }
        td > *,th > *{
            margin-top:-2pt;
            padding-bottom:1pt;
        }
        table.sub-table{
            padding-top:6pt;
        }
        table.sub-table td{
            border:0px;
            padding-left:0px;
        }
        .checkbox {
            position:absolute;
            width:9pt;
            height:8pt;
            border: 1px solid #000;
            margin-top:1pt;
        }
        .label-check{
            padding-left:15pt;
            line-height:13pt;
        }
        .text-underline{
            border-bottom:1px solid;
        }
        

        .checkbox.checked:before, .checkbox.checked:after {
            position: absolute;
            left: 15px;
            content: ' ';
            height: 33px;
            width: 2px;
            background-color: #333;
        }
        .checkbox.checked:before {
            transform: rotate(45deg);
        }
        .checkbox.checked:after {
            transform: rotate(-45deg);
        }

        /* Content */
        .stamp{
            padding-top:0pt;
            width:70pt;
        }
        div.alert-text{
            position:absolute;
            width:100%;
            text-align:center;
            margin-top: 20pt;
        }
        .sign{
            position:relative;
            width:120pt;
            border-bottom:1pt dashed #000000;
            margin:0 auto;
        }
        .sign-detail{
            text-align:center;
        }
        .sign-date{
            position: absolute;
            width: 100%;
            text-align:center;
            top: 10pt;
        }
        .sign > img{
            text-align:center;
            height:35pt;
        }
        .company-stamp{
            position:absolute;
            margin-top:-40pt;
            top:-15pt;
            margin-left:90pt;
        }
        .company-stamp > img{
            width:219pt;
        }

        .head .barcode{
            position: absolute;
            top: 16pt;
            right: 0pt;
        }
        .head .barcode-text{
            position: absolute;
            top: -5pt;
            right: 0pt;
            letter-spacing: 1pt;
            font-size: 14pt;
        }
        .head .barcode-text-bottom{
            position: absolute;
            top: 34pt;
            right: 16pt;
            letter-spacing: 6pt;
            font-size: 11pt;
        }
        p.br-shit{
            line-height: 5pt;
            padding-top: 6pt;
            padding-bottom: 3pt;
        }

        .prb-foot{
            position: relative;
            margin-top:65pt;
        }
        .prb-foot .text-header{
            width: 300pt;
            font-size: 13pt;
            line-height: 11pt;
            text-align:center;
            font-weight: bold;
            padding-top: -5pt;
        }
        .prb-foot .barcode{
            position: absolute;
            top: 0pt;
            right: 0pt;
        }
        .prb-foot .barcode-text{
            position: absolute;
            top: -20pt;
            right: 0pt;
            letter-spacing: 1pt;
            font-size: 14pt;
        }
        .prb-foot .barcode-text-bottom{
            position: absolute;
            top: 18pt;
            right: 16pt;
            letter-spacing: 6pt;
            font-size: 11pt;
        }
        .prb-foot .detail{
            padding-top: 5pt;
        }
        .prb-foot .detail .wrap-dotted{
            position: relative;
            display: inline-block;
        }
        .prb-foot .dotted{
            position: absolute;
            border-bottom: 0.7pt dotted #000000;
            text-decoration: none;   
            width:100%;
            top:5pt;
            text-align:center;    
            padding-bottom:-2pt;   
            padding-left:10pt;
            padding-right:11pt;     
        }
        .hr-line{
            position:absolute;
            width: 100%;
            height: 1pt;
            border-bottom: .5pt dashed;
            top: -15pt;
        }
        .water-mask{
            position: absolute;
            top:0px;
            left: 0px;
            width: 21cm;
            height: 29.7cm;
            text-align: center;
        }
        .water-mask img{
            width: 100%;
        }
        .company{
            height: 90pt;
        }
        .add-company{
            font-size: 11pt;
            position: absolute;
            right: 0;
            top: 70pt;
        }

        </style>
    </head>
    <body>
    @php
        $order = $init->order()->first();
        $payment = $init->payment()->first();
        $vehicle = $init->vehicle_info()->first();
        $endorse = $init->endorse()->first();
        $driver = $endorse->owner()->first();
        $driver1 = $endorse->driver1()->first();
        $driver2 = $endorse->driver2()->first();
        
        $province = $driver->province()->first();
        $district = $driver->district()->first();
        $subdistrict = $driver->subdistrict()->first();
        $province_prefix = trans('step5.province_prefix');
        $district_prefix = ($province->id=="00"?trans('step5.district_prefix_c'):trans('step5.district_prefix'));
        $subdistrict_prefix = ($province->id=="00"?trans('step5.subdistrict_prefix_c'):trans('step5.subdistrict_prefix'));
        $full_address = $driver->address." ".$subdistrict_prefix.$subdistrict->name_th." ".$district_prefix.$district->name_th." ".$province_prefix.$province->name_th." ".$driver->postalcode;
        
        $signFolder = "7-nov-19";
        if (strtotime($order->created_at->toDateTimeString()) >= strtotime('2019-11-14 00:00:00')) {
           $signFolder = "14-nov-19";
        }   
        $sign = [
            'director_1' => public_path('images/company/'.$signFolder."/director_1/sign-image.png"),
            'director_2' => public_path('images/company/'.$signFolder."/director_2/sign-image.png"),
            'authorized' => public_path('images/company/'.$signFolder."/authorized/sign-image.png")
        ];
    
    @endphp

    @if(!empty($copy))
        @if($copy)
        <div class="water-mask">
            <img src="{{ public_path('images/admin/background/watermask.png') }}" />
        </div>
        @endif
    @endif
    
    <div class="head">
        <div class="barcode">
            {!! '<img width="220pt" src="data:image/png;base64,' . DNS1D::getBarcodePNG($sticker_no, "C128",3,50) . '" />' !!}
        </div>
        <div class="barcode-text-bottom">
            {{ $sticker_no }}
        </div>
        <div class="company">
            @if(empty($clean))
            <img src="{{ public_path('images/company/stamp_header/stamp-image.png') }}" width="435pt" />
            @endif
        </div>
    </div>
    <div class="body">
        <div class="add-company">
            สำนักงานใหญ่
        </div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="border-top h-padding" width="100%" align="center" style="padding-top:2pt;padding-bottom: 1pt;"><h1 class="header-text">ตารางกรมธรรม์ประกันภัยคุ้มครองผู้ประสบภัยจากรถยนต์ขายผ่านทางอิเล็กทรอนิกส์ (Online)/ใบเสร็จรับเงิน/ใบกำกับภาษี <br/> THE SCHEDULE/RECEIPT/TAX INVOICE</h1></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" style="padding-bottom:-2pt;" width="7%"><p class="br-shit" style="font-size: 10pt">รหัสบริษัท<br/>Co. Code</p></td>
                <td class="no-border-left no-border-right" width="5%"><p style="font-weight: bold;font-size: 16pt;padding-top:-8pt;">SIT</p></td>
                <td class="no-border-left no-border-right" style="padding-bottom:-2pt;" width="15%"><p class="br-shit" style="font-size: 10pt" >กรมธรรม์ประกันภัยเลขที่<br/>Policy No.</p></td>
                <td class="no-border-left no-border-right" width="23%"><p style="padding-top:-3pt;font-size: 10pt">{{ $init->policy_com_number }}</p></td>
                
                <td class="no-border-left no-border-right" style="padding-bottom:-2pt;" width="15%"><p class="br-shit" style="font-size: 10pt" >ใบเสร็จรับเงิน/ใบกำกับภาษีเลขที่<br/>Receipt/Tax invoice No.</p></td>
                <td class=" no-border-left no-border-right" width="15%"><p style="padding-top:-3pt;font-size: 10pt">{{ $init->com_tax_note_number }}</p></td>
                <td class=" no-border-left no-border-right" style="padding-top:1pt" width="5%"><p class="br-shit" style="font-size: 10pt">วันที่<br/>Date</p></td>
                <td class="no-border-left" width="15%"><p style="padding-top:-3pt;font-size: 10pt">{{ getLocaleDate(date('Y-m-d')) }}</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right no-border-bottom" width="10.3%"><p class="br-shit">รายการ <br/>Item</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="19%"><p class="br-shit" style="line-height: 8pt;">1. ผู้เอาประกันภัย <br/>1. The Insured</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="8.3%"><p class="br-shit" style="line-height: 8pt;">ชื่อ : <br/>Name</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="45%"><p class="br-shit">{{ $driver->prefix_name." ".$driver->name." ".$driver->lastname }}</td>
                <td class="no-border-left no-border-bottom"><p class="br-shit" style="line-height: 8pt;text-align:right;">อาณาเขตที่คุ้มครอง<br/>Territorial Limit Covered</p></td>
            </tr>
            <tr>
                <td class="no-border-right" width="10.3%" style="padding-top:-7pt;"></td>
                <td class="no-border-right no-border-left"></td>
                <td class="no-border-right no-border-left" style="padding-top:-7pt;"><p class="br-shit" style="line-height: 8pt;">ที่อยู่ : <br/>Address</p></td>
                <td class="no-border-right no-border-left" style="padding-top:-7pt;"><p class="br-shit">{{ $full_address }}</td>
                <td class="no-border-left" style="padding-top:-7pt;"><p class="br-shit" style="line-height: 8pt;text-align:right;">: ประเทศไทย<br/>Thailand</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="10.3%"><p class="br-shit">รายการ <br/>Item</p></td>
                <td class="no-border-right no-border-left" width="19%"><p class="br-shit" style="line-height: 8pt;">2. ระยะเวลาประกันภัย :<br/>2. Period Insured</p></td>
                <td class="no-border-right no-border-left" width="8.3%"><p class="br-shit" style="line-height: 8pt;">เริ่มต้นวันที่<br/>From</p></td>
                <td class="no-border-right no-border-left" width="13%"><p class="br-shit">{{ getLocaleDate($init->compulsory_start) }}</td>
                <td class="no-border-right no-border-left" width="15%"><p class="br-shit" style="line-height: 8pt;">เวลา {{ date('H:i',strtotime($init->compulsory_start)) }} น.<br/>At {{ date('H:i',strtotime($init->compulsory_start)) }} hours</p></td>
                <td class="no-border-right no-border-left" width="8.3%"><p class="br-shit" style="line-height: 8pt;">ถึงวันที่<br/>To</p></td>
                <td class="no-border-right no-border-left" width="13%"><p class="br-shit">{{ getLocaleDate($init->compulsory_expire) }}</td>
                <td class="no-border-left" width="15%"><p class="br-shit" style="line-height: 8pt;">เวลา {{ date('H:i',strtotime($init->compulsory_expire)) }} น.<br/>At {{ date('H:i',strtotime($init->compulsory_expire)) }} hours</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="10.3%" style="padding-top:0pt;"><p class="br-shit">รายการ <br/>Item</p></td>
                <td class="no-border-left"><p class="br-shit" style="line-height: 8pt;padding-top:4pt">3. รถที่เอาประกันภัย &nbsp;&nbsp;&nbsp;:<br/>3. Particulars of Motor Vehicle</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-top:-3pt;padding-bottom:-4pt;" align="center"><p class="br-shit">รหัส<br/>Code</p></td>
                    <td style="padding-top:-3pt;padding-bottom:-4pt;" align="center"><p class="br-shit">ชื่อรถ<br/>Make</p></td>
                    <td style="padding-top:-3pt;padding-bottom:-4pt;" align="center"><p class="br-shit">เลขทะเบียน<br/>Licence No.</p></td>
                    <td style="padding-top:-3pt;padding-bottom:-4pt;" align="center"><p class="br-shit">เลขตัวถัง<br/>Chassis No.</p></td>
                    <td style="padding-top:-3pt;padding-bottom:-4pt;" align="center"><p class="br-shit">แบบตัวถัง<br/>Body Type</p></td>
                    <td style="padding-top:0pt;padding-bottom:-2pt;" align="center"><p class="br-shit" style="font-size:9pt;">ขนาดเครื่องยนต์<br/>จำนวนที่นั่ง/น้ำหนักรวม<br/>Capacity</p></td>
                </tr>
                <tr>
                    <td align="center" width="8%" style="padding-top:-1pt;padding-bottom:-1pt;"><p>{{ $vehicle->mortor_code_av }}</p></td>
                    <td class="line-br" align="center" width="18%" style="padding-top:-3pt;padding-bottom:-4pt;"><p>{{ strtoupper($vehicle->brand." ".$vehicle->model) }}</p></td>
                    <td class="line-br" align="center" width="10%" style="padding-top:0pt;padding-bottom:0pt;"><p>{{ str_replace(' ','-',$init->car_licence) }}<br/>{{ $init->car_province()->first()->name_th }}</p></td>
                    <td align="center" width="19%"><p>{{ $init->car_chassis_number }}</p></td>
                    <td align="center" width="10%"><p>{{ $vehicle->body_type }}</p></td>
                    <td align="center" width="15%">
                    <p>
                        @if( round($vehicle->mortor_code_ac,2) == 1.10)
                            {{ number_format($vehicle->cc,0)." cc/7/-" }}
                        @else
                            -/{{ $vehicle->car_seat }}/ไม่เกิน 3 ตัน
                        @endif
                        
                    </p>
                    </td>
                </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="10.3%" style="vertical-align:text-top;padding-bottom:-2pt;"><p class="br-shit">รายการ <br/>Item</p></td>
                <td class="no-border-right no-border-left" width="30%" style="vertical-align:text-top;padding-top:-2pt;padding-bottom:-2pt;"><p class="br-shit" style="line-height: 8pt;">4. จำนวนเงินคุ้มครองผู้ประสบภัย :<br/>4. Limit of Covered</p></td>
                <td class="no-border-left" style="vertical-align:text-top;padding-top: -3pt;padding-bottom:-2pt;">
                    <p class="br-shit" style="line-height: 8pt;font-size:10pt;">
                        (1) 80,000 บาท ต่อหนึ่งคน สำหรับความเสียหายต่อร่างกายหรืออนามัย<br/>
                        (2) 300,000 บาท ต่อหนึ่งคน สำหรับการเสียชีวิต หรือทุพพลภาพอย่างถาวร<br/>
                        (3) 200,000 บาท ถึง 300,000 บาท ต่อหนึ่งคน สำหรับการสูญเสียอวัยวะตามเงื่อนไขกรมธรรม์ฯ ข้อ 3<br/>
                        (4) 200 บาทต่อวัน รวมกันไม่เกิน 20 วัน สำหรับการชดเชยรายวันกรณีเข้ารักษาในสถานพยาบาลในฐานะคนไข้ใน<br/>
                        ทั้งนี้จำนวนเงินคุ้มครองสูงสุดสำหรับ (1) (2) (3) และ (4) รวมกันไม่เกิน 304,000 บาท ต่อหนึ่งคน<br/>
                        และรวมกันไม่เกินห้าล้านบาทสำหรับรถที่มีที่นั่งไม่เกินเจ็ดคนหรือรถบรรทุกผู้โดยสารรวมทั้งผู้ขับขี่ไม่เกินเจ็ดคน<br/>
                        และไม่เกินสิบล้านบาท สำหรับรถที่มีที่นั่งเกินเจ็ดคนหรือรถบรรทุกผู้โดยสารรวมทั้งผู้ขับขี่เกินเจ็ดคน<br/>
                        ต่ออุบัติเหตุแต่ละครั้ง
                    </p>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right no-border-bottom" width="10.3%" style="vertical-align:text-top;"><p class="br-shit">รายการ <br/>Item</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="30%" style="vertical-align:text-top;padding-top:-2pt;"><p class="br-shit" style="line-height: 8pt;">5. จำนวนเงินค่าเสียหายเบื้องต้น :<br/>5. Limit of Preliminary Compenasation</p></td>
                <td class="no-border-left no-border-bottom" style="vertical-align:text-top;padding-top: -3pt;">
                    <p class="br-shit" style="line-height: 8pt;font-size:10pt;">
                        ความเสียหายต่อร่างกาย ไม่เกิน 30,000 บาท ต่อหนึ่งคน หรือตามที่กฎหมายกำหนด<br/>
                        ความเสียหายต่อร่างกาย สำหรับการสูญเสียอวัยวะ หรือทุพพลภาพอย่างถาวร 35,000 บาท หรือตามที่กฎหมายกำหนด<br/>
                        ความเสียหายต่อชีวิต 35,000 บาท ต่อหนึ่งคน หรือตามที่กฎหมายกำหนด<br/>
                    </p>
                </td>
            </tr>
            <tr>
                <td class="no-border-right" style="padding-top: -7pt;padding-bottom:-2pt;">&nbsp;</td>
                <td class="no-border-left" colspan="2" style="padding-top: -7pt;padding-bottom:-2pt;"><p>จำนวนเงินค่าเสียหายเบื้องต้นนี้เป็นส่วนหนึ่งของจำนวนเงินคุ้มครองผู้ประสบภัยตามรายการ 4</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="10.3%" style="vertical-align:text-top;padding-bottom:-2pt;"><p class="br-shit">รายการ <br/>Item</p></td>
                <td class="no-border-right no-border-left" width="30%" style="vertical-align:text-top;padding-top:-2pt;padding-bottom:-2pt;"><p class="br-shit" style="line-height: 8pt;">6. เบี้ยประกันภัย : (บาท)<br/>6. Premium : (Baht)</p></td>
                <td class="no-border-left no-border-right" width="45%" style="padding-top:-4pt;">&nbsp;</td>
                <td class="no-border-left" align="right"><p class="br-shit stamp"><img class="stamp" src="{{ public_path('images/admin/icon/stamp.png') }}"/></p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-bottom:-1pt;padding-top:1pt;" align="center"><p class="br-shit">เบี้ยประกันภัย<br/>Premium</p></td>
                    <td style="padding-bottom:-1pt;padding-top:1pt;" align="center"><p class="br-shit">ส่วนลดจากการประกันภัยโดยตรง<br/>Premium Discounts</p></td>
                    <td style="padding-bottom:-1pt;padding-top:1pt;" align="center"><p class="br-shit">เบี้ยประกันภัยสุทธิ<br/>Net Premium</p></td>
                    <td style="padding-bottom:-1pt;padding-top:1pt;" align="center"><p class="br-shit">อากรแสตมป์<br/>Stamps</p></td>
                    <td style="padding-bottom:-1pt;padding-top:1pt;" align="center"><p class="br-shit">ภาษีมูลค่าเพิ่ม<br/>VAT</p></td>
                    <td style="padding-bottom:-1pt;padding-top:1pt;" align="center"><p class="br-shit">รวมเงิน<br/>Total</p></td>
                </tr>
                <tr>
                    <td align="center" width="8%"><p>{{ number_format($order->compulsory_net_premium + $order->compulsory_stamp + $order->compulsory_vat,2) }} บาท</p></td>
                    <td align="center" width="18%"><p>-</p></td>
                    <td align="center" width="10%"><p>{{ number_format($order->compulsory_net_premium,2) }} บาท</p></td>
                    <td align="center" width="19%"><p>{{ number_format($order->compulsory_stamp,2) }} บาท</p></td>
                    <td align="center" width="10%"><p>{{ number_format($order->compulsory_vat,2) }} บาท</p></td>
                    <td align="center" width="15%"><p>{{ number_format($order->compulsory_net_premium + $order->compulsory_stamp + $order->compulsory_vat,2) }} บาท</p></td>
                </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="10.3%" style="vertical-align:text-top;"><p class="br-shit">รายการ <br/>Item</p></td>
                <td class="no-border-left no-border-right" width="15%"  style="vertical-align:text-top;padding-top:-2pt;"><p class="br-shit" style="line-height: 8pt;">7. การใช้รถยนต์ :<br/>7. Use of Motor Vehicle</p></td>            
                <td class="no-border-left"><p class="br-shit">
                    @if($vehicle->mortor_code_av != 320)
                    <p>ใช้เป็นรถยนต์ส่วนบุคคล ไม่ใช้รับจ้างหรือให้เช่า</p>
                    @else
                    <p>ใช้เพื่อการพาณิชย์ไม่ใช้เพื่อการบรรทุกและขนส่งสินค้าที่มีความเสี่ยงภัยสูง เช่น เชื้อเพลิง กรด แก๊ส</p>
                    @endif
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding-bottom:0pt;padding-top:2pt;" class="" width="27%">
                    <div class="checkbox checked"><img src="{{ public_path('images/web/icon/x-bar.png') }}" width="12pt" /></div><p class="label-check br-shit">การประกันภัยโดยตรง Direct Insurance</p>
                </td>
                <td style="padding-bottom:0pt;padding-top:2pt;" class="no-border-left " width="22%">
                    <div class="checkbox"></div><p class="label-check br-shit">ตัวแทนประกันภัยรายนี้ Agent</p>
                </td>
                <td style="padding-bottom:0pt;padding-top:2pt;" class="no-border-left " align="left" >
                    <div class="checkbox"></div><p class="label-check br-shit">นายหน้าประกันภัยรายนี้ Broker</p>
                </td>
                <td class="" align="left" width="27%">
                    <p class="br-shit">ใบอนุญาตเลขที่ Licence No. &nbsp;&nbsp;&nbsp;</p>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="20%">
                    <p class="br-shit" >วันทำสัญญาประกันภัย: <br/>Agreement made on</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="20%">
                    <p style="padding-top: -3pt;">{{ getLocaleDate($payment->paid_date) }}</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="left" width="30%">
                    <p class="br-shit">วันทำกรมธรรม์ประกันภัย: <br/>Policy Issue on</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="left" width="20%">
                    <p style="padding-top: -3pt;">{{ getLocaleDate($payment->paid_date) }}</p>
                </td>
                <td class="no-border-left no-border-bottom no-border-right" align="left">
                    <p>&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" colspan="5">
                    <p class="br-shit">เพื่อเป็นหลักฐาน บริษัท โดยบุคคลผู้มีอำนาจได้ลงลายมือชื่อและประทับตราของบริษัทไว้เป็นสำคัญ ณ สำนักงานของบริษัท<br/>
                    As evidence the Company has caused this Policy to be signed by duly authorized persons and the Company's stamp to be affixed at its Office</p>
                </td>
            </tr>
        </table>
    </div>
    <div class="foot" style="padding-top:10pt;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ $sign['director_1'] }}"/>
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ $sign['director_2'] }}"/>
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="center">
                    <div class="sign">
                        <img src="{{ $sign['authorized'] }}"/>
                    </div>
                    <div class="sign-detail">
                        <p>ผู้รับมอบอำนาจ Authorized Signature / ผู้รับเงิน Cashier</p>
                        <P class="sign-date">{{ getLocaleDate($payment->paid_date) }}</P>
                    </div>
                </td>
            </tr>
        </table>
        <div class="company-stamp">
            <img src="{{ public_path('images/company/stamp_footer/stamp-image.png') }}" />
        </div>
    </div>
    

    <div class="prb-foot">
        <div class="hr-line"></div>
        <div class="text-header">หลักฐานแสดงการประกันภัยตามพระราชบัญญัติคุ้มครองผู้ประสบภัยจากรถ<br/>เพื่อใช้สำหรับการจดทะเบียนรถใหม่หรือขอเสียภาษีประจำปีต่อนายทะเบียนขนส่ง</div>
        <!-- <div class="barcode-text">
            {{ $sticker_no }}
        </div> -->
        <div class="barcode-text-bottom">
            {{ $sticker_no }}
        </div>
        <div class="barcode">
            {!! '<img width="220pt" src="data:image/png;base64,' . DNS1D::getBarcodePNG($sticker_no, "C128",3,50) . '" />' !!}
        </div>

        <div class="detail">
            <p style="padding-left: 30pt">
                เอกสารนี้ให้ไว้เพื่อแสดงว่า รถหมายเลขทะเบียนที่
                <span class="wrap-dotted" style="width:155pt;height:20pt;text-align:center;"><div class="dotted" style="text-align:center;">{{ str_replace(' ','-',$init->car_licence) }} {{ $init->car_province()->first()->name_th }}</div></span>
                ตัวถังรถเลขที่
                <span class="wrap-dotted" style="width:172pt;height:20pt;text-align:center;"><div class="dotted" style="text-align:center;">{{ $init->car_chassis_number }}</div></span>
            </p>
            <p>ได้ทำประกันภัยตามพระราชบัญญัติคุ้มครองผู้ประสบภัยจากรถ พ.ศ.2535 แล้ว โดยมีระยะเวลาประกันภัย</p>
            <p style="padding-left: 0pt">
                เริ่มต้นวันที่
                <span class="wrap-dotted" style="width:240pt;height:20pt;text-align:center;"><div class="dotted" style="text-align:center;">{{ getLocaleDate($init->compulsory_start) }}</div></span>
                ถึงวันที่
                <span class="wrap-dotted" style="width:240pt;height:20pt;text-align:center;"><div class="dotted" style="text-align:center;">{{ getLocaleDate($init->compulsory_expire) }}</div></span>
            </p>
            <p style="padding-left: 0pt">
                ตามกรมธรรม์ประกันภัยเลขที่
                <span class="wrap-dotted" style="width:183pt;height:20pt;text-align:center;"><div class="dotted" style="text-align:center;">{{ $init->policy_com_number }}</div></span>
                ของบริษัท
                <span class="wrap-dotted" style="width:238pt;height:20pt;text-align:center;"><div class="dotted" style="text-align:center;">ซมโปะ ประกันภัย (ประเทศไทย) จำกัด (มหาชน)</div></span>
            </p>
        </div>

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ $sign['director_1'] }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ $sign['director_2'] }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="center">
                    <div class="sign">
                        <img src="{{ $sign['authorized'] }}" />
                    </div>
                    <div class="sign-detail">
                        <p>ผู้รับมอบอำนาจ Authorized Signature / ผู้รับเงิน Cashier</p>
                        <P class="sign-date" >{{ getLocaleDate($payment->paid_date) }}</P>
                    </div>
                </td>
            </tr>
        </table>
        <div class="company-stamp" style="top: 100pt">
            <img src="{{ public_path('images/company/stamp_footer/stamp-image.png') }}" />
        </div>
    </div>

    </body>
    </html>
    
    

    