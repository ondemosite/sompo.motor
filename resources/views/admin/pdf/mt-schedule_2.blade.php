    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        @page {
            margin-top: 0.13in;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        @font-face {
            font-family: 'AngsanaNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/AngsanaNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'AngsanaNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/AngsanaNewBold.ttf') }}") format('truetype');
        }
        body{
            font-family: 'AngsanaNew';
            font-size:12pt;
            width:100%;
        }
        .page-break {
            page-break-after: always;
        }

        /*  Content */
        .h-padding{
            padding:0pt 20pt;
        }
        .g-padding{
            padding:0px 5pt;
        }
        .no-border-left{
            border-left:0pt !important;
        }
        .no-border-right{
            border-right:0pt !important;
        }
        .no-border-bottom{
            border-bottom:0pt !important;
        }
        .no-border-top{
            border-top:0pt !important;
        }
        .border-top{
            border-top:0.1pt solid #000000 !important;
        }
        .large-text{
            font-weight:bold;
            font-size:36pt;
        }
        .header-text{
            font-weight:bold;
            font-size:15pt;
        }
        .line-br{
            padding-top:2pt;
            padding-bottom:2pt;
            line-height:9pt;
        }
        .text-area{
            height:30pt;
            vertical-align:text-top;
        }
        .head{
            margin-left:-5pt;
        }
        .head .barcode{
            position: absolute;
            top: 16pt;
            right: 0pt;
        }
        .head .barcode-text{
            position: absolute;
            top: -5pt;
            right: 0pt;
            letter-spacing: 1pt;
            font-size: 14pt;
        }
        .head .barcode-text-bottom{
            position: absolute;
            top: 34pt;
            right: 16pt;
            letter-spacing: 6pt;
            font-size: 11pt;
        }
        /* element */
        p{
            padding:0pt;
            margin:0pt;
            font-size:12pt;
        }
        h1{
            padding:0px;
            margin:0px;
        }
        td,th {
            border:0.1pt solid #000000;
            border-top: 0px;
            padding:0pt 5pt;
            font-weight:100;
        }
        td > *,th > *{
            margin-top:-2pt;
            padding-bottom:1pt;
        }
        table.sub-table{
            padding-top:6pt;
        }
        table.sub-table td{
            border:0px;
            padding-left:0px;
        }
        .checkbox {
            position:absolute;
            width:9pt;
            height:8pt;
            border: 1px solid #000;
            margin-top:5pt;
        }
        .label-check{
            margin-left:15pt;
            line-height:13pt;
        }
        .text-underline{
            border-bottom:1px solid;
        }
        
        /* This is what simulates a checkmark icon */
        .checkbox.checked:after {
            content: '';
            display: block;
            width: 3pt;
            height: 7pt;
            
            /* "Center" the checkmark */
            position:relative;
            top:-1pt;
            left:3pt;
            
            border: solid #000;
            border-width: 0 1.5pt 1.5pt 0;
            transform: rotate(45deg);
        }
        /* Content */
        .stamp{
            padding-top:3pt;
            width:70pt;
        }
        div.alert-text{
            position:absolute;
            width:100%;
            text-align:center;
            top: 225px;
        }
        .sign{
            position:relative;
            width:120pt;
            border-bottom:1pt dashed #000000;
            margin:0 auto;
        }
        .sign-detail{
            text-align:center;
        }
        .sign > img{
            text-align:center;
            height:35pt;
        }
        .company{
            height: 90pt;
        }
        .company-stamp{
            position:absolute;
            margin-top:-40pt;
            top:-15pt;
            margin-left:90pt;
        }
        .company-stamp > img{
            width:219pt;
        }
        .water-mask{
            position: absolute;
            top:0px;
            left: 0px;
            width: 21cm;
            height: 29.7cm;
            text-align: center;
        }
        .water-mask img{
            width: 100%;
        }
        </style>
    </head>
    <body>
    @php
        $order = $init->order()->first();
        $payment = $init->payment()->first();
        $vehicle = $init->vehicle_info()->first();
        $endorse = $init->endorse()->first();
        $driver = $endorse->owner()->first();
        $driver1 = $endorse->driver1()->first();
        $driver2 = $endorse->driver2()->first();
        
        $province = $driver->province()->first();
        $district = $driver->district()->first();
        $subdistrict = $driver->subdistrict()->first();
        $province_prefix = trans('step5.province_prefix');
        $district_prefix = ($province->id=="00"?trans('step5.district_prefix_c'):trans('step5.district_prefix'));
        $subdistrict_prefix = ($province->id=="00"?trans('step5.subdistrict_prefix_c'):trans('step5.subdistrict_prefix'));
        $full_address = $driver->address." ".$subdistrict_prefix.$subdistrict->name_th." ".$district_prefix.$district->name_th." ".$province_prefix.$province->name_th." ".$driver->postalcode;
        $sumNet = $order->basic_premium_cover + $order->additional_premium_cover + $order->theft_net_premium
                  + $order->taxi_net_premium + $order->hb_net_premium + $order->carloss_net_premium;

        $signFolder = "7-nov-19";
        if (strtotime($order->created_at->toDateTimeString()) >= strtotime('2019-11-14 00:00:00')) {
           $signFolder = "14-nov-19";
        }   
        $sign = [
            'director_1' => public_path('images/company/'.$signFolder."/director_1/sign-image.png"),
            'director_2' => public_path('images/company/'.$signFolder."/director_2/sign-image.png"),
            'authorized' => public_path('images/company/'.$signFolder."/authorized/sign-image.png")
        ];
    
    @endphp

    @if(!empty($copy))
        @if($copy)
        <div class="water-mask">
             <img src="{{ public_path('images/admin/background/watermask.png') }}" />
        </div>
        @endif
    @endif
    
    <div class="head">
        <div class="company">
            @if(empty($clean))
            <img src="{{ public_path('images/company/stamp_header/stamp-image.png') }}" width="435pt" />
            @endif
        </div>
        <div class="barcode">
            {!! '<img width="220pt" src="data:image/png;base64,' . DNS1D::getBarcodePNG($init->policy_number, "C128",3,150) . '" />' !!}
        </div>
    </div>
    <div class="body">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="border-top h-padding" width="100%" align="center"><h1 class="header-text">ตารางกรมธรรม์ประกันภัยรถยนต์แบบคุ้มครองเฉพาะภัย SOMPO MOTOR PLUS (ขายผ่านทางอิเล็กทรอนิกส์ (Online))</h1></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="11%"><p>รหัสบริษัท : SIT</p></td>
                <td class="no-border-right no-border-left" width="10%"><p>กรมธรรม์ประกันภัยเลขที่</p></td>
                <td class="no-border-right no-border-left" width="18%"><p>{{ $init->policy_number }}</p></td>
                <td class="no-border-right no-border-left" width="22%" align="right"><p>อาณาเขตคุ้มครอง</p></td>
                <td class="no-border-left" width="9%" align="left"><p>ประเทศไทย</p></td>
            </tr>
            <tr>
                <td class="no-border-right no-border-bottom" ><p>ผู้เอาประกันภัย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ชื่อ</p></td>
                <td class="no-border-right no-border-left no-border-bottom" colspan="2"><p>{{ $driver->prefix_name.$driver->name." ".$driver->lastname }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" align="left"><p>อาชีพ &nbsp;&nbsp;&nbsp;-</p></td>
                <td class="no-border-left no-border-bottom" align="left"><p></p></td>
            </tr>
            <tr>
                <td class="no-border-right text-area" align="right"><p>ที่อยู่</p></td>
                <td class="no-border-left no-border-right text-area" valign="top" colspan="2" style="line-height:10pt;padding-top:5pt;"><p>{{ $full_address }}</p></td>
                <td class="no-border-left no-border-right">&nbsp;</td>
                <td class="no-border-left">&nbsp;</td>
            </tr>
        </table>


        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right no-border-bottom" width="11%"><p>ผู้ขับขี่ 1</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="36%"><p>{{ !empty($driver1)?($driver1->name." ".$driver1->lastname):'-' }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="10%" align="right"><p>วัน/เดือน/ปีเกิด</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="18%"><p>{{ !empty($driver1)?(getLocaleDate($driver1->birth_date)):'-'  }}</p></td>
                <td class="no-border-right no-border-left no-border-bottom" width="7%" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left no-border-bottom" align="left" width="18%"><p>-</p></td>
            </tr>
            <tr>
                <td class="no-border-right " width="11%"><p>ผู้ขับขี่ 2</p></td>
                <td class="no-border-right no-border-left " width="36%"><p>{{ !empty($driver2)?($driver2->name." ".$driver2->lastname):'-' }}</p></td>
                <td class="no-border-right no-border-left " width="10%" align="right"><p>วัน/เดือน/ปีเกิด</p></td>
                <td class="no-border-right no-border-left " width="18%"><p>{{ !empty($driver2)?(getLocaleDate($driver2->birth_date)):'-'  }}</p></td>
                <td class="no-border-right no-border-left " width="7%" align="right"><p>อาชีพ</p></td>
                <td class="no-border-left " align="left" width="18%"><p>-</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right no-border-bottom" width="8.7%" align="left"><p>ผู้รับประโยชน์</p></td>
                <td class="no-border-left no-border-right no-border-bottom" width="50%" align="left"><p>-</p></td>
                <td class="no-border-left no-border-bottom" width="10%" style="padding-right:0px;"><img class="stamp" src="{{ public_path('images/admin/icon/stamp.png') }}"/></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" witdh="22%" style="padding-top:-5pt;"><p>ระยะเวลาประกันภัย : เริ่มต้นวันที่</p></td>
                <td class="no-border-left no-border-right" align="left" width="18%" style="padding-top:-5pt;"><p>{{ getLocaleDate($init->insurance_start) }}</p></td>
                <td class="no-border-left no-border-right" width="11%" style="padding-top:-5pt;"><p>เวลา {{ date('H:i',strtotime($init->insurance_start)) }} น.</p></td>
                <td class="no-border-left no-border-right" align="right" width="19%" style="padding-top:-5pt;"><p>สิ้นสุดวันที่</p></td>
                <td class="no-border-left no-border-right" width="17%" style="padding-top:-5pt;"><p>{{ getLocaleDate($init->insurance_expire) }}</p></td>
                <td class="no-border-left" align="left" width="13%" style="padding-top:-5pt;"><p>เวลา {{ date('H:i',strtotime($init->insurance_expire)) }} น.</p></td>
            </tr>
            <tr>
                <td colspan="6" style="padding-top:0pt;"><p>รายการรถยนต์ที่เอาประกันภัย</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ลำดับ</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">รหัส</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ชื่อรถยนต์/รุ่น</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">เลขทะเบียน</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">เลขตัวถัง</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">ปีรุ่น</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">แบบตัวถัง</td>
                    <td style="padding-top:-3pt;padding-bottom:1pt;" align="center">จำนวนที่นั่ง/ขนาด/น้ำหนัก</td>
                </tr>
                <tr>
                    <td align="center" width="5%"><p>001</p></td>
                    <td align="center" width="5%"><p>{{ $vehicle->mortor_code_av }}</p></td>
                    <td class="line-br" align="center" width="16%"><p>{{ strtoupper($vehicle->brand." ".$vehicle->model) }}</p></td>
                    <td class="line-br" align="center" width="10%"><p>{{ str_replace(' ','-',$init->car_licence) }}<br/>{{ $init->car_province()->first()->name_th }}</p></td>
                    <td align="center" width="19%"><p>{{ $init->car_chassis_number }}</p></td>
                    <td align="center" width="5%"><p>{{ $vehicle->year }}</p></td>
                    <td align="center" width="10%"><p>{{ $vehicle->body_type }}</p></td>
                    <td align="center" width="15%">
                    <p>
                        @if($vehicle->mortor_code_av == 110)
                            {{ $vehicle->car_seat."/".number_format($vehicle->cc,0) }}cc/- 
                        @else
                            {{ $vehicle->car_seat."/-/"."ไม่เกิน 3 ตัน" }}
                        @endif
                    </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <p>จำนวนเงินเอาประกันภัย: กรมธรรม์ประกันภัยนี้ให้การคุ้มครองเฉพาะข้อตกลงคุ้มครองที่มีจำนวนเงินเอาประกันภัยระบุไว้เท่านั้น</p>
                    </td>
                </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" width="20%"><p>ความคุ้มครองหลัก (1)</p></td>
                <td class="line-br" width="30.5%" align="center"><p>ความคุ้มครองความเสียหายต่อตัวรถยนต์ตาม<br/>เอกสารแนบท้ายแบบคุ้มครองเฉพาะภัย (2)</p></td>
                <td class="line-br" width="39%" align="center"><p>ความคุ้มครองอื่นๆ ตามเอกสารแนบท้าย<br/>และเอกสารแนบท้ายแบบคุ้มครองเฉพาะภัย (3)</p></td>
            </tr>
            <tr>
                <td class="line-br" align="left" style="padding-top:-10pt">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left"><p>ความคุ้มครองความรับผิดต่อบุคคลภายนอก</p></td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding-top:1.4pt"><p>1.1)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ.</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_person_damage_person,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คน</p></td>
                        </tr>
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_person_damage_once,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1.2)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายต่อทรัพย์สิน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_stuff_damage,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1.2)</p></td>
                            <td valign="top" align="left"><p>ความเสียหายส่วนแรก</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_deduct,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>2)</p></td>
                            <td valign="top" align="left"><p>ความคุ้มครองความเสียหายต่อตัวรถยนต์</p></td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding-top:0pt"><p>2.1)</p></td>
                            <td valign="top" align="left"><p>รถยนต์สูญหาย/ไฟไหม้</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            @if($init->insurance_plan_id==1)
                                <td width="80%" align="right"><p>{{number_format($init->insurance_ft_si,2)}}</p></td>
                            @else
                                <td width="80%" align="right"><p>-</p></td>
                            @endif
                            <td width="20%" align="right"><p>บาท</p></td>
                        </tr>
                    </table>
                </td>
                <td class="line-br" valign="top">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left" style="padding-top:-3pt;"><p>ความคุ้มครองความเสียหายต่อตัวรถยนต์ เนื่องจากการชนกับยานพาหนะทางบก</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_ft_si,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>&nbsp;</p></td>
                            <td valign="top" align="left"><p>ความเสียหายส่วนแรกกรณีเป็นฝ่ายผิด</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80%" align="right"><p>{{number_format($init->insurance_deduct,2)}}</p></td>
                            <td width="20%" align="right"><p>บาท/คร้ัง</p></td>
                        </tr>
                    </table>
                    <div class="alert-text">
                        <h1 class="large-text">ไม่รวม พ.ร.บ.</h1>
                    </div>
                </td>
                <td class="line-br" align="center" width="25%" valign="top" style="height:250px;position:relative;">
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="5%"><p>1)</p></td>
                            <td valign="top" align="left" style="padding-left:-3pt;"><p>อุบัติเหตุส่วนบุคคล</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="6%"><p></p></td>
                            <td style="padding-top:-3pt;padding-left:-3pt;" colspan="3"><p>1.1)&nbsp;เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร</p></td>
                            <!-- <td style="padding-top:-5pt" colspan="2"></td> -->
                        </tr>
                        <tr>
                            <td width="6%" style="padding-left:-5pt;"><p>&nbsp;</p></td>
                            <td width="40%" align="left" style="padding-top:2pt;padding-left:-7pt;"><p>ก)&nbsp;ผู้ขับขี่ 1 คน</p></td>
                            <td width="35%" align="left" style="padding-top:2pt;padding-left:1pt;"><p>{{number_format($init->insurance_death_disabled,2)}}</p></td>
                            <td width="15%" align="left" style="padding-top:2pt;padding-left:0pt;"><p>บาท</p></td>
                        </tr>
                        <tr>
                            <td width="6%" style="padding-left:-5pt;"><p>&nbsp;</p></td>
                            <td width="40%" align="left" style="padding-top:2pt;padding-left:-7pt;"><p>ข)&nbsp;ผู้โดยสาร 4 คน</p></td>
                            <td width="35%" align="left" style="padding-top:2pt;padding-left:1pt;"><p>{{number_format($init->insurance_death_disabled,2)}}</p></td>
                            <td width="15%" align="left" style="padding-top:2pt;padding-left:0pt;"><p>บาท/คน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="6%"><p></p></td>
                            <td style="padding-top:-3pt;padding-left:-3pt;" colspan="3"><p>1.2)&nbsp;ทุพพลภาพชั่วคราว</p></td>
                            <!-- <td style="padding-top:-5pt" colspan="2"></td> -->
                        </tr>
                        <tr>
                            <td width="6%" style="padding-left:-5pt;"><p>&nbsp;</p></td>
                            <td width="40%" align="left" style="padding-top:2pt;padding-left:-7pt;"><p>ก)&nbsp;ผู้ขับขี่ 1 คน</p></td>
                            <td width="21%" align="right" style="padding-top:2pt;padding-left:1pt;"><p>0.00</p></td>
                            <td width="29%" align="right" style="padding-top:2pt;padding-left:0pt;padding-right:0pt;"><p>บาท/สัปดาห์</p></td>
                        </tr>
                        <tr>
                            <td width="6%" style="padding-left:-5pt;"><p>&nbsp;</p></td>
                            <td width="40%" align="left" style="padding-top:2pt;padding-left:-7pt;"><p>ข)&nbsp;ผู้โดยสาร - คน</p></td>
                            <td width="21%" align="right" style="padding-top:2pt;padding-left:1pt;"><p>0.00</p></td>
                            <td width="29%" align="right" style="padding-top:2pt;padding-left:0pt;padding-right:0pt;"><p>บาท/คน/สัปดาห์</p></td>
                        </tr>
                    </table>

                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="5%" align="left" ><p>2)</p></td>
                            <td width="31%" style="padding-left:-3pt;"><p>ค่ารักษาพยาบาล</p></td>
                            <td width="30%" align="right"><p>{{number_format($init->insurance_medical_fee,2)}}</p></td>
                            <td width="30%" align="right" style="padding-right:0px;"><p>บาท/คน</p></td>
                        </tr>
                    </table>
                    <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="5%" align="left" style="padding-top:-1pt"><p>3) </p></td>
                            <td width="31%" style="padding-top:-1pt;padding-left: -3pt;"><p>การประกันตัวผู้ขับขี่</p></td>
                            <td width="30%" align="right" style="padding-top:-1pt"><p>{{number_format($init->insurance_bail_driver,2)}}</p></td>
                            <td width="45%" align="right" style="padding-right:0px;padding-top:-1pt"><p>บาท/ครั้ง</p></td>
                        </tr>
                    </table>
                    <!-- <table class="sub-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="5%" align="left" style="padding-top: -10pt;"><p>4) </p></td>
                            <td width="45%" style="padding-top: 1pt;"><p>ความคุ้มครองเพิ่มเติมตามเอกสารแนบ<br/>(กรณีเลือกซื้อเพิ่ม)</p></td>
                        </tr>
                    </table> -->
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"><p>เบี้ยประกันภัย</p></td>
            </tr>
            <tr>
                <td class="no-border-right" width="46.5%"><p>เบี้ยประกันภัยตามความคุ้มครอง (1) และ (3)</p></td>
                <td class="no-border-left no-border-right" align="right" width="10%"><p>{{ number_format($sumNet,2) }}</p></td>
                <td class="no-border-left" align="left"><p>บาท (เบี้ยประกันภัยนี้ได้หักส่วนลดกรณีระบุชื่อผู้ขับขี่แล้ว)</p></td>
            </tr>
        </table>
        <table class="unknow" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="18%" class="no-border-bottom"><p>ส่วนลด/ส่วนเพิ่มเติม</p></td>
                <td class="no-border-left no-border-right no-border-bottom" width="10%"><p>ความเสียหายส่วนแรก</p></td>
                <td class="no-border-left no-border-right no-border-bottom" width="11%" align="center"><p>{{number_format($order->deduct,2)}} บาท</p></td>
                <td class="no-border-left no-border-right no-border-bottom" width="8%"><p>ส่วนลดกลุ่ม</p></td>
                <td class="no-border-left no-border-right no-border-bottom" width="8%" align="center"><p>{{number_format($order->fleet,2)}} บาท</p></td>
                <td class="no-border-left no-border-right no-border-bottom" width="5%"><p>ประวัติดี</p></td>
                <td class="no-border-left no-border-right no-border-bottom" width="12%" align="center"><p>{{number_format($order->ncb,2)}} บาท</p></td>
                <td class="no-border-left no-border-bottom"><p>รวมส่วนลด -{{ number_format(abs($order->deduct)+abs($order->fleet)+abs($order->ncb),2) }} บาท</p></td>
            </tr>
            <tr>
                <td class="no-border-right" width="18%"><p>ความคุ้มครอง (1) และ (3)</p></td>
                <td colspan="7" align="left"><p>ประวัติเพิ่ม 0.00 บาท</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right" width="23%"><p>เบี้ยประกันภัยตามความคุ้มครอง (2)</p></td>
                <td class="no-border-right no-border-left" width="14.7%" align="right"><p>{{number_format($order->od_total_premium,2)}}</p></td>
                <td class="no-border-left"><p>บาท</p></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-right"><p>ส่วนลดอื่นๆ</p></td>
                <td class="no-border-right no-border-left" align="right"><p>-{{number_format(abs($order->cctv_discount)+abs($order->direct)+abs($order->net_discount),2)}}</p></td>
                <td class="no-border-left no-border-right" align="left"><p>บาท</p></td>
                <td class="no-border-left no-border-right" align="left"></td>
                <td class="no-border-left no-border-right" align="left"></td>
                <td class="no-border-left " align="left" colspan="4" style="padding-top:-6pt;"><span style="text-decoration:underline;">หมายเหตุ</span></td>
            </tr>
            <tr>
                <td class="no-border-right"><p>เบี้ยประกันภัยสุทธิ</p></td>
                <td class="no-border-right no-border-left" align="right"><p>{{number_format($order->insurance_net_premium,2)}}</p></td>
                <td class="no-border-left" align="left"><p>บาท</p></td>
                <td class="no-border-left no-border-right" align="left"><p>อากรแสตมป์</p></td>
                <td class="no-border-left" align="right"><p>{{number_format($order->stamp,2)}} บาท</p></td>
                <td class="no-border-left no-border-right" align="left"><p>ภาษีมูลค่าเพิ่ม</p></td>
                <td class="no-border-left" align="right"><p>{{number_format($order->vat,2)}} บาท</p></td>
                <td class="no-border-left no-border-right" align="left"><p>รวม</p></td>
                <td class="no-border-left" align="right"><p>{{number_format($order->insurance_net_premium+$order->stamp+$order->vat,2)}} บาท</p></td>
            </tr>
            <tr>
                <td colspan="9">
                    @if($vehicle->mortor_code_av != 320)
                    <p>การใช้รถยนต์ : ใช้เป็นรถยนต์ส่วนบุคคล ไม่ใช้รับจ้างหรือให้เช่า</p>
                    @else
                    <p>การใช้รถยนต์ : ใช้เพื่อการพาณิชย์ไม่ใช้เพื่อการบรรทุกและขนส่งสินค้าที่มีความเสี่ยงภัยสูง เช่น เชื้อเพลิง กรด แก๊ส</p>
                    @endif
                </td>
            </tr>
            <!--
            <tr>
                <td class="no-border-bottom" colspan="9"><p>SOMPO BROKERS (THAILAND) CO., LTD</p></td>
            </tr>-->
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding-bottom:2pt;" class="no-border-right" width="20%">
                    <div class="checkbox"><img src="{{ public_path('images/web/icon/x-bar.png') }}" width="12pt" /></div><span class="label-check">ประกันภัยโดยตรง</span>
                </td>
                <td style="padding-bottom:2pt;" class="no-border-left no-border-right" width="20%">
                    <div class="checkbox"></div><span class="label-check">ตัวแทนประกันภัยรายนี้</span>
                </td>
                <td style="padding-bottom:2pt;" class="no-border-left no-border-right" align="left" width="30%">
                    <div class="checkbox"></div><span class="label-check">นายหน้าประกันภัยรายนี้</span>
                </td>
                <td style="padding-bottom:0pt;" class="no-border-left no-border-right" align="left" width="12%">
                    <p>ใบอนุญาตเลขที่</p>
                </td>
                <td style="padding-bottom:0pt;" class="no-border-left" align="left">
                    <p>&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="20%">
                    <p>วันทำสัญญาประกันภัย</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="20%">
                    <p>{{ getLocaleDate($payment->paid_date) }}</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="left" width="30%">
                    <p>วันทำกรมธรรม์ประกันภัย</p>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="left" width="20%">
                    <p>{{ getLocaleDate($payment->paid_date) }}</p>
                </td>
                <td class="no-border-left no-border-bottom no-border-right" align="left">
                    <p>&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" colspan="5">
                    <p>เพื่อเป็นหลักฐาน บริษัท โดยบุคคลผู้มีอำนาจได้ลงลายมือชื่อและประทับตราของบริษัทไว้เป็นสำคัญ ณ สำนักงานของบริษัท</p>
                </td>
            </tr>
        </table>
    </div>

    <div class="foot" style="padding-top:10pt;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ $sign['director_1'] }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" width="33%" align="center">
                    <div class="sign">
                        <img src="{{ $sign['director_2'] }}" />
                    </div>
                    <div class="sign-detail">
                        <p>กรรมการ Director</p>
                    </div>
                </td>
                <td class="no-border-left no-border-right no-border-bottom" align="center">
                    <div class="sign">
                        <img src="{{ $sign['authorized'] }}" />
                    </div>
                    <div class="sign-detail">
                        <p>ผู้รับมอบอำนาจ Authorized Signature</p>
                    </div>
                </td>
            </tr>
        </table>
        <div class="company-stamp">
            <img src="{{ public_path('images/company/stamp_footer/stamp-image.png') }}" />
        </div>
    </div>

    </body>
    </html>
    
    

    