@extends('admin.layout.default')
@section('title') Admins @endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/compile.css') !!}">
@endsection

@section('content')
<div class="page-content">
    <!-- <i class="fa fa-check text-success p-r-1"></i>
    <i class="fa fa-close text-danger p-r-1"></i> -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-wrench"></i>Compile</span>
                </div>
                <div class="panel-body">
                    <div class="row wrapSections wrapTransactions">
                        <div class="col-xs-12">
                            <form>
                            <fieldset>
                                <legend>Processing Logs</legend>
                                <div id="genarateTransactionLogs">
                                    <table width="70%">
                                        <tr>
                                            <td>Order No.</td>
                                            <td>Process</td>
                                            <td>Status</td>
                                            <td>Action Date</td>
                                        </tr>
                                        
                                    </table>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>

                    <!-- ORDER PROCESS -->
                    <div class="row wrapSections wrapOrder">
                        <div class="col-xs-12">
                            <form>
                            <fieldset>
                                <legend>Update Order Status: <span id="order_status">
                                    @if($init['orders']['status'] != "PAID")
                                    <i class="fa fa-close text-danger p-r-1"></i>
                                    @else
                                    <i class="fa fa-check text-success p-r-1"></i>
                                    @endif
                                </span></legend>
                                <div>
                                    <div class="row">
                                        @if($init['orders']['status'] != "PAID")
                                        <div class="col-md-2">Order Status: <span class="text-danger">{{ $init['orders']['status'] }}</span></div>
                                        @else
                                        <div class="col-md-2">Order Status: <span class="text-success">{{ $init['orders']['status'] }}</span></div>
                                        @endif
                                    </div>
                                    @if($init['orders']['status'] != "PAID")
                                    <div class="row">
                                        <div class="col-md-2"><button class="btn btn-primary btn-process" id="update_order">UPDATE TO PAID</button><span class="button-text-state" id="state_order">&nbsp;&nbsp;กำลังดำเนินการ...</span></div>
                                    </div>
                                    @endif
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>

                    <!-- PAYMENT PROCESS -->
                    <div class="row wrapSections wrapPayment">
                        <div class="col-xs-12">
                            <form class="form-inline">
                            <fieldset>
                                <legend>Update Payment Status: <span id="payment_status">
                                    @if($init['payment']['status'] != "PAID")
                                    <i class="fa fa-close text-danger p-r-1"></i>
                                    @else
                                    <i class="fa fa-check text-success p-r-1"></i>
                                    @endif
                                </span></legend>
                                <div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        @if($init['payment']['status'] != "PAID")
                                        <div class="col-md-2">Payment Status: <span class="text-danger">{{ $init['payment']['status'] }}</span></div>
                                        @else
                                        <div class="col-md-2">Payment Status: <span class="text-success">{{ $init['payment']['status'] }}</span></div>
                                        @endif
                                    </div>
                                    @if($init['payment']['status'] != "PAID")
                                    <div class="row">
                                        <div class="col-sm-12"><label>Update</label></div>
                                        <div class="col-sm-12"><label class="label-form">วันที่ชำระเงิน (มีผลในการออกกรมธรรม์)</label></div>
                                        <div class="col-sm-12 form-group form-inline">
                                            
                                            {{ Form::text('paid_date','', ['class' => 'form-control datepicker','placeholder' => 'ระบุวันที่ชำระเงิน']) }}
                                            <button class="btn btn-primary btn-process" id="update_payment">UPDATE TO PAID</button><span class="button-text-state" id="state_payment">&nbsp;&nbsp;กำลังดำเนินการ...</span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>


                    <!-- POLICY -->
                    <div class="row wrapSections wrapPolicy">
                        <div class="col-xs-12">
                            <form>
                                <fieldset>
                                    <legend>Create Policy: <span id="policy_status">
                                        @if(!empty($init['policy']))
                                        <i class="fa fa-check text-success p-r-1"></i>
                                        @else
                                        <i class="fa fa-close text-danger p-r-1"></i>
                                        @endif
                                    </span></legend>
                                    <div>
                                        <div class="row">
                                            @if(!empty($init['policy']))
                                            <div class="col-sm-12">Policy has been created: <span class="text-success">YES</span></div>
                                            <div class="col-sm-12">Policy Number: <a href="{{ route('admin.get_policy_data').'/'.$init['policy']['id'] }}">{{ $init['policy']['policy_number'] }}</a></div>
                                            @else
                                            <div class="col-sm-12">Policy has been created: <span class="text-danger">NOT YET</span></div>
                                            @endif
                                        </div>
                                        @if(empty($init['policy']))
                                        <div class="row">
                                            <div class="col-md-2"><button class="btn btn-primary btn-process" id="create_policy">CREATE POLICY</button><span class="button-text-state" id="state_policy">&nbsp;&nbsp;กำลังดำเนินการ...</span></div>
                                        </div>
                                        @endif
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>

                    <!-- RECEIPT PROCESS -->
                    <div class="row wrapSections wrapReceipt">
                        <div class="col-xs-12">
                            <form>
                            <fieldset>
                                <legend>Create Receipt: <span id="receipt_status">
                                    @if(!empty($init['receipt']))
                                    <i class="fa fa-check text-success p-r-1"></i>
                                    @else
                                    <i class="fa fa-close text-danger p-r-1"></i>
                                    @endif
                                </span></legend>
                                <div>
                                    <div class="row">
                                        @if(!empty($init['receipt']))
                                        <div class="col-sm-12">Receipt has been created: <span class="text-success">YES</span></div>
                                        <div class="col-sm-12">Receipt Number: {{ $init['receipt']['receipt_number'] }}</div>
                                        @else
                                        <div class="col-sm-12">Receipt has been created: <span class="text-danger">NOT YET</span></div>
                                        @endif
                                    </div>
                                    @if(empty($init['receipt']))
                                    <div class="row">
                                        <div class="col-sm-12"><label>Create</label></div>
                                        <div class="col-sm-12"><label class="label-form">ยอดที่ชำระ (ถ้าไม่ระบุจะอ้างอิงตาม Payment)</label></div>
                                        <div class="col-sm-12 form-group form-inline">
                                            {{ Form::number('receipt_amount','', ['class' => 'form-control','placeholder' => '']) }}
                                        </div>
                                        <div class="col-sm-12"><label class="label-form">หมายเลขบัตรเครดิต (ไม่จำเป็นต้องระบุ)</label></div>
                                        <div class="col-sm-12 form-group form-inline">
                                            {{ Form::text('masked_pan','', ['class' => 'form-control','placeholder' => '']) }}
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="btn btn-primary btn-process" id="create_receipt">CREATE RECEIPT</button><span class="button-text-state" id="state_receipt">&nbsp;&nbsp;กำลังดำเนินการ...</span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>


                    <!-- PDF -->
                    @if(!empty($init['policy']))
                    <div class="row wrapSections wrapPolicy">
                        <div class="col-xs-12">
                            <form>
                                <fieldset>
                                    <legend>Create PDF:</legend>
                                    <div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                            <table width="60%">
                                                <tr>
                                                    <td width="20%">AV5 Schedule:</td>
                                                    @if(!empty($init['files']['policy_document_path']))
                                                    <td width="10%"><span class="text-success">YES</span></td>
                                                    <td width="60%"><button class="btn btn-primary btn-process send_ftp" data-type="voluntary">SEND TO FTP</button><span class="button-text-state state_ftp" data-type="voluntary">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @else
                                                    <td width="10%"><span class="text-danger">NO</span></td>
                                                    <td width="60%"><button class="btn btn-primary btn-process create_pdf" data-type="voluntary">CREATE PDF</button><span class="button-text-state state_pdf" data-type="voluntary">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Original Invoice:</td>
                                                    @if(!empty($init['files']['policy_original_invoice_path']))
                                                    <td><span class="text-success">YES</span></td>
                                                    <td><button class="btn btn-primary btn-process send_ftp" data-type="ori_invoice">SEND TO FTP</button><span class="button-text-state state_ftp" data-type="ori_invoice">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @else
                                                    <td><span class="text-danger">NO</span></td>
                                                    <td><button class="btn btn-primary btn-process create_pdf" data-type="ori_invoice">CREATE PDF</button><span class="button-text-state state_pdf" data-type="ori_invoice">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Copy Invoice:</td>
                                                    @if(!empty($init['files']['policy_copy_invoice_path']))
                                                    <td><span class="text-success">YES</span></td>
                                                    <td><button class="btn btn-primary btn-process send_ftp" data-type="copy_invoice">SEND TO FTP</button><span class="button-text-state state_ftp" data-type="copy_invoice">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @else
                                                    <td width="20%"><span class="text-danger">NO</span></td>
                                                    <td><button class="btn btn-primary btn-process create_pdf" data-type="copy_invoice">CREATE PDF</button><span class="button-text-state state_pdf" data-type="copy_invoice">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @endif
                                                </tr>
                                                @if(!empty($init['policy']['policy_com_number']))
                                                <tr>
                                                    <td>AC3 Schedule:</td>
                                                    @if(!empty($init['files']['compulsory_document_path']))
                                                    <td><span class="text-success">YES</span></td>
                                                    <td><button class="btn btn-primary btn-process send_ftp" data-type="compulsory">SEND TO FTP</button><span class="button-text-state state_ftp" data-type="compulsory">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @else
                                                    <td width="20%"><span class="text-danger">NO</span></td>
                                                    <td><button class="btn btn-primary btn-process create_pdf" data-type="compulsory">CREATE PDF</button><span class="button-text-state state_pdf" data-type="compulsory">&nbsp;&nbsp;กำลังดำเนินการ...</span></td>
                                                    @endif
                                                </tr>
                                                @endif
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    @endif


                    <!-- SEND SMS -->
                    @if(!empty($init['policy']))
                    <div class="row wrapSections wrapSMS">
                        <div class="col-xs-12">
                            <form>
                            <fieldset>
                                <legend>Send SMS Status: <span id="sms_status">
                                    @if(empty($init['sms']))
                                    <i class="fa fa-close text-danger p-r-1"></i>
                                    @else
                                    <i class="fa fa-check text-success p-r-1"></i>
                                    @endif
                                </span></legend>
                                <div>
                                    <div class="row">
                                        @if(empty($init['sms']))
                                        <div class="col-md-2">Send SMS Status: <span class="text-danger">NO</span></div>
                                        @else
                                        <div class="col-md-2">Send SMS Status: <span class="text-success">YES</span></div>
                                        @endif
                                    </div>
                                    @if(empty($init['sms']))
                                    <div class="row">
                                        <div class="col-md-2"><button class="btn btn-primary btn-process" id="update_sms">SEND SMS</button><span class="button-text-state" id="state_sms">&nbsp;&nbsp;กำลังดำเนินการ...</span></div>
                                    </div>
                                    @endif
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                    @endif


                    <!-- SEND EMAIL -->
                    @if(!empty($init['policy']))
                    <div class="row wrapSections wrapSMS">
                        <div class="col-xs-12">
                            <form>
                            <fieldset>
                                <legend>Send EMAIL Status: <span id="email_status">
                                    @if(empty($init['email']))
                                    <i class="fa fa-close text-danger p-r-1"></i>
                                    @else
                                    <i class="fa fa-check text-success p-r-1"></i>
                                    @endif
                                </span></legend>
                                <div>
                                    <div class="row">
                                        @if(empty($init['email']))
                                        <div class="col-md-2">Send EMAIL Status: <span class="text-danger">NO</span></div>
                                        @else
                                        <div class="col-md-2">Send EMAIL Status: <span class="text-success">YES</span></div>
                                        @endif
                                    </div>
                                    @if(empty($init['email']))
                                    <div class="row">
                                        <div class="col-md-2"><button class="btn btn-primary btn-process" id="update_email">SEND EMAIL</button><span class="button-text-state" id="state_email">&nbsp;&nbsp;กำลังดำเนินการ...</span></div>
                                    </div>
                                    @endif
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                    @endif


                    <!-- SEND ENDORSE -->
                    @if(!empty($init['policy']))
                    <div class="row wrapSections wrapEndorse">
                        <div class="col-xs-12">
                            <form>
                            <fieldset>
                                <legend>Send Endorse Status: <span id="email_status">
                                    @if($init['policy']['update_core_status'] == 1)
                                    <i class="fa fa-check text-success p-r-1"></i>
                                    @else
                                    <i class="fa fa-close text-danger p-r-1"></i>
                                    @endif
                                </span></legend>
                                <div>
                                    <div class="row">
                                        @if($init['policy']['update_core_status'] == 1)
                                        <div class="col-sm-12">Send Endorse Status: <span class="text-success">YES</span></div>
                                        @else
                                        <div class="col-sm-12">Send Endorse Status: <span class="text-danger">NO</span> 
                                            @if(!empty($init['endorseLog']))
                                            <a class="btn btn-viewlog" href="{{ route('admin.endorse_log_detail').'/'.$init['endorseLog']['id'] }}" target="_blank">VIEW LOGS</a>
                                            @endif
                                        </div>
                                        @endif
                                    </div>
                                    @if($init['policy']['update_core_status'] != 1)
                                    <div class="row">
                                        <div class="col-sm-12"><button class="btn btn-primary btn-process" id="update_endorse">SEND ENDORSE</button><span class="button-text-state" id="state_endorse">&nbsp;&nbsp;กำลังดำเนินการ...</span></div>
                                    </div>
                                    @endif
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                    @endif






                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/compile.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_order_no',$init['orders']['id']) }} 
{{ Form::hidden('hd_get_process_list',route('admin.compile.process')) }} 
{{ Form::hidden('hd_update_order',route('admin.compile.order')) }} 
{{ Form::hidden('hd_update_payment',route('admin.compile.payment')) }} 
{{ Form::hidden('hd_create_policy',route('admin.compile.policy')) }} 
{{ Form::hidden('hd_create_receipt',route('admin.compile.receipt')) }} 
{{ Form::hidden('hd_create_pdf',route('admin.compile.pdf')) }} 
{{ Form::hidden('hd_send_ftp',route('admin.compile.ftp')) }} 
{{ Form::hidden('hd_send_sms',route('admin.compile.sms')) }} 
{{ Form::hidden('hd_send_email',route('admin.compile.email')) }} 
{{ Form::hidden('hd_send_endorse',route('admin.compile.endorse')) }} 

@endsection
