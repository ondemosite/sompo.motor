@extends('admin.layout.default')
@section('title') Receipt @endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-money"></i>@lang('admin_receipt.topic')</span>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_receipt.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_receipt_number">@lang('admin_receipt.receipt_number')</label>
                                {{ Form::text('search_receipt_number',null, ['class' => 'form-control','placeholder' => trans('admin_receipt.receipt_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_order_number">@lang('admin_receipt.payment_number')</label>
                                {{ Form::text('search_payment_number',null, ['class' => 'form-control','placeholder' => trans('admin_receipt.payment_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_policy_number">@lang('admin_receipt.credit_number')</label>
                                {{ Form::text('search_credit_number',null, ['class' => 'form-control','placeholder' => trans('admin_receipt.credit_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_receipt.date_from')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_receipt.date_from')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_receipt.date_to')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_receipt.date_to')]) }}
                            </div>

                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_order.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_order.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_receipt.no')</th>
                                <th>@lang('admin_receipt.receipt_number')</th>
                                <th>@lang('admin_receipt.payment_number')</th>
                                <th>@lang('admin_receipt.amount')</th>
                                <th>@lang('admin_receipt.credit_number')</th>
                                <th>@lang('admin_receipt.created_at')</th>
                                <th>@lang('admin_receipt.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/receipt.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_link_policy',route('admin.get_policy_data')) }} 
{{ Form::hidden('hd_link_order',route('admin.get_order_data')) }} 
{{ Form::hidden('hd_list',route('admin.get_receipt_list')) }} 
{{ Form::hidden('hd_data',route('admin.get_receipt_data')) }} 
@include('admin.receipt.modals.receipt') 
@endsection
