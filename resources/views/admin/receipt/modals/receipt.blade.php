<div class="modal fade" id="modal-receipt" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-"></i>@lang('admin_receipt.receipt_data')</h4>
            </div>
            <div class="modal-body">
                <div class="modal-topic"><b>@lang('admin_receipt.receipt_number') :</b> <span id="receipt_no"></span>
                    <span class="pull-right"><b>@lang('admin_receipt.created_at') :</b> <span id="created_date"></span></span>
                </div>
                <table width="100%" class="table table-striped">
                    <tbody>
                        <tr>
                            <td>@lang('admin_payment2.order_number')</td>
                            <td id="field_rc_order"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_receipt.payment_number')</td>
                            <td id="field_rc_payment"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment2.policy_number')</td>
                            <td id="field_rc_policy"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_receipt.amount')</td>
                            <td id="field_rc_am"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_receipt.channel_code')</td>
                            <td id="field_rc_cn"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_receipt.credit_number')</td>
                            <td id="field_rc_cre"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_receipt.payment_scheme')</td>
                            <td id="field_rc_ps"></td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
    </div>