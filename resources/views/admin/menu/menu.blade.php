@extends('admin.layout.default')
@section('title') Menu & Permission @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/menu_permission.css') !!}">
@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.menu.modal.menus')
<div class="page-content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-tasks"></i>@lang('admin_menu.menu')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-menu" title="@lang('admin_menu.add_menu')"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <!-- Generate Menu -->
                    <ol class="menu vertical"></ol>
                </div>
                <div class="panel-footer text-right">
                    <button type="button" class="btn btn-primary btn-update-menu">@lang('admin_menu.save')</button>
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
        <div class="col-md-6">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-unlock-alt"></i>@lang('admin_menu.permission')</span>
                </div>
                <div class="panel-body">
                    <div class="selection">
                        {{ Form::select('select_privilege',[], null, ['class' => 'form-control input-lg m-b-2',
                        'placeholder'=> 'Select Privilege','id'=>'select_privilege' ]) }}
                    </div>
                    <div class="table-responsive ">
                        <table class="table table-render">
                        </table>
                    </div>
                </div>
            </div> <!-- /Panel -->
        </div>
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('component/sortable/js/sortable.js') }}"></script>
<script src="{{ asset('admin/js/menu_permission.js') }}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_menu',route('admin.get_menu')) }} 
{{ Form::hidden('hd_save_menu',route('admin.save_menu')) }} 
{{ Form::hidden('hd_get_parent',route("admin.get_parent")) }} 
{{ Form::hidden('hd_update_order',route("admin.update_order")) }} 
{{ Form::hidden('hd_get_menu_data',route("admin.get_menu_data")) }} 
{{ Form::hidden('hd_delete_menu',route("admin.delete_menu")) }} 
{{ Form::hidden('hd_get_pm_menu',route("admin.get_pm_menu")) }} 
{{ Form::hidden('hd_get_privilege',route("admin.get_menu_privilege")) }} 
{{ Form::hidden('hd_set_pm_menu',route("admin.set_pm_menu")) }} 


@endsection
