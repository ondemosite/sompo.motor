<div class="modal fade" id="modal-menu" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-tasks p-r-1"></i>@lang('admin_menu.menu')</h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_menu"),'method' => 'POST','class'=>'menu-form','id'=>'menu-form')) }}
                {{ Form::hidden('menu_id') }}
                <div class="form-group">
                    <label class="required">{{ trans('admin_menu.parent_menu')}}</label>
                    {{ Form::select('parent_id',[], null, ['class' => 'form-control input-lg m-b-2t',
                    'placeholder'=> 'root' ]) }}
                </div>
                <div class="form-group">
                    <label class="required">{{ trans('admin_menu.menu_name')}}</label>
                    {{ Form::text('name', '', ['class' => 'form-control input-lg m-b-2'
			        ,'placeholder' => trans('admin_menu.menu_name')]) }}
                </div>

                <div class="form-group">
                    <label class="required">{{ trans('admin_menu.menu_name_th')}}</label>
                    {{ Form::text('name_th', '', ['class' => 'form-control input-lg m-b-2'
			        ,'placeholder' => trans('admin_menu.menu_name_th')]) }}
                </div>

                <div class="form-group">
                    <label>{{ trans('admin_menu.menu_path')}}</label>
                    {{ Form::text('path', '', ['class' => 'form-control input-lg m-b-2'
			        ,'placeholder' => trans('admin_menu.menu_path_example')]) }}
                </div>
                
                <div class="form-group">
                    <label>{{ trans('admin_menu.icon')}}</label>
                    {{ Form::text('icon', '', ['class' => 'form-control input-lg m-b-2'
			        ,'placeholder' => '']) }}
                </div>
                <div class="form-group">
                    <label class="required">{{ trans('admin_menu.orders')}}</label>
                    {{ Form::text('orders', '', ['class' => 'form-control input-lg m-b-2'
			        ,'placeholder' => trans('admin_menu.orders_example')]) }}
                </div>
                <div class="form-group">
                    <label class="m-b-2">@lang('admin_menu.display')</label>
                    <label for="switcher-primary" class="switcher switcher-primary">
                        <input type="checkbox" name="isshow" id="switcher-primary" checked="">
                        <div class="switcher-indicator">
                            <div class="switcher-yes">@lang('common.yes')</div>
                            <div class="switcher-no">@lang('common.no')</div>
                        </div>
                    </label>
                </div>
                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">@lang('common.close')</button>
                <button type="button" class="btn btn-primary" id="submit_button">@lang('common.save')</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>