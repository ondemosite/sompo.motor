@extends('admin.layout.default')

@section('breadcrumb')
<h1 class="pull-xs-left"><span class="text-muted font-weight-light"><i class="page-header-icon fa fa-expeditedssl"></i>Access Page</h1>
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel panel-danger">
                <div class="panel-heading"><i class="ti-face-sad"></i> {{ trans('common.permission_denied') }}</div>
                    <div class="panel-body text-center">
                    {{ trans('common.permission_denied_detail') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


