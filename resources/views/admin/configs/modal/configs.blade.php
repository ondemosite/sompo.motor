<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-share-alt p-r-1"></i><span>Add Faq</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.save_configs"),'name'=>'form_configs','class'=>'form_configs','id'=>'form_configs')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_configs.title')</label>
                    {{ Form::text('title',null, ['class' => 'form-control','placeholder' => trans('admin_configs.title')]) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_configs.code')</label>
                    {{ Form::text('config_code',null, ['class' => 'form-control','placeholder' => trans('admin_configs.code')]) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_configs.value')</label>
                    {{ Form::text('config_value',null, ['class' => 'form-control','placeholder' => trans('admin_configs.value')]) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_configs.start')</label>
                    {{ Form::text('start_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_configs.start')]) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_configs.end')</label>
                    {{ Form::text('expire_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_configs.end')]) }}
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <label>&nbsp;</label>
                        <label class="switcher switcher-lg switcher-primary">
                            <input type="checkbox" name="status">
                            <div class="switcher-indicator">
                                <div class="switcher-yes">@lang('common.active')</div>
                                <div class="switcher-no">@lang('common.inactive')</div>
                            </div>                     
                        </label>
                    </div>
                </div>

                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_button">Save changes</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>