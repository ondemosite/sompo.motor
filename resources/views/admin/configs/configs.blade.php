@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')

@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.configs.modal.configs')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-share-alt"></i>@lang('admin_configs.topic')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add API"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_configs.no')</th>
                                <th>@lang('admin_configs.title')</th>
                                <th>@lang('admin_configs.code')</th>
                                <th>@lang('admin_configs.start')</th>
                                <th>@lang('admin_configs.end')</th>
                                <th>@lang('admin_configs.status')</th>
                                <th>@lang('admin_configs.update')</th>
                                <th>@lang('admin_configs.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/configs.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_list',route('admin.get_configs_list')) }} 
{{ Form::hidden('hd_data',route('admin.get_data_configs')) }} 
{{ Form::hidden('hd_save',route('admin.save_configs')) }} 
{{ Form::hidden('hd_delete',route('admin.delete_configs')) }} 
{{ Form::hidden('hd_status',route('admin.set_status_configs')) }} 
@endsection
