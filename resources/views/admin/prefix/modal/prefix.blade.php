<div class="modal fade" id="modal-add" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-address-card-o p-r-1"></i><span>Add Prefix</span></h4>
            </div> 
            <div class="modal-body">
                {{ Form::open(array('url' => route("admin.prefix.save"),'name'=>'form_action','class'=>'form_action','id'=>'form_action')) }}
                {{ Form::hidden('id',null) }} 
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_prefix.name')</label>
                    {{ Form::text('name',null, ['class' => 'form-control','placeholder' => trans('admin_prefix.name')]) }}
                </div>
                <div class="form-group">
                    <label class="control-label required" for="required-input">@lang('admin_prefix.name_en')</label>
                    {{ Form::text('name_en',null, ['class' => 'form-control','placeholder' => trans('admin_prefix.name_en')]) }}
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>เพศ</label>
                        <div>
                            <label class="radio-inline">
                                @php
                                    $gender = "NONE";
                                    if(!empty($data['gender'])){
                                        $gender = $data['gender'];
                                    }
                                @endphp
                                <input type="radio" name="gender" value="MALE" {{ ($gender=="MALE"?"checked":"") }} > ชาย
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="FEMALE" {{ ($gender=="FEMALE"?"checked":"") }} > หญิง
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="NONE" {{ ($gender=="NONE"?"checked":"") }} > ไม่ระบุ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <label class="m-b-2">ตั้งเป็นค่าเริ่มต้น</label>
                        <label class="switcher switcher-lg switcher-primary">
                            @if(isset($data['is_default']))
                            <input type="checkbox" name="is_default" {{ $data['is_default']==1?'checked':'' }}>
                            @else
                            <input type="checkbox" checked name="is_default">
                            @endif
                            <div class="switcher-indicator">
                                <div class="switcher-yes">@lang('common.yes')</div>
                                <div class="switcher-no">@lang('common.no')</div>
                            </div>                     
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <label class="m-b-2">สถานะ</label>
                        <label class="switcher switcher-lg switcher-primary">
                            <input type="checkbox" name="status">
                            <div class="switcher-indicator">
                                <div class="switcher-yes">@lang('common.active')</div>
                                <div class="switcher-no">@lang('common.inactive')</div>
                            </div>                     
                        </label>
                    </div>
                </div>

                {{ Form::close() }}
            </div> <!-- /modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_button">Save changes</button>
            </div> <!-- /modal-footer -->
        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div>