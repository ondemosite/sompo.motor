@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')@endsection

@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
@include('admin.prefix.modal.prefix')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-address-card-o"></i>@lang('admin_prefix.topic')</span>
                    <div class="panel-heading-controls">
                        <a id="open-modal" href="#" data-toggle="modal" data-target="#modal-add" title="Add Prefix"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_configs.no')</th>
                                <th>@lang('admin_prefix.name')</th>
                                <th>@lang('admin_prefix.name_en')</th>
                                <th>@lang('admin_prefix.default')</th>
                                <th>@lang('admin_configs.status')</th>
                                <th>@lang('admin_configs.update')</th>
                                <th>@lang('admin_configs.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->

@endsection

@section('javascript')
<script src="{!! asset('admin/js/prefix.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_list',route('admin.prefix.list')) }} 
{{ Form::hidden('hd_data',route('admin.prefix.get')) }} 
{{ Form::hidden('hd_save',route('admin.prefix.save')) }} 
{{ Form::hidden('hd_delete',route('admin.prefix.delete')) }} 
@endsection
