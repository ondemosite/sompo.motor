@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/payment_log.css') !!}">
@endsection
@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon fa fa-car"></i>@lang('admin_user_manage_log.topic')</span>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_user_manage_log.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_user_manage_log.create_date')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_user_manage_log.action_date_from') ]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_user_manage_log.create_date')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_user_manage_log.action_date_to') ]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_user_manage_log.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_user_manage_log.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_user_manage_log.no')</th>
                                <th>@lang('admin_user_manage_log.edit_data')</th>
                                <th>@lang('admin_user_manage_log.old_data')</th>
                                <th>@lang('admin_user_manage_log.create_date')</th>
                                <th>@lang('admin_user_manage_log.create_by')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('admin/js/user_management_log.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_list',route('admin.user_management_log_list')) }} 
{{ Form::hidden('hd_get_data',route('admin.user_management_log_data')) }} 
@include('admin.logs.modal.user_management_log') 
@endsection
