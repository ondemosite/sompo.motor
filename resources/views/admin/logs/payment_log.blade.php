@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/payment_log.css') !!}">
@endsection
@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon"></i>@lang('admin_payment.topic')</span>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_payment.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_type_class">@lang('admin_payment.order_number')</label>
                                {{ Form::text('search_order_number',null, ['class' => 'form-control','placeholder' => trans('admin_payment.order_number')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_transaction_ref">@lang('admin_payment.2c2p_ref')</label>
                                {{ Form::text('search_transaction_ref',null, ['class' => 'form-control','placeholder' => 'transaction reference']) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_status">@lang('admin_payment.status')</label>
                                {{ Form::select('search_status',Config::get("payment_code.payment_status"),null, ['class' => 'form-control','placeholder' => 'Select Status']) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_type">@lang('admin_payment.credit_number')</label>
                                {{ Form::text('search_credit_number',null, ['class' => 'form-control','placeholder' => trans('admin_payment.credit_number') ]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_payment.request_date')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_payment.request_date_from') ]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_payment.request_date')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_payment.request_date_to') ]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_payment.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_payment.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_payment.no')</th>
                                <th>@lang('admin_payment.order_number')</th>
                                <th>@lang('admin_payment.amount')</th>
                                <th>@lang('admin_payment.ref_number')</th>
                                <th>@lang('admin_payment.payment_channel')</th>
                                <th>@lang('admin_payment.payment_status')</th>
                                <th>@lang('admin_payment.credit_number')</th>
                                <th>@lang('admin_payment.scheme')</th>
                                <th>@lang('admin_payment.request_date')</th>
                                <th>@lang('admin_payment.response_date')</th>
                                <th>@lang('admin_payment.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('admin/js/payment_log.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_list',route('admin.payment_log_list')) }} 
{{ Form::hidden('hd_get_data',route('admin.payment_log_data')) }} 

@include('admin.logs.modal.payment_log') 
@endsection
