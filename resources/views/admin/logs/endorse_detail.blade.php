@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/endorse_log.css') !!}">
@endsection
@section('breadcrumb')
<h1 class="pull-xs-left"><span class="text-muted font-weight-light"><i class="page-header-icon fa fa fa-save"></i>ระบบบันทึกรายการ / </span>Endorse Request</h1>
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel"> <!-- Privilege -->
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon"></i>@lang('admin_endorse_log.topic')</span>
                </div>
                <div class="panel-body">
                    @php
                        $policy = $init->policy()->first();
                        $requestArray = json_decode($init->data);
                        $responseArray = json_decode($init->response);
                    @endphp
                    <h3 class="log-detail-title">Policy Number: <span> {{ $policy->policy_number }}</span></h3>
                    <h3 class="log-detail-title">Endorse Number: <span> {{ $init->endorse_no }}</span></h3>
                    <h3 class="log-detail-title">Reference Number: <span> {{ $init->av_reference_no }} </span></h3>
                    <h3 class="log-detail-title {{ $init->status=='SUCCESS'?'text-success':'text-danger' }}">Status: <span> {{ $init->status }} </span></h3>
                    <h3 class="log-code-title">Request Data</h3>
                    <div>
                        <pre id="post_area">
                            {!! json_encode($requestArray, JSON_PRETTY_PRINT) !!}
                        </pre>
                    </div>
                    <h3 class="log-code-title">Response Data</h3>
                    <div>
                        <pre id="post_area">
                            {!! json_encode($responseArray, JSON_PRETTY_PRINT) !!}
                        </pre>
                    </div>
                    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('admin/js/endorse_detail.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_list',route('admin.endorse_log_list')) }} 
@include('admin.logs.modal.json_view') 
@endsection
