<div class="modal fade" id="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel">@lang('admin_user_manage_log.topic')</h4>
            </div>
            <div class="modal-body">
                <table width="100%" class="table table-striped">
                    <thead>
                        <th width="30%">@lang('admin_user_manage_log.field')</th>
                        <th>@lang('admin_user_manage_log.data')</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>@lang('admin_user_manage_log.username')</td>
                            <td id="field_username"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_user_manage_log.password')</td>
                            <td id="field_password"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_user_manage_log.name')</td>
                            <td id="field_name">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_user_manage_log.lastname')</td>
                            <td id="field_lastname">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_user_manage_log.email')</td>
                            <td id="field_email">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_user_manage_log.privilege')</td>
                            <td id="field_privilege">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_user_manage_log.status')</td>
                            <td id="field_status">-</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>