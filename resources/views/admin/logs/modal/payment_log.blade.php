<div class="modal fade" id="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel">@lang('admin_payment.topic')</h4>
            </div>
            <div class="modal-body">
                <table width="100%" class="table table-striped">
                    <thead>
                        <th width="30%">@lang('admin_payment.field')</th>
                        <th>@lang('admin_payment.data')</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>@lang('admin_payment.order_number')</td>
                            <td id="field_order_number"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.amount')</td>
                            <td id="field_amount"></td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.trans_ref')</td>
                            <td id="field_ref">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.approve_code')</td>
                            <td id="field_approve">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.eci')</td>
                            <td id="field_eci">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.payment_channel')</td>
                            <td id="field_channel">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.payment_status')</td>
                            <td id="field_status">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.channal_response_code')</td>
                            <td id="field_response">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.channel_response_desc')</td>
                            <td id="field_response_desc">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.credit_number')</td>
                            <td id="field_credit_number">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.payment_scheme')</td>
                            <td id="field_scheme">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.browser_info')</td>
                            <td id="field_browser">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.request_at')</td>
                            <td id="field_request_at">-</td>
                        </tr>
                        <tr>
                            <td>@lang('admin_payment.response_at')</td>
                            <td id="field_response_at">-</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>