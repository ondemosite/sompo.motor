@extends('admin.layout.default')
@section('title') Admins @endsection
@section('css')
<link rel="stylesheet" href="{!! asset('admin/css/email_log.css') !!}">
@endsection
@section('breadcrumb')
{!!html_entity_decode(Menu::genBreadcrumb())!!}
@endsection

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="panel-title-icon"></i>@lang('admin_otp_log.topic')</span>
                </div>
                <div class="panel-body">
                    <div class="search-box">
                        <h6 class="m-t-0 m-b-2 font-weight-semibold text-default font-size-16">@lang('admin_resend_log.search')</h6>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_otp_log.receiver')</label>
                                {{ Form::text('search_receiver',null, ['class' => 'form-control','maxlength'=>10,'placeholder'=>trans('admin_otp_log.receiver')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_status">@lang('admin_resend_log.status')</label>
                                {{ Form::select('search_status',['SUCCESS'=>'SUCCESS','FAIL'=>'FAIL'],null, ['class' => 'form-control','placeholder' => trans('admin_resend_log.select_status')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_from_date">@lang('admin_otp_log.date_from')</label>
                                {{ Form::text('search_from_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_otp_log.date_from')]) }}
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="search_to_date">@lang('admin_otp_log.date_to')</label>
                                {{ Form::text('search_to_date',null, ['class' => 'form-control datepicker','placeholder' => trans('admin_otp_log.date_to')]) }}
                            </div>
                            <button id="search" type="button" class="btn btn-primary"><i class="fa fa-search"></i> @lang('admin_resend_log.search')</button>
                            <button id="clear" type="button" class="btn"><i class="fa fa-refresh"></i> @lang('admin_resend_log.clear')</button>
                        </form>
                        <hr class="page-wide-block">
                    </div>
                    <div class="table-light">
                        <table class="table table-striped table-bordered" id="datatables">
                            <thead>
                            <tr>
                                <th>@lang('admin_resend_log.no')</th>
                                <th>@lang('admin_otp_log.receiver')</th>
                                <th>@lang('admin_resend_log.data')</th>
                                <th>@lang('admin_email_log.response')</th>
                                <th>@lang('admin_resend_log.status')</th>
                                <th>@lang('admin_resend_log.created_date')</th>
                            </tr>
                            </thead>
                        </table>
                    </div> <!-- /table-light -->    
                </div>
            </div> <!-- /Panel -->
        </div> <!-- /col-md-12 -->
    </div> <!-- /row -->
</div> <!-- /Page-content -->
@endsection

@section('javascript')
<script src="{!! asset('admin/js/otp_log.js') !!}"></script>
@endsection

@section('hidden')
{{ Form::hidden('hd_get_list',route('admin.otp_log_list')) }} 
{{ Form::hidden('hidden_json','') }} 
@include('admin.logs.modal.json_view') 
@endsection
