<?php

return [

    'no' => 'No',
    'edit' => 'Edit',
    'topic' => 'Cutoff Date',
    'alert_exist' => 'This config has already exist',
    'years' => 'Years',
    'months' => 'Months',
    'cutoff' => 'Cutoff Date',
    'last_update' => 'Lastest Update',
    'updated_by' => 'Updated By',
    'action' => 'Action'

];
