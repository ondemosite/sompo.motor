<?php

return [
    'no' => 'No',
    'topic' => 'Receipt',
    'search' => 'Search',
    'receipt_number' => 'Receipt Number',
    'payment_number' => 'Payment Number',
    'credit_number' => 'Credit Number',
    'date_from' => 'Create Date From',
    'date_to' => 'Create Date to',
    'amount' => 'Payment Amount',
    'created_at' => 'Created At',
    'action' => 'Action',
    'receipt_data' => "Receipt Information",
    'channel_code' => 'Payment Channel Code',
    'payment_scheme' => 'Payment Scheme'


];
