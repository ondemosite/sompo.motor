<?php

return [
    'yourplan' => 'Your plan',
    'type' => 'Type',

    'start' => 'Start',
    'cover_detail' => 'Coverage Details',
    'modify_here' => 'Modify Here',
    'first_damage' => 'Deductible',
    'owner_damage' => 'Damage to your car',
    'accident_have' => 'Accident with third parties',
    'car_crash' => 'Car Crash',
    'car_rob' => 'Cars Lost/Robbery',
    'car_fire' => 'Car was on fire',
    'car_terrorism' => 'Car Terrorism',
    '24hour' => '24 hours, Lift truck service',
    'so_accident' => 'By Accident',
    '20_percent_fix' => '20% percent of the repair cost',
    'garanty_out' => 'Third party liability',
    'garanty_stuff' => 'Liability for property',
    'death_disable' => 'Injury or death',
    'per_person' => 'Per Person',
    'per_once' => 'Per Times',
    'coverage_addition' => 'Additional Coverage',
    'private_accident' => 'Personal Accident',
    'medical_fee' => 'Medical Expense',
    'bail_driver' => 'Bail Driver',
    'nodeduct' => 'None',
    'genaral_garage' => 'Genaral Garage',
    'dealer_garage' => 'Dealer Garage',

    'car' => 'Car',
    'plan' => 'Insurance Type',
    'begin_insurance' => 'Initial Insurance',
    'begin_payment' => 'Balance to be paid',
    'modify_price' => 'Modify Plan', 

    'coverage_stuff_outside' => 'Damage to the property of third parties',
    'coverage_death_outside' => 'coverage injury or death of a driver\'s',
    'coverage_people' => 'Passengers and third parties'
];