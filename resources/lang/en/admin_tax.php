<?php

return [

    'edit' => 'Edit',
    'tax' => 'Tax',
    'topic' => 'Edit Tax',
    'stamp' => 'Stamp',
    'vat' => 'VAT',
    'submit' => 'Submit',
    'medical_free' => 'Medical Expense',
    'disable' => 'Permanent Disability Compensation'

];
