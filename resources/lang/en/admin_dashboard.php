<?php

return [

    'dashboard' => 'Dashboard',
    'earned_today' => 'EARNED TODAY',
    'total_income' => 'TOTAL INCOME',
    'active_insurance' => 'ACTIVE INSURANCE',
    'insurance_popular' => 'Insurance Plan Popular',
    'monthly_summary' => 'Monthly Summary',
    'more' => 'More',
    'monthly_visitor' => 'Monthly visitor statistics',
    'used_ram' => 'USED RAM',
    'hard_disk' => 'HARD DISK',
    'new_orders' => 'New Orders',
    'order_number' => 'Order Number',
    'plan_name' => 'Plan Name',
    'full_name' => 'Full Name',
    'contact' => 'Contact',
    'amount' => 'Amount',
    'created_at' => 'Create Date',
    'status' => 'status',
    'monthly_earned_stat' => 'Monthly Earned Statics (Baht / Month)',

];
