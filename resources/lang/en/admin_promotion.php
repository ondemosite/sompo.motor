<?php

return [

    'topic' => 'Promotion',
    'title' => 'Title',
    'search' => 'Search',
    'clear' => 'Clear',    
    'discount' => 'Discount (%)',
    'no' => 'No',
    'code' => 'Code',
    'discount_2' => 'Discount',
    'start_date' => 'Start Date',
    'expire_date' => 'Expire Date',
    'received' => 'Received',
    'status' => 'Status',
    'action' => 'Action',
    
    'manage_topic' => 'Add/Edit Promotion',
    'promotion_title' => 'Promotion Title',
    'promotion_code' => 'Promotion Code',
    'maximum_grant' => 'Maximum Grant',
    'active_plan' => 'Active Plan',
    'submit' => 'Submit',
    'add' => 'Add',
    'edit' => 'Edit',

    'regis_name' => 'Name - Lastname',
    'regis_email' => 'Email',
    'regis_tel' => 'Tel',
    'regis_expire' => 'Insurance Expire',
    'booking_register' => 'Register Promotion',
    'register_amount' => 'Maximum Grant',
    'register_list' => 'Register List',
    'created_at' => 'Created At',
    'vehicle_brand' => 'Car Brand',
    'vehicle_model' => 'Car Model',
    'select_vehicle_model' => 'Select Car Model',
    'select_vehicle_brand' => 'Select Car Brand',
    'ending_message' => 'Showing Message'
];
