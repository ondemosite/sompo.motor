<?php

return [

    'compulsory_topic' => 'Compulsory Pricing List',
    'add_compulsory' => 'Add Compulsory',
    'no' => 'No',
    'body_type' => 'Body Type',
    'mortor_code_ac' => 'Motor Code AC',
    'net_premium' => 'Net Premium',
    'stamp' => 'Stamp',
    'vat' => 'VAT',
    'gross_premium' => 'Gross Premium',
    'action' => 'Action',
    'close' => 'Close',
    'save' => 'Save changes'

];
