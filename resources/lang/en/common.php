<?php

return [
    'ajax_timeout' => 'Server Response Timeout!',
    'save_sucess'=>'Save Data successfully.',
    'delete_success'=>'Delete Data successfully.',
    'confirm_action'=>'Confirm to run command',
    'delete_action'=>'Do you want to delete :attribute ?',
    'change_to_admin'=>'Becareful! If you replace :attribute as Admin you can not modify them further.<br/> Do you want to replace :attribute as Admin ?',
    'permission_denied' => 'Permission Denied!',
    'permission_denied_detail' => 'You do not have permission to access this page, please refer to your system administrator.',
    'approve_command'=>'Do you want to approve :attribute ?',

    'loading' => 'Uploading',

    'year' => 'Years',
    'year_up' => 'Years Over',
    'person' => 'Person',
    'yes' => 'Yes',
    'no' => 'No',
    'buy' => 'Buy',
    'no_buy' => 'No',
    'specify' => 'Yes',
    'no_specify' => 'None',
    'open' => 'Open',
    'close' => 'Close',

    'age' => 'Age',
    'name' => 'Name',
    'lastname' => 'Lastname',
    'idcard' => 'ID Card Number',
    'sex' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'birthdate' => 'Birthdate',
    'address' => 'Address',
    'province' => 'Province',
    'district' => 'District',
    'sub_district' => 'Sub-district',
    'zip_code' => 'Zip Code',
    'phone' => 'Phone Number',
    'mobile_phone' => 'Mobile Phone Number',
    'email' => 'E-mail',
    'driver_licence' => 'Driver\'s license number',
    'car_licence' => 'Car Licence Number',
    'car_chassis' => 'Chassis Number',
    'car' => 'Car',
    'find' => 'Find',
    'brand' => 'Brand',
    'model' => 'Model',
    'sub_model' => 'Sub Model',
    'baht' => 'Baht',
    'baht_times' => 'Baht/Times',
    'baht_person' => 'Baht/Person',
    'find_car' => 'Find',
    'plan' => 'Select Plan',
    'ft_si' => 'Sum Insured',
    'modify' => 'Modify Plan',
    'information' => 'Informarion',
    'payment' => 'Payment',
    'payment_cost' => 'Total Amount',
    'check_it' => 'Check',
    'garage_dealer' => 'Dealer Garage',
    'garage_general' => 'General Garage',
    'name_lastname' => 'Name - Lastname',
    'view' => 'View',
    'save' => 'Save',
    'close' => 'Close',
    'active' => 'Active',
    'inactive' => 'Inactive',

    'specify_coverage' => 'Specify Total Insured',
    'otp_notfound' => 'รหัส OTP ไม่ถูกต้อง กรุณาตรวจสอบ รหัส OTP อีกครั้ง',
    'otp_expire' => 'OTP has been expired.',
    'otp_notmatch' => 'The OTP entered is incorrect.',
    'otp_match' => 'Confirmation Successfully.',

    'verify_blacklist' => 'กำลังตรวจสอบรายชื่อ',
    'blacklist' => 'รายชื่อของคุณไม่ผ่านการตรวจสอบ กรุณาติดต่อเจ้าหน้าที่บริษัท',
    'verifing' => 'กำลังตรวจสอบข้อมูล'
];
