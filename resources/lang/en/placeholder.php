<?php

return [
    'province' => 'Province',
    'district' => 'District',
    'type_name' => 'Type Garage Name',
    'select_birthdate' => 'Plase Select Date',
    'please_select' => 'Please Select',

    'model' => 'Model',
    'model_year' => 'Model Year / Register Year',
    'sub_model' => 'Model Detail',

    'select_province' => 'Select Province',
    'select_district' => 'Select District',
    'select_sub_district' => 'Select Sub-District',
    
    'select_body_type' => 'Select Body Type',
    'select_year' => 'Select Year',
    'select_brand' => 'Select Brand Name',
    'select_model' => 'Select Model Name',
    'select_model_type' => 'Select Model Type Name',
    'type_cost' => 'Specify the amount you satisfied',

    'specify_insurance' => 'Please Specify amount',
    'start_date' => 'Start Date',
    'expire_date' => 'Expire Date',
    'select_status' => 'Select Status',
    'clear' => 'Clear',

    'stamp_percent' => 'stamp (%)',
    'vat_percent' => 'vat (%)',
    

    
];