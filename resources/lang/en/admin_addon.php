<?php

return [

    'topic' => 'Addon Car Loss',
    'topic2' => 'Addon HB',
    'topic3' => 'Addon Taxi',
    'topic4' => 'Addon Theft',

    'add_topic' => 'Add Addon Car Loss',
    'no' => 'No',
    'sum_insured' => 'Sum Insured',
    'ttl_carloss' => 'Total Loss Sum Insured',
    'normal' => 'Normal',
    'age_min_29' => 'Age < 29',
    'age_max_29' => 'Age >= 29',
    'cctv' => 'CCTV',
    'admin' => 'Admin',
    'action' => 'Action',


];