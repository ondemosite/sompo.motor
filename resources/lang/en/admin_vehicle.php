<?php

return [
    'topic' => 'Vehicle List',
    'vehicle' => 'Vehicle',
    'search' => 'Search',
    'clear' => 'Clear',
    'body_type' => 'Body Type',
    'brand' => 'Brand',
    'model' => 'Model',
    'year' => 'Year',
    'model_type' => 'Model Type',
    'model_type2' => 'Model Type (Display)',
    'mortor_av' => 'Mortor Code AV',
    'motor_ac' => 'Mortor Code AC',
    'action' => 'Action',

    'edit' => 'Edit',
    'add' => 'Add',
    'topic_form' => 'Add/Edit Vehicle',
    'cc' => 'CC',
    'tons' => 'Tons',
    'car_seat' => 'Car Seat',
    'driver_passenger' => 'Driver Passenger',
    'red_plate' => 'Red Plate',
    'used_car' => 'Used Car',
    'car_age' => 'Car Age',
    'submit' => 'Submit',

    'topic_brand' => 'Vehicle Brand',
    'top_order' => 'Top Order',
    'brand_name' => 'Brand Name',

    'topic_model' => 'Vehicle Model',
    'model_name' => 'Model Name',

    'no' => 'No',
    'close' => 'Close',
    'save' => 'Save Change'
    




]; 
