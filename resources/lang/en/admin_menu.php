<?php

return [

    'menu' => 'Menu',
    'permission' => 'Permission',
    'add_menu' => 'Add Menu',
    'update_menu' => 'Update Menu',
    'parent_menu' => 'Parent Menu',
    'menu_name' => 'Menu Name',
    'menu_name_th' => 'Menu Name TH',
    'menu_path' => 'Menu Path',
    'menu_path_example' => 'motoradmins/menu',
    'icon' => 'Icon',
    'icon_example' => 'ti-user',
    'orders'=>'Order',
    'orders_example' => '10.1',

    'th_menu' => 'MENU',
    'th_view' => 'VIEW',
    'th_add' => 'ADD',
    'th_edit' => 'EDIT',
    'th_delete' => 'DELETE',
    'save' => 'Save Change',
    'display' => 'Display',
    'close' => 'Close',

];
