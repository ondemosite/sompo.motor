<?php

return [

    'our_product' => 'Our Product',
    'no' => 'No',
    'cover_th' => 'Cover (Thai)',
    'cover_en' => 'Cover (English)',
    'title_th' => 'Title (Thai)',
    'title_en' => 'Title (English)',
    'link' => 'Link',
    'view' => 'View',
    'description_th' => 'Description (Thai)',
    'description_en' => 'Description (English)',
    'order' => 'Order',
    'action' => 'Action',

    'add' => 'Add',
    'edit' => 'Edit',
    'article' => 'Article',
    'change' => 'Change',
    'submit' => 'Submit',

    'whysompo'=>'Why Sompo',


];