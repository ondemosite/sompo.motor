<?php

return [
   'home'=>'Home',
   'product'=>'Product',
   'garage' => 'Garage Center',
   'faq' => 'FAQ',
   'contactus' => 'Contact Us',
   'calculate' => 'Calculate Premium'
];
