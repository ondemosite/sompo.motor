<?php

return [

    '2plus_detail' => 'Insurance Type 2+',
    '3plus_detail' => 'Insurance Type 3+',
    'type3_detail' => 'Insurance Type 3',
    'outside_coverage' => 'Third party liability',
    'damage' => 'Damage to life, the body and health',
    'per_once' => 'Per Times',
    'per_person' => 'Per Person',
    'stuff_damage' => 'Damage to the property',
    'under_coverage' => 'Addition Coverage',
    'death_disable' => 'Permanent Disability Compensation',
    'medical_free' => 'Medical Expense/Person',
    'bail_driver' => 'Bail Driver',
    'submit' => 'Submit',
    'general' => 'General Setting',
    'max_age' => 'Maximun Vehicle Age (Years)',
    'max_pre' => 'Maximun Advance Purchase Policy (Days)'

];
