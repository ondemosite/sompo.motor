<?php

return [

    'topic' => 'Payment Request/Response Log',
    'search' => 'Search',
    'order_number' => 'Order Number',
    '2c2p_ref' => '2c2p Reference Code',
    'status' => 'Status',
    'credit_number' => 'Credit Number',
    'request_date_from' => 'Request Date From',
    'request_date_to' => 'Request Date To',
    'clear' => 'Clear',
    'no' => 'No',
    'amount' => 'Amount',
    'ref_number' => 'Reference Number',
    'payment_channel' => 'Payment Channel',
    'payment_status' => 'Payment Status',
    'scheme' => 'Scheme',
    'request_date' => 'Request Date',
    'response_date' => 'Reponse Date',
    'action' => 'Action',
    'field' => 'Field',
    'data' => 'Data',
    'trans_ref' => 'Transaction Reference',
    'approve_code' => 'Approval Code',
    'eci' => 'ECI',
    'channal_response_code' => 'Channel Response Code',
    'channel_response_desc' => 'Channel Response Description',
    'payment_scheme' => 'Payment Scheme',
    'browser_info' => 'Browser Info',
    'request_at' => 'Request At',
    'response_at' => 'Response At',
    'select_status' => 'Select Status',
    '4degit' => '4 degits of front / 4 degits of rear',

    


];
