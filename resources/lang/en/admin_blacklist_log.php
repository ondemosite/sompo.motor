<?php

return [

    'topic' => 'Resend Document Log',
    'search' => 'Search',
    'select_type' => 'Select Type',
    'policy_number' => 'Policy Number',
    'select_status' => 'Select Status',
    'resend_date_from' => 'Resend Date From',
    'resend_date_to' => 'Resend Date To',
    'clear' => 'Clear',
    'no' => 'No',
    'type' => 'Type',
    'policy_number' => 'Policy Number',
    'status' => 'Status',
    'reason' => 'Reason',
    'created_by' => 'Created By',
    'created_date' => 'Created Date',
    'data' => 'Request Data'

];
