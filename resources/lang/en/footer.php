<?php

return [
    'company' => 'Sompo Insurance (Thailand) (Public) Company Limited',
    'company2' => '990 ABDULRAHIM PLACE, 12th, 14th Floor, Rama IV Road',
    'company3' => 'Silom, Bang Rak, Bangkok 10500',
    'customer' => 'Customer Service', //Customer Service
    'every_day' => 'Everyday 8.00- 20.00 hours.',
    'call_me' => 'Car accidents report 24 hours.',
    'every_hour' => 'Everyday 24 hours',
    'sos' => 'Roadside assistance service 24 hours.',
];