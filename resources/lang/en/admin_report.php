<?php

return [

    'topic' => 'Policy List',
    'export' => 'Export Excel',
    'search' => 'Search',
    'policy_number' => 'Policy Number',
    'fullname' => 'Name Lastname',
    'email' => 'Email',
    'tel' => 'Tel',
    'car_licence' => 'Car Licence',
    'create_date_from' => 'Create Date From',
    'create_date_to' => 'Create Date To',
    'status' => 'Status',
    'policy_status' => 'Policy Status',
    'clear' => 'Clear',
    'no' => 'No',
    'vehicle' => 'Vehicle',
    'sum_insured' => 'Sum Insured',
    'create_date' => 'Created Date',
    'conclusion' => 'Conclusion Date',
    'action' => 'Action',
    'export_date' => 'Export Date',
    'page' => 'Page',
    'export_policy' => 'Export Policy',
    'select_column' => 'Select Column To Export',
    'policy_number' => 'Policy Number',
    'policy_addon' => 'policy Addon Number',
    'tax_note' => 'Tax Note Number',
    'order_number' => 'Order Number',
    'status' => 'Status',
    'advice' => 'Advice',
    'car_info' => 'Car Info',
    'car_licence' => 'Car Licence',
    'car_chasis_number' => 'Car Chassis Number',
    'plan_name' => 'Plan Name',
    'created_date' => 'Created Date',
    'insurance_start' => 'Insurance Start',
    'garage_type' => 'Garage Type',
    'deductible' => 'Duductible',
    'addition_theft' => 'Addition Theft',
    'addition_taxi' => 'Addition Taxi',
    'addition_hb' => 'Addition HB',
    'addition_carloss' => 'Addition Car Loss',
    'main_name' => 'Main Name',
    'first_driver_name' => '1st Driver Name',
    'second_driver_name' => '2nd Driver Name',
    'select_record' => 'Select Record Amount',
    'item_per_page' => 'Items Per Page',
    'select_item_page' => 'Select Items Per Page',
    'select_page' => 'Select Page',
    'close' => 'Close',
    'export' => 'Export',

    /* Owner Report */
    'topic_owner' => 'Owner List',
    'agree' => 'Agree Advice',
    'insurance_status' => 'Insurance Status',
    'export_owner' => 'Export Policy Owner',
    'advice_state' => 'Advice State',
    'name' => 'Name',
    'lastname' => 'Lastname',
    'lastest_update' => 'Lastest Update',

    /* Order */
    'topic_order' => 'Order Request List',
    'insurance_amount' => 'Insurance Amount',
    'insurance' => 'Insurance',
    'addon' => 'Addon',
    'compulsory' => 'Compulsory',
    'payment_amount' => 'Payment Amount',
    'export_order' => 'Export Order',
    'order_status' => 'Order Status',
    'insurance_plan' => 'Insurance Plan',
    'promotion_id' => 'Promotion ID',
    'discount' => 'Discount',
    'order_expire' => 'Order Expire',
    'balance_required' => 'Balance Required',
    'policy_gross' => 'Policy Gross Premium',
    'theft_gross' => 'Theft Gross Premium',
    'taxi_gross' => 'Taxi Gross Premium',
    'hb_gross' => 'HB Gross Premium',
    'carloss_gross' => 'Carloss Gross Premium',
    'cctv' => 'CCTV',

    'prefix' => 'KHUN',
    






   


];