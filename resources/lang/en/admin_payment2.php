<?php

return [

    'topic' => 'Payment',
    'search' => 'Search',
    'no' => "No",
    'payment_no' => "Payment Number",
    'order_number' => "Order Number",
    'policy_number' => "Policy Number",
    'amount' => "Payment Amount",
    'status' => "Status",
    'paid_date' => 'Paid Date',
    'created_date' => "Create Date",
    "action" => "Action",
    'paid_date_from' => 'Paid Date Form',
    'paid_date_to' => 'Paid Date To',
    'payment_data' => 'Payment Information',
    'merchant_id' => 'Merchant ID',
    'currency_code' => 'Currency Code',
    'description' => 'Product Description',
    'status_detail' => 'Status Detail',
    'updated_at' => 'Lastest Update'


];
