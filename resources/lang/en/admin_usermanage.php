<?php

return [
    'topic' => 'Admin',
    'add_admins' => 'Add Admins',
    'no' => 'No',
    'name' => 'Name',
    'username' => 'Username',
    'email' => 'Email',
    'privilege' => 'Privilege',
    'action' => 'Action'


];
