<?php

return [

    'topic' => 'Garage',
    'add_garage' => 'Add Garage',
    'search' => 'Search',
    'clear' => 'Clear',
    'type_class' => 'Type Class',
    'type' => 'Type',
    'province' => 'Provice',
    'district' => 'District',
    'sub_district' => 'Sub District',
    'name' => 'Name',
    'name_en' => 'Name(English)',
    'no' => 'No',
    'action' => 'Action',

    'topic_type' => 'Garage Type',
    'add_type' => 'Add Garage Type',

    'topic_form' => 'Add/Edit Garage',
    'garage_name' => 'Garage Name',
    'telephone' => 'Telephone',
    'fax' => 'Fax',
    'lattitude' => 'Latitude',
    'longitude' => 'Longitude',
    'address' => 'Address',
    'submit' => 'Submit',
    'add' => 'Add',
    'edit' => 'Edit',
    'close' => 'Close',
    'save' => 'Save Change'




];
