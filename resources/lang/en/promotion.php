<?php

return [
    'topic'=>'Sign up to receive the benefits',
    'old_insurance_expire'=>'The conclusion date of old policy',
    'let' => 'Let\' Sign Up',
    
    'promotion_inactive' => 'Promotion Not Found.',
    'promotion_expired' => 'Promotion has been expired.',
    'promotion_register_expired' => 'Registration has been expired.',
    'promotion_max_grant' => 'The maximum amount of registration has already reached.',
    'register_fail' => 'Register failed, try again.',
    'register_success' => 'Register Success',
    'register_exis' => 'This email already exists',
    'vehicle_brand' => 'Car Brand',
    'vehicle_model' => 'Car Model',
    'thankyou' => 'Thank you for your register.'
    
 ];
 
