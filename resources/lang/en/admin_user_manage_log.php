<?php

return [

    'topic' => 'User Management Log',
    'search' => 'Search',    
    'clear' => 'Clear', 
    'action_date_from' => 'Action Date From', 
    'action_date_to' => 'Action Date To', 
    'no' => 'No', 
    'edit_data' => 'Edit Data', 
    'old_data' => 'Old Data', 
    'create_date' => 'Created Date', 
    'create_by' => 'Created By', 
    'field' => 'Field',
    'data' => 'Data',
    'username' => 'Username',
    'password' => 'Password',
    'name' => 'Name',
    'lastname' => 'Lastname',
    'email' => 'Email',
    'privilege' => 'Privilege',
    'status' => 'Status',

];
