<?php

return [

    'topic' => 'Pricing List',
    'search' => "Search",
    'clear' => 'Clear',
    'select_plan' => 'Select Plan',
    'ft_si' => 'FT SI/TPPD SI',
    'car_code' => 'Car Code',
    'car_engine' => 'Car Engine',
    'define_name' => 'Define Name',
    'mortor_code' => 'Mortor Package Code',
    'no' => 'No',
    'sompo_plan' => 'Sompo Plan',
    'garage_type' => 'Garage Type',
    'gross_premium' => 'Gross Premium',
    'action' => 'Action',

    'pricing_plan' => 'Pricing Plan',
    'manage_plan' => 'Add/Edit Pricing Plan',
    'standard' => 'Standard Input',
    'submit' => 'Submit',

    'add' => 'Add',
    'edit' => 'Edit',
    'deductible' => 'Deductible',
    'addition_coverge' => 'Addition Coverage',
    'type' => 'Type',
     
];