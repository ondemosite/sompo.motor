<?php

return [

    'topic' => 'Profile',
    'setting' => 'Setting',
    'password' => 'Password',
    'privilege' => 'Privilege',
    'select_privilege' => 'Select Privilege',
    'name' => 'Name',
    'lastname' => 'Lastname',
    'username' => 'Username',
    'email' => 'E-mail',
    'verify' => 'Verify password',
    'submit' => 'Submit',
    'change' => 'Change',
    'old_password' => 'Old password',
    'new_password' => 'New password',
    'verify_password' => 'Verify password',
    'change_password' => 'Change password',
    'create' => 'Create',
    'profile' => 'Profile',
    'small-y' => 'Your account for login.',
    'mini' => 'Minimum 6 characters',

];
