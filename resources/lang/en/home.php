<?php

return [
    'find_insurance' => 'Find the insurance that match your car ',
    'your_age' => 'Your Age',
    'year' => 'Years',
    'overyear' => 'Years Over',
    'submit_find' => 'Calculate',
    'find' => 'Find',
    'garage' => 'Garage',
    'place' => 'Place',
    'garage_name' => 'Garage Name',
    'faq' => 'Faq',
    'cert' => 'Standards certification ',
    'channel' => 'secure payment channel',
    'required_brand' => 'Please select car brand',
    'required_model' => 'Please select car model',
    'required_year' => 'Please select Model Year/Register Year',
    'required_submodel' => 'Please select model detail',
    ''

];