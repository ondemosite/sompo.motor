<?php

return [
    'topic' => 'ผู้ใช้',
    'add_admins' => 'เพิ่มผู้ใช้',
    'no' => 'ลำดับ',
    'name' => 'ชื่อ',
    'username' => 'ผู้ใช้',
    'email' => 'อีเมล',
    'privilege' => 'สิทธิ์',
    'action' => 'จัดการ'


];
