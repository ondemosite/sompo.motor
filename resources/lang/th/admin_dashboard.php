<?php

return [

    'dashboard' => 'หน้าหลัก',
    'earned_today' => 'รายได้วันนี้',
    'total_income' => 'รายได้ทั้งหมด',
    'active_insurance' => 'กรมธรรม์ที่มีผล',
    'insurance_popular' => 'กรมธรรม์ยอดนิยม',
    'monthly_summary' => 'สรุปยอดขายแต่ละเดิอน',
    'more' => 'เพิ่มขึ้น',
    'monthly_visitor' => 'ยอดผู้เข้าชมเว็บไซต์',
    'used_ram' => 'พื้นที่ใช้งานแรม',
    'hard_disk' => 'พื้นที่เก็บข้อมูล',
    'new_orders' => 'รายการสั่งซื้อล่าสุด',
    'order_number' => 'เลขที่สั่งซื้อ',
    'plan_name' => 'กรมธรรม์',
    'full_name' => 'ชื่อ นามสกุล',
    'contact' => 'ติดต่อ',
    'amount' => 'ยอดชำระ',
    'created_at' => 'วันที่ทำรายการ',
    'status' => 'สถานะ',
    'monthly_earned_stat' => 'สถิติรายได้แต่ละเดือน (บาท / เดือน)',

];
