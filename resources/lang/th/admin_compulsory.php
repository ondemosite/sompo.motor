<?php

return [

    'compulsory_topic' => 'รายการข้อมูล พ.ร.บ.',
    'add_compulsory' => 'เพิ่มข้อมูล พ.ร.บ.',
    'no' => 'ลำดับ',
    'body_type' => 'ประเภทโครงสร้างรถยนต์',
    'mortor_code_ac' => 'Motor Code AC',
    'net_premium' => 'เบี้ยสุทธิ',
    'stamp' => 'แสตมป์',
    'vat' => 'VAT',
    'gross_premium' => 'เบี้ยประกัน',
    'action' => 'จัดการ',
    'close' => 'ปิด',
    'save' => 'บันทึก'

];
