<?php

return [

    'topic' => 'บันทึกรายการจัดส่งเอกสาร',
    'search' => 'ค้นหา',
    'select_type' => 'เลือกประเภท',
    'policy_number' => 'เลขที่ประกันภัย',
    'select_status' => 'เลือกสถานะ',
    'resend_date_from' => 'ส่งเอกสารวันที่ จาก',
    'resend_date_to' => 'จัดส่งเอกสารวันที่ ถึง',
    'clear' => 'ล้าง',
    'no' => 'ลำดับ',
    'type' => 'ประเภท',
    'policy_number' => 'เลขที่กรมธรรม์',
    'status' => 'สถานะ',
    'reason' => 'เหตุผล',
    'created_by' => 'สร้างโดย',
    'created_date' => 'สร้างวันที่',
    'data' => 'ข้อมูลที่ส่ง'

];
