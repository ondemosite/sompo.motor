<?php

return [

    'topic' => 'อู่',
    'add_garage' => 'เพิ่มอู่',
    'search' => 'ค้นหา',
    'clear' => 'ล้าง',
    'type_class' => 'ประเภทอู่',
    'type' => 'ประเภทบริการ',
    'province' => 'จังหวัด',
    'district' => 'อำเภอ/เขต',
    'sub_district' => 'ตำบล/แขวง',
    'name' => 'ชื่อ',
    'name_en' => 'ชื่อ (ภาษาอังกฤษ)',
    'no' => 'ลำดับ',
    'action' => 'จัดการ',

    'topic_type' => 'ประเภทอู่(บริการ)',
    'add_type' => 'เพิ่ม ประเภทอู่(บริการ)',

    'topic_form' => 'เพิ่ม/แก้ไข อู่',
    'garage_name' => 'ชื่ออู่',
    'telephone' => 'โทรศัพท์',
    'fax' => 'แฟกซ์',
    'lattitude' => 'ละติจูด',
    'longitude' => 'ลองติจูด',
    'address' => 'ที่อยู่',
    'submit' => 'ยืนยัน',
    'add' => 'เพิ่ม',
    'edit' => 'แก้ไข',
    'close' => 'ปิด',
    'save' => 'บันทึก'




];
