<?php

return [
    'home'=>'หน้าหลัก',
    'product'=>'ผลิตภัณฑ์',
    'garage' => 'ศูนย์ซ่อม',
    'faq' => 'คำถามที่พบบ่อย',
    'contactus' => 'ติดต่อเรา',
    'calculate' => 'คำนวณเบี้ยประกันภัย'
 ];
 
