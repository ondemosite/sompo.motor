<?php

return [

    '2plus_detail' => 'ประกันภัยประเภท 2+',
    '3plus_detail' => 'ประกันภัยประเภท 3+',
    'type3_detail' => 'ประกันภัยประเภท 3',
    'outside_coverage' => 'ความรับผิดชอบต่อบุคคลภายนอก',
    'damage' => 'ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย',
    'per_once' => 'ต่อครั้ง',
    'per_person' => 'ต่อคน',
    'stuff_damage' => 'ความเสียหายต่อทรัพย์สิน',
    'under_coverage' => 'ความคุ้มครองตามเอกสารแนบท้าย',
    'death_disable' => 'เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร ต่อคน',
    'medical_free' => 'ค่ารักษาพยาบาล ต่อคน',
    'bail_driver' => 'วงเงินการประกันตัวผู้ขับขี่',
    'submit' => 'ยีนยัน',
    'general' => 'ตั้งค่าทั่วไป',
    'max_age' => 'อายุสูงสุดของรถยนต์ (ปี)',
    'max_pre' => 'ซื้อกรมธรรม์ล่วงหน้าได้สูงสุด (วัน)'
];
