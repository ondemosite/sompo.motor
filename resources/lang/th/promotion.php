<?php

return [
    'topic'=>'ลงทะเบียนเพื่อรับโปรโมชั่น',
    'old_insurance_expire'=>'วันที่กรมธรรม์เดิมหมดอายุ',
    'let' => 'ลงทะเบียนรับสิทธิ์',
    
    'promotion_inactive' => 'ไม่พบข้อมูลโปรโมชั่น',
    'promotion_expired' => 'โปรโมชั่นสิ้นสุดแล้ว',
    'promotion_register_expired' => 'สิ้นสุดระยะเวลาลงทะเบียน',
    'promotion_max_grant' => 'จำนวนสิทธิ์ที่ได้รับเต็มแล้ว',
    'register_fail' => 'ลงทะเบียนล้มเหลว ลองใหม่อีกครั้ง',
    'register_success' => 'ลงทะเบียนสำเร็จแล้ว',
    'register_exis' => 'อีเมลนี้ได้รับการลงทะเบียนแล้ว',
    'vehicle_brand' => 'รุ่นรถ',
    'vehicle_model' => 'โมเดล',
    'thankyou' => 'ขอบคุณที่ลงทะเบียน'
 ];
 
