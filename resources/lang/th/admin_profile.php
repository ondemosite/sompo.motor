<?php

return [

    'topic' => 'โปรไฟล์',
    'setting' => 'ตั้งค่า',
    'password' => 'พาสเวิด',
    'privilege' => 'สิทธิ์',
    'select_privilege' => 'เลือกสิทธิ์',
    'name' => 'ชิ่อ',
    'lastname' => 'นามสกุล',
    'username' => 'ชื่อผู้ใช้',
    'email' => 'อีเมล',
    'verify' => 'ตรวจสอบพาสเวิด',
    'submit' => 'ยืนยัน',
    'change' => 'บันทึก',
    'old_password' => 'พาสเวิดเดิม',
    'new_password' => 'พาสเวิดใหม่',
    'verify_password' => 'ยีนยันพาสเวิด',
    'change_password' => 'เปลี่ยนพาสเวิด',
    'create' => 'สร้าง',
    'profile' => 'โปรไฟล์',
    'small-y' => 'ชื่อผู้ใช้งานสำหรับเข้าระบบ',
    'mini' => 'อย่างน้อย 6 ตัวอักษร'

];
