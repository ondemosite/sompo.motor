<?php

return [

    'menu' => 'เมนู',
    'permission' => 'สิทธิ์การใช้งาน',
    'add_menu' => 'เพิ่ม สิทธิ์การใช้งาน',
    'update_menu' => 'แก้ไขเมนู',
    'parent_menu' => 'เมนู ขั้นก่อน',
    'menu_name' => 'ชื่อเมนู',
    'menu_name_th' => 'ชื่อเมนู ภาษาไทย',
    'menu_path' => 'ลิงค์',
    'menu_path_example' => 'motoradmins/menu',
    'icon' => 'ไอคอน',
    'icon_example' => 'ti-user',
    'orders'=>'ลำดับ',
    'orders_example' => '10.1',

    'th_menu' => 'เมยู',
    'th_view' => 'ดู',
    'th_add' => 'เพิ่ม',
    'th_edit' => 'แก้ไข',
    'th_delete' => 'ลบ',
    'save' => 'บันทึก',
    'display' => 'แสดง',
    'close' => 'ปิด'

];
