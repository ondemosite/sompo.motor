<?php

return [
    'no' => 'ลำดับ',
    'topic' => 'ใบเสร็จ',
    'search' => 'ค้นหา',
    'receipt_number' => 'เลขที่ใบเสร็จ',
    'payment_number' => 'เลขที่ใบชำระเงิน',
    'credit_number' => 'หมายเลขบัตรเครดิต',
    'date_from' => 'สร้างวันที่ จาก',
    'date_to' => 'สร้างวันที่ ถึง',
    'amount' => 'ยอดชำระ',
    'created_at' => 'วันที่สร้าง',
    'action' => 'จัดการ',
    'receipt_data' => "ข้อมูลใบเสร็จ",
    'channel_code' => 'Payment Channel Code',
    'payment_scheme' => 'Payment Scheme'


];
