<?php

return [

    'topic' => 'โปรโมชั่น',
    'title' => 'หัวข้อ',
    'search' => 'ค้นหา',
    'clear' => 'ล้าง',   
    'discount' => 'ส่วนลด (%)',
    'no' => 'ลำดับ',
    'code' => 'โค๊ด',
    'discount_2' => 'ส่วนลด',
    'start_date' => 'เริ่มต้นวันที่',
    'expire_date' => 'สิ้นสุดวันที่',
    'received' => 'ได้รับแล้ว',
    'status' => 'สถานะ',
    'action' => 'จัดการ',
    
    'manage_topic' => 'เพิ่ม/แก้ไข โปรโมชั่น',
    'promotion_title' => 'ชื่อโปรโมชั่น',
    'promotion_code' => 'โปรโมชั่น โค๊ด',
    'maximum_grant' => 'จำนวนสูงสุด',
    'active_plan' => 'แผน',
    'submit' => 'บันทึก',
    'add' => 'เพิ่ม',
    'edit' => 'แก้ไข',

    'regis_name' => 'ชื่อ - นามสกุล',
    'regis_email' => 'อีเมล',
    'regis_tel' => 'เบอร์โทรศัพท์',
    'regis_expire' => 'วันที่กรมธรรม์เดิมหมดอายุ',
    'booking_register' => 'ลงทะเบียนล่วงหน้า',
    'register_amount' => 'จำนวนสิทธิ์',
    'register_list' => 'รายชื่อ',
    'created_at' => 'สมัครเมื่อ',
    'vehicle_brand' => 'ยี่ห้อรถ',
    'vehicle_model' => 'รุ่นรถ',
    'select_vehicle_model' => 'เลือกรุ่นรถ',
    'select_vehicle_brand' => 'เลือกยี่ห้อรถ',
    'ending_message' => 'ข้อความแสดงหลัง Key Promotion'
];
