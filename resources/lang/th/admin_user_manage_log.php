<?php

return [

    'topic' => 'บันทึกรายการแก้ไขข้อมูลผู้ใช้',
    'search' => 'ค้นหา',    
    'clear' => 'ล้าง', 
    'action_date_from' => 'ทำรายการวันที่ จาก', 
    'action_date_to' => 'ทำรายการวันที่ ถึง', 
    'no' => 'ลำดับ', 
    'edit_data' => 'ข้อมูลที่แก้ไข', 
    'old_data' => 'ข้อมูลเก่า', 
    'create_date' => 'ทำรายการวันที่', 
    'create_by' => 'ทำรายการโดย', 
    'field' => 'Field',
    'data' => 'ข้อมูล',
    'username' => 'ผู้ใช้',
    'password' => 'พาสเวิด',
    'name' => 'ชื่อ',
    'lastname' => 'นามสกุล',
    'email' => 'อีเมล',
    'privilege' => 'สิทธิ์',
    'status' => 'สถานะ',

];
