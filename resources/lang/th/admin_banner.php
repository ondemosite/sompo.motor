<?php

return [

    'topic' => 'แบนเนอร์',
    'no' => 'ลำดับ',
    'title' => 'หัวเรื่อง',
    'link' => 'เชื่อมโยง',
    'view' => 'ดู',
    'action' => 'จัดการ',
    'add' => 'เพิ่ม',
    'edit' => 'แก้ไข',
    'change' => 'เปลี่ยน',
    'description' => 'รายละเอียด',
    'order' => 'ลำดับ',
    'submit' => 'ยืนยัน',
    'description' => 'รายละเอียด'


];