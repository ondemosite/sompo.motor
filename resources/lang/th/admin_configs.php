<?php

return [

    'topic' => 'API Configs',
    'no' => 'ลำดับ',
    'title' => 'ชื่อ API',
    'start' => 'วันที่เริ่ม',
    'end' => 'วันที่สิ้นสุด',
    'update' => 'แก้ไขล่าสุด',
    'action' => 'จัดการ',
    'value' => 'ค่า Token',
    'code' => 'ประเภท Token',
    'status' => 'สเตตัส',


];