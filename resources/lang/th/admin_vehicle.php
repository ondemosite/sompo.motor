<?php

return [
    'topic' => 'รายการรถยนต์',
    'vehicle' => 'รถยนต์',
    'search' => 'ค้นหา',
    'clear' => 'ล้าง',
    'body_type' => 'ประเภทโครงสร้าง',
    'brand' => 'ยี่ห้อ',
    'model' => 'รุ่น',
    'year' => 'ปี',
    'model_type' => 'รุ่นย่อย',
    'model_type2' => 'รุ่นย้อย (สำหรับแสดง)',
    'mortor_av' => 'Mortor Code AV',
    'motor_ac' => 'Mortor Code AC',
    'action' => 'จัดการ',

    'edit' => 'แก้ไข',
    'add' => 'เพิ่ม',
    'topic_form' => 'เพิ่ม/แก้ไข รถยนต์',
    'cc' => 'CC',
    'tons' => 'Tons',
    'car_seat' => 'จำนวนที่นั่ง',
    'driver_passenger' => 'ผู้โดยสาร',
    'red_plate' => 'ป้ายแดง',
    'used_car' => 'Used Car',
    'car_age' => 'Car Age',
    'submit' => 'ยีนยัน',

    'topic_brand' => 'ยี่ห้อรถยนต์',
    'top_order' => 'ลำดับแรก',
    'brand_name' => 'ชื่อยี่ห้อ',

    'topic_model' => 'รุ่นรถยนต์',
    'model_name' => 'ชื่อรุ่น',

    'no' => 'ลำดับ',
    'close' => 'ปิด',
    'save' => 'บันทึก'


]; 
