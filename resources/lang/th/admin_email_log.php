<?php

return [

    'topic' => 'ส่งข้อมูลผ่านอีเมล',
    'endorse_no' => 'Endorse No.',
    'files' => 'ไฟล์ที่จัดส่ง',
    'response' => 'ข้อมูลตอบกลับ'
];
