<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'ข้อมูล :attribute ต้องมีการยอมรับ',
    'active_url'           => 'ข้อมูล :attribute URL ไม่ถูกต้อง',
    'after'                => 'ข้อมูล :attribute ต้องเป็นวันที่หลังจาก :date.',
    'after_or_equal'       => 'ข้อมูล :attribute ต้องเป็นวันที่หลังจาก หรือ เท่ากับ :date.',
    'alpha'                => 'ข้อมูล :attribute ต้องเป็นตัวอักษรเท่านั้น',
    'alpha_dash'           => 'ข้อมูล :attribute ต้องเป็นตัวอักษร, ตัวเลข, และ dash.',
    'alpha_num'            => 'ข้อมูล :attribute ต้องเป็นตัวอักษร หรือ ตัวเลข',
    'array'                => 'ข้อมูล :attribute must be an array.',
    'before'               => 'ข้อมูล :attribute must be a date before :date.',
    'before_or_equal'      => 'ข้อมูล :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'ข้อมูล :attribute field must be true or false.',
    'confirmed'            => 'ข้อมูล :attribute confirmation does not match.',
    'date'                 => 'ข้อมูล :attribute รูปแบบไม่ถูกต้อง',
    'date_format'          => 'ข้อมูล :attribute รูปแบบไม่ถูกต้อง :format.',
    'different'            => 'ข้อมูล :attribute และ :other ต้องแตกต่างกัน',
    'digits'               => 'ข้อมูล :attribute ต้องเป็น :digits หลัก',
    'digits_between'       => 'ข้อมูล :attribute ต้องอยู่ระหว่าง :min และ :max หลัก',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'ข้อมูล :attribute รูปแบบ อีเมล ไม่ถูกต้อง',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'ข้อมูล :attribute ต้องเป็นไฟล์',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'ข้อมูล :attribute ต้องเป็นรูปภาพ',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'ข้อมูล :attribute ต้องเป็นตัวเลขจำนวนเต็ม',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'ข้อมูล :attribute ต้องเป็นตัวเลข',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'ข้อมูล :attribute จำเป็นต้องระบุ',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'ข้อมูล :attribute ต้องมีขนาด :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'ข้อมูล :attribute ต้องเป็นสายอักขระ',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'ข้อมูล :attribute รูปแบบไม่ถูกต้อง',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'common-all' => "Please type the valid data.",
        'delete_admin' => "You can't delete Admin Role",
        'admin_role' => "You can't edit Admin Role",
        'edit_admin' => "You can't edit Admin User",
        'delete_admin_user' => "You can't delete Admin User",
        'edit_admin_permission' => "You can't edit Admin Permission",
        'document_size' => 'ขนาดของไฟล์ต้องไม่เกิน 2000 กิโลไบต์',
        'document_mimes' => 'นามสกุลไฟล์ไม่ถูกต้อง',
        'required' => 'กรุณาระบุข้อมูลให้ถูกต้อง',
        'number' => 'เฉพาะตัวเลขเท่านั้น',
        'idcard' => 'รูปแบบหมายเลขประจำตัวประชาชนไม่ถูกต้อง',
        'tel_length' => 'หมายเลขเบอร์โทรศัพท์ต้องมี 10 ตัว',
        'email' => 'รูปแบบ อีเมล ไม่ถูกต้อง',
        'alpha' => 'เฉพาะตัวเลขและตัวอักษร',
        'max_20' => 'จำนวนไม่เกิน 20 ตัวอักษร',
        'uniqe_chassis' => 'เลขตัวถังที่ระบุมีอยู่แล้วในระบบ',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],
    

];
