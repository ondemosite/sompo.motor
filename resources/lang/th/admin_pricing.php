<?php

return [

    'topic' => 'รายการราคา',
    'search' => "ค้นหา",
    'clear' => 'ล้าง',
    'select_plan' => 'เลือกแผน',
    'ft_si' => 'ทุนประกัน',
    'car_code' => 'รหัสรถยนต์',
    'car_engine' => 'เครื่องยนต์',
    'define_name' => 'ระบุอายุ',
    'mortor_code' => 'Mortor Package Code',
    'no' => 'ลำดับ',
    'sompo_plan' => 'แผนประกัน',
    'garage_type' => 'ประเภทอู่',
    'gross_premium' => 'เบี้ยประกัน',
    'action' => 'จัดการ',

    'pricing_plan' => 'แผนราคา',
    'manage_plan' => 'เพิ่ม/แก้ไข แผนราคา',
    'standard' => 'ข้อมูลพื้นฐาน',
    'submit' => 'ยีนยัน',

    'add' => 'เพิ่ม',
    'edit' => 'แก้ไข',
    'deductible' => 'ค่าเสียหายส่วนแรก',
    'addition_coverge' => 'ความคุ้มครองเสริม',
    'type' => 'ประเภท',
];