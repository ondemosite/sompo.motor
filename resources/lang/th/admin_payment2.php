<?php

return [

    'topic' => 'ใบชำระเงิน',
    'search' => 'ค้นหา',
    'no' => "ลำดับ",
    'payment_no' => "เลขที่ใบชำระเงิน",
    'order_number' => "เลขที่ใบสั่งซื้อ",
    'policy_number' => "เลขที่กรมธรรม์",
    'amount' => "ยอดชำระ",
    'status' => "สถานะ",
    'paid_date' => 'วันที่ชำระเงิน',
    'created_date' => "วันที่สร้าง",
    "action" => "จัดการ",
    'paid_date_from' => 'วันที่ชำระเงิน จาก',
    'paid_date_to' => 'วันที่ชำระเงิน ถึง',
    'payment_data' => 'รายละเอียดใบชำระเงิน',
    'merchant_id' => 'รหัสร้านค้า',
    'currency_code' => 'รหัสสกุลเงิน',
    'description' => 'รายละเอียดผลิตภันฑ์',
    'status_detail' => 'รายละเอียดสถานะ',
    'updated_at' => 'แก้ไขล่าสุด'

];
