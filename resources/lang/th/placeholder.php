<?php

return [
        'province' => 'จังหวัด',
        'district' => 'อำเภอ',
        'type_name' => 'ระบุชื่ออู่',
        'select_birthdate' => 'กรุณาเลือกวันที่',
        'please_select' => 'กรุณาเลือก',

        'model' => 'รุ่น',
        'model_year' => 'รุ่นปี/ปีจดทะเบียนรถ',
        'sub_model' => 'รุ่นย่อย',

        'select_province' => 'เลือก จังหวัด',
        'select_district' => 'เลือก เขต / อำเภอ',
        'select_sub_district' => 'เลือก แขวง / ตำบล',

        'select_body_type' => 'เลือกประเภทตัวถัง',
        'select_year' => 'เลือกปี',
        'select_brand' => 'เลือกยี่ห้อ',
        'select_model' => 'เลือกรุ่น',
        'select_model_type' => 'เลือกรุ่นย่อย',
        'type_cost' => 'ระบุจำนวนเงินที่คุณพอใจ',

        'specify_insurance' => 'Please Specify amount',
        'start_date' => 'Start Date',
        'expire_date' => 'Expire Date',
        'select_status' => 'Select Status',
        'clear' => 'Clear',

        'stamp_percent' => 'stamp (%)',
        'vat_percent' => 'vat (%)',
];