<?php

use Illuminate\Database\Seeder;

use \App\Models\Privilege;
use \App\Models\Menus;
use \App\Models\Admins;
use \App\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        //Privilege ---------------------
        $privilege = new Privilege();
        $privilege->name = "Super Admin";
        $privilege->save();

        $privilege = new Privilege();
        $privilege->name = "Admin";
        $privilege->save();

        //Menus ---------------------
        $menus = new Menus();
        $menus->name="System";
        $menus->parent_id=0;
        $menus->path=null;
        $menus->icon="fa-cog";
        $menus->orders=1;
        $menus->save();

        $menus = new Menus();
        $menus->name="Admins";
        $menus->parent_id=1;
        $menus->path="admins";
        $menus->icon=null;
        $menus->orders=1.1;
        $menus->save();

        $menus = new Menus();
        $menus->name="Privilege";
        $menus->parent_id=1;
        $menus->path="privilege";
        $menus->icon=null;
        $menus->orders=1.2;
        $menus->save();

        $menus = new Menus();
        $menus->name="Menu & Permission";
        $menus->parent_id=1;
        $menus->path="menu";
        $menus->icon=null;
        $menus->orders=1.3;
        $menus->save();
        
        //Permission ---------------------
        $permission = new Permission();
        $permission->privilege_id = 1;
        $permission->menu_id = 1;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        $permission = new Permission();
        $permission->privilege_id = 1;
        $permission->menu_id = 2;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        $permission = new Permission();
        $permission->privilege_id = 1;
        $permission->menu_id = 3;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        $permission = new Permission();
        $permission->privilege_id = 1;
        $permission->menu_id = 4;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        $permission = new Permission();
        $permission->privilege_id = 2;
        $permission->menu_id = 1;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        $permission = new Permission();
        $permission->privilege_id = 2;
        $permission->menu_id = 2;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        $permission = new Permission();
        $permission->privilege_id = 2;
        $permission->menu_id = 3;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        $permission = new Permission();
        $permission->privilege_id = 2;
        $permission->menu_id = 4;
        $permission->view = 1;
        $permission->add = 1;
        $permission->edit = 1;
        $permission->delete = 1;
        $permission->save();

        //Admins ---------------------
        $admin = new Admins();
        $admin->username = "superadmin";
        $admin->password = Hash::make("j8v2z320");
        $admin->name = "Wanus";
        $admin->lastname = "Chansama";
        $admin->email = "wanus.tob@gmail.com";
        $admin->privilege = 1;
        $admin->save();

        $admin = new Admins();
        $admin->username = "admin";
        $admin->password = Hash::make("j8v2z320");
        $admin->name = "admin";
        $admin->lastname = "admin";
        $admin->privilege = 2;
        $admin->save();


    }
}
