<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('formular',100);
            $table->string('title',200);
            $table->string('code',200);
            $table->float('discount',8,2)->nullable();
            $table->dateTime('start_at')->nullable();
            $table->dateTime('expire_at')->nullable();
            $table->string('coverage_id',10);
            $table->integer('maximum_grant')->default(1);
            $table->integer('receive_amount')->default(0);

            $table->text('vehicle_brand')->nullable();
            $table->text('vehicle_model')->nullable();
            
            $table->integer('is_register')->default(0);
            $table->integer('max_register')->default(0);
            $table->dateTime('register_start_at')->nullable();
            $table->dateTime('register_expire_at')->nullable();
            $table->string('cover_register',200)->nullable();

            $table->string('ending_message',300)->nullable();

            $table->integer('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion');
    }
}
