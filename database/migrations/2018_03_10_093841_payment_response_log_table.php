<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentResponseLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_response_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number',30); //show
            $table->float('amount',8,2)->nullable(); //show
            $table->string('transaction_ref',30)->nullable(); //show
            $table->string('approval_code',30)->nullable(); 
            $table->string('eci',5)->nullable(); 
            $table->string('payment_channel',5)->nullable(); //show
            $table->string('payment_status',5)->nullable(); //show
            $table->string('channel_response_code',5)->nullable();
            $table->text('channel_response_desc')->nullable();
            $table->string('masked_pan',20)->nullable(); //show
            $table->string('payment_scheme',5)->nullable(); //show
            $table->string('process_by',5)->nullable(); //show
            $table->string('browser_info',200)->nullable();
            $table->datetime('request_at')->nullable(); //show
            $table->datetime('response_at')->nullable(); //show
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_response_log');
    }
}
