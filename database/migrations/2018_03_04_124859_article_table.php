<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('category',['WHYSOMPO','OURPRODUCT']);
            $table->string('title_en',100);
            $table->string('title_th',100);
            $table->string('link',100)->nullable();
            $table->integer("order");
            $table->integer("status");
            $table->string('cover_path_en',100);
            $table->string('cover_path_th',100);
            $table->text('detail_en',100)->nullable();
            $table->text('detail_th',100)->nullable();
            $table->integer("viewer");
            $table->integer('created_by')->default(1);
            $table->integer('updated_by')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
