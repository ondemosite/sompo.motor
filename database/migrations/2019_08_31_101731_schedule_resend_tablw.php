<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScheduleResendTablw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_resend', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type',['EMAIL','ENDORSE']);
            $table->integer('policy_id');
            $table->integer('retry_remaining')->default(3);
            $table->enum('complete_status',['SUCCESS','PENDING','WAITING','FAILED']);
            $table->integer('process_status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
