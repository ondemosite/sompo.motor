<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriverInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prefix')->nullable();
            $table->string('prefix_name',100)->nullable();
            $table->string('name',100);
            $table->string('lastname',100);
            $table->string('idcard',20);
            $table->string('licence',20)->nullable();
            $table->dateTime('birth_date');
            $table->enum('gender',['MALE','FEMALE']);
            $table->text('address')->nullable();
            $table->string('province',5)->nullable();
            $table->string('district',5)->nullable();
            $table->string('subdistrict',5)->nullable();
            $table->string('province_name',100)->nullable();
            $table->string('district_name',100)->nullable();
            $table->string('subdistrict_name',100)->nullable();
            $table->string('province_name_th',100)->nullable();
            $table->string('district_name_th',100)->nullable();
            $table->string('subdistrict_name_th',100)->nullable();
            $table->string('postalcode',5)->nullable();
            $table->string('tel',20)->nullable();
            $table->string('email',100)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_info');
    }
}
