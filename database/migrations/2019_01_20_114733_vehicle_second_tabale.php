<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VehicleSecondTabale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_second', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('brand_id');
            $table->integer('model_id');
            $table->integer('year');
            $table->enum('body_type', ['SEDAN', 'PICKUP']);
            $table->string('model_type', 200);
            $table->string('model_type_full', 100)->nullable();
            $table->integer('mortor_code_av');
            $table->float('mortor_code_ac',8, 2);
            $table->integer('cc');
            $table->float('tons', 8, 2);
            $table->integer('car_seat');
            $table->string('driver_passenger',10);
            $table->integer('red_plate');
            $table->integer('used_car');
            $table->integer('car_age');
            $table->integer('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
