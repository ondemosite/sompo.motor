<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EndorseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endorse', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->integer('id');
            $table->integer('policy_id');
            $table->string('endorse_no',2);

            //Main Profile
            $table->integer('owner'); //ข้อมูลผู้เอาประกัน
            $table->integer('driver1')->nullable(); //ข้อมูลผู้ขับคนที่ 1
            $table->integer('driver2')->nullable(); //ข้อมูลผู้ขับคนที่ 2

            //Policy Document
            $table->string('policy_document_path',200)->nullable(); // Policy
            $table->string('policy_original_invoice_path',200)->nullable(); // Original Receipt
            $table->string('policy_copy_invoice_path',200)->nullable(); //Copy Receipt
            $table->string('compulsory_document_path',200)->nullable(); // Compulsory
            $table->string('compulsory_original_invoice_path',200)->nullable(); // Original Receipt
            $table->string('compulsory_copy_invoice_path',200)->nullable(); // Copy Receipt

            $table->integer('is_print_policy')->default(0);
            $table->integer('is_print_original_invoice')->default(0);
            $table->integer('is_print_copy_invoice')->default(0);
            $table->integer('is_print_compulsory')->default(0);

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->primary(['id', 'policy_id','endorse_no']);
        });
        Schema::table('endorse', function (Blueprint $table) {
            $table->integer('id', true, true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endorse');
    }
}
