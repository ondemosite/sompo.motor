<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy', function (Blueprint $table) {
            //Order Header
            $table->increments('id'); // ID
            $table->string('policy_number',50);
            $table->string('policy_addon_number',50)->nullable();
            $table->string('policy_com_number',50)->nullable();
            $table->string('tax_note_number',50);
            $table->string('com_tax_note_number',50)->nullable();
            $table->integer('order_id');
            $table->enum('status',['NORMAL','EDIT','CANCEL','EXPIRE','INACTIVE'])->default('NORMAL'); // POLICY STATUS
            $table->integer('is_advice')->default(0);
            $table->integer('is_post')->default(0);
            $table->integer('is_personal')->default(0);
            $table->integer('is_email')->default(0);
            $table->integer('is_disclosure')->default(0);
            $table->integer('is_true')->default(0);
            $table->integer('is_agree')->default(0);
            $table->text('note')->nullable();
            $table->integer('owner')->nullable();
            //Car Info
            $table->integer('vehicle_info');
            $table->string('car_licence',10);
            $table->string('car_province',5);
            $table->string('car_chassis_number',30)->nullable();
            $table->integer('car_cctv');
            
            //Insurance Data
            $table->integer('insurance_plan_id'); //ทำไว้รองรับ Dynamic
            $table->string('insurance_plan_name',100); //แบบ static
            $table->dateTime('insurance_start'); //เริ่มคุ้มครอง
            $table->dateTime('insurance_expire'); //หมดคุ้มครอง
            $table->integer('insurance_ft_si'); //ทุนประกัน1
            $table->enum('insurance_garage_type',['GENERAL','DEALER','NO']); //ซ่อมอู่
            $table->float('insurance_deduct',8,2); //ความเสียหายส่วนแรก
            $table->integer('insurance_person_damage_once'); //ความรับผิดต่อบุคคลภายนอก/ ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย ต่อครั้ง
            $table->integer('insurance_person_damage_person'); //ความรับผิดต่อบุคคลภายนอก/ ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย ต่อคน
            $table->integer('insurance_stuff_damage'); //ความเสียหายต่อทรัพย์สิน
            $table->integer('insurance_death_disabled'); //อุบัติเหตุส่วนบุคคล
            $table->integer('insurance_medical_fee'); //ค่ารักษาพยาบาล
            $table->integer('insurance_bail_driver'); //การประกันตัวผู้ขับขี่ (ต่อครั้ง)
            $table->integer('insurance_driver_amount'); //จำนวนผู้ขับขี่ประจำของรถคันนี้

            //Compusory
            $table->string('sticker_no',50)->nullable();
            $table->dateTime('compulsory_start')->nullable();
            $table->dateTime('compulsory_expire')->nullable();

            //Insurace Addition
            $table->double('addon_theft',15,8)->nullable(); //คุ้มครองการโจรกรรมทรัพย์สิน
            $table->double('addon_taxi',15,8)->nullable();//ค่าเดินทาง
            $table->double('addon_hb',15,8)->nullable(); 
            $table->double('addon_carloss',15,8)->nullable();

            //Document
            $table->string('document_path_personal_id',100)->nullable();
            $table->string('document_path_car_licence',100)->nullable();
            $table->string('document_path_cctv_inside',100)->nullable();
            $table->string('document_path_cctv_outside',100)->nullable();
            $table->string('document_path_driver1_licence',100)->nullable();
            $table->string('document_path_driver2_licence',100)->nullable();

            $table->string('locale',2)->default('th');
            $table->integer('update_core_status')->default(0);

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy');
    }
}
