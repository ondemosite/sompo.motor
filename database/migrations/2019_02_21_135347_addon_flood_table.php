<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddonFloodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addon_flood', function (Blueprint $table) {
            $table->increments('id');
            $table->float('sum_insured',8,2);
            $table->float('net_premium',8,2);
            $table->float('stamp',8,2);
            $table->float('vat',8,2);
            $table->float('gross_premium',8,2);
            $table->float('max_rate',8,2)->default(0); // Max Rate
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
