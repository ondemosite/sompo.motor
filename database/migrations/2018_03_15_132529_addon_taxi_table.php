<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddonTaxiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addon_taxi', function (Blueprint $table) {
            $table->increments('id');
            $table->float('sum_insured',8,2);

            $table->float('net_normal',8,2)->default(0);
            $table->float('stamp_normal',8,2)->default(0);
            $table->float('vat_normal',8,2)->default(0);
            $table->float('normal',8,2)->default(0);

            $table->float('net_under29',8,2)->default(0);
            $table->float('stamp_under29',8,2)->default(0);
            $table->float('vat_under29',8,2)->default(0);
            $table->float('under29',8,2)->default(0);

            $table->float('net_over29',8,2)->default(0);
            $table->float('stamp_over29',8,2)->default(0);
            $table->float('vat_over29',8,2)->default(0);
            $table->float('over29',8,2)->default(0);

            $table->float('net_cctv',8,2)->default(0);
            $table->float('stamp_cctv',8,2)->default(0);
            $table->float('vat_cctv',8,2)->default(0);
            $table->float('cctv',8,2)->default(0);

            $table->float('max_rate',8,2)->default(0); // Max Rate

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addon_taxi');
    }
}
