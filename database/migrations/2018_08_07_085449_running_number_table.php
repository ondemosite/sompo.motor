<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RunningNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('running_number')) {
            Schema::create('running_number', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('order')->default(0);
                $table->integer('payment')->default(0);
                $table->integer('policy_av5')->default(0);
                $table->integer('policy_av3')->default(0);
                $table->integer('com')->default(0);
                $table->integer('misc')->default(0);
                $table->integer('tax')->default(0);
                $table->integer('year')->default(0)->unique();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
