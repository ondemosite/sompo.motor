<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            //Order Header
            $table->increments('id'); // ID
            $table->string('order_number',100);
            $table->enum('status',['WAITING','EXPIRE','CANCEL','INACTIVE','PAID'])->default('WAITING'); // ORDER STATUS
            $table->integer('promotion_id')->nullable();
            $table->string('promotion_code',100)->nullable();
            $table->integer('is_advice')->default(0);
            $table->integer('is_post')->default(0);
            $table->integer('is_personal')->default(0);
            $table->integer('is_email')->default(0);
            $table->integer('is_disclosure')->default(0);
            $table->integer('is_true')->default(0);
            $table->integer('is_agree')->default(0);
            $table->dateTime('order_expire');
            $table->double('payment_result',15,8);
            $table->integer('is_otp')->default(0);
            $table->integer('is_bangkok')->nullable();
            
            //Pricing Data
            $table->float('gross_premium',8,2); //ก่อนหักส่วนลด
            $table->float('theft_gross_premium',8,2)->default(0);
            $table->float('taxi_gross_premium',8,2)->default(0);
            $table->float('hb_gross_premium',8,2)->default(0);
            $table->float('carloss_gross_premium',8,2)->default(0);
            $table->float('theft_net_premium',8,2)->default(0);
            $table->float('taxi_net_premium',8,2)->default(0);
            $table->float('hb_net_premium',8,2)->default(0);
            $table->float('carloss_net_premium',8,2)->default(0);

            $table->float('net_discount',8,2)->default(0); // Net Discount +
            $table->float('discount',8,2)->default(0); // Gross Discount
            $table->float('insurance_net_premium',8,2); //หลังหักส่วนลด
            $table->float('stamp',8,2);
            $table->float('vat',8,2);
            $table->float('b_net_premium',8,2); //forward ก่อนหักส่วนลด
            $table->float('b_stamp',8,2); //forward ก่อนหักส่วนลด
            $table->float('b_vat',8,2); //forward ก่อนหักส่วนลด
            $table->float('based_premium',8,2);
            $table->float('od_based_premium',8,2);
            $table->float('basic_premium_cover',8,2);
            $table->float('additional_premium_cover',8,2);
            $table->float('fleet',8,2);
            $table->float('fleet_percent',8,2);
            $table->float('ncb',8,2);
            $table->float('ncb_percent',8,2);
            $table->float('od_si',8,2);
            $table->float('od_total_premium',8,2);
            $table->float('deduct',8,2);
            $table->float('cctv_percent',8,2);
            $table->float('cctv_discount',8,2);
            $table->float('direct_percent',8,2);
            $table->float('direct',8,2);
            $table->float('flood_net_premium',8,2)->default(0);
            $table->float('flood_stamp',8,2)->default(0);
            $table->float('flood_vat',8,2)->default(0);
            $table->float('flood_gross_premium',8,2)->default(0);
            
            //Car Info
            $table->integer('vehicle_info');
            $table->string('car_licence',10);
            $table->string('car_province',5);
            $table->string('car_chassis_number',30)->nullable();
            $table->integer('car_cctv');

            //Insurance Data
            $table->integer('insurance_plan_id'); //ทำไว้รองรับ Dynamic
            $table->string('insurance_plan_name',100); //แบบ static
            $table->dateTime('insurance_start'); //เริ่มคุ้มครอง
            $table->dateTime('insurance_expire'); //หมดคุ้มครอง
            $table->integer('insurance_ft_si'); //ทุนประกัน1
            $table->enum('insurance_garage_type',['GENERAL','DEALER','NO']); //ซ่อมอู่
            $table->float('insurance_deduct',8,2); //ความเสียหายส่วนแรก
            $table->integer('insurance_person_damage_once'); //ความรับผิดต่อบุคคลภายนอก/ ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย ต่อครั้ง
            $table->integer('insurance_person_damage_person'); //ความรับผิดต่อบุคคลภายนอก/ ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย ต่อคน
            $table->integer('insurance_stuff_damage'); //ความเสียหายต่อทรัพย์สิน
            $table->integer('insurance_death_disabled'); //อุบัติเหตุส่วนบุคคล
            $table->integer('insurance_medical_fee'); //ค่ารักษาพยาบาล
            $table->integer('insurance_bail_driver'); //การประกันตัวผู้ขับขี่ (ต่อครั้ง)
            $table->integer('insurance_driver_amount'); //จำนวนผู้ขับขี่ประจำของรถคันนี้

            //Compusory
            $table->float('compulsory_net_premium',8,2)->default(0); 
            $table->float('compulsory_stamp',8,2)->default(0); 
            $table->float('compulsory_vat',8,2)->default(0);
            $table->dateTime('compulsory_start')->nullable();
            $table->dateTime('compulsory_expire')->nullable();

            //Insurace Addition
            $table->double('addon_flood',15,8)->nullable(); //คุ้มครองน้ำท่วม
            $table->double('addon_theft',15,8)->nullable(); //คุ้มครองการโจรกรรมทรัพย์สิน
            $table->double('addon_taxi',15,8)->nullable(); //ค่าเดินทาง
            $table->double('addon_hb',15,8)->nullable();//คุ้มครองการโจรกรรมทรัพย์สิน
            $table->double('addon_carloss',15,8)->nullable();//ค่าเดินทาง

            //Main Profile
            $table->integer('main_driver'); //ข้อมูลผู้เอาประกัน
            $table->integer('driver1')->nullable(); //ข้อมูลผู้ขับคนที่ 1
            $table->integer('driver2')->nullable(); //ข้อมูลผู้ขับคนที่ 1
            $table->string('document_path_personal_id',100);
            $table->string('document_path_car_licence',100);
            $table->string('document_path_cctv_inside',100)->nullable();
            $table->string('document_path_cctv_outside',100)->nullable();
            $table->string('document_path_driver1_licence',100)->nullable();
            $table->string('document_path_driver2_licence',100)->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
