<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->char('username', 50)->unique();
            $table->string('password', 200);
            $table->string('name', 100);
            $table->string('lastname', 100);
            $table->string('email', 200)->nullable();
            $table->integer('privilege');
            $table->string('cover', 200)->nullable();
            $table->integer('status')->default(1);
            $table->integer('created_by')->default(1);
            $table->integer('modified_by')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
