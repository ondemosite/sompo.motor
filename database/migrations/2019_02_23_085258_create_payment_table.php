<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number',50);
            $table->string('policy_number',50)->nullable();
            $table->string('payment_no', 50);
            $table->string('merchant_id', 50);
            $table->string('currency_code', 50);
            $table->string('description', 100);
            $table->float('amount',8,2);
            $table->enum('status',['PROCESSING','FAILED','CANCELED','PAID'])->default('PROCESSING');
            $table->text('status_detail');
            $table->dateTime('expire_date')->nullable();
            $table->dateTime('paid_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
