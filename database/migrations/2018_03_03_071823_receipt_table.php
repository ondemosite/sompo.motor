<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt', function (Blueprint $table) {
            $table->increments('id');
            $table->string('receipt_number',50);
            $table->string('order_number',50)->nullable();
            $table->string('payment_number',50)->nullable();
            $table->string('policy_number',50)->nullable();
            $table->float('amount',8,2)->nullable();
            $table->string('payment_channel_code',5)->nullable();
            $table->string('credit_number',20)->nullable();
            $table->string('payment_scheme',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt');
    }
}
