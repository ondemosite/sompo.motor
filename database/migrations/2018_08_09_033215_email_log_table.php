<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_log')) {
            Schema::create('email_log', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('policy_id');
                $table->string('endorse_no',2);
                $table->text('data');
                $table->text('response')->nullable();
                $table->enum('status',['SUCCESS','FAIL']);
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->timestamps();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
