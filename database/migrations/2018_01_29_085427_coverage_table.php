<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoverageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coverage', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan', 20);
            $table->integer('person_damage_person')->nullable();
            $table->integer('person_damage_once')->nullable();
            $table->integer('stuff_damage')->nullable();
            $table->integer('death_disabled')->nullable();
            $table->integer('medical_fee')->nullable();
            $table->integer('bail_driver')->nullable();
            $table->integer('max_age')->nullable();
            $table->integer('min_age')->nullable();
            $table->integer('max_pre')->nullable();
            $table->enum('status',['ACTIVE','INACTIVE'])->default('ACTIVE');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coverage');
    }
}
