<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',200);
            $table->string('cover_en',200)->nullable();
            $table->string('cover_th',200)->nullable();
            $table->string('cover_en_mobile',200)->nullable();
            $table->string('cover_th_mobile',200)->nullable();
            $table->enum('sync_type', ['PROMOTION', 'LINK']);
            $table->integer('promotion_id')->nullable();
            $table->string('link',200)->nullable();
            $table->text('description')->nullable();
            $table->integer('orders')->nullable();
            $table->integer('view')->default(1);
            $table->integer('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner');
    }
}
