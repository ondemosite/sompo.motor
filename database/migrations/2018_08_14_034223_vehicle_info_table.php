<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VehicleInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_info')) {
        Schema::create('vehicle_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand',200);
            $table->string('model',200);
            $table->integer('year');
            $table->enum('body_type', ['SEDAN', 'PICKUP']);
            $table->string('model_type', 200);
            $table->string('model_type_full', 200);
            $table->integer('mortor_code_av');
            $table->float('mortor_code_ac',8, 2);
            $table->integer('cc');
            $table->float('tons', 8, 2);
            $table->integer('car_seat');
            $table->string('driver_passenger',10);
            $table->integer('red_plate');
            $table->integer('used_car');
            $table->integer('car_age');
            $table->integer('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->timestamps();
        });
        }
    }

}
