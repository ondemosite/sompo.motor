<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pricing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('coverage_id')->nullable(); /* ทำไว้รองรับการผูกกับ Coverage */
            $table->string('coverage_name',20)->nullable();
            $table->integer('ft_si')->nullable();
            $table->integer('car_code')->nullable();
            $table->enum('car_engine',['<=2000CC', '>2000CC','<=4TONS','>4TONS'])->nullable();
            $table->enum('define_name',['UNNAMED','18-24Y','25-28Y','29-35Y','36-50Y','50UP'])->nullable();
            $table->boolean('cctv')->nullable();
            $table->enum('garage_type',['GENERAL','DEALER','NO'])->nullable();
            $table->integer('deductible')->nullable();
            $table->enum('additional_coverage',['NO','FLOOD'])->nullable();
            $table->string('mortor_package_code',10)->nullable();
            $table->float('based_prem',8,2)->nullable();
            $table->float('based_prem_percent',8,2)->nullable();
            $table->float('name_policy',8,2)->nullable();
            $table->float('basic_premium_cover',8,2)->nullable();
            $table->float('add_premium_cover',8,2)->nullable();
            $table->float('fleet_percent',8,2)->nullable();
            $table->float('fleet',8,2)->nullable();
            $table->float('ncb_percent',8,2)->nullable();
            $table->float('ncb',8,2)->nullable();
            $table->float('total_premium',8,2)->nullable();
            $table->float('od_si',8,2)->nullable();
            $table->float('od_based_prem',8,2)->nullable();
            $table->float('od_total_premium',8,2)->nullable();
            $table->float('deduct_percent',8,2)->nullable();
            $table->float('deduct',8,2)->nullable();
            $table->float('cctv_discount_percent',8,2)->nullable();
            $table->float('cctv_discount',8,2)->nullable();
            $table->float('direct_percent',8,2)->nullable();
            $table->float('direct',8,2)->nullable();
            $table->float('net_premium',8,2)->nullable();
            $table->float('stamp',8,2)->nullable();
            $table->float('vat',8,2)->nullable();
            $table->float('gross_premium',8,2)->nullable();
            $table->float('flood_net_premium',8,2)->nullable();
            $table->float('flood_stamp',8,2)->nullable();
            $table->float('flood_vat',8,2)->nullable();
            $table->float('flood_gross_premium',8,2)->nullable();
            $table->integer('is_bangkok')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing');
    }
}
