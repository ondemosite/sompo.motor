<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GarageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type_class', ['GENERAL', 'DEALER']);
            $table->integer('type');
            $table->string('name',200);
            $table->text('address');
            $table->string('province',5);
            $table->string('district',5);
            $table->string('sub_district',5);
            $table->string('tel',100);
            $table->string('fax',100);
            $table->double('lat',15, 8);
            $table->double('long',15, 8);
            $table->integer('status')->default(1);
            $table->integer('created_by')->default(1);
            $table->integer('updated_by')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garage');
    }
}
