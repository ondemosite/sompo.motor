<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PromotionRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('promotion_register')) {
            Schema::create('promotion_register', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('promotion_id');
                $table->string('name',200);
                $table->string('lastname',200);
                $table->string('email',100);
                $table->string('tel',20);
                $table->integer('status')->default(1);
                $table->integer('vehicle_brand');
                $table->integer('vehicle_model');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
