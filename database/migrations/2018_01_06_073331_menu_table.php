<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name',100)->unique();
            $table->integer('parent_id');
            $table->string('path',100)->nullable();
            $table->string('icon',20)->nullable();
            $table->integer('isshow')->default(1);
            $table->float('orders',3, 2);
            $table->integer('level')->default(0);
            $table->integer('created_by')->default(1);
            $table->integer('modified_by')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
