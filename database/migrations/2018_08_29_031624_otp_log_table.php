<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OtpLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('otp_log')) {
            Schema::create('otp_log', function (Blueprint $table) {
                $table->increments('id');
                $table->string('receiver',20);
                $table->text('data');
                $table->text('response')->nullable();
                $table->enum('status',['SUCCESS','FAIL']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
