<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompulsoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compulsory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('body_type', 20);
            $table->float('motor_code_ac',8,2)->default(0);
            $table->float('net_premium',8,2)->default(0);
            $table->float('stamp',8,2)->default(0);
            $table->float('vat',8,2)->default(0);
            $table->float('gross_premium',8,2)->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compulsory');
    }
}
