<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ---------------- Test ------------------*/
// Route::get('clearSession',function(){
//     session()->forget('insurance_data');
//     session()->forget('database_active');
//     $value = session('insurance_data');
//     var_dump($value);
// });

// Route::get('getSession',function(){
//     $value = session('database_active');
//     var_dump($value);
// });

// Route::get('createStorageLink',function(){
//     App::make('files')->link(storage_path('app/public'), public_path('storage'));
// });

Route::get('/config-clear', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Config Clear</h1>';
});

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});


//Remove Cache
Route::get('/remove-cache','web\TestController@fileStorageRemove');


//Create Folder
Route::get('create_folder','web\TestController@createFolder');
Route::get('clear_storage','web\TestController@clearStorage');
Route::get('truncate_table','web\TestController@truncateTable');

Route::get('alterTable',function(){
    /*
    DB::statement('ALTER TABLE dbo.article DROP COLUMN title,cover_path,detail');
    DB::statement('ALTER TABLE dbo.article ADD title_th VARCHAR(100) NULL');
    DB::statement('ALTER TABLE dbo.article ADD title_en VARCHAR(100) NULL');
    DB::statement('ALTER TABLE dbo.article ADD cover_path_th VARCHAR(100) NULL');
    DB::statement('ALTER TABLE dbo.article ADD cover_path_en VARCHAR(100) NULL');
    DB::statement('ALTER TABLE dbo.article ADD 	detail_en TEXT NULL');
    DB::statement('ALTER TABLE dbo.article ADD 	detail_th TEXT NULL');*/
    DB::statement('ALTER TABLE dbo.promotion ADD formular VARCHAR(100) NULL');
    DB::statement('ALTER TABLE dbo.configs ADD title VARCHAR(100) NULL');
    DB::statement('ALTER TABLE dbo.configs ADD start_date datetime NULL');
    DB::statement('ALTER TABLE dbo.configs ADD expire_date datetime NULL');
    DB::statement('ALTER TABLE dbo.configs ADD status smallint NULL DEFAULT(1)');

});
Route::get('phpinfo',function(){ phpinfo(); });
Route::get('testEmail','web\TestController@testSendEmail');
Route::get('testDone','web\InsuranceController@donetest');
Route::get('setYear','web\TestController@genRunningYear');
Route::get('testPdf','web\TestController@testPdf');
Route::get('testBarcode/{param?}','web\TestController@testBarcode');
Route::get('testPdf2','web\TestController@testPdf2');
Route::get('testSign','web\TestController@testDigitalSign');
Route::get('testEpass','web\TestController@testEpass');
Route::get('testEpassExample','web\TestController@epassExample');
Route::get('testEpassCertList','web\TestController@epassCertList');
Route::get('testEpassCampaignList','web\TestController@epassCampaignList');
Route::get('testSendEpass','web\TestController@testSendEpass');
Route::get('tesUpdateTemplate','web\TestController@epassUpdateTemplate');
Route::get('tesUpdateCampaign','web\TestController@updateCampaign');
Route::get('tesLoadPdf','web\TestController@testLoadPdf');
Route::get('tesEncrypt','web\TestController@testEncrypt');
Route::get('testSMS','web\TestController@testSMS');
Route::get('testEndorse','web\TestController@testEndorse');
Route::get('testOtp','web\TestController@testOtp');
Route::get('testBlacklist','web\TestController@testBlackList');
Route::get('checkOtp','web\TestController@checkOTP');
Route::get('displayLog','web\TestController@openLogFile');
Route::get('removeLogFile','web\TestController@removeLogFile');
Route::get('testApi','web\TestController@testApi');
Route::get('logTransaction','web\TestController@logTransaction');
Route::get('packageNumber','web\TestController@testPackageNumber');
Route::get('transactions/{orderId?}','web\InsuranceController@getTransactionLog');
Route::get('testFtp','web\TestController@testFtp');
Route::get('testLoadExcel','web\TestController@testLoadExcel');

Route::get('testFunction',function(){
    $string = '/user/compulsory/HQAC30000012MOL01201902-00/Receipt-Invoice-Tax/5c7419b64e73f.pdf';
    $exploded = explode("/",$string);
    $lastExplode = $exploded[sizeof($exploded)-1];
    $fileName = "copy_".$lastExplode;

    $filePath = str_replace($lastExplode,$fileName,$string);
});



Route::post('ajaxTest','web\PromotionController@checkRegisterReceived');
Route::get('vehicle','web\TestController@vehicle');


Route::get('testLockTable','web\TestController@vehicle');
/* ---------------- / Test ----------------*/




Route::get('/','web\HomeController@index')->name('web.dashboard');
Route::get('/home','web\HomeController@index');
Route::get('/login','web\UserLoginController@showLoginForm')->name('web.login');
Route::post('/login','web\UserLoginController@login')->name('web.login.submit');
Route::get('/logout','web\UserLoginController@logout')->name('web.logout');
Route::get('/session_expire','web\SessionController@expire')->name('web.session_expire');

/* File Access */
Route::get('access_file/{params?}','FileAccessController@get')->name('access_file');

/*Register */
Route::post('register', 'web\UserController@register')->name('web.register');

/* Social Login */
Route::get('login/social/{type}', 'web\UserLoginController@redirectToProvider');
Route::get('login/social/callback/{type}', 'web\UserLoginController@handleProviderCallback');

//---- Change Language -----//
Route::get('language_switch/{locale?}','ConfigController@switchLang');

Route::get('testPdf','web\TestController@testPdf');

//------------ Web -----------------//
Route::group(array('prefix' => 'home'), function () {
    Route::post('/get_model','web\HomeController@getVehicleModel')->name('web.get_vehicle_data_model');
    Route::post('get_model_year','web\HomeController@getModelYear')->name('web.get_vehicle_data_model_year');
    Route::post('get_model_type','web\HomeController@getModelType')->name('web.get_vehicle_data_model_type');
});
Route::group(array('prefix' => 'insurance'), function () {
    Route::get('/step2','web\InsuranceController@initialStep2')->name('web.insurance_step2');
    Route::post('/step2','web\InsuranceController@initialStep2')->name('web.insurance_step2_submit');
    Route::get('/step3','web\InsuranceController@initialStep3')->name('web.insurance_step3');
    Route::post('/step3','web\InsuranceController@initialStep3')->name('web.insurance_step3_submit');
    Route::get('/step4','web\InsuranceController@initialStep4')->name('web.insurance_step4');
    Route::post('/step4','web\InsuranceController@initialStep4')->name('web.insurance_step4_submit');
    Route::get('/step5','web\InsuranceController@initialStep5')->name('web.insurance_step5');
    Route::post('/step5','web\InsuranceController@initialStep5')->name('web.insurance_step5_submit');
    Route::post('calculate','web\InsuranceController@calculateModified')->name('web.calculate_modified');
    Route::post('form_input','web\InsuranceController@formInput')->name('web.form_input');
    Route::post('submit_otp','web\InsuranceController@submitOTP')->name('web.submit_otp');
    Route::post('request_otp','web\InsuranceController@requestOTP')->name('web.request_otp');
    Route::post('check_otp','web\InsuranceController@checkOTP')->name('web.check_otp');
    Route::post('round_otp','web\InsuranceController@roundOTP')->name('web.round_otp');
    Route::post('check_promotion_code','web\InsuranceController@checkPromotionCode')->name('web.check_promotion_code');
    Route::post('saveInformation','web\InsuranceController@saveInformation')->name('web.save_information');

    //Payment Response
    Route::get('/done','web\InsuranceController@done')->name('web.insurance_done');
    Route::get('/fail','web\InsuranceController@fail')->name('web.insurance_fail');

    //Test Payment Success
    Route::get('/payment_success','api\PaymentController@testPayment');
    Route::get("/payment_test",'web\InsuranceController@testPayment');
    Route::get("/payment_fail",'web\InsuranceController@paymentFail');
});

Route::group(array('prefix' => 'garage'), function () {
    Route::get('/','web\GarageController@index')->name('web.garage');
    Route::post('find','web\GarageController@findGarage')->name('web.find_garage');
    Route::post('get_district','web\GarageController@getDistrict')->name('web.get_data_district');
    Route::post('get_data_garage','web\GarageController@getDataGarage')->name('web.get_data_garage');
});
Route::group(array('prefix' => 'promotion'), function () {
    Route::get('/','web\PromotionController@index')->name('web.promotion');
    Route::post('register','web\PromotionController@register')->name('web.promotion_register');

    Route::post('get_vehicle_brand','web\PromotionController@getVehicleBrand')->name('web.promotion_get_vehicle_brand');
    Route::post('get_vehicle_model','web\PromotionController@getVehicleModel')->name('web.promotion_get_vehicle_model');

});
Route::group(array('prefix' => 'confirmPolicy'), function () {
    Route::get('/{id?}','web\InsuranceController@confirmPolicy')->name('web.confirm_policy');
    Route::post('submit','web\InsuranceController@submitConfirmPolicy')->name('web.confirm_policy_submit');
});



//------------Backoffice-----------//
Route::group(array('prefix' => 'motoradmins'), function () {
    Route::get('/','admin\HomeController@index');
    Route::get('dashboard','admin\HomeController@index')->name('admin.dashboard');
    Route::get('login','admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('login','admin\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout','admin\AdminLoginController@logout')->name('admin.logout');

    //------ Profile ----------//
    Route::group(array('prefix' => 'profile'), function () {
        Route::get('/{id?}','admin\AdminController@getProfile')->name('admin.get_profile');
        Route::post('/upload_cover','admin\AdminController@uploadAvatar')->name('admin.uploadProfileAvatar');
        Route::post('/save_profile','admin\AdminController@saveProfile')->name('admin.save_profile');
        Route::post('/edit_password','admin\AdminController@editPassword')->name('admin.edit_password');
    });

    //------ Privilege --------//
    Route::group(array('prefix' => 'privilege'), function () {
        Route::get('/','admin\PrivilegeController@index')->name('admin.privilege');
        Route::get('get_privilege','admin\PrivilegeController@getPrivilege')->name('admin.get_privilege_list');
        Route::post('save_privilege','admin\PrivilegeController@savePrivilege')->name('admin.save_privilege');
        Route::post('get_data_privilege','admin\PrivilegeController@getDataPrivilege')->name('admin.get_data_privilege');
        Route::post('delete_privilege','admin\PrivilegeController@deletePrivilege')->name('admin.delete_privilege');
    });

    //------ Admin ------------//
    Route::group(array('prefix' => 'admin_list'), function () {
        Route::get('/','admin\AdminController@index')->name('admin.admin_list');
        Route::get('get_admin','admin\AdminController@getAdminList')->name('admin.get_admin_list');
        Route::post('get_privilege','admin\AdminController@getPrivilege')->name('admin.get_privilege');
        Route::delete('delete_admin','admin\AdminController@deleteAdmin')->name('admin.delete_admin');
    });

    //------ Menu Permission ------//
    Route::group(array('prefix' => 'menu'), function () {
        Route::get('/','admin\MenuController@index')->name('admin.menu');
        Route::post('get_menu_privilege','admin\MenuController@getMenuPrivilege')->name('admin.get_menu_privilege');
        Route::post('get_menu','admin\MenuController@genMenu')->name('admin.get_menu');
        Route::post('save_menu','admin\MenuController@saveMenu')->name('admin.save_menu');
        Route::post('update_order','admin\MenuController@updateOrder')->name('admin.update_order');
        Route::post('get_parent_menu','admin\MenuController@getParentMenu')->name('admin.get_parent');
        Route::post('get_menu_data','admin\MenuController@getMenuData')->name('admin.get_menu_data');
        Route::post('get_menu','admin\MenuController@genMenu')->name('admin.get_menu');
        Route::delete('delete_menu','admin\MenuController@deleteMenu')->name('admin.delete_menu');
        Route::post('gen_permission_menu','admin\MenuController@get_permission_menu')->name('admin.get_pm_menu');
        Route::post('set_permission_menu','admin\MenuController@setPermission')->name('admin.set_pm_menu');
    });

    //------ Garage ----------//
    Route::group(array('prefix' => 'garage'), function () {
        Route::get('/garage_type','admin\GarageController@garageType')->name('admin.garage_type');
        Route::get('/get_garage_type','admin\GarageController@getGarageType')->name('admin.get_garage_type');
        Route::post('save_garage_type','admin\GarageController@saveGarageType')->name('admin.save_garage_type');
        Route::post('get_data_garage_type','admin\GarageController@getDataGarageType')->name('admin.get_data_garage_type');
        Route::delete('delete_garage_type','admin\GarageController@deleteGarageType')->name('admin.delete_garage_type');

        Route::get('/garage','admin\GarageController@index')->name('admin.garage');
        Route::get('/garage_form/{id?}','admin\GarageController@garageForm')->name('admin.garage_form');
        Route::get('/get_garage','admin\GarageController@getGarage')->name('admin.get_garage');
        Route::post('get_data_garage','admin\GarageController@getDataGarage')->name('admin.get_data_garage');
        Route::post('save_garage','admin\GarageController@saveGarage')->name('admin.save_garage');
        Route::delete('delete_garage','admin\GarageController@deleteGarage')->name('admin.delete_garage');
        Route::post('get_district','admin\GarageController@getDistrict')->name('admin.get_data_district');
        Route::post('get_sub_district','admin\GarageController@getSubDistrict')->name('admin.get_data_subdistrict');

        Route::get('/import','admin\GarageController@setData');
    });

    //------ Vehicle ----------//
    Route::group(array('prefix' => 'vehicle'), function () {
        /* Brand */
        Route::get('brand','admin\VehicleController@vehicleBrand')->name('admin.vehicle_brand');
        Route::get('get_brand_list','admin\VehicleController@getBrandList')->name('admin.get_vehicle_brand_list');
        Route::post('save_vehicle_brand','admin\VehicleController@saveBrand')->name('admin.save_vehicle_brand');
        Route::post('get_data_brand','admin\VehicleController@getDataBrand')->name('admin.get_data_vehicle_brand');
        Route::delete('delete_brand','admin\VehicleController@deleteBrand')->name('admin.delete_brand');

        /* Model */
        Route::get('model','admin\VehicleController@vehicleModel')->name('admin.vehicle_model');
        Route::get('get_model_list','admin\VehicleController@getModelList')->name('admin.get_vehicle_model_list');
        Route::post('save_vehicle_model','admin\VehicleController@saveModel')->name('admin.save_vehicle_model');
        Route::post('get_data_model','admin\VehicleController@getDataModel')->name('admin.get_data_vehicle_model');
        Route::delete('delete_model','admin\VehicleController@deleteModel')->name('admin.delete_model');

        /* Vehicle */
        Route::get('vehicle','admin\VehicleController@index')->name('admin.vehicle');
        Route::get('vehicle_form/{id?}','admin\VehicleController@vehicleForm')->name('admin.vehicle_form');
        Route::get('get_vehicle','admin\VehicleController@getVehicleList')->name('admin.get_vehicle_list');
        Route::post('get_data_vehicle','admin\VehicleController@getDataVehicle')->name('admin.get_data_vehicle');
        Route::post('save_vehicle','admin\VehicleController@saveVehicle')->name('admin.save_vehicle');
        Route::post('get_select_model','admin\VehicleController@getSelectModel')->name('admin.get_select_model');
        Route::delete('delete_vehicle','admin\VehicleController@deleteVehicle')->name('admin.delete_vehicle');
        Route::post('get_select_brand','admin\VehicleController@getSelectBrand')->name('admin.get_select_brand');

        
        Route::get('/import_brand','admin\VehicleController@setBrand');
        Route::get('/import_model','admin\VehicleController@setModel');
        Route::get('/import_vehicle','admin\VehicleController@setData');

        Route::post('import_csv','admin\VehicleController@importXLS')->name('admin.vehicle_import');
        Route::post('upload_chunks','admin\VehicleController@chunksVehicle')->name('admin.vehicle_chunks');
        Route::post('backup_vehicle','admin\VehicleController@backupVehicle')->name('admin.vehicle_backup');
        Route::get('/import_data','admin\VehicleController@setData');
        Route::get('get_imported_list','admin\VehicleController@getImportedVehicleList')->name('admin.vehicle_imported_list');
        Route::post('submit_import','admin\VehicleController@submitImportChange')->name('admin.vehicle_submit_import');

        Route::get('/export','admin\VehicleController@export')->name('admin.vehicle.export');
    });

    //------ Motor Coverage ---------//
    Route::group(array('prefix' => 'coverage'), function () {
        Route::get('/','admin\CoverageController@index')->name('admin.coverage');
        Route::post('save_data','admin\CoverageController@saveCoverage')->name('admin.save_coverage');
    });

    //------- Pricing ------------//
    Route::group(array('prefix' => 'pricing'), function () {
        Route::get('/pricing','admin\PricingController@index')->name('admin.pricing');
        Route::get('pricing_form/{id?}','admin\PricingController@pricingForm')->name('admin.pricing_form');
        Route::get('get_pricing_list','admin\PricingController@getPricingList')->name('admin.get_pricing_list');
        Route::post('save_pricing','admin\PricingController@savePricing')->name('admin.save_pricing');
        Route::delete('delete_pricing','admin\PricingController@deletePricing')->name('admin.delete_pricing');

        Route::get('/compulsory','admin\PricingController@compulsory')->name('admin.compulsory');
        Route::get('get_compulsory_list','admin\PricingController@get_compulsory_list')->name('admin.get_compulsory_list');
        Route::post('save_compulsory','admin\PricingController@saveCompulsory')->name('admin.save_compulsory');
        Route::post('get_data_compulsory','admin\PricingController@getDataCompulsory')->name('admin.get_data_compulsory');
        Route::delete('delete_compulsory','admin\PricingController@deleteCompulsory')->name('admin.delete_compulsory');

        Route::get('/addon','admin\PricingController@addon')->name('admin.addon');
        Route::get('get_addon_list','admin\PricingController@getAddonList')->name('admin.get_addon_list');
        Route::post('save_addon','admin\PricingController@saveAddon')->name('admin.save_addon');
        Route::post('get_data_addon','admin\PricingController@getDataAddon')->name('admin.get_data_addon');
        Route::delete('delete_addon','admin\PricingController@deleteAddon')->name('admin.delete_addon');

        Route::post('import_csv','admin\PricingController@importXLS')->name('admin.pricing_import');
        Route::post('upload_chunks','admin\PricingController@chunksPricing')->name('admin.pricing_chunks');
        Route::post('backup_pricing','admin\PricingController@backupPricing')->name('admin.pricing_backup');
        Route::get('/import_data','admin\PricingController@setData');
        Route::get('get_imported_list','admin\PricingController@getImportedPricingList')->name('admin.pricing_imported_list');
        Route::post('submit_import','admin\PricingController@submitImportChange')->name('admin.pricing_submit_import');

        Route::get('/export','admin\PricingController@export')->name('admin.pricing.export');
    });

    //------- Addon ------------//
    Route::group(array('prefix' => 'addon'), function () {
        Route::group(array('prefix' => 'carloss'), function () {
            Route::get('/','admin\AddonPricingController@index_carloss')->name('admin.addon_carloss');
            Route::get('/list','admin\AddonPricingController@list_carloss')->name('admin.addon_carloss_list');
            Route::post('/get','admin\AddonPricingController@get_carloss')->name('admin.addon_carloss_get');
            Route::post('/save','admin\AddonPricingController@save_carloss')->name('admin.addon_carloss_save');
            Route::delete('delete','admin\AddonPricingController@delete_carloss')->name('admin.addon_carloss_delete');
        });
        Route::group(array('prefix' => 'hb'), function () {
            Route::get('/','admin\AddonPricingController@index_hb')->name('admin.addon_hb');
            Route::get('/list','admin\AddonPricingController@list_hb')->name('admin.addon_hb_list');
            Route::post('/get','admin\AddonPricingController@get_hb')->name('admin.addon_hb_get');
            Route::post('/save','admin\AddonPricingController@save_hb')->name('admin.addon_hb_save');
            Route::delete('delete','admin\AddonPricingController@delete_hb')->name('admin.addon_hb_delete');
        });
        Route::group(array('prefix' => 'taxi'), function () {
            Route::get('/','admin\AddonPricingController@index_taxi')->name('admin.addon_taxi');
            Route::get('/list','admin\AddonPricingController@list_taxi')->name('admin.addon_taxi_list');
            Route::post('/get','admin\AddonPricingController@get_taxi')->name('admin.addon_taxi_get');
            Route::post('/save','admin\AddonPricingController@save_taxi')->name('admin.addon_taxi_save');
            Route::delete('delete','admin\AddonPricingController@delete_taxi')->name('admin.addon_taxi_delete');
        });
        Route::group(array('prefix' => 'theft'), function () {
            Route::get('/','admin\AddonPricingController@index_theft')->name('admin.addon_theft');
            Route::get('/list','admin\AddonPricingController@list_theft')->name('admin.addon_theft_list');
            Route::post('/get','admin\AddonPricingController@get_theft')->name('admin.addon_theft_get');
            Route::post('/save','admin\AddonPricingController@save_theft')->name('admin.addon_theft_save');
            Route::delete('delete','admin\AddonPricingController@delete_theft')->name('admin.addon_theft_delete');
        });
    });

    // ------- Prefix ------------//
    Route::group(array('prefix' => 'prefix'), function () {
        Route::get('/','admin\PrefixController@index')->name('admin.prefix.index');
        Route::get('list','admin\PrefixController@list')->name('admin.prefix.list');
        Route::post('get','admin\PrefixController@get')->name('admin.prefix.get');
        Route::post('save','admin\PrefixController@save')->name('admin.prefix.save');
        Route::delete('delete','admin\PrefixController@delete')->name('admin.prefix.delete');
    });

    //-------- Faq ---------------//
    Route::group(array('prefix' => 'faq'), function () {
        Route::get('/faq','admin\FaqController@index')->name('admin.faq');
        Route::get('get_faq_list','admin\FaqController@getFaqList')->name('admin.get_faq_list');
        Route::post('get_data_faq','admin\FaqController@getDataFaq')->name('admin.get_data_faq');
        Route::post('save_faq','admin\FaqController@saveFaq')->name('admin.save_faq');
        Route::post('set_status','admin\FaqController@setStatus')->name('admin.set_status_faq');
        Route::delete('delete_faq','admin\FaqController@deleteFaq')->name('admin.delete_faq');
    });

    //-------- Configs ---------------//
    Route::group(array('prefix' => 'configs'), function () {
        Route::get('/configs','admin\ConfigController@index')->name('admin.configs');
        Route::get('list','admin\ConfigController@getList')->name('admin.get_configs_list');
        Route::post('data','admin\ConfigController@getData')->name('admin.get_data_configs');
        Route::post('save','admin\ConfigController@saveData')->name('admin.save_configs');
        Route::delete('delete','admin\ConfigController@deleteData')->name('admin.delete_configs');
        Route::post('set_status','admin\ConfigController@setStatus')->name('admin.set_status_configs');
    });

    //-------- Banner ---------------//
    Route::group(array('prefix' => 'banner'), function () {
        Route::get('/','admin\BannerController@index')->name('admin.banner');
        Route::get('get_banner_list','admin\BannerController@getBannerList')->name('admin.get_banner_list');
        Route::get('banner_form/{id?}','admin\BannerController@bannerForm')->name('admin.banner_form');
        Route::post('save_banner','admin\BannerController@saveBanner')->name('admin.save_banner');
        Route::delete('delete_faq','admin\BannerController@deleteBanner')->name('admin.delete_banner');

    });

    //--------- Promotion ------------//
    Route::group(array('prefix' => 'promotion'), function () {
        Route::get('/','admin\PromotionController@index')->name('admin.promotion');
        Route::get('get_promotion_list','admin\PromotionController@getPromotion')->name('admin.get_promotion_list');
        Route::get('promotion_form/{id?}','admin\PromotionController@promotionForm')->name('admin.promotion_form');
        Route::post('save_promotion','admin\PromotionController@savePromotion')->name('admin.save_promotion');
        Route::delete('delete_promotion','admin\PromotionController@deletePromotion')->name('admin.delete_promotion');

        Route::get('get_register_list','admin\PromotionController@getRegister')->name('admin.get_promotion_register_list');
        Route::post('set_register','admin\PromotionController@setRegister')->name('admin.set_promotion_register');
        Route::post('save_register','admin\PromotionController@saveRegister')->name('admin.save_promotion_register');

        Route::post('get_vehicle_brand','admin\PromotionController@getVehicleBrand')->name('admin.promotion_get_vehicle_brand');
        Route::post('get_vehicle_model','admin\PromotionController@getVehicleModel')->name('admin.promotion_get_vehicle_model');
    });

    //-------- Report -------------//
    Route::group(array('prefix' => 'report'), function () {
        /* Report Policy */
        Route::get('/policy','admin\ReportController@listPolicy');
        Route::get('/list_policy','admin\ReportController@getPolicyList')->name('admin.report.policy_list');
        Route::get('/pdf_policy/{id?}','admin\ReportController@pdfPolicy')->name('admin.report.pdf_policy');
        Route::post('/excel_policy','admin\ReportController@excelPolicy')->name('admin.report.excel_policy');

        /* Report Owner */
        Route::get('/policy_owner','admin\ReportController@listOwner');
        Route::get('/list_owner','admin\ReportController@getOwnerList')->name('admin.report.owner_list');
        Route::post('/excel_owner','admin\ReportController@excelOwner')->name('admin.report.excel_owner');

        /* Report Order */
        Route::get('/order','admin\ReportController@listOrder');
        Route::get('/list_order','admin\ReportController@getOrderList')->name('admin.report.order_list');
        Route::post('/excel_order','admin\ReportController@excelOrder')->name('admin.report.excel_order');


    });

    //------- PDF --------------//
    Route::group(array('prefix' => 'pdf'), function () {
        Route::get('/{id?}','admin\PdfController@pdf');
    });

    //-------- Tax -------------//
    Route::group(array('prefix' => 'tax'), function () {
        Route::get('/tax','admin\TaxController@index')->name('admin.tax');
        Route::post('/tax','admin\TaxController@saveTax')->name('admin.save_tax');
    });

    //-------- Cutoff -------------//
    Route::group(array('prefix' => 'cutoff'), function () {
        Route::get('/','admin\CutoffController@index')->name('admin.cutoff');
        Route::get('list','admin\CutoffController@listCutoff')->name('admin.cutoff_list');
        Route::post('save','admin\CutoffController@saveCutoff')->name('admin.save_cutoff');
        Route::post('get','admin\CutoffController@getCutoff')->name('admin.get_cutoff');
        Route::delete('delete','admin\CutoffController@deleteCutoff')->name('admin.delete_cutoff');
    });
    
    //-------- Why Sompo ---------//
    Route::group(array('prefix' => 'article'), function () {
        Route::group(array('prefix' => 'whysompo'), function () {
            Route::get('/','admin\ArticleController@whysompo')->name('admin.whysompo');
            Route::get('/list','admin\ArticleController@whysompoList')->name('admin.get_whysompo_list');
            Route::get('/form/{id?}','admin\ArticleController@whysompoForm')->name('admin.whysompo_form');
            Route::post('/save','admin\ArticleController@whysompoSave')->name('admin.whysompo_save');
            Route::delete('delete','admin\ArticleController@deleteWhysompo')->name('admin.delete_whysompo');
        });
        Route::group(array('prefix' => 'ourproduct'), function () {
            Route::get('/','admin\ArticleController@ourproduct')->name('admin.ourproduct');
            Route::get('/list','admin\ArticleController@ourproductList')->name('admin.get_ourproduct_list');
            Route::get('/form/{id?}','admin\ArticleController@ourproductForm')->name('admin.ourproduct_form');
            Route::post('/save','admin\ArticleController@ourproductSave')->name('admin.ourproduct_save');
            Route::delete('delete','admin\ArticleController@deleteOurProduct')->name('admin.delete_ourproduct');
        });
    });

    //--------- Order -----------//
    Route::group(array('prefix' => 'order'), function () {
        Route::get('/','admin\OrderController@index');
        Route::get('/list','admin\OrderController@getOrderList')->name('admin.get_order_list');
        Route::get('/detail/{id?}','admin\OrderController@getOrderData')->name('admin.get_order_data');
        Route::post('/create_policy','admin\OrderController@createPolicy')->name('admin.order_create_policy');
    });

    //--------- Payment -----------//
    Route::group(array('prefix' => 'payment'), function () {
        Route::get('/','admin\PaymentController@index');
        Route::get('/list','admin\PaymentController@getPaymentList')->name('admin.get_payment_list');
        Route::post('/data','admin\PaymentController@getPaymentData')->name('admin.get_payment_data');
    });

    //--------- Receipt -----------//
    Route::group(array('prefix' => 'receipt'), function () {
        Route::get('/','admin\ReceiptController@index');
        Route::get('/list','admin\ReceiptController@getReceiptList')->name('admin.get_receipt_list');
        Route::post('/data','admin\ReceiptController@getReceiptData')->name('admin.get_receipt_data');
    });

    //--------- Policy ----------//
    Route::group(array('prefix' => 'policy'), function () {
        Route::get('/','admin\PolicyController@index');
        Route::get('/list','admin\PolicyController@getPolicyList')->name('admin.get_policy_list');
        Route::get('/detail/{id?}','admin\PolicyController@getPolicyData')->name('admin.get_policy_data');
        Route::post('/edit_log','admin\PolicyController@getEditLog')->name('admin.get_policy_edit_log');
        Route::post('/save_note','admin\PolicyController@saveNote')->name('admin.policy_save_note');
        Route::post('/edit_policy_name','admin\PolicyController@saveEditPolicyName')->name('admin.edit_policy_name');
        Route::post('/edit_log_data','admin\PolicyController@getEditLogData')->name('admin.get_edit_policy_log');
        Route::post('/save_policy_doc','admin\PolicyController@savePolicyDoc')->name('admin.save_policy_doc');
        Route::post('/get_policy_doc','admin\PolicyController@getPolicyDocLog')->name('admin.get_policy_doc');
        Route::post('/get_policy_doc_data','admin\PolicyController@getDocLogData')->name('admin.get_policy_doc_data');
        Route::post('/get_receipt_data','admin\PolicyController@getReceipt')->name('admin.get_policy_receipt');
        Route::post('/get_email_log','admin\PolicyController@getEmailLog')->name('admin.get_email_log');
        Route::post('/get_epass_log','admin\PolicyController@getEpassLog')->name('admin.get_epass_log');
        Route::post('/resend_document','admin\PolicyController@resendDocument')->name('admin.policy_resend_doc');
        Route::post('/resend_sms','admin\PolicyController@resendSMS')->name('admin.policy_resend_sms');
        Route::post('/update_core','admin\PolicyController@updateCore')->name('admin.policy_update_core');
        Route::post('/get_update_core','admin\PolicyController@getUpdateCore')->name('admin.policy_get_update_core');
        Route::post('/get_endorse','admin\PolicyController@getEndorseList')->name('admin.policy_endorse_get');
        Route::post('/first_endorse','admin\PolicyController@getEndorseFirst')->name('admin.policy_endorse_first');
        Route::post('/save_endorse','admin\PolicyController@saveEndorse')->name('admin.save_policy_endorse');
        Route::get('/download/{policyId?}/{policyFile?}/{method?}','admin\PolicyController@downloadDocument')->name('admin.download_policy');
    });

    //--------- Log ------------//
    Route::group(array('prefix' => 'log'), function () {
        //Payment Log
        Route::get('payment_response','admin\LogsController@payment_log')->name('admin.payment_log');
        Route::get('payment_response/list','admin\LogsController@getPaymentLogList')->name('admin.payment_log_list');
        Route::post('payment_response/data','admin\LogsController@getPaymentLogData')->name('admin.payment_log_data');

        //Usermanagement Log
        Route::get('user_management','admin\LogsController@user_management_log')->name('admins.user_management_log');
        Route::get('user_management/list','admin\LogsController@getUserManagementLogList')->name('admin.user_management_log_list');
        Route::post('user_management/data','admin\LogsController@getUserManagementtLogData')->name('admin.user_management_log_data');

        //Email Log
        Route::get('email_log','admin\LogsController@emailLog')->name('admins.email_log');
        Route::get('email_log/list','admin\LogsController@emailLogList')->name('admin.email_log_list');

        //Endorse Log
        Route::get('endorse_log','admin\LogsController@endorseLog')->name('admins.endorse_log');
        Route::get('endorse_log/list','admin\LogsController@endorseLogList')->name('admin.endorse_log_list');
        Route::get('endorse_log/detail/{id?}','admin\LogsController@endorseLogDetail')->name('admin.endorse_log_detail');

        //Passkit Log
        Route::get('passkit_log','admin\LogsController@passkitLog')->name('admins.passkit_log');
        Route::get('passkit_log/list','admin\LogsController@passkitLogList')->name('admin.passkit_log_list');

        //SMS Log
        Route::get('sms_log','admin\LogsController@smsLog')->name('admins.sms_log');
        Route::get('sms_log/list','admin\LogsController@smsLogList')->name('admin.sms_log_list');

        //OTP Log
        Route::get('otp_log','admin\LogsController@otpLog')->name('admins.otp_log');
        Route::get('otp_log/list','admin\LogsController@otpLogList')->name('admin.otp_log_list');

        //Barcode Log
        Route::get('barcode_log','admin\LogsController@barcodeLog')->name('admins.barcode_log');
        Route::get('barcode_log/list','admin\LogsController@barcodeLogList')->name('admin.barcode_log_list');

        //Digital Sign Log
        Route::get('sign_log','admin\LogsController@signLog')->name('admins.sign_log');
        Route::get('sign_log/list','admin\LogsController@signLogList')->name('admin.sign_log_list');

        //Blacklist Log
        Route::get('blacklist_log','admin\LogsController@blacklistLog')->name('admins.blacklist_log');
        Route::get('blacklist_log/list','admin\LogsController@blacklistLogList')->name('admin.blacklist_log_list');

        //Chassis Log
        Route::get('chassis_log','admin\LogsController@chassisLog')->name('admins.chassis_log');
        Route::get('chassis_log/list','admin\LogsController@chassisLogList')->name('admin.chassis_log_list');

        //Cityzen Log
        Route::get('cityzen_log','admin\LogsController@cityzenLog')->name('admins.cityzen_log');
        Route::get('cityzen_log/list','admin\LogsController@cityzenLogList')->name('admin.cityzen_log_list');

        //Email Log
        Route::get('resend_schedule_log','admin\LogsController@resendScheduleLog')->name('admins.resend_schedule_log');
        Route::get('resend_schedule_log/list','admin\LogsController@resendScheduleLogList')->name('admin.resend_schedule_log_list');
        
    });
    
    //for test schedule report
    Route::group(array('prefix' => 'schedules'), function () {
        
        Route::get('report_frontend', array('uses' => 'admin\ScheduleController@report_frontend'));
    });

    //------ Compile ---------//
    Route::group(array('prefix' => 'compile'), function () {
        Route::get('/{id?}','admin\CompileController@index')->name('admin.compile');
        Route::post('/','admin\CompileController@getProcessLogs')->name('admin.compile.process');
        Route::post('update_order_status','admin\CompileController@updateOrderStatus')->name('admin.compile.order');
        Route::post('update_payment','admin\CompileController@updatePayment')->name('admin.compile.payment');
        Route::post('create_policy','admin\CompileController@createPolicy')->name('admin.compile.policy');
        Route::post('create_receipt','admin\CompileController@createReceipt')->name('admin.compile.receipt');
        Route::post('create_pdf','admin\CompileController@createPdf')->name('admin.compile.pdf');
        Route::post('send_ftp','admin\CompileController@sendFTP')->name('admin.compile.ftp');
        Route::post('send_sms','admin\CompileController@sendSMS')->name('admin.compile.sms');
        Route::post('send_email','admin\CompileController@sendEmail')->name('admin.compile.email');
        Route::post('send_endorse','admin\CompileController@sendEndorse')->name('admin.compile.endorse');
        
    });

    

});

//---------- API WITH SESSIONS -----------//
Route::group(array('prefix' => 'get_api'), function () {
    // ---------- Payment ----------//
    Route::group(array('prefix' => 'payment'), function () {
        Route::post('front_url','api\PaymentController@payment_front_response');
    });
    Route::post('/flood_gloss_premium','web\InsuranceController@getFloodGlossPremium')->name('api.web.flood_gloss_premium');
    Route::post('/validate_preorder','web\InsuranceController@checkMaxPreOrder')->name('api.web.check_max_preorder');
    Route::post('/validate_webservices','web\InsuranceController@checkWebService')->name('api.web.check_webservice');
    
    
});




