<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/************* INSURANCE *************/
Route::post('get_district','web\InsuranceController@getDistrict')->name('api.web.get_district');
Route::post('get_subdistrict','web\InsuranceController@getSubDistrict')->name('api.web.get_subdistrict');
Route::post('get_postalcode','web\InsuranceController@getPostalcode')->name('api.web.get_postalcode');
Route::post('/taxi_gloss_premium','web\InsuranceController@getTaxiGlossPremium')->name('api.web.taxi_gloss_premium');
Route::post('/theft_gloss_premium','web\InsuranceController@getTheftGlossPremium')->name('api.web.theft_gloss_premium');
Route::post('/carloss_gloss_premium','web\InsuranceController@getCarlossGlossPremium')->name('api.web.carloss_gloss_premium');
Route::post('/hb_gloss_premium','web\InsuranceController@getHbGlossPremium')->name('api.web.hb_gloss_premium');
Route::post('get_prefix','web\InsuranceController@getPrefix')->name('api.web.get_prefix');
Route::post('get_gender','web\InsuranceController@getGender')->name('api.get_gender');


Route::post('ajaxTest','web\PromotionController@checkRegisterReceived');
// Route::post('/validate_chassis','web\InsuranceController@validateUniqeChassisNumber')->name('api.web.check_uniqe_chassis');
// Route::post('/validate_blacklist','web\InsuranceController@checkBlacklist')->name('api.web.check_blacklist');
// Route::post('/validate_duplicate','web\InsuranceController@checkDuplicate')->name('api.web.check_duplicate');

/************* PAYMENT *****************/
Route::group(array('prefix' => 'payment'), function () {
    Route::post('back_url','api\PaymentController@payment_back_response');
    Route::get('back_url','api\PaymentController@payment_back_response');
});

Route::post('/testConcurrent','api\TestController@createPolicy');

/************* SERVICES ******************/
Route::group(array('prefix' => 'services'), function () {
    Route::get('epass_download','api\EpassController@downloadDocument')->name('api.web.download_epass');
});