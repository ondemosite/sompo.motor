// ===============================================================================
//

// Initialize
$(function() {
  $('body > .px-nav').pxNav();
  $('body > .px-footer').pxFooter();

  // Language Switcher
  $(".language-toggle").click(function(){
     $(".language-data").fadeToggle("fast");
  });
});

function notify(text,type){
    $('.px-alert').pxBlockAlert(text,{type:type,style:"dark"});
    setTimeout(function(){
      $('.px-alert').pxBlockAlert('clear',"default");
    }, 7000);
}

var isMobile = {
  Android: function () {
      return navigator.userAgent.match(/Android/i);
  },
  iOS: function () {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Windows: function () {
      return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
  }
};



