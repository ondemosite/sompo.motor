document.querySelector("html").classList.add('js');

function readURL(input,handlePreview = null) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            if(handlePreview!=null){
                handlePreview.attr('src', e.target.result);
            }else{
                $('#image-preview').attr('src', e.target.result);
            }
            
        };

        reader.readAsDataURL(input.files[0]);
    }
}