// Validate  --------
$.validator.addMethod("id_card", function(value, element) {
    var pid = value;
    pid = pid.toString().replace(/\D/g,'');
    if(pid.length == 13){
            var sum = 0;
            for(var i = 0; i < pid.length-1; i++){
            sum += Number(pid.charAt(i))*(pid.length-i);
            }
            var last_digit = (11 - sum % 11) % 10;
            $(element).val(pid);
            return pid.charAt(12) == last_digit;
    }else{
            return false;
    }
}, "เลขบัตรประชาชนไม่ถูกต้อง!");

$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[0-9a-zA-Z\_]+$/);
}, "Invalid Data!");

$.validator.addMethod("thaiAlpha",function(value,element){
    var regex = /^[ก-๏\s]+$/;
    return regex.test( value );
},"เฉพาะตัวอักษรภาษาไทยเท่านั้น");

$.validator.addMethod("thaiAddress",function(value,element){
    var removeSpaceValue = value.replace(/\s/g,'');
    var regex = /^[0-9ก-๏\.\/\s-]+$/;
    return regex.test( removeSpaceValue );
},"เฉพาะตัวอักษรภาษาไทยและเครื่องหมายที่อนุญาต");

$.validator.addMethod("custom_email",function(value,element){
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( value );
});

$.validator.addMethod("car_prefix",function(value,element){
    if($.isNumeric(value)){
        return false;
    }else{
        var regex = /^[0-9ก-ฮ][ก-ฮ]?[ก-ฮ]?$/;
        return regex.test( value );
    }
});

$.validator.addMethod("input_doc_id",function(value,element){
    if($("[name='input_doc_id']").val()=="" || $("[name='input_doc_id']").val()==null){
        if($("[name='input_doc_id_check']").val()==""){
            return false;
        }
    }
    return true;
});

$.validator.addMethod("input_doc_car_licence",function(value,element){
    if($("[name='input_doc_car_licence']").val()=="" || $("[name='input_doc_car_licence']").val()==null){
        if($("[name='input_doc_car_licence_check']").val()==""){
            return false;
        }
    }
    return true;
});

$.validator.addMethod("input_doc_licence1",function(value,element){
    if($("[name='input_doc_licence1']").val()=="" || $("[name='input_doc_licence1']").val()==null){
        if($("[name='input_doc_licence1_check']").val()==""){
            return false;
        }
    }
    return true;
});

$.validator.addMethod("input_doc_licence1",function(value,element){
    if($("[name='input_doc_licence1']").val()=="" || $("[name='input_doc_licence1']").val()==null){
        if($("[name='input_doc_licence1_check']").val()==""){
            return false;
        }
    }
    return true;
});

$.validator.addMethod("input_doc_licence2",function(value,element){
    if($("[name='input_doc_licence2']").val()=="" || $("[name='input_doc_licence2']").val()==null){
        if($("[name='input_doc_licence2_check']").val()==""){
            return false;
        }
    }
    return true;
});

$.validator.addMethod("input_doc_cctv_outside",function(value,element){
    if($("[name='input_doc_cctv_outside']").val()=="" || $("[name='input_doc_cctv_outside']").val()==null){
        if($("[name='input_doc_cctv_outside_check']").val()==""){
            return false;
        }
    }
    return true;
});

$.validator.addMethod("input_doc_cctv_outside",function(value,element){
    if($("[name='input_doc_cctv_outside']").val()=="" || $("[name='input_doc_cctv_outside']").val()==null){
        if($("[name='input_doc_cctv_outside_check']").val()==""){
            return false;
        }
    }
    return true;
});

$.validator.addMethod("input_doc_cctv_inside",function(value,element){
    if($("[name='input_doc_cctv_inside']").val()=="" || $("[name='input_doc_cctv_inside']").val()==null){
        if($("[name='input_doc_cctv_inside_check']").val()==""){
            return false;
        }
    }
    return true;
});