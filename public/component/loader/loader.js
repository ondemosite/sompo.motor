$.fn.loader = function() {
    html ='<div class="loader-css"><style> .loader{border: 16px solid #FFFFFF;border-radius: 50%;border-top: 16px solid #68B3C8;width: 80px;height: 80px;-webkit-animation: spin 1s linear infinite;animation: spin 1s linear infinite;';
    html+='position: absolute;top:50%;left:50%;transform: translate(-50%, -50%);z-index: 20000;}';
    html+='.loader-css{background-color: #5d5d5d;top: 0px;left: 0px;width: 100%;height: 100%;position: absolute;content: " ";opacity: 0.5;z-index: 9999;}';
    html+='@-webkit-keyframes spin {0% { -webkit-transform: rotate(0deg); }100% { -webkit-transform: rotate(360deg); }}';
    html+='@keyframes spin {0% { transform: rotate(0deg); }100% { transform: rotate(360deg); }}</style></div><div class="loader"></div>';
    this.append(html);
    
};
$.fn.removeLoader = function() {
    $(".loader-css").remove();
    $(".loader").remove();
};