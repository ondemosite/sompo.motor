$(function() {
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    function parseHTML(html) {
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>'+ html,'text/html');
        return dom.body.textContent;
    }

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_list']").val(),
              "data":function(d){
                  return {
                      'search_policy_number':$("[name='search_policy_number']").val(),
                      'search_driver_name':$("[name='search_driver_name']").val(),
                      'search_email':$("[name='search_email']").val(),
                      'search_tel':$("[name='search_tel']").val(),
                      'search_car_licence':$("[name='search_car_licence']").val(),
                      'search_from_date':$("[name='search_from_date']").val(),
                      'search_to_date':$("[name='search_to_date']").val(),
                      'search_advice':$("[name='search_advice']").val(),
                      'search_status':$("[name='search_status']").val()
                  }
              }
          },
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "policy_number","width":"20%" },
              { "data": "car_licence"},
              { "data": "name"},
              { "data": "gender" },
              { "data": "email"},
              { "data": "tel"},
              { 
                  "data": "status",
                  'render':function(data,type,row){
                        if(data=="NORMAL"){
                            return '<span class="text-default"> ปกติ </span>';
                        }else if(data=="EDIT"){
                            return '<span class="text-warning"> แก้ไข </span>';
                        }else if(data=="CANCEL"){
                            return '<span class="text-light"> ยกเลิก </span>';
                        }else if(data=="EXPIRE"){
                            return '<span class="text-danger"> หมดอายุ </span>';
                        }else if(data=="INACTIVE"){
                            return '<span class="text-danger"> ปิด </span>';
                        }
                  },
                  "width":"10%"
              },
              { "data": "created_at"},
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Owner List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_policy_number']" ).val(null);
        $("input[name='search_driver_name']").val(null);
        $("input[name='search_email']").val(null);
        $("input[name='search_tel']").val(null);
        $("input[name='search_car_licence']").val(null);
        $("input[name='search_from_date']").val(null);
        $("input[name='search_to_date']").val(null);
        $("select[name='search_advice']").val(null);
        $("select[name='search_status']").val(null);
        table.ajax.reload();
    });

    $("[name='item_per_page']").change(function(){
        var item_per_page = $(this).val();
        var total_item = table.data().count();
        var total_page = 1;
        if(item_per_page<total_item){
            total_page = Math.ceil(total_item/item_per_page);
        }

        var html = '<option selected="selected" value="">Select Page</option>';
        for(var i=0;i<total_page;i++){
            html+='<option value="'+(i+1)+'">'+(i+1)+'</option>';
        }
        $("[name='page_number']").html(html);
        $("[name='total_page']").val(total_page);
    });

    $("#submit_button").click(function(){
        var total_item = table.data().count();
        if(total_item>0){
            $("[name='total_item']").val(total_item);
            $("[name='input_policy_number']").val($("[name='search_policy_number']").val());
            $("[name='input_driver_name']").val($("[name='search_driver_name']").val());
            $("[name='input_email']").val($("[name='search_email']").val());
            $("[name='input_tel']").val($("[name='search_tel']").val());
            $("[name='input_car_licence']").val($("[name='search_car_licence']").val());
            $("[name='input_advice']").val($("[name='search_advice']").val());
            $("[name='input_from_date']").val($("[name='search_from_date']").val());
            $("[name='input_to_date']").val($("[name='search_to_date']").val());
            $("[name='input_status']").val($("[name='search_status']").val());
        }
        $("#form_export").submit();
        
    });

     // Validate  --------
     $('#form_export').pxValidate({
        rules: {
            'item_per_page': {
                required: true,
            },
            'page_number':{
                required: true,
            },
        },
    });


});