$(function() {

    function clearModal(){
      $("#form_privilege").find("input[name='id']").val(null);
      $("#form_privilege").find("input[name='name']").val(null);
    }
    $("#open-modal").click(function(){
      $("#form_privilege").find("input[name='name']").focus();
      clearModal();
    });

  // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          serverSide: true,
          ajax: $("input[name='hd_get_admin_list']").val(),
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "name" },
              { "data": "username" },
              { "data": "email" },
              { "data": "privilege" },
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Admin List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

  
    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_admin']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });



});