$(function() {

    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_get_promotion_list']").val(),
              "data":function(d){
                  return {
                      'search_code':$("[name='search_code']").val(),
                      'search_discount':$("[name='search_discount']").val(),
                      'search_start_date':$("[name='search_start_date']").val(),
                      'search_expire_date':$("[name='search_expire_date']").val(),
                      'search_status':$("[name='search_status']").val(),
                  }
              }
          },
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "title" },
              { "data": "code" },
              { "data": "type" },
              { 
                "data": "discount",
                'render': function(data){
                    return data+"%";
                }
              },
              { "data": "maximum_grant" },
              {  
                "name": "remaining",
                'render': function(data,type,row){
                    return row.maximum_grant - row.receive_amount ;
                }
            },
              { "data": "start_at" },
              { "data": "expire_at" },
              
              { 
                  "name": "status",
                  "data": "status",
                  'render':function(data,type,row){
                        if(data==0){
                            return '<span class="label label-danger">INACTIVE</span>';
                        }else{
                            return '<span class="label label-success">ACTIVE</span>';
                        }
                  }
              },
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Promotion List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_promotion']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          console.log(result);
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_title']").val(null);
        $("input[name='search_code']").val(null);
        $("input[name='search_discount']").val(null);
        $("input[name='search_start_date']").val(null);
        $("input[name='search_expire_date']").val(null);
        $("select[name='search_status']" ).val(null);
        table.ajax.reload();
    });


    


});