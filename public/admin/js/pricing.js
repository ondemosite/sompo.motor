$(function() {
    var _imported_table = null;
    $('.custom-file').pxFile();
    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_get_pricing_list']").val(),
              "data":function(d){
                  return {
                      'coverage_id':$("[name='coverage_id']").val(),
                      'ft_si':$("[name='ft_si']").val(),
                      'car_code':$("[name='car_code']").val(),
                      'car_engine':$("[name='car_engine']").val(),
                      'define_name':$("[name='define_name']").val(),
                      'mortor_package_code':$("[name='mortor_package_code']").val(),
                  }
              }
          },
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "coverage_name" },
              { "data": "ft_si","type": "num-fmt" },
              { "data": "car_code" },
              { "data": "car_engine" },
              { "data": "define_name" },
              { "data": "garage_type" },
              { "data": "gross_premium" },
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Pricing List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_pricing']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("select[name='search_body_type']" ).val(null);
        $("select[name='search_year']").val(null);
        $("select[name='search_brand']").val(null);
        $("select[name='search_model']").val(null);
        $("input[name='search_model_type']").val(null);
        table.ajax.reload();
    });


    function showImportedModal(){
        _imported_table = $('#datatablesImported').DataTable({
            
            processing: true,
            ajax :{
                "url": $("input[name='hd_imported_list']").val()
            },
            columns: [
                { "data": null,"width":"10%","sortable": false},
                { "data": "coverage_name" },
                { "data": "ft_si" },
                { "data": "car_code" },
                { "data": "car_engine" },
                { "data": "define_name" },
                { "data": "cctv" },
                { "data": "garage_type" },
                { "data": "deductible" },
                { "data": "additional_coverage" },
                { "data": "mortor_package_code" },
                { "data": "based_prem" },
                { "data": "based_prem_percent" },
                { "data": "name_policy" },
                { "data": "basic_premium_cover" },
                { "data": "add_premium_cover" },
                { "data": "fleet_percent" },
                { "data": "fleet" },
                { "data": "ncb_percent" },
                { "data": "ncb" },
                { "data": "total_premium" },
                { "data": "od_si" },
                { "data": "od_based_prem" },
                { "data": "od_total_premium" },
                { "data": "deduct_percent" },
                { "data": "deduct" },
                { "data": "cctv_discount_percent" },
                { "data": "cctv_discount" },
                { "data": "direct_percent" },
                { "data": "direct" },
                { "data": "net_premium" },
                { "data": "stamp" },
                { "data": "vat" },
                { "data": "gross_premium" },
                { "data": "flood_net_premium" },
                { "data": "flood_stamp" },
                { "data": "flood_vat" },
                { "data": "flood_gross_premium" },
                { 
                    "data": "is_bangkok",
                    'render':function(data,type,row){
                        console.log(data);
                        var html = "";
                        if(data == '1'){
                            html+= "Y";
                        }else{
                            html+='N';
                        }
                        return html;
                    },
                    "width":"10%"
                },
            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
              var info = $(this).DataTable().page.info();
              $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
              return nRow;
          }
      });
      
      $("#modal-imported").modal();
    }

    $("#submit_import").click(function(){
        var handle = $(this);
        var fileName = $("#csv_file").val();
        if(fileName) {
            var formData = new FormData();
            formData.append('file', $('#csv_file')[0].files[0]);
            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
            /* ------------------Chunks------------------ */
            handle.addClass("btn-loading");
            handle.attr("disabled","disabled");
            $(".import-status").html("Upload Processing...");
            $.ajax({
                url : $("input[name='hd_chunks']").val(),
                type : 'POST',
                data : formData,
                dataType: "json",
                processData: false,
                contentType: false,
            }).done(function(upload_result){
                if(!upload_result.resp_error){
                    if(upload_result.status=="SUCCESS"){
                        /* ---------------Import---------------- */
                        $(".import-status").html("Import Processing Please wait...");
                        $.ajax({
                            url : $("input[name='hd_import']").val(),
                            type : 'POST',
                            data : {
                                '_token': $('meta[name="csrf-token"]').attr('content'),
                            },
                            dataType: "json",
                        }).done(function(import_result){
                            if(!import_result.resp_error){
                                if(import_result.status=="SUCCESS"){
                                    $("#modal-import").modal("hide");
                                    showImportedModal();
                                }
                            }else{
                                notify(import_result.resp_error,"danger");
                            }
                        }).fail(function(jqXHR, textStatus){
                            if(textStatus == 'timeout'){
                                notify("Server Response Timeout!","danger");
                            }
                            else{
                                notify(textStatus,"danger");
                            }
                        }).always(function(){
                            handle.removeClass("btn-loading");
                            handle.removeAttr("disabled","disabled");
                            $(".import-status").html("");
                            var $el = $('#csv_file');
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                        });
                    }
                }else{
                    notify(upload_result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
                handle.removeClass("btn-loading");
                handle.removeAttr("disabled","disabled");
            });
        }
    });


    $("#submit_import_change").click(function(){
        $.ajax({
            url : $("input[name='hd_imported_submit']").val(),
            type : 'POST',
            data : {
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
        }).done(function(import_result){

            if(!import_result.resp_error){
                if(import_result.status=="SUCCESS"){
                    notify("Import File Successfully.","success");
                    $("#modal-imported").modal('hide');
                    table.ajax.reload();
                    _imported_table.destroy();
                }
            }else{
                notify(import_result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
    });

});