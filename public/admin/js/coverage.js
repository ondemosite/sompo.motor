$(document).ready(function(){
    //
    $('.form_coverage').pxValidate({
        rules: {
            'person_damage_person': {
                number: true
            },
            'person_damage_once': {
                number: true
            },
            'stuff_damage': {
                number: true
            },
            'death_disabled': {
                number: true
            },
            'medical_fee': {
                number: true
            },
            'bail_driver': {
                number: true
            },
        },
    });

    $('.input-money').simpleMoneyFormat();

    $('.form_coverage').on("submit",function(){
        if($(this).valid()){
            $(this).find(".input-money").each(function(index){
                var val = $(this).val();
                var real_val = val.replace(/,/g,'');
                $(this).val(real_val);
            });
        }else{
            e.preventDefault();
        }
    });


});