$(document).ready(function(){
    // Validate  --------
    $('#form_tax').pxValidate({
        rules: {
            'vat': {
                required: true,
                number:true
            },
            'stamp': {
                required: true,
                number:true
            },
        },
    });

});