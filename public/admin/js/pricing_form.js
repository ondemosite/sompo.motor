$(document).ready(function(){


    function reCalculate(){
        var basic_premium_cover = parseFloat($("[name='basic_premium_cover']").val()!=""?$("[name='basic_premium_cover']").val():0); //Fleet //Total Premium
        var additional_premium_cover = parseFloat($("[name='add_premium_cover']").val()!=""?$("[name='add_premium_cover']").val():0); //Fleet //Total Premium
        var fleet_percent = parseFloat($("[name='fleet_percent']").val()!=""?$("[name='fleet_percent']").val()/100:0); //Fleet
        var ncb_percent = parseFloat($("[name='ncb_percent']").val()!=""?$("[name='ncb_percent']").val()/100:0); //Ncb
        var deductible =  parseFloat($("[name='deductible']").val()!=""?$("[name='deductible']").val():0); //Deduct
        var deduct_percent = parseFloat($("[name='deduct_percent']").val()!=""?$("[name='deduct_percent']").val()/100:0); //Deduct
        var od_total_premium = parseFloat($("[name='od_total_premium']").val()!=""?$("[name='od_total_premium']").val():0); //cctv //Direct //Net Premium //Net Premium
        var cctv_percent = parseFloat($("[name='cctv_discount_percent']").val()!=""?$("[name='cctv_discount_percent']").val()/100:0); //cctv
        var direct_percent = parseFloat($("[name='direct_percent']").val()!=""?$("[name='direct_percent']").val()/100:0); //Direct

        var fleet = Math.round((basic_premium_cover+additional_premium_cover)*fleet_percent)*-1; //Fleet //Total Premium
        var ncb = Math.round((basic_premium_cover+additional_premium_cover)*ncb_percent)*-1; //Ncb //Total Premium
        var total_premium = basic_premium_cover+additional_premium_cover+fleet+ncb; //Total Premium //cctv //Direct //Net Premium
        var deduct = Math.round(deductible*deduct_percent)*-1; //Deduct //cctv //Direct //Net Premium
        var cctv = Math.round((total_premium + od_total_premium + deduct)*cctv_percent)*-1; //cctv //Direct
        var direct = Math.round((total_premium + od_total_premium + deduct + cctv)*direct_percent)*-1; //Direct //Net Premium
        var net_premium = total_premium + od_total_premium  + deduct + cctv + direct; //Net Premium //Stamp //Vat //Gross Premium
        var stamp = Math.ceil(net_premium*(0.4/100)); //Stamp //Vat 
        var vat = roundToTwo((net_premium + stamp)*(7/100),2); //Vat //Gross Premium
        var gross_premium = net_premium + vat + stamp; //Gross Premium


        $("[name='fleet']").val(fleet);
        $("[name='ncb']").val(ncb);
        $("[name='total_premium']").val(total_premium);
        $("[name='deduct']").val(deduct);
        $("[name='cctv_discount']").val(cctv);
        $("[name='direct']").val(direct);
        $("[name='net_premium']").val(net_premium);
        $("[name='stamp']").val(stamp);
        $("[name='vat']").val(vat);
        $("[name='gross_premium']").val(gross_premium);
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function roundToTwo(num) {    
        return +(Math.round(num + "e+2")  + "e-2");
    }

    $('.form_pricing').pxValidate({
        rules: {
            'coverage_id': {
                required: true,
            },
            'ft_si':{
                required: true,
            },
            'car_engine':{
                required: true,
            },
            'car_code':{
                required: true,
            },
            'define_name':{
                required: true,
            },
            'cctv':{
                required:true
            },
            'garage_type': {
                required:true
            },
            'deductible':{
                required:true
            },
            'additional_coverage':{
                required:true
            },
            'based_prem': {
                number: true
            },
            'based_prem_percent': {
                number: true
            },
            'name_policy': {
                number: true
            },
            'basic_premium_cover': {
                number: true
            },
            'add_premium_cover': {
                number: true
            },
            'fleet_percent': {
                number: true
            },
            'fleet': {
                number: true
            },
            'ncb_percent': {
                number: true
            },
            'ncb': {
                number: true
            },
            'total_premium': {
                number: true
            },
            'od_si': {
                number: true
            },
            'od_based_prem': {
                number: true
            },
            'od_total_premium': {
                number: true
            },
            'deduct_percent': {
                number: true
            },
            'deduct': {
                number: true
            },
            'cctv_discount_percent': {
                number: true
            },
            'cctv_discount': {
                number: true
            },
            'direct_percent': {
                number: true
            },
            'direct': {
                number: true
            },
            'net_premium': {
                number: true
            },
            'stamp': {
                number: true
            },
            'vat': {
                number: true
            },
            'gross_premium': {
                number: true
            },
            'flood_net_premium': {
                number: true
            },
            'flood_stamp': {
                number: true
            },
            'flood_vat': {
                number: true
            },
            'flood_gross_premium': {
                number: true
            },
        },
    });

    

    $(".input-recal").keypress(function (e) {
        if(!isNumber(e)){
            return false;
        }
    });

    $(".input-recal").keyup(function (e) {
        reCalculate()
    });

    
    

    //$('.input-money').simpleMoneyFormat();

    $('.form_pricing').on("submit",function(){
        if($(this).valid()){
            $(this).find(".input-money").each(function(index){
                var val = $(this).val();
                var real_val = val.replace(/,/g,'');
                $(this).val(real_val);
            });
        }else{
            e.preventDefault();
        }
    });



});