$(function() {
    function clearModal(){
        $("#field_username").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_password").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_name").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_lastname").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_email").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_privilege").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_status").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
    }

    function setModalData(id,type){
        $.ajax({
            url: $("[name='hd_get_data']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
                'type':type
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            console.log(result);
            if(!result.resp_error){
                if(!result.fail){
                    var data = result.data;
                    
                    if(data.username){
                        $("#field_username").html(data.username);
                    }
                    if(data.password){
                        $("#field_password").html(data.password);
                    }
                    if(data.name){
                        $("#field_name").html(data.name);
                    }
                    if(data.lastname){
                        $("#field_lastname").html(data.lastname);
                    }
                    if(data.email){
                        $("#field_email").html(data.email);
                    }
                    if(data.privilege){
                        $("#field_privilege").html(data.privilege);
                    }
                    if(data.status){
                        $("#field_status").html(data.status);
                    }
                }else if(result.fail){
                    notify(result.fail,"danger");
                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });
    }


    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_get_list']").val(),
              "data":function(d){
                  return {
                      'search_from_date':$("[name='search_from_date']").val(),
                      'search_to_date':$("[name='search_to_date']").val(),
                  }
              }
          },
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { 
                "data": "edit_data",
                "render":function(data,type,row){
                    return '<a class="cursor-pointer view-modal" data-id="'+row['id']+'" data-toggle="modal" data-target="#modal" data-type="edit"><i class="fa fa-search-plus"> View</i></a>'   
                }
              },
              { 
                "data": "old_data",
                "render":function(data,type,row){
                    return '<a class="cursor-pointer view-modal" data-id="'+row['id']+'" data-toggle="modal" data-target="#modal" data-type="old"><i class="fa fa-search-plus"> View</i></a>'   
                }
              },
              { "data": "created_at" },
              { "data": "created_by" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('User Management List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');


    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_from_date']" ).val(null);
        $("input[name='search_to_date']").val(null);
        table.ajax.reload();
    });

    $(document).on("click",".view-modal",function(){
        var id = $(this).data("id");
        var type = $(this).data("type");
        clearModal();
        setModalData(id,type);
    });

});