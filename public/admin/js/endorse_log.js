$(function() {

    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    function setPopOver(){
        $(document).find('[data-toggle="popover"]').popover();
        $('[data-toggle="popover"]').popover();
    }
    
    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_get_list']").val(),
              "data":function(d){
                  return {
                      'search_policy_number':$("[name='search_policy_number']").val(),
                      'search_status':$("[name='search_status']").val(),
                      'search_credit_number':$("[name='search_credit_number']").val(),
                      'search_from_date':$("[name='search_from_date']").val(),
                      'search_to_date':$("[name='search_to_date']").val(),
                      'search_av_ref_no':$("[name='search_av_ref']").val(),
                      'search_ac_ref_no':$("[name='search_ac_ref']").val(),
                  }
              }
          },
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "policy_number" },
              { "data": "endorse_no" },
              { "data": "av_reference_no" },
              { "data": "ac_reference_no" },
              { 
                "data": "data",
                "width":"15%",
                'render':function(data,type,row){
                    $html = '<a class="view-pointer" data-content="'+data+'" ><i class="fa fa-search-plus"> View</i></a>';
                    return $html;
                }
              },
              { 
                "data": "response",
                "width":"15%",
                'render':function(data,type,row){
                    $html = '<a class="view-pointer" data-content="'+data+'" ><i class="fa fa-search-plus"> View</i></a>';
                    return $html;
                }
              },
              { 
                "data": "status",
                "width":"5%",
                'render':function(data,type,row){
                    if(data=="SUCCESS"){
                        $html = '<label class="label label-success">'+data+'</label>';
                    }else{
                        $html = '<label class="label label-danger">'+data+'</label>';
                    }
                    return $html;
                }
              },
              { "data": "created_by" },
              { "data": "created_at" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        },
        "initComplete":function( settings, json){
            setPopOver();
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Endorse Log List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');


    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_policy_number']").val(null);
        $("select[name='search_status']").val(null);
        $("input[name='search_credit_number']").val(null);
        $("input[name='search_from_date']").val(null);
        $("input[name='search_to_date']").val(null);
        table.ajax.reload();
    });

    $(document).on("click",".view-pointer",function(){
        var json_data = $(this).data("content");
        var jsonPretty = JSON.stringify(json_data,null,4);  
        $("#modal-json-view").find("#post_area").html("");
        $("#modal-json-view").find("#post_area").append(document.createTextNode(jsonPretty));
        $("#modal-json-view").modal();
    });

});