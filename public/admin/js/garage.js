$(function() {

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_get_garage']").val(),
              "data":function(d){
                  return {
                      'search_type_class':$("[name='search_type_class']").val(),
                      'search_type':$("[name='search_type']").val(),
                      'search_provice':$("[name='search_provice']").val(),
                      'search_district':$("[name='search_district']").val(),
                      'search_subdistrict':$("[name='search_subdistrict']").val(),
                      'search_name':$("[name='search_name']").val(),
                  }
              }
          },
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "type_class" },
              { "data": "type" },
              { "data": "name" },
              { "data": "province" },
              { "data": "district" },
              { "data": "sub_district" },
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Garage Type List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_garage']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          console.log(result);
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("select[name='search_type_class']" ).val(null);
        $("select[name='search_type']").val(null);
        $("select[name='search_provice']").val(null);
        $("select[name='search_district']").val(null);
        $("select[name='search_subdistrict']").val(null);
        $("input[name='search_name']").val(null);
        table.ajax.reload();
    });

    $("select[name='search_provice']" ).change(function(){
        var id = $(this).val();
        if(id!=null){
            $.ajax({
                url: $("[name='hd_get_data_district']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">District</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='search_district']").html(select);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }
        
    });

    $("select[name='search_district']" ).change(function(){
        var id = $( "select[name='search_district']" ).val();
        if(id!=null){
            $.ajax({
                url: $("[name='hd_get_data_subdistrict']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'district_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">Sub District</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='search_subdistrict']").html(select);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }
        
    });


});