$(function() {
    var _imported_table = null;
    $('.custom-file').pxFile();
    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          serverSide: true,
          stateSave: false,
          pageLength: 15,
          ajax :{
              "url": $("input[name='hd_get_vehicle_list']").val(),
              "data":function(d){
                    d._token = $('meta[name="csrf-token"]').attr('content');
                    d.search_body_type = $("[name='search_body_type']").val();
                    d.search_year = $("[name='search_year']").val();
                    d.search_brand = $("[name='search_brand']").val();
                    d.search_model = $("[name='search_model']").val();
                    d.search_model_type = $("[name='search_model_type']").val();
                    return d;
                },
          },
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "brand_name" },
              { "data": "model_name" },
              { "data": "year" },
              { "data": "body_type" },
              { "data": "model_type" },
              { "data": "mortor_code_av" },
              { "data": "mortor_code_ac" },
              { "data": "action",orderable: false,"width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Vehicle List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_vehicle']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("select[name='search_body_type']" ).val(null);
        $("select[name='search_year']").val(null);
        $("select[name='search_brand']").val(null);
        $("select[name='search_model']").val(null);
        $("input[name='search_model_type']").val(null);
        table.ajax.reload();
    });

    $("select[name='search_provice']" ).change(function(){
        var id = $(this).val();
        if(id!=null){
            $.ajax({
                url: $("[name='hd_get_data_district']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">District</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='search_district']").html(select);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }
        
    });

    $("select[name='search_brand']" ).change(function(){
        var id = $(this).val();
        if(id!=null && id!=""){
            $.ajax({
                url: $("[name='hd_get_select_model']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'brand_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value>Model Name</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='search_model']").html(select);
                }else{
                    notify("Error please try again.","danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify("Error please try again.","danger");
                }
            })
        }else{
            select = '<option selected="selected" value>Model Name</option>';
            $("select[name='search_model']").html(select);
        }
    });


    function showImportedModal(){

        _imported_table = $('#datatablesImported').DataTable({
            processing: true,
            serverSide: true,
            stateSave: false,
            pageLength: 5,
            ajax :{
                "url": $("input[name='hd_imported_list']").val(),
                "data":function(d){
                    d._token = $('meta[name="csrf-token"]').attr('content');
                    return d;
                },
            },
            columns: [
                { "data": null,"width":"10%",orderable: false},
                { "data": "brand_name" },
                { "data": "model_name" },
                { "data": "year" },
                { "data": "body_type" },
                { "data": "model_type" },
                { "data": "mortor_code_av" },
                { "data": "mortor_code_ac" },
                { "data": "cc" },
                { "data": "tons" },
                { "data": "car_seat" },
                { "data": "driver_passenger" },
                { "data": "red_plate" },
                { "data": "used_car" },
                { "data": "car_age" }
            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
              var info = $(this).DataTable().page.info();
              $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
              return nRow;
          }
      });
      
      $("#modal-imported").modal();
    }

    $("#submit_import").click(function(){
        var handle = $(this);
        var fileName = $("#csv_file").val();
        if(fileName) {
            var formData = new FormData();
            formData.append('file', $('#csv_file')[0].files[0]);
            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
            /* ------------------Chunks------------------ */
            handle.addClass("btn-loading");
            handle.attr("disabled","disabled");
            $(".import-status").html("Upload Processing...");
            $.ajax({
                url : $("input[name='hd_chunks']").val(),
                type : 'POST',
                data : formData,
                dataType: "json",
                processData: false,
                contentType: false,
            }).done(function(upload_result){
                console.log(upload_result);
                if(!upload_result.resp_error){
                    if(upload_result.status=="SUCCESS"){
                        /* ---------------Import---------------- */
                        $(".import-status").html("Import Processing Please wait...");
                        $.ajax({
                            url : $("input[name='hd_import']").val(),
                            type : 'POST',
                            data : {
                                '_token': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType: "json",
                        }).done(function(import_result){
                            if(!import_result.resp_error){
                                if(import_result.status=="SUCCESS"){
                                    $("#modal-import").modal("hide");
                                    showImportedModal();
                                }
                            }else{
                                notify(import_result.resp_error,"danger");
                            }
                        }).fail(function(jqXHR, textStatus){
                            if(textStatus == 'timeout'){
                                notify("Server Response Timeout!","danger");
                            }
                            else{
                                notify(textStatus,"danger");
                            }
                        }).always(function(){
                            handle.removeClass("btn-loading");
                            handle.removeAttr("disabled","disabled");
                            $(".import-status").html("");
                            var $el = $('#csv_file');
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                        });
                    }
                }else{
                    notify(upload_result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
                handle.removeClass("btn-loading");
                handle.removeAttr("disabled","disabled");
            });
        }
    });

    $("#submit_import_change").click(function(){
        $.ajax({
            url : $("input[name='hd_imported_submit']").val(),
            type : 'POST',
            data : {
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
        }).done(function(import_result){

            if(!import_result.resp_error){
                if(import_result.status=="SUCCESS"){
                    notify("Import File Successfully.","success");
                    $("#modal-imported").modal('hide');
                    table.ajax.reload();
                    _imported_table.destroy();
                }
            }else{
                notify(import_result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
    });

    $("#test").click(function(){
        showImportedModal();
    });


});