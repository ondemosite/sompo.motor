$(function() {

    function clearModal(){
      $("#form_configs").find("input[name='id']").val(null);
      $("#form_configs").find("input[name='title']").val(null);
      $("#form_configs").find("input[name='config_code']").val(null);
      $("#form_configs").find("input[name='config_value']").val(null);
      $("#form_configs").find("input[name='start_date']").val(null);
      $("#form_configs").find("input[name='expire_date']").val(null);
      $("#form_configs").find("input[name='status']").prop('checked', false);
      $("#modal-add").find(".modal-title").find("span").html("Add API");
    }

    $("#open-modal").click(function(){
      $("#form_configs").find("input[name='title']").focus();
      clearModal();
    });

    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'bottom left',
        format: 'yyyy-mm-dd',
    });

    function parseHTML(html) {
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>'+ html,'text/html');
        return dom.body.textContent;
    }

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          serverSide: true,
          ajax: $("input[name='hd_list']").val(),
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "title","width":"20%"},
              { "data": "config_code","width":"20%"},
              { "data": "start_date","width":"10%"},
              { "data": "expire_date","width":"10%"},
              { 
                "name": "status",
                "data": "status",
                'render':function(data,type,row){
                    if(data==0){
                        return '<span class="label label-danger">INACTIVE</span>';
                    }else{
                        return '<span class="label label-success">ACTIVE</span>';
                    }
                }
              },
              { "data": "updated_at","width":"10%"},
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Token List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_data']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  clearModal();
                  $("#form_configs").find("input[name='id']").val(result.id);
                  $("#form_configs").find("input[name='title']").val(result.title);
                  $("#form_configs").find("input[name='config_code']").val(result.config_code);
                  $("#form_configs").find("input[name='config_value']").val(result.config_value);
                  $("#form_configs").find("input[name='start_date']").val(result.start_date);
                  $("#form_configs").find("input[name='expire_date']").val(result.expire_date);
                  if(result.status==1){
                    $("#form_configs").find("input[name='status']").prop('checked', true);
                  }else{
                    $("#form_configs").find("input[name='status']").prop('checked', false);
                  }

                  $("#modal-add").modal('toggle');
                  $("#modal-add").find(".modal-title").find("span").html("Edit API Token");
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

    //Edit Status  
    $(document).on("change",".edit-status",function(){
        var id = $(this).val();
        var type = 0;
        if($(this).is(':checked')) type = 1;
        $.ajax({
            url: $("input[name='hd_status']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'type':type,
                'id':id
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            notify(result.resp_error,"danger");
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
        
  });


  //------------------------

  // Validate Modal --------
  $('#form_configs').pxValidate({
      rules: {
        'title': {
            required: true,
        },
        'config_code':{
            required: true,
        },
        'config_value':{
            required: true,
        },
        'start_date':{
            required: true,
        },
        'expire_date':{
            required: true,
        }
      },
  });
  //------------------------

  // Modal Event -----------
  function saveData(){
      if($('#form_configs').valid()){
          $.ajax({
              url: $("input[name='hd_save']").val(),
              data: $("#form_configs").serialize(),
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  if(result.status == "success"){
                      table.ajax.reload();
                      notify("Save Data Success.","success");
                  }
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          }).always(function() {
              $("#modal-add").modal('toggle');
          });
      }
  }

  $("#modal-add #submit_button").click(function(){
      saveData();
  });
  //------------------------


});