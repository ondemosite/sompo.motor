$(function() {

    $('.summernote').summernote({
        height: 200,
        toolbar: [
          ['parastyle', ['style']],
          ['fontstyle', ['fontname', 'fontsize']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['insert', ['picture', 'link', 'video', 'table', 'hr']],
          ['history', ['undo', 'redo']],
          ['misc', ['codeview']],
          ['help', ['help']]
        ],
    });


    function clearModal(){
      $("#form_faq").find("input[name='question']").val(null);
      $("#form_faq").find("[name='answer']").summernote('code',null);
      $("#modal-add").find(".modal-title").find("span").html("Add Faq");
    }

    $("#open-modal").click(function(){
      $("#form_faq").find("input[name='question']").focus();
      clearModal();
    });

    function parseHTML(html) {
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>'+ html,'text/html');
        return dom.body.textContent;
    }

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          serverSide: true,
          ajax: $("input[name='hd_get_faq_list']").val(),
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { 
                  "data": "question",
                  "width":"70%",
                  'render':function(data,type,row){
                    var obj = $.parseJSON(parseHTML(data));
                    html = '<div><a class="accordion-toggle collapsed" data-toggle="collapse" href="#target-'+row.id+'" aria-expanded="false"><i class="fa fa-plus"></i>&nbsp;&nbsp;'+obj.question+'</a></div>';
                    html+= '<div id="target-'+row.id+'" class="panel-collapse collapse"><div class="panel-body panel-prem">'+obj.answer+'</div></div>';
                    return html;
                  }
              },
              { "data": "status","width":"10%","sortable": false,
                "render":function(data,type,row){
                    var checked = "";
                    if(row.status==1){
                        checked = "checked";
                    }
                    return '<label for="switcher-rounded-'+row.id+'" class="switcher switcher-danger"><input class="edit-status" type="checkbox" id="switcher-rounded-'+row.id+'" '+checked+' value="'+row.id+'"><div class="switcher-indicator"><div class="switcher-yes">YES</div><div class="switcher-no">NO</div></div></label>';
                }
              },
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Faq List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_get_data_faq']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  clearModal();
                  $("#form_faq").find("input[name='id']").val(result.id);
                  $("#form_faq").find("input[name='question']").val(result.question);
                  $("#form_faq").find("[name='answer']").summernote('code',result.answer);
                  $("#modal-add").modal('toggle');
                  $("#modal-add").find(".modal-title").find("span").html("Edit Faq");
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_faq']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

    //Edit Status  
    $(document).on("change",".edit-status",function(){
        var id = $(this).val();
        var type = 0;
        if($(this).is(':checked')) type = 1;
        $.ajax({
            url: $("input[name='hd_set_status']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'type':type,
                'id':id
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            notify(result.resp_error,"danger");
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
        
  });


  //------------------------

  // Validate Modal --------
  $('#form_faq').pxValidate({
      rules: {
        'question': {
            required: true,
        },
        'answer':{
            required: true,
        }
      },
  });
  //------------------------

  // Modal Event -----------
  function saveData(){
      if($('#form_faq').valid()){
          $.ajax({
              url: $("input[name='hd_save_faq']").val(),
              data: $("#form_faq").serialize(),
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  if(result.status == "success"){
                      table.ajax.reload();
                      notify("Save Data Success.","success");
                  }
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          }).always(function() {
              $("#modal-add").modal('toggle');
          });
      }
  }

  $("#modal-add #submit_button").click(function(){
      saveData();
  });
  //------------------------


});