$(document).ready(function(){
    _state_change = false;
    _state_edit_doc = false;
    $('.custom-file').pxFile();
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        format: 'yyyy-mm-dd',
    });

    function clearEditDataModal(){
        $("#field_driver1_name").val("");
        $("#field_driver1_lastname").val("");
        $("#field_driver1_idcard").val("");
        $("#field_driver1_licence").val("");
        $("#field_driver1_birthdate").val("");
        $("input[name='driver1_gender']").prop("checked", false);

        $("#field_driver2_name").val("");
        $("#field_driver2_lastname").val("");
        $("#field_driver2_idcard").val("");
        $("#field_driver2_licence").val("");
        $("#field_driver2_birthdate").val("");
        $("input[name='field_driver2_gender']").prop("checked", false);
    }

    function clearDocDataModal(){
        $("#field_doc_id_card").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_doc_licence").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_doc_driver1_licence").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_doc_driver2_licence").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_doc_cctv_inside").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
        $("#field_doc_cctv_outside").html('<p class="text-muted">ไม่มีการแก้ไข</p>');
    }

    function clearReceiptModal(){
        $("#field_rc_order").html('<p class="text-muted">-</p>');
        $("#field_rc_channel").html('<p class="text-muted">-</p>');
        $("#field_rc_credit_number").html('<p class="text-muted">-</p>');
        $("#field_rc_scheme").html('<p class="text-muted">-</p>');
        $("#field_rc_amount").html('<p class="text-muted">-</p>');
    }

    function clearResendInput(){
        $("[name='resend_files']").prop('checked', false); 
    }

    function getEndorse(){
        var id = $("[name='id']").val();
        $.ajax({
            url: $("[name='hd_endorse_get']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                $("#tab-resize-info").html("");
                $(".tab-content-info").html("");
                $.each(result,function(index,value){
                    var tab_html = "";
                    var content_html = "";
                    tab_html+='<li class="'+(index==(result.length-1)?"active":'')+'"><a href="#tab-resize-info-'+index+'" data-toggle="tab" aria-expanded="'+(index==(result.length-1)?"true":'false')+'">'+result[index].endorse_no+'</a></li>';
                    content_html+='<div class="tab-pane '+(index==(result.length-1)?"active":'')+'" id="tab-resize-info-'+index+'">';
                    /* Owner */
                    content_html+='<table width="100%">';
                    content_html+='<tr><td class="td-name" width="40%">ผู้ขับขี่หลัก</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].owner.fullname+'</label></td></tr>';
                    content_html+='<tr><td class="td-name" width="40%">วันเกิด</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].owner.birth_date+'</label></td></tr>';
                    content_html+='<tr><td class="td-name" width="40%">เลขที่บัตรประชาชน</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].owner.idcard+'</label></td></tr>';
                    content_html+='<tr><td class="td-name" width="40%">เพศ</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].owner.gender+'</label></td></tr>';
                    content_html+='<tr><td class="td-name" width="40%">ที่อยู่</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].owner.address+'</label></td></tr>';
                    content_html+='<tr><td class="td-name" width="40%">เบอร์โทรศัพท์</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].owner.tel+'</label></td></tr>';
                    content_html+='<tr><td class="td-name" width="40%">E-mail</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].owner.email+'</label></td></tr>';
                    content_html+='</table>';
                    content_html+='<hr/>';
                    /* Driver1 */
                    if(result[index].driver1!=null){
                        content_html+='<table width="100%">';
                        content_html+='<tr><td class="td-name" width="40%">ผู้ขับขี่คนที่ 1</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver1.fullname+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">วันเกิด</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver1.birth_date+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">เลขที่บัตรประชาชน</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver1.idcard+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">เลขที่ใบขับขี่</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver1.licence+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">เพศ</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver1.gender+'</label></td></tr>';
                        content_html+='</table>';
                        content_html+='<hr/>';
                    }
                    
                    /* Driver2 */
                    if(result[index].driver2!=null){
                        content_html+='<table width="100%">';
                        content_html+='<tr><td class="td-name" width="40%">ผู้ขับขี่คนที่ 2</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver2.fullname+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">วันเกิด</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver2.birth_date+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">เลขที่บัตรประชาชน</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver2.idcard+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">เลขที่ใบขับขี่</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver2.licence+'</label></td></tr>';
                        content_html+='<tr><td class="td-name" width="40%">เพศ</td><td class="td-name" width="5%">:</td><td class="td-value"><label class="lb-name">'+result[index].driver2.gender+'</label></td></tr>';
                        content_html+='</table>';
                        content_html+='<hr/>';
                    }
                    content_html+='</div>';
                    $("#tab-resize-info").append(tab_html);
                    $(".tab-content-info").append(content_html);
                });
                $('#tab-resize-info').pxTabResize('destroy');
                $('#tab-resize-info').pxTabResize();
            }else{
                notify("Error","danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });
    }getEndorse();

    function getPolicyDocLog(){
        var id = $("[name='id']").val();
        $.ajax({
            url: $("[name='hd_policy_doc_log']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                if(!result.fail){
                    var html = "";
                    $.each(result,function(index,value){
                       html+='<tr><td>'+(index+1)+'</td><td>'+(result[index].date)+'</td><td><a id="view-doc-modal" class="view-modal" data-id="'+result[index].id+'" data-toggle="modal" data-target="#modal-doc-log" title="ดูข้อมูลเก่า"><i class="fa fa-search"></i> View</a></td><td>'+result[index].admin+'</td></tr>'
                    });
                    if(html=="") html = "<tr><td colspan=\"4\" align=\"center\">ยังไม่พบข้อมูล</td></tr>"
                    $("#edit_documment_body").html(html);
                }else if(result.fail){
                    $("#edit_documment_body").html("<tr><td colspan=\"4\">No Data Found.</td></tr>");
                }else{
                    $("#edit_documment_body").html("<tr><td colspan=\"4\">Permission Denied!</td></tr>");
                }
            }else{
                notify("Error","danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });
    }getPolicyDocLog();


    function getEmailLog(){
        var id = $("[name='id']").val();
        $.ajax({
            url: $("[name='hd_policy_email_log']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                if(!result.fail){
                    var html = "";
                    $.each(result,function(index,value){
                       html+='<tr><td>'+(index+1)+'</td><td>'+(result[index].created_at)+'</td><td>'+result[index].files+'</td><td>'+result[index].status+'</td><td>'+result[index].created_by+'</td></tr>'
                    });
                    $("#email_log_body").html(html);
                }else if(result.fail){
                    $("#email_log_body").html("<tr><td colspan=\"4\">No Data Found.</td></tr>");
                }else{
                    $("#email_log_body").html("<tr><td colspan=\"4\">Permission Denied!</td></tr>");
                }
            }else{
                notify("Error","danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });
    }getEmailLog();

    // function getEPassLog(){
    //     var id = $("[name='id']").val();
    //     $.ajax({
    //         url: $("[name='hd_policy_epass_log']").val(),
    //         type: "post",
    //         data: {
    //             '_token': $('meta[name="csrf-token"]').attr('content'),
    //             'id':id,
    //         },
    //         dataType: "json",
    //         timeout:10000
    //     }).done(function(result){
    //         if(!result.resp_error){
    //             if(!result.fail){
    //                 var html = "";
    //                 $.each(result,function(index,value){
    //                    html+='<tr><td>'+(index+1)+'</td><td>'+(result[index].created_at)+'</td><td>'+result[index].endorse_no+'</td><td>'+result[index].status+'</td><td>'+result[index].created_by+'</td></tr>'
    //                 });
    //                 $("#epass_log_body").html(html);
    //             }else if(result.fail){
    //                 $("#epass_log_body").html("<tr><td colspan=\"4\">No Data Found.</td></tr>");
    //             }else{
    //                 $("#epass_log_body").html("<tr><td colspan=\"4\">Permission Denied!</td></tr>");
    //             }
    //         }else{
    //             notify("Error","danger");
    //         }
    //     }).fail(function(jqXHR, textStatus){
    //         notify(textStatus,"danger");
    //     });
    // }getEPassLog();

    
    function setDocLogData(id){
        $.ajax({
            url: $("[name='hd_policy_doc_log_data']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            //dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                if(!result.fail){
                    var data = $.parseJSON(result);
                    if(data.personal_id){
                        var html = '<a class="lb-doc" href="'+data.personal_id+'" title="สำเนาบัตรประชาชน" target="_blank"><i class="fa fa-search"></i> View</a>';
                        $("#field_doc_id_card").html(html);
                    }
                    if(data.car_licence){
                        var html = '<a class="lb-doc" href="'+data.car_licence+'" title="สำเนาใบขับขี่" target="_blank"><i class="fa fa-search"></i> View</a>';
                        $("#field_doc_licence").html(html);
                    }
                    if(data.cctv_inside){
                        var html = '<a class="lb-doc" href="'+data.cctv_inside+'" title="กล้อง CCTV ภายใน" target="_blank"><i class="fa fa-search"></i> View</a>';
                        $("#field_doc_cctv_inside").html(html);
                    }
                    if(data.cctv_outside){
                        var html = '<a class="lb-doc" href="'+data.cctv_outside+'" title="กล้อง CCTV ภายนอก" target="_blank"><i class="fa fa-search"></i> View</a>';
                        $("#field_doc_cctv_outside").html(html);
                    }
                    if(data.driver1_licence){
                        var html = '<a class="lb-doc" href="'+data.driver1_licence+'" title="สำเนาใบขับขี่ คนที่1" target="_blank"><i class="fa fa-search"></i> View</a>';
                        $("#field_doc_driver1_licence").html(html);
                    }
                    if(data.driver2_licence){
                        var html = '<a class="lb-doc" href="'+data.driver2_licence+'" title="สำเนาใบขับขี่ คนที่2" target="_blank"><i class="fa fa-search"></i> View</a>';
                        $("#field_doc_driver2_licence").html(html);
                    }
                }else if(result.fail){
                    notify(result.fail,"danger");
                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });
    }

    function setReceiptData(id){
        $.ajax({
            url: $("[name='hd_policy_receipt_data']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                if(!result.fail){
                    var data = result;
                    if(data.receipt_number){
                        $("#tax_number").html(data.receipt_number);
                    }
                    if(data.created_at){
                        $("#receipe_created_at").html(data.created_at);
                    }
                    if(data.receipt_order){
                        $("#field_rc_order").html(data.receipt_order);
                    }
                    if(data.receipt_channel){
                        $("#field_rc_channel").html(data.receipt_channel);
                    }
                    if(data.receipt_credit_number){
                        $("#field_rc_credit_number").html(data.receipt_credit_number);
                    }
                    if(data.receipt_scheme){
                        $("#field_rc_scheme").html(data.receipt_scheme);
                    }
                    if(data.receipt_amount){
                        $("#field_rc_amount").html(data.receipt_amount);
                    }
                }else if(result.fail){
                    notify(result.fail,"danger");
                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });
    }

    function saveEditPolicyDoc(){
        var id = $("[name='id']").val();
        var form = $("#form_edit_doc");
        $("#form_edit_doc").addClass("form-loading");
        $.ajax({
            url: $("#form_edit_doc").attr("action"),
            type: "post",
            data: new FormData(form[0]),
            contentType: false,
            cache: false,
            processData:false,
            dataType: "json",
            timeout:10000,
        }).done(function(result){
            if(!result.resp_error){
                if(result.status=="success"){
                    notify("Save Data Success","success");
                    location.reload();
                }else{
                    notify(result.reason,"danger"); 
                }
            }else{
                notify("Error","danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify("Error","danger");
        }).always(function(){
            $("#form_edit_doc").removeClass("form-loading");
            $(".input-name").addClass("disable");
            $("#row_save_name").hide();
            $(".lb-doc").show();
        });
    }

    function getPolicyEndorse(){
        var id = $("[name='id']").val();
        $.ajax({
            url: $("[name='hd_endorse_first']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            dataType: "json",
            timeout:10000,
        }).done(function(result){
            if(!result.resp_error){
                $("#field_name").val(result.owner.name);
                $("#field_lastname").val(result.owner.lastname);
                $("#field_birthdate").val(result.owner.birth_date);
                $("#field_idcard").val(result.owner.idcard);
                $("#field_tel").val(result.owner.tel);
                $("#field_email").val(result.owner.email);
                $("input[name='main_gender'][value='"+result.owner.gender+"']").prop('checked', true);
                $("#field_address").val(result.owner.address);
                $("#field_postalcode").val(result.owner.postalcode);
                _state_change = true;
                $("select[name='main_province']" ).val(result.owner.province).change();
                setDistrict(result.owner.district,result.owner.subdistrict);

                if(result.driver1!=null){
                    $("#field_driver1_name").val(result.driver1.name);
                    $("#field_driver1_lastname").val(result.driver1.lastname);
                    $("#field_driver1_birthdate").val(result.driver1.birth_date);
                    $("#field_driver1_idcard").val(result.driver1.idcard);
                    $("#field_driver1_licence").val(result.driver1.licence);
                    $("input[name='driver1_gender'][value='"+result.driver1.gender+"']").prop('checked', true);
                    $("#info-box-1").show();
                }else{
                    $("#info-box-1").hide();
                }
                if(result.driver2!=null){
                    $("#field_driver2_name").val(result.driver2.name);
                    $("#field_driver2_lastname").val(result.driver2.lastname);
                    $("#field_driver2_birthdate").val(result.driver2.birth_date);
                    $("#field_driver2_idcard").val(result.driver2.idcard);
                    $("#field_driver2_licence").val(result.driver2.licence);
                    $("input[name='driver2_gender'][value='"+result.driver2.gender+"']").prop('checked', true);
                    $("#info-box-2").show();
                }else{
                    $("#info-box-2").hide();
                }
            }else{
                notify("Error","danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify("Error","danger");
        });
    }


    function setDistrict(value,subvalue){
        var id = $("select[name='main_province']" ).val();
        $.ajax({
            url: $("[name='hd_get_district']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'province_id':id
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                select = '<option selected="selected" value disabled>เลือก เขต/อำเภอ</option>';
                $.each(result, function(i,data)
                {
                    select +='<option value="'+data+'">'+i+'</option>';
                });
                select += '</select>';
                $("select[name='main_district']").html(select);
                $("select[name='main_district']").val(value).change();
                setSubdistrict(subvalue);
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
    }

    function setSubdistrict(value){
        var province_id = $("select[name='main_province']" ).val();
        var id = $("select[name='main_district']" ).val();
        $.ajax({
            url: $("[name='hd_get_subdistrict']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'province_id':province_id,
                'district_id':id
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                select = '<option selected="selected" value disabled>เลือก แขวง/ตำบล</option>';
                $.each(result, function(i,data)
                {
                    select +='<option value="'+data+'">'+i+'</option>';
                });
                select += '</select>';
                $("select[name='main_subdistrict']").html(select);
                $("select[name='main_subdistrict']").val(value).change();
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        }).always(function(){
            _state_change = false;
        })
    }

    function resendDocument(){
        var id = $("[name='id']").val();
        $("#bt-resend-submit").attr("disabled","disabled");
        $("#bt-resend-submit").addClass("btn-loading");
        var val = [];
        $("[name='resend_files']:checked").each(function(i){
          val[i] = $(this).val();
        });
        
        if(val.length>0){
            $(".resend-status").html("Sending Process...");
            $.ajax({
                url: $("[name='hd_policy_resend_doc']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id,
                    'files':JSON.stringify(val)
                },
                dataType: "json",
            }).done(function(result){
                if(!result.resp_error){
                    if(result.fail){
                        notify(result.fail,"danger");
                    }else{
                        notify("Resend Email Success!","success");
                        $("#modal-resend").modal("hide");
                        clearResendInput();
                    }
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                notify(textStatus,"danger");
            }).always(function() {
                $("#bt-resend-submit").removeAttr("disabled");
                $("#bt-resend-submit").removeClass("btn-loading");
                $("html, body").animate({ scrollTop: 0 }, "fast");
                $(".resend-status").html("");
                getEmailLog();
            });
        }
    }

    function resendSMS(){
        bootbox.confirm({
            message:   'ต้องการจัดส่ง E-Pass & SMS อีกครั้งหรือไม่?',
            className: 'bootbox-sm',
            callback: function(result) {
                if(result){
                    var id = $("[name='id']").val();
                    $("#bt-resend-sms").attr("disabled","disabled");
                    $("#bt-resend-sms").addClass("btn-loading");

                    $.ajax({
                        url: $("[name='hd_resend_sms']").val(),
                        type: "post",
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content'),
                            'id':id,
                        },
                        dataType: "json",
                    }).done(function(result){
                        if(!result.resp_error){
                            if(result.fail){
                                notify(result.fail,"danger");
                            }else{
                                notify("Resend E-Pass & SMS Success!","success");
                            }
                        }else{
                            notify(result.resp_error,"danger");
                        }
                    }).fail(function(jqXHR, textStatus){
                        notify(textStatus,"danger");
                    }).always(function() {
                        $("#bt-resend-sms").removeAttr("disabled");
                        $("#bt-resend-sms").removeClass("btn-loading");
                        $("html, body").animate({ scrollTop: 0 }, "fast");
                        //getEPassLog();
                    });
                }
            },
        });
    }

    function getUpdateCore(){
        var id = $("[name='id']").val();
        $.ajax({
            url: $("[name='hd_get_update_core']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            dataType: "json",
        }).done(function(result){
            if(!result.resp_error){
                if(result.result){
                    if(result.result=="yes"){
                        $("#text-update-core").find(".update-btn").html('<label class="label label-success font-size-14">Yes</label>');
                        //$(".bt-update-endorse").removeClass("hidden");
                    }else{
                        $("#text-update-core").find(".update-btn").html('<label class="label label-danger font-size-14">No</label>');
                        //$(".bt-update-endorse").addClass("hidden");
                    }
                }else{
                    notify("Failed to get update Core system Status","warning");
                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        }).always(function() {
            $("#bt-resend-sms").removeAttr("disabled");
            $("#bt-resend-sms").removeClass("btn-loading");
            $("html, body").animate({ scrollTop: 0 }, "fast");
            //getEPassLog();
        });
    }

    jQuery.validator.addMethod("id_card", function(value, element) {
        var pid = value;
        pid = pid.toString().replace(/\D/g,'');
        if(pid.length == 13){
                var sum = 0;
                for(var i = 0; i < pid.length-1; i++){
                sum += Number(pid.charAt(i))*(pid.length-i);
                }
                var last_digit = (11 - sum % 11) % 10;
                $(element).val(pid);
                return pid.charAt(12) == last_digit;
        }else{
                return false;
        }
    }, "เลขบัตรประชาชนไม่ถูกต้อง!");

    $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[0-9a-zA-Z\_]+$/);
    }, "Invalid Data!");

    // Validate  --------
    $('#form_info').validate({
        rules: {
            'main_name': {
                required: true,
            },
            'main_lastname': {
                required: true,
            },
            'main_birthdate':{
                required: true,
            },
            'main_licence':{
                required:true,
                alpha:true
            },
            'main_idcard':{
                required: true,
                number:true,
                id_card:true
            },
            'main_address':{
                required:true
            },
            'main_tel':{
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            },
            'main_email':{
                required:true,
                email:true
            },
            'main_gender':{
                required:true,
            },
            'main_province':{
                required:true
            },
            'main_district':{
                required:true
            },
            'main_subdistrict':{
                required:true
            },
            
            'driver1_name':{
                required: true,
            },
            'driver1_lastname':{
                required: true,
            },
            'driver1_birthdate':{
                required: true,
            },
            'driver1_idcard':{
                required: true,
                number:true,
                id_card:true
            },
            'driver1_licence':{
                required:true,
                alpha:true
            },
            'driver1_gender':{
                required:true,
            },

            'driver2_name':{
                required: true,
            },
            'driver2_lastname':{
                required: true,
            },
            'driver2_birthdate':{
                required: true,
            },
            'driver2_idcard':{
                required: true,
                number:true,
                id_card:true
            },
            'driver2_licence':{
                required:true,
                alpha:true
            },
            'driver2_gender':{
                required:true,
            },
            
        },
    });

    $(".submit-info").click(function(){
        var handle = $(this);
        
        if($("#form_info").valid()){
            var mode = $(this).data("mode");
            $("[name='is_core']").val(mode);
            handle.addClass("btn-loading");
            handle.attr("disabled","disabled");
            $.ajax({
                url: $("[name='hd_endorse_save']").val(),
                type: "post",
                data: $("#form_info").serialize(),
                dataType: "json",
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status=="success"){
                        notify("Save Data Success","success");
                        setTimeout(function(){
                            location.reload();
                        },1500);
                        getEndorse();
                        $('#modal-info').modal('toggle');
                    }else{
                        notify(result.reason,"danger"); 
                    }
                }else{
                    notify("Error","danger");
                }
            }).fail(function(jqXHR, textStatus){
                notify("Error","danger");
            }).always(function(){
                handle.removeClass("btn-loading");
                handle.removeAttr("disabled");
                $("html, body").animate({ scrollTop: 0 }, "fast");
                getUpdateCore();
            });
        }
    });

    $(".bt-update-endorse").click(function(){
        bootbox.confirm({
            message:   'ต้องการอัพเดทข้อมูลไปที่ Core System หรือไม่?',
            className: 'bootbox-sm',
            callback: function(result) {
                if(result){
                    var id = $("[name='id']").val();
                    $(".bt-update-endorse").attr("disabled","disabled");
                    $(".bt-update-endorse").addClass("btn-loading");

                    $.ajax({
                        url: $("[name='hd_update_core']").val(),
                        type: "post",
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content'),
                            'id':id,
                        },
                        dataType: "json",
                    }).done(function(result){
                        if(!result.resp_error){
                            if(result.fail){
                                notify(result.fail,"danger");
                            }else{
                                notify("Update Data To Core System Success!","success");
                            }
                        }else{
                            notify(result.resp_error,"danger");
                        }
                    }).fail(function(jqXHR, textStatus){
                        notify(textStatus,"danger");
                    }).always(function() {
                        $(".bt-update-endorse").removeAttr("disabled");
                        $(".bt-update-endorse").removeClass("btn-loading");
                        $("html, body").animate({ scrollTop: 0 }, "fast");
                        getUpdateCore();
                    });
                }
            },
        });
    });


    $("#save_note").click(function(){
        var id = $("[name='id']").val();
        var text = $("[name='note']").val();
        if(text!=null){
            $("#save_note").addClass("btn-loading");
            $.ajax({
                url: $("[name='hd_policy_note']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id,
                    'value':text
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.fail){
                        notify("Server Response Timeout!","danger");
                    }else{
                        notify("Save Note Success","success");
                    }
                }
            }).always(function() {
                $("#save_note").removeClass("btn-loading");
                $("html, body").animate({ scrollTop: 0 }, "fast");
            });
        }
    }); 

    $(".bt-edit-doc").click(function(){
        if(_state_edit_doc==false){
            _state_edit_doc = true;
            $(".input-doc").removeClass("disable");
            $("#row_save_doc").show();
            $(".lb-doc").hide();
        }else{
            _state_edit_doc = false;
            $(".input-doc").addClass("disable");
            $("#row_save_name").hide();
            $(".lb-doc").show();
        }
    });

    $("#save-edit-doc").click(function(){
        if($("#form_edit_doc").valid()){
            saveEditPolicyDoc();
        }
    });
    $("#bt-resend-submit").click(function(){
        resendDocument();
    });

    $("#bt-resend-sms").click(function(){
        resendSMS();
    });

    $("select[name='main_province']" ).change(function(){
        if(!_state_change){
            var id = $(this).val();
            if(id!=null){
                $.ajax({
                    url: $("[name='hd_get_district']").val(),
                    type: "post",
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content'),
                        'province_id':id
                    },
                    dataType: "json",
                    timeout:10000
                }).done(function(result){
                    if(!result.resp_error){
                        select = '<option selected="selected" value disabled>เลือก เขต/อำเภอ</option>';
                        $.each(result, function(i,data)
                        {
                            select +='<option value="'+i+'">'+data+'</option>';
                        });
                        select += '</select>';
                        $("select[name='main_district']").html(select);
                        $("select[name='main_subdistrict']").html('<option selected="selected" value disabled>เลือก แขวง/ตำบล</option>');
                    }
                }).fail(function(jqXHR, textStatus){
                    if(textStatus == 'timeout'){
                        notify("Server Response Timeout!","danger");
                    }
                    else{
                        notify(textStatus,"danger");
                    }
                });
            }
        }
    });


    $("select[name='main_district']" ).change(function(){
        if(!_state_change){
            var id = $(this).val();
            var province_id = $("select[name='main_province']" ).val();
            if(id!=null && province_id!=null){
                $.ajax({
                    url: $("[name='hd_get_subdistrict']").val(),
                    type: "post",
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content'),
                        'province_id':province_id,
                        'district_id':id
                    },
                    dataType: "json",
                    timeout:10000
                }).done(function(result){
                    if(!result.resp_error){
                        select = '<option selected="selected" value disabled>เลือก แขวง/ตำบล</option>';
                        $.each(result, function(i,data)
                        {
                            select +='<option value="'+i+'">'+data+'</option>';
                        });
                        select += '</select>';
                        $("select[name='main_subdistrict']").html(select);
                    }
                }).fail(function(jqXHR, textStatus){
                    if(textStatus == 'timeout'){
                        notify("Server Response Timeout!","danger");
                    }
                    else{
                        notify(textStatus,"danger");
                    }
                });
            }
        }
    });

    $(document).on("click","#view-doc-modal",function(){
        var id = $(this).data("id");
        clearDocDataModal();
        setDocLogData(id);
    });

    $(document).on("click","#view-receipt-modal",function(){
        var id = $(this).data("id");
        clearReceiptModal();
        setReceiptData(id);
    });

    $(document).on("click",".bt-edit-info",function(){
        var id = $("[name='id']").val();
        clearEditDataModal();
        getPolicyEndorse();
        $("#modal-info").modal();
    });

    $(".btn-clean").click(function(){
        var target = $(this).data("printed");
        $("#"+target).find(".print_icon").removeClass("active");
        $("#"+target).find(".print_enable").addClass("active");
    });

    
});