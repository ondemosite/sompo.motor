$(document).ready(function(){

    $(document).on("change","#select_file",function(){
        readURL(this);
    });

    $(document).on("click","#btn-change",function(){
        $("#select_file").click();
    });

    $(document).on("click","#btn-remove",function(){
        bootbox.confirm({
            message: 'Are you sure to clear profile ?',
            callback: function (state) {
                if(state){
                    var $el = $('#select_file');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    $("#image-preview").attr("src","/images/icon-user-default.png");
                    $("input[name='clear_cover']").val("true");
                }
            }
        });
    });

     // Validate  --------
    $('#form_profile').pxValidate({
        rules: {
            'name': {
                required: true,
                minlength: 3
            },
            'lastname':{
                required: true,
                minlength: 3
            },
            'username':{
                required: true,
                minlength: 5
            },
            email: {
                email: true
            },
            password :{
                required:true,
                minlength: 6
            },
            verify_password :{
                required:true,
                equalTo: "#password"
            },
            privilege :{
                required:true
            }
        },
    });

    $('#form_edit_password').pxValidate({
        rules: {
            'old_password': {
                required: true,
            },
            new_password :{
                required:true,
                minlength: 6
            },
            verify_password :{
                required:true,
                equalTo: "#new_password"
            }
        },
    });
    //------------------------
});