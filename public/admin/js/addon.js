$(function() {

    function clearModal(){
      $("#form_addon").find("input[name='taxi']").val(null);
      $("#form_addon").find("input[name='theft']").val(null);
      $("#form_addon").find("input[name='misc_package_code']").val(0);
      $("#form_addon").find("input[name='sum_insured']").val(0);
      $("#form_addon").find("input[name='net_premium']").val(0);
      $("#form_addon").find("input[name='stamp']").val(0);
      $("#form_addon").find("input[name='vat']").val(0);
      $("#form_addon").find("input[name='gross_premium']").val(0);
    }

    

    $("#open-modal").click(function(){
      $("#form_addon").find("input[name='taxi']").focus();
      clearModal();
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
        processing: true,
        serverSide: true,
        ajax: $("input[name='hd_get_addon_list']").val(),
        columns: [
            { "data": null,"width":"10%","sortable": false},
            { "data": "coverage" },
            { "data": "misc_package_code" },
            { "data": "sum_insured" },
            { "data": "net_premium" },
            { "data": "stamp" },
            { "data": "vat"},
            { "data": "gross_premium"},
            { "data": "action","width": "10%","className": "text-center" },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
          var info = $(this).DataTable().page.info();
          $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
          return nRow;
      }
  });
    
    $('#datatables_wrapper .table-caption').text('Add-On List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_get_data_addon']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  clearModal();
                  $("#form_addon").find("input[name='id']").val(result.id);
                  $("#form_addon").find("input[name='taxi']").val(result.taxi);
                  $("#form_addon").find("input[name='theft']").val(result.theft);
                  $("#form_addon").find("input[name='misc_package_code']").val(result.misc_package_code);
                  $("#form_addon").find("input[name='sum_insured']").val(result.sum_insured);
                  $("#form_addon").find("input[name='net_premium']").val(result.net_premium);
                  $("#form_addon").find("input[name='stamp']").val(result.stamp);
                  $("#form_addon").find("input[name='vat']").val(result.vat);
                  $("#form_addon").find("input[name='gross_premium']").val(result.gross_premium);
                  $("#modal-add").modal('toggle');
                  $("#modal-add").find(".modal-title").find("span").html("Edit Addon");
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_addon']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
    });




  //------------------------

    // Validate Modal --------
    $('#form_addon').pxValidate({
        rules: {
            'taxi':{
                number: true,
            },
            'theft':{
                number: true,
            },
            'misc_package_code':{
                required: true,
            },
            'sum_insured': {
                number: true
            },
            'net_premium': {
                number: true
            },
            'stamp': {
                number: true
            },
            'vat': {
                number: true
            },
            'gross_premium': {
                number: true
            },
        },
    });
    //------------------------

    // Modal Event -----------
    function saveData(){
        if($('#form_addon').valid()){
            $.ajax({
                url: $("input[name='hd_save_addon']").val(),
                data: $("#form_addon").serialize(),
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "success"){
                        table.ajax.reload();
                        notify("Save Data Success.","success");
                    }
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function() {
                $("#modal-add").modal('toggle');
            });
        }
    }

    $("#modal-add #submit_button").click(function(){
        saveData();
    });


  //------------------------


});