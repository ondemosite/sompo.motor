$(function() {


    // -------------- initialize --------------//
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        clearBtn:              true,
        todayHighlight:        true,
        orientation:           'auto left',
        format: 'yyyy-mm-dd',
    });
    
    function getProcessLogs(){
        $.ajax({
            url: $("input[name='hd_get_process_list']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'order_no':$("[name='hd_order_no']").val()
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(result.status == "SUCCESS"){
                var html = '<table width="70%"><tr class="tr-head"><td>ORDER NO</td><td>PROCESS</td><td>STATUS</td><td>ACTION DATE</td></tr>';
                $.each( result.data, function( key, value ) {
                    html+='<tr>';
                    html+='<td>'+result.data[key].order_no+'</td>';
                    html+='<td>'+result.data[key].descriptions+'</td>';
                    if(result.data[key].type == "SUCCESS"){
                        html+='<td class="text-success">'+result.data[key].type+'</td>';
                    }else{
                        html+='<td class="text-danger">'+result.data[key].type+'</td>';
                    }
                    html+='<td>'+result.data[key].created_at+'</td>';
                    html+='</tr>';
                });
                html+='</table>';
                $("#genarateTransactionLogs").html(html);
            }else{
                var html = "<div class=\"text-default\">ไม่พบข้อมูล</div>"
                $("#genarateTransactionLogs").html(html);
            }
        }).fail(function(jqXHR, textStatus){
            alert(textStatus);
        });
    }getProcessLogs();


    function updateOrder(){
        $("#update_order").attr("disabled","disabled");
        $("#state_order").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_update_order']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $("#update_order").removeAttr("disabled");
                $("#state_order").removeClass("active");
            });
        },1000);
    }

    function updatePayment(){
        $("#update_payment").attr("disabled","disabled");
        $("#state_payment").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_update_payment']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val(),
                    'paid_date': $("[name='paid_date']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $("#update_payment").removeAttr("disabled");
                $("#state_payment").removeClass("active");
            });
        },1000);
    }

    function createPolicy(){
        $("#create_policy").attr("disabled","disabled");
        $("#state_policy").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_create_policy']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $("#create_policy").removeAttr("disabled");
                $("#state_policy").removeClass("active");
            });
        },1000);
        
    }

    function createReceipt(){
        $("#create_receipt").attr("disabled","disabled");
        $("#state_receipt").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_create_receipt']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val(),
                    'amount': $("[name='receipt_amount']").val(),
                    'masked_pan': $("[name='masked_pan']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $("#create_receipt").removeAttr("disabled");
                $("#state_receipt").removeClass("active");
            });
        },1000);
    }

    function createPdf(type){
        $(".create_pdf").attr("disabled","disabled");
        $(".state_pdf").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_create_pdf']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val(),
                    'type': type
                },
                type: 'POST',
                dataType: "json",
                timeout:30000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $(".create_pdf").removeAttr("disabled");
                $(".state_pdf").removeClass("active");
            });
        },1000);
    }

    function sendFTP(type){
        
        $(".send_ftp").attr("disabled","disabled");
        $(".state_ftp").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_send_ftp']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val(),
                    'type': type
                },
                type: 'POST',
                dataType: "json",
                timeout:30000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $(".send_ftp").removeAttr("disabled");
                $(".state_ftp").removeClass("active");
            });
        },1000);
    }

    function sendSMS(){
        $("#update_sms").attr("disabled","disabled");
        $("#state_sms").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_send_sms']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:30000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $("#update_sms").removeAttr("disabled");
                $("#state_sms").removeClass("active");
            });
        },1000);
    }

    function sendEmail(){
        $("#update_email").attr("disabled","disabled");
        $("#state_email").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_send_email']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:30000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $("#update_email").removeAttr("disabled");
                $("#state_email").removeClass("active");
            });
        },1000);
    }

    function sendEndorse(){
        $("#update_endorse").attr("disabled","disabled");
        $("#state_endorse").addClass("active");
        setTimeout(function(){
            $.ajax({
                url: $("input[name='hd_send_endorse']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':$("[name='hd_order_no']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:100000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "SUCCESS"){
                        window.location.reload();
                    }
                }else{
                    alert("ERROR:"+result.resp_error);
                }
            }).fail(function(jqXHR, textStatus){
                alert(textStatus);
            }).always(function(){
                $("#update_endorse").removeAttr("disabled");
                $("#state_endorse").removeClass("active");
            });
        },1000);
    }



    // --------------------------------- EVENT ----------------------------------//


    $("#update_order").click(function(){
        updateOrder();
    });

    $("#update_payment").click(function(){
        updatePayment();
    });

    $("#create_policy").click(function(){
        createPolicy();
    });

    $("#create_receipt").click(function(){
        createReceipt();
    });

    $(".create_pdf").click(function(){
        var type = $(this).data("type");
        createPdf(type);
    });

    $(".send_ftp").click(function(){
        var type = $(this).data("type");
        sendFTP(type);
    });

    $("#update_sms").click(function(){
        sendSMS();
    });

    $("#update_email").click(function(){
        sendEmail();
    });

    $("#update_endorse").click(function(){
        sendEndorse();
    });


    



});