$(function() {

    function clearModal(){
      $("#form_compulsory").find("input[name='body_type']").val(null);
      $("#form_compulsory").find("input[name='motor_code_ac']").val(null);
      $("#form_compulsory").find("input[name='net_premium']").val(0);
      $("#form_compulsory").find("input[name='stamp']").val(0);
      $("#form_compulsory").find("input[name='vat']").val(0);
      $("#form_compulsory").find("input[name='gross_premium']").val(0);
    }

    function roundToTwo(num) {    
        return +(Math.round(num + "e+2")  + "e-2");
    }

    function reCalculate(){
        $code_ac = parseFloat($("[name='motor_code_ac']").val()!=""?$("[name='motor_code_ac']").val():0);
        $net_premium = parseFloat($("[name='net_premium']").val()!=""?$("[name='net_premium']").val():0);
        $stamp = Math.ceil($net_premium*(0.4/100));
        $vat = roundToTwo(($net_premium+$stamp)*0.07);
        $gloss_premium = $net_premium+$stamp+$vat;
        $("#form_compulsory").find("input[name='stamp']").val($stamp);
        $("#form_compulsory").find("input[name='vat']").val($vat);
        $("#form_compulsory").find("input[name='gross_premium']").val($gloss_premium);

    }

    $("#open-modal").click(function(){
      $("#form_compulsory").find("input[name='name']").focus();
      clearModal();
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          serverSide: true,
          ajax: $("input[name='hd_get_compulsory_list']").val(),
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "body_type" },
              { "data": "motor_code_ac" },
              { "data": "net_premium" },
              { "data": "stamp" },
              { "data": "vat" },
              { "data": "gross_premium" },
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Compulsory List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_get_data_compulsory']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  clearModal();
                  $("#form_compulsory").find("input[name='id']").val(result.id);
                  $("#form_compulsory").find("input[name='body_type']").val(result.body_type);
                  $("#form_compulsory").find("input[name='motor_code_ac']").val(result.motor_code_ac);
                  $("#form_compulsory").find("input[name='net_premium']").val(result.net_premium);
                  $("#form_compulsory").find("input[name='stamp']").val(result.stamp);
                  $("#form_compulsory").find("input[name='vat']").val(result.vat);
                  $("#form_compulsory").find("input[name='gross_premium']").val(result.gross_premium);
                  $("#modal-add").modal('toggle');
                  $("#modal-add").find(".modal-title").find("span").html("Edit Compulsory");
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_compulsory']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
    });




  //------------------------

    // Validate Modal --------
    $('#form_compulsory').pxValidate({
        rules: {
            'body_type':{
                required: true,
            },
            'motor_code_ac':{
                required: true,
            },
            'net_premium': {
                required: true,
                number: true
            },
        },
    });
    //------------------------

    // Modal Event -----------
    function saveData(){
        if($('#form_compulsory').valid()){
            $.ajax({
                url: $("input[name='hd_save_compulsory']").val(),
                data: $("#form_compulsory").serialize(),
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "success"){
                        table.ajax.reload();
                        notify("Save Data Success.","success");
                    }
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function() {
                $("#modal-add").modal('toggle');
            });
        }
    }

    $("#modal-add #submit_button").click(function(){
        saveData();
    });

    $(".input-recal").keypress(function (e) {
        if(!isNumber(e)){
            return false;
        }
    });

    $(".input-recal").keyup(function (e) {
        reCalculate()
    });
  //------------------------


});