$(function() {
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    function parseHTML(html) {
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>'+ html,'text/html');
        return dom.body.textContent;
    }

    function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
        try {
          decimalCount = Math.abs(decimalCount);
          decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
      
          const negativeSign = amount < 0 ? "-" : "";
      
          let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
          let j = (i.length > 3) ? i.length % 3 : 0;
      
          return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
        } catch (e) {
          console.log(e)
        }
    };

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_list']").val(),
              "data":function(d){
                  return {
                      'search_payment_number':$("[name='search_payment_number']").val(),
                      'search_order_number':$("[name='search_order_number']").val(),
                      'search_policy_number':$("[name='search_policy_number']").val(),
                      'search_from_date':$("[name='search_from_date']").val(),
                      'search_to_date':$("[name='search_to_date']").val(),
                      'search_status':$("[name='search_status']").val()
                  }
              }
          },
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "payment_no" },
              { 
                "data": "order_number",
              },
              { 
                "data": "policy_number",
              },
              { "data": "amount" },
              { 
                  "data": "status",
                  'render':function(data,type,row){
                        if(data=="PROCESSING"){
                            return '<span class="text-default"> '+data+' </span>';
                        }else if(data=="CANCELED"){
                            return '<span class="text-light"> '+data+' </span>';
                        }else if(data=="FAILED"){
                            return '<span class="text-danger"> '+data+' </span>';
                        }else if(data=="PAID"){
                            return '<span class="text-success"> '+data+' </span>';
                        }
                  }
              },
              { "data": "paid_date" },
              { 
                  "data": "created_at",
                  'render':function(data,type,row){
                      var html = "";
                      var obj = $.parseJSON(parseHTML(data));
                      html = obj.date;
                      if(obj.state=="NEW"){
                          html+='&nbsp;<span class="label label-success">New</span>';
                      }
                      return html;
                  }
              },
              { "data": "action","width": "5%","className": "text-center","sortable": false},
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Payment List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_payment_number']" ).val(null);
        $("input[name='search_order_number']").val(null);
        $("input[name='search_policy_number']").val(null);
        $("input[name='search_from_date']").val(null);
        $("input[name='search_to_date']").val(null);
        $("select[name='search_status']").val(null).change();
        table.ajax.reload();
    });

    // Edit
    $(document).on("click",".action-edit",function(){
            var id = $(this).data("id");
            $.ajax({
                url: $("input[name='hd_data']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id
                },
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    var data = result;
                    console.log(data);
                    if(data.payment_no){
                        $("#payment_no").html(data.payment_no);
                    }
                    if(data.order_number){
                        $("#field_rc_order").html('<a target="_blank" href="'+$("input[name='hd_link_order']").val()+"/"+data.order_id+'">'+data.order_number+'</a>');
                    }
                    if(data.policy_number){
                        $("#field_rc_policy").html('<a target="_blank" href="'+$("input[name='hd_link_policy']").val()+"/"+data.policy_id+'">'+data.policy_number+'</a>');
                    }
                    if(data.merchant_id){
                        $("#field_rc_mc").html(data.merchant_id);
                    }
                    if(data.currency_code){
                        $("#field_rc_cc").html(data.currency_code);
                    }
                    if(data.description){
                        $("#field_rc_dc").html(data.description);
                    }
                    if(data.amount){
                        $("#field_rc_am").html(formatMoney(data.amount));
                    }
                    if(data.status){
                        $("#field_rc_st").html(data.status);
                    }
                    if(data.status_detail){
                        $("#field_rc_stt").html(data.status_detail);
                    }
                    if(data.paid_date){
                        $("#field_rc_pd").html(data.paid_date);
                    }
                    if(data.created_at){
                        $("#created_date").html(data.created_at);
                    }
                    if(data.updated_at){
                        $("#field_rc_up").html(data.updated_at);
                    }
                    $("#modal-payment").modal();
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            });
    });

    


});