$(function() {
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    function parseHTML(html) {
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>'+ html,'text/html');
        return dom.body.textContent;
    }

    // DataTable -------------
    var table = $('#datatables').DataTable({
            processing: true,
            serverSide: true,
            stateSave: false,
            pageLength: 15,
            ajax :{
                "url": $("input[name='hd_order_list']").val(),
                "data":function(d){
                    d._token = $('meta[name="csrf-token"]').attr('content');
                    d.search_order_number = $("[name='search_order_number']").val();
                    d.search_driver_name = $("[name='search_driver_name']").val();
                    d.search_insurance_ft_si = $("[name='search_insurance_ft_si']").val();
                    d.search_car_licence = $("[name='search_car_licence']").val();
                    d.search_from_date = $("[name='search_from_date']").val();
                    d.search_to_date = $("[name='search_to_date']").val();
                    d.search_status = $("[name='search_status']").val();
                    return d;
                }
            },
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "order_number" },
              { "data": "name" },
              { 
                "data": "brand",
                'render':function(data,type,row){
                    var obj = $.parseJSON(parseHTML(data));
                    return obj.brand+" "+obj.model+" "+obj.year+"<br/>"+obj.car_licence;
                }
              },
              { "data": "insurance_ft_si" },
              { 
                  "data": "is_addon",
                  'render':function(data,type,row){
                      return '<span class="text-success">'+data+'</span>';
                  }
              },
              { 
                  "data": "is_compulsory",
                  'render':function(data,type,row){
                      if(data=="yes"){
                          return '<span class="text-success">ซื้อประกันภัย </span>';
                      }else{
                          return '<span class="text-light">ไม่ระบุ </span>'
                      }
                  }
              },
              { "data": "payment_result" },
              { 
                  "data": "status",
                  'render':function(data,type,row){
                        if(data=="WAITING"){
                            return '<span class="text-default"> รอชำระเงิน </span>';
                        }else if(data=="EXPIRE"){
                            return '<span class="text-muted"> หมดอายุ </span>';
                        }else if(data=="CANCEL"){
                            return '<span class="text-light"> ยกเลิกการชำระเงิน </span>';
                        }else if(data=="INACTIVE"){
                            return '<span class="text-danger"> ปิด </span>';
                        }else if(data=="PAID"){
                            return '<span class="text-success"> ชำระเงินแล้ว </span>';
                        }else if(data=="FAILED"){
                            return '<span class="text-danger"> ล้มเหลว </span>';
                        }
                  }
              },
              { 
                  "data": "created_at",
                  'render':function(data,type,row){
                      var html = "";
                      var obj = $.parseJSON(parseHTML(data));
                      html = obj.date;
                      if(obj.state=="NEW"){
                          html+='&nbsp;<span class="label label-success">New</span>';
                      }
                      return html;
                  }
              },
              { "data": "action","width": "5%","className": "text-center","sortable": false},
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Order List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_order_number']" ).val(null);
        $("input[name='search_driver_name']").val(null);
        $("input[name='search_insurance_ft_si']").val(null);
        $("input[name='search_car_licence']").val(null);
        $("input[name='search_from_date']").val(null);
        $("input[name='search_to_date']").val(null);
        $("select[name='search_status']").val(null);
        table.ajax.reload();
    });

    $(document).on("click",".force-create",function(){
        var handle = $(this);
        var order_id = handle.data("id");
        handle.addClass("btn-loading");
        $.ajax({
            url: $("input[name='hd_order_create']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'order_id':order_id
            },
            type: 'POST',
            dataType: "json",
        }).done(function(result){
            if(!result.resp_error){
                notify("create policy successfuly","success");
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        }).always(function(){
            handle.removeClass("btn-loading");
        });
    });


});