$(function() {

    function clearModal(){
      $("#form_model").find("input[name='id']").val(null);
      $("#form_model").find("input[name='name']").val(null);
      $("#form_model").find("select[name='brand_id']").val(null);
      $("#modal-add").find(".modal-title").find("span").html("Add Vehicle Model");
    }

    $("#open-modal").click(function(){
      $("#form_model").find("input[name='name']").focus();
      clearModal();
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
            "url": $("input[name='hd_get_vehicle_model_list']").val(),
            "data":function(d){
                return {
                    'search_name':$("[name='search_model_name']").val(),
                    'search_brand':$("[name='search_brand']").val(),
                }
            }
        },
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "name" },
              { "data": "brand_name" },
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Vehicle Model List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_get_data_vehicle_model']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  console.log(result);
                  clearModal();
                  $("#form_model").find("input[name='id']").val(result.id);
                  $("#form_model").find("input[name='name']").val(result.name);
                  $("#form_model").find("select[name='brand_id']").val(result.brand_id).change();
                  $("#modal-add").modal('toggle');
                  $("#modal-add").find(".modal-title").find("span").html("Edit Vehicle Model");
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_model']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          console.log(result);
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });


    //------------------------

    // Validate Modal --------
    $('#form_model').pxValidate({
        rules: {
            'name': {
                required: true,
                minlength: 3
            },
            'brand_id':{
                required:true,
            }
        },
    });
    //------------------------

    // Modal Event -----------
    function saveData(){
        if($('#form_model').valid()){
            $.ajax({
                url: $("input[name='hd_save_vehicle_model']").val(),
                data: $("#form_model").serialize(),
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "success"){
                        table.ajax.reload();
                        notify("Save Data Success.","success");
                    }
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function() {
                $("#modal-add").modal('toggle');
            });
        }
    }

    $("#form_brand").find("input[name='name']").keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            saveData();
            event.preventDefault();
        }
        
    });

    $("#modal-add #submit_button").click(function(){
        saveData();
    });

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_model_name']" ).val(null);
        $("select[name='search_brand']").val(null);
        table.ajax.reload();
    });
  //------------------------


});