$(document).ready(function(){
    $('#form_login').validate({
        rules: {
            'username': {
                required: true,
            },
            password: {
                required: true,
                minlength: 6
            },
        },
    });
})