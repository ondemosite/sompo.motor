function clearMenuForm(){
    $('[name="menu_id"]').val("");
    $('select[name="parent_id"]').val(0).change();
    $('[name="name"]').val("");
    $('[name="name_th"]').val("");
    $('[name="path"]').val("");
    $('[name="icon"]').val("");
    $('[name="orders"]').val("");
    $('[name="isshow"]').prop('checked', true);
}

function genMenu(){
    $.ajax({
        url: $("input[name='hd_get_menu']").val(),
        type: 'POST',
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content'),
        },
        dataType: "json",
        timeout:10000
    }).done(function(result){
        if(!result.resp_error){ 
            var html = "";
            $.each( result, function( key, value ) {   
                if(result[key].parent_id=="0"){
                    var hidden_class = "";
                    if(result[key].isshow==0) hidden_class = "bg-white darken"; 
                    html+='<li class="'+hidden_class+'" data-id="'+result[key].id+'"><span><i class="fa '+result[key].icon+'"></i> '+result[key].name+'<a class="pull-right delete-menu" data-id="'+result[key].id+'" data-name="'+result[key].name+'"><i class="fa fa-trash"></i></a><a class="pull-right edit-menu" data-id="'+result[key].id+'"><i class="fa fa-pencil"></i></a></span>';
                    html+='<ol class="menu">';
                    if(result[key].sub_menu.length>0){
                        $.each( result[key].sub_menu, function( key2, value2 ) {   
                            html+='<li data-id="'+result[key].sub_menu[key2].id+'"><span><i class=""></i> '+result[key].sub_menu[key2].name+'<a class="pull-right delete-menu" data-id="'+result[key].sub_menu[key2].id+'" data-name="'+result[key].sub_menu[key2].name+'"><i class="fa fa-trash"></i></a><a class="pull-right edit-menu" data-id="'+result[key].sub_menu[key2].id+'"><i class="fa fa-pencil"></i></a></span>';
                            html+='<ol class="menu">';
                            if(result[key].sub_menu[key2].sub_menu.length>0){ //Level3
                                $.each(result[key].sub_menu[key2].sub_menu, function( key3, value3 ) {  
                                    html+='<li data-id="'+result[key].sub_menu[key2].sub_menu[key3].id+'"><span><i class=""></i> '+result[key].sub_menu[key2].sub_menu[key3].name+'<a class="pull-right delete-menu" data-id="'+result[key].sub_menu[key2].sub_menu[key3].id+'" data-name="'+result[key].sub_menu[key2].sub_menu[key3].name+'"><i class="fa fa-trash"></i></a><a class="pull-right edit-menu" data-id="'+result[key].sub_menu[key2].sub_menu[key3].id+'"><i class="fa fa-pencil"></i></a></span></li>';
                                });
                            }
                            html+='</ol>';
                            html+= '</li>';
                        });
                    }
                    html+='</ol>';
                    html+='</li>';
                }
            });
            $('ol.menu').html(html); 
        }else{
            notify(result.resp_error,"danger");
        }
    }).fail(function(jqXHR, textStatus){
        if(textStatus == 'timeout'){
            notify("Server Response Timeout!","danger");
        }
        else{
            notify(textStatus,"danger");
        }
    });
}

function submit_menu(){
    if($('#menu-form').valid()){
        $.ajax({
            url: $("input[name='hd_save_menu']").val(),
            type: 'POST',
            data: $("#menu-form").serialize(),
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){ 
                if(result.status == "success"){
                    notify("Save Data Success.","success");
                    clearMenuForm();
                    getParentMenu();
                    genMenu();
                    gen_permission_menu();
                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        }).always(function() {
            $("#modal-menu").modal('toggle');
        });
    }
}

function getParentMenu(){
    $.ajax({
        url: $("input[name='hd_get_parent']").val(),
        type: 'POST',
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content'),
        },
        dataType: "json",
        timeout:10000
    }).done(function(result){
        if(!result.resp_error){ 
            var html = ""
            var select = '<option selected="selected" value="0">root</option>';
            $.each( result, function( key, value ) {   
                select +='<option value="'+result[key].id+'">'+result[key].name+'</option>';
            });
            $("select[name='parent_id']").html(select);   
        }else{
            notify(result.resp_error,"danger");
        }
    }).fail(function(jqXHR, textStatus){
        if(textStatus == 'timeout'){
            notify("Server Response Timeout!","danger");
        }
        else{
            notify(textStatus,"danger");
        }
    });
}

function getMenuData(id){
    $.ajax({
        url: $("input[name='hd_get_menu_data']").val(),
        type: 'POST',
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            'id':id
        },
        dataType: "json",
        timeout:10000
    }).done(function(result){
        if(!result.resp_error){ 
            $('[name="menu_id"]').val(result.id);
            $('select[name="parent_id"]').val(result.parent_id).change();
            $('[name="name"]').val(result.name);
            $('[name="name_th"]').val(result.name_th);
            $('[name="path"]').val(result.path);
            $('[name="icon"]').val(result.icon);
            $('[name="orders"]').val(result.orders);
            if(result.isshow==1){
                $('[name="isshow"]').prop('checked', true);
            }else{
                $('[name="isshow"]').prop('checked', false);
            }
            $('#modal-menu').modal();
        }else{
            notify(result.resp_error,"danger");
        }
    }).fail(function(jqXHR, textStatus){
        if(textStatus == 'timeout'){
            notify("Server Response Timeout!","danger");
        }
        else{
            notify(textStatus,"danger");
        }
    });
}

function gen_permission_menu(id){
    if(!id){
       id = $("[name='select_privilege']").val();
    }
    if(id){
        $.ajax({
            url: $("input[name='hd_get_pm_menu']").val(),
            type: 'POST',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'privilege_id':id
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            console.log(result);
            if(!result.resp_error){ 
                html = "Not Found";
                if(result.length>0){
                    var html = '<tr><th class="col-md-4">Menu</th><th class="col-md-2 info text-center">View</th>';
                    html+= '<th class="col-md-2 success text-center">Add</th><th class="col-md-2 warning text-center">Edit</th>';
                    html+= '<th class="col-md-2 danger text-center">Delete</th></tr>';
                    $.each( result, function( key, value ) {   
                        var hidden_class = "";
                        if(result[key].isshow==0) hidden_class = "bg-white darken";
                        html+= '<tr><td class="p-a-05"><div class="pem-menu '+hidden_class+'"><i class="fa '+result[key].icon+'"></i> '+result[key].name+'</div></td>';
                        html+= '<td class="text-center p-a-05"><label for="switcher-key-'+key+'-view" class="switcher switcher-primary"><input type="checkbox" id="switcher-key-'+key+'-view" class="pm_checkbox" name="view" value="'+result[key].menu_id+'" '+(result[key].view==1?'checked':'')+' /><div class="switcher-indicator switcher-center"><div class="switcher-yes">YES</div><div class="switcher-no">NO</div></div></label></td>';
                        html+= '<td class="text-center p-a-05"><label for="switcher-key-'+key+'-add" class="switcher switcher-primary"><input type="checkbox" id="switcher-key-'+key+'-add" class="pm_checkbox" name="add" value="'+result[key].menu_id+'" '+(result[key].add==1?'checked':'')+' /><div class="switcher-indicator switcher-center"><div class="switcher-yes">YES</div><div class="switcher-no">NO</div></div></label></td>';
                        html+= '<td class="text-center p-a-05"><label for="switcher-key-'+key+'-edit" class="switcher switcher-primary"><input type="checkbox" id="switcher-key-'+key+'-edit" class="pm_checkbox" name="edit" value="'+result[key].menu_id+'" '+(result[key].edit==1?'checked':'')+' /><div class="switcher-indicator switcher-center"><div class="switcher-yes">YES</div><div class="switcher-no">NO</div></div></label></td>';
                        html+= '<td class="text-center p-a-05"><label for="switcher-key-'+key+'-delete" class="switcher switcher-primary"><input type="checkbox" id="switcher-key-'+key+'-delete" class="pm_checkbox" name="delete" value="'+result[key].menu_id+'" '+(result[key].delete==1?'checked':'')+' /><div class="switcher-indicator switcher-center"><div class="switcher-yes">YES</div><div class="switcher-no">NO</div></div></label></td>';
                    });
                    $(".table-render").html(html);

                    

                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout') notify("Server Response Timeout!","danger");
            else notify(textStatus,"danger");
        });
    }
}

function getPrivilege(){
    $.ajax({
        url: $("input[name='hd_get_privilege']").val(),
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content'),
        },
        type: 'POST',
        dataType: "json",
        timeout:10000
    }).done(function(result){
        if(!result.resp_error){
            var html = ""
            var select = '<option selected="selected" disabled="disabled" hidden="hidden" value="">Select Privilege</option>';
            $.each( result, function( key, value ) {   
                select +='<option value="'+result[key].id+'">'+result[key].name+'</option>';
            });
            $("select[name='select_privilege']").html(select);   
        }else{
            notify(result.resp_error,"danger");
        }
    }).fail(function(jqXHR, textStatus){
        if(textStatus == 'timeout') notify("Server Response Timeout!","danger");
        else notify(textStatus,"danger");
    });
}

function updateMenu(data){
    $.ajax({
        url: $("input[name='hd_update_order']").val(),
        type: 'POST',
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            'orders':data
        },
        dataType: "json",
        timeout:10000
    }).done(function(result){
        if(!result.resp_error){ 
            if(result.status == "success"){
                genMenu();
                gen_permission_menu();
                notify("Update Data Success.","success");
            }
        }else{
            notify(result.resp_error,"danger");
        }
    }).fail(function(jqXHR, textStatus){
        if(textStatus == 'timeout'){
            notify("Server Response Timeout!","danger");
        }
        else{
            notify(textStatus,"danger");
        }
    });
}

function delete_menu(id){
    $.ajax({
        url: $("input[name='hd_delete_menu']").val(),
        type: 'DELETE',
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            'id':id
        },
        dataType: "json",
        timeout:10000
    }).done(function(result){
        if(!result.resp_error){ 
            if(result.status == "success"){
                notify("Delete Success.","success");
                genMenu();
                getParentMenu();
            }
        }else{
            notify(result.resp_error,"danger");
        }
    }).fail(function(jqXHR, textStatus){
        if(textStatus == 'timeout') notify("Server Response Timeout!","danger");
        else notify(textStatus,"danger");
    });
}




$(document).ready(function(){
    getParentMenu();
    genMenu();
    getPrivilege();

    $("#menu-form").validate({
        rules: {
            name :{
                required: true,
                minlength: 3
            },
            orders:{
                required: true,
                number  : true
            }
        } 
    });

    $("#modal-menu #submit_button").click(function(){
        submit_menu();
    });

    $("#open-modal").click(function(){
        clearMenuForm();
    });

    $(document).on("click",".edit-menu",function(){
        clearMenuForm();
        var id = $(this).attr("data-id");
        getMenuData(id);    
    });

    $(document).on("click",".delete-menu",function(){
        var name = $(this).attr("data-name");
        var id = $(this).attr("data-id");
        bootbox.confirm({
            message: 'Are you sure to delete <b>'+name+'</b> ?',
            callback: function (state) {
                if(state){
                    delete_menu(id);
                }
            }
        });
    });

    $(document).on("change",".pm_checkbox",function(event){
        var handle = $(this);
        var name = $(this).attr("name");
        var menu_id = $(this).val();
        var is_check = $(this).is(':checked');
        var privilege_id = $("#select_privilege").val();
        if(privilege_id!=1){
            $.ajax({
                url: $("input[name='hd_set_pm_menu']").val(),
                type: 'POST',
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'name':name,
                    'menu_id':menu_id,
                    'is_check':is_check,
                    'privilege_id':privilege_id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(result.resp_error){ 
                    handle.prop('checked', !is_check);
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout') notify("Server Response Timeout!","danger");
                else notify(textStatus,"danger");
                handle.prop('checked', !is_check);
            });
        }else{
            handle.prop('checked', true);
            notify("Permission Denied","danger");
        }
    });

    $("[name='select_privilege']").change(function(){
        var id = $(this).val();
        gen_permission_menu(id);
    });


    if (!isMobile.Android() && !isMobile.iOS()){
        var oldContainer;
        var thisSortAble;
        thisSortAble = $("ol.menu").sortable({
            group: 'nested',
            afterMove: function (placeholder, container) {
            if(oldContainer != container){
            if(oldContainer)
                oldContainer.el.removeClass("active");
                container.el.addClass("active");
                oldContainer = container;
            }
        },
        onDrop: function ($item, container, _super) {
            container.el.removeClass("active");
            _super($item, container);
        }
        });

        $(".btn-update-menu").click(function(){
            var data = thisSortAble.sortable("serialize").get();
            updateMenu(data)
        }); 
        
        $(document).on("mouseenter",".edit-menu,.delete-menu",function(){
            $("ol.menu").sortable("disable");
        });
        $(document).on("mouseleave",".edit-menu,.delete-menu",function(){
            $("ol.menu").sortable("enable");
        });
    }else{
        $(".btn-update-menu").hide();
    }

    

});