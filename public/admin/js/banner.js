$(function() {

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax: $("input[name='hd_get_banner_list']").val(),
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { 
                  "name": "cover_en",
                  "data": "cover_en",
                  "width":"20%",
                  'render':function(data,type,row){
                        if(data){
                            return '<div><img src="'+$("[name='hd_asset_url']").val()+data+'" width="300px"/></div>';
                        }else{
                            return '<div><img src="'+$("[name='hd_asset_url']").val()+'/images/admin/background/preview-banner.jpg" width="300px"/></div>';
                        }
                        
                  }
              },
              { "data":"title","width":"20%"},
              { "data":"description","width":"20%"},
              { "data":"link","width":"20%"},
              { "data":"view","width":"5%"},
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Banner List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete_banner']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });


  //------------------------


});