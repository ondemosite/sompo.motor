$(function() {

    function clearModal(){
        $("#field_order_number").html('<p class="text-muted">-</p>');
        $("#field_amount").html('<p class="text-muted">-</p>');
        $("#field_ref").html('<p class="text-muted">-</p>');
        $("#field_approve").html('<p class="text-muted">-</p>');
        $("#field_eci").html('<p class="text-muted">-</p>');
        $("#field_channel").html('<p class="text-muted">-</p>');
        $("#field_status").html('<p class="text-muted">-</p>');
        $("#field_response").html('<p class="text-muted">-</p>');
        $("#field_response_desc").html('<p class="text-muted">-</p>');
        $("#field_credit_number").html('<p class="text-muted">-</p>');
        $("#field_scheme").html('<p class="text-muted">-</p>');
        $("#field_browser").html('<p class="text-muted">-</p>');
        $("#field_request_at").html('<p class="text-muted">-</p>');
        $("#field_response_at").html('<p class="text-muted">-</p>');
    }

    function setModalData(id){
        $.ajax({
            url: $("[name='hd_get_data']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':id,
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            console.log(result);
            if(!result.resp_error){
                if(!result.fail){
                    var data = result.data;
                    
                    if(data.order_number){
                        $("#field_order_number").html(data.order_number);
                    }
                    if(data.amount){
                        $("#field_amount").html(data.amount);
                    }
                    if(data.transaction_ref){
                        $("#field_ref").html(data.transaction_ref);
                    }
                    if(data.approval_code){
                        $("#field_approve").html(data.approval_code);
                    }
                    if(data.eci){
                        $("#field_eci").html(data.eci);
                    }
                    if(data.payment_channel){
                        $("#field_channel").html(data.payment_channel);
                    }
                    if(data.payment_status){
                        $("#field_status").html(data.payment_status);
                    }
                    if(data.channel_response_code){
                        $("#field_response").html(data.channel_response_code);
                    }
                    if(data.channel_response_desc){
                        $("#field_response_desc").html(data.channel_response_desc);
                    }
                    if(data.credit_number){
                        $("#field_credit_number").html(data.credit_number);
                    }
                    if(data.payment_scheme){
                        $("#field_scheme").html(data.payment_scheme);
                    }
                    if(data.browser_info){
                        $("#field_browser").html(data.browser_info);
                    }
                    if(data.request_at){
                        $("#field_request_at").html(data.request_at);
                    }
                    if(data.response_at){
                        $("#field_response_at").html(data.response_at);
                    }
                }else if(result.fail){
                    notify(result.fail,"danger");
                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });
    }

    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_get_list']").val(),
              "data":function(d){
                  return {
                      'search_order_number':$("[name='search_order_number']").val(),
                      'search_transaction_ref':$("[name='search_transaction_ref']").val(),
                      'search_status':$("[name='search_status']").val(),
                      'search_credit_number':$("[name='search_credit_number']").val(),
                      'search_from_date':$("[name='search_from_date']").val(),
                      'search_to_date':$("[name='search_to_date']").val(),
                  }
              }
          },
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "order_number" },
              { "data": "amount" },
              { "data": "transaction_ref" },
              { "data": "payment_channel" },
              { "data": "payment_status" },
              { "data": "masked_pan" },
              { "data": "payment_scheme" },
              { "data": "request_at" },
              { "data": "response_at" },
              { "data": "action","width": "5%","className": "text-center","sortable": false },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Garage Type List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    $("#search").click(function(){
        table.ajax.reload();
    });

    


    

    $("#clear").click(function(){
        $("input[name='search_order_number']" ).val(null);
        $("input[name='search_transaction_ref']").val(null);
        $("select[name='search_status']").val(null);
        $("input[name='search_credit_number']").val(null);
        $("input[name='search_from_date']").val(null);
        $("input[name='search_to_date']").val(null);
        table.ajax.reload();
    });

    $(document).on("click",".view-modal",function(){
        var id = $(this).data("id");
        clearModal();
        setModalData(id);
    });

});