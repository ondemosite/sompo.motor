$(function() {
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    function parseHTML(html) {
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>'+ html,'text/html');
        return dom.body.textContent;
    }
    
    function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
        try {
          decimalCount = Math.abs(decimalCount);
          decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
      
          const negativeSign = amount < 0 ? "-" : "";
      
          let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
          let j = (i.length > 3) ? i.length % 3 : 0;
      
          return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
        } catch (e) {
          console.log(e)
        }
    };


    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_list']").val(),
              "data":function(d){
                  return {
                      'search_receipt_number':$("[name='search_receipt_number']").val(),
                      'search_payment_number':$("[name='search_payment_number']").val(),
                      'search_credit_number':$("[name='search_credit_number']").val(),
                      'search_from_date':$("[name='search_from_date']").val(),
                      'search_to_date':$("[name='search_to_date']").val()
                  }
              }
          },
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "receipt_number" },
              { 
                "data": "payment_number",
              },
              { 
                "data": "amount",
              },
              { "data": "credit_number" },
              { "data": "created_at"},
              { "data": "action","width": "5%","className": "text-center","sortable": false},
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Receipt List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_receipt_number']" ).val(null);
        $("input[name='search_payment_number']").val(null);
        $("input[name='search_credit_number']").val(null);
        $("input[name='search_from_date']").val(null);
        $("input[name='search_to_date']").val(null);
        table.ajax.reload();
    });

    // Edit
    $(document).on("click",".action-edit",function(){
            var id = $(this).data("id");
            $.ajax({
                url: $("input[name='hd_data']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id
                },
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    var data = result;
                    console.log(data);
                    if(data.receipt_number){
                        $("#receipt_no").html(data.receipt_number);
                    }
                    if(data.order_number){
                        $("#field_rc_order").html('<a target="_blank" href="'+$("input[name='hd_link_order']").val()+"/"+data.order_id+'">'+data.order_number+'</a>');
                    }
                    if(data.payment_number){
                        $("#field_rc_payment").html(data.payment_number);
                    }
                    if(data.policy_number){
                        $("#field_rc_policy").html('<a target="_blank" href="'+$("input[name='hd_link_policy']").val()+"/"+data.policy_id+'">'+data.policy_number+'</a>');
                    }
                    if(data.amount){
                        $("#field_rc_am").html(formatMoney(data.amount));
                    }
                    if(data.channel){
                        $("#field_rc_cn").html(data.channel);
                    }
                    if(data.credit_number){
                        $("#field_rc_cre").html(data.credit_number);
                    }
                    if(data.scheme){
                        $("#field_rc_ps").html(data.scheme);
                    }
                    if(data.created_at){
                        $("#created_date").html(data.created_at);
                    }
                    $("#modal-receipt").modal();
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            });
    });

    


});