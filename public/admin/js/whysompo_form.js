$(document).ready(function(){
    $('.summernote').summernote({
        height: 200,
        toolbar: [
          ['parastyle', ['style']],
          ['fontstyle', ['fontname', 'fontsize']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['insert', ['picture', 'link', 'video', 'table', 'hr']],
          ['history', ['undo', 'redo']],
          ['misc', ['codeview']],
          ['help', ['help']]
        ],
    });

    $(document).on("change","#select_file_th",function(){
        readURL(this,$("#image-preview-th"));
        $("input[name='clear_cover_th']").val("false");
    });

    $(document).on("change","#select_file_en",function(){
        readURL(this,$("#image-preview-en"));
        $("input[name='clear_cover_en']").val("false");
    });

    $(document).on("click","#btn-change-th",function(){
        $("#select_file_th").click();
    });

    $(document).on("click","#btn-change-en",function(){
        $("#select_file_en").click();
    });

    $(document).on("click","#btn-remove-th",function(){
        bootbox.confirm({
            message: 'Are you sure to clear Image ?',
            callback: function (state) {
                if(state){
                    var $el = $('#select_file_th');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    $("#image-preview-th").attr("src","/images/admin/background/preview-whysompo.jpg");
                    $("input[name='clear_cover_th']").val("true");
                }
            }
        });
    });

    $(document).on("click","#btn-remove-en",function(){
        bootbox.confirm({
            message: 'Are you sure to clear Image ?',
            callback: function (state) {
                if(state){
                    var $el = $('#select_file_en');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    $("#image-preview-en").attr("src","/images/admin/background/preview-whysompo.jpg");
                    $("input[name='clear_cover_en']").val("true");
                }
            }
        });
    });

    // Validate  --------
    $('#form_article').pxValidate({
        rules: {
            'title_th': {
                required: true,
            },
            'title_en': {
                required: true,
            },
            'order':{
                required: true,
            },
        },
    });

});