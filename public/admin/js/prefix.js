$(function() {

    function clearModal(){
      $("#form_action").find("input[name='id']").val(null);
      $("#form_action").find("input[name='name']").val(null);
      $("#form_action").find("input[name='name_en']").val(null);
      $("#form_action").find("input[name='status']").prop('checked', false);
      $('input:radio[name="gender"][value="NONE"]').prop('checked', true);
      $("#modal-add").find(".modal-title").find("span").html("Add Prefix");
    }

    $("#open-modal").click(function(){
      $("#form_action").find("input[name='name']").focus();
      clearModal();
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          serverSide: true,
          ajax: $("input[name='hd_list']").val(),
          columns: [
              { "data": null,"width":"10%","sortable": false},
              { "data": "name","width":"20%"},
              { "data": "name_en","width":"20%"},
              { 
                "name": "is_default",
                "data": "is_default",
                'render':function(data,type,row){
                    if(data=='YES'){
                        return '<span class="label label-success">YES</span>';
                    }else{
                        return '<span class="label label-danger">NO</span>';
                    }
                }
              },
              { 
                "name": "status",
                "data": "status",
                'render':function(data,type,row){
                    if(data=='INACTIVE'){
                        return '<span class="label label-danger">INACTIVE</span>';
                    }else{
                        return '<span class="label label-success">ACTIVE</span>';
                    }
                }
              },
              { "data": "updated_at","width":"10%"},
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Prefix List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_data']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  clearModal();
                  $("#form_action").find("input[name='id']").val(result.id);
                  $("#form_action").find("input[name='name']").val(result.name);
                  $("#form_action").find("input[name='name_en']").val(result.name_en);
                  if(result.status=='ACTIVE'){
                    $("#form_action").find("input[name='status']").prop('checked', true);
                  }else{
                    $("#form_action").find("input[name='status']").prop('checked', false);
                  }
                  if(result.is_default=='YES'){
                    $("#form_action").find("input[name='is_default']").prop('checked', true);
                  }else{
                    $("#form_action").find("input[name='is_default']").prop('checked', false);
                  }
                  $('input:radio[name="gender"][value="'+result.gender+'"]').prop('checked', true);

                  $("#modal-add").modal('toggle');
                  $("#modal-add").find(".modal-title").find("span").html("Edit Prefix");
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

    //Edit Status  
    $(document).on("change",".edit-status",function(){
        var id = $(this).val();
        var type = 0;
        if($(this).is(':checked')) type = 1;
        $.ajax({
            url: $("input[name='hd_status']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'type':type,
                'id':id
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            notify(result.resp_error,"danger");
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
        
  });


  //------------------------

  // Validate Modal --------
  $('#form_action').pxValidate({
      rules: {
        'name': {
            required: true,
        },
        'name_en':{
            required: true,
        }
      },
  });
  //------------------------

  // Modal Event -----------
  function saveData(){
      if($('#form_action').valid()){
          $.ajax({
              url: $("input[name='hd_save']").val(),
              data: $("#form_action").serialize(),
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  if(result.status == "success"){
                      table.ajax.reload();
                      notify("Save Data Success.","success");
                  }
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          }).always(function() {
              $("#modal-add").modal('toggle');
          });
      }
  }

  $("#modal-add #submit_button").click(function(){
      saveData();
  });
  //------------------------


});