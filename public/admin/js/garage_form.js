$(document).ready(function(){
    var garage_id = $("[name='garage_id']").val();
    var localData = null;
    if(garage_id!=null){
        installProvince(garage_id);
    }

    function installProvince(garage_id){
        $.ajax({
            url: $("input[name='hd_get_data_garage']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':garage_id
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            console.log(result);
            if(!result.resp_error){
                if(result.province){
                    $("select[name='province']" ).val(result.province).change();
                    localData = result;
                    $("select[name='district']" ).val(result.district).change();
                    $("select[name='sub_district']" ).val(result.sub_district);
                }
            }
        });
    }

    $("select[name='province']" ).change(function(){
        var id = $(this).val();
        localData = null;
        if(id!=null && id!=""){
            $.ajax({
                url: $("[name='hd_get_data_district']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value>District</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='district']").html(select);
                    if(localData.district){
                        $("select[name='district']").val(localData.district).change();
                    }
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }else{
            select = '<option selected="selected" value>District</option>';
            $("select[name='district']").html(select);
            $("select[name='district']").val(null).change();
        }
    });

    $("select[name='district']" ).change(function(){
        var id = $(this).val();
        if(id!=null && id!=""){
            $.ajax({
                url: $("[name='hd_get_data_subdistrict']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'district_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value>Sub District</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='sub_district']").html(select);
                    if(localData.sub_district){
                        $("select[name='sub_district']").val(localData.sub_district).change();
                    }
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }else{
            select = '<option selected="selected" value>Sub District</option>';
            $("select[name='sub_district']").html(select);
            $("select[name='sub_district']").val(null).change();
        }
        
    });

    // Validate  --------
    $('#form_edit_garage').pxValidate({
        rules: {
            'type_class': {
                required: true,
            },
            'type':{
                required: true,
            },
            'name':{
                required: true,
            },
            'province':{
                required: true,
            },
            'district':{
                required: true,
            },
            'sub_district':{
                required: true,
            },
            'lat':{
                required: true,
                number: true
            },
            'long':{
                required: true,
                number: true
            }
        },
    });

});