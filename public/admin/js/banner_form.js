$(document).ready(function(){
   
    $(document).on("change",".select_file",function(){
        var target = $(this).attr("name");
        readURL(this,$(".image-preview[data-belong='"+target+"']"));
        $("input[name='clear_"+target+"']").val("false");
    });

    $(document).on("click",".btn-change",function(){
        var target = $(this).data("target");
        $("input[name='"+target+"']").click();
    });

    $(document).on("click",".btn-remove",function(){
        var handle = $(this);
        var target = $(this).data("target");
        bootbox.confirm({
            message: 'Are you sure to clear Image ?',
            callback: function (state) {
                if(state){
                    var $el = $("input[name='"+target+"']");
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    handle.parent().siblings(".box-image-preview").find(".image-preview").attr("src","/images/admin/background/preview-banner_"+target+".jpg");
                    $("input[name='clear_"+target+"']").val("true");
                }
            }
        });
    });

    $(".sync_type").change(function(){
        var type = $(".sync_type:checked").val();
        $(".box-append").hide();
        $(".box-"+type).slideDown("fast");
    });

    // Validate  --------
    $('#form_banner').pxValidate({
        rules: {
            'title': {
                required: true,
            },
        },
    });

});