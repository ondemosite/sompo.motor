$(document).ready(function(){
    var vehicle_id = $("[name='vehicle_id']").val();
    var localData = null;
    if(vehicle_id!=null){
        installBrand(vehicle_id);
    }
    $('.input-money').simpleMoneyFormat();

    function installBrand(vehicle_id){
        $.ajax({
            url: $("input[name='hd_get_data_vehicle']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'id':vehicle_id
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                if(result.brand_id){
                    console.log(result);
                    localData = result;
                    $("select[name='brand_id']" ).val(result.brand_id).change();
                    //$("select[name='model_id']" ).val(result.model_id).change();
                }
            }
        });
    }

    $("select[name='brand_id']" ).change(function(){
        var id = $(this).val();
        if(id!=null && id!=""){
            $.ajax({
                url: $("[name='hd_get_select_model']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'brand_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value>Vehicle Model</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='model_id']").html(select);
                    if(localData!=null){
                        if(localData.model_id){
                            $("select[name='model_id']").val(localData.model_id).change();
                        }
                        localData = null;
                    }
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }else{
            select = '<option selected="selected" value>Vehicle Model</option>';
            $("select[name='model_id']").html(select);
            $("select[name='model_id']").val(null).change();
        }
    });

    $("#form_vehicle").on("submit",function(e){
       
        if($("#form_vehicle").valid()){
            $('.input-money').each(function(index){
                var val = $(this).val();
                var real_val = val.replace(/,/g,'');
                $(this).val(real_val);
            });
        }else{
            e.preventDefault();
        }
    });

    
    // Validate  --------
    $('#form_vehicle').pxValidate({
        rules: {
            'brand_id': {
                required: true,
            },
            'model_id':{
                required: true,
            },
            'year':{
                required: true,
            },
            'body_type':{
                required: true,
            },
            'model_type':{
                required: true,
            },
            'model_type_full':{
                required: true,
            },
            'mortor_code_av':{
                required: true,
                number: true
            },
            'mortor_code_ac':{
                required: true,
                number: true
            },
            'cc':{
                required: true,
                number: true
            },
            'tons':{
                required: true,
                number: true
            },
            'car_seat':{
                required: true,
                number: true
            },
            'driver_passenger':{
                required: true,
            },
            'red_plate':{
                required: true,
                number: true
            },
            'used_car':{
                required: true,
                number: true
            },
            'car_age':{
                required: true,
                number: true
            }
        }
    });

});