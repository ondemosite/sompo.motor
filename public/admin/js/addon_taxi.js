$(function() {

    function clearModal(){
      $("#form_addon").find("input[name='id']").val(null);
      $("#form_addon").find("input[name='sum_insured']").val(null);

      $("#form_addon").find("input[name='net_normal']").val(null);
      $("#form_addon").find("input[name='stamp_normal']").val(null);
      $("#form_addon").find("input[name='vat_normal']").val(null);
      $("#form_addon").find("input[name='normal']").val(null);
      $("#form_addon").find("input[name='net_under29']").val(null);
      $("#form_addon").find("input[name='stamp_under29']").val(null);
      $("#form_addon").find("input[name='vat_under29']").val(null);
      $("#form_addon").find("input[name='under29']").val(null);
      $("#form_addon").find("input[name='net_over29']").val(null);
      $("#form_addon").find("input[name='stamp_over29']").val(null);
      $("#form_addon").find("input[name='vat_over29']").val(null);
      $("#form_addon").find("input[name='over29']").val(null);
      $("#form_addon").find("input[name='net_cctv']").val(null);
      $("#form_addon").find("input[name='stamp_cctv']").val(null);
      $("#form_addon").find("input[name='vat_cctv']").val(null);
      $("#form_addon").find("input[name='cctv']").val(null);
    }

    $("#open-modal").click(function(){
      $("#form_addon").find("input[name='sum_insured']").focus();
      clearModal();
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
        processing: true,
        serverSide: true,
        ajax: $("input[name='hd_list']").val(),
        columns: [
            { "data": null,"width":"10%","sortable": false},
            { "data": "sum_insured" },
            { "data": "normal" },
            { "data": "under29" },
            { "data": "over29" },
            { "data": "cctv"},
            { "data": "updated_by"},
            { "data": "action","width": "10%","className": "text-center" },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
          var info = $(this).DataTable().page.info();
          $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
          return nRow;
      }
  });
    
    $('#datatables_wrapper .table-caption').text('Addon Taxi List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_get']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              console.log(result);
              if(!result.resp_error){
                  clearModal();
                  $("#form_addon").find("input[name='id']").val(result.id);
                  $("#form_addon").find("input[name='sum_insured']").val(result.sum_insured);
                  $("#form_addon").find("input[name='normal']").val(result.normal);
                  $("#form_addon").find("input[name='under29']").val(result.under29);
                  $("#form_addon").find("input[name='over29']").val(result.over29);
                  $("#form_addon").find("input[name='cctv']").val(result.cctv);
                  $("#form_addon").find("input[name='net_normal']").val(result.net_normal);
                  $("#form_addon").find("input[name='net_under29']").val(result.net_under29);
                  $("#form_addon").find("input[name='net_over29']").val(result.net_over29);
                  $("#form_addon").find("input[name='net_cctv']").val(result.net_cctv);
                  $("#form_addon").find("input[name='stamp_normal']").val(result.stamp_normal);
                  $("#form_addon").find("input[name='stamp_under29']").val(result.stamp_under29);
                  $("#form_addon").find("input[name='stamp_over29']").val(result.stamp_over29);
                  $("#form_addon").find("input[name='stamp_cctv']").val(result.stamp_cctv);
                  $("#form_addon").find("input[name='vat_normal']").val(result.vat_normal);
                  $("#form_addon").find("input[name='vat_under29']").val(result.vat_under29);
                  $("#form_addon").find("input[name='vat_over29']").val(result.vat_over29);
                  $("#form_addon").find("input[name='vat_cctv']").val(result.vat_cctv);
                  $("#modal-add").modal('toggle');
                  $("#modal-add").find(".modal-title").find("span").html("Edit Addon Taxi");
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
    });



  //------------------------

    // Validate Modal --------
    $('#form_addon').pxValidate({
        rules: {
            'sum_insured':{
                required: true,
                number: true,
            },
            'normal':{
                required: true,
                number: true,
            },
            'under29': {
                required: true,
                number: true,
            },
            'over29': {
                required: true,
                number: true,
            },
            'cctv': {
                required: true,
                number: true,
            },
            'net_normal':{
                required: true,
                number: true,
            },
            'net_under29': {
                required: true,
                number: true,
            },
            'net_over29': {
                required: true,
                number: true,
            },
            'net_cctv': {
                required: true,
                number: true,
            },
        },
    });
    //------------------------

    // Modal Event -----------
    function saveData(){
        if($('#form_addon').valid()){
            $.ajax({
                url: $("input[name='hd_save']").val(),
                data: $("#form_addon").serialize(),
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "success"){
                        table.ajax.reload();
                        notify("Save Data Success.","success");
                    }
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function() {
                $("#modal-add").modal('toggle');
            });
        }
    }

    $("#modal-add #submit_button").click(function(){
        saveData();
    });


  //------------------------


});