$(function() {

    var current_year = null;
    var current_month = null;

    $('.datepicker').datepicker({
        todayBtn:              'linked',
        clearBtn:              true,
        todayHighlight:        true,
        orientation:           'auto left',
        format: 'yyyy-mm-dd',
        maxViewMode:'days'
    });

    function clearModal(){
      $("#form_cutoff").find("input[name='id']").val(null);
      $("#form_cutoff").find("select[name='years']").val(null).change();
      $("#form_cutoff").find("select[name='months']").val(null).change();
      $("#form_cutoff").find("input[name='cutoff_date']").val(null);
      $("#modal-cutoff").find(".modal-title").find("span").html("Add Cutoff");
      $("#form_cutoff").find("input[name='cutoff_date']").attr("disabled","disabled");
      current_year = current_month = null;
    }

    function setRangeDate(){
        if(current_year!=null && current_month!=null){

            var startDate = moment([current_year, current_month - 1]);
            var endDate = moment(startDate).endOf('month');
            $("#form_cutoff").find("input[name='cutoff_date']").removeAttr("disabled");
            $('.datepicker').datepicker('setStartDate',startDate.toDate());
            $('.datepicker').datepicker('setEndDate',endDate.toDate());
            $('.datepicker').datepicker('setDate',startDate.toDate());
        }
    }

    $("#open-modal").click(function(){
      clearModal();
    });

    $("#form_cutoff").find("select[name='years']").change(function(){    
        current_year = $(this).val();
        setRangeDate();
    });

    $("#form_cutoff").find("select[name='months']").change(function(){
        current_month = $(this).val();
        setRangeDate();
    });

    

    // DataTable -------------
    var table = $('#datatables').DataTable({
          processing: true,
          ajax :{
              "url": $("input[name='hd_list']").val(),
              "data":function(d){
                  return {
                      'search_years':$("[name='search_years']").val(),
                      'search_months':$("[name='search_months']").val(),
                  }
              }
          },
          columns: [
              { "data": null,"width":"10%","sortable": true},
              { "data": "years","sortable": true},
              { "data": "months","sortable": true},
              { "data": "cutoff_date","sortable": true},
              { "data": "updated_at","sortable": true},
              { "data": "updated_by","sortable": true},
              { "data": "action","width": "10%","className": "text-center" },
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Cutoff List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    $("#search").click(function(){
        table.ajax.reload();
    });

    // Edit
    $(document).on("click",".action-edit",function(){
          var id = $(this).data("id");
          $.ajax({
              url: $("input[name='hd_get']").val(),
              data: {
                  '_token': $('meta[name="csrf-token"]').attr('content'),
                  'id':id
              },
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              console.log(result);
              if(!result.resp_error){
                  clearModal();
                  $("#form_cutoff").find("input[name='id']").val(result.id);
                  $("#form_cutoff").find("select[name='years']").val(result.years).change();
                  $("#form_cutoff").find("select[name='months']").val(result.months).change();
                  $('.datepicker').datepicker('setDate',result.cutoff_date);
                  $("#modal-cutoff").modal('toggle');
                  $("#modal-cutoff").find(".modal-title").find("span").html("Edit Cutoff");
                  current_year = result.years;
                  current_month = result.months;
                  $("#form_cutoff").find("input[name='cutoff_date']").removeAttr("disabled");

              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          });
    });

    //Delete  
    $(document).on("click",".action-delete",function(){
          var id = $(this).data("id");
          var name = $(this).data("name");
          bootbox.confirm({
              message:   'Are you sure to delete <b>'+name+'</b> ?',
              className: 'bootbox-sm',
              callback: function(state) {
                  if(state){
                      $.ajax({
                          url: $("input[name='hd_delete']").val(),
                          data: {
                              '_token': $('meta[name="csrf-token"]').attr('content'),
                              'id':id
                          },
                          type: 'DELETE',
                          dataType: "json",
                          timeout:10000
                      }).done(function(result){
                          if(!result.resp_error){
                              notify("Delete Success","success");
                              table.ajax.reload();
                          }else{
                              notify(result.resp_error,"danger");
                          }
                      }).fail(function(jqXHR, textStatus){
                          if(textStatus == 'timeout'){
                              notify("Server Response Timeout!","danger");
                          }
                          else{
                              notify(textStatus,"danger");
                          }
                      });
                  }
              },
          });
          
    });

  //------------------------

  // Validate Modal --------
  $('#form_cutoff').pxValidate({
      rules: {
        'years': {
            required: true,
            number: true
        },
        'months':{
            required: true,
            number: true
        },
        'cutoff_date':{
            required: true
        }
      },
  });
  //------------------------

  // Modal Event -----------
  function saveData(){
      if($('#form_cutoff').valid()){
          $.ajax({
              url: $("input[name='hd_save']").val(),
              data: $("#form_cutoff").serialize(),
              type: 'POST',
              dataType: "json",
              timeout:10000
          }).done(function(result){
              if(!result.resp_error){
                  if(result.status == "success"){
                      table.ajax.reload();
                      notify("Save Data Success.","success");
                  }else{
                    notify(result.reason,"danger");
                  }
              }else{
                  notify(result.resp_error,"danger");
              }
          }).fail(function(jqXHR, textStatus){
              if(textStatus == 'timeout'){
                  notify("Server Response Timeout!","danger");
              }
              else{
                  notify(textStatus,"danger");
              }
          }).always(function() {
              $("#modal-cutoff").modal('toggle');
          });
      }
  }

  $("#modal-cutoff #submit_button").click(function(){
      saveData();
  });
  //------------------------


});