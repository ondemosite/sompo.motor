$(function() {
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto right',
        format: 'yyyy-mm-dd',
    });

    function parseHTML(html) {
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>'+ html,'text/html');
        return dom.body.textContent;
    }

    // DataTable -------------
    var table = $('#datatables').DataTable({
            processing: true,
            serverSide: true,
            stateSave: false,
            pageLength: 15,
          ajax :{
              "url": $("input[name='hd_policy_list']").val(),
              "data":function(d){
                d._token = $('meta[name="csrf-token"]').attr('content');
                d.search_policy_number = $("[name='search_policy_number']").val();
                d.search_policy_com_number = $("[name='search_policy_com_number']").val();
                d.search_driver_name = $("[name='search_driver_name']").val();
                d.search_insurance_ft_si = $("[name='search_insurance_ft_si']").val();
                d.search_car_licence = $("[name='search_car_licence']").val();
                d.search_from_date = $("[name='search_from_date']").val();
                d.search_to_date = $("[name='search_to_date']").val();
                d.search_status = $("[name='search_status']").val();
                d.search_post = $("[name='search_post']").val();
                return d;
            }
          },
          columns: [
              { "data": null,"width":"5%","sortable": false},
              { "data": "policy_number","width":"15%" },
              { "data": "policy_com_number","width":"15%" },
              { "data": "name","width":"15%",orderable: false },
              { 
                "data": "brand",
                'render':function(data,type,row){
                    var obj = $.parseJSON(parseHTML(data));
                    return obj.brand+" "+obj.model+" "+obj.year+"<br/>"+obj.car_licence;
                },
                "width":"10%"
              },
              { "data": "insurance_ft_si" },
              { 
                  "data": "status",
                  'render':function(data,type,row){
                        if(data=="NORMAL"){
                            return '<span class="text-default"> ปกติ </span>';
                        }else if(data=="EDIT"){
                            return '<span class="text-warning"> แก้ไข </span>';
                        }else if(data=="CANCEL"){
                            return '<span class="text-light"> ยกเลิก </span>';
                        }else if(data=="EXPIRE"){
                            return '<span class="text-danger"> หมดอายุ </span>';
                        }else if(data=="INACTIVE"){
                            return '<span class="text-danger"> ปิด </span>';
                        }
                  },
                  "width":"10%"
              },
              { 
                  "data": "created_at",
                  'render':function(data,type,row){
                      var html = "";
                      var obj = $.parseJSON(parseHTML(data));
                      html = obj.date;
                      if(obj.state=="NEW"){
                          html+='&nbsp;<span class="label label-success">New</span>';
                      }
                      return html;
                  },
                  "width":"10%"
              },
              { "data": "action","className": "text-center","sortable": false},
          ],
          "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        }
    });
    
    $('#datatables_wrapper .table-caption').text('Order List');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    $("#search").click(function(){
        table.ajax.reload();
    });

    $("#clear").click(function(){
        $("input[name='search_policy_number']" ).val(null);
        $("input[name='search_policy_com_number']" ).val(null);
        $("input[name='search_insurance_ft_si']").val(null);
        $("input[name='search_car_licence']").val(null);
        $("input[name='search_from_date']").val(null);
        $("input[name='search_to_date']").val(null);
        $("select[name='search_status']").val(null).change();
        table.ajax.reload();
    });


});