$(document).ready(function(){
   
    $('.datepicker').datepicker({
        calendarWeeks:         true,
        todayBtn:              'linked',
        //daysOfWeekDisabled:    '1',
        clearBtn:              true,
        todayHighlight:        true,
        //daysOfWeekHighlighted: '1,2',
        orientation:           'auto left',
        format: 'yyyy-mm-dd',
    });

    // DataTable -------------
    var table = $('#datatables').DataTable({
        processing: true,
        ajax :{
            "url": $("input[name='hd_get_register_list']").val(),
            'data': {
                'promotion_id': $("[name='promotion_id']").val()
            }
        },
        columns: [
            { "data": null,"width":"10%","sortable": false},
            { "data": "name" },
            { "data": "email" },
            { "data": "tel" },
            { "data": "created_at" },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
          var info = $(this).DataTable().page.info();
          $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
          return nRow;
      }
  });
  
  $('#datatables_wrapper .table-caption').text('Register List');
  $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

    // Validate  --------
    $('#form_promotion').pxValidate({
        rules: {
            'title': {
                required: true,
            },
            'code': {
                required: true,
            },
            'discount':{
                required: true,
                number:true
            },
            'coverage_id[]':{
                required: true,
            },
            'maximum_grant':{
                required: true,
                number: true,
            },
        },
        messages: {
            'maximum_grant':{
                required: 'กรุณาระบุจำนวนคูปองสูงสุด',
                number: 'เฉพาะตัวเลขเท่านั้น',
            },
            'coverage_id[]':{
                required: 'กรุณาเลือกแผน',
            },
        }
    });

    $('#form_register').pxValidate({
        rules: {
            'max_register': {
                required: true,
            },
            'register_start_at':{
                required: true,
            },
            'register_expire_at':{
                required: true,
            },
        },
    });



    $('.select2').select2({
        placeholder: 'Select value',
    });

    $("[name='is_register']").change(function(){
        var mode = "false";
        if($("[name='is_register']").is(':checked')){
            mode = "true";
        }

        $.ajax({
            url: $("[name='hd_set_register_status']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'promotion_id':$("[name='promotion_id']").val(),
                'mode': mode
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                if(result.status=="success"){
                    if(mode=="true"){
                        $(".register-body").addClass('active');
                    }else{
                        $(".register-body").removeClass('active');
                    }
                }else{
                    notify('Error',"danger");
                }
            }else{
                notify(result.resp_error,"danger");
            }
        }).fail(function(jqXHR, textStatus){
            notify(textStatus,"danger");
        });


        
    });

    $(document).on("change",".select_file",function(){
        var target = $(this).attr("name");
        readURL(this,$(".image-preview[data-belong='"+target+"']"));
        $("input[name='clear_"+target+"']").val("false");
    });

    $(document).on("click",".btn-change",function(){
        var target = $(this).data("target");
        $("input[name='"+target+"']").click();
    });

    $(document).on("click",".btn-remove",function(){
        var handle = $(this);
        var target = $(this).data("target");
        bootbox.confirm({
            message: 'Are you sure to clear Image ?',
            callback: function (state) {
                if(state){
                    var $el = $("input[name='"+target+"']");
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    handle.parent().siblings(".box-image-preview").find(".image-preview").attr("src","/images/admin/background/preview-banner-promotion.jpg");
                    $("input[name='clear_cover']").val("true");
                }
            }
        });
    });

    $("#submit_register").click(function(){
        if($("#form_register").valid()){
            $("#form_register").addClass("form-loading form-loading-inverted");

            var formData = new FormData();
            formData.append('promotion_id', $("[name='promotion_id']").val());
            formData.append('max_register', $("[name='max_register']").val());
            formData.append('register_start_at', $("[name='register_start_at']").val());
            formData.append('register_expire_at', $("[name='register_expire_at']").val());
            formData.append('clear_cover', $("[name='clear_cover']").val());
            formData.append('cover_register', $("[name='cover_register']")[0].files[0]);
            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

            $.ajax({
                url: $("[name='hd_save_register_status']").val(),
                type: "post",
                data : formData,
                dataType: "json",
                processData: false,  // tell jQuery not to process the data
                contentType: false,
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    if(result.status == "success"){
                        notify("Updated Promotion Success!!","success");
                    }else{
                        notify(result.reason,"danger");
                    }
                }else{
                    notify(result.resp_error,"danger");
                }
            }).fail(function(jqXHR, textStatus){
                notify(textStatus,"danger");
            }).always(function(){
                $("html, body").animate({ scrollTop: 0 }, "fast");
                $("#form_register").removeClass("form-loading form-loading-inverted");
            });
        }
    });


    //hd_get_vehicle_model
    function getVehicleBrand(){
        $.ajax({
            url: $("[name='hd_get_vehicle_brand']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            console.log(result);
            if(!result.resp_error){
                select = '<option selected="selected" value="">'+$("[name='hd_lang_brand']").val()+'</option>';
                $.each(result, function(i,data)
                {
                    select +='<option value="'+i+'">'+data+'</option>';
                });
                select += '</select>';
                $("select[name='vehicle_brand']").html(select);
                if($("[name='hd_def_brand']").val()!=""){
                    $("select[name='vehicle_brand'] option[value="+$("[name='hd_def_brand']").val()+"]").prop('selected', true);
                    $("select[name='vehicle_brand']").change();
                }
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        }).always(function(){
            $(".form-model").removeClass("loading");
        });
    }getVehicleBrand();

    function getVehicleModel(id){
        if(id!="" && id!=null){
            $.ajax({
                url: $("[name='hd_get_vehicle_model']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">'+$("[name='hd_lang_model']").val()+'</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='vehicle_model']").html(select);
                    if($("[name='hd_def_model']").val()!=""){
                        $("select[name='vehicle_model'] option[value="+$("[name='hd_def_model']").val()+"]").prop('selected', true);
                    }
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function(){
                $(".form-model").removeClass("loading");
            });
        }else{
            select = '<option selected="selected" value="">'+$("[name='hd_lang_model']").val()+'</option>';
            $("select[name='vehicle_model']").html(select);
            $("select[name='vehicle_model']").change();
        }
    }


    $("select[name='vehicle_brand']").change(function(){
        var id = $(this).val();
        getVehicleModel(id);
    });

    $("[name='maximum_grant']").change(function(e){
        var valMax = parseInt($(this).val());
        var valUsed = parseInt($("[name='used_coupon']").val());
        if(valMax < valUsed){
            $("[name='maximum_grant']").val(valUsed);
            $(".coupon-alert").removeClass("hide");
        }else{
            $(".coupon-alert").addClass("hide");
        }
        calculateCoupon();
    });


    function calculateCoupon(){
        var max = parseInt($("[name='maximum_grant']").val());
        var used = parseInt($("[name='used_coupon']").val());
        var remaining = max - used;
        if(max - used < 0) remaining = 0;
        $("[name='remaining']").val(remaining);
    }calculateCoupon();

    $("[name='max_register']").change(function(e){
        var valMax = parseInt($(this).val());
        var valUsed = parseInt($("[name='used_register']").val());
        if(valMax < valUsed){
            $("[name='max_register']").val(valUsed);
            $(".register-alert").removeClass("hide");
        }else{
            $(".register-alert").addClass("hide");
        }
        calculateRegister();
    });

    function calculateRegister(){
        var max = parseInt($("[name='max_register']").val());
        var used = parseInt($("[name='used_register']").val());
        var remaining = max - used;
        if(max - used < 0) remaining = 0;
        $("[name='remaining_register']").val(remaining);
    }calculateRegister();

});