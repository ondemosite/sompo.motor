$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};

function setAnimate(handle,animate,delay){
    if(!handle.hasClass("finished-animate")){
        handle.addClass(animate);
        handle.addClass("finished-animate");
        handle.removeClass("myfade");
    }
}

function setDefaultSelect(){
    select = '<option selected="selected" value="">'+$("[name='hd_lang_model']").val()+'</option>';
    $("select[name='input_model']").html(select);
    select = '<option selected="selected" value="">'+$("[name='hd_lang_model_year']").val()+'</option>';
    $("select[name='input_model_year']").html(select);
    select = '<option selected="selected" value="">'+$("[name='hd_lang_sub_model']").val()+'</option>';
    $("select[name='input_sub_model']").html(select);
}

function detectMenu(){
    $('.target-detect').each(function() {
        if ($(this).isInViewport()) {
            var target = $(this).data("target");
            var type = window.location.hash.substr(1);
            $(".nav-link").removeClass("active");
            if(target == "target-product" && type=="section-product"){
                $(".nav-link[data-target='"+target+"']").addClass("active");
            }else if((target == "target-faq" || target == "target-footer") && type=="section_faq"){
                $(".nav-link[data-target='target-faq']").addClass("active");
            }else if(target == "target-footer" && type=="section_footer"){
                $(".nav-link[data-target='"+target+"']").addClass("active");
            }else{
                $(".nav-link[data-target='target-home']").addClass("active");
            }
        }
    });
}

$(document).ready(function(){
    setDefaultSelect();

    $("#form_calculate").find("button.my-radio").click(function(){
        var value = $(this).val();
        var target = $(this).data("target");

        $(".my-radio[data-target='" + target + "']").removeClass("active");
        $(this).addClass("active");
        $("input[name='"+target+"'").val(value);
    });

    $(".slide-row").slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    dots: true,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                }
            }
        ]
    });

    $(".promotion-slider").slick({
        arrows: true,
        autoplay: true,
        autoplaySpeed: 3000,
    });

    $('.wp-animate').waypoint(function() {
        setAnimate($(this.element),this.element.getAttribute('data-wptype'),0);
    }, { offset: '80%' });

    $('.wp-animate').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
        var ownClass = $(this).data("wptype");
        $(this).removeClass(ownClass);
    });

    $("select[name='input_garage_prov']" ).change(function(){
        var id = $(this).val();
        if(id!=null && id!=""){
            $.ajax({
                url: $("[name='hd_get_data_district']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">อำเภอ</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_garage_district']").html(select);
                }else{
                    notify("Error please try again.","danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify("Error please try again.","danger");
                }
            })
        }else{
            select = '<option selected="selected" value="">อำเภอ</option>';
            $("select[name='input_garage_district']").html(select);
        }
    });

    // $("input[name='input_garage_name']").keyup(function(){
    //     if($(this).val().length > 4){
    //             $.ajax({
    //                 url: $("[name='hd_get_data_garage']").val(),
    //                 type: "post",
    //                 data: {
    //                     '_token': $('meta[name="csrf-token"]').attr('content'),
    //                     'search_text':$(this).val().trim()
    //                 },
    //                 dataType: "json",
    //                 timeout:10000
    //             }).done(function( result ) {
    //                 if(!result.resp_error){
    //                     if(result.length>0){
                            
    //                         var html = "";
    //                         $.each(result, function(i,data){
    //                             html+='<li class="search-item-result" data-id="'+data.id+'" data-name="'+data.name+'"><h3>'+data.name+'</h3><p><small>'+data.address+'</small></p></li>';
    //                         });
    //                         $(".result-box").find("ul").html(html);
    //                         $(".result-box").show();
    //                     }   
    //                 }
    //             });
    //     }else{
    //         $(".result-box").hide();
    //     }
    // });

    $("select[name='input_brand']").change(function(){
        var id = $(this).val();
        if(id!="" && id!=null){
            $(".form-model").addClass("loading");
            $.ajax({
                url: $("[name='hd_get_data_model']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">'+$("[name='hd_lang_model']").val()+'</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_model']").html(select);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function(){
                $(".form-model").removeClass("loading");
            });
        }else{
            select = '<option selected="selected" value="">'+$("[name='hd_lang_model']").val()+'</option>';
            $("select[name='input_model']").html(select);
            $("select[name='input_model']").change();
        }
    });

    $("select[name='input_model']").change(function(){
        var id = $(this).val();
        if(id!="" && id!=null){
            $(".form-model-year").addClass("loading");
            $.ajax({
                url: $("[name='hd_get_data_model_year']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">'+$("[name='hd_lang_model_year']").val()+'</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+data+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_model_year']").html(select);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function(){
                $(".form-model-year").removeClass("loading");
            });
        }else{
            select = '<option selected="selected" value="">'+$("[name='hd_lang_model_year']").val()+'</option>';
            $("select[name='input_model_year']").html(select);
            $("select[name='input_model_year']").change();

        }
    });

    $("select[name='input_model_year']").change(function(){
        var year = $(this).val();
        var brand = $("[name='input_brand']").val();
        var model = $("[name='input_model']").val();
        if(year!="" && year!=null){
            $(".form-sub-model").addClass("loading");
            $.ajax({
                url: $("[name='hd_get_data_model_type']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'year':year,
                    'brand_id':brand,
                    'model_id':model
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">'+$("[name='hd_lang_sub_model']").val()+'</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+data+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_sub_model']").html(select);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function(){
                $(".form-sub-model").removeClass("loading");
            });
        }else{
            select = '<option selected="selected" value="">'+$("[name='hd_lang_sub_model']").val()+'</option>';
            $("select[name='input_sub_model']").html(select);
            $("select[name='input_sub_model']").change();
        }
    });

    $(document).on("click",".search-item-result",function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $("[name='selected_name']").val(id);
        $("#form_garage_name").submit();
    });

    $.validator.addMethod("currentcy",function(value,element){
        if(value!=""){
            var regex = /^[0-9,]+$/;
            return regex.test( value );
        }
        return true;
    });

    // Validate  --------
    $('#form_calculate').validate({
        rules: {
            'input_brand': {
                required: true,
            },
            'input_model':{
                required: true,
            },
            'input_model_year':{
                required: true,
            },
            'input_sub_model':{
                required: true,
            },
            'input_is_bkk':{
                required: true,
            },
            'input_plan': {
                required: true,
            }
        },
        messages: {
            'input_brand': {
                required: $("[name='hd_lang_required_brand']").val(),
            },
            'input_model':{
                required: $("[name='hd_lang_required_model']").val(),
            },
            'input_model_year':{
                required: $("[name='hd_lang_required_year']").val(),
            },
            'input_sub_model':{
                required: $("[name='hd_lang_required_submodel']").val(),
            },
            'input_is_bkk':{
                required: 'กรุณาเลือกจังหวัดที่จดทะเบียน',
            },
            'input_plan':{
                required: 'กรุณาเลือกประกันภัยรถยนต์',
            },

        }
    });

    // Faq Collapse ---
    $('.faq-header').click( function(e) {
        $('.faq-body').collapse('hide');
    });

    //Our Product --
    $(document).on("click",".product-toggle-hide",function(){
        $(this).parent().slideUp("fast");
    });
    $(document).on("click",".product-toggle-show",function(){
        $(this).parent().siblings(".article_detail").slideDown("fast",function(){
            
        });
    });

    $('.input-number').simpleMoneyFormat();

    

    $(window).on('resize scroll', function() {
        detectMenu();
    });

    $(".nav-link").click(function(){
        $(".nav-link").removeClass("active");
        $(this).addClass("active");
    });


    $("#searchGarageSubmit").click(function(e){
        var provinceText = $("[name='input_garage_prov'] option:selected").text();
        var provinceVal = $("[name='input_garage_prov'] option:selected").val();
        var garage = $("[name='input_garage_name']").val();
        var externalLink = "https://www.sompo.co.th/page/motor_claim?provinceName="+(provinceVal?provinceText:'')+"&garage="+garage+"#garageZone"
        window.open(externalLink,'_blank');
        e.preventDefault();
    });

    
  
});