$(document).ready(function(){

    $.validator.addMethod("custom_email",function(value,element){
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( value );
    });
    
    $('#form_register').validate({
        rules: {
            input_name: {
                required: true,
            },
            input_lastname : {
                required: true,
            },
            input_email: {
                required: true,
                custom_email: true
            },
            input_tel :{
                required: true,
                number:true,
                minlength: 6
            },
            input_brand: {
                required: true,
            },
            input_model: {
                required: true,
            },
        },
        messages: {
            input_name: {
                required: $("[name='hd_lang_required']").val(),
            },
            input_lastname : {
                required: $("[name='hd_lang_required']").val(),
            },
            input_email: {
                required: $("[name='hd_lang_required']").val(),
                custom_email: $("[name='hd_lang_email']").val()
            },
            input_tel :{
                required: $("[name='hd_lang_required']").val(),
                number:$("[name='hd_lang_number']").val(),
                minlength: $("[name='hd_lang_tel']").val()
            },
            input_brand: {
                required: $("[name='hd_lang_required']").val(),
            },
            input_model : {
                required: $("[name='hd_lang_required']").val(),
            },
        }
    });

    function setDateInitial(lang){
        $option_main = {
            'format' : null,
            'lang'  : lang,
            weekStart : 0, 
            time: false
        }
        if($option_main!=null){
            if(lang=='en'){
                $option_main['format'] = 'DD MMMM YYYY';
            } 
            $("#date").bootstrapMaterialDatePicker($option_main);
        }
    }setDateInitial($("input[name='hd_current_lang']").val());


    $("select[name='input_brand']").change(function(){
        var id = $(this).val();
        if(id!="" && id!=null){
            $(".form-model").addClass("loading");
            $.ajax({
                url: $("[name='hd_get_vehicle_model']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">'+$("[name='hd_lang_model']").val()+'</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_model']").html(select);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            }).always(function(){
                $(".form-model").removeClass("loading");
            });
        }else{
            select = '<option selected="selected" value="">'+$("[name='hd_lang_model']").val()+'</option>';
            $("select[name='input_model']").html(select);
            $("select[name='input_model']").change();
        }
    });


    $("#button-submit").click(function(){
        if($("#form_register").valid()){

            //Validate Car Model
            var flagPass = true;
            var fixModel = $("[name='fix_model']").val();
            var fixBrand = $("[name='fix_brand']").val();
            var inputModel = $("select[name='input_model'] option:selected").val();
            var inputBrand = $("select[name='input_brand'] option:selected").val();
            if(typeof fixModel!=="undefined"){
                var fixModelArray = fixModel.split(",");
                if(fixModelArray.indexOf(inputModel) == -1){
                    flagPass = false;
                }
            }
            if(typeof fixBrand!=="undefined"){
                var fixBrandArray = fixBrand.split(",");
                if(fixBrandArray.indexOf(inputBrand) == -1){
                    flagPass = false;
                }
            }
            if(!flagPass){
                alert("ข้อมูล ยี่ห้อรถยนต์ หรือ รุ่นรถยนต์ ไม่ตรงกับเงื่อนไขของโปรโมชัน");
                return false;
            }else{
                $(".wrap-form").loader();
                $.ajax({
                    url: $("#form_register").attr("action"),
                    type: "post",
                    data: $("#form_register").serialize(),
                    dataType: "json",
                    timeout:10000
                }).done(function(result){
                    console.log(result);
                    if(!result.resp_error){
                        if(result.status == "SUCCESS"){
                            $(".wrap-popup").addClass("active");
                            $(".box-popup").addClass("bounceIn");
                            setTimeout(() => {
                                document.location.href="/";
                            }, 2000);
                        }
                    }else{
                        notify(result.resp_error,"danger");
                    }
                }).fail(function(jqXHR, textStatus){
                    if(textStatus == 'timeout'){
                        notify("Server Response Timeout!","danger");
                    }
                    else{
                        notify(textStatus,"danger");
                    }
                }).always(function(){
                    $(".wrap-form").removeLoader();
                });
            }





            
        }
    });


});