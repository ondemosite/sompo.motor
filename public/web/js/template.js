/*** Check Mobile ****/
var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    }
};

function isMobileDetect(){
    if (isMobile.Android() || isMobile.iOS()) {
        return true;
    }
    return false;
}


function ajaxLogin(form){
    $.ajax({
        url:$(form).attr('action'),
        type:'POST',
        data:$(form).serialize(),
        dataType: "json",
        success:function(data){
            if(!data.resp_error){
                if(data.auth){
                    $(".my-alert").html("เข้าสู่ระบบสำเร็จ").removeClass("alert-danger").addClass("alert-success").slideDown("fast");
                    setTimeout(function(){location.reload()},1500);
                }else{
                    $(".my-alert").html(data.reason).addClass("alert-danger").slideDown("fast");
                }
            }else{
                $(".my-alert").html(data.resp_error).addClass("alert-danger").slideDown("fast");
            }
            $(".wrap_register").hide();
            $(".wrap_login").show();
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function notify(message,type){
    $.notify({
        message: message
    },{
        type: type,
        placement: {
            from: "top",
            align: "center"
        }
    });
}





$(document).ready(function(){
    $("#loginForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 4
            },
        },
        submitHandler: function (form) {
            ajaxLogin(form);
            return false;
        }
    });
   
    
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $(".web-navbar").addClass("affix");
        } else {
            $(".web-navbar").removeClass("affix");
        }
    });
    $('.backtotop').click(function(){
            $('html, body').animate({scrollTop: '0px'}, 500);
            return false;
    });

    jQuery('img.svg').each(function(){
      var $img = jQuery(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src');

      jQuery.get(imgURL, function(data) {
          // Get the SVG tag, ignore the rest
          var $svg = jQuery(data).find('svg');

          // Add replaced image's ID to the new SVG
          if(typeof imgID !== 'undefined') {
              $svg = $svg.attr('id', imgID);
          }
          // Add replaced image's classes to the new SVG
          if(typeof imgClass !== 'undefined') {
              $svg = $svg.attr('class', imgClass+' replaced-svg');
          }

          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');

          // Replace image with new SVG
          $img.replaceWith($svg);

        }, 'xml');
    });

    $(".select2").select2({
        theme: "bootstrap",
        width: '100%'
    });

    $('a.scroll-active').click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top-70 
            }, 1000, function() {
                /*
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }; */
            });
          }
        }
    });

    


});

