function showRegisterForm(){
    document.getElementById('wrap_login').style.display = 'none';
    document.getElementById('wrap_register').style.display = 'block'; 
}

function showLogin(){
    document.getElementById('wrap_register').style.display = 'none';       
    document.getElementById('wrap_login').style.display = 'block';     
}

$(document).ready(function(){
    $("#loginForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 4
            }
        }
    });
    $("#registerForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 4
            },
            retype_password:{
                required: true,
                minlength: 4,
                equalTo: "#register_password"
            }
        }
    });
});