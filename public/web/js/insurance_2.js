$(document).ready(function(){

    function checkIsTabletScreen(){
        var screenWidth = window.innerWidth;
        if(screenWidth <= 991){
            return true;
        }
        return false;
    }

    function setSummay(){
        $(".active-tag").each(function(){
            if($(this).hasClass("active")){
                $("#plan_price").html($(this).data("fi-form"));
                $("#plan_pay").html($(this).data("gloss"));
                $("#plan_choose").html($(this).data("name"));
                $("[name='input_ft_si']").val($(this).data("fi"));
            }
        })
    }setSummay();

    function focusForm(target){
        $('html, body').animate({
            scrollTop: target.offset().top-200 
          }, 500, function() {
          });
    }

    function findActiveTag(){
        $(".active-tag").each(function(index){
            if($(this).hasClass("active")){
                $("[name='input_ft_si']").val($(this).data('fi'));

                var parentHandle = $(this).parents(".plan-topic-item");
                $(".item-car").css("left",parentHandle.position().left);
                $(".item-car").css("top",10);
            }
        });
    }findActiveTag();


    $( window ).resize(function() {
        findActiveTag();
    });


    $(".active-tag").click(function(e){
        e.preventDefault();
        var target = $(this).data("target");
        $(".box-plan").removeClass("active");
        $(this).parent().addClass("active");
        $(".box-detail").removeClass("active");
        $(".active-tag").removeClass("active");
        $(this).addClass("active");
        $("[name='input_ft_si']").val($(this).data('fi'));
        $("#"+target).addClass("active");
        $(".box-detail-tablet[data-dest='"+target+"']").addClass("active");
        
            // //Desktop Animate
            findActiveTag();

            //Tablet Animate
            if($(".box-detail-tablet[data-dest='"+target+"']").hasClass("render")){
                $(".box-detail-tablet[data-dest='"+target+"']").removeClass("render");
            }else{
                isFocus = true;
                $(".box-detail-tablet[data-dest='"+target+"']").addClass("render");
            }

            $(".box-detail-tablet").each(function(element){
                if($(this).data("dest") != target){
                    if($(this).hasClass("render")){
                        $(this).removeClass("render");
                    }
                }   
            });

        _now_target = target;
        setSummay();
        
    });


    $(".item-car").click(function(){
        $("#form_submit").submit();
    });

    $(".wrap-box-detail").click(function(){
        var data_id = $(this).data("id");
        $(".active-tag[data-id='"+data_id+"']").click();
    });

    


});