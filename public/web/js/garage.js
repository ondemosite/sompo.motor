var submit_running = false;
var markers = [];
var focus_markers = {handle:null,svg:null}
var focus_info = {handle:null}
var focus_bounce = {handle:null}
var map ;
var svg_file = '<svg xmlns="http://www.w3.org/2000/svg" width="36" height="48" viewBox="-18 -48 36 48"><path fill="#4267b2" fill-opacity="0.85" d="M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z" /></svg>';
var url0 = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg_file); //Base 64 encode
var locations = [];
var mcOptions = {
    gridSize: 50,
    zoomControl: false,
    styles: [{
        imagePath: url0,
        height: 48,
        url: url0,
        width: 36,
        textColor: '#fff',
        textSize: 14,

    }]
};
var search_timeout = null;



function initMap() {
    var marker, i;

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      scrollwheel: false,
      center: {lat: (parseFloat(locations[0].lat))?parseFloat(locations[0].lat):13.7563, lng: (parseFloat(locations[0].long))?parseFloat(locations[0].long):100.5018}
    });


    for (i = 0; i < locations.length; i++) {

      var svg = locations[i].svg;
      var url = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg);

      var lat = parseFloat(locations[i].lat);
      var lng = parseFloat(locations[i].long);

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat,lng),
        id : locations[i].id,
        map: map,
        icon: url,
      });

      markers.push(marker);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {

          if(focus_markers.handle){
            focus_markers.handle.setIcon(focus_markers.svg);
          }

          //Clear Old Focus
          var svg_old = locations[i].svg;
          var url_old = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg_old);
          focus_markers.handle = this;
          focus_markers.svg = url_old;

          //Active Focus
          var svg3 = locations[i].svg_focus;
          var url3 = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg3);
          this.setIcon(url3);

          //Set Center
         
          map.panTo(new google.maps.LatLng(lat,lng));

          //Set info
          var infoWindow = new google.maps.InfoWindow({
             content:locations[i].name
          });
          infoWindow.open(map,marker);

          //Clear Info
          if(focus_info.handle!=null){
            focus_info.handle.close(map,marker);
          }
          focus_info.handle = infoWindow;

          //Bounce
          if(focus_bounce.handle!=null){
            focus_bounce.handle.setAnimation(null);
          }
          focus_bounce.handle = marker;
          marker.setAnimation(google.maps.Animation.BOUNCE);


            $(".search-result").find('li.list-item.active').removeClass("active");
            $(".search-result").find('li.list-item').each(function(){
                if($(this).data('id') == marker.id){
                    $(this).addClass('active');
                    $('.search-result').scrollTo(".list-item[data-id='" + marker.id + "']",200,function(){});
                }
            });

        }
      })(marker, i));


    }
    var markerCluster = new MarkerClusterer(map,markers,mcOptions);
  }


  function clearMap() {
    focus_markers.handle = null;
    focus_markers.svg = null;
    locations = [];
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers = [];
  }

  function findGarage(){
    $(".search-title").hide();
    $(".search-title-loading").show();
    $(".loding-icon").show();
    $(".search-result").find("li:not('.clone')").remove();

    if($("[name='selected_name']").val()!=null && $("[name='selected_name']").val()!=""){
        selectGarage($("[name='selected_name']").val());
        $("[name='selected_name']").val(null);
    }else{
        $.ajax({
            url: $("#form_garage").attr("action"),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'input_garage_prov':$("[name='input_garage_prov']").val(),
                'input_garage_district':$("[name='input_garage_district']").val(),
                'input_name':$("[name='input_name']").val(),
            },
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                clearMap();
                if(result.length>0){
                    locations = result;          
                    initMap();
                    
                    //Init Ul list
                    var clone_elm = $(".garage-list").find("li.clone");
                    $.each(locations, function( index, value ) {
                        cloned = clone_elm.clone();
                        $(cloned).removeClass("clone");
                        $(cloned).find("a.tag").html(locations[index].alpabet);
                        $(cloned).find(".list-names").html(locations[index].name);
                        $(cloned).find(".list-address").html(locations[index].address);
                        $(cloned).attr('data-id',locations[index].id);
                        cloned.appendTo(".search-result ul");
                    });
    
                    
                    
                }
                var search_title = "";
                if($("[name='input_garage_prov']").val()){
                    search_title+='"'+$("[name='input_garage_prov'] :selected").text().trim()+'"';
                }
                if($("[name='input_garage_district']").val()){
                    search_title+=',"'+$("[name='input_garage_district'] :selected").text().trim()+'"';
                }
                if($("[name='input_name']").val()!=""){
                    search_title+=',"'+$("[name='input_name']").val().trim()+'"';
                }
    
                $(".search-title b.search-name").html(search_title);
                $(".search-title b.search-amount").html(locations.length);
                
                $(".search-title").show();
                $(".search-title-loading").hide();
                $(".loding-icon").hide();
                
    
            }else{
                notify("Error please try again.","danger");
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify("Error please try again.","danger");
            }
        }).always(function() {
            submit_running = false;
        });
    }
    
  }


  function selectGarage(id){

    $(".search-title").hide();
    $(".search-title-loading").show();
    $(".loding-icon").show();
    $(".search-result").find("li:not('.clone')").remove();
    $.ajax({
        url: $("#form_garage").attr("action"),
        type: "post",
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            'input_garage_id':id,
        },
        dataType: "json",
        timeout:10000
    }).done(function(result){
        if(!result.resp_error){
            clearMap();
            if(result.length>0){
                locations = result;          
                initMap();
                
                //Init Ul list
                var clone_elm = $(".garage-list").find("li.clone");
                $.each(locations, function( index, value ) {
                    cloned = clone_elm.clone();
                    $(cloned).removeClass("clone");
                    $(cloned).find("a.tag").html(locations[index].alpabet);
                    $(cloned).find(".list-names").html(locations[index].name);
                    $(cloned).find(".list-address").html(locations[index].address);
                    $(cloned).attr('data-id',locations[index].id);
                    cloned.appendTo(".search-result ul");
                });

                var search_title = "";
                if($("[name='input_garage_prov']").val()){
                    search_title+='"'+$("[name='input_garage_prov'] :selected").text().trim()+'"';
                }
                if($("[name='input_garage_district']").val()){
                    search_title+=',"'+$("[name='input_garage_district'] :selected").text().trim()+'"';
                }
                if($("[name='input_name']").val()!=""){
                    search_title+=',"'+$("[name='input_name']").val().trim()+'"';
                }
            }
            
            $(".search-title b.search-name").html(search_title);
            $(".search-title b.search-amount").html(locations.length);
            $(".search-title").show();
            $(".search-title-loading").hide();
            $(".loding-icon").hide();
            $("[name='input_name']").val("");

        }else{
            notify("Error please try again.","danger");
        }
    }).fail(function(jqXHR, textStatus){
        if(textStatus == 'timeout'){
            notify("Server Response Timeout!","danger");
        }
        else{
            notify("Error please try again.","danger");
        }
    }).always(function() {
        submit_running = false;
    });
  }







  $(document).ready(function(){
    
    $(document).on("click",".list-item",function(){
        $(".list-item").removeClass("active");
        $(this).addClass("active");

        var handle = $(this);
        $.each( locations, function( key, value ) {

            if(locations[key].id == handle.data('id')){

                if(focus_markers.handle){
                    focus_markers.handle.setIcon(focus_markers.svg);
                }
        
                //Clear Old Focus
                var svg_old = locations[key].svg;
                var url_old = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg_old);
                focus_markers.handle = markers[key];
                focus_markers.svg = url_old;

                var svg = locations[key].svg_focus;
                var url = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg);
                markers[key].setIcon(url);
                map.panTo(new google.maps.LatLng(locations[key].lat, locations[key].long));
                map.setZoom(15);
                return false; 
            }
        });
    });

    $(document).on("click",".search-item-result",function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $("[name='input_name']").val(name);
        $("[name='input_garage_prov']").val("").change();
        selectGarage(id);
        $(".result-box").hide();
    });

    

    $("select[name='input_garage_prov']" ).change(function(){
        var id = $(this).val();
        if(id!=null && id!=""){
            $.ajax({
                url: $("[name='hd_get_data_district']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':id
                },
                dataType: "json",
                timeout:10000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value="">อำเภอ</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+i+'">'+data+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_garage_district']").html(select);
                    if($("[name='selected_district']").val()!=null && $("[name='selected_district']").val()!=""){
                        $("select[name='input_garage_district']").val($("[name='selected_district']").val()).change();
                        $("#butmit_search").click();
                        $("[name='selected_district']").val(null);
                    }
                }else{
                    notify("Error please try again.","danger");
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify("Error please try again.","danger");
                }
            })
        }else{
            select = '<option selected="selected" value="">อำเภอ</option>';
            $("select[name='input_garage_district']").html(select);
        }
    });

    $("input[name='input_name']").keyup(function(){
        if($(this).val().length > 4){
                $.ajax({
                    url: $("[name='hd_get_data_garage']").val(),
                    type: "post",
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content'),
                        'search_text':$(this).val().trim()
                    },
                    dataType: "json",
                    timeout:10000
                }).done(function( result ) {
                    if(!result.resp_error){
                        if(result.length>0){
                            
                            var html = "";
                            $.each(result, function(i,data){
                                html+='<li class="search-item-result" data-id="'+data.id+'" data-name="'+data.name+'"><h3>'+data.name+'</h3><p><small>'+data.address+'</small></p></li>';
                            });
                            $(".result-box").find("ul").html(html);
                            $(".result-box").show();
                        }   
                    }
                });
        }else{
            $(".result-box").hide();
        }
    });


    $("[name='input_name']").keypress(function(e) {
        if(e.which == 13) {
            e.preventDefault();
            $("#butmit_search").click();
        }
    });

    $("#butmit_search").click(function(){
        if(!submit_running){
            submit_running = true;
            findGarage();
        }
    });


    if($("[name='selected_province']").val()!=null && $("[name='selected_province']").val()!=""){
        $("[name='input_garage_prov']").val($("[name='selected_province']").val()).change();
    }

    $(".nav-link").removeClass("active");
    $(".nav-link[data-target='target-garage']").addClass("active");


    
    
    
});
