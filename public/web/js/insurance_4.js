$(document).ready(function(){
    var _is_main = $("[name='input_is_main_driver']").val()==1?true:false;
    var _is_otp = false;
    var _is_otp_agree = false;
    var otp_store = {};
    // var _resend_counter = 10;
    // var _resend_limit = 2;
    // var _resend_handle = null;
    var _submit_counter = 5;
    var _submit_handle = null;
    var _enable_otp = false;
    var _counting_handle = null;
    var _driver1_min_age = null;
    var _driver1_max_age = null;
    var is_resend_click = false;

    var _otp_state = {
        isCheckEnable: false, //false //OTP CHANGE
        isCheckRound: false, //false //OTP CHANGE
        isRunning: false,
        isContinue: false,
        round: 1,
        watingTime: 60,
    }

    function AllowOnlyNumbers(e) {

        e = (e) ? e : window.event;
        var key = null;
        var charsKeys = [
            97, // a  Ctrl + a Select All
            65, // A Ctrl + A Select All
            99, // c Ctrl + c Copy
            67, // C Ctrl + C Copy
            118, // v Ctrl + v paste
            86, // V Ctrl + V paste
            115, // s Ctrl + s save
            83, // S Ctrl + S save
            112, // p Ctrl + p print
            80 // P Ctrl + P print
        ];
    
        var specialKeys = [
        8, // backspace
        9, // tab
        27, // escape
        13, // enter
        35, // Home & shiftKey +  #
        36, // End & shiftKey + $
        37, // left arrow &  shiftKey + %
        39, //right arrow & '
        46, // delete & .
        45 //Ins &  -
        ];
    
        key = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    
        //console.log("e.charCode: " + e.charCode + ", " + "e.which: " + e.which + ", " + "e.keyCode: " + e.keyCode);
        //console.log(String.fromCharCode(key));
    
        // check if pressed key is not number 
        if (key && key < 48 || key > 57) {
    
            //Allow: Ctrl + char for action save, print, copy, ...etc
            if ((e.ctrlKey && charsKeys.indexOf(key) != -1) ||
                //Fix Issue: f1 : f12 Or Ctrl + f1 : f12, in Firefox browser
                (navigator.userAgent.indexOf("Firefox") != -1 && ((e.ctrlKey && e.keyCode && e.keyCode > 0 && key >= 112 && key <= 123) || (e.keyCode && e.keyCode > 0 && key && key >= 112 && key <= 123)))) {
                return true
            }
                // Allow: Special Keys
            else if (specialKeys.indexOf(key) != -1) {
                //Fix Issue: right arrow & Delete & ins in FireFox
                if ((key == 39 || key == 45 || key == 46)) {
                    return (navigator.userAgent.indexOf("Firefox") != -1 && e.keyCode != undefined && e.keyCode > 0);
                }
                    //DisAllow : "#" & "$" & "%"
                else if (e.shiftKey && (key == 35 || key == 36 || key == 37)) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    }

    function thaiToEnglishDate(thaiDate){
        var monthNamesThai = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน",
        "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
        var fullDate = thaiDate.split(" ");
        var day = fullDate[0];
        var month = monthNamesThai.indexOf(fullDate[1]) + 1;
        var year = parseInt(fullDate[2]) - 543;
        return year+"-"+month+"-"+day;
    }

    function clearInitForm(){
        $("[name='input_info_province']").change();
        $("[name='input_doc_id']").val("");
        $("[name='input_doc_id_val']").val("");
        // $("[name='input_doc_car_licence']").val("");
        // $("[name='input_doc_car_licence_val']").val("");
        // $("[name='input_doc_licence1']").val("");
        // $("[name='input_doc_licence1_val']").val("");
        // $("[name='input_doc_licence2']").val("");
        // $("[name='input_doc_licence2_val']").val("");
        // $("[name='input_doc_cctv_outside']").val("");
        // $("[name='input_doc_cctv_outside_val']").val("");
        // $("[name='input_doc_cctv_inside']").val("");
        // $("[name='input_doc_cctv_inside_val']").val("");

        if($("[name='input_info_gender']").val()!="" &&  $("[name='input_info_gender']").val()!=null){
            var datafor = $("[name='input_info_gender']").attr("name");
            var val = $("[name='input_info_gender']").val();
            $(".select-radio[data-for='"+datafor+"'][data-value='"+val+"'").addClass("active");
        }
        // if($("[name='input_driver1_gender']").length>0){
        //     if($("[name='input_driver1_gender']").val()!="" &&  $("[name='input_driver1_gender']").val()!=null){
        //         var datafor = $("[name='input_driver1_gender']").attr("id");
        //         var val = $("[name='input_driver1_gender']").val();
        //         $(".select-radio[data-for='"+datafor+"'][data-value='"+val+"'").addClass("active");
        //     }
        // }
        // if($("[name='input_driver2_gender']").length>0){
        //     if($("[name='input_driver2_gender']").val()!="" &&  $("[name='input_driver2_gender']").val()!=null){
        //         var datafor = $("[name='input_driver2_gender']").attr("id");
        //         var val = $("[name='input_driver2_gender']").val();
        //         $(".select-radio[data-for='"+datafor+"'][data-value='"+val+"'").addClass("active");
        //     }
        // }
    }

    function validateWebService(callback, handle){
        //Disabled Button
        handle.addClass("loading");
        handle.attr("disabled","disabled");
        $("#loading").fadeIn("fast");
        //---------------
        $.ajax({
            url: $("[name='hd_check_webservice']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'firstname': $("[name='input_info_name']").val(),
                'lastname': $("[name='input_info_lastname']").val(),
                'idcard': $("[name='input_info_id']").val(),
                'chassis': $("[name='input_info_car_number']").val()
            },
            dataType: "json",
            timeout:30000
        }).done(function(result){
            if(!result.resp_error){
                if(result.status == false){
                    //Disabled Button
                    handle.removeClass("loading");
                    handle.removeAttr("disabled");
                    $("#loading").fadeOut("fast");
                    //---------------
                    openModal("แจ้งให้ทราบ",result.message);
                    
                }else{
                    callback();
                }
            }else{
                notify(result.resp_error,"danger");
                //Disabled Button
                handle.removeClass("loading");
                handle.removeAttr("disabled");
                $("#loading").fadeOut("fast");
                //---------------
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
            //Disabled Button
            handle.removeClass("loading");
            handle.removeAttr("disabled");
            $("#loading").fadeOut("fast");
            //---------------
        });
    }

    function setDateInitial(lang){
        var today = moment();
        var start = moment(today).add(0, 'days');

        /* Main */
        var main_maxdate = moment(today).subtract(18, 'years');
        $option_main = {
            'format' : null,
            'lang'  : lang,
            weekStart : 0, 
            time: false,
            maxDate : main_maxdate,
            currentDate : main_maxdate,
        }
        if($option_main!=null){
            if(lang=='en'){
                $option_main['format'] = 'DD MMMM YYYY';
            } 
            $(".datepicker-main").bootstrapMaterialDatePicker($option_main);
        }

        // /* Driver 1 */
        // var driver1_age_value = $('input[name="driver1_age_value"]').val();
        // if(driver1_age_value!='UNNAMED' && driver1_age_value!=null && driver1_age_value!=""){
        //     var mindate_val = null;
        //     var maxdate_val = null;
        //     var mindate = null;
        //     var maxdate = null;
        //     var $option1 = null;
        //     if(driver1_age_value=="50UP"){
        //         mindate_val = _driver1_min_age = 51;
        //         _driver1_max_age = 999;
        //         maxdate = moment(today).subtract(mindate_val, 'years');
        //         $option1 = {
        //             'format' : null,
        //             'lang'  : lang,
        //             weekStart : 0, 
        //             time: false,
        //             maxDate : maxdate,
        //             currentDate : maxdate,
        //         }
        //     }else{
        //         driver1_age_value = driver1_age_value.replace('Y','');
        //         var preriod_date = driver1_age_value.split('-');
        //         mindate_val = _driver1_min_age = parseInt(preriod_date[0]);
        //         maxdate_val = _driver1_max_age = parseInt(preriod_date[1]);
        //         mindate = moment(today).subtract(maxdate_val, 'years');
        //         maxdate = moment(today).subtract(mindate_val, 'years');
        //         $option1 = {
        //             'format' : null,
        //             'lang'  : lang,
        //             weekStart : 0, 
        //             time: false,
        //             minDate : mindate,
        //             maxDate : maxdate,
        //             currentDate : maxdate,
        //         }
        //     }  
        //     if($option1!=null){
        //         if(lang=='en'){
        //             $option1['format'] = 'DD MMMM YYYY';
        //         } 
        //         $(".datepicker-driver1").bootstrapMaterialDatePicker($option1).on("change",function(e,date){
        //             if(_is_main){
        //                 $(".datepicker-main").val($(".datepicker-driver1").val());
        //             }
        //         });
        //     }
        // }

        // /* Driver2 */
        // var driver2_age_value = $('input[name="driver2_age_value"]').val();
        // if(driver2_age_value!='UNNAMED' && driver2_age_value!=null && driver2_age_value!=""){
        //     var mindate_val = null;
        //     var maxdate_val = null;
        //     var mindate = null;
        //     var maxdate = null;
        //     var $option2 = null;
        //     if(driver2_age_value=="50UP"){
        //         mindate_val = 50;
        //         maxdate = moment(today).subtract(mindate_val, 'years');
        //         $option2 = {
        //             'format' : null,
        //             'lang'  : lang,
        //             weekStart : 0, 
        //             time: false,
        //             maxDate : maxdate,
        //             currentDate : maxdate,
        //         }
        //     }else{
        //         driver2_age_value = driver2_age_value.replace('Y','');
        //         var preriod_date = driver2_age_value.split('-');
        //         mindate_val = parseInt(preriod_date[0]);
        //         maxdate_val = parseInt(preriod_date[1]);
        //         mindate = moment(today).subtract(maxdate_val, 'years');
        //         maxdate = moment(today).subtract(mindate_val, 'years');
        //         $option2 = {
        //             'format' : null,
        //             'lang'  : lang,
        //             weekStart : 0, 
        //             time: false,
        //             minDate : mindate,
        //             maxDate : maxdate,
        //             currentDate : maxdate,
        //         }
        //     }  
        //     if($option2!=null){
        //         if(lang=='en'){
        //             $option2['format'] = 'DD MMMM YYYY';
        //         } 
        //         $(".datepicker-driver2").bootstrapMaterialDatePicker($option2);
        //     }
        // }
    }

    function bs_input_file() {
        $(".input-file").before(
            function() {
                if ( ! $(this).prev().hasClass('input-ghost') ) {
                    var element = $("<input type='file' accept='.pdf,.jpg,.png' name='' class='input-ghost on-file-change' style='visibility:hidden; height:0'>");
                    element.attr("name",$(this).data("name"));
                    element.change(function(){
                        element.next(element).find('input').val((element.val()).split('\\').pop());
                    });
                    $(this).find("button.btn-choose").click(function(){
                        element.click();
                    });
                    $(this).find("button.btn-reset").click(function(){
                        element.val(null);
                        $(this).parents(".input-file").find('input').val('');
                    });
                    $(this).find('input').css("cursor","pointer");
                    $(this).find('input').mousedown(function() {
                        $(this).parents('.input-file').prev().click();
                        return false;
                    });
                    return element;
                }
            }
        );
    }

    function focusForm(target){
        $('html, body').animate({
            scrollTop: target.offset().top-200 
          }, 500, function() {
          });
    }

    function setMainDriver(){
        var name = $("[name='input_info_name']").val();
        var lastname = $("[name='input_info_lastname']").val();
        var idcard = $("[name='input_info_id']").val();
        var gender = $("[name='input_info_gender']").val();
        var birthdate = $("[name='input_info_birth']").val();

        $("[name='input_driver1_name']").val(name);
        $("[name='input_driver1_lastname']").val(lastname);
        $("[name='input_driver1_id']").val(idcard);
        $(".select-radio[data-group='driver1_gender'][data-value='"+gender+"']").click();
        $("[name='input_is_main_driver']").val(1);
        
        var engBirthDate = thaiToEnglishDate(birthdate);
        var years = moment().diff(engBirthDate, 'years');
        if(_driver1_min_age<=years && _driver1_max_age>=years){
            $(".datepicker-driver1").val(birthdate);
        }else{
            $("[name='input_info_birth']").val($(".datepicker-driver1").val());
            var title = "แจ้งเตือนการระบุวันเกิด";
            var periodDate = _driver1_min_age==51?"51 ปีขึ้นไป":_driver1_min_age+" - "+_driver1_max_age+" ปี";
            var detail = "กรุณาระบุวันเกิดของผู้ขับขี่หลักให้ถูกต้อง กรณีผู้ขับขี่หลักคือเจ้าของกรมธรรม์<br/> ช่วงอายุของผู้ขับขี่หลัก ที่ท่านได้ระบุไว้คือ "+periodDate;
            openModal(title,detail);
        }
    }


    function getPrefix(){
        $.ajax({
            url: $("[name='hd_get_prefix']").val(),
            type: "post",
            data: {
                'gender':$("[name='input_info_gender']").val(),
            },
            dataType: "json",
            timeout:30000
        }).done(function(result){
            if(!result.resp_error){
                var selected = $("select[name='input_info_title'] option:selected").val();
                var is_checked = false;
                select = '<option selected="selected" value disabled>เลือกคำนำหน้า</option>';
                $.each(result, function(i,data)
                {
                    if(data == selected){
                        is_checked = true;
                    }
                    select +='<option value="'+data+'">'+i+'</option>';
                });
                select += '</select>';
                $("select[name='input_info_title']").html(select);
                if($("[name='input_info_title_inf']").val()!=""){
                    $("select[name='input_info_title']").val($("[name='input_info_title_inf']").val()).change();
                    $("[name='input_info_title_inf']").val("");
                }
                if(is_checked){
                    $("select[name='input_info_title']").val(selected).change();
                }
                
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        })
    }getPrefix();
    
    bs_input_file();
    setDateInitial($("input[name='hd_current_lang']").val());
    
    $(".select2-local").select2({
        theme: "bootstrap",
        width: '100%'
    });
    
    $(".select-radio").click(function(){
        var target = $(this).data("target");
        var type = $(this).data("type");
        var group = $(this).data("group");
        var value = $(this).data("value");
        var value_for = $(this).data("for");

        // Change Value
        if(value_for!=null && value!=null){
            $("#"+value_for).val(value).change();
        }

        $(".select-radio[data-group='"+group+"']").removeClass("active");
        $(this).addClass("active");
        if(type!=null){
            if(type=="show"){
                $("#"+target).slideDown("fast");
            }else{
                $("#"+target).slideUp("fast");
            }
        }

        if(group=="info_gender"){
            getPrefix();
            $(".gender-text").removeClass("hide");
        }

    });

    $('.datepicker').bootstrapMaterialDatePicker({ weekStart : 0, time: false });

    $("select[name='input_info_province']" ).change(function(){
        var id = $(this).val();
        if(id!=null){
            $.ajax({
                url: $("[name='hd_get_district']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':id,
                    'locale':$("[name='hd_current_lang']").val()
                },
                dataType: "json",
                timeout:30000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value disabled>'+$("[name='hd_lang_district']").val()+'</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+data+'">'+i+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_info_district']").html(select);
                    if($("[name='input_info_district_inf']").val()!=""){
                        $("select[name='input_info_district']").val($("[name='input_info_district_inf']").val()).change();
                    }
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }
        
    });

    $("select[name='input_info_district']" ).change(function(){
        var id = $(this).val();
        var province_id = $("select[name='input_info_province']" ).val();
        if(id!=null && province_id!=null){
            $.ajax({
                url: $("[name='hd_get_subdistrict']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':province_id,
                    'district_id':id,
                    'locale':$("[name='hd_current_lang']").val()
                },
                dataType: "json",
                timeout:30000
            }).done(function(result){
                if(!result.resp_error){
                    select = '<option selected="selected" value disabled>'+$("[name='hd_lang_subdistrict']").val()+'</option>';
                    $.each(result, function(i,data)
                    {
                        select +='<option value="'+data+'">'+i+'</option>';
                    });
                    select += '</select>';
                    $("select[name='input_info_subdistrict']").html(select);
                    if($("[name='input_info_subdistrict_inf']").val()!=""){
                        $("select[name='input_info_subdistrict']").val($("[name='input_info_subdistrict_inf']").val()).change();
                    }
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }
    });

    $("select[name='input_info_subdistrict']" ).change(function(){
        var province_id = $("select[name='input_info_province']" ).val();
        var district_id = $("select[name='input_info_district']" ).val();
        var subdistrict_id = $(this).val();

        if(province_id!=null && district_id!=null && subdistrict_id!=null){
            $.ajax({
                url: $("[name='hd_get_postalcode']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'province_id':province_id,
                    'district_id':district_id,
                    'subdistrict_id':subdistrict_id,
                },
                dataType: "json",
                timeout:30000
            }).done(function(result){
                if(!result.resp_error){
                   $("[name='input_info_code']").val(result);
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
            })
        }
    });

    $("select[name='input_info_title']").change(function(){
        $(".gender-text").addClass("hide");
        var nowValue = $("select[name='input_info_title'] option:selected").val();
        var genderHandle = $(".select-radio[data-group='info_gender']");
        $.ajax({
            url: $("[name='hd_get_gender']").val(),
            type: "post",
            data: {
                'id': nowValue,
            },
            dataType: "json",
            timeout:30000
        }).done(function(result){
            if(result){
                if(result.gender=="MALE" || result.gender=="FEMALE"){
                    var value_for = genderHandle.data("for");
                    if(value_for!=null){
                        $("#"+value_for).val(result.gender).change();
                    }
                    genderHandle.removeClass("active");
                    $(".select-radio[data-group='info_gender'][data-value='"+result.gender+"'").addClass("active");
                }
            }
        });
    });


    $('#form_insurance').validate({
        rules: {
            'input_info_title': {
                required: true,
            },
            'input_info_name': {
                required: true,
                thaiAlpha: true
            },
            'input_info_lastname':{
                required: true,
                thaiAlpha: true
            },
            'input_info_id':{
                required: true,
                number:true,
                id_card:true
            },
            'input_info_birth':{
                required:true
            },
            'input_info_address':{
                required:true,
                thaiAddress: true
            },
            'input_info_province':{
                required:true
            },
            'input_info_district':{
                required:true
            },
            'input_info_subdistrict':{
                required:true
            },
            'input_info_code':{
                required:true,
                number:true,
            },
            'input_info_tel':{
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            },
            'input_info_email':{
                required:true,
                custom_email:true
            },
            'input_driver1_name':{
                required:true
            },
            'input_driver1_lastname':{
                required:true
            },
            'input_driver1_id':{
                required:true,
                id_card:true
            },
            'input_driver1_licence':{
                maxlength:20,
                required:true,
                number:true
            },
            'input_driver1_birth':{
                required:true
            },
            'input_driver2_name':{
                required:true
            },
            'input_driver2_lastname':{
                required:true
            },
            'input_driver2_id':{
                required:true,
                id_card:true
            },
            'input_driver2_licence':{
                maxlength:20,
                required:true,
                number:true
            },
            'input_driver2_birth':{
                required:true
            },
            'input_info_prefix':{
                required:true,
                car_prefix:true
            },
            'input_info_licence':{
                required:true,
                number:true
            },
            'input_info_car_province':{
                required:true
            },
            'input_info_car_number':{
                required:true,
                maxlength:20,
                alpha:true,
                // "remote": {
                //     url: $("[name='hd_check_chassis']").val(),
                //     type: "post",
                // }
            },
            // 'input_doc_id':{
            //     input_doc_id:true
            // },
            // 'input_doc_car_licence':{
            //     input_doc_car_licence:true
            // },
            // 'input_doc_licence1':{
            //     input_doc_licence1:true
            // },
            // 'input_doc_licence2':{
            //     input_doc_licence2:true
            // },
            // 'input_doc_cctv_outside':{
            //     input_doc_cctv_outside:true
            // },
            // 'input_doc_cctv_inside':{
            //     input_doc_cctv_inside:true
            // }
        },
        messages: {
            input_info_title: {
                required: 'กรุณาเลือกคำนำหน้า'
            },
            input_info_name: {
                required: $("[name='hd_lang_required']").val(),
                thaiAlpha: "เฉพาะตัวอักษรภาษาไทยเท่านั้น"
            },
            input_info_lastname: {
                required: $("[name='hd_lang_required']").val(),
                thaiAlpha: "เฉพาะตัวอักษรภาษาไทยเท่านั้น"
            },
            input_info_id : {
                required : $("[name='hd_lang_required']").val(),
                number : $("[name='hd_lang_number']").val(),
                id_card : $("[name='hd_lang_idcard']").val(),
            },
            input_info_birth:{
                required:$("[name='hd_lang_required']").val(),
            },
            input_info_address:{
                required: $("[name='hd_lang_required']").val(),
                thaiAddress: "เฉพาะตัวอักษรภาษาไทยและเครื่องหมายที่อนุญาต"
            },
            input_info_province:{
                required:$("[name='hd_lang_required']").val(),
            },
            input_info_district:{
                required:$("[name='hd_lang_required']").val(),
            },
            input_info_subdistrict:{
                required:$("[name='hd_lang_required']").val(),
            },
            input_info_code:{
                required:$("[name='hd_lang_required']").val(),
                number:$("[name='hd_lang_number']").val(),
            },
            input_info_tel:{
                required:$("[name='hd_lang_required']").val(),
                number:$("[name='hd_lang_number']").val(),
                minlength:$("[name='hd_lang_tel']").val(),
                maxlength:$("[name='hd_lang_tel']").val()
            },
            input_info_email:{
                required:$("[name='hd_lang_required']").val(),
                custom_email:$("[name='hd_lang_email']").val()
            },
            input_driver1_name:{
                required:$("[name='hd_lang_required']").val()
            },
            input_driver1_lastname:{
                required:$("[name='hd_lang_required']").val()
            },
            input_driver1_id:{
                required:$("[name='hd_lang_required']").val(),
                id_card:$("[name='hd_lang_idcard']").val()
            },
            input_driver1_licence:{
                required:$("[name='hd_lang_required']").val(),
                number:$("[name='hd_lang_number']").val(),
                maxlength:$("[name='hd_lang_max20']").val()
            },
            input_driver1_birth:{
                required:$("[name='hd_lang_required']").val()
            },
            input_driver2_name:{
                required:$("[name='hd_lang_required']").val()
            },
            input_driver2_lastname:{
                required:$("[name='hd_lang_required']").val()
            },
            input_driver2_id:{
                required:$("[name='hd_lang_required']").val(),
                id_card:$("[name='hd_lang_idcard']").val()
            },
            input_driver2_licence:{
                required:$("[name='hd_lang_required']").val(),
                number:$("[name='hd_lang_number']").val(),
                maxlength:$("[name='hd_lang_max20']").val()
            },
            input_driver2_birth:{
                required:$("[name='hd_lang_required']").val()
            },
            input_info_prefix:{
                required:$("[name='hd_lang_required']").val(),
                car_prefix:$("[name='hd_lang_invalid_form']").val()
            },
            input_info_licence:{
                required:$("[name='hd_lang_required']").val(),
                number:$("[name='hd_lang_number']").val(),
            },
            input_info_car_province:{
                required:$("[name='hd_lang_required']").val()
            },
            input_info_car_number:{
                required:$("[name='hd_lang_required']").val(),
                alpha:$("[name='hd_lang_alpha']").val(),
                maxlength:$("[name='hd_lang_max20']").val(),
                // remote:$("[name='hd_lang_chassis']").val()
            },
            // input_doc_id:{
            //     input_doc_id:$("[name='hd_lang_required']").val()
            // },
            input_doc_car_licence:{
                input_doc_car_licence:$("[name='hd_lang_required']").val()
            },
            input_doc_licence1:{
                input_doc_licence1:$("[name='hd_lang_required']").val()
            },
            input_doc_licence2:{
                input_doc_licence2:$("[name='hd_lang_required']").val()
            },
            input_doc_cctv_outside:{
                input_doc_cctv_outside:$("[name='hd_lang_required']").val()
            },
            input_doc_cctv_inside:{
                input_doc_cctv_inside:$("[name='hd_lang_required']").val()
            }
        },
        invalidHandler:function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {                    
                validator.errorList[0].element.focus();
            }
        }
    });

    $("#calulate_bar").find("button[type='submit']").click(function(e){
        var is_error = false;
        
        if($("[name='input_info_gender']").val()=="" || $("[name='input_info_gender']").val()==null){
            var html = '<label class="my-error">'+$("[name='hd_lang_required']").val()+'</label>';
            $(".error_info_gender").html(html);
            $(".row_focus_gender").addClass("row_focus");
            focusForm($(".row_focus_gender"));
            is_error = true;
        }else{
            $(".error_info_gender").html("");
            $(".row_focus_gender").removeClass("row_focus");
        }
        // if($("[name='input_driver1_gender']").length>0){
        //     var html ="";
        //     if($("[name='input_driver1_gender']").val()=="" || $("[name='input_driver1_gender']").val()==null){
        //         html = '<label class="my-error">'+$("[name='hd_lang_required']").val()+'</label>';
        //         $(".row_focus_gender_1").addClass("row_focus");
        //         if(!is_error){
        //             focusForm($(".row_focus_gender_1"));
        //         }
        //         is_error = true;
        //     }else{
        //         $(".row_focus_gender_1").removeClass("row_focus");
        //     }
        //     $(".error_driver1_gender").html(html);
        // }
        // if($("[name='input_driver2_gender']").length>0){
        //     var html ="";
        //     if($("[name='input_driver2_gender']").val()=="" || $("[name='input_driver2_gender']").val()==null){
        //         var html = '<label class="my-error">'+$("[name='hd_lang_required']").val()+'</label>';
        //         $(".row_focus_gender_2").addClass("row_focus");
        //         if(!is_error){
        //             focusForm($(".row_focus_gender_2"));
        //         }
        //         is_error = true;
        //     }else{
        //         $(".row_focus_gender_2").removeClass("row_focus");
        //     }
        //     $(".error_driver2_gender").html(html);
        // }
        if(!$("[name='is_agree']").is(":checked")){
            var html = '<label class="my-error">'+$("[name='hd_lang_is_agree']").val()+'</label>';
            $(".error_is_agree").html(html);
            $(".row_focus_agree").addClass("row_focus");
            if(!is_error){
                focusForm($(".row_focus_agree"));
            }
            is_error = true;
        }else{
            $(".row_focus_agree").removeClass("row_focus");
            $(".error_is_agree").html("");
        }

        if(!$("[name='is_personal']").is(":checked")){
            var html = '<label class="my-error">เฉพาะรถยนต์ใช้ส่วนบุคคลเท่านั้น</label>';
            $(".error_is_personal").html(html);
            $(".row_focus_personal").addClass("row_focus");
            if(!is_error){
                focusForm($(".row_focus_personal"));
            }
            is_error = true;
        }else{
            $(".row_focus_personal").removeClass("row_focus");
            $(".error_is_personal").html("");
        }

        if(!$("[name='is_email']").is(":checked") && !$("[name='is_post']").is(":checked") ){
            var html = '<label class="my-error">กรุณาเลือกช่องทางการรับกรมธรรม์ประกันภัย</label>';
            $(".error_is_policy_channel").html(html);
            $(".row_focus_policy_channel").addClass("row_focus");
            if(!is_error){
                focusForm($(".row_focus_policy_channel"));
            }
            is_error = true;
        }else{
            $(".row_focus_policy_channel").removeClass("row_focus");
            $(".error_is_policy_channel").html("");
        }

        if(!$("[name='is_true']").is(":checked")){
            var html = '<label class="my-error">กรุณายืนยันคำรับรอง</label>';
            $(".error_is_true").html(html);
            $(".row_focus_true").addClass("row_focus");
            if(!is_error){
                focusForm($(".row_focus_true"));
            }
            is_error = true;
        }else{
            $(".row_focus_true").removeClass("row_focus");
            $(".error_is_true").html("");
        }

        // if(_is_main){
        //     if($("[name='input_driver1_birth']").val() != $("[name='input_info_birth']").val()){
        //         is_error = true;
        //         var title = "แจ้งเตือนการระบุวันเกิด";
        //         var detail = " กรณีผู้ขับขี่หลัก คือเจ้าของกรมธรรม์ <br/>กรุณาระบุวันเกิดของผู้ขับขี่หลักให้ตรงกับข้อมูลผู้เอาประกันภัย ";
        //         $(".birth-alert").addClass("color-pulsate").show();
        //         openModal(title,detail);
        //     }
        // }


        if(is_error){
            e.preventDefault();
        }else{
            if($("#form_insurance").valid()){
                var handle = $(this);
                //Check OTP
                if(_otp_state.isCheckEnable && _otp_state.isCheckRound){
                    if(!_is_otp_agree && _enable_otp){
                        if($("[name='input_info_tel']").valid()){
                            $("#otp_modal").modal();
                            if(!_otp_state.isContinue){
                                callOTP();
                            }
                        }else{
                            $("[name='input_info_tel']").focus();
                            $("[name='is_agree']").prop('checked', false);
                        }
                        return false;
                    }else{
                        validateWebService(function(){
                            $("#form_insurance").submit();
                        },handle);
                    }
                }
                
            }
            
        }
        
    });
    
    clearInitForm();
    

    // OTP

    function secondToTime(given_seconds){
        hours = Math.floor(given_seconds / 3600);
        minutes = Math.floor((given_seconds - (hours * 3600)) / 60);
        seconds = given_seconds - (hours * 3600) - (minutes * 60);
        timeString =  minutes.toString() + ':' + seconds.toString().padStart(2, '0');
        $("#counting-down").html(timeString);
    }

    function callOTP(){
        if(!_otp_state.isRunning){
            _otp_state.isRunning = true;
            if(_otp_state.round == 1){
                requestOtp();
            }else{
                if(_submit_handle != null){
                    clearInterval(_submit_handle);
                    _submit_handle = null;
                    _submit_counter = 5;
                    $(".counter-submit").find("a").html("");
                }
                $(".input_otp").val("");
                $(".input_otp").attr("disabled","disabled");
                $("#btn-resend-otp").attr("disabled","disabled");
                $("#btn-resend-otp").removeClass("active");
                $('.counting-box').addClass("active");
                $(".sent-box").removeClass("active");
                $(".result-box").removeClass("active");
                secondToTime(_otp_state.watingTime);
                countingDownSendOTP()
            }
        }
        
    }

    function countingDownSendOTP(){
        _counting_handle = setInterval(function(){ 
            _otp_state.watingTime--;
            secondToTime(_otp_state.watingTime);
            if(_otp_state.watingTime<=0){
                $(".counting-box").removeClass("active");
                requestOtp();
                clearInterval(_counting_handle);
            }
        }, 1000);
    }

    function getOTPRoundNumber(){
        $.ajax({
            url: $("[name='hd_round_otp']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            dataType: "json",
            timeout:30000
        }).done(function(result){
            if(result.status){
               _otp_state.round = result.round;
               if(_otp_state.round > 3 ){
                _otp_state.watingTime = 300;
               }else{
                _otp_state.watingTime = 60; 
               }
            }
            _otp_state.isCheckRound = true;
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
    }getOTPRoundNumber();

    function checkOtp(){ //OTP CHANGE
        $.ajax({
            url: $("[name='hd_check_otp']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            dataType: "json",
            timeout:30000
        }).done(function(result){
            if(result.status){
               _enable_otp = true;
            }else{
                _enable_otp = false;
            }
            _otp_state.isCheckEnable = true;
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
    }checkOtp();


    function requestOtp(){
        var tel = $("[name='input_info_tel']").val();
        if($("[name='input_info_tel']").valid()){
            $(".result-box").removeClass("active");
            $(".sending-box").addClass("active");
            $(".input_otp").attr("disabled","disabled");
            $.ajax({
                url: $("[name='hd_request_otp']").val(),
                type: "post",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'input_tel':tel,
                },
                dataType: "json",
                timeout:30000
            }).done(function(result){

                if(result.status){
                    otp_store.ref = result.referenceNo;
                    _otp_state.round = _otp_state.round + 1;
                    if(_otp_state.round > 3){
                        _otp_state.watingTime = 300;
                    }else{
                        _otp_state.watingTime = 60;
                    }
                    $(".otp-to").html(tel.substring(0,tel.length-2)+"xx");
                    $(".sent-box").addClass("active");
                    $("#btn-resend-otp").addClass("active");
                    $("#btn-resend-otp").removeAttr("disabled","disabled");
                    $(".sending-box").removeClass("active");
                    $(".input_otp").removeAttr("disabled");
                    $(".input_otp[data-index='1']").focus();
                }else{
                    $(".sending-box").removeClass("active");
                    $(".result-box").find("p").html("ไม่สามารถดำเนินการได้ กรุณาติดต่อเจ้าหน้าที่");
                    $(".result-box").addClass("active");
                    
                }
            }).fail(function(jqXHR, textStatus){
                if(textStatus == 'timeout'){
                    notify("Server Response Timeout!","danger");
                }
                else{
                    notify(textStatus,"danger");
                }
                $(".sending-box").removeClass("active");
            }).always(function(){
                _otp_state.isRunning = false;
            });
        }else{
            $(".result-box").find("p").html("ข้อมูลเบอร์โทรศัพท์ไม่ถูกต้อง");
            $(".result-box").addClass("active");
        }
        
    }


    function submitOtp(input_otp,callbackSuccess,callbackError){
        $.ajax({
            url: $("[name='hd_submit_otp']").val(),
            type: "post",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'input_otp':input_otp,
                'input_ref':otp_store.ref
            },
            dataType: "json",
            timeout:30000
        }).done(function(result){
            if(result.status){
                callbackSuccess();
                setTimeout(function(){
                    validateWebService(function(){
                        $("#form_insurance").submit();
                    },$("#btn-submit"));
                },1600);
            }else{
                callbackError(result.reason);
            }
        }).fail(function(jqXHR, textStatus){
            if(textStatus == 'timeout'){
                notify("Server Response Timeout!","danger");
            }
            else{
                notify(textStatus,"danger");
            }
        });
    }

    function counterSubmit(){
        $("#btn-submit-otp").addClass("appendCounter");
        _submit_handle = setInterval(function(){ 
            _submit_counter--;
            $(".counter-submit").find("a").html(_submit_counter);
            if(_submit_counter<=0){
                _submit_counter = 5;
               
                $("#btn-submit-otp").removeClass("appendCounter");
                $(".input_otp").removeAttr("disabled");
                $(".input_otp[data-index='1']").focus();
                clearInterval(_submit_handle);
            }
        }, 1000);
    }

    $('#otp_modal').on('hidden.bs.modal', function () {
        if(_otp_state.isRunning){
            _otp_state.isContinue = true;
        }else{
            _otp_state.isContinue = false;
        }
    });
    
    $("#btn-submit-otp").click(function(){
        
        //Get Input
        var input_otp = "";
        $(".input_otp").each(function(){
            input_otp+=$(this).val();
        });
        if(input_otp.length==6){
            $(this).find(".fa").addClass("active");
            $(".result-box").removeClass("active");
            submitOtp(input_otp,function(){
                // Success Box ----------------------------
                $("#otp_modal").find(".correct-box").show();
                $(".input-box").hide();
                setTimeout(function(){
                    $("#otp_modal").find(".correct-box").find("p").addClass("active");
                    setTimeout(function(){
                        $("#otp_modal").modal('hide');
                    },1500);
                },550);
                _is_otp_agree = true;
                
                //----------------------------------------
            },function(message){
                // Error Box ----------------------------
                $(".sent-box").removeClass("active");
                $("#btn-submit-otp").removeClass("active");
                $("#btn-submit-otp").attr("disabled","disabled");
                $(".result-box").find("p").html(message);
                $(".result-box").addClass("active");
                $("#btn-submit-otp").find(".fa").removeClass("active");
                $(".input_otp").val("");
                $(".input_otp[data-index='1']").focus();
                $(".input_otp").attr("disabled","disabled");
                counterSubmit();
                //---------------------------------------
            });
        }else{
            $(".input_otp").each(function(){
                if($(this).val()==""){
                    $(this).focus();
                    return false;
                } 
            });
        }
    });

    $("#btn-resend-otp").click(function(){
        if(!is_resend_click){
            is_resend_click = true;
            var handle = $(this);
            handle.find(".fa").addClass("active");
            $("#btn-submit-otp").removeClass("active");
            $("#btn-submit-otp").attr("disabled","disabled");
            setTimeout(function(){
                handle.find(".fa").removeClass("active");
                is_resend_click = false;
                callOTP();
            },500);
            
        }
        
        
    });

    $('.input_otp').keyup(function(e){
        if(e.keyCode == 8){
            var index = $(this).data("index");
            $(".input_otp[data-index='"+(index-1)+"']").focus();
            $("#btn-submit-otp").removeClass("active");
            $("#btn-submit-otp").attr('disabled','disabled');
        }else{

            var nowValue = $(this).val();
            if(nowValue!= "" && nowValue!=null){
                var index = $(this).data("index");
                var nextFocus = parseInt(index) + 1;
                $(".input_otp[data-index='"+(nextFocus)+"']").focus();
            }


            var checkHasInput = 0;
            $(".input_otp").each(function(){
                if($(this).val()!=null && $(this).val()!=""){
                    checkHasInput++;
                }
            });
            if(checkHasInput == 6){
                $("#btn-submit-otp").addClass("active");
                $("#btn-submit-otp").removeAttr('disabled');
            }
        }
    });

    $(".input_otp").keypress(function(e){
        if(!AllowOnlyNumbers(e)){
            e.preventDefault();
            return false;
        }
    });

    $("[name='input_info_email']").keyup(function(event) {
        var value = $(this).val();
        $(this).val(value.replace(/[^a-zA-Z0-9_/-@.]/g,''));
    });

    $("#btn-rebuild").click(function(){
        window.location.href = "/";
    });


    function savePeriod(data,fileUpload,callback){
        var ajaxOptions = {
            url: $("[name='hd_save_inform']").val(),
            type: "post",
            data: data,
            dataType: "json",
            timeout:10000,
        };
        if(fileUpload){
            ajaxOptions.contentType = false;
            ajaxOptions.cache = false;
            ajaxOptions.processData = false;
        }
        $.ajax(ajaxOptions).done(function(){
            if(callback!=null){
                callback();
            }
            
        }).fail(function(jqXHR, textStatus){
            if(callback!=null){
                callback();
            }
        });
    }

    $(".on-focus-out").focusout(function(){
        if($(this).valid()){
            var obj = $("#form_insurance").serialize();
            savePeriod(obj,false,null);
        }
    });

    $(".on-change-value").change(function(){
        if($(this).valid()){
            var obj = $("#form_insurance").serialize();
            savePeriod(obj,false,null);
        }
    });

    $(document).on("change",".on-file-change",function(){
        var form = $("#form_insurance");
        $("#loading").fadeIn("fast");
        savePeriod(new FormData(form[0]),true,function(){
            $("#loading").fadeOut("fast");
            $(".box-doc").hide();
        });
    });

    $(".back-action").click(function(e){
        var form = $("#form_insurance");
        var href = $(this).attr("href");
        $("#loading").fadeIn("fast");
        savePeriod(new FormData(form[0]),true,function(){
            window.location.href = href;
        });
        e.preventDefault();
        
    });


    $("[name='input_info_prefix']").keyup(function(event) {
        var value = $(this).val();
        var regex = /^[0-9ก-ฮ]+$/;
        if(!regex.test(value)){
            $(this).val(value.replace(/[^0-9ก-ฮ]/g,''));
        }
    });



    

});