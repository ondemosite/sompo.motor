$(document).ready(function(){

    var _alertState = true;

    function calculateForm(callback){
        //console.log("calculate");
        /*******Make Data **********/
        var data = {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            'input_start_insurance':$("[name='input_start_insurance']").val(),
            'input_end_insurance':$("[name='input_end_insurance']").val(),
            'input_define_name':$("[name='input_define_name']").val(),
            'input_define_name1':$("[name='input_define_name_value1']").val(),
            'input_define_name2':$("[name='input_define_name_value2']").val(),
            'input_cctv':$("[name='input_cctv']").val(),
            'input_garage':$("[name='input_garage']").val(),
            'input_deductible':$("[name='input_deductible']").val(),
            'input_flood':$("[name='input_flood']").val(),
            // Addition
            'input_robbery':$("[name='input_robbery']").val(), // is select rob
            'input_travel':$("[name='input_travel']").val(), // is select taxi
            'input_is_hb':$("[name='input_is_hb']").val(), // is select hb
            'input_carloss':$("[name='input_carloss']").val(), // is select carloss

            'input_theft':$("[name='input_theft']").val(), // theft selected cost 
            'input_taxi':$("[name='input_taxi']").val(), // taxi selected cost
            'input_hb': $("[name='input_hb']").val(), // hb selected cost

            'input_prb':$("[name='input_prb']").val(),
            'input_start_compulsory':$("[name='input_start_compulsory']").val(),
            'input_end_compulsory':$("[name='input_end_compulsory']").val(),
            'input_promotion':$("[name='input_promotion']").val(),
            'input_info_prefix':$("[name='input_info_prefix']").val(),
            'input_info_licence':$("[name='input_info_licence']").val(),
            'input_info_car_province':$("[name='input_info_car_province']").val(),
            
            'input_personal': $("[name='input_personal']").val()
        }

        
        /**************************/
        $.ajax({
            url: $("input[name='hd_calculate_step3']").val(),
            data: data,
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
                if(result.status==true){
                    if(result.payment){
                        $("#plan_pay").html(result.payment);
                    }
                    $("#btn-calculate").removeClass("active").attr("disabled","disabled");
                    $("#btn-submit").addClass("active").removeAttr("disabled");
                    if(callback!=null){
                        callback();
                    }
                }else{
                    if(result.status == "limit"){
                        var title = "แจ้งให้ทราบ";
                        var detail = result.reason;
                        BootstrapDialog.show({
                            title: title,
                            message: detail,
                            draggable: true
                        });
                        $(".promotion-icon").removeClass("success").addClass("danger");
                        $(".promotion-icon").find(".fa").removeClass("fa-check").addClass("fa-close");
                        $("[name='input_promotion']").focus();

                    }else{
                        notify(result.reason,"danger");
                        setTimeout(function(){
                            //location.reload()
                        }, 2000);
                    }
                    
                }
            }
        }).always(function(){
            $("#calulate_bar").removeLoader();
        });
    }

    function focusForm(target){
        $('html, body').animate({
            scrollTop: target.offset().top-200 
          }, 500, function() {
          });
    }

    function validateForm(load,callback){
        $("#calulate_bar").loader();
        var result = true;
        var result_car_info = true;
        var start_insurance = $("[name='input_start_insurance']").val();
        var input_start_compulsory = $("[name='input_start_compulsory']").val();
        var input_personal = $("[name='input_personal']").val();
        var input_promotion = $("[name='input_promotion']").val();        //Start Date
        var input_info_prefix = $("[name='input_info_prefix']").val();
        var input_info_licence = $("[name='input_info_licence']").val();
        var input_info_car_province = $("[name='input_info_car_province']").val();

        if(start_insurance=="" || start_insurance==null){
            $("label[for='input_start_insurance']").show();
            $("[name='input_start_insurance']").focus();
            result = false;
        }else{
            $("label[for='input_start_insurance']").hide();
        }
        


        if(!load){
            if(input_info_car_province=="" || input_info_car_province==null){
                $("label[for='input_info_car_province']").show();
                $("[name='input_info_car_province']").focus();
                result_car_info = result = false;
            }else{
                $("label[for='input_info_car_province']").hide();
            }
            if(input_info_licence=="" || input_info_licence==null){
                $("label[for='input_info_licence']").html($("[name='hd_lang_error_specify']").val());
                $("label[for='input_info_licence']").show();
                $("[name='input_info_licence']").focus();
                result_car_info = result = false;
            }else{
                var numericReg = /^\d+$/;
                if(!numericReg.test(input_info_licence)) {
                    $("label[for='input_info_licence']").html($("[name='hd_lang_only_number']").val());
                    $("label[for='input_info_licence']").show();
                    $("[name='input_info_licence']").focus();
                    result_car_info = result = false;
                }else{
                    $("label[for='input_info_licence']").hide();
                }
            }
            if(input_info_prefix=="" || input_info_prefix==null){
                $("label[for='input_info_prefix']").show();
                $("[name='input_info_prefix']").focus();
                result_car_info = result = false;
            }else{
                var regex = /^[0-9ก-ฮ][ก-ฮ]?[ก-ฮ]?$/;
                if($.isNumeric(input_info_prefix) || !regex.test(input_info_prefix)){
                    $("label[for='input_info_prefix']").html($("[name='hd_lang_invalid_form']").val());
                    $("label[for='input_info_prefix']").show();
                    $("[name='input_info_prefix']").focus();
                    result_car_info = result = false;
                }else{
                    $("label[for='input_info_prefix']").hide();
                }
            }
            if(!result_car_info){
                $(".row_focus_car").addClass("row_focus");
            }else{
                $(".row_focus_car").removeClass("row_focus");
            }

            if(input_personal=="" || input_personal==null || input_personal==0){
                if(result){
                    focusForm($(".row_focus_personal"));
                    var title = $(".alert-carpersonal").data("title");
                    var detail = $(".alert-carpersonal").data("detail");
                    BootstrapDialog.show({
                        title: title,
                        message: detail,
                        draggable: true
                    });
                    $("label[for='input_personal']").show();
                    $(".row_focus_personal").addClass("row_focus");
                }
                result = false;
            }else{
                $("label[for='input_personal']").hide();
                $(".row_focus_personal").removeClass("row_focus");
            }
        }
        
        //PRB
        if(input_start_compulsory=="" || input_start_compulsory==null){
            $("label[for='input_start_compulsory']").show();
            $("[name='input_start_compulsory']").focus();
            result = false;
        }else{
            $("label[for='input_start_compulsory']").hide();
        }

        //Promotion Code
        if(input_promotion!="" && input_promotion!=null){
            $.ajax({
                url: $("input[name='hd_check_promotion_code']").val(),
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'code':input_promotion,
                    'coverage':$("[name='input_coverage_id']").val()
                },
                type: 'POST',
                dataType: "json",
                timeout:10000
            }).done(function(response){
                if(!response.resp_error){
                   if(response.status==false){
                       $(".promotion-icon").removeClass("success").addClass("danger");
                       $(".promotion-icon").find(".fa").removeClass("fa-check").addClass("fa-close");
                       $("[name='input_promotion']").focus();
                       if(response.message){
                           $(".promotion-alert").html(response.message);
                           $(".promotion-alert").removeClass("hide");
                       }
                       result = false;
                   }else{
                       $(".promotion-alert").addClass("hide");
                       $(".promotion-icon").removeClass("danger").addClass("success");
                       $(".promotion-icon").find(".fa").removeClass("fa-close").addClass("fa-check");
                   }
                   if(result && result_car_info){
                        calculateForm(callback);
                   }else{
                       $("#calulate_bar").removeLoader();
                   }
                }else{
                    $("#calulate_bar").removeLoader();
                }
            });
        }else{
            $(".promotion-alert").addClass("hide");
            $(".promotion-icon").removeClass("success danger");
            $(".promotion-icon").find(".fa").removeClass("fa-check fa-close");
            if(result!=false){
                calculateForm(callback);
            }else{
                $("#calulate_bar").removeLoader();
            }       
        }
    }

    function checkPreOrder(callback){
        $.ajax({
            url: $("input[name='hd_check_max_pre']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'start_date': $("[name='input_start_insurance']").val()
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               if(result.status == "SUCCESS"){
                    callback();
               }else{
                   openModal("แจ้งให้ทราบ",result.message);
               }
            }
        });
    }


    function setDateInitial(lang){
        var addDay = 1;
        var today = moment();
        var start = moment(today).add(addDay, 'days');
        var maxPre = $("[name='hd_max_pre']").val();
        var maximum = null;
        if(maxPre != 0){
           maximum = moment(today).add(maxPre, 'days');
        }
        
        // Carlendar Insurace Start
        $options = {
            'format' : null,
            'lang'  : lang,
            weekStart : 0, 
            time: false,
            currentDate : start,
            minDate : start,
            maxDate : maximum,
        }
        if(lang=='en') $options['format'] = 'DD MMMM YYYY';
        $("[name='input_start_insurance']").bootstrapMaterialDatePicker($options).on('change', function(e, date){
            if(lang=='th'){
                $current_year_th = parseInt(date.format('YYYY'))+543;
                $year_th = parseInt(date.add(1,'years').format('YYYY'))+543;
                $("[name='input_start_compulsory']").val(date.format('DD MMMM')+" "+$current_year_th);

                $("[name='input_end_insurance']").val(date.add(1,'years').format('DD MMMM')+" "+$year_th );
                $("[name='input_end_compulsory']").val(date.add(1,'years').format('DD MMMM')+" "+$year_th );
            }else{
                $("[name='input_start_compulsory']").val(date.format('DD MMMM YYYY'));
                
                $("[name='input_end_insurance']").val(date.add(1,'years').format('DD MMMM YYYY'));
                $("[name='input_end_compulsory']").val(date.add(1,'years').format('DD MMMM YYYY'));
            }
        });
        
        if($("[name='input_end_insurance']").val()=="" || $("[name='input_end_insurance']").val()==null){
            if(lang=='th'){
                $year_th = parseInt(moment(today).add(1,'years').format('YYYY'))+543;
                $("[name='input_end_insurance']").val(moment(today).add(addDay, 'days').add(1,'years').format('DD MMMM')+" "+$year_th);
                $("[name='input_end_compulsory']").val(moment(today).add(addDay, 'days').add(1,'years').format('DD MMMM')+" "+$year_th);
            }else{
                $("[name='input_end_insurance']").val(moment(today).add(addDay, 'days').add(1,'years').format('DD MMMM YYYY'));
                $("[name='input_end_compulsory']").val(moment(today).add(addDay, 'days').add(1,'years').format('DD MMMM YYYY'));
            }
        }
        
        
        // Calendar Compulsory
        $("[name='input_start_compulsory']").val($("[name='input_start_insurance']").val());
        $("[name='input_end_compulsory']").val($("[name='input_end_insurance']").val());
        
        
    }

    function initialCarloss(){
        var ft_si = $("[name='input_ft_si']").val();
        var define_name = $("[name='input_define_name']").val();
        var age1 = $("[name='input_define_name_value1']").val();
        var age2 = $("[name='input_define_name_value2']").val();
        var cctv = $("[name='input_cctv']").val();

        $.ajax({
            url: $("input[name='hd_input_api_carloss']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'ft_si':ft_si,
                'define_name':define_name,
                'age1':age1,
                'age2':age2,
                'cctv':cctv
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               $(".specify-carloss").html(result);
            }else{
                console.log(result.resp_error);
            }
        }); 
    }

    function initialHb(){
        var si = $("[name='input_hb']").val();
        var define_name = $("[name='input_define_name']").val();
        var age1 = $("[name='input_define_name_value1']").val();
        var age2 = $("[name='input_define_name_value2']").val();
        var cctv = $("[name='input_cctv']").val();

        $.ajax({
            url: $("input[name='hd_input_api_hb']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'si':si,
                'define_name':define_name,
                'age1':age1,
                'age2':age2,
                'cctv':cctv
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               $(".specify-hb").html(result);
               validateForm(true,null);
            }else{
                console.log(result.resp_error);
            }
        }); 
    }

    function initialTheft(){
        var si = $("[name='input_theft']").val();
        var define_name = $("[name='input_define_name']").val();
        var age1 = $("[name='input_define_name_value1']").val();
        var age2 = $("[name='input_define_name_value2']").val();
        var cctv = $("[name='input_cctv']").val();

        $.ajax({
            url: $("input[name='hd_input_api_theft']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'si':si,
                'define_name':define_name,
                'age1':age1,
                'age2':age2,
                'cctv':cctv
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               $(".specify-theft").html(result);
               validateForm(true,null);
            }else{
                console.log(result.resp_error);
            }
        }); 
    }

    function initialTaxi(){
        var si = $("[name='input_taxi']").val();
        var define_name = $("[name='input_define_name']").val();
        var age1 = $("[name='input_define_name_value1']").val();
        var age2 = $("[name='input_define_name_value2']").val();
        var cctv = $("[name='input_cctv']").val();

        $.ajax({
            url: $("input[name='hd_input_api_taxi']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'si':si,
                'define_name':define_name,
                'age1':age1,
                'age2':age2,
                'cctv':cctv
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               $(".specify-taxi").html(result);
               validateForm(true,null);
            }else{
                console.log(result.resp_error);
            }
        }); 
    }

    function initialFlood(){
        var ft_si = $("[name='input_ft_si']").val();
        var define_name = $("[name='input_define_name']").val();
        var age1 = $("[name='input_define_name_value1']").val();
        var age2 = $("[name='input_define_name_value2']").val();
        var cctv = $("[name='input_cctv']").val();
        var garage = $("[name='input_garage']").val();
        var deduct = $("[name='input_deductible']").val();


        $.ajax({
            url: $("input[name='hd_input_api_flood']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'ft_si':ft_si,
                'define_name':define_name,
                'age1':age1,
                'age2':age2,
                'cctv':cctv,
                'garage':garage,
                'deduct':deduct,
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               $(".specify-flood").html(result);
               validateForm(true,null);
            }else{
                console.log(result.resp_error);
            }
        }); 
    }


    $(".flood-change").change(function(){
        initialFlood();
    });
    $(".hb-change").change(function(){
        initialHb();
    });
    $(".theft-change").change(function(){
        initialTheft();
    });
    $(".taxi-change").change(function(){
        initialTaxi();
    });

    $(".carloss-change").change(function(){
        initialCarloss();
    });
    

    $(".select2-local").select2({
        theme: "bootstrap",
        width: '100%'
    });


    $(".select-radio").click(function(){
        var target = $(this).data("target");
        var type = $(this).data("type");
        var group = $(this).data("group");
        var value = $(this).data("value");
        var value_for = $(this).data("for");

        if($(this).hasClass("disable")){
            var text = $(this).data("disable");
            BootstrapDialog.show({
                title: "แจ้งให้ทราบ",
                message: text,
                draggable: true
            });
            return false;
        }

        // Change Value
        if(value_for!=null && value!=null){
            $("#"+value_for).val(value).change();
        }

        $(".select-radio[data-group='"+group+"']").removeClass("active");
        $(this).addClass("active");
        if(type!=null){
            if(type=="show"){
                $("#"+target).slideDown("fast");
            }else{
                $("#"+target).slideUp("fast");
            }
        }

        //Check Garage Dealer
        if(group=="garage" && value=="DEALER"){

            var enable_garage = $("[name='input_enable_garage_dealer']").val();
            if(enable_garage==0){
                $(".select-radio[data-group='"+group+"']").removeClass("active");
                $(".select-radio[data-group='"+group+"'][data-value='GENERAL'").addClass("active");
                $("[name='input_garage']").val("GENERAL");
                var title = $("[name='hd_lang_alert']").val();
                var detail = $("[name='hd_lang_garage']").val();
                
                BootstrapDialog.show({
                    title: title,
                    message: detail,
                    draggable: true
                });
            }
        }

        
    });

    $("[name='input_theft']").change(function(){
        var value = $(this).val();
        $.ajax({
            url: $("input[name='hd_input_api_theft']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'theft':value
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               $(".specify-theft").html(result[0]);
            }
        });
    });

    $("[name='input_taxi']").change(function(){
        var value = $(this).val();
        $.ajax({
            url: $("input[name='hd_input_api_taxi']").val(),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'taxi':value
            },
            type: 'POST',
            dataType: "json",
            timeout:10000
        }).done(function(result){
            if(!result.resp_error){
               $(".specify-travel").html(result[0]);
            }
        });
    });

    $(".detect-change").change(function(){
        $("#btn-calculate").addClass("active").removeAttr("disabled");
        $("#btn-submit").removeClass("active").attr("disabled","disabled");
        validateForm(true,null);
    });

    $("#btn-calculate").click(function(){
        if(!$(this).attr('disabled') && !$(this).prop('disabled'))
        {
            validateForm(false,null);
        }
    });

    $("[name='input_ft_si']").change(function(){
        var text = $("[name='input_ft_si'] option:selected").text();
        $("#plan_price").html(text)
    });

    $("#btn-submit").click(function(e){
        checkPreOrder(function(){
            validateForm(false,function(){
                $("#form_insurance").submit();
            });
        });
        e.preventDefault();
    });

    $(".select-radio[data-group='driver']").click(function(){
        var value = $(this).data("value");
        if(value==1){
            $("select[name='input_deductible']").val("2000").change();
            $("select[name='input_deductible']").removeAttr("disabled");
        }else{
            _alertState = false;
            $("select[name='input_deductible']").val("0").change();
            $("select[name='input_deductible']").attr("disabled","disabled");
        }
    });

    $("select[name='input_deductible']").change(function(){
        if($(this).val()==0){
            if(_alertState){
                var title = $("[name='hd_lang_alert']").val();
                var detail = $("[name='hd_lang_excess_detai']").val();
                BootstrapDialog.show({
                    title: title,
                    message: detail,
                    draggable: true
                });
            }
            _alertState = true;
        }
    });

    $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".promotion-icon").click(function(){
        if($(this).hasClass("danger")){
            $("input[name='input_promotion']").val("").change();
            $(".promotion-alert").addClass("hide");
        }
    });

    $("[name='input_info_prefix']").keyup(function(event) {
        var value = $(this).val();
        var regex = /^[0-9ก-ฮ]+$/;
        if(!regex.test(value)){
            $(this).val(value.replace(/[^0-9ก-ฮ]/g,''));
        }
    });

    setDateInitial($("input[name='hd_current_lang']").val());

    //Set Calculate Default
    validateForm(true,null);



    

});