$(document).ready(function(){
    var isSubmit = false;

    $("#btn-submit").click(function(e){
        if(!isSubmit){
            if(!$("[name='is_agree']").is(":checked")){
                var html = '<label class="my-error">กรุณาคลิกยอมรับข้อตกลงก่อนทำการสมัครประกัน</label>';
                $(".error_is_agree").html(html);
                $(".row-accept").addClass("row_focus");
                focusForm($(".row-accept"));
            }else{
                $(".row-accept").removeClass("row_focus");
                $(".error_is_agree").html("");
                isSubmit = true;
                $(this).addClass("loading disable");
                $(this).attr("disabled","disabled");
                $("#form_insurance").submit();
            }
        }else{
            console.log("double submit");
        }
        e.preventDefault();
    });

    $("[name='is_agree']").change(function(){
        if($("[name='is_agree']").is(":checked")){
            $("#btn-submit").addClass("active");
        }else{
            $("#btn-submit").removeClass("active");
        }
    });

    function focusForm(target){
        $('html, body').animate({
            scrollTop: target.offset().top-200 
          }, 500, function() {
          });
    }


    function bs_input_file() {
        $(".input-file").before(
            function() {
                if ( ! $(this).prev().hasClass('input-ghost') ) {
                    var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                    element.attr("name",$(this).attr("name"));
                    element.change(function(){
                        element.next(element).find('input').val((element.val()).split('\\').pop());
                    });
                    $(this).find("button.btn-choose").click(function(){
                        element.click();
                    });
                    $(this).find("button.btn-reset").click(function(){
                        element.val(null);
                        $(this).parents(".input-file").find('input').val('');
                    });
                    $(this).find('input').css("cursor","pointer");
                    $(this).find('input').mousedown(function() {
                        $(this).parents('.input-file').prev().click();
                        return false;
                    });
                    return element;
                }
            }
        );
    }
    
    $(function() {
        bs_input_file();
    });
    

});