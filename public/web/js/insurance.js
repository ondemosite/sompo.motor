
    var _now_target = null;

    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
  
        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            // Replace image with new SVG
            $img.replaceWith($svg);
          }, 'xml');
    });

    function openModal(title,detail,img){
        $(".wrap-popup-alert").find("[data-detail]").html(detail);
        $(".wrap-popup-alert").find("[data-title]").html(title);
        $(".wrap-popup-alert").find("[data-img]").attr("src",img);
        $(".wrap-popup-alert").fadeIn("fast");
        $(".box-popup-alert").addClass("bounceIn");
    }


    $("a.show-detail").click(function(){
        $(this).addClass("hidden").removeClass("show");
        $("a.hidden-detail").removeClass("hidden").addClass("show");
        $("#calulate_bar").addClass("collapsed");
    });

    $("a.hidden-detail").click(function(){
        $(this).addClass("hidden").removeClass("show");
        $("a.show-detail").removeClass("hidden").addClass("show");
        $("#calulate_bar").removeClass("collapsed");
    });


    $(".button-close-popup-alert").click(function(e){
        $(".wrap-popup-alert").fadeOut("fast");
        $(".box-popup-alert").removeClass("bounceIn");
        $(".wrap-popup-alert").find(["data-detail"]).html("");
        $(".wrap-popup-alert").find(["data-title"]).html("");
        $(".wrap-popup-alert").find("[data-img]").attr("src","");
        e.preventDefault();
    });

    $(".help-modal").click(function(e){
        var title = $(this).data("title");
        var detail = $(this).data("detail");
        var img = $(this).data("img");
        openModal(title,detail);
        e.preventDefault();
    });