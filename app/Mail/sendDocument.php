<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Policy;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Library\insuranceManager;

class sendDocument extends Mailable
{
    use Queueable, SerializesModels;
    public $policy;
    private $endorse;
    private $files;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Policy $policy,$files)
    {
        $this->policy = $policy;
        $this->endorse = $policy->endorse()->first();
        $this->files = $files;
        $this->insuranceManager = new insuranceManager();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
        $driver_info = $this->endorse->owner()->first();

        $policy_doc_path = $this->endorse->policy_document_path;
        $original_invoice_path = $this->endorse->original_invoice_path;
        $invoice_copy_path = $this->endorse->invoice_copy_path;
        $receipt_copy_path = $this->endorse->invoice_copy_path;
        $misc_policy_document_path = !empty($this->endorse->misc_policy_document_path)?$this->endorse->misc_policy_document_path:null;
        $misc_original_invoice_path = !empty($this->endorse->misc_original_invoice_path)?$this->endorse->misc_original_invoice_path:null;
        $misc_invoice_copy_path = !empty($this->endorse->misc_invoice_copy_path)?$this->endorse->misc_invoice_copy_path:null;
        $misc_receipt_copy_path = !empty($this->endorse->misc_receipt_copy_path)?$this->endorse->misc_receipt_copy_path:null;
        
        // Init Data
        $init = [
            'policy_number' => $this->policy->policy_number,
            'password' => $this->generatePassword()
        ];
        $obj = $this->view('emails.policy')->to($driver_info->email)->with(['init' => $init]);

        if($this->checkSelectedFile($this->files,"policy")){
            if(!empty($policy_doc_path)){
                $obj->attach($directory_path."/".$policy_doc_path,[
                    'as' => $this->insuranceManager->generatePDFFileName("Ori",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            }
        }
        if($this->checkSelectedFile($this->files,"policy_ori_invoice")){
            if(!empty($original_invoice_path)){
                $obj->attach($directory_path."/".$original_invoice_path,[
                    'as' => $this->insuranceManager->generatePDFFileName("RecOri",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            }
        }
        if($this->checkSelectedFile($this->files,"policy_copy_invoice")){
            if(!empty($invoice_copy_path)){
                $obj->attach($directory_path."/".$invoice_copy_path,[
                    'as' => $this->insuranceManager->generatePDFFileName("InvCopy",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            }
        }
        if($this->checkSelectedFile($this->files,"policy_copy_receipt")){
            if(!empty($receipt_copy_path)){
                $obj->attach($directory_path."/".$receipt_copy_path,[
                    'as' => $this->insuranceManager->generatePDFFileName("RecCopy",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            }
        }
        if($this->checkSelectedFile($this->files,"misc")){
            if(!empty($misc_policy_document_path)){
                $obj->attach($directory_path."/".$misc_policy_document_path,[
                    'as' => $this->insuranceManager->generatePDFFileNameAddon("Ori",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            }
        }
        if($this->checkSelectedFile($this->files,"misc_ori_invoice")){
            if(!empty($misc_original_invoice_path)){
                $obj->attach($directory_path."/".$misc_original_invoice_path,[
                    'as' => $this->insuranceManager->generatePDFFileNameAddon("RecOri",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            }
        }
        if($this->checkSelectedFile($this->files,"misc_copy_invoice")){
            if(!empty($misc_invoice_copy_path)){
                $obj->attach($directory_path."/".$misc_invoice_copy_path,[
                    'as' => $this->insuranceManager->generatePDFFileNameAddon("InvCopy",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            }
        }
        if($this->checkSelectedFile($this->files,"misc_copy_receipt")){
            if(!empty($misc_receipt_copy_path)){
                $obj->attach($directory_path."/".$misc_receipt_copy_path,[
                    'as' => $this->insuranceManager->generatePDFFileNameAddon("RecCopy",$this->policy,false),
                    'mime' => 'application/pdf'
                ]);
            } 
        }

        return $obj;
    }

    private function generatePassword(){
        $policy_name = $this->endorse->owner()->first();
        $birthdate = $policy_name->birth_date;
        $month = date("m",strtotime($birthdate));
        $day = date("d",strtotime($birthdate));
        return $day.$month;
    }


    private function checkSelectedFile($files,$name){
        if(is_array($files)){
            if(in_array($name,$files)) return true;
        }else{
            if($files=="all") return true;
        }
        return false;
    }

}
