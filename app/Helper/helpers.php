<?php

use App\Models\DatabaseActive;
use App\Models\TransactionLog;

function getMaskupImage($page = null){
    if($page=="garage"){
        return asset('images/web/maskup/garage.jpg');
    }else{
        return asset('images/web/logo/og-image.jpg');
    }
    
}

function has_permission($menu_path, $permission_type = array("view")) {
    try{
        $privilege_id = Auth::user('admin')->privilege;
        if($privilege_id==1) return true; //Admin
        if ($menu = DB::table('menus')->where('path', '=', strtoupper($menu_path))->select('id')->first()) {
            $menu_id = $menu->id;
            $sql = DB::table('permission')->select('id')
                            ->where('privilege_id','=',$privilege_id)
                            ->where('menu_id','=',$menu_id);
            foreach($permission_type as $key => $value){
                $sql->where($permission_type[$key],'=',1);
            }
            $permission = $sql->count();
                            
            if($permission>0) return true;
            else return false;
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return false;
    }
}

function getMenuName($path){
    $data = DB::table('menus')->select("name")->where('path','=',$path)->first();
    return $data->name;
}

function post_url($url, $fields_string, $header = null) {
    //open connection
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

    if (!empty($header)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }
    //execute post
    $result = curl_exec($ch); //close connection
    curl_close($ch);
    return $result;
}

function post_xml($url, $xml_data) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function post_get($url, $header = null) {
    //open connection
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    if (!empty($header)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }
    //execute post
    $result = curl_exec($ch); //close connection
    curl_close($ch);
    return $result;
}

function array_to_xml(array $arr, \SimpleXMLElement $xml) {
    foreach ($arr as $k => $v) {
        is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    }
    return $xml;
}

function format_phone($phone)
{
    $phoneNumber = preg_replace('/[^0-9]/','',$phone);
    if(strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }
    return $phoneNumber;
}

function convertdate_web2db($web_date, $sep = '/', $lang = 'en') {
    $dd = explode($sep, $web_date);

    if (count($dd) == 3) {
        return ($lang == 'th' ? $dd[2] - 543 : $dd[2]) . '-' . $dd[1] . '-' . $dd[0];
    }
    return null;
}

function correctDate($string){
    $lang = null;
    $month_index = null;
    $th_month = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
    $en_month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    $explode = explode(" ",trim($string));
    $day = trim($explode[0]);
    $month = trim($explode[1]);
    $year = trim($explode[2]);
    //Array Search
    foreach($th_month as $index => $find_month){
        if(strcmp($find_month,$month) == 0){
            $month_index = $index;
            $lang = 'th';
            break;
        }
    }
    foreach($en_month as $index => $find_month){
        if(strcmp($find_month,$month) == 0){
            $month_index = $index;
            $lang = 'en';
            break;
        }
    }

    if($month_index!=null || $month_index==0){
        if($lang=='th') return (intval($year)-543)."-".(str_pad($month_index+1,2, "0", STR_PAD_LEFT))."-".(str_pad($day,2, "0", STR_PAD_LEFT));
        if($lang=='en') return (intval($year))."-".(str_pad($month_index+1,2, "0", STR_PAD_LEFT))."-".(str_pad($day,2, "0", STR_PAD_LEFT));
    }else{
        return $string;
    } 
}

function getLocaleMonth($number){
    if(!empty($number)){
        $locale = \App::getLocale();
        $th_month = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
        $en_month = ['January','February','March','April','May','June','July ','August','September','October','November','December'];
        if($locale=='en'){
            return $en_month[$number-1];
        }else{
            return $th_month[$number-1];
        }
      
    }else{
        return null;
    }
}

function getLocaleDate($string, $active_time=false){
    if(!empty($string)){
        $string = date('Y-m-d H:i',strtotime($string));
        $locale = \App::getLocale();
        $th_month = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
        $en_month = ['January','February','March','April','May','June','July ','August','September','October','November','December'];
        $explode = explode("-",trim($string));
        $year = trim($explode[0]);
        $month = trim($explode[1]);
        $day = trim($explode[2]);
        $time = null;
        $day_explode = explode(" ",trim($day));
        if(is_array($day_explode)){
            $day = $day_explode[0];
            if(!empty($day_explode[1])){
                $time = $day_explode[1];
            }   
        }
        
        $buff_year = null;
        $buff_month = null;
        $buff_day = null;
        if($locale=='en'){
            $buff_year = $year;
            $buff_month = $en_month[intval($month)-1];
            $buff_day = $day;
        }else{
            $buff_year = intval($year)+543;
            $buff_month = $th_month[intval($month)-1];
            $buff_day = $day;
        }
        if(!empty($time) && $active_time){
            return $buff_day." ".$buff_month." ".$buff_year." ".$time;
        }else{
            return $buff_day." ".$buff_month." ".$buff_year;
        }
    }else{
        return $string;
    }
    
}

function logTransaction($orderNo, $description, $type){
    TransactionLog::create([
        'order_no' => $orderNo,
        'descriptions' => $description,
        'type' => $type
    ]);
}

function getAge($string){
    $dtz = new DateTimeZone("Asia/Bangkok");
    $d1 = new DateTime(); 
    $d1->setTimezone($dtz);
    $d2 = new DateTime($string,$dtz);
    $diff = $d2->diff($d1);
    return $diff->y;
}


function getUniqueFilename($directory,$file_extension)
{
    if(!empty($directory) && !empty($file_extension))
    {
        // getting unique file name
        $file_name = uniqid().".".$file_extension;

        while(file_exists($directory."/".$file_name)) {
            $file_name = uniqid().".".$file_extension;
        }

        return $file_name;
    } // ends for is_array check
    else
    {
        return false;
    } // else ends
} // ends

function getDatabaseActive($variable = null,$clear = false){
    if($clear){
        session()->forget('database_active');
    }
    $databaseActive = session('database_active');
    if(empty($databaseActive)){
        //Set Database Active
        $databaseActiveData = DatabaseActive::where('id',1)->first();
        
        $databaseActive = [
            'vehicle' => $databaseActiveData->vehicle_active,
            'pricing' => $databaseActiveData->pricing_active
        ];
        session(['database_active' => $databaseActive]);
    }
    if($variable == "vehicle") return $databaseActive['vehicle'];
    else if($variable == "pricing") return $databaseActive['pricing'];
    else return $databaseActive;
    

}

?>