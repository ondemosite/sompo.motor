<?php

class Menu {

    public static function genMenu(){
        try{
            $privilege_id = Auth::user('admin')->privilege;
            $permission_list = DB::table('permission')->where('privilege_id','=',$privilege_id)->orderBy('id')->get();
            $menu_list = DB::table('menus')->orderBy('orders','asc')->get()->toArray();
            if(!empty($menu_list)){
                $html = "";
                foreach ($menu_list as $key => $value) {
                    if(Menu::has_permission($permission_list,$menu_list[$key]->id)){
                        if($menu_list[$key]->parent_id=='0' && $menu_list[$key]->isshow==1){
                            $owner_active = false;
                            $url_active = false;
                            $has_submenu = false;

                            if(Menu::hasSubMenu($menu_list,$menu_list[$key]->id)){
                                if(empty($menu_list[$key]->path)){
                                    $has_submenu = true;
                                }
                            }
                            
                            if(Menu::isSubMenuPath($menu_list,$menu_list[$key]->id)){ // if submenu URL Active
                                $url_active = true;
                            }
                            
                            $html .= '<li class="px-nav-item '.Menu::activeMenu($menu_list[$key]).' '.($has_submenu?'px-nav-dropdown':'').' '.($url_active?'active px-open':'').'">';
                            $html .= '<a href="'.url(!empty($menu_list[$key]->path)?$menu_list[$key]->path:'').'"><i class="px-nav-icon fa '.(!empty($menu_list[$key]->icon)?$menu_list[$key]->icon:'').'"></i><span class="px-nav-label">'.(App::isLocale('en')?$menu_list[$key]->name:$menu_list[$key]->name_th).'    </span></a>';

                            if($has_submenu){
                                $html .= '<ul class="px-nav-dropdown-menu">' ;
                                $html.=Menu::findSubMenu($permission_list,$privilege_id,$menu_list,$menu_list[$key]->id);
                                $html.='</ul>';         
                            }else{
                                $html .='</li>';
                            }
                        }
                    }
                }
                return $html;
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public static function genBreadcrumb(){
        $html = "";
        $currentUrl = Request::path();
        $subMenu = DB::table('menus')->where('path',$currentUrl)->first();
        if(isset($subMenu->parent_id)){
            if($subMenu->parent_id==0){
                $html = '<h1 class="pull-xs-left"><span class="text-muted font-weight-light"><i class="page-header-icon fa '.(!empty($subMenu->icon)?$subMenu->icon:'').'"></i>'.(App::isLocale('en')?$subMenu->name:$subMenu->name_th).'</span></h1>';
            }else{
                if($subMenu->level==1){
                    $parentMenu = DB::table('menus')->where('id',$subMenu->parent_id)->first();
                    if(isset($parentMenu->name)){
                        $html = '<h1 class="pull-xs-left"><span class="text-muted font-weight-light"><i class="page-header-icon fa '.(!empty($parentMenu->icon)?$parentMenu->icon:'').'"></i>'.(App::isLocale('en')?$parentMenu->name:$parentMenu->name_th).' / </span>'.(App::isLocale('en')?$subMenu->name:$subMenu->name_th).'</h1>';
                    }
                }else if($subMenu->level==2){
                    $html = "";
                    $parentMenu = DB::table('menus')->where('id',$subMenu->parent_id)->first();
                    $rootMenu = DB::table('menus')->where('id',$parentMenu->parent_id)->first();
                    if(isset($rootMenu->name)){
                        $html = '<h1 class="pull-xs-left"><span class="text-muted font-weight-light"><i class="page-header-icon fa '.(!empty($rootMenu->icon)?$rootMenu->icon:'').'"></i>'.(App::isLocale('en')?$rootMenu->name:$rootMenu->name_th).' / </span>';
                        if(isset($parentMenu->name)){
                            $html .='<span class="text-muted font-weight-light">'.(App::isLocale('en')?$parentMenu->name:$parentMenu->name_th).' / </span>'.(App::isLocale('en')?$subMenu->name:$subMenu->name_th);
                        }
                        $html .= '</h1>';
                    }
                }
                
            }
        }
        return $html;
    }

    public static function isSubMenuPath($menu,$parent_id){
        foreach ($menu as $key => $value) {
            if($menu[$key]->parent_id==$parent_id){ 
                if(Request::is($menu[$key]->path) || Request::is($menu[$key]->path."/*")){
                    return true;
                }else{
                    if(Menu::isSubMenuPath($menu,$menu[$key]->id)){ // if submenu URL Active
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static function hasSubMenu($menu,$parent_id){
        foreach ($menu as $key => $value) {
            if($menu[$key]->parent_id==$parent_id){
                return true;
            }
        }
        return false;
    }

    public static function findSubMenu($permission_list,$privilege_id,$menu,$parent_id){
        $html = "";
        foreach ($menu as $key => $value) {
            if($menu[$key]->parent_id==$parent_id && $menu[$key]->isshow==1){
                if(Menu::has_permission($permission_list,$menu[$key]->id)){
                    $active = '';
                    if(Request::is($menu[$key]->path) || Request::is($menu[$key]->path."/*")){
                        $active = 'active';
                    }
                    
                    $has_submenu = false;
                    if(Menu::hasSubMenu($menu,$menu[$key]->id)){
                        $has_submenu = true;
                    }

                    $url_active = false;
                    if(Menu::isSubMenuPath($menu,$menu[$key]->id)){ // if submenu URL Active
                        $url_active = true;
                    }

                    $url = "";
                    if(!empty($menu[$key]->path)){
                        $url = url($menu[$key]->path);
                    }

                    $html .= '<li class="px-nav-item '.$active.' '.($has_submenu?'px-nav-dropdown':'').' '.($url_active?'active px-open':'').'"><a href="'.$url.'"><span class="px-nav-label">'.(App::isLocale('en')?$menu[$key]->name:$menu[$key]->name_th).'</span></a>';
                    
                    if($has_submenu){
                        $html.= '<ul class="px-nav-dropdown-menu">' ;
                        $html.= Menu::findSubMenu($permission_list,$privilege_id,$menu,$menu[$key]->id);
                        $html.='</ul>';         
                    }else{
                        $html .='</li>';
                    }
                }
            }
        }
        return $html;
    }

    public static function has_permission($permission_list,$menu_id) {
        if($permission_list){
            foreach($permission_list as $key => $value){
                if($permission_list[$key]->menu_id==$menu_id){
                    if($permission_list[$key]->view == 1) return true; 
                }
            }
        }
        return false;
    }

    public static function activeMenu($menu) {
        $active = '';
        if(Request::is($menu->path) || Request::is($menu->path."/*")){
            $active = 'active';
        }
        return $active;

    }

    


}

?>