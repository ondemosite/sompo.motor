<?php

namespace App\Http\Middleware;

use Closure;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $ip = $request->ip();
        echo $ip;
        if($ip!='203.150.31.50'){
          throw new \Exception("Error Processing Request", 1);

        }else{
          return $next($request);
        }
    }
}
