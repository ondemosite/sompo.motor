<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AccessFiles;
use Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Library\AccessFile;
use Illuminate\Http\Response;

class FileAccessController extends Controller
{
  public function __construct(){

  }

  public function get($params){
    try{
      if(!empty($params)){
        $accessFile = new AccessFile();
        if($accessFile->checkAccess($params)){
            
            //Check File Exits
            $stringDecode = base64_decode($params);
            $explode = explode(",",$stringDecode);
            if(is_array($explode)){
                if(sizeof($explode) == 4){
                    $token = $explode[0];
                    $refId = $explode[1];
                    $fileName = $explode[2];
                    $disk = $explode[3];
                    if(Storage::disk($disk)->exists($fileName)){
                        $fileContent = Storage::disk($disk)->get($fileName);
                        $mimeType = Storage::disk($disk)->mimeType($fileName);
                        $response = response()->make($fileContent);
                        $response->header('Content-Type', $mimeType);
                        return $response;
                    }
                }
            }
        }
      }
      return view('notfound');

    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }



  


}
