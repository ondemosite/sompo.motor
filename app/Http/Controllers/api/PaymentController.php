<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Library\insuranceManager;
use App\Library\insurancePayment;
use App\Library\Mpasskit\Mpasskit;
use App\Library\SMS;
use App\Library\Endorse;
use App\Library\MyEmail;
use App\Models\Order;
use App\Models\Policy;
use App\Models\ApiLog;
use App\Models\PaymentResponseLog;
use App\Models\ResendDocumentLog;
use App\Models\SmsLog;
use Response;

class PaymentController extends Controller
{

  public function __construct(){
      $this->insurance_manager = new insuranceManager();  
      $this->insurance_payment = new insurancePayment();
  }

  public function index(){   
    
  }

  public function payment_back_response(Request $request){
      $input = $request->all();
      //Log::info($input);
      if(!empty($input)){
            //Create Payment Log
            $this->createPaymentLog($input);
            if($this->insurance_payment->checkHash($input)){
                $order_number = $input['order_id'];
                $order = Order::where('order_number',$order_number)->first();
                if(!empty($order)){
                    logTransaction($order->order_number,"ได้รับการตอบกลับจาก 2c2p","SUCCESS");
                    $payment = $order->payment()->first();
                    if(intval($input['payment_status'])==0){
                        //Update Order ----
                        $order->status = "PAID";
                        $order->save();
                        //Update Promotion Remaining
                        if(!empty($order->promotion_id)){
                            logTransaction($order->order_number,"Update Promotion Remaining","SUCCESS");
                            $promotion = $order->promotion()->first();
                            if(!empty($promotion)){
                                $promotion->receive_amount = $promotion->receive_amount + 1;
                                $promotion->save();
                            }
                        }

                        //Update Payment
                        $payment->status = "PAID";
                        $payment->status_detail = "ชำระเงินแล้ว";
                        $payment->paid_date = date("Y-m-d H:i:s");
                        $payment->save();
                        logTransaction($order->order_number,"Update Payment Status: PAID","SUCCESS");
                        //Create Policy
                        $policy_id = $this->createPolicy($order);
                        
                        $policy = Policy::where('id',$policy_id)->first();
                        if(!empty($policy)){
                            //Update Payment 
                            $payment->policy_number = $policy->policy_number;
                            $payment->save();
                          
                            //$paidDate = date("Y-m-d",strtotime($payment->paid_date));
                            //$timePaid = strtotime($paidDate);

                            //Update Policy Start Date
                            // $startDate = date("Y-m-d",strtotime($policy->insurance_start));
                            // $timeStart = strtotime($startDate);
                            // if($timePaid == $timeStart){
                            //     $policy->insurance_start = date("Y-m-d",strtotime($payment->paid_date))." 00:01:00";
                            // }else{
                            $newDate = date("Y-m-d",strtotime($policy->insurance_start));
                            $policy->insurance_start = $newDate." 00:01:00";
                            //}
                            $policy->insurance_expire = date('Y-m-d',strtotime("+1 year",strtotime($policy->insurance_start)))." 16:30:00";

                            //Update Compulsory Start Date
                            if(!empty($policy->policy_com_number)){
                                // $startDate = date("Y-m-d",strtotime($policy->compulsory_start));
                                // $timeStart = strtotime($startDate);
    
                                // if($timePaid == $timeStart){
                                //     $policy->compulsory_start = date("Y-m-d",strtotime($payment->paid_date))." 00:01:00";
                                // }else{
                                $newDate = date("Y-m-d",strtotime($policy->compulsory_start));
                                $policy->compulsory_start = $newDate." 00:01:00";
                                //}
                                $policy->compulsory_expire = date('Y-m-d',strtotime("+1 year",strtotime($policy->compulsory_start)))." 16:30:00";
                            }
                            $policy->save();
                            //------------------------------

                            //Create Receipt
                            if($policy_id!=false){
                                logTransaction($order->order_number,"ดำเนินการสร้างใบเสร็จ","SUCCESS");
                                $id = $this->createReceipt($order,$policy,$payment,$input);
                                if($id){
                                    logTransaction($order->order_number,"สร้างใบเสร็จสำเร็จ","SUCCESS");
                                }else{
                                    logTransaction($order->order_number,"สร้างใบเสร็จล้มเหลว","FAIL");
                                }
                            }
                            
                            //generatePDF
                            $this->insurance_manager->generatePDF($policy);
                            
                            //generateEpass
                            // $epass_link = $this->generateEpass($policy);
                            $epass_link = null;
                            
                            //Send SMS 
                            $this->sendSMS($policy,$epass_link);

                            //Send Email
                            $this->sendEmail($policy);
                            
                            //Send to Endrose
                            $this->sendEndorse($policy);

                        }else{
                            logTransaction($order->order_number,"ไม่พบ กรมธรรม์","FAIL");
                        }
                        
                    }else{
                        if($input['payment_status']=='003'){
                            $payment->status = "CANCELED";
                            $order->status = "CANCEL";
                        }else{
                            $payment->status = "FAILED";
                            $order->status = "FAILED";
                        }
                        $payment->status_detail = $input['channel_response_desc'];
                        $payment->save();
                        $order->save();
                        logTransaction($order->order_number,"การชำระเงินไม่สำเร็จ CODE: ".$input['payment_status']." DETAIL:".$input['channel_response_desc'],"FAIL");
                    }
                }
            }else{
                Log::info("check hash faild");
            }
      }else{
          Log::info("Empty 2c2p Data-----");
      }
  }

  public function payment_front_response(Request $request){
    $input = $request->all();
    if(!empty($input)){
        if($this->insurance_payment->checkHash($input)){
            if(intval($input['payment_status'])==0){
                //Thank Page
                $order_number = $input['order_id'];
                $order = Order::where('order_number',$order_number)->first();
                if(!empty($order)){
                    return redirect()->route('web.insurance_done')->with( ['done_order' => $order->order_number] );
                }else{
                    $code = "004";
                    return redirect()->route('web.insurance_fail')->with( ['fail_code' => $code] );
                }
            }else{
                $code = "003";
                return redirect()->route('web.insurance_fail')->with( ['fail_code' => $code] );
            }
        }else{
            $code = "002";
            return redirect()->route('web.insurance_fail')->with( ['fail_code' => $code] );
        }
    }else{
        $code = "001";
        return redirect()->route('web.insurance_fail')->with( ['fail_code' => $code] );
    }
  }


  private function createPolicy($order){
        return $this->insurance_manager->createPolicy($order);
  }

  private function createReceipt($order,$policy,$payment,$input){
        $receipt_data = [
            'receipt_number' => $policy->tax_note_number,
            'order_number' => $order->order_number,
            'payment_number' => $payment->payment_no,
            'policy_number' => $policy->policy_number,
            'amount' => floatval($input['amount'])/100,
            'payment_channel_code' => $input['payment_channel'],
            'credit_number' => $input['masked_pan'],
            'payment_scheme' => $input['payment_scheme'],
        ];
        return $this->insurance_manager->createReceipt($receipt_data);
  }

  public function testPayment(){
    $id = "HQ-OD-2018-03-0000004"; //get ID from response
    //Set Log Call API
    $log = [
        'order_number' => $id,
        'response_data' => "ASDSDSADSA"
    ];
    ApiLog::create($log);

    $order = Order::where("order_number",$id)->where("status","WAITING")->first();
    if(!empty($order)){
        //Create Policy
        $policy_id = $this->insurance_manager->createPolicy($order->order_number);
        if($policy_id!=false){
            //$order->status = "PAID";
            //$order->save();
            $this->insurance_manager->generatePDF($policy_id);
            //Create Receipt
            $policy = Policy::where('id',$policy_id)->first();
            $receipt_data = [
                'receipt_number' => $policy->tax_note_number,
                'order_id' => $order->id,
                'policy_id' => $policy->id,
            ];
            $this->insurance_manager->createReceipt($receipt_data);

            //PDF Encrypt

            //Send PDF Email

            //SMS PASSWORD
        }
    }
  }

  private function sendEmail($policy){
    $mail = new MyEmail();
    return $mail->sendEmail($policy,null);
  }

  private function generateEpass($policy){
    $mk = new Mpasskit();
    return $mk->createPass($policy);
  }

  private function createPaymentLog($input){
        $data = [
            'order_number' => !empty($input['order_id'])?$input['order_id']:null,
            'amount' => !empty($input['amount'])?(floatval($input['amount'])/100):null,
            'transaction_ref' => !empty($input['transaction_ref'])?$input['transaction_ref']:null,
            'approval_code' => !empty($input['approval_code'])?$input['approval_code']:null,
            'eci' => !empty($input['eci'])?$input['eci']:null,
            'payment_channel' => !empty($input['payment_channel'])?$input['payment_channel']:null,
            'payment_status' => !empty($input['payment_status'])?$input['payment_status']:null,
            'channel_response_code' => !empty($input['channel_response_code'])?$input['channel_response_code']:null,
            'channel_response_desc' => !empty($input['channel_response_desc'])?$input['channel_response_desc']:null,
            'masked_pan' => !empty($input['masked_pan'])?$input['masked_pan']:null,
            'payment_scheme' => !empty($input['payment_scheme'])?$input['payment_scheme']:null,
            'process_by' => !empty($input['process_by'])?$input['process_by']:null,
            'browser_info' => !empty($input['browser_info'])?$input['browser_info']:null,
            'request_at' => !empty($input['request_timestamp'])?$input['request_timestamp']:null,
            'response_at' => !empty($input['transaction_datetime'])?$input['transaction_datetime']:null,
        ];
        return PaymentResponseLog::create($data)->id;
  }

  private function sendSMS($policy,$epass_link){
    $sms = new SMS();
    return $sms->sendSMS($policy,$epass_link);
  }

  private function sendEndorse($policy){
    $endorse = new Endorse();
    if($endorse->request($policy)!=false){
        $policy->update_core_status = 1;
        $policy->save();
    }else{
        //$endorse->alertToAdmin($policy_id);
    }
    return true;
  }




  
}
