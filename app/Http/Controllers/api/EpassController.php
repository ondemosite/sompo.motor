<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Policy;
use App\Models\EpassToken;
use App\Library\insuranceManager;

use Response;

class EpassController extends Controller
{

  public function __construct(){

  }

  public function downloadDocument(Request $request){
    try{
        $input = $request->all();
        if(isset($input['token'])){
            $decrypted = $this->decryptToken($input['token']);
            if($decrypted!=false){
                $result = json_decode($decrypted,true);
                if(isset($result['type']) && isset($result['token']) && isset($result['policy_id'])){
                    $epass = EpassToken::where([
                        'policy_id' => $result['policy_id'],
                        'token' => $result['token']
                    ])->first();
                    if(!empty($epass)){
                        $policy = Policy::where('id',$result['policy_id'])->first();
                        if(!empty($policy)){
                            $endorse = $policy->endorse()->first();
                            if(!empty($endorse)){
                                $path = "";
                                if($result['type']=="motor"){
                                    $path = $endorse->policy_document_path;
                                }else if($result['type']=="misc"){
                                    $path = $endorse->misc_policy_document_path;
                                }
                                if(!empty($path)){
                                    $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
                                    $headers = array(
                                        'Content-Type: application/pdf',
                                    );
                                    $im = new insuranceManager();
                                    $filename = "";
                                    if($result['type']=="motor"){
                                        $filename = $im->generatePDFFileName("Ori",$policy,false);
                                    }else{
                                        $filename = $im->generatePDFFileNameAddon("Ori",$policy,false);
                                    }
                                    
                                    return response()->download($directory_path.$path,$filename,$headers);
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return null;
    }
  }

  private function decryptToken($string){
    try {
        $decrypted = decrypt($string);
        return $decrypted;
    } catch (DecryptException $e) {
        false;
    }
  }
  
}
