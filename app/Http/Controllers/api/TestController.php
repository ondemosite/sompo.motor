<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Order;
use App\Models\Policy;
use App\Models\EpassToken;
use App\Library\insuranceManager;
use Response;

class TestController extends Controller
{

  public function __construct(){
    $this->insurance_manager = new insuranceManager();
  }

  public function createPolicy(Request $request){
    try{
            Log::info("- request");
            $input = $request->all();
            $order_id = $input['order_id'];
            $order = Order::where('id',$order_id)->first();
            if(!empty($order)){
                //Update Order ----
                $order->status = "PAID";
                $order->save();
                //Create Policy Log
                //Log::info("--create-policy --");
                $policy_id = $this->createPolicyData($order->order_number);
                //Log::info("--create-policy success--");
                //Create Receipt Log
                if($policy_id!=false){
                    //Log::info("--create-receipt--");
                    //$this->createReceipt($order,$policy_id,$input);
                    //Log::info("--create-receipt success--");
                }
                
                // //generatePDF
                // Log::info("-- Generate PDF --");
                // $this->insurance_manager->generatePDF($policy_id);
                // Log::info("-- Generate PDF SUCCESS --");

                // //generateEpass
                // Log::info("-- Generate E-pass --");
                // //$epass_link = $this->generateEpass($policy_id);
                // Log::info("-- Generate E-pass SUCCESS --");

                // //Send SMS 
                // Log::info("-- Send SMS --");
                // if(!empty($epass_link)){
                //     //$this->sendSMS($policy_id,$epass_link);
                //     Log::info("SMS PASSESS:".$epass_link);
                // }
                // Log::info("-- Send SMS SUCCESS --");

                // //Send to Endrose
                // Log::info("-- Sending Endorse --");
                // //$this->sendEndorse($policy_id);
                // Log::info("-- Send Endorse Success --");

                // //Send Email
                // Log::info("-- Sending Email --");
                // $this->sendEmail($policy_id);
                // Log::info("-- Send Email Success --");

                echo json_encode(["status"=>'success']);

            }   
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}


private function createPolicyData($order_number){
    return $this->insurance_manager->createPolicy($order_number);
}



  
}
