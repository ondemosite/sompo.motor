<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;

class ConfigController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param UserRepository  $users
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }
    
    /**
     * SMS
     *
     * @param  -
     * @return -
     */
    public function switchLang($lang) {
        Session::put('language', $lang); 
	    return redirect()->back();
    }


}
