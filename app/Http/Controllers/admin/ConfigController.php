<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Configs;
use Validator;
use DateTime;
use Illuminate\Support\Facades\Log;
use Auth;

class ConfigController extends Controller
{

    private $menu1 = "motoradmins/configs/configs";

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.configs.configs');
    }



    public function getList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $query = Configs::All();
                return Datatables::of($query)->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->title.'"><i class="fa fa-trash"></i></a>';
                })->editColumn('start_date',function($data){
                    return date('Y-m-d',strtotime($data->start_date));
                })->editColumn('expire_date',function($data){
                    return date('Y-m-d',strtotime($data->expire_date));
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getData(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $response = Configs::where('id',$input['id'])->first();
            if(!empty($response)){
                    $response->start_date = date('Y-m-d',strtotime($response->start_date));
                    $response->expire_date = date('Y-m-d',strtotime($response->expire_date));
                return json_encode($response);
            }

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
        }
    }


  public function saveData(Request $request){
      try{
          if($request->ajax()){
            
            if(!has_permission($this->menu1,["view","edit","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $input = $request->all();
            Log::info($input);
            
            // Validate 
            $validator = Validator::make($input, [
                'config_code'=>'required',
                'config_value'=>'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'title' => !empty($input['title'])?$input['title']:'',
                'config_code'=> $input['config_code'],
                'config_value'=> $input['config_value'],
                'updated_by' => Auth::guard('admin')->user()->id,
                'status' => isset($input['status'])?1:0,
                'start_date' => !empty($input['start_date'])?$input['start_date']:null,
                'expire_date' => !empty($input['expire_date'])?$input['expire_date']:null
            );
            if(!empty($input['id'])){ //Edit mode
                Configs::where('id', '=', $input['id'])->update($data);
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                Configs::create($data);
            }
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function deleteData(Request $request){
      try{
          
          if(!has_permission($this->menu1,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){

            $input = $request->all();
            $validator = Validator::make($input, [
              'id' => 'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            Configs::where('id', '=', $input['id'])->delete();
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function setStatus(Request $request){
    try{
        if(!has_permission($this->menu1,array("view","edit"))){
            return json_encode(array("resp_error"=>trans('common.permission_denied')));
        }
        if($request->ajax()){
          $input = $request->all();
          
          // Validate 
          $validator = Validator::make($input, [
              'id' => 'required',
              'type' => 'required|numeric'
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(array("resp_error"=>$validate_first[0]));
          }
          $data = array(
              'status'=>$input['type'],
          );

          Configs::where('id', '=', $input['id'])->update($data);
          return json_encode(array("status"=>"success"));
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }
 
}
