<?php

namespace App\Http\Controllers\admin;

use App\Repositories\ReportRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use App\Models\Policy;
use Validator;
use Auth;
use Datetime;
use PDF;
use Excel;

class ReportController extends Controller
{
    private $menu1 = "motoradmins/report/policy";
    private $menu2 = "motoradmins/report/policy_owner";
    private $menu3 = "motoradmins/report/order";


    public function __construct(ReportRepository $report){
        $this->middleware('auth:admin');
        $this->report = $report;
    }


    public function listPolicy(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.report.policy');

    }

    public function getPolicyList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $report = $this->report->getPolicyList($input);

                return Datatables::of($report)
                ->editColumn('vehicle_brand',function($data){
                    return json_encode([
                        'brand' => $data->brand,
                        'model' => $data->model,
                        'year' => $data->year,
                        'car_licence' => $data->car_licence
                    ]);
                })->editColumn('insurance_start',function($data){
                    return getLocaleDate($data->insurance_start,true);
                })->editColumn('insurance_expire',function($data){
                    return getLocaleDate($data->insurance_expire,true);
                })->editColumn('name',function($data){
                    return $data->name." ".$data->lastname;
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function pdfPolicy($policy_id){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        ini_set('memory_limit', '-1');
        header('Content-Type: text/html; charset=UTF-8');
        $policy = Policy::where("id",$policy_id)->first();
        if(!empty($policy)){
            if($policy->insurance_plan_id!=3){
                $pdf = PDF::loadView('admin.report.pdf.mt-schedule_2',['init'=>$policy])->setPaper('A4');
                return $pdf->stream();
            }else{
                $pdf = PDF::loadView('admin.report.pdf.mt-schedule_3',['init'=>$policy])->setPaper('A4');
                return $pdf->stream();
            }
        }else{
            echo "Not Found Data";
        }
    }

    public function excelPolicy(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return view('admin.common.permission_denied');
            }

            $input = $request->all();
            $validator = Validator::make($input, [
                'item_per_page'=>'required',
                'page_number'=>'required'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $result = $this->report->getPolicyList($input);
            $start = ($input['page_number'] - 1) * $input['item_per_page'];
            if(!empty($result)){
                $header = [
                    'topic' => "Report Policy",
                    "date" => getLocaleDate(date('Y-m-d H:i:s'),true),
                    "page" =>  $input['page_number'] . '/' . $input['total_page'],
                    'start' => $start
                ];
                
                Excel::create('report_policy_'.date('Y.m.d'), function($excel) use($result,$input,$header){
                    $excel->sheet('Sheet1', function($sheet) use($result,$input,$header) {
                        $sheet->loadView('admin.report.excel.policy',["data"=>$result,"input"=>$input,'header'=>$header]);
                    });
                })->export('xls');
            }else{
                return back()->withErrors([
                    "error" => 'Error Data Not Found'
                ])->withInput();
            }
        }catch (\Exception $e) {
            Log::info($e->getMessage());
            return back()->withErrors([
                "error" => 'Error Please Contact Administrator'
            ])->withInput();
        }
    }

    public function testExport(){
        
        $data = array();
        $data[] = array("Report Campaign");
        $data[] = array("Export Date", date('d/m/Y H:i:s'));
        $data[] = array("Page");
        $data[] = array('NO',"test1","test2");

        Excel::create('report_campaign_'.date('Y.m.d'), function($excel) use($data) {
            $excel->sheet('Sheet1', function($sheet) use($data) {
                $sheet->loadView('admin.report.excel.test');
            });
        })->export('xls');
    }

    /****************** Owner Report *********************/
    public function listOwner(){
        if(!has_permission($this->menu2)){
            return view('admin.common.permission_denied');
        }
        return view('admin.report.owner');
    }

    public function getOwnerList(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $report = $this->report->getOwnerList($input);
                return Datatables::of($report)->editColumn('name',function($data){
                    return $data->name." ".$data->lastname;
                })->editColumn('car_licence',function($data){
                    return $data->car_licence." ".$data->car_province;
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function excelOwner(Request $request){
        if(!has_permission($this->menu2)){
            return view('admin.common.permission_denied');
        }
        ini_set('memory_limit', '-1');
        header('Content-Type: text/html; charset=UTF-8');

        $input = $request->all();
        $validator = Validator::make($input, [
            'item_per_page'=>'required',
            'page_number'=>'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $result = $this->report->getOwnerList($input);
        $start = ($input['page_number'] - 1) * $input['item_per_page'];
        if(!empty($result)){
            $header = [
                'topic' => "Report Policy Owner Data",
                "date" => getLocaleDate(date('Y-m-d H:i:s'),true),
                "page" =>  $input['page_number'] . '/' . $input['total_page'],
                'start' => $start
            ];

            Excel::create('report_owner_'.date('Y.m.d'), function($excel) use($result,$input,$header){
                $excel->sheet('Sheet1', function($sheet) use($result,$input,$header) {
                    $sheet->loadView('admin.report.excel.owner',["data"=>$result,"input"=>$input,'header'=>$header]);
                });
            })->export('xls');

        }else{
            return back()->withErrors([
                "error" => 'Error Data Not Found'
            ])->withInput();
        }
    }

    /**************** Order Report **************/
    public function listOrder(){
        if(!has_permission($this->menu3)){
            return view('admin.common.permission_denied');
        }
        return view('admin.report.order');
    }

    public function getOrderList(Request $request){
        try{
            if(!has_permission($this->menu3)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $report = $this->report->getOrderList($input);
                //Log::info($report->toArray());
                return Datatables::of($report)->editColumn('name',function($data){
                    return $data->name." ".$data->lastname;
                })->editColumn('car_licence',function($data){
                    return $data->car_licence." ".$data->car_province;
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->editColumn('brand',function($data){
                    return $data->brand." ".$data->model." ".$data->year;
                })->addColumn('is_compulsory', function ($data) {
                    if(!empty($data->compulsory_net_premium)){
                        return 'yes';
                    }else{
                        return 'no';
                    }
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function excelOrder(Request $request){
        if(!has_permission($this->menu3)){
            return view('admin.common.permission_denied');
        }
        ini_set('memory_limit', '-1');
        header('Content-Type: text/html; charset=UTF-8');

        $input = $request->all();
        $validator = Validator::make($input, [
            'item_per_page'=>'required',
            'page_number'=>'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $result = $this->report->getOrderList($input);
        $start = ($input['page_number'] - 1) * $input['item_per_page'];
        if(!empty($result)){
            $header = [
                'topic' => "Report Order",
                "date" => getLocaleDate(date('Y-m-d H:i:s'),true),
                "page" =>  $input['page_number'] . '/' . $input['total_page'],
                'start' => $start
            ];

            Excel::create('report_order_'.date('Y.m.d'), function($excel) use($result,$input,$header){
                $excel->sheet('Sheet1', function($sheet) use($result,$input,$header) {
                    $sheet->loadView('admin.report.excel.order',["data"=>$result,"input"=>$input,'header'=>$header]);
                });
            })->export('xls');

        }else{
            return back()->withErrors([
                "error" => 'Error Data Not Found'
            ])->withInput();
        }
    }
 
}
