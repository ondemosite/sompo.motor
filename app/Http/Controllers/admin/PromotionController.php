<?php

namespace App\Http\Controllers\admin;

use App\Repositories\PromotionRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Promotion;
use App\Models\PromotionRegister;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;
use App\Models\Coverage;

class PromotionController extends Controller
{

  private $menu1 = "motoradmins/promotion";
  private $menu2 = "motoradmins/promotion/form";


  public function __construct(){
    $this->middleware('auth:admin');
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    return view('admin.promotion.promotion');
  }




/* ********************** Promotion ****************** */
public function getPromotion(Request $request){
    try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            
            $input = $request->all();
            $repository = new PromotionRepository();
            $data_resp = $repository->list_promotion($input);

            return Datatables::of($data_resp)->addColumn('action', function ($data) {
                return '<a href="'.route('admin.promotion_form').'/'.$data->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->title.'"><i class="fa fa-trash"></i></a>';
            })->addColumn('type', function ($data) {
                return "PERCENTAGE";
            })->editColumn('start_at', function ($data) {
                return date('Y-m-d',strtotime($data->start_at));
            })->editColumn('expire_at', function ($data) {
                return date('Y-m-d',strtotime($data->expire_at));
            })->make(true);
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}

public function getRegister(Request $request){
    try{
        if(!has_permission($this->menu2)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            
            $input = $request->all();
            $data_resp = PromotionRegister::where('promotion_id',$input['promotion_id'])->get();
            return Datatables::of($data_resp)->editColumn('name', function ($data) {
                return $data->name." ".$data->lastname;
            })->editColumn('created_at',function($data){
                return getLocaleDate($data->created_at,true);
            })->make(true);
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}

public function savePromotion(Request $request){
    try{
          if(!has_permission($this->menu2,["view","edit","add"])){
              return view('admin.common.permission_denied');
          }

          $input = $request->all();
          // Validate 
          $filter_validate = [
              'title'=>'required',
              'code'=>'required',
              'discount'=>'required|numeric',
              'maximum_grant'=>'required|numeric',
              'coverage_id'=>'required'
          ];

          $validator = Validator::make($input,$filter_validate);
          if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
          }          

          $data = array(
            'title'=>$input['title'],
            'code'=>$input['code'],
            'discount'=>$input['discount'],
            'start_at'=>!empty($input['start_at'])?$input['start_at']:null,
            'expire_at'=>!empty($input['expire_at'])?$input['expire_at']:null,
            'coverage_id'=>implode(",",$input['coverage_id']),
            'vehicle_brand'=>!empty($input['vehicle_brand'])?implode(",",$input['vehicle_brand']):null,
            'vehicle_model'=>!empty($input['vehicle_model'])?implode(",",$input['vehicle_model']):null,
            'status'=>!empty($input['status'])?1:0,
            'maximum_grant'=>!empty($input['maximum_grant'])?$input['maximum_grant']:0,
            'updated_by' => Auth::guard('admin')->user()->id,
            'ending_message' => !empty($input['ending_message'])?$input['ending_message']:'',
          );

          if(!empty($input['promotion_id'])){ //Edit mode
            Promotion::where('id', '=', $input['promotion_id'])->update($data);
            return redirect(route('admin.promotion_form')."/".$input['promotion_id'])->with('status', 'Updated Promotion Success!!');
          }else{ //Add mode
            $data['created_by'] = Auth::guard('admin')->user()->id;
            $data['formular'] = uniqid();
            $id = Promotion::create($data)->id;
            if(!empty($id)){
                return redirect(route('admin.promotion_form')."/".$id)->with('status', 'Created Promotion Success!!');
            }
          }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return back()->withInput()->withErrors(['Error']);
    }
}

public function deletePromotion(Request $request){
    try{
        
        if(!has_permission($this->menu2,["view","delete"])){
          return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){

          $input = $request->all();
          $validator = Validator::make($input, [
            'id' => 'required'
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }
          Promotion::where('id', '=', $input['id'])->delete();
          return json_encode(["status"=>"success"]);
        }       
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}


public function promotionForm($id = null){
    try{
        if(!has_permission($this->menu2,["view"])){
            return view('admin.common.permission_denied');
        }

        $return['id'] = $id;
        $return['coverage_id'] = [];
        $init['coverage'] = Coverage::All()->toArray();
        $init['vehicle_brand'] = VehicleBrand::All()->toArray();
        $init['vehicle_model'] = VehicleModel::All()->toArray();
        if(!empty($id)){ //Edit Mode
            $data = Promotion::where('id','=',$id)->first();
            if(!empty($data)){
                $return['id'] = !empty($data->id)?$data->id:'';
                $return['title'] = !empty($data->title)?$data->title:'';
                $return['code'] = !empty($data->code)?$data->code:'';
                $return['discount'] = !empty($data->discount)?$data->discount:null;
                $return['start_at'] = !empty($data->start_at)?$data->start_at:'';
                $return['expire_at'] = !empty($data->expire_at)?$data->expire_at:'';
                $return['status'] = !empty($data->status)?$data->status:0;
                $return['maximum_grant'] = !empty($data->maximum_grant)?$data->maximum_grant:null;
                $return['receive_amount'] = !empty($data->receive_amount)?$data->receive_amount:0;
                $return['coverage_id'] = explode(",",$data->coverage_id);
                $return['vehicle_brand'] = explode(",",$data->vehicle_brand);
                $return['vehicle_model'] = explode(",",$data->vehicle_model);
                $return['is_register'] = !empty($data->is_register)?$data->is_register:0;
                $return['max_register'] = !empty($data->max_register)?$data->max_register:'';
                $return['register_start_at'] = !empty($data->register_start_at)?$data->register_start_at:'';
                $return['register_expire_at'] = !empty($data->register_expire_at)?$data->register_expire_at:'';
                $return['cover_register'] = !empty($data->cover_register)?$data->cover_register:'';
                $return['ending_message'] = !empty($data->ending_message)?$data->ending_message:'';
                $return['register_amount'] = $data->registerList()->count();
            }else{
                return back()->withInput()->withErrors(['No Data Found!']);
            }
            Log::info($data->registerList()->count());
        }

        


        return view('admin.promotion.promotion_form',["data"=>$return,'init'=>$init]);
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return back()->withInput()->withErrors(['Error']);
    }
}

    public function setRegister(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit","delete"])){
                return json_encode(["resp_error"=>"Permission Denied"]);
            }
            $input = $request->all();
            $validator = Validator::make($input, [
                'promotion_id' => 'required',
                'mode' => 'required'
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $promotion = Promotion::where('id',$input['promotion_id'])->first();
            if(!empty($promotion)){
                $promotion->is_register = ($input['mode']=="true"?1:0);
                $promotion->save();
                return json_encode(["status"=>"success"]);
            }
            return json_encode(["status"=>"fail"]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function saveRegister(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit"])){
                return json_encode(["resp_error"=>"Permission Denied"]);
            }
            $input = $request->all();
            $validator = Validator::make($input, [
                'promotion_id' => 'required',
                'max_register' => 'required',
                'register_start_at' => 'required',
                'register_expire_at' => 'required',
            ]);

            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $data = [
                'max_register' => $input['max_register'],
                'register_start_at' => $input['register_start_at'],
                'register_expire_at' => $input['register_expire_at'],
            ];

            $promotion = Promotion::where('id',$input['promotion_id'])->first();
            if(!empty($promotion)){
                if(!empty($input['cover_register']) && $input['cover_register']!="undefined"){
                    $this->setCover($request,$input['promotion_id']);
                }
                if(isset($input['clear_cover']) && $input['clear_cover']=="true"){
                    @unlink(public_path($promotion->cover_register));
                    $promotion->cover_register = "";
                }
                $promotion->max_register = $input['max_register'];
                $promotion->register_start_at = $input['register_start_at'];
                $promotion->register_expire_at = $input['register_expire_at'];
                $promotion->save();
                
                return json_encode(["status"=>"success"]);
            }
            return json_encode(["status"=>"fail","reason" => "Update Promotion Fail"]);

            

         
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    public function setCover($request,$id){

        $this->validate($request, [
            'cover_register' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $input = $request->all();
        $path = public_path(Config::get('path.banner_path'));
        $promotion = Promotion::where('id','=',$id)->first();
            
        $name = uniqid();
        $ext = $request->cover_register->getClientOriginalExtension();
        $buffer = $name .'.'.$ext;

        $request->cover_register->move($path, $buffer); 
        Image::make($path . $buffer)->resize(1280,1037)->save($path . $name .'_release.'.$ext);

        //update path
        
        @unlink(public_path($promotion->cover_register));
        $promotion->cover_register = Config::get('path.banner_path') . $name .'_release.'.$ext;
        $promotion->save();

        return true;
    }


    public function getVehicleBrand(Request $request){
        try{
            if($request->ajax()){
                // Validate 
                $brand = VehicleBrand::orderBy('top_order','asc')->get()->pluck('name','id');
                if(!empty($brand));
                return response()->json($brand->toArray());
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
      }


    public function getVehicleModel(Request $request){
        try{
            if($request->ajax()){
                // Validate 
                $input = $request->all();
                $validator = Validator::make($input, [  
                    'id'=>'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
    
                $model = VehicleModel::where('brand_id',$input['id'])->pluck('name','id')->toArray();
                if(is_array($model)){
                    return response()->json($model);
                }else{
                    return response()->json([]);
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }



 
}
