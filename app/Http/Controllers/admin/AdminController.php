<?php

namespace App\Http\Controllers\admin;

use App\Repositories\AdminRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Privilege;
use App\Models\Admins;
use App\Models\UserManagementLog;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;

class AdminController extends Controller
{

  private $menu1 = "motoradmins/admin_list";
  private $menu2 = "motoradmins/profile";


  public function __construct(AdminRepository $admin){
    $this->middleware('auth:admin');
    $this->admin = $admin;
  }

  public function index(){   
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    return view('admin.system.admins');
  }

  public function getPrivilege(Request $request){
      try{
        if($request->ajax()){
            $data = Privilege::all();
            return $data->toJson();
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getAdminList(Request $request){
      try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $admins = Admins::where('privilege','!=',1)->get();
            return Datatables::of($admins)->addColumn('action', function ($data) {
                return '<a href="'.route('admin.get_profile').'/'.$data->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.' '.$data->lastname.'"><i class="fa fa-trash"></i></a>';
            })->editColumn('name',function($data){
                return $data->name." ".$data->lastname;
            })->editColumn('privilege',function($data){
                return $data->privilege_name()->first()->name;
            })->make(true);
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(array("resp_error"=>$e->getMessage()));
      }
  }

  public function getProfile($id = null){
    try{
        if($id=="me" || $id==Auth::guard('admin')->user()->id){
            $id = Auth::guard('admin')->user()->id;
            $return['role_disabled'] = true;
        }

        if($id==1 && Auth::guard('admin')->user()->id!=1){ //ไม่อนุญาติให้เข้าดู Super Admin
            return view('admin.common.permission_denied');
        }
        if(!has_permission($this->menu2,["view","edit"]) && $id!=Auth::guard('admin')->user()->id){
            return view('admin.common.permission_denied');
        }

        $return['id'] = $id;
        if(!empty($id)){ //Edit Mode
            $data = Admins::where('id','=',$id)->first();
            if($data){
                $return['id'] = !empty($data->id)?$data->id:'';
                $return['username'] = !empty($data->username)?$data->username:'';
                $return['email'] = !empty($data->email)?$data->email:'';
                $return['name'] = !empty($data->name)?$data->name:'';
                $return['lastname'] = !empty($data->lastname)?$data->lastname:'';
                $return['privilege'] = !empty($data->privilege)?$data->privilege:'';
                $return['privilege_name'] = !empty($data->privilege)?$data->privilege_name()->first()->name:'';
                $return['cover'] = !empty($data->cover)?$data->cover:'';
                $return['status'] = !empty($data->status)?$data->status:0;
                $return['mode'] = "edit";
            }else{
                return back()->withInput()->withErrors(['No Data Found!']);
            }
        }else{ //Add Mode
            $return['mode'] = "add";
        }
        return view('admin.system.profile',["data"=>$return,'select_privilege'=>Privilege::where('id','!=',1)->pluck('name','id')->toArray()]);
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return back()->withInput()->withErrors(['Error']);
    }
  }

  public function saveProfile(Request $request) {
    try{
        $input = $request->all();

        if(!empty($input['profile_id'])){ //Edit Mode
            if(!has_permission($this->menu2,["view","add","edit"]) && $input['profile_id']!=Auth::guard('admin')->user()->id){
                return view('admin.common.permission_denied');
            }
        }else{ //Add Mode
            if(!has_permission($this->menu2,["view","add","edit"])){
                return view('admin.common.permission_denied');
            }
        }
        
          // Validate 
          $validate_input = array(
            'name' => 'required|min:3',
            'lastname' => 'required|min:3',
            'username' => [
                'required',
                'min:5',
                Rule::unique('admins'),
            ],
            'privilege' => 'required'
          );
          if(!empty($input['profile_id'])){ //Mode Edit
            $validate_input['username'] = [
                'required',
                'min:5',
                Rule::unique('admins')->ignore($input['profile_id']),
            ];
          }else{
            $validate_input['password'] = 'required|min:5';
            $validate_input['verify_password'] = 'required|min:5|same:password';
          }
            
          $validator = Validator::make($input,$validate_input);
          if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
          }
          $data = array(
            'username'=>$input['username'],
            'name'=>$input['name'],
            'lastname'=>$input['lastname'],
            'email'=>!empty($input['email'])?$input['email']:'',
            'privilege'=>$input['privilege'],
            'status'=>!empty($input['status'])?1:0,
          );
          if(!empty($input['password'])){
            $data['password'] = Hash::make($input['password']);
          }
          if(!empty($input['profile_id'])){ //Edit mode
            
            if($input['profile_id']==Auth::guard('admin')->user()->id){ //ไม่มีการแก้ไขตำแหน่งของตัวเอง
                unset($data['privilege']);
            } 

            $admin = Admins::where('id', '=', $input['profile_id'])->first();
            $buffer_data = $admin;
            if(($admin->privilege==1 || $admin->privilege==2) && (Auth::guard('admin')->user()->id!=1 && $input['profile_id']!=Auth::guard('admin')->user()->id)){
                return view('admin.common.permission_denied');
            }

            Admins::where('id', '=', $input['profile_id'])->update($data);
            $this->setCover($request);
            if(isset($input['clear_cover']) && $input['clear_cover']=="true"){
              $admin = Admins::where('id','=',$input['profile_id'])->first();
              //@unlink(public_path($admin->cover));
              $admin->cover = "";
              $admin->save();
            }

            //Save Log
            $this->setUserManagementLog($buffer_data,$data);

            return redirect(route('admin.get_profile')."/".$input['profile_id'])->with('status', 'Updated Profile Success!!');
          }else{ // Add Mode
            $id = Admins::create($data)->id;
            if(!empty($id)){
                $this->setCover($request,$id);
                return redirect(route('admin.get_profile')."/".$id)->with('status', 'Created Profile Success!!');
            }
          } 
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return back()->withInput()->withErrors(['Error']);
    }
  }

  public function setCover($request,$id=null){
    if(isset($request['cover'])){
        $this->validate($request, [
            'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        if(!empty($request['profile_id'])) $id = $request['profile_id'];
        if($id!=null){
            $path = public_path(Config::get('path.admin_cover'));
            $name = time();
            $ext = $request->cover->getClientOriginalExtension();
            $imageName = $name .'.'.$ext;
            $thumb_imageName = $name .'_thumb.'. $ext; 

            $request->cover->move($path, $imageName);
            Image::make($path . $imageName)->resize(160, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path . $thumb_imageName);

            //update path
            $admin = Admins::where('id','=',$id)->first();
            @unlink(public_path($admin->cover));
            @unlink($path.$imageName);
            
            $admin->cover = Config::get('path.admin_cover') . $thumb_imageName;
            $admin->save();
        }
    }
  }

  public function editPassword(Request $request) {
    try{
        $input = $request->all();
        
        if(!empty($input['profile_id'])){
            if(!has_permission($this->menu2,["view","edit"]) && $input['profile_id']!=Auth::guard('admin')->user()->id){
                return view('admin.common.permission_denied');
            }
        }
        
        // Validate 
        $validate_input = array(
            'profile_id' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'verify_password' => 'required|min:6|same:new_password',
        );
        
        
        $validator = Validator::make($input,$validate_input);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        $admin = Admins::where('id','=',$input['profile_id'])->first();
        $buffer_data = $admin;
        if(($admin->privilege==1 || $admin->privilege==2) && (Auth::guard('admin')->user()->id!=1 && $input['profile_id']!=Auth::guard('admin')->user()->id)){
            return view('admin.common.permission_denied');
        }
        if(!Hash::check($input['old_password'], $admin->password)){
            return back()->withInput()->withErrors(['The specified old password does not match']);
        }else{
            $admin->password = Hash::make($input['new_password']);
            $admin->save();
            //Save Log
            $this->setUserManagementLog($buffer_data,[
                'password' => Hash::make($input['new_password'])
            ]);
            
            return redirect(route('admin.get_profile')."/".$input['profile_id'])->with('status', 'Updated Password Success!!');
        }

    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return back()->withInput()->withErrors(['Error']);
    }
  }

  public function deleteAdmin(Request $request) {
    try{
        
        if(!has_permission($this->menu2,["delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'id'=>'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $admin = Admins::where('id', '=', $input['id'])->first();
            if(($admin->privilege==2 && Auth::guard('admin')->user()->id!=1) || ($admin->privilege==1) || ($input['id']==Auth::guard('admin')->user()->id)){
              return json_encode(array("resp_error"=>trans('common.permission_denied')));
            }
        
            Admins::where('id','=',$input['id'])->delete();
            return json_encode(array("status"=>"success"));
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  private function setUserManagementLog($admin,$data){
        $edit_data = [];
        $old_data = [];
        if(!empty($data['username'])){
            if(strcmp($admin->username,$data['username']) != 0){
                $edit_data['username'] = $data['username'];
                $old_data['username'] = $admin->username;
            }
        }
        if(!empty($data['password'])){
            if(strcmp($admin->password,$data['password']) != 0){
                $edit_data['password'] = $data['password'];
                $old_data['password'] = $admin->password;
            }
        }
        if(!empty($data['name'])){
            if(strcmp($admin->name,$data['name']) != 0){
                $edit_data['name'] = $data['name'];
                $old_data['name'] = $admin->name;
            }
        }
        if(!empty($data['lastname'])){
            if(strcmp($admin->lastname,$data['lastname']) != 0){
                $edit_data['lastname'] = $data['lastname'];
                $old_data['lastname'] = $admin->lastname;
            }
        }
        if(!empty($data['email'])){
            if(strcmp($admin->email,$data['email']) != 0){
                $edit_data['email'] = $data['email'];
                $old_data['email'] = $admin->email;
            }
        }
        if(!empty($data['privilege'])){
            if(strcmp($admin->privilege,$data['privilege']) != 0){
                $edit_data['privilege'] = $data['privilege'];
                $old_data['privilege'] = $admin->privilege;
            }
        }
        if(!empty($data['status']) || $data['status']==0){
            if(strcmp($admin->status,$data['status']) != 0){
                $edit_data['status'] = $data['status'];
                $old_data['status'] = $admin->status;
            }
        }
        
        if(!empty($edit_data) && !empty($old_data)){
            $log = [
                'edit_data' => json_encode($edit_data),
                'old_data' => json_encode($old_data),
                'created_by' => Auth::guard('admin')->user()->id
            ];
            UserManagementLog::create($log);
        }   
        
        return true;
  }



  
}
