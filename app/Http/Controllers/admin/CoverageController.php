<?php

namespace App\Http\Controllers\admin;

use App\Repositories\VehicleRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Coverage;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;

class CoverageController extends Controller
{

    private $menu1 = "motoradmins/coverage";

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        } 
        //$init['select_brand'] = VehicleBrand::pluck('name','id')->toArray();
        //return view('admin.coverage.coverage',['init'=>$init]);
        $return['2plus'] = Coverage::where('id',1)->first()->toArray();
        $return['3plus'] = Coverage::where('id',2)->first()->toArray();
        $return['type3'] = Coverage::where('id',3)->first()->toArray();
        return view('admin.coverage.coverage',['data'=>$return]);
    }

    public function saveCoverage(Request $request){
        try{
            if(!has_permission($this->menu1,["view","edit","add"])){
                return view('admin.common.permission_denied');
            }

            $input = $request->all();
            // Validate 
            $filter_validate = [
                'coverage_id'=>'required',
                'person_damage_person'=>'numeric',
                'person_damage_once'=>'numeric',
                'stuff_damage'=>'numeric',
                'death_disabled'=>'numeric',
                'medical_fee'=>'numeric',
                'bail_driver'=>'numeric',
                'max_age' => 'numeric',
                'min_age' => 'numeric',
                'max_pre' => 'nullable|numeric'
            ];

            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $data = array(
                'person_damage_person'=>$input['person_damage_person'],
                'person_damage_once'=>$input['person_damage_once'],
                'stuff_damage'=>$input['stuff_damage'],
                'death_disabled'=>$input['death_disabled'],
                'medical_fee'=>$input['medical_fee'],
                'bail_driver'=>$input['bail_driver'],
                'max_age' => $input['max_age'],
                'min_age' => $input['min_age'],
                'max_pre' => !empty($input['max_pre'])?$input['max_pre']:null,
                'status' => isset($input['status'])?'ACTIVE':'INACTIVE',
            );


            if(!empty($input['coverage_id'])){ //Edit mode
                Coverage::where('id', '=', $input['coverage_id'])->update($data);
                return redirect(route('admin.coverage'))->with('status', 'Updated Coverage Success!!');
            }else{ //Add mode
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }



}
