<?php

namespace App\Http\Controllers\admin;

use App\Repositories\PricingRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Pricing;
use App\Models\Compulsory;
use App\Models\Addon;
use App\Models\Coverage;
use App\Models\DatabaseActive;

use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Excel;
use Illuminate\Support\Facades\Storage;

class PricingController extends Controller
{

    private $menu1 = "motoradmins/pricing/pricing";
    private $menu2 = "motoradmins/pricing/pricing_form";
    private $menu3 = "motoradmins/pricing/compulsory";
    private $menu4 = "motoradmins/pricing/addon";
    private $pricingTableActive = null;

    public function __construct(){
        $this->middleware('auth:admin');
        $this->pricingTableActive = getDatabaseActive('pricing');
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        } 
        return view('admin.pricing.pricing');
    }

    public function getPricingList(Request $request){
        try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
            $repository = new PricingRepository();
            $data_resp = $repository->list_pricing($input);
            return Datatables::of($data_resp)->addColumn('action', function ($data) {
                return '<a href="'.route('admin.pricing_form').'/'.$data->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->mortor_package_code.'"><i class="fa fa-trash"></i></a>';
            })->make(true);
        }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getImportedPricingList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $repository = new PricingRepository();
                $data_resp = $repository->list_pricing_imported();
                return Datatables::of($data_resp)->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function pricingForm($id = null){
        try{
            if(!has_permission($this->menu2,["view","edit","add"])){
                return view('admin.common.permission_denied');
            }

            $return['id'] = $id;
            if(!empty($id)){ //Edit Mode
                $data = Pricing::where('id','=',$id)->first();
                if($data){
                    $return['id'] = !empty($data->id)?$data->id:'';
                    $return['coverage_id'] = !empty($data->coverage_id)?$data->coverage_id:'';
                    $return['coverage_name'] = !empty($data->coverage_name)?$data->coverage_name:'';
                    $return['ft_si'] = !empty($data->ft_si)?$data->ft_si:0;
                    $return['car_code'] = !empty($data->car_code)?$data->car_code:0;
                    $return['car_engine'] = !empty($data->car_engine)?$data->car_engine:null;
                    $return['define_name'] = !empty($data->define_name)?$data->define_name:null;
                    $return['cctv'] = !empty($data->cctv)?$data->cctv:0;
                    $return['garage_type'] = !empty($data->garage_type)?$data->garage_type:'NO';
                    $return['deductible'] = !empty($data->deductible)?$data->deductible:0;
                    $return['additional_coverage'] = !empty($data->additional_coverage)?$data->additional_coverage:null;
                    $return['mortor_package_code'] = !empty($data->mortor_package_code)?$data->mortor_package_code:null;
                    $return['based_prem'] = !empty($data->based_prem)?$data->based_prem:0;
                    $return['based_prem_percent'] = !empty($data->based_prem_percent)?$data->based_prem_percent:0;
                    $return['name_policy'] = !empty($data->name_policy)?$data->name_policy:0;
                    $return['basic_premium_cover'] = !empty($data->basic_premium_cover)?$data->basic_premium_cover:0;
                    $return['add_premium_cover'] = !empty($data->add_premium_cover)?$data->add_premium_cover:0;
                    $return['fleet_percent'] = !empty($data->fleet_percent)?$data->fleet_percent:0;
                    $return['fleet'] = !empty($data->fleet)?$data->fleet:0;
                    $return['ncb_percent'] = !empty($data->ncb_percent)?$data->ncb_percent:0;
                    $return['ncb'] = !empty($data->ncb)?$data->ncb:0;
                    $return['total_premium'] = !empty($data->total_premium)?$data->total_premium:0;
                    $return['od_si'] = !empty($data->od_si)?$data->od_si:0;
                    $return['od_based_prem'] = !empty($data->od_based_prem)?$data->od_based_prem:0;
                    $return['od_total_premium'] = !empty($data->od_total_premium)?$data->od_total_premium:0;
                    $return['deduct_percent'] = !empty($data->deduct_percent)?$data->deduct_percent:0;
                    $return['deduct'] = !empty($data->deduct)?$data->deduct:0;
                    $return['cctv_discount_percent'] = !empty($data->cctv_discount_percent)?$data->cctv_discount_percent:0;
                    $return['cctv_discount'] = !empty($data->cctv_discount)?$data->cctv_discount:0;
                    $return['direct_percent'] = !empty($data->direct_percent)?$data->direct_percent:0;
                    $return['direct'] = !empty($data->direct)?$data->direct:0;
                    $return['net_premium'] = !empty($data->net_premium)?$data->net_premium:0;
                    $return['stamp'] = !empty($data->stamp)?$data->stamp:0;
                    $return['vat'] = !empty($data->vat)?$data->vat:0;
                    $return['gross_premium'] = !empty($data->gross_premium)?$data->gross_premium:0;
                    $return['flood_net_premium'] = !empty($data->flood_net_premium)?$data->flood_net_premium:0;
                    $return['flood_stamp'] = !empty($data->flood_stamp)?$data->flood_stamp:0;
                    $return['flood_vat'] = !empty($data->flood_vat)?$data->flood_vat:0;
                    $return['flood_gross_premium'] = !empty($data->flood_gross_premium)?$data->flood_gross_premium:0;
                    $return['is_bangkok'] = !empty($data->is_bangkok)?$data->is_bangkok:0;
                }else{
                    return back()->withInput()->withErrors(['No Data Found!']);
                }
            }

            $init = [];
            $init['coverage'] = ['1'=>'SOMPO 2+','2'=>'SOMPO 3+','3'=>'SOMPO 3']; 
            $init['car_code'] = ["110"=>"110","320"=>"320"];
            $init['car_engine'] = ["<=2000CC"=>"<=2000CC",">2000CC"=>">2000CC","<=4TONS"=>">4TONS"];
            $init['define_name'] = ["UNNAMED"=>"UNNAMED","18-24Y"=>"18-24Y","25-28Y"=>"25-28Y","29-35Y"=>"29-35Y","36-50Y"=>"36-50Y","50UP"=>"50UP"];
            $init['garage_type'] = ["GENERAL"=>"GENERAL","DEALER"=>"DEALER","NO"=>"NO"];
            $init['additional_coverage'] = ["NO"=>"NO","FLOOD"=>"FLOOD"];
            return view('admin.pricing.pricing_form',["data"=>$return,"init"=>$init]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }


    public function savePricing(Request $request){
        try{
              if(!has_permission($this->menu2,["view","edit","add"])){
                  return view('admin.common.permission_denied');
              }

              $input = $request->all();
              //Log::info($input);
              // Validate 
              $filter_validate = [
                  'coverage_id'=>'required',
                  'ft_si'=>'required',
                  'car_engine'=>'required',
                  'car_code'=>'required',
                  'define_name'=>'required',
                  'cctv'=>'required',
                  'garage_type'=>'required',
                  'deductible'=>'required',
                  'additional_coverage'=>'required',
                  //'mortor_package_code'=>'required',
                  'based_prem'=>'numeric',
                  'based_prem_percent'=>'numeric',
                  'name_policy'=>'numeric',
                  'basic_premium_cover'=>'numeric',
                  'add_premium_cover'=>'numeric',
                  'fleet_percent'=>'numeric',
                  'fleet'=>'numeric',
                  'ncb_percent'=>'numeric',
                  'ncb'=>'numeric',
                  'total_premium'=>'numeric',
                  'od_si'=>'numeric',
                  'od_based_prem'=>'numeric',
                  'od_total_premium'=>'numeric',
                  'deduct_percent'=>'numeric',
                  'deduct'=>'numeric',
                  'cctv_discount_percent'=>'numeric',
                  'cctv_discount'=>'numeric',
                  'direct_percent'=>'numeric',
                  'direct'=>'numeric',
                  'net_premium'=>'numeric',
                  'stamp'=>'numeric',
                  'vat'=>'numeric',
                  'gross_premium'=>'numeric',
                  'flood_net_premium'=>'numeric',
                  'flood_stamp'=>'numeric',
                  'flood_vat'=>'numeric',
                  'flood_gross_premium'=>'numeric',
                  'is_bangkok'=>'required',
              ];
    
              $validator = Validator::make($input,$filter_validate);
              if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
              }
    
              $data = array(
                'coverage_id'=>$input['coverage_id'],
                'ft_si'=>$input['ft_si'],
                'car_engine'=>$input['car_engine'],
                'car_code'=>$input['car_code'],
                'define_name'=>$input['define_name'],
                'cctv'=>$input['cctv'],
                'garage_type'=>$input['garage_type'],
                'deductible'=>$input['deductible'],
                'additional_coverage'=>$input['additional_coverage'],
                'mortor_package_code'=>!empty($input['mortor_package_code'])?$input['mortor_package_code']:'',
                'based_prem'=>$input['based_prem'],
                'based_prem_percent'=>$input['based_prem_percent'],
                'name_policy'=>$input['name_policy'],
                'basic_premium_cover'=>$input['basic_premium_cover'],
                'add_premium_cover'=>$input['add_premium_cover'],
                'fleet_percent'=>$input['fleet_percent'],
                'fleet'=>$input['fleet'],
                'ncb_percent'=>$input['ncb_percent'],
                'ncb'=>$input['ncb'],
                'total_premium'=>$input['total_premium'],
                'od_si'=>$input['od_si'],
                'od_based_prem'=>$input['od_based_prem'],
                'od_total_premium'=>$input['od_total_premium'],
                'deduct_percent'=>$input['deduct_percent'],
                'deduct'=>$input['deduct'],
                'cctv_discount_percent'=>$input['cctv_discount_percent'],
                'cctv_discount'=>$input['cctv_discount'],
                'direct_percent'=>$input['direct_percent'],
                'direct'=>$input['direct'],
                'net_premium'=>$input['net_premium'],
                'stamp'=>$input['stamp'],
                'vat'=>$input['vat'],
                'gross_premium'=>$input['gross_premium'],
                'flood_net_premium'=>$input['flood_net_premium'],
                'flood_stamp'=>$input['flood_stamp'],
                'flood_vat'=>$input['flood_vat'],
                'flood_gross_premium'=>$input['flood_gross_premium'],
                'is_bangkok'=>$input['is_bangkok'],
                'updated_by' => Auth::guard('admin')->user()->id,
              );

              if(!empty($input['pricing_id'])){ //Edit mode
                Pricing::where('id', '=', $input['pricing_id'])->update($data);
                return redirect(route('admin.pricing_form')."/".$input['pricing_id'])->with('status', 'Updated Pricing Success!!');
              }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                $id = Pricing::create($data)->id;
                if(!empty($id)){
                    return redirect(route('admin.pricing_form')."/".$id)->with('status', 'Created Pricing Success!!');
                }
              }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }


    public function deletePricing(Request $request){
        try{
            
            if(!has_permission($this->menu2,["view","delete"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
  
              $input = $request->all();
              $validator = Validator::make($input, [
                'id' => 'required'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              Pricing::where('id', '=', $input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }



    /************* compulsory ***************/
    public function compulsory(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        } 
        return view('admin.pricing.compulsory');
    }

    public function get_compulsory_list(Request $request){
        try{
            if(!has_permission($this->menu3)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
              $input = $request->all();
              $data_resp = Compulsory::All();
              return Datatables::of($data_resp)->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
              })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getDataCompulsory(Request $request){
        try{
          if(!has_permission($this->menu3)){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
              'id' => 'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
  
            $response = Compulsory::where('id',$input['id'])->first();
            if(!empty($response)){
              return json_encode($response);
            }
  
          }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
        }
    }

    public function saveCompulsory(Request $request){
        try{
            if($request->ajax()){
              
              if(!has_permission($this->menu3,["view","edit","add"])){
                  return json_encode(["resp_error"=>trans('common.permission_denied')]);
              }
  
              $input = $request->all();
              
              // Validate 
              $validator = Validator::make($input, [
                  'body_type'=>'required',
                  'motor_code_ac'=>'required|numeric',
                  'net_premium'=>'required|numeric',
                  'stamp'=>'required|numeric',
                  'vat'=>'required|numeric',
                  'gross_premium'=>'required|numeric'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              $data = array(
                  'body_type'=>$input['body_type'],
                  'motor_code_ac'=>$input['motor_code_ac'],
                  'net_premium'=>$input['net_premium'],
                  'stamp'=>$input['stamp'],
                  'vat'=>$input['vat'],
                  'gross_premium'=>$input['gross_premium'],
                  'updated_by' => Auth::guard('admin')->user()->id
              );
              if(!empty($input['id'])){ //Edit mode
                  Compulsory::where('id', '=', $input['id'])->update($data);
              }else{ //Add mode
                  $data['created_by'] = Auth::guard('admin')->user()->id;
                  Compulsory::create($data);
              }
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
        }
    }

    public function deleteCompulsory(Request $request){
        try{
            
            if(!has_permission($this->menu3,["view","delete"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
  
              $input = $request->all();
              $validator = Validator::make($input, [
                'id' => 'required'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              Compulsory::where('id', '=', $input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    /************* Add-On ***************/
    public function addon(){
        if(!has_permission($this->menu4)){
            return view('admin.common.permission_denied');
        } 
        return view('admin.pricing.addon');
    }

    public function getAddonList(Request $request){
        try{
            //Log::info("ASDSD");
            if(!has_permission($this->menu4)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
              $input = $request->all();
              $data_resp = Addon::All();

              
              return Datatables::of($data_resp)->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
              })->addColumn('coverage', function ($data) {
                return (!empty($data['taxi'])?"TAXI:".$data['taxi']:"").(!empty($data['theft'])?" THEFT:".$data['theft']:"") ;
              })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getDataAddon(Request $request){
        try{
          if(!has_permission($this->menu4)){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
              'id' => 'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
  
            $response = Addon::where('id',$input['id'])->first();
            if(!empty($response)){
              return json_encode($response);
            }
  
          }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
        }
    }

    public function saveAddon(Request $request){
        try{
            if($request->ajax()){
              
              if(!has_permission($this->menu4,["view","edit","add"])){
                  return json_encode(["resp_error"=>trans('common.permission_denied')]);
              }
  
              $input = $request->all();
              
              // Validate 
              $validator = Validator::make($input, [
                  'misc_package_code'=>'required',
                  'net_premium'=>'required|numeric',
                  'stamp'=>'required|numeric',
                  'vat'=>'required|numeric',
                  'gross_premium'=>'required|numeric'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              $data = array(
                'taxi'=>$input['taxi'],
                'theft'=>$input['theft'],
                'misc_package_code'=>$input['misc_package_code'],
                'net_premium'=>$input['net_premium'],
                'stamp'=>$input['stamp'],
                'vat'=>$input['vat'],
                'gross_premium'=>$input['gross_premium'],
                'updated_by' => Auth::guard('admin')->user()->id
              );
              if(!empty($input['id'])){ //Edit mode
                  Addon::where('id', '=', $input['id'])->update($data);
              }else{ //Add mode
                  $data['created_by'] = Auth::guard('admin')->user()->id;
                  Addon::create($data);
              }
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
        }
    }

    public function deleteAddon(Request $request){
        try{
            
            if(!has_permission($this->menu4,["view","delete"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
  
              $input = $request->all();
              $validator = Validator::make($input, [
                'id' => 'required'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              Addon::where('id', '=', $input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function importXLS(Request $request){
        try{
            Log::info("import");
            ini_set('memory_limit', '-1');
            $input = $request->all();
            //if(!empty($input['backup_path'])){
                $directory = Storage::disk('upload-chunks')->getDriver()->getAdapter()->getPathPrefix();
                $filename = "pricing.xls";
                if($this->setPricingData($directory."/".$filename)){
                    return json_encode(["status"=>"SUCCESS"]);
                }
            //}
            return json_encode(["resp_error"=>"Fail to import, Try again."]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function chunksPricing(Request $request){
        try{
            Log::info("chunks");
            ini_set('memory_limit', '-1');
            if(!has_permission($this->menu1,["view","edit","add","delete"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $validator = Validator::make(
                [
                    'file'      => $request->file,
                    'extension' => strtolower($request->file->getClientOriginalExtension()),
                ],
                [
                    'file'          => 'required',
                    'extension'      => 'required|in:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp',
                ]
            );
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $path = Storage::disk('upload-chunks')->putFileAs('', $request->file('file'),'pricing.xls');
            if (!empty($path)) {
                return json_encode(["status"=>"SUCCESS"]);
            }else{
                return json_encode(["resp_error"=>"Upload File Failed!, Try Again."]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    private function setPricingData($path){
        try{
            set_time_limit(0);
            ini_set('memory_limit', '-1');
            session()->forget('database_active');
            
            if(!Storage::exists($path)) {

                //Remove Data
                $pricing = new pricing;
                $this->pricingTableActive==1?$pricing->setTable('pricing_second'):$pricing->setTable('pricing');
                $pricing->whereNotNull('id')->delete();

                Excel::load($path,function($reader){
                    $excelData = $reader->get();
                    $coverage_name_store = Coverage::all()->pluck('plan','id')->toArray();
                    $created_by = Auth::guard('admin')->user()->id;
                    $is_delete_1 = false;
                    $is_delete_2 = false;
                    $is_delete_3 = false;
                    $loop_index = 0;
                    $primary_id = 0;
                    
                    foreach($excelData as $key => $data){
                        $planId = 3;
                        if($data->plan=="SOMPO2+") $planId = 1;
                        else if($data->plan=="SOMPO3+") $planId = 2;

                        $pricing = new Pricing;
                        $pricing = $this->pricingTableActive==1?$pricing->setTable('pricing_second'):$pricing->setTable('pricing');
                        $pricing->id = ++$primary_id;
                        $pricing->coverage_id = $planId;
                        $pricing->coverage_name = $coverage_name_store[$planId];
                        
                        $pricing->ft_si = $planId!=3?floatval($data->od_si):$data->ft_si;
                        $pricing->car_code = $data->car_code;
                        $pricing->car_engine = strtoupper(str_replace([' ',','], '',$data->car_engine));
                        $pricing->define_name = strtoupper(str_replace(' ','',$data->define_name));
                        $pricing->cctv = (strtoupper($data->cctv)=="NO"?0:1);
                        $pricing->garage_type = strtoupper(str_replace(' ','',$data->garage_type));
                        $pricing->deductible = $data->deductible;
                        $pricing->additional_coverage = strtoupper(str_replace(' ','',$data->additional_coverage));
                        $pricing->mortor_package_code = '';
                        $pricing->based_prem = floatval($data->type2_based_prem);
                        $pricing->based_prem_percent = floatval(str_replace('%','',$data->type2_name_policy_percent));
                        $pricing->name_policy = floatval($data->type2_name_policy);
                        $pricing->basic_premium_cover = floatval($data->type2_basic_premium_cover);
                        $pricing->add_premium_cover = floatval($data->type2_additional_premium_cover);
                        $pricing->fleet_percent = floatval(str_replace('%','',$data->type2_fleet_percent));
                        $pricing->fleet = floatval($data->type2_fleet);
                        $pricing->ncb_percent = floatval(str_replace('%','',$data->type2_ncb_percent));
                        $pricing->ncb = floatval($data->type2_ncb);
                        $pricing->total_premium = floatval($data->type2_total_premium);
                        $pricing->od_si = floatval($data->od_si);
                        $pricing->od_based_prem = floatval($data->od_based_prem);
                        $pricing->od_total_premium = floatval($data->od_total_premium);
                        $pricing->deduct_percent = floatval(str_replace('%','',$data->deduct_percent));
                        $pricing->deduct = floatval($data->deduct);
                        $pricing->cctv_discount_percent = floatval(str_replace('%','',$data->cctv_percent));
                        $pricing->cctv_discount = floatval($data->cctv);
                        $pricing->direct_percent = floatval(str_replace('%','',$data->direct_percent));
                        $pricing->direct = floatval($data->direct);
                        $pricing->net_premium = floatval($data->type2_od_net_premium);
                        $pricing->stamp = floatval($data->type2_od_stamp);
                        $pricing->vat = floatval($data->type2_od_vat);
                        $pricing->gross_premium = floatval($data->type2_od_gross_premium);
                        $pricing->is_bangkok = (strtoupper($data->bkk)=="Y"?1:0);
                        $pricing->created_by = $created_by;
                        $pricing->updated_by = $created_by;
                        $pricing->save();
                        $loop_index++;
                    }
                });
            }

            return true;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        } 
    }

    public function submitImportChange(){
        try{
            $databaseActive = DatabaseActive::where('id',1)->first();
            $databaseActive->pricing_active = $databaseActive->pricing_active==1?2:1;
            $databaseActive->save();
            return json_encode(["status"=>"SUCCESS"]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        } 
    }


public function export(Request $request){
    try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }

        $input = $request->all();
        $repository = new PricingRepository();
        $result = $repository->list_pricing();

        if(!empty($result)){
            Excel::create('B2C_Motor_Pricing_'.date('Ymd'), function($excel) use($result){
                $excel->sheet('Sheet1', function($sheet) use($result) {
                    $sheet->loadView('admin.pricing.excel.export',["data"=>$result]);
                });
            })->export('xls');
        }else{
            return back()->withErrors([
                "error" => 'Error Data Not Found'
            ])->withInput();
        }
    }catch (\Exception $e) {
        Log::info($e->getMessage());
        return back()->withErrors([
            "error" => 'Error Please Contact Administrator'
        ])->withInput();
    }
}


}
