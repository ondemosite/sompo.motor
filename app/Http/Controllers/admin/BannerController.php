<?php

namespace App\Http\Controllers\admin;

use App\Repositories\GarageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Banner;
use App\Models\Promotion;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;

class BannerController extends Controller
{

    private $menu1 = "motoradmins/banner";
    private $menu2 = "motoradmins/banner/banner_form";

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.banner.banner');
    }

    public function bannerForm($id = null){
        if(!has_permission($this->menu2)){
            return view('admin.common.permission_denied');
        }
        $return['id'] = $id;
        if(!empty($id)){ //Edit Mode
            $data = Banner::where('id','=',$id)->first();
            if($data){
                $return['id'] = !empty($data->id)?$data->id:null;
                $return['title'] = !empty($data->title)?$data->title:null;
                $return['cover_en'] = !empty($data->cover_en)?$data->cover_en:null;
                $return['cover_th'] = !empty($data->cover_th)?$data->cover_th:null;
                $return['cover_en_mobile'] = !empty($data->cover_en_mobile)?$data->cover_en_mobile:null;
                $return['cover_th_mobile'] = !empty($data->cover_th_mobile)?$data->cover_th_mobile:null;
                $return['sync_type'] = !empty($data->sync_type)?$data->sync_type:null;
                $return['promotion_id'] = !empty($data->promotion_id)?$data->promotion_id:null;
                $return['link'] = !empty($data->link)?$data->link:null;
                $return['description'] = !empty($data->description)?$data->description:null;
                $return['orders'] = !empty($data->orders)?$data->orders:null;
                $return['view'] = !empty($data->view)?$data->view:null;
                $return['status'] = !empty($data->status)?$data->status:0;
            }else{
                return back()->withInput()->withErrors(['No Data Found!']);
            }
        }
        $init['promotions'] = Promotion::where('status',1)->orderBy('id','desc')->pluck('title','id');
        return view('admin.banner.banner_form',["data"=>$return,"init"=>$init]);
    }

    public function getBannerList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
            $banner = Banner::All();
            return Datatables::of($banner)->addColumn('action', function ($data) {
                return '<a href="'.route('admin.banner_form').'/'.$data->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->title.'"><i class="fa fa-trash"></i></a>';
            })->editColumn('link', function ($data) {
               if($data->sync_type=="LINK"){
                   return $data->link;
               }else{
                   $promotion = $data->promotion()->first();
                   if(!empty($promotion)){
                        return "Promotion: ".$promotion->title;
                   }else{
                       return "ไม่พบการเชื่อมโยง";
                   }
               }
            })->editColumn('description', function ($data) {
                if(empty($data->description)){
                    return "-";
                }else{
                    return $data->description;
                }
             })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function saveBanner(Request $request){
        try{
            if(!has_permission($this->menu2,["view","edit","add"])){
                return view('admin.common.permission_denied');
            }

            $input = $request->all();
           // Log::info($input);
            // Validate 
            $filter_validate = [
                'title'=>'required|max:200',
            ];

            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $data = array(
                'title'=>$input['title'],
                'sync_type' => !empty($input['sync_type'])?$input['sync_type']:'PROMOTION',
                'promotion_id' => !empty($input['promotion_id'])?$input['promotion_id']:null,
                'link' => !empty($input['link'])?$input['link']:null,
                'description'=>!empty($input['description'])?$input['description']:null,
                'orders'=>!empty($input['orders'])?$input['orders']:null,
                'status'=>!empty($input['status'])?1:0,
                'updated_by' => Auth::guard('admin')->user()->id
            );
            if(!empty($input['banner_id'])){ //Edit mode
                Banner::where('id', '=', $input['banner_id'])->update($data);
                $this->setBannerImage($request,$input['banner_id']);

                //Clear ---
                $banner = Banner::where('id','=',$input['banner_id'])->first();
                if(isset($input['clear_cover_en']) && $input['clear_cover_en']=="true"){
                    @unlink(public_path($banner->cover_en));
                    $banner->cover_en = null;
                    $banner->save();
                }
                if(isset($input['clear_cover_th']) && $input['clear_cover_th']=="true"){
                    @unlink(public_path($banner->cover_th));
                    $banner->cover_th = null;
                    $banner->save();
                }
                if(isset($input['clear_cover_en_mobile']) && $input['clear_cover_en_mobile']=="true"){
                    @unlink(public_path($banner->cover_en_mobile));
                    $banner->cover_en_mobile = null;
                    $banner->save();
                }
                if(isset($input['clear_cover_th_mobile']) && $input['clear_cover_th_mobile']=="true"){
                    @unlink(public_path($banner->cover_th_mobile));
                    $banner->cover_th_mobile = null;
                    $banner->save();
                }

                return redirect(route('admin.banner_form')."/".$input['banner_id'])->with('status', 'Updated Banner Success!!');
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                $id = Banner::create($data)->id;
                if(!empty($id)){
                    $this->setBannerImage($request,$id);
                    return redirect(route('admin.banner_form')."/".$id)->with('status', 'Created Banner Success!!');
                }
            }

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }

    public function setBannerImage($request,$id){
        if(isset($request['cover_en']) || isset($request['cover_th']) || isset($request['cover_en_mobile']) || isset($request['cover_th_mobile']) ){
            $this->validate($request, [
                'cover_en' => 'image|mimes:jpeg,png,jpg,gif,svg',
                'cover_th' => 'image|mimes:jpeg,png,jpg,gif,svg',
                'cover_en_mobile' => 'image|mimes:jpeg,png,jpg,gif,svg',
                'cover_th_mobile' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $input = $request->all();
            $path = public_path(Config::get('path.banner_path'));
            $banner = Banner::where('id','=',$id)->first();
            if(!empty($banner) && !empty($input['cover_en'])){
                
                $name = uniqid();
                $ext = $request->cover_en->getClientOriginalExtension();
                $buffer = $name .'.'.$ext;

                $request->cover_en->move($path, $buffer); 
                Image::make($path . $buffer)->resize(1920,500)->save($path . $name .'_release.'.$ext);

                //update path
                
                @unlink(public_path($banner->cover_en));
                $banner->cover_en = Config::get('path.banner_path') . $name .'_release.'.$ext;
                $banner->save();
            }
            if(!empty($banner) && !empty($input['cover_th'])){
                $name = uniqid();
                $ext = $request->cover_th->getClientOriginalExtension();
                $buffer = $name .'.'.$ext;

                $request->cover_th->move($path, $buffer); 
                Image::make($path . $buffer)->resize(1920,500)->save($path . $name .'_release.'.$ext);

                //update path
                
                @unlink(public_path($banner->cover_th));
                $banner->cover_th = Config::get('path.banner_path') . $name .'_release.'.$ext;
                $banner->save();

            }
            if(!empty($banner) && !empty($input['cover_en_mobile'])){
                $name = uniqid();
                $ext = $request->cover_en_mobile->getClientOriginalExtension();
                $buffer = $name .'.'.$ext;

                $request->cover_en_mobile->move($path, $buffer); 
                Image::make($path . $buffer)->resize(770, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path . $name .'_release.'.$ext);
                //Image::make($path . $buffer)->resize(770,500)->save($path . $name .'_release.'.$ext);

                //update path
                
                @unlink(public_path($banner->cover_en_mobile));
                $banner->cover_en_mobile = Config::get('path.banner_path') . $name .'_release.'.$ext;
                $banner->save();
            }
            if(!empty($banner) && !empty($input['cover_th_mobile'])){
                $name = uniqid();
                $ext = $request->cover_th_mobile->getClientOriginalExtension();
                $buffer = $name .'.'.$ext;

                $request->cover_th_mobile->move($path, $buffer); 
                //Image::make($path . $buffer)->resize(770,500)->save($path . $name .'_release.'.$ext);
                Image::make($path . $buffer)->resize(770, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path . $name .'_release.'.$ext);

                //update path
                
                @unlink(public_path($banner->cover_th_mobile));
                $banner->cover_th_mobile = Config::get('path.banner_path') . $name .'_release.'.$ext;
                $banner->save();
            }
        }
        return true;
    }

    public function deleteBanner(Request $request){
        try{
            if(!has_permission($this->menu1,["view","delete"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){

                $input = $request->all();
                $validator = Validator::make($input, [
                'id' => 'required'
                ]);
                if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
                }
                Banner::where('id', '=', $input['id'])->delete();
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }



 
}
