<?php

namespace App\Http\Controllers\admin;

use App\Repositories\ReceiptRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use App\Models\Receipt;
use App\Models\Payment;
use App\Models\Order;
use App\Models\Policy;
use Validator;
use DateTime;
use Auth;

class ReceiptController extends Controller
{

    private $menu1 = "motoradmins/receipt";

    public function __construct(ReceiptRepository $receipt){
        $this->middleware('auth:admin');
        $this->receipt = $receipt;
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.receipt.receipt');
    }

    public function getReceiptList(Request $request){
        try{
            Log::info("request");
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                Log::info($input);
                
                $query = $this->receipt->list($input);
                return Datatables::of($query)->addColumn('action', function ($data) {
                    $html = '<a style="cursor:pointer;" class="m-a-1 action-edit" title="More Detail" data-id="'.$data->id.'"><i class="fa fa-search-plus"></i></a>';
                    return $html;
                })->editColumn('amount',function($data){
                    return number_format($data->amount,2);
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at)." ".date("H:i:s",strtotime($data->created_at));
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getReceiptData(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                $channel = config('payment_code.payment_channel_code');
                $scheme = config('payment_code.payment_scheme');
                $receipt = Receipt::where('id',$input['id'])->first();
                if($receipt){
                    $result = $receipt->toArray();
                    $order = Order::where('order_number',$result['order_number'])->first();
                    $policy = Policy::where('policy_number',$result['policy_number'])->first();
                    $result['order_id'] = !empty($order)?$order->id:'';
                    $result['policy_id'] = !empty($policy)?$policy->id:'';
                    $result['channel'] = $channel[$result['payment_channel_code']];
                    $result['scheme'] = $scheme[$result['payment_scheme']];
                    return response()->json($result);
                }
                return json_encode(["resp_error"=>"No Data"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

 
}
