<?php

namespace App\Http\Controllers\admin;

use App\Repositories\PaymentRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use App\Models\Payment;
use App\Models\Order;
use App\Models\Policy;
use Validator;
use DateTime;
use Auth;

class PaymentController extends Controller
{

    private $menu1 = "motoradmins/payment";

    public function __construct(PaymentRepository $payment){
        $this->middleware('auth:admin');
        $this->payment = $payment;
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.payment.payment');
    }

    public function getPaymentList(Request $request){
        try{
            Log::info("request");
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                Log::info($input);
                
                $query = $this->payment->list($input);
                Log::info($query);
                return Datatables::of($query)->addColumn('action', function ($data) {
                    $html = '<a style="cursor:pointer;" class="m-a-1 action-edit" title="More Detail" data-id="'.$data->id.'"><i class="fa fa-search-plus"></i></a>';
                    return $html;
                })->editColumn('amount',function($data){
                    return number_format($data->amount,2);
                })->editColumn('created_at',function($data){
                    $return = [
                        'date' => getLocaleDate($data->created_at)." ".date("H:i:s",strtotime($data->created_at)),
                        'state' => "NORMAL"
                    ];
                    $date = new DateTime();
                    $match_date = new DateTime($data->created_at);
                    $interval = $date->diff($match_date);
                    if($interval->days == 0) {
                        $return['state'] = "NEW";
                    }
                    return json_encode($return);
                })->editColumn('paid_date',function($data){
                    return getLocaleDate($data->paid_date)." ".date("H:i:s",strtotime($data->paid_date));
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getPaymentData(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();

                $payment = Payment::where('id',$input['id'])->first();
                if($payment){
                    $result = $payment->toArray();
                    $order = Order::where('order_number',$result['order_number'])->first();
                    $policy = Policy::where('policy_number',$result['policy_number'])->first();
                    $result['order_id'] = !empty($order)?$order->id:'';
                    $result['policy_id'] = !empty($policy)?$policy->id:'';
                    return response()->json($result);
                }
                return json_encode(["resp_error"=>"No Data"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

 
}
