<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;

class AdminLoginController extends Controller
{
  public function __construct(){
      $this->middleware('guest:admin',['except' => 'logout']);
  }

  public function showLoginForm(){
      return view('admin.login.login');
  }

  public function login(Request $request){
    try{
        $input = $request->all();
        $validator = Validator::make($input, [
            'username' => 'required',
            'password' => 'required|min:4'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        if(Auth::guard('admin')->attempt(['username'=>$input['username'],'password'=>$input['password']])){
            return redirect()->intended(route('admin.dashboard'));
        }
        return redirect()->back()->withErrors(["Username or Password incorrect!"])->withInput();
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return redirect()->back()->withErrors(["Error please try again or contact administrator"])->withInput();
    }
      
  }

  public function logout(Request $request){
    try{
        Auth::guard('admin')->logout();
        //$request->session()->flush();
        //$request->session()->regenerate();
        return redirect(route('admin.login'));
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return redirect()->back()->withErrors("System Error!");
    }
        
  }


}
