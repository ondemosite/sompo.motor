<?php

namespace App\Http\Controllers\admin;

use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;
use App\Models\Order;
use App\Models\Policy;
use Validator;
use DateTime;
use Auth;
use App\Library\insuranceManager;
use App\Library\MyEmail;
use App\Library\AccessFile;

class OrderController extends Controller
{

    private $menu1 = "motoradmins/order";
    private $menu2 = "motoradmins/order/order_detail";

    public function __construct(OrderRepository $order){
        $this->middleware('auth:admin');
        $this->order = $order;
        $this->insurance_manager = new insuranceManager();  
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.order.order');
    }

    public function getOrderList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                
                $result = $this->order->getOrderList($input);

                $items = array();
                $no = $input['start'];

                foreach ($result['items'] as $item) {

                    $is_addon = "";
                    if($item->taxi_gross_premium > 0){
                        $is_addon.="taxi";
                    }
                    if($item->theft_gross_premium > 0){
                        $is_addon.=", theft";
                    }
                    
                    if($item->hb_gross_premium > 0){
                        $is_addon.=", hb";
                    }
                    if($item->carloss_gross_premium > 0){
                        $is_addon.=", carloss";
                    }

                    $brand = json_encode([
                        'brand' => $item->brand,
                        'model' => $item->model,
                        'year' => $item->year,
                        'car_licence' => $item->car_licence
                    ]);

                    $createAt = [
                        'date' => getLocaleDate($item->created_at),
                        'state' => "NORMAL"
                    ];
                    $date = new DateTime();
                    $match_date = new DateTime($item->created_at);
                    $interval = $date->diff($match_date);
                    if($interval->days == 0) {
                        $createAt['state'] = "NEW";
                    }


                    $items[] = array(
                        'no' => ++$no,
                        'order_number' => $item->order_number,
                        'name' => $item->name." ".$item->lastname,
                        'brand' => $brand,
                        'insurance_ft_si' => number_format($item->insurance_ft_si,2),
                        'is_addon' => $is_addon,
                        'is_compulsory' => !empty($item->compulsory_net_premium)?'yes':'no',
                        'payment_result' => number_format($item->payment_result,2),
                        'status' => $item->status,
                        'created_at' => json_encode($createAt),
                        'action' => '<a href="'.route('admin.get_order_data').'/'.$item->id.'" class="m-a-1 action-edit" title="More Detail" data-id="'.$item->id.'"><i class="fa fa-search-plus"></i></a>',
                    );
                }

                return array(
                    'draw' => $input['draw'],
                    'recordsTotal' => $result['total'],
                    'recordsFiltered' => $result['total'],
                    'data' => $items
                );

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getOrderData($id){
        try{
            $order = Order::where('id',$id)->first();
            if($order){
                $documentFileList = [];
                if(!empty($order->document_path_personal_id)){
                    array_push($documentFileList,$order->document_path_personal_id);
                }
                if(!empty($order->document_path_car_licence)){
                    array_push($documentFileList,$order->document_path_car_licence);
                }
                if(!empty($order->document_path_driver1_licence)){
                    array_push($documentFileList,$order->document_path_driver1_licence);
                }
                if(!empty($order->document_path_driver2_licence)){
                    array_push($documentFileList,$order->document_path_driver2_licence);
                }
                if(!empty($order->document_path_cctv_inside)){
                    array_push($documentFileList,$order->document_path_cctv_inside);
                }
                if(!empty($order->document_path_cctv_outside)){
                    array_push($documentFileList,$order->document_path_cctv_outside);
                }
                $accessFile = new AccessFile();
                $accessId = $accessFile->setToken([
                    'ref_id' => Auth::user()->id,
                    'files' => $documentFileList
                ]);
                $document = [
                    'personal_id' => $accessFile->getUrl($accessId,Auth::user()->id,$order->document_path_personal_id,'personal-release'),
                    'car_licence' => $accessFile->getUrl($accessId,Auth::user()->id,$order->document_path_car_licence,'personal-release'),
                    'driver1_licence' => !empty($order->document_path_driver1_licence)?$accessFile->getUrl($accessId,Auth::user()->id,$order->document_path_driver1_licence,'personal-release'):"",
                    'driver2_licence' => !empty($order->document_path_driver2_licence)?$accessFile->getUrl($accessId,Auth::user()->id,$order->document_path_driver2_licence,'personal-release'):"",
                ];
                $document['cctv_inside'] = !empty($order->document_path_cctv_inside)?$accessFile->getUrl($accessId,Auth::user()->id,$order->document_path_cctv_inside,'personal-release'):"";
                $document['cctv_outside'] = !empty($order->document_path_cctv_outside)?$accessFile->getUrl($accessId,Auth::user()->id,$order->document_path_cctv_outside,'personal-release'):"";
                
                return view('admin.order.order_detail',['init'=>$order,'document' => $document]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withErrors(['Not Found Data!']);
        }
    }

    public function createPolicy(Request $request){
        try{
            if($request->ajax()){
                /*
                $input = $request->all();
                $order_id = $input['order_id'];
                $order = Order::where('id',$order_id)->first();
                if(!empty($order)){
                    $payment = $order->payment()->first();
                    //Update Order ----
                    $order->status = "PAID";
                    $order->save();
                    //Update Payment
                    $payment->status = "PAID";
                    $payment->status_detail = "ชำระเงินแล้ว";
                    $payment->paid_date = date("Y-m-d H:i:s");
                    $payment->save();
                    //Create Policy
                    Log::info("--create-policy --");
                    $policy_id = $this->createPolicyData($order);
                    Log::info("--create-policy success--");
                    
                    $policy = Policy::where('id',$policy_id)->first();
                    if(!empty($policy)){
                        //Update Payment 
                        $payment->policy_number = $policy->policy_number;
                        $payment->save();

                        //Update Policy Start Date
                        $paidDate = date("Y-m-d",strtotime($payment->paid_date));
                        $timePaid = strtotime($paidDate);
                        $startDate = date("Y-m-d",strtotime($policy->insurance_start));
                        $timeStart = strtotime($startDate);

                        if($timePaid == $timeStart){
                            $policy->insurance_start = $payment->paid_date;
                            Log::info($policy->insurance_start);
                            $policy->insurance_expire = date('Y-m-d H:i:s',strtotime("+1 year",strtotime($policy->insurance_start)));
                        }else{
                            $newDate = date("Y-m-d",strtotime($policy->insurance_start));
                            $policy->insurance_start = $newDate." 00:01:01";
                            $policy->insurance_expire = date('Y-m-d H:i:s',strtotime("+1 year",strtotime($policy->insurance_start)));
                        }
                        //Update Compulsory Start Date
                        if(!empty($policy->policy_com_number)){
                            $startDate = date("Y-m-d",strtotime($policy->compulsory_start));
                            $timeStart = strtotime($startDate);

                            if($timePaid == $timeStart){
                                $policy->compulsory_start = $payment->paid_date;
                                $policy->compulsory_expire = date('Y-m-d H:i:s',strtotime("+1 year",strtotime($policy->compulsory_start)));
                            }else{
                                $newDate = date("Y-m-d",strtotime($policy->compulsory_start));
                                $policy->compulsory_start = $newDate." 00:01:01";
                                $policy->compulsory_expire = date('Y-m-d H:i:s',strtotime("+1 year",strtotime($policy->compulsory_start)));
                            }
                        }
                        $policy->save();
                        //------------------------------

                        // //Create Receipt
                        // if($policy_id!=false){
                        //     Log::info("--create-receipt--");
                        //     $id = $this->createReceipt($order,$policy,$payment,$input);
                        //     Log::info("--create-receipt success--");
                        // }
                        
                        // //generatePDF
                        // Log::info("-- Generate PDF --");
                        // $this->insurance_manager->generatePDF($policy);
                        // Log::info("-- Generate PDF SUCCESS --");
                        
                        // //generateEpass
                        // Log::info("-- Generate E-pass --");
                        // $epass_link = $this->generateEpass($policy_id);
                        // Log::info("-- Generate E-pass SUCCESS --");
                        
                        // //Send SMS 
                        // Log::info("-- Send SMS --");
                        // if(!empty($epass_link)){
                        //     $this->sendSMS($policy_id,$epass_link);
                        //     Log::info("SMS PASSESS:".$epass_link);
                        // }
                        // Log::info("-- Send SMS SUCCESS --");
                        
                        // //Send to Endrose
                        // Log::info("-- Sending Endorse --");
                        // //$this->sendEndorse($policy_id);
                        // Log::info("-- Send Endorse Success --");

                        // //Send Email
                        // Log::info("-- Sending Email --");
                        // $this->sendEmail($policy_id);
                        // Log::info("-- Send Email Success --");
                    }

                    return json_encode(["status"=>'success']);

                }   */
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    private function createPolicyData($order){
        return $this->insurance_manager->createPolicy($order);
    }

    private function createReceipt($order,$policy_id,$input){
            $policy = Policy::where('id',$policy_id)->first();
            $receipt_data = [
                'receipt_number' => $policy->tax_note_number,
                'order_number' => $order->order_number,
                'policy_id' => $policy->id,
                'amount' => floatval($input['amount'])/100,
                'payment_channel_code' => $input['payment_channel'],
                'credit_number' => $input['masked_pan'],
                'payment_scheme' => $input['payment_scheme'],
            ];
            $this->insurance_manager->createReceipt($receipt_data);
    }

    private function sendEmail($policy){
        $mail = new MyEmail();
        return $mail->sendEmail($policy,null);
    }
    
    private function generateEpass($policy_id){
        $mk = new Mpasskit();
        return $mk->createPass($policy_id);
    }
    
    private function sendSMS($policy_id,$epass_link){
        $sms = new SMS();
        return $sms->sendSMS($policy_id,$epass_link);
    }
    
    private function sendEndorse($policy_id){
        $endorse = new Endorse();
        if($endorse->request($policy_id)!=false){
            $policy = Policy::where('id',$policy_id)->first();
            $policy->update_core_status = 1;
            $policy->save();
        }else{
            $endorse->alertToAdmin($policy_id);
        }
        return true;
    }

 
}
