<?php

namespace App\Http\Controllers\admin;

use App\Repositories\VehicleRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use App\Models\Vehicle;
use App\Models\DatabaseActive;

use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;
use Excel;
use Illuminate\Support\Facades\Storage;

class VehicleController extends Controller
{

  private $menu1 = "motoradmins/vehicle/vehicle";
  private $menu2 = "motoradmins/vehicle/brand";
  private $menu3 = "motoradmins/vehicle/model";
  private $vehicleTableActive = null;

  public function __construct(){
    $this->middleware('auth:admin');
    $this->vehicleTableActive = getDatabaseActive('vehicle');
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    } 
    
    $init['select_brand'] = null;
    $init['select_brand'] = VehicleBrand::pluck('name','id')->toArray();
    $init['select_body_type'] = ["SEDAN"=>"SEDAN","PICKUP"=>"PICKUP"];
    $init['select_year'] = array_combine(range(date('Y'),date('Y')-15),range(date('Y'),date('Y')-15));

    return view('admin.vehicle.vehicle',['init'=>$init]);
  }

    public function getVehicleList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $repository = new VehicleRepository();
                $result = $repository->list_vehicle($input);
                $items = array();
                $no = $input['start'];

                foreach ($result['items'] as $item) {

                    $items[] = array(
                        'no' => ++$no,
                        'brand_name' => $item->brand_name()->first()->name,
                        'model_name' => $item->model_name()->first()->name,
                        'year' => $item->year,
                        'body_type' => $item->body_type,
                        'model_type' => $item->model_type,
                        'mortor_code_av' => $item->mortor_code_av,
                        'mortor_code_ac' => $item->mortor_code_ac,
                        'action' => '<a href="'.route('admin.vehicle_form').'/'.$item->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$item->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$item->id.'" data-name="'.$item->model_type.'"><i class="fa fa-trash"></i></a>'
                    );
                }

                return array(
                    'draw' => $input['draw'],
                    'recordsTotal' => $result['total'],
                    'recordsFiltered' => $result['total'],
                    'data' => $items
                );

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getImportedVehicleList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){

                $input = $request->all();
                $repository = new VehicleRepository();
                $result = $repository->list_imported_vehicle($input);

                $items = array();
                $no = $input['start'];

                foreach ($result['items'] as $item) {

                    $vehicleBrand = new vehicleBrand;
                    $this->vehicleTableActive==1?$vehicleBrand->setTable('vehicle_brand_second'):$vehicleBrand->setTable('vehicle_brand');
                    $branName = $vehicleBrand->where('id',$item->brand_id)->first()->name;

                    $vehicleModel = new vehicleModel;
                    $this->vehicleTableActive==1?$vehicleModel->setTable('vehicle_model_second'):$vehicleModel->setTable('vehicle_model');
                    $modelName = $vehicleModel->where('id',$item->model_id)->first()->name;

                    $items[] = array(
                        'no' => ++$no,
                        'brand_name' => $branName,
                        'model_name' => $modelName,
                        'year' => $item->year,
                        'body_type' => $item->body_type,
                        'model_type' => $item->model_type,
                        'mortor_code_av' => $item->mortor_code_av,
                        'mortor_code_ac' => $item->mortor_code_ac,
                        'cc' => $item->cc,
                        'tons' => $item->tons,
                        'car_seat' => $item->car_seat,
                        'driver_passenger' => $item->driver_passenger,
                        'red_plate' => $item->red_plate,
                        'used_car' => $item->used_car,
                        'car_age' => $item->car_age
                    );

                }

                return array(
                    'draw' => $input['draw'],
                    'recordsTotal' => $result['total'],
                    'recordsFiltered' => $result['total'],
                    'data' => $items
                );

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


  public function getSelectModel(Request $request){
    try{
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'brand_id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $response = VehicleModel::where('brand_id',$input['brand_id'])->pluck('name','id')->toArray();
            if(!empty($response)){
                return json_encode($response);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }

  public function deleteVehicle(Request $request){
    try{
        
        if(!has_permission($this->menu1,["view","delete"])){
          return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){

          $input = $request->all();
          $validator = Validator::make($input, [
            'id' => 'required'
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }
          
          Vehicle::where('id', '=', $input['id'])->delete();
          return json_encode(["status"=>"success"]);
        }       
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}

    public function vehicleForm($id = null){
        try{
            if(!has_permission($this->menu1,["view","edit","add"])){
                return view('admin.common.permission_denied');
            }

            $return['id'] = $id;
            if(!empty($id)){ //Edit Mode
                $data = vehicle::where('id','=',$id)->first();
                if($data){
                    $return['id'] = !empty($data->id)?$data->id:'';
                    $return['brand_id'] = !empty($data->brand_id)?$data->brand_id:null;
                    $return['model_id'] = !empty($data->model_id)?$data->model_id:null;
                    $return['year'] = !empty($data->year)?$data->year:null;
                    $return['body_type'] = !empty($data->body_type)?$data->body_type:null;
                    $return['model_type'] = !empty($data->model_type)?$data->model_type:null;
                    $return['model_type_full'] = !empty($data->model_type_full)?$data->model_type_full:null;
                    $return['mortor_code_av'] = !empty($data->mortor_code_av)?$data->mortor_code_av:null;
                    $return['mortor_code_ac'] = !empty($data->mortor_code_ac)?$data->mortor_code_ac:null;
                    $return['cc'] = !empty($data->cc)?$data->cc:null;
                    $return['tons'] = !empty($data->tons)?$data->tons:null;
                    $return['car_seat'] = !empty($data->car_seat)?$data->car_seat:null;
                    $return['driver_passenger'] = !empty($data->driver_passenger)?$data->driver_passenger:null;
                    $return['red_plate'] = !empty($data->red_plate)?$data->red_plate:null;
                    $return['used_car'] = !empty($data->used_car)?$data->used_car:null;
                    $return['car_age'] = !empty($data->car_age)?$data->car_age:null;
                    $return['status'] = !empty($data->status)?$data->status:0;
                }else{
                    return back()->withInput()->withErrors(['No Data Found!']);
                }
            }

            $init = [];
            $init['select_brand'] = VehicleBrand::pluck('name','id')->toArray();
            $init['select_body_type'] = ["SEDAN"=>"SEDAN","PICKUP"=>"PICKUP"];
            $init['select_year'] = array_combine(range(date('Y'),date('Y')-15),range(date('Y'),date('Y')-15));
            return view('admin.vehicle.vehicle_form',["data"=>$return,"init"=>$init]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }

    public function getDataVehicle(Request $request){
        try{
          if(!has_permission($this->menu1)){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
              'id' => 'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
    
            $response = Vehicle::where('id',$input['id'])->first();
            if(!empty($response)){
              return json_encode($response);
            }
    
          }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
        }
    }

    public function saveVehicle(Request $request){
        try{
              if(!has_permission($this->menu1,["view","edit","add"])){
                  return view('admin.common.permission_denied');
              }
    
              $input = $request->all();
              // Validate 
              $filter_validate = [
                  'brand_id'=>'required',
                  'model_id'=>'required',
                  'year'=>'required|numeric',
                  'body_type'=>'required',
                  'model_type'=>'required',
                  'model_type_full'=>'required',
                  'mortor_code_av'=>'required|numeric',
                  'mortor_code_ac'=>'required|numeric',
                  'cc'=>'required|numeric',
                  'tons'=>'required|numeric',
                  'car_seat'=>'required|numeric',
                  'driver_passenger'=>'required',
                  'red_plate'=>'required|numeric',
                  'used_car'=>'required|numeric',
                  'car_age'=>'required|numeric',
              ];
    
              $validator = Validator::make($input,$filter_validate);
              if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
              }
    
              $data = array(
                'brand_id'=>$input['brand_id'],
                'model_id'=>$input['model_id'],
                'year'=>$input['year'],
                'body_type'=>$input['body_type'],
                'model_type'=>trim($input['model_type']),
                'model_type_full' => trim($input['model_type_full']),
                'mortor_code_av'=>$input['mortor_code_av'],
                'mortor_code_ac'=>$input['mortor_code_ac'],
                'cc'=>$input['cc'],
                'tons'=>$input['tons'],
                'car_seat'=>$input['car_seat'],
                'driver_passenger'=>$input['driver_passenger'],
                'red_plate'=>$input['red_plate'],
                'used_car' =>$input['used_car'],
                'car_age' =>$input['car_age'],
                'status'=>!empty($input['status'])?1:0,
                'modified_by' => Auth::guard('admin')->user()->id,
              );

              if(!empty($input['vehicle_id'])){ //Edit mode
                Vehicle::where('id', '=', $input['vehicle_id'])->update($data);
                return redirect(route('admin.vehicle_form')."/".$input['vehicle_id'])->with('status', 'Updated Vehicle Success!!');
              }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                $id = Vehicle::create($data)->id;
                if(!empty($id)){
                    return redirect(route('admin.vehicle_form')."/".$id)->with('status', 'Created Vehicle Success!!');
                }
              }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }

  public function vehicleBrand(){
    if(!has_permission($this->menu2)){
        return view('admin.common.permission_denied');
    }
    return view('admin.vehicle.vehicle_brand');
  }

  public function vehicleModel(){
    if(!has_permission($this->menu3)){
        return view('admin.common.permission_denied');
    }
    $init['brand'] = VehicleBrand::pluck('name','id')->toArray();
    return view('admin.vehicle.vehicle_model',["init"=>$init]);
  }

  public function getBrandList(Request $request){
      try{
        if(!has_permission($this->menu2)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $vehicleBrand = VehicleBrand::All();
          return Datatables::of($vehicleBrand)->addColumn('action', function ($data) {
              return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
          })->make(true);
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getDataBrand(Request $request){
      try{
        if(!has_permission($this->menu2)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $input = $request->all();
          // Validate 
          $validator = Validator::make($input, [
            'id' => 'required',
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }

          $response = VehicleBrand::where('id',$input['id'])->first();
          if(!empty($response)){
            return json_encode($response);
          }

        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function saveBrand(Request $request){
      try{
          if($request->ajax()){
            
            if(!has_permission($this->menu2,["view","edit","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $input = $request->all();
            
            // Validate 
            $validator = Validator::make($input, [
                'name'=>'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'name'=> $input['name'],
                'top_order'=>!empty($input['top_order'])?$input['top_order']:null,
                'updated_by' => Auth::guard('admin')->user()->id
            );
            if(!empty($input['id'])){ //Edit mode
                VehicleBrand::where('id', '=', $input['id'])->update($data);
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                VehicleBrand::create($data);
            }
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function deleteBrand(Request $request){
      try{
          
          if(!has_permission($this->menu2,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){

            $input = $request->all();
            $validator = Validator::make($input, [
              'id' => 'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            VehicleBrand::where('id', '=', $input['id'])->delete();
            
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getModelList(Request $request){
      try{
        if(!has_permission($this->menu3)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $input = $request->all();
          $repository = new VehicleRepository();
          $data_resp = $repository->list_model($input);
          return Datatables::of($data_resp)->addColumn('action', function ($data) {
            return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
          })->addColumn('brand_name',function($data){
              return $data->brand_name()->first()->name;
          })->make(true);
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getDataModel(Request $request){
      try{
        if(!has_permission($this->menu3)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $input = $request->all();
          // Validate 
          $validator = Validator::make($input, [
            'id' => 'required',
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }

          $response = VehicleModel::where('id',$input['id'])->first();
          if(!empty($response)){
            return json_encode($response);
          }

        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function saveModel(Request $request){
      try{
          if($request->ajax()){
            
            if(!has_permission($this->menu3,["view","edit","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $input = $request->all();
            
            // Validate 
            $validator = Validator::make($input, [
                'name'=>'required',
                'brand_id'=>'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'name'=> $input['name'],
                'brand_id'=>!empty($input['brand_id'])?$input['brand_id']:null,
                'updated_by' => Auth::guard('admin')->user()->id
            );
            if(!empty($input['id'])){ //Edit mode
                VehicleModel::where('id', '=', $input['id'])->update($data);
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                VehicleModel::create($data);
            }
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function deleteModel(Request $request){
      try{
          
          if(!has_permission($this->menu3,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){

            $input = $request->all();
            $validator = Validator::make($input, [
              'id' => 'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            VehicleModel::where('id', '=', $input['id'])->delete();
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }


    public function chunksVehicle(Request $request){
        try{
            Log::info("chunks");
            ini_set('memory_limit', '-1');
            if(!has_permission($this->menu1,["view","edit","add","delete"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            
            $validator = Validator::make(
                [
                    'file'      => $request->file,
                    'extension' => strtolower($request->file->getClientOriginalExtension()),
                ],
                [
                    'file'          => 'required',
                    'extension'      => 'required|in:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp',
                ]
            );

            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
        
            $path = Storage::disk('upload-chunks')->putFileAs('', $request->file('file'),'car.xls');
            if (!empty($path)) {
                return json_encode(["status"=>"SUCCESS"]);
            }else{
                return json_encode(["resp_error"=>"Upload File Failed!, Try Again."]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    public function importXLS(Request $request){
        try{
            Log::info("importing");
            ini_set('memory_limit', '-1');
            $input = $request->all();
            $directory = Storage::disk('upload-chunks')->getDriver()->getAdapter()->getPathPrefix();
            $filename = "car.xls";

            if($this->setVehicleData($directory."/".$filename)){
                return json_encode(["status"=>"SUCCESS"]);
            }else{
                // Rollback Process
                return json_encode(["resp_error"=>"Fail to import, Try again."]);
            }
            return json_encode(["resp_error"=>"Fail to import, Try again."]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    /*
    public function backupVehicle(){
        try{
            Log::info("backup");
            ini_set('memory_limit', '-1');
            $data = Vehicle::orderBy('brand_id','asc')->orderBy('id','asc')->get();
            if(!empty($data)){
                if(!has_permission($this->menu1)){
                    return json_encode(["resp_error"=>trans('common.permission_denied')]);
                }
                $path = public_path(Config::get('path.import_path_backup'));

                $response = Excel::create('car_'.date('Y.m.d'), function($excel) use($data){
                    $excel->sheet('Sheet1', function($sheet) use($data) {
                        $sheet->loadView('admin.vehicle.excel.backup',["init"=>$data]);
                    });
                })->store('csv',$path,true);
                if(isset($response['full'])){
                    return json_encode(["status"=>"SUCCESS","path"=>$response['full']]);
                }
            }
            return json_encode(["resp_error"=>"Backup File Failed!, Try Again."]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        } 
    }*/

    private function setVehicleData($path){
        try{
            set_time_limit(0);
            ini_set('memory_limit', '-1');
            session()->forget('database_active');

            if(!Storage::exists($path)) {

                //Remove Data
                $vehicle = new vehicle;
                $this->vehicleTableActive==1?$vehicle->setTable('vehicle_second'):$vehicle->setTable('vehicle');
                $vehicle->whereNotNull('id')->delete();

                $vehicleBrand = new vehicleBrand;
                $this->vehicleTableActive==1?$vehicleBrand->setTable('vehicle_brand_second'):$vehicleBrand->setTable('vehicle_brand');
                $vehicleBrand->whereNotNull('id')->delete();

                $vehicleModel = new vehicleModel;
                $this->vehicleTableActive==1?$vehicleModel->setTable('vehicle_model_second'):$vehicleModel->setTable('vehicle_model');
                $vehicleModel->whereNotNull('id')->delete();

                Excel::load($path,function($reader){
                    $excelData = $reader->get();
                    $brand_store = [];
                    $model_store = [];
                    $brand_key = [];
                    $model_key = [];
                    $loop_index = 0;
                    // Start Loop
                    foreach($excelData as $key => $data){
                        $brand = "";
                        $model = "";
                        $sql = "";

                        if(!in_array($data->make,$brand_store)){ //ถ้ายังไม่เคยตรวจสอบ Brand
                            $vehicleBrand = new vehicleBrand;
                            $this->vehicleTableActive==1?$vehicleBrand->setTable('vehicle_brand_second'):$vehicleBrand->setTable('vehicle_brand');
                            $vehicleBrand->name = $data->make;
                            $vehicleBrand->save();
                            $brand = $vehicleBrand->id;
                            array_push($brand_store,$data->make);
                            $brand_key['key_'.$data->make] = $brand;
                        }else{
                            $brand = $brand_key['key_'.$data->make];
                        }
                        if(!in_array($data->model,$model_store) && !empty($brand)){ //ถ้ายังไม่เคยตรวจสอบ Model
                            $vehicleModel = new vehicleModel;
                            $this->vehicleTableActive==1?$vehicleModel->setTable('vehicle_model_second'):$vehicleModel->setTable('vehicle_model');
                            $vehicleModel->brand_id = $brand;
                            $vehicleModel->name = $data->model;
                            $vehicleModel->save();
                            $model = $vehicleModel->id;
                            array_push($model_store,$data->model);
                            $model_key['key_'.$data->model] = $model;
                        }else{
                            $model = $model_key['key_'.$data->model];
                        }

                        // Create Process
                        if(!empty($brand) && !empty($model)){
                            $vehicle = new Vehicle;
                            $vehicle = $this->vehicleTableActive==1?$vehicle->setTable('vehicle_second'):$vehicle->setTable('vehicle');
                            $vehicle->id = $loop_index;
                            $vehicle->brand_id = $brand;
                            $vehicle->model_id = $model;
                            $vehicle->year = intval($data->year);
                            $vehicle->body_type = strtoupper($data->body_type);
                            $vehicle->model_type = $data->model_type_2;
                            $vehicle->model_type_full = $data->model_type;
                            $vehicle->mortor_code_av = intval($data->motor_code_av);
                            $vehicle->mortor_code_ac = floatval($data->motor_code_ac);
                            $vehicle->cc = intval($data->cc);
                            $vehicle->tons = floatval($data->tons);
                            $vehicle->car_seat = intval($data->car_seat);
                            $vehicle->driver_passenger = $data->passenger;
                            $vehicle->red_plate = intval($data->red_plate);
                            $vehicle->used_car = intval($data->used_car);
                            $vehicle->car_age = intval($data->car_age);
                            $vehicle->save();
                        }else{
                            Log::info("SOME:".$loop_index);
                            return false;
                        }

                        $loop_index++;

                    }
                });

            }
            return true;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        } 
    }

    public function submitImportChange(){
        try{
            $databaseActive = DatabaseActive::where('id',1)->first();
            $databaseActive->vehicle_active = $databaseActive->vehicle_active==1?2:1;
            $databaseActive->save();
            return json_encode(["status"=>"SUCCESS"]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        } 
    }

    public function export(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
    
            $input = $request->all();
            $repository = new VehicleRepository();
            $result = $repository->list_export();
    
            if(!empty($result)){
                Excel::create('Car_pricing_'.date('Ymd'), function($excel) use($result){
                    $excel->sheet('Sheet1', function($sheet) use($result) {
                        $sheet->loadView('admin.vehicle.excel.export',["data"=>$result]);
                    });
                })->export('xls');
            }else{
                return back()->withErrors([
                    "error" => 'Error Data Not Found'
                ])->withInput();
            }
        }catch (\Exception $e) {
            Log::info($e->getMessage());
            return back()->withErrors([
                "error" => 'Error Please Contact Administrator'
            ])->withInput();
        }
    }








 
}
