<?php

namespace App\Http\Controllers\admin;

use App\Repositories\GarageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Banner;
use App\Models\Order;
use App\Models\Policy;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Storage;
use PDF;
use Illuminate\Http\File;
use FPDI_Protection;

class PdfController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

     //mt1-01
    public function pdf($id){
        if(!empty($id)){
            if($id==1){
                /* Initial PDF */
                $pdf = PDF::loadView('admin.pdf.mt-1-01');
                $pdf->setPaper('A4');
                
                if(Storage::disk('pdf')->exists('/plan/MT-1-02-PLAN.pdf')){ //Need Merge Files
                    // Save Buffer File 
                    $directory = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
                    $file_buffer_name = getUniqueFilename($directory."/buffer/","pdf");
                    Storage::disk('pdf')->put('/buffer/'.$file_buffer_name, $pdf->output()); //Save
                    
                    // Merge PDF Files
                    $mt_1_01 = $directory."/buffer/".$file_buffer_name;
                    $mt_1_02 = $directory."/plan/MT-1-02-PLAN.pdf";
                    $desc_path =  $directory."/buffer/".getUniqueFilename($directory."/buffer/","pdf");
                    $pdfMerge = new \LynX39\LaraPdfMerger\PdfManage;
                    $pdfMerge->addPDF($mt_1_01, 'all');
                    $pdfMerge->addPDF($mt_1_02, 'all');
                    $pdfMerge->merge('file',$desc_path, 'P'); // 'P = portait' 
                    $pdfMerge = null;

                    // Encrypted PDF File
                    $user_password = "";
                    $admin_password = "";
                    $encrypted_pdf = $directory."/user/".getUniqueFilename($directory."/user/","pdf");
                    $this->pdfEncrypt($desc_path,$encrypted_pdf,$user_password,$admin_password);

                }else{
                    $user_password = "";
                    $admin_password = "";
                    $directory = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
                    $file_name = getUniqueFilename($directory."/user/","pdf");
                    $pdf->setProtection($user_password,$admin_password,["print","copy","modify"]);
                    Storage::disk('pdf')->put('/user/'.$file_name, $pdf->output()); //Save
                }
            }else if($id==2){
                /* Initial PDF */
                $fix = ['tax_id_no' => '0107555000287'];
                $data = Policy::where('id',22)->first();
                $pdf = PDF::loadView('admin.pdf.mt-schedule-original-receipt',['init'=>$data,'fix'=>$fix,'doc_type'=>"invoice"])->setPaper('A4');
                $pdf->setPaper('A4');
                //return view('admin.pdf.mt-flood');
                return $pdf->stream();
            }else if($id==3){
                /* Initial PDF */
                $pdf = PDF::loadView('admin.pdf.mt-1-04');
                $pdf->setPaper('A4');
            }
        }
        
    }

    public function testEncryped(){
        $directory = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
        $file_path = $directory."/user/1.pdf";
        $file_path_2 = $directory."/user/1.pdf";
        $user_password = "01Feb2014";
        $admin_password = "01Feb2014";
        $this->pdfEncrypt($file_path,$file_path_2,$user_password,$admin_password);
    }

    function pdfEncrypt ($origFile, $destFile, $owner_password=null, $user_password=null){
        $pdf = new FPDI_Protection();
        $pagecount = $pdf->setSourceFile($origFile);
        
        for ($loop = 1; $loop <= $pagecount; $loop++) {
            $tplidx = $pdf->importPage($loop);
            $pdf->addPage();
            $pdf->useTemplate($tplidx);
        }
        $pdf->SetProtection(\FPDI_Protection::FULL_PERMISSIONS,$user_password,$owner_password);
        $pdf->Output($destFile, 'F');
        return true;
    }

}
