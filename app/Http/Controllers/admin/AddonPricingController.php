<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\AddonCarLoss;
use App\Models\AddonHb;
use App\Models\AddonTaxi;
use App\Models\AddonTheft;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Validator;
use Auth;

class AddonPricingController extends Controller
{

    private $menu1 = "motoradmins/addon/carloss";
    private $menu2 = "motoradmins/addon/hb";
    private $menu3 = "motoradmins/addon/taxi";
    private $menu4 = "motoradmins/addon/theft";

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index_carloss(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        } 
        return view('admin.addon.addon_carloss');
    }

    public function index_hb(){
        if(!has_permission($this->menu2)){
            return view('admin.common.permission_denied');
        } 
        return view('admin.addon.addon_hb');
    }

    public function index_taxi(){
        if(!has_permission($this->menu3)){
            return view('admin.common.permission_denied');
        } 
        return view('admin.addon.addon_taxi');
    }

    public function index_theft(){
        if(!has_permission($this->menu4)){
            return view('admin.common.permission_denied');
        } 
        return view('admin.addon.addon_theft');
    }

    public function list_carloss(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $data_resp = AddonCarLoss::All();
                return Datatables::of($data_resp)->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
                })->editColumn('created_by', function ($data) {
                    return $data->admin()->first()->username;
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
        
    }

    public function list_hb(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data_resp = AddonHb::All();
                return Datatables::of($data_resp)->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
                })->editColumn('created_by', function ($data) {
                    return $data->admin()->first()->username;
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function list_taxi(Request $request){
        try{
            if(!has_permission($this->menu3)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data_resp = AddonTaxi::All();
                return Datatables::of($data_resp)->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
                })->editColumn('created_by', function ($data) {
                    return $data->admin()->first()->username;
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
        
    }

    public function list_theft(Request $request){
        try{
            if(!has_permission($this->menu4)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data_resp = AddonTheft::All();
                return Datatables::of($data_resp)->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
                })->editColumn('created_by', function ($data) {
                    return $data->admin()->first()->username;
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function get_carloss(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
    
                $response = AddonCarLoss::where('id',$input['id'])->first();
                if(!empty($response)){
                    return json_encode($response);
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function get_hb(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
    
                $response = AddonHb::where('id',$input['id'])->first();
                if(!empty($response)){
                    return json_encode($response);
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function get_taxi(Request $request){
        try{
            if(!has_permission($this->menu3)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
    
                $response = AddonTaxi::where('id',$input['id'])->first();
                if(!empty($response)){
                    return json_encode($response);
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function get_theft(Request $request){
        try{
            if(!has_permission($this->menu4)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
    
                $response = AddonTheft::where('id',$input['id'])->first();
                if(!empty($response)){
                    return json_encode($response);
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
        
    }

    public function save_carloss(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                if(!has_permission($this->menu1,["view","edit","add"])){
                    return json_encode(["resp_error"=>trans('common.permission_denied')]);
                }
    
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'ft_si'=>'required|numeric',
                    'total_si'=>'required|numeric',
                    'normal'=>'required|numeric',
                    'under29'=>'required|numeric',
                    'over29'=>'required|numeric',
                    'cctv'=>'required|numeric',
                    'net_normal'=>'required|numeric',
                    'net_under29'=>'required|numeric',
                    'net_over29'=>'required|numeric',
                    'net_cctv'=>'required|numeric',
                    'stamp_normal'=>'required|numeric',
                    'stamp_under29'=>'required|numeric',
                    'stamp_over29'=>'required|numeric',
                    'stamp_cctv'=>'required|numeric',
                    'vat_normal'=>'required|numeric',
                    'vat_under29'=>'required|numeric',
                    'vat_over29'=>'required|numeric',
                    'vat_cctv'=>'required|numeric'
                ]);
                if ($validator->fails()) {
                  $validate_first = $validator->errors()->all();
                  return json_encode(["resp_error"=>$validate_first[0]]);
                }
                $data = array(
                    'ft_si'=>$input['ft_si'],
                    'total_si'=>$input['total_si'],
                    'normal'=>$input['normal'],
                    'under29'=>$input['under29'],
                    'over29'=>$input['over29'],
                    'cctv'=>$input['cctv'],
                    'net_normal'=>$input['net_normal'],
                    'net_under29'=>$input['net_under29'],
                    'net_over29'=>$input['net_over29'],
                    'net_cctv'=>$input['net_cctv'],
                    'stamp_normal'=>$input['stamp_normal'],
                    'stamp_under29'=>$input['stamp_under29'],
                    'stamp_over29'=>$input['stamp_over29'],
                    'stamp_cctv'=>$input['stamp_cctv'],
                    'vat_normal'=>$input['vat_normal'],
                    'vat_under29'=>$input['vat_under29'],
                    'vat_over29'=>$input['vat_over29'],
                    'vat_cctv'=>$input['vat_cctv'],
                    'updated_by' => Auth::guard('admin')->user()->id
                );
                if(!empty($input['id'])){ //Edit mode
                    AddonCarLoss::where('id', '=', $input['id'])->update($data);
                }else{ //Add mode
                    $data['created_by'] = Auth::guard('admin')->user()->id;
                    AddonCarLoss::create($data);
                }
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
        
    }

    public function save_hb(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                if(!has_permission($this->menu2,["view","edit","add"])){
                    return json_encode(["resp_error"=>trans('common.permission_denied')]);
                }
    
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'sum_insured'=>'required|numeric',
                    'normal'=>'required|numeric',
                    'under29'=>'required|numeric',
                    'over29'=>'required|numeric',
                    'cctv'=>'required|numeric',
                    'net_normal'=>'required|numeric',
                    'net_under29'=>'required|numeric',
                    'net_over29'=>'required|numeric',
                    'net_cctv'=>'required|numeric',
                    'stamp_normal'=>'required|numeric',
                    'stamp_under29'=>'required|numeric',
                    'stamp_over29'=>'required|numeric',
                    'stamp_cctv'=>'required|numeric',
                    'vat_normal'=>'required|numeric',
                    'vat_under29'=>'required|numeric',
                    'vat_over29'=>'required|numeric',
                    'vat_cctv'=>'required|numeric'
                ]);
                if ($validator->fails()) {
                  $validate_first = $validator->errors()->all();
                  return json_encode(["resp_error"=>$validate_first[0]]);
                }
                $data = array(
                    'sum_insured'=>$input['sum_insured'],
                    'normal'=>$input['normal'],
                    'under29'=>$input['under29'],
                    'over29'=>$input['over29'],
                    'cctv'=>$input['cctv'],
                    'net_normal'=>$input['net_normal'],
                    'net_under29'=>$input['net_under29'],
                    'net_over29'=>$input['net_over29'],
                    'net_cctv'=>$input['net_cctv'],
                    'stamp_normal'=>$input['stamp_normal'],
                    'stamp_under29'=>$input['stamp_under29'],
                    'stamp_over29'=>$input['stamp_over29'],
                    'stamp_cctv'=>$input['stamp_cctv'],
                    'vat_normal'=>$input['vat_normal'],
                    'vat_under29'=>$input['vat_under29'],
                    'vat_over29'=>$input['vat_over29'],
                    'vat_cctv'=>$input['vat_cctv'],
                    'updated_by' => Auth::guard('admin')->user()->id
                );
                if(!empty($input['id'])){ //Edit mode
                    AddonHb::where('id', '=', $input['id'])->update($data);
                }else{ //Add mode
                    $data['created_by'] = Auth::guard('admin')->user()->id;
                    AddonHb::create($data);
                }
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
        
    }

    public function save_taxi(Request $request){
        try{
            if(!has_permission($this->menu3)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                if(!has_permission($this->menu3,["view","edit","add"])){
                    return json_encode(["resp_error"=>trans('common.permission_denied')]);
                }
    
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'sum_insured'=>'required|numeric',
                    'normal'=>'required|numeric',
                    'under29'=>'required|numeric',
                    'over29'=>'required|numeric',
                    'cctv'=>'required|numeric',
                    'net_normal'=>'required|numeric',
                    'net_under29'=>'required|numeric',
                    'net_over29'=>'required|numeric',
                    'net_cctv'=>'required|numeric',
                    'stamp_normal'=>'required|numeric',
                    'stamp_under29'=>'required|numeric',
                    'stamp_over29'=>'required|numeric',
                    'stamp_cctv'=>'required|numeric',
                    'vat_normal'=>'required|numeric',
                    'vat_under29'=>'required|numeric',
                    'vat_over29'=>'required|numeric',
                    'vat_cctv'=>'required|numeric'
                ]);
                if ($validator->fails()) {
                  $validate_first = $validator->errors()->all();
                  return json_encode(["resp_error"=>$validate_first[0]]);
                }
                $data = array(
                    'sum_insured'=>$input['sum_insured'],
                    'normal'=>$input['normal'],
                    'under29'=>$input['under29'],
                    'over29'=>$input['over29'],
                    'cctv'=>$input['cctv'],
                    'net_normal'=>$input['net_normal'],
                    'net_under29'=>$input['net_under29'],
                    'net_over29'=>$input['net_over29'],
                    'net_cctv'=>$input['net_cctv'],
                    'stamp_normal'=>$input['stamp_normal'],
                    'stamp_under29'=>$input['stamp_under29'],
                    'stamp_over29'=>$input['stamp_over29'],
                    'stamp_cctv'=>$input['stamp_cctv'],
                    'vat_normal'=>$input['vat_normal'],
                    'vat_under29'=>$input['vat_under29'],
                    'vat_over29'=>$input['vat_over29'],
                    'vat_cctv'=>$input['vat_cctv'],
                    'updated_by' => Auth::guard('admin')->user()->id
                );
                if(!empty($input['id'])){ //Edit mode
                    AddonTaxi::where('id', '=', $input['id'])->update($data);
                }else{ //Add mode
                    $data['created_by'] = Auth::guard('admin')->user()->id;
                    AddonTaxi::create($data);
                }
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
        
    }

    public function save_theft(Request $request){
        try{
            if(!has_permission($this->menu4)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                if(!has_permission($this->menu4,["view","edit","add"])){
                    return json_encode(["resp_error"=>trans('common.permission_denied')]);
                }
    
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'sum_insured'=>'required|numeric',
                    'normal'=>'required|numeric',
                    'under29'=>'required|numeric',
                    'over29'=>'required|numeric',
                    'cctv'=>'required|numeric',
                    'net_normal'=>'required|numeric',
                    'net_under29'=>'required|numeric',
                    'net_over29'=>'required|numeric',
                    'net_cctv'=>'required|numeric',
                    'stamp_normal'=>'required|numeric',
                    'stamp_under29'=>'required|numeric',
                    'stamp_over29'=>'required|numeric',
                    'stamp_cctv'=>'required|numeric',
                    'vat_normal'=>'required|numeric',
                    'vat_under29'=>'required|numeric',
                    'vat_over29'=>'required|numeric',
                    'vat_cctv'=>'required|numeric'
                ]);
                if ($validator->fails()) {
                  $validate_first = $validator->errors()->all();
                  return json_encode(["resp_error"=>$validate_first[0]]);
                }
                $data = array(
                    'sum_insured'=>$input['sum_insured'],
                    'normal'=>$input['normal'],
                    'under29'=>$input['under29'],
                    'over29'=>$input['over29'],
                    'cctv'=>$input['cctv'],
                    'net_normal'=>$input['net_normal'],
                    'net_under29'=>$input['net_under29'],
                    'net_over29'=>$input['net_over29'],
                    'net_cctv'=>$input['net_cctv'],
                    'stamp_normal'=>$input['stamp_normal'],
                    'stamp_under29'=>$input['stamp_under29'],
                    'stamp_over29'=>$input['stamp_over29'],
                    'stamp_cctv'=>$input['stamp_cctv'],
                    'vat_normal'=>$input['vat_normal'],
                    'vat_under29'=>$input['vat_under29'],
                    'vat_over29'=>$input['vat_over29'],
                    'vat_cctv'=>$input['vat_cctv'],
                    'updated_by' => Auth::guard('admin')->user()->id
                );
                if(!empty($input['id'])){ //Edit mode
                    AddonTheft::where('id', '=', $input['id'])->update($data);
                }else{ //Add mode
                    $data['created_by'] = Auth::guard('admin')->user()->id;
                    AddonTheft::create($data);
                }
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function delete_carloss(Request $request){
        try{
            
            if(!has_permission($this->menu1,["view","delete"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
  
              $input = $request->all();
              $validator = Validator::make($input, [
                'id' => 'required'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              AddonCarLoss::where('id', '=', $input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function delete_hb(Request $request){
        try{
            
            if(!has_permission($this->menu2,["view","delete"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
  
              $input = $request->all();
              $validator = Validator::make($input, [
                'id' => 'required'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              AddonHb::where('id', '=', $input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function delete_taxi(Request $request){
        try{
            
            if(!has_permission($this->menu3,["view","delete"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
  
              $input = $request->all();
              $validator = Validator::make($input, [
                'id' => 'required'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              AddonTaxi::where('id', '=', $input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function delete_theft(Request $request){
        try{
            
            if(!has_permission($this->menu4,["view","delete"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
  
              $input = $request->all();
              $validator = Validator::make($input, [
                'id' => 'required'
              ]);
              if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
              }
              AddonTheft::where('id', '=', $input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }




    




 
}
