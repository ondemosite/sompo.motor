<?php

namespace App\Http\Controllers\admin;

use App\Repositories\AdminRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Privilege;
use App\Models\Admins;
use App\Models\Permission;
use App\Models\Menus;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;

class PrivilegeController extends Controller
{

  private $menu1 = "motoradmins/privilege";

  public function __construct(AdminRepository $admin){
    $this->middleware('auth:admin');
    $this->admin = $admin;
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    return view('admin.system.privilege');
  }

  public function getPrivilege(Request $request){
      try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $privilege = Privilege::All();
          return Datatables::of($privilege)->addColumn('action', function ($data) {
              return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
          })->make(true);
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getDataPrivilege(Request $request){
      try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $input = $request->all();
          // Validate 
          $validator = Validator::make($input, [
            'id' => 'required',
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }

          $response = Privilege::where('id',$input['id'])->first();
          if(!empty($response)){
            return json_encode($response);
          }

        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }


  public function savePrivilege(Request $request){
      try{
          if($request->ajax()){
            $input = $request->all();
            
            // Validate 
            $validator = Validator::make($input, [
                'name' => [
                    'required',
                    Rule::unique('privilege')->ignore($input['id']),
                ],
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'name'=> $input['name']
            );
            if(!empty($input['id'])){ //Edit mode
              if(!has_permission($this->menu1,["view","edit"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
              }
              if($input['id']!=1 && $input['id']!=2){
                Privilege::where('id', '=', $input['id'])->update($data);
              }else{
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
              }
            }else{ //Add mode
              if(!has_permission($this->menu1,["view","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
              }
              $privilege_id = Privilege::create($data)->id;
              if(!empty($privilege_id)){
                  $menus = Menus::all()->pluck('id')->toArray();
                  foreach ($menus as $key => $menu_id) {
                      $role_check = 0;
                      $permission = array(
                          'privilege_id'=>$privilege_id,
                          'menu_id'=>$menu_id,
                          'view'=>$role_check,
                          'add'=>$role_check,
                          'edit'=>$role_check,
                          'delete'=>$role_check
                      );
                      Permission::create($permission);
                  }
              }
            }

            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function deletePrivilege(Request $request){
      try{
          
          if(!has_permission($this->menu1,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){

            $input = $request->all();
            $validator = Validator::make($input, [
              'id' => 'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }

            if($input['id']!=1 && $input['id']!=2){
              Privilege::where('id', '=', $input['id'])->delete();
              Permission::where('privilege_id','=',$input['id'])->delete();
              return json_encode(["status"=>"success"]);
            }else{
              return json_encode(["resp_error"=>"Permission Denied!!"]);
            }

          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

 
}
