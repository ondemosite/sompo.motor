<?php

namespace App\Http\Controllers\admin;

use App\Repositories\CutoffRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cutoff;
use Validator;
use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;

class CutoffController extends Controller
{

  private $menu1 = "motoradmins/cutoff";

  public function __construct(){
    $this->middleware('auth:admin');
    $this->cutoff = new CutoffRepository();
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    $init = [];
    //Generate Years ------------
    $years = [];
    $now_years = date('Y');
    for($i=$now_years;$i<($now_years+30);$i++){
        $years["".$i] = $i;
    }
    //---------------------------
    //Generate Month ------------
    $months = [];
    for($i=1;$i<13;$i++){
        $months["".$i] = $i." (".getLocaleMonth($i).")";
    }
    $init['years'] = $years;
    $init['months'] = $months;
    return view('admin.cutoff.cutoff',['init'=>$init]);
  }

  public function listCutoff(Request $request){
      try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $input = $request->all();
          $query = $this->cutoff->list($input);
          //Log::info($query);
          return Datatables::of($query)->addColumn('action', function ($data) {
              return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name=""><i class="fa fa-trash"></i></a>';
          })->editColumn('months', function ($data) {
            return $data->months." "."(".getLocaleMonth($data->months).")";
          })->editColumn('cutoff_date', function ($data) {
            return date('Y-m-d',strtotime($data->cutoff_date));
          })->editColumn('updated_by', function ($data) {
              return $data->admin()->first()->username;
          })->make(true);
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function saveCutoff(Request $request){
    try{
        if($request->ajax()){
          
          if(!has_permission($this->menu1,["view","edit","add"])){
              return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }

          $input = $request->all();
          
          // Validate 
          $validator = Validator::make($input, [
              'years'=>'required|numeric',
              'months'=>'required|numeric',
              'cutoff_date' => 'required'
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }

          $data = array(
              'years'=> $input['years'],
              'months'=> $input['months'],
              'cutoff_date' => $input['cutoff_date'],
              'updated_by' => Auth::guard('admin')->user()->id
          );
          if(!empty($input['id'])){ //Edit mode
              Cutoff::where('id', '=', $input['id'])->update($data);
          }else{ //Add mode
            //Check Exist
            $count = Cutoff::where([
                'years' => $input['years'],
                'months' => $input['months']
            ])->count();
            if($count>0){
                return json_encode([
                    "status"=>"error",
                    "reason" => trans('admin_cutoff.alert_exist')
                ]);
            }
              $data['created_by'] = Auth::guard('admin')->user()->id;
              Cutoff::create($data);
          }
          return json_encode(["status"=>"success"]);
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
    }
  }

  public function getCutoff(Request $request){
      try{
        if(!has_permission($this->menu1,["view","edit","add"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
              'id' => 'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
  
            $response = Cutoff::where('id',$input['id'])->first();
            if(!empty($response)){
                $response = $response->toArray();
                $response['cutoff_date'] = date('Y-m-d',strtotime($response['cutoff_date']));
              return json_encode($response);
            }
        }

      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return back()->withInput()->withErrors(['Error']);
      }
  }

  public function deleteCutoff(Request $request){
    try{
        
        if(!has_permission($this->menu1,["view","delete"])){
          return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){

          $input = $request->all();
          $validator = Validator::make($input, [
            'id' => 'required'
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }
          Cutoff::where('id', '=', $input['id'])->delete();
          return json_encode(["status"=>"success"]);
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}



 
}
