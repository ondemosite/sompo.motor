<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Policy;
use App\Models\Receipt;
use App\Models\VisitorLog;
use App\Models\Order;

class HomeController extends Controller
{
  public function __construct(){
    $this->middleware('auth:admin');
  }

  public function index(){
      $number_of_users = count(scandir(ini_get("session.save_path")));
      $init = [
          'active_insurance' => $this->getActiveInsurance(),
          'earned_all' => $this->getEarnedAll(),
          'earned_today' => $this->getEarnedToday(),
          'visitor' => [
              'percent' => $this->getVisitorPercent(),
              'value' => $this->getVisitorInMonth(),
          ],
          'popular' => $this->getPopularPlan(),
          'monthly' => [
              'month'=> [
                'January','February','March','April','May','June','July','August','September','October','November','December'
              ],
              'value' => $this->getMonthly()
          ],
          'ram' => [
              'limit' => ini_get('memory_limit'),
              'value' => memory_get_usage()
          ],
          'order' => $this->getLastestOrder()
      ];
      return view('admin.dashboard.dashboard',['init'=>$init]);
  }

  function get_server_memory_usage(){
 
    $free = shell_exec('free');
    $free = (string)trim($free);
    $free_arr = explode("\n", $free);
    $mem = explode(" ", $free_arr[1]);
    $mem = array_filter($mem);
    $mem = array_merge($mem);
    $memory_usage = $mem[2]/$mem[1]*100;
   
    return $memory_usage;
  }

  function get_server_cpu_usage(){
 
    $load = sys_getloadavg();
    return $load[0];
   
  }

  private function getActiveInsurance(){
      $data = Policy::where('insurance_expire','>',date('Y-m-d H:i:s'))->where('status','!=',"CANCEL")->get();
      if(!empty($data)){
        return count($data->toArray());
      }else{
        return 0;
      }
  }

  private function getEarnedAll(){
      $result = 0;
      $data = Receipt::get()->sum('amount');
      if(!empty($data)){
          $result = $data;
      }
      return $result;
  }

  private function getEarnedToday(){
      $result = 0;
      $data = Receipt::whereDate('created_at', '=', date('Y-m-d'))->sum('amount');
      if(!empty($data)){
          $result = $data;
      }
      return $result;
  }

  private function getVisitorInMonth(){
      $data = VisitorLog::whereMonth('created_at','=',date('m'))->whereYear('created_at','=',date('Y'))->orderBy('created_at','asc')->pluck('counter');
      if(!empty($data)){
        return $data->toArray();
      }
  }

  private function getVisitorPercent(){
      $lastest = VisitorLog::whereMonth('created_at','=',str_pad(intval(date('m'))-1,2,"0",STR_PAD_LEFT))->whereYear('created_at','=',date('Y'))->sum('counter');
      $current = VisitorLog::whereMonth('created_at','=',date('m'))->whereYear('created_at','=',date('Y'))->sum('counter');
      if($lastest>=$current){
          return 0;
      }
      else{
         $diff = $current - $lastest;
         if($lastest!=0){
            return ($diff/$lastest)*100;
         }else{
            return ($diff)*100;
         }
         
      }

  }

  private function getPopularPlan(){
      $plan_1 = Policy::where('insurance_plan_id',1)->get()->count();
      $plan_2 = Policy::where('insurance_plan_id',2)->get()->count();
      $plan_3 = Policy::where('insurance_plan_id',3)->get()->count();
      return [$plan_1,$plan_2,$plan_3];
  }

  private function getMonthly(){
      $array = [];
      for($i=0;$i<12;$i++){
          array_push($array,Receipt::whereMonth('created_at', '=',  str_pad($i+1,2,0,STR_PAD_LEFT))->whereYear('created_at','=',date('Y'))->sum('amount'));
      }
      return $array;
  }

  private function getLastestOrder(){
      $order = Order::orderBy('created_at','desc')->skip(0)->take(10)->get();
      return $order;
  }

  

  
}
