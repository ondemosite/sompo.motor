<?php

namespace App\Http\Controllers\admin;

use App\Repositories\ArticleRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Article;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;
use Validator;
use Auth;

class ArticleController extends Controller
{

    private $menu1 = "motoradmins/article/whysompo";
    private $menu1_1 = "motoradmins/article/whysompo/form";
    private $menu2 = "motoradmins/article/ourproduct";
    private $menu2_1 = "motoradmins/article/ourproduct/form";

    public function __construct(ArticleRepository $article){
        $this->middleware('auth:admin');
        $this->article = $article;
    }

    public function index(){
       // Do notthing
    }

    public function whysompo(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.article.whysompo');
    }

    public function whysompoList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $article = Article::where('category','WHYSOMPO')->get();
                return Datatables::of($article)->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.whysompo_form').'/'.$data->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->title.'"><i class="fa fa-trash"></i></a>';
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function whysompoForm($id = null){
        if(!has_permission($this->menu1_1)){
            return view('admin.common.permission_denied');
        }
        $return['id'] = $id;
        if(!empty($id)){ //Edit Mode

            $data = Article::where('id','=',$id)->where('category','WHYSOMPO')->first();
            if($data){
                $return['id'] = !empty($data->id)?$data->id:null;
                $return['title_en'] = !empty($data->title_en)?$data->title_en:null;
                $return['title_th'] = !empty($data->title_th)?$data->title_th:null;
                $return['cover_path_en'] = !empty($data->cover_path_en)?$data->cover_path_en:null;
                $return['cover_path_th'] = !empty($data->cover_path_th)?$data->cover_path_th:null;
                $return['link'] = !empty($data->link)?$data->link:null;
                $return['detail_en'] = !empty($data->detail_en)?$data->detail_en:null;
                $return['detail_th'] = !empty($data->detail_th)?$data->detail_th:null;
                $return['order'] = !empty($data->order)?$data->order:null;
                $return['view'] = !empty($data->view)?$data->view:null;
                $return['status'] = !empty($data->status)?$data->status:0;
            }else{
                return back()->withInput()->withErrors(['No Data Found!']);
            }
        }
        return view('admin.article.whysompo_form',["data"=>$return]);
    }

    public function whysompoSave(Request $request){
        try{
            if(!has_permission($this->menu1_1,["view","edit","add"])){
                return view('admin.common.permission_denied');
            }

            $input = $request->all();
            // Validate 
            $filter_validate = [
                'title_en'=>'required|max:100',
                'title_th'=>'required|max:100',
                'order'=>'required'
            ];

            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $data = array(
                'category'=>'WHYSOMPO',
                'title_en'=>$input['title_en'],
                'title_th'=>$input['title_th'],
                'link'=>!empty($input['link'])?$input['link']:null,
                'detail_en'=>!empty($input['detail_en'])?$input['detail_en']:null,
                'detail_th'=>!empty($input['detail_th'])?$input['detail_th']:null,
                'order'=>!empty($input['order'])?$input['order']:null,
                'status'=>!empty($input['status'])?1:0,
                'updated_by' => Auth::guard('admin')->user()->id
            );
            if(!empty($input['article_id'])){ //Edit mode
                Article::where('id', '=', $input['article_id'])->update($data);
                $this->setWhysompoImage($request,$input['article_id']);
                if(isset($input['clear_cover_th']) && $input['clear_cover_th']=="true"){
                    $article = Article::where('id','=',$input['article_id'])->first();
                    @unlink(public_path($article->cover_path_th));
                    $article->cover_path_th = "";
                    $article->save();
                }
                if(isset($input['clear_cover_en']) && $input['clear_cover_en']=="true"){
                    $article = Article::where('id','=',$input['article_id'])->first();
                    @unlink(public_path($article->cover_path_en));
                    $article->cover_path_en = "";
                    $article->save();
                }
                return redirect(route('admin.whysompo_form')."/".$input['article_id'])->with('status', 'Updated Article Success!!');
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                $id = Article::create($data)->id;
                if(!empty($id)){
                    $this->setWhysompoImage($request,$id);
                    return redirect(route('admin.whysompo_form')."/".$id)->with('status', 'Created Article Success!!');
                }
            }

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }

    public function deleteWhysompo(Request $request){
        try{
            if(!has_permission($this->menu1,["view","delete"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){

                $input = $request->all();
                $validator = Validator::make($input, [
                    'id' => 'required'
                ]);
                if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
                }
                Article::where('id', '=', $input['id'])->where('category','WHYSOMPO')->delete();
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }



    public function setWhysompoImage($request,$id){
        if(isset($request['cover_path_th'])){
            $this->validate($request, [
                'cover_path_th' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $path = public_path(Config::get('path.cover_path'));
            $name = uniqid();
            $ext = $request->cover_path_th->getClientOriginalExtension();
            $buffer = $name .'_buffer.'.$ext;
            $request->cover_path_th->move($path, $buffer);

            Image::make($path . $buffer)->resize(null,300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path . $name .'_release.'.$ext);

            //update path
            $article = Article::where('id','=',$id)->first();
            @unlink(public_path($article->cover_path_th));
            @unlink($path."/".$buffer);

            $article->cover_path_th = Config::get('path.cover_path') . $name .'_release.'.$ext;
            $article->save();
        }
        if(isset($request['cover_path_en'])){
            $this->validate($request, [
                'cover_path_en' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $path = public_path(Config::get('path.cover_path'));
            $name = uniqid();
            $ext = $request->cover_path_en->getClientOriginalExtension();
            $buffer = $name .'_buffer.'.$ext;
            $request->cover_path_en->move($path, $buffer);

            Image::make($path . $buffer)->resize(null,300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path . $name .'_release.'.$ext);

            //update path
            $article = Article::where('id','=',$id)->first();
            @unlink(public_path($article->cover_path_en));
            @unlink($path."/".$buffer);

            $article->cover_path_en = Config::get('path.cover_path') . $name .'_release.'.$ext;
            $article->save();
        }
    }

    public function ourproduct(){
        if(!has_permission($this->menu2)){
            return view('admin.common.permission_denied');
        }
        return view('admin.article.ourproduct');
    }

    public function ourproductList(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $article = Article::where('category','OURPRODUCT')->get();
                return Datatables::of($article)->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.ourproduct_form').'/'.$data->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->title.'"><i class="fa fa-trash"></i></a>';
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function ourproductForm($id = null){
        if(!has_permission($this->menu2_1)){
            return view('admin.common.permission_denied');
        }
        $return['id'] = $id;
        if(!empty($id)){ //Edit Mode

            $data = Article::where('id','=',$id)->where('category','OURPRODUCT')->first();
            if($data){
                $return['id'] = !empty($data->id)?$data->id:null;
                $return['title_en'] = !empty($data->title_en)?$data->title_en:null;
                $return['title_th'] = !empty($data->title_th)?$data->title_th:null;
                $return['cover_path_en'] = !empty($data->cover_path_en)?$data->cover_path_en:null;
                $return['cover_path_th'] = !empty($data->cover_path_th)?$data->cover_path_th:null;
                $return['link'] = !empty($data->link)?$data->link:null;
                $return['detail_en'] = !empty($data->detail_en)?$data->detail_en:null;
                $return['detail_th'] = !empty($data->detail_th)?$data->detail_th:null;
                $return['order'] = !empty($data->order)?$data->order:null;
                $return['view'] = !empty($data->view)?$data->view:null;
                $return['status'] = !empty($data->status)?$data->status:0;
            }else{
                return back()->withInput()->withErrors(['No Data Found!']);
            }
        }
        return view('admin.article.ourproduct_form',["data"=>$return]);

    }

    public function ourproductSave(Request $request){
        try{
            if(!has_permission($this->menu2_1,["view","edit","add"])){
                return view('admin.common.permission_denied');
            }

            $input = $request->all();
            // Validate 
            $filter_validate = [
                'title_en'=>'required|max:100',
                'title_th'=>'required|max:100',
                'order'=>'required'
            ];

            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $data = array(
                'category'=>'OURPRODUCT',
                'title_en'=>$input['title_en'],
                'title_th'=>$input['title_th'],
                'link'=>!empty($input['link'])?$input['link']:null,
                'detail_en'=>!empty($input['detail_en'])?$input['detail_en']:null,
                'detail_th'=>!empty($input['detail_th'])?$input['detail_th']:null,
                'order'=>!empty($input['order'])?$input['order']:null,
                'status'=>!empty($input['status'])?1:0,
                'updated_by' => Auth::guard('admin')->user()->id
            );
            if(!empty($input['article_id'])){ //Edit mode
                Article::where('id', '=', $input['article_id'])->update($data);
                $this->setOurProductImage($request,$input['article_id']);
                if(isset($input['clear_cover_th']) && $input['clear_cover_th']=="true"){
                    $article = Article::where('id','=',$input['article_id'])->first();
                    @unlink(public_path($article->cover_path_th));
                    $article->cover_path_th = "";
                    $article->save();
                }
                if(isset($input['clear_cover_en']) && $input['clear_cover_en']=="true"){
                    $article = Article::where('id','=',$input['article_id'])->first();
                    @unlink(public_path($article->cover_path_en));
                    $article->cover_path_en = "";
                    $article->save();
                }
                return redirect(route('admin.ourproduct_form')."/".$input['article_id'])->with('status', 'Updated Article Success!!');
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                $id = Article::create($data)->id;
                if(!empty($id)){
                    $this->setOurProductImage($request,$id);
                    return redirect(route('admin.ourproduct_form')."/".$id)->with('status', 'Created Article Success!!');
                }
            }

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }

    public function deleteOurProduct(Request $request){
        try{
            if(!has_permission($this->menu2,["view","delete"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){

                $input = $request->all();
                $validator = Validator::make($input, [
                    'id' => 'required'
                ]);
                if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
                }
                Article::where('id', '=', $input['id'])->where('category','OURPRODUCT')->delete();
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }



    public function setOurProductImage($request,$id){
        if(isset($request['cover_path_th'])){
            $this->validate($request, [
                'cover_path_th' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $path = public_path(Config::get('path.cover_path'));
            $name = uniqid();
            $ext = $request->cover_path_th->getClientOriginalExtension();
            $buffer = $name .'_buffer.'.$ext;
            $request->cover_path_th->move($path, $buffer);

            Image::make($path . $buffer)->resize(null,300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path . $name .'_release.'.$ext);

            //update path
            $article = Article::where('id','=',$id)->first();
            @unlink(public_path($article->cover_path_th));
            @unlink($path."/".$buffer);

            $article->cover_path_th = Config::get('path.cover_path') . $name .'_release.'.$ext;
            $article->save();
        }
        if(isset($request['cover_path_en'])){
            $this->validate($request, [
                'cover_path_en' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $path = public_path(Config::get('path.cover_path'));
            $name = uniqid();
            $ext = $request->cover_path_en->getClientOriginalExtension();
            $buffer = $name .'_buffer.'.$ext;
            $request->cover_path_en->move($path, $buffer);

            Image::make($path . $buffer)->resize(null,300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path . $name .'_release.'.$ext);

            //update path
            $article = Article::where('id','=',$id)->first();
            @unlink(public_path($article->cover_path_en));
            @unlink($path."/".$buffer);

            $article->cover_path_en = Config::get('path.cover_path') . $name .'_release.'.$ext;
            $article->save();
        }
    }



    


 
}
