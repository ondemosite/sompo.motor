<?php

namespace App\Http\Controllers\admin;

use App\Repositories\PolicyRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use App\Models\Policy;
use App\Models\Tax;
use App\Models\PolicyEditLog;
use App\Models\PolicyEditDocLog;
use App\Models\Receipt;
use App\Models\DriverInfo;
use App\Models\Endorse;
use App\Library\Endorse as EndorseLib;
use App\Models\Province;
use App\Models\District;
use App\Models\SubDistrict;
use App\Models\EmailLog;
use App\Models\SmsLog;
use App\Models\AddonFlood;
use App\Library\MyEmail;
use App\Library\insuranceManager;
use App\Library\SMS;
use App\Library\Mpasskit\Mpasskit;
use Validator;
use DateTime;
use Auth;
use File;
use App;
use App\Library\AccessFile;

class PolicyController extends Controller
{

    private $menu1 = "motoradmins/policy";
    private $menu2 = "motoradmins/policy/policy_detail";

    public function __construct(PolicyRepository $policy){
        $this->middleware('auth:admin');
        $this->policy = $policy;
        $this->insurance_manager = new insuranceManager();  
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.policy.policy');
    }

    public function getPolicyList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                
                $result = $this->policy->getPolicyList($input);


                $items = array();
                $no = $input['start'];

                foreach ($result['items'] as $item) {

                    $brand = json_encode([
                        'brand' => $item->brand,
                        'model' => $item->model,
                        'year' => $item->year,
                        'car_licence' => $item->car_licence." ".$item->car_province()->first()->name_th
                    ]);

                    $endores = $item->endorse()->first();
                    $owner = $endores->owner()->first();
                    $name =  $owner->name." ".$owner->lastname;

                    $status = "";
                    $today = date("Y-m-d H:i:s");
                    $expire_date = date("Y-m-d H:i:s",strtotime($item->insurance_expire));
                    if($item->status=="INACTIVE"){
                        $status = $item->status;
                    }else{
                        if($today > $expire_date){
                            $status = "EXPIRE";
                        }else{
                            $status = "NORMAL";
                        }
                    }

                    $return = [
                        'date' => getLocaleDate($item->created_at,true),
                        'state' => "NORMAL"
                    ];
                    $date = new DateTime();
                    $match_date = new DateTime($item->created_at);
                    $interval = $date->diff($match_date);
                    if($interval->days == 0) {
                        $return['state'] = "NEW";
                    }
                    $created_at = json_encode($return);

                    $items[] = array(
                        'no' => ++$no,
                        'policy_number' => $item->policy_number,
                        'policy_com_number' => $item->policy_com_number,
                        'name' => $name,
                        'brand' => $brand,
                        'insurance_ft_si' => number_format($item->insurance_ft_si,2),
                        'status' => $status,
                        'created_at' => $created_at,
                        'action' => '<a href="'.route('admin.get_policy_data').'/'.$item->id.'" class="m-a-1 action-edit" title="More Detail" data-id="'.$item->id.'"><i class="fa fa-search-plus"></i></a>'
                    );
                }

                return array(
                    'draw' => $input['draw'],
                    'recordsTotal' => $result['total'],
                    'recordsFiltered' => $result['total'],
                    'data' => $items
                );

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getPolicyData($id){
        try{
            $policy = Policy::where('id',$id)->first();
            $flood = AddonFlood::where('id',1)->first();
            $static = [
                'compulsory_medical_fee' => Tax::where('id','1')->first()->compulsory_medical_fee,
                'compulsory_death_disable' => Tax::where('id','1')->first()->compulsory_death_disable,
                'document_id_extension' => File::extension($policy->document_path_personal_id),
                'document_car_licence_extension' => File::extension($policy->document_path_car_licence),
                'document_path_cctv_inside_extension' => !empty($policy->document_path_cctv_inside)?File::extension($policy->document_path_cctv_inside):'',
                'document_path_cctv_outside_extension' => !empty($policy->document_path_cctv_outside)?File::extension($policy->document_path_cctv_outside):'',
                'document_path_driver1_licence_extension' => !empty($policy->document_path_driver1_licence)?File::extension($policy->document_path_driver1_licence):'',
                'document_path_driver2_licence_extension' => !empty($policy->document_path_driver2_licence)?File::extension($policy->document_path_driver2_licence):'',
                'province' => Province::orderBy('id','asc')->orderBy('name_th','asc')->pluck('name_th','id')->toArray()
            ];
            if($policy){

                $documentFileList = [];
                if(!empty($policy->document_path_personal_id)){
                    array_push($documentFileList,$policy->document_path_personal_id);
                }
                if(!empty($policy->document_path_car_licence)){
                    array_push($documentFileList,$policy->document_path_car_licence);
                }
                if(!empty($policy->document_path_driver1_licence)){
                    array_push($documentFileList,$policy->document_path_driver1_licence);
                }
                if(!empty($policy->document_path_driver2_licence)){
                    array_push($documentFileList,$policy->document_path_driver2_licence);
                }
                if(!empty($policy->document_path_cctv_inside)){
                    array_push($documentFileList,$policy->document_path_cctv_inside);
                }
                if(!empty($policy->document_path_cctv_outside)){
                    array_push($documentFileList,$policy->document_path_cctv_outside);
                }
                $accessFile = new AccessFile();
                $accessId = $accessFile->setToken([
                    'ref_id' => Auth::user()->id,
                    'files' => $documentFileList
                ]);
                $document = [
                    'personal_id' => $accessFile->getUrl($accessId,Auth::user()->id,$policy->document_path_personal_id,'personal-release'),
                    'car_licence' => $accessFile->getUrl($accessId,Auth::user()->id,$policy->document_path_car_licence,'personal-release'),
                    'driver1_licence' => !empty($policy->document_path_driver1_licence)?$accessFile->getUrl($accessId,Auth::user()->id,$policy->document_path_driver1_licence,'personal-release'):"",
                    'driver2_licence' => !empty($policy->document_path_driver2_licence)?$accessFile->getUrl($accessId,Auth::user()->id,$policy->document_path_driver2_licence,'personal-release'):"",
                ];
                $document['cctv_inside'] = !empty($policy->document_path_cctv_inside)?$accessFile->getUrl($accessId,Auth::user()->id,$policy->document_path_cctv_inside,'personal-release'):"";
                $document['cctv_outside'] = !empty($policy->document_path_cctv_outside)?$accessFile->getUrl($accessId,Auth::user()->id,$policy->document_path_cctv_outside,'personal-release'):"";

                $endorse = $policy->endorse()->first();
                $files = [
                    'policy_document_path' => $endorse->policy_document_path,
                    'policy_original_invoice_path' => $endorse->policy_original_invoice_path,
                    'policy_copy_invoice_path' => $endorse->policy_copy_invoice_path,
                    'compulsory_document_path' => $endorse->compulsory_document_path,
                    'compulsory_original_invoice_path' => $endorse->compulsory_original_invoice_path,
                    'compulsory_copy_invoice_path' => $endorse->compulsory_copy_invoice_path
                ];

                //จัดส่ง Email และ SMS
                $emailingLog = EmailLog::where([
                    'policy_id' => $id,
                    'status' => 'SUCCESS',
                ])->orderBy('id','desc')->first();
                $smsLog = SmsLog::where([
                    'policy_id' => $id,
                    'status' => 'SUCCESS',
                ])->orderBy('id','desc')->first();

                


                return view('admin.policy.policy_detail',[
                    'init'=>$policy,'static'=>$static,'document' => $document,
                    'files'=> $files,'flood' => $flood,'emailingLog'=>$emailingLog,
                    'smsLog' => $smsLog]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withErrors(['Not Found Data!']);
        }
    }

    public function getEndorseList(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $endorses = Policy::where("id",$input['id'])->first()->endorses()->get();
                if(!empty($endorses)){
                    $result = [];
                    foreach($endorses as $item){
                        $buffer = [];
                        $owner_record = $item->owner()->first();
                        $owner = $item->owner()->first()->toArray();
                        /* Address */
                        $province = $owner_record->province()->first();
                        $district = $owner_record->district()->first();
                        $subdistrict = $owner_record->subdistrict()->first();
                        $province_prefix = trans('step5.province_prefix');
                        $district_prefix = ($province->id=="00"?trans('step5.district_prefix_c'):trans('step5.district_prefix'));
                        $subdistrict_prefix = ($province->id=="00"?trans('step5.subdistrict_prefix_c'):trans('step5.subdistrict_prefix'));
                        $owner['address'] = $owner['address']." ".$subdistrict_prefix.$subdistrict->name_th." ".$district_prefix.$district->name_th." ".$province_prefix.$province->name_th." ".$owner['postalcode'];
                        
                        $owner['fullname'] = $owner['name']." ".$owner['lastname'];
                        $owner['birth_date'] = getLocaleDate($owner['birth_date'])." (".getAge($owner['birth_date'])." ปี)";
                        if(App::isLocale('th')){
                            $owner['gender'] = ($owner['gender']=="MALE"?"ชาย":"หญิง");
                        }
                        $driver1 = (!empty($item->driver1)?$item->driver1()->first()->toArray():null);
                        if(!empty($driver1)){
                            $driver1['fullname'] = $driver1['name']." ".$driver1['lastname'];
                            $driver1['birth_date'] = getLocaleDate($driver1['birth_date'])." (".getAge($driver1['birth_date'])." ปี)";
                            if(App::isLocale('th')){
                                $driver1['gender'] = ($driver1['gender']=="MALE"?"ชาย":"หญิง");
                            }
                        }
                        
                        $driver2 = (!empty($item->driver2)?$item->driver2()->first()->toArray():null);
                        if(!empty($driver2)){
                            $driver2['fullname'] = $driver2['name']." ".$driver2['lastname'];
                            $driver2['birth_date'] = getLocaleDate($driver2['birth_date'])." (".getAge($driver2['birth_date'])." ปี)";
                            if(App::isLocale('th')){
                                $driver2['gender'] = ($driver2['gender']=="MALE"?"ชาย":"หญิง");
                            }
                        }
                        
                        

                        $buffer['endorse_no'] = $item->endorse_no;
                        $buffer['owner'] = $owner;
                        $buffer['driver1'] = $driver1;
                        $buffer['driver2'] = $driver2;
                        $buffer['updated_at'] = $item->updated_at;
                        array_push($result,$buffer);
                    }
                    return json_encode($result);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getEndorseFirst(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $endorse = Policy::where("id",$input['id'])->first()->endorse()->first();
                if(!empty($endorse)){
                    $buffer = [];
                    $owner = $endorse->owner()->first()->toArray();
                    $owner['birth_date'] = date('Y-m-d',strtotime($owner['birth_date']));
                    $driver1 = (!empty($endorse->driver1)?$endorse->driver1()->first()->toArray():null);
                    if(!empty($driver1)){
                        $driver1['birth_date'] = date('Y-m-d',strtotime($driver1['birth_date']));
                    }
                    $driver2 = (!empty($endorse->driver2)?$endorse->driver2()->first()->toArray():null);
                    if(!empty($driver2)){
                        $driver2['birth_date'] = date('Y-m-d',strtotime($driver2['birth_date']));
                    }
                    $buffer['owner'] = $owner;
                    $buffer['driver1'] = $driver1;
                    $buffer['driver2'] = $driver2;
                    return json_encode($buffer);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getEditLog(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $edit_log = PolicyEditLog::where("policy_id",$input['id'])->get();
                if(!empty($edit_log)){
                    $return = [];
                    foreach($edit_log as $key => $value){
                        array_push($return,[
                            'id' => $edit_log[$key]->id,
                            'date' => getLocaleDate($edit_log[$key]->created_at),
                            'admin' => $edit_log[$key]->admin()->first()->username
                        ]);
                    }
                    return json_encode($return);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getEditLogData(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $edit_log = PolicyEditLog::where("id",$input['id'])->first();
                if(!empty($edit_log)){
                    $return = [
                        'data' => json_decode($edit_log->data)
                    ];
                    return json_encode($return);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getPolicyDocLog(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $edit_log = PolicyEditDocLog::where("policy_id",$input['id'])->get();
                if(!empty($edit_log)){
                    $return = [];
                    foreach($edit_log as $key => $value){
                        array_push($return,[
                            'id' => $edit_log[$key]->id,
                            'date' => getLocaleDate($edit_log[$key]->created_at),
                            'admin' => $edit_log[$key]->admin()->first()->username
                        ]);
                    }
                    return json_encode($return);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getDocLogData(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){

                $edit_log = PolicyEditDocLog::where("id",$input['id'])->first();
                if(!empty($edit_log)){
                    $decodeData = json_decode($edit_log->data,true);

                    $documentFileList = [];
                    if(!empty($decodeData['document_path_personal_id'])){
                        array_push($documentFileList,$decodeData['document_path_personal_id']);
                    }
                    if(!empty($decodeData['document_path_car_licence'])){
                        array_push($documentFileList,$decodeData['document_path_car_licence']);
                    }
                    if(!empty($decodeData['document_path_driver1_licence'])){
                        array_push($documentFileList,$decodeData['document_path_driver1_licence']);
                    }
                    if(!empty($decodeData['document_path_driver2_licence'])){
                        array_push($documentFileList,$decodeData['document_path_driver2_licence']);
                    }
                    if(!empty($decodeData['document_path_cctv_outside'])){
                        array_push($documentFileList,$decodeData['document_path_cctv_outside']);
                    }
                    if(!empty($decodeData['document_path_cctv_inside'])){
                        array_push($documentFileList,$decodeData['document_path_cctv_inside']);
                    }
                    $accessFile = new AccessFile();
                    $accessId = $accessFile->setToken([
                        'ref_id' => Auth::user()->id,
                        'files' => $documentFileList
                    ]);
                    $document = [
                        'personal_id' => !empty($decodeData['document_path_personal_id'])?$accessFile->getUrl($accessId,Auth::user()->id,$decodeData['document_path_personal_id'],'personal-release'):"",
                        'car_licence' => !empty($decodeData['document_path_car_licence'])?$accessFile->getUrl($accessId,Auth::user()->id,$decodeData['document_path_car_licence'],'personal-release'):"",
                        'driver1_licence' => !empty($decodeData['document_path_driver1_licence'])?$accessFile->getUrl($accessId,Auth::user()->id,$decodeData['document_path_driver1_licence'],'personal-release'):"",
                        'driver2_licence' => !empty($decodeData['document_path_driver2_licence'])?$accessFile->getUrl($accessId,Auth::user()->id,$decodeData['document_path_driver2_licence'],'personal-release'):"",
                    ];
                    $document['cctv_inside'] = !empty($decodeData['document_path_cctv_outside'])?$accessFile->getUrl($accessId,Auth::user()->id,$decodeData['document_path_cctv_outside'],'personal-release'):"";
                    $document['cctv_outside'] = !empty($decodeData['document_path_cctv_inside'])?$accessFile->getUrl($accessId,Auth::user()->id,$decodeData['document_path_cctv_inside'],'personal-release'):"";

                    return json_encode($document);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getEmailLog(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $log = EmailLog::where("policy_id",$input['id'])->get();
                if(!empty($log)){
                    $return = [];
                    foreach($log as $key => $value){
                        $admin = $log[$key]->admin()->first();
                        $files = "";
                        if(!empty($log[$key]->data)){
                            $files_buffer = json_decode($log[$key]->data);
                            $files = "";
                            $loop = 0;
                            foreach($files_buffer as $item){
                                $files.=$item;
                                if($loop!=sizeof($files_buffer)-1) $files.=", ";
                                $loop++;
                            }
                        }
                        
                        array_push($return,[
                            'id' => $log[$key]->id,
                            'created_at' => getLocaleDate($log[$key]->created_at,true),
                            'files' => $files,
                            'status' => $log[$key]->status,
                            'created_by' => !empty($admin)?$log[$key]->admin()->first()->username:'system'
                        ]);
                    }
                    return json_encode($return);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getEpassLog(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $log = SmsLog::where("policy_id",$input['id'])->orderBy('id')->get();
                if(!empty($log)){
                    $return = [];
                    foreach($log as $key => $value){
                        $admin = $log[$key]->admin()->first();
                        
                        array_push($return,[
                            'id' => $log[$key]->id,
                            'endorse_no' => $log[$key]->endorse_no,
                            'status' => $log[$key]->status,
                            'created_at' => getLocaleDate($log[$key]->created_at,true),
                            'created_by' => !empty($admin)?$log[$key]->admin()->first()->username:'system'
                        ]);
                    }
                    return json_encode($return);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function saveNote(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $data = [
                    'note' => $input['value']
                ];
                Policy::where('id',$input['id'])->update($data);
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function saveEndorse(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $policy = Policy::where("id",$input['id'])->first();
                $endorse = $policy->endorse()->first();
                $endorse_data = [
                    'policy_id' => $input['id'],
                    'endorse_no' => str_pad((intval($endorse->endorse_no)+1),2,"0",STR_PAD_LEFT),
                    'created_by' => Auth::guard('admin')->user()->id,
                    'updated_by' => Auth::guard('admin')->user()->id
                ];

                $owner = $endorse->owner()->first();
                $driver1 = $endorse->driver1()->first();
                $driver2 = $endorse->driver2()->first();
                $is_owner_change = false;
                $is_driver1_change = false;
                $is_driver2_change = false;

                //Check Owner Change
                if($owner->name!=$input['main_name']){
                    $is_owner_change = true;
                }
                if($owner->lastname!=$input['main_lastname']){
                    $is_owner_change = true;
                } 
                if($owner->idcard!=$input['main_idcard']){
                    $is_owner_change = true;
                } 
                if(date('Y-m-d',strtotime($owner->birth_date))!=$input['main_birthdate']){
                    $is_owner_change = true;
                }
                if($owner->gender!=$input['main_gender']){
                    $is_owner_change = true;
                }
                if($owner->address!=$input['main_address']){
                    $is_owner_change = true;
                }
                if($owner->tel!=$input['main_tel']){
                    $is_owner_change = true;
                }
                if($owner->email!=$input['main_email']){
                    $is_owner_change = true;
                }
                if($owner->province()->first()->id!=$input['main_province']){
                    $is_owner_change = true;
                }
                if($owner->district()->first()->id!=$input['main_district']){
                    $is_owner_change = true;
                }
                if($owner->subdistrict()->first()->id!=$input['main_subdistrict']){
                    $is_owner_change = true;
                }
                if($owner->postalcode!=$input['main_postalcode']){
                    $is_owner_change = true;
                }
                //Check Driver1 Change
                if(!empty($driver1)){
                    if($driver1->name!=$input['driver1_name']){
                        $is_driver1_change = true;
                    } 
                    if($driver1->lastname!=$input['driver1_lastname']){
                        $is_driver1_change = true;
                    }
                    if($driver1->idcard!=$input['driver1_idcard']){
                        $is_driver1_change = true;
                    }
                    if(date('Y-m-d',strtotime($driver1->birth_date))!=$input['driver1_birthdate']){
                        $is_driver1_change = true;
                    } 
                    if($driver1->licence!=$input['driver1_licence']){
                        $is_driver1_change = true;
                    }
                    if($driver1->gender!=$input['driver1_gender']){
                        $is_driver1_change = true;
                    }
                   
                }else{
                    if(!empty($input['driver1_name'])){
                        $is_driver1_change = true;
                    }
                }
                //Check Driver2 Change
                if(!empty($driver2)){
                    if($driver2->name!=$input['driver2_name']){
                        $is_driver2_change = true;
                    } 
                    if($driver2->lastname!=$input['driver2_lastname']){
                        $is_driver2_change = true;
                    } 
                    if($driver2->idcard!=$input['driver2_idcard']){
                        $is_driver2_change = true;
                    }
                    if(date('Y-m-d',strtotime($driver2->birth_date))!=$input['driver2_birthdate']){
                        $is_driver2_change = true;
                    }
                    if($driver2->licence!=$input['driver2_licence']){
                        $is_driver2_change = true;
                    }
                    if($driver2->gender!=$input['driver2_gender']){
                        $is_driver2_change = true;
                    }
                   
                }else{
                    if(!empty($input['driver2_name'])){
                        $is_driver2_change = true;
                    }
                }
                
                
                if($is_owner_change || $is_driver1_change || $is_driver2_change){
                    if($is_owner_change){
                        $province = Province::where('id',str_pad($input['main_province'],2,"0",STR_PAD_LEFT))->first();
                        $district = District::where([
                            'province_id' => str_pad($input['main_province'],2,"0",STR_PAD_LEFT),
                            'id' => $input['main_district']
                        ])->first();
                        $sudistrict = SubDistrict::where([
                            'province_id' => str_pad($input['main_province'],2,"0",STR_PAD_LEFT),
                            'district_id' => $input['main_district'],
                            'subdistrict_id' => $input['main_subdistrict']
                        ])->first();
                        $driver = [
                            'name' => trim($input['main_name']),
                            'lastname' => trim($input['main_lastname']),
                            'idcard' => $input['main_idcard'],
                            'birth_date' => $input['main_birthdate'],
                            'gender' => $input['main_gender'],
                            'address' => trim($input['main_address']),
                            'tel' => $input['main_tel'],
                            'email' => trim($input['main_email']),
                            'province' => str_pad($input['main_province'],2,"0",STR_PAD_LEFT),
                            'district' => $input['main_district'],
                            'subdistrict' => $input['main_subdistrict'],
                            'province_name' => $province->name,
                            'district_name' => $district->name,
                            'subdistrict_name' => $sudistrict->name,
                            'province_name_th' => $province->name_th,
                            'district_name_th' => $district->name_th,
                            'subdistrict_name_th' => $sudistrict->name_th,
                            'postalcode' => $input['main_postalcode']
                        ];
                        $id = DriverInfo::create($driver)->id;
                        $endorse_data['owner'] = $id;
                        //Update Policy
                        $policy->owner = $id;
                    }else{
                        $endorse_data['owner'] = $owner->id;
                    }
                    if($is_driver1_change){
                        $driver = [
                            'name' => trim($input['driver1_name']),
                            'lastname' => trim($input['driver1_lastname']),
                            'idcard' => $input['driver1_idcard'],
                            'birth_date' => $input['driver1_birthdate'],
                            'gender' => $input['driver1_gender'],
                            'licence' => $input['driver1_licence']
                        ];
                        $id = DriverInfo::create($driver)->id;
                        if(!empty($id)){
                            $endorse_data['driver1'] = $id;
                        }
                    }else{
                        if(!empty($driver1)){
                            $endorse_data['driver1'] = $driver1->id;
                        }else{
                            $endorse_data['driver1'] = null;
                        }
                    }
                    if($is_driver2_change){
                        $driver = [
                            'name' => trim($input['driver2_name']),
                            'lastname' => trim($input['driver2_lastname']),
                            'idcard' => $input['driver2_idcard'],
                            'birth_date' => $input['driver2_birthdate'],
                            'gender' => $input['driver2_gender'],
                            'licence' => $input['driver2_licence']
                        ];
                        $id = DriverInfo::create($driver)->id;
                        if(!empty($id)){
                            $endorse_data['driver2'] = $id;
                        }
                    }else{
                        if(!empty($driver2)){
                            $endorse_data['driver2'] = $driver2->id;
                        }else{
                            $endorse_data['driver2'] = null;
                        }
                    }
                    Endorse::create($endorse_data);
                    $this->insurance_manager->generatePDF($policy,$endorse_data['endorse_no']);

                    if($input['is_core']=="YES"){
                        $endorse_lib = new EndorseLib();
                        if($endorse_lib->request($policy,Auth::guard('admin')->user()->id)!=false){
                            $policy->update_core_status = 1;
                            $policy->save();
                        }else{
                            $policy->update_core_status = 0;
                            $policy->save();
                        }
                    }else{
                        $policy->update_core_status = 0;
                        $policy->save();
                    }
                }
                return json_encode(["status"=>"success"]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    public function saveEditPolicyName(Request $request){
        try{
            $response = [
                'status' => 'success',
                'reason' => ''
            ];
            if(!has_permission($this->menu2,["view","add","edit"])){
                $response['status'] = "fail";
                $response['reason'] = trans('common.permission_denied');
                return json_encode($response);
            }
            $input = $request->all();
            $policy = Policy::where('id',$input['id'])->first();
            if(!empty($policy)){
                $edit_log = [];
                $data = [];
                $policy_name = $policy->policy_name()->first();
                $driver_1 = $policy->driver_1()->first();
                $driver_2 = $policy->driver_2()->first();
                //Main Name
                $fullname = explode(' ',$input['main_name']);
                $name = trim($fullname[0]);
                $lastname = $lastname = !empty($fullname[1])?trim($fullname[1]):'';
                if(strcmp($name,$policy_name->name) != 0){
                    $edit_log['main_driver_name'] = $policy_name->name;
                    $policy_name->name = $name;
                }
                if(strcmp($lastname,$policy_name->lastname) != 0){
                    $edit_log['main_driver_lastname'] = $policy_name->lastname;
                    $policy_name->lastname = $lastname;
                    
                }
                // ID Card
                if(strcmp($input['main_idcard'],$policy_name->idcard)!=0){
                    $edit_log['main_driver_idcard'] = $policy_name->idcard;
                    $policy_name->idcard = $input['main_idcard']; 
                }
                // BirthDate
                if(strcmp($input['main_birth_date'],$policy_name->birth_date)!=0){
                    $edit_log['main_birth_date'] = $policy_name->birth_date;
                    $policy_name->birth_date = $input['main_birth_date']; 
                }
                //Gender
                if(strcmp($input['main_gender'],$policy_name->gender)!=0){
                    $edit_log['main_gender'] = $policy_name->gender;
                    $policy_name->gender = $input['main_gender']; 
                }
                //Address
                if(strcmp($input['main_address'],$policy_name->address)!=0){
                    $edit_log['main_address'] = $policy_name->address;
                    $policy_name->address = $input['main_address']; 
                }
                //Tel
                if(strcmp($input['main_tel'],$policy_name->tel)!=0){
                    $edit_log['main_tel'] = $policy_name->tel;
                    $policy_name->tel = $input['main_tel']; 
                }
                //Email
                if(strcmp($input['main_email'],$policy_name->email)!=0){
                    $edit_log['main_email'] = $policy_name->email;
                    $policy_name->email = $input['main_email']; 
                }
                $policy_name->save();
                
                //Driver1
                if(!empty($policy->driver1)){
                    //Main Name
                    $fullname = explode(' ',$input['driver1_name']);
                    $name = trim($fullname[0]);
                    $lastname = !empty($fullname[1])?trim($fullname[1]):'';
                    if(strcmp($name,$driver_1->name) != 0){
                        $edit_log['driver1_name'] = $driver_1->name;
                        $driver_1->name = $name;
                    }
                    if(strcmp($lastname,$driver_1->lastname) != 0){
                        $edit_log['driver1_lastname'] = $driver_1->lastname;
                        $driver_1->lastname = $lastname;
                        
                    }
                    // ID Card
                    if(strcmp($input['driver1_idcard'],$driver_1->idcard)!=0){
                        $edit_log['driver1_idcard'] = $driver_1->idcard;
                        $driver_1->idcard = $input['driver1_idcard']; 
                    }
                    // Licence
                    if(strcmp($input['driver1_licence'],$driver_1->licence)!=0){
                        $edit_log['driver1_licence'] = $driver_1->licence;
                        $driver_1->licence = $input['driver1_licence']; 
                    }
                    // BirthDate
                    if(strcmp($input['driver1_birth_date'],$driver_1->birth_date)!=0){
                        $edit_log['driver1_birth_date'] = $driver_1->birth_date;
                        $driver_1->birth_date = $input['driver1_birth_date']; 
                    }
                    //Gender
                    if(strcmp($input['driver1_gender'],$driver_1->gender)!=0){
                        $edit_log['driver1_gender'] = $driver_1->gender;
                        $driver_1->gender = $input['driver1_gender']; 
                    }
                    $driver_1->save();
                }

                //Driver1
                if(!empty($policy->driver2)){
                    //Main Name
                    $fullname = explode(' ',$input['driver2_name']);
                    $name = trim($fullname[0]);
                    $lastname = !empty($fullname[1])?trim($fullname[1]):'';
                    if(strcmp($name,$driver_2->name) != 0){
                        $edit_log['driver2_name'] = $driver_2->name;
                        $driver_2->name = $name;
                    }
                    if(strcmp($lastname,$driver_2->lastname) != 0){
                        $edit_log['driver2_lastname'] = $driver_2->lastname;
                        $driver_2->lastname = $lastname;
                        
                    }
                    // ID Card
                    if(strcmp($input['driver2_idcard'],$driver_2->idcard)!=0){
                        $edit_log['driver2_idcard'] = $driver_2->idcard;
                        $driver_2->idcard = $input['driver2_idcard']; 
                    }
                    // Licence
                    if(strcmp($input['driver2_licence'],$driver_2->licence)!=0){
                        $edit_log['driver2_licence'] = $driver_2->licence;
                        $driver_2->licence = $input['driver2_licence']; 
                    }
                    // BirthDate
                    if(strcmp($input['driver2_birth_date'],$driver_2->birth_date)!=0){
                        $edit_log['driver2_birth_date'] = $driver_2->birth_date;
                        $driver_2->birth_date = $input['driver2_birth_date']; 
                    }
                    //Gender
                    if(strcmp($input['driver2_gender'],$driver_2->gender)!=0){
                        $edit_log['driver2_gender'] = $driver_2->gender;
                        $driver_2->gender = $input['driver2_gender']; 
                    }
                    $driver_2->save();
                }

                // Add Edit Log
                if(!empty($edit_log)){
                    $edit_log_data = [
                        'policy_id' => $input['id'],
                        'data' => json_encode($edit_log),
                        'created_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                    ];
                    PolicyEditLog::create($edit_log_data);
                }

            }
            return json_encode($response);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function savePolicyDoc(Request $request){
        try{
            $response = [
                'status' => 'success',
                'reason' => ''
            ];
            if(!has_permission($this->menu2,["view","add","edit"])){
                $response['status'] = "fail";
                $response['reason'] = trans('common.permission_denied');
                return json_encode($response);
            }
            $input = $request->all();
            $policy = Policy::where('id',$input['id'])->first();
            if(!empty($policy)){
                $filter_validate = [];
                if(!empty($input['doc_main_idcard'])){
                    $filter_validate['doc_main_idcard'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2000';
                }
                if(!empty($input['doc_main_licence'])){
                    $filter_validate['doc_main_licence'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2000';
                }
                if(!empty($input['doc_driver1_licence'])){
                    $filter_validate['doc_driver1_licence'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2000';
                }
                if(!empty($input['doc_driver2_licence'])){
                    $filter_validate['doc_driver2_licence'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2000';
                }
                if(!empty($input['doc_cctv_inside'])){
                    $filter_validate['doc_cctv_inside'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2000';
                }
                if(!empty($input['doc_cctv_outside'])){
                    $filter_validate['doc_cctv_outside'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2000';
                }
                $validator = Validator::make($input,$filter_validate);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    $response['status'] = "fail";
                    $response['reason'] = $validate_first[0];
                    return json_encode($response);
                }

                // Add Edit Doc Log
                $edit_log = [];
                if(!empty($input['doc_main_idcard'])){
                    $edit_log['document_path_personal_id'] = $policy->document_path_personal_id;
                    $path = Storage::disk('personal-release')->putFile('', $input['doc_main_idcard']);
                    $policy->document_path_personal_id = $path;
                }
                if(!empty($input['doc_main_licence'])){
                    $edit_log['	document_path_car_licence'] = $policy->document_path_car_licence;
                    $path = Storage::disk('personal-release')->putFile('', $input['doc_main_licence']);
                    $policy->document_path_car_licence = $path;
                }
                if(!empty($input['doc_driver1_licence'])){
                    $edit_log['document_path_driver1_licence'] = $policy->document_path_driver1_licence;
                    $path = Storage::disk('personal-release')->putFile('', $input['doc_driver1_licence']);
                    $policy->document_path_driver1_licence = $path;
                }
                if(!empty($input['doc_driver2_licence'])){
                    $edit_log['document_path_driver2_licence'] = $policy->document_path_driver2_licence;
                    $path = Storage::disk('personal-release')->putFile('', $input['doc_driver2_licence']);
                    $policy->document_path_driver2_licence = $path;
                }
                if(!empty($input['doc_cctv_inside'])){
                    $edit_log['document_path_cctv_inside'] = $policy->document_path_cctv_inside;
                    $path = Storage::disk('personal-release')->putFile('', $input['doc_cctv_inside']);
                    $policy->document_path_cctv_inside = $path;
                }
                if(!empty($input['doc_cctv_outside'])){
                    $edit_log['document_path_cctv_outside'] = $policy->document_path_cctv_outside;
                    $path = Storage::disk('personal-release')->putFile('', $input['doc_cctv_outside']);
                    $policy->document_path_cctv_outside = $path;
                }
                if(!empty($edit_log)){
                    $edit_log_data = [
                        'policy_id' => $input['id'],
                        'data' => json_encode($edit_log),
                        'created_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                    ];
                    PolicyEditDocLog::create($edit_log_data);
                    $policy->save();
                }
            }
            return json_encode($response);

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    public function getReceipt(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $receipt = Receipt::where("receipt_number",$input['id'])->first();
                if(!empty($receipt)){
                    $payment_channel = Config::get("payment_code.payment_channel_code");
                    $payment_scheme = Config::get("payment_code.payment_scheme");
                    $return = [
                        'receipt_id' => $receipt->id,
                        'receipt_number' => $receipt->receipt_number,
                        'receipt_order' => $receipt->order_number,
                        'receipt_amount' => $receipt->amount,
                        'receipt_channel' => !empty($payment_channel[$receipt->payment_channel_code])?$payment_channel[$receipt->payment_channel_code]:'ระบุไม่ได้',
                        'receipt_credit_number' => $receipt->credit_number,
                        'receipt_scheme' => !empty($payment_scheme[$receipt->payment_scheme])?$payment_scheme[$receipt->payment_scheme]:'ระบุไม่ได้',
                        'created_at' => getLocaleDate($receipt->created_at)
                    ];
                    return json_encode($return);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function resendDocument(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id']) && !empty($input['files'])){
                $policy = Policy::where('id',$input['id'])->first();
                if(!empty($policy)){
                    $files = json_decode($input['files'],true);
                    $mail = new MyEmail();
                    $response = $mail->sendEmail($policy,$files,Auth::guard('admin')->user()->id);
                    if($response!=false){
                        return json_encode(["success"=>"success"]);
                    }else{
                        return json_encode(["fail"=>"Send e-mail failed check document logs for details"]);
                    }
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function resendSMS(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $policy = Policy::where('id',$input['id'])->first();
                $mk = new Mpasskit();
                $epass_link = $mk->createPass($policy,Auth::guard('admin')->user()->id);
                if(!empty($epass_link)){
                    $sms = new SMS();
                    $response = $sms->sendSMS($policy,$epass_link,Auth::guard('admin')->user()->id);
                    if($response!=false){
                        return json_encode(["success"=>"success"]);
                    }else{
                        return json_encode(["fail"=>"Send E-Pass failed, check document logs for details."]);
                    } 
                }else{
                    return json_encode(["fail"=>"Create E-Pass Fail, check document logs for details. "]);
                }
            }
            return json_encode(["fail"=>"No data Found"]);
            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function updateCore(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $policy = Policy::where('id',$input['id'])->first();
                if(!empty($policy)){
                    $endorse_lib = new EndorseLib();
                    if($endorse_lib->request($policy,Auth::guard('admin')->user()->id)!=false){
                        $policy->update_core_status = 1;
                        $policy->save();
                        return json_encode(["success"=>"success"]);
                    }else{
                        $policy->update_core_status = 0;
                        $policy->save();
                        return json_encode(["fail"=>"Update Data to Core System failed, check document logs for details."]);
                    }
                }
            }
            return json_encode(["fail"=>"Not Found Data"]);

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getUpdateCore(Request $request){
        try{
            if(!has_permission($this->menu2,["view","add","edit"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            $input = $request->all();
            if(!empty($input['id'])){
                $policy = Policy::where('id',$input['id'])->first();
                if(!empty($policy)){
                    if($policy->update_core_status==1) return json_encode(["result"=>"yes"]);
                    else return json_encode(["result"=>"no"]);
                }
            }
            return json_encode(["resp_error"=>"Not Found Data"]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function downloadDocument($policyId,$policyFile,$method = null){
        try{
            //Check Auth Admin Permission
            $policy = Policy::where('id',$policyId)->first();
            if(!empty($policy)){
                $endorse = $policy->endorse()->first();
                if(!empty($endorse)){

                    $recordArray = $endorse->toArray();
                    $filePath = $recordArray[$policyFile];
                    
                    if(!empty($method)){
                        $exploded = explode("/",$filePath);
                        $lastExplode = $exploded[sizeof($exploded)-1];
                        $fileName = $method."_".$lastExplode;
                        $filePath = str_replace($lastExplode,$fileName,$filePath);
                    }

                    $directoryPath = Storage::disk('pdf')->getAdapter()->getPathPrefix();

                    if(Storage::disk('pdf')->exists($filePath)){
                        
                        if(!empty($method)){
                            if($method == "clean"){
                                if($policyFile == "policy_document_path"){
                                    $endorse->is_print_policy = 1;
                                }else if($policyFile == "policy_original_invoice_path"){
                                    $endorse->is_print_original_invoice = 1;
                                }else if($policyFile == "policy_copy_invoice_path"){
                                    $endorse->is_print_copy_invoice = 1;
                                }else if($policyFile == "compulsory_document_path"){
                                    $endorse->is_print_compulsory = 1;
                                }
                            }
                            $endorse->save();
                        }
                        
                        $fileDownloadName = "";
                        if($policyFile == "policy_document_path"){
                            $prefix = "Ori";
                            if($method=="copy"){
                                $prefix = "Copy";
                            }
                            $fileDownloadName = $this->insurance_manager->generatePDFFileName($prefix,$policy,false);
                        }else if($policyFile == "policy_original_invoice_path"){
                            $fileDownloadName = $this->insurance_manager->generatePDFFileName("RecOri",$policy,false);

                        }else if($policyFile == "policy_copy_invoice_path"){
                            $fileDownloadName = $this->insurance_manager->generatePDFFileName("RecCopy",$policy,false);

                        }else if($policyFile == "compulsory_document_path"){
                            $prefix = "Ori";
                            if($method=="copy"){
                                $prefix = "Copy";
                            }
                            $fileDownloadName = $this->insurance_manager->generatePDFCompulsoryFileName($prefix,$policy,false);
                        }
                        return response()->download($directoryPath.$filePath,$fileDownloadName);
                    }else{
                        return "File Not Found!";
                    }
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withErrors(['Error while downloading process please contact administrator']);
        }
    }

 
}
