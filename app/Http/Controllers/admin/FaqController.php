<?php

namespace App\Http\Controllers\admin;

use App\Repositories\GarageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Faq;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;

class FaqController extends Controller
{

  private $menu1 = "motoradmins/faq/faq";

  public function __construct(){
    $this->middleware('auth:admin');
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    
    return view('admin.faq.faq');
  }



  public function getFaqList(Request $request){
      try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $faq = Faq::All();
          return Datatables::of($faq)->addColumn('action', function ($data) {
              return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
          })->editColumn('question',function($data){
              return json_encode(["question"=>$data->question,"answer"=>$data->answer]);
          })->make(true);
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getDataFaq(Request $request){
      try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $input = $request->all();
          // Validate 
          $validator = Validator::make($input, [
            'id' => 'required',
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }

          $response = Faq::where('id',$input['id'])->first();
          if(!empty($response)){
            return json_encode($response);
          }

        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }


  public function saveFaq(Request $request){
      try{
          if($request->ajax()){
            
            if(!has_permission($this->menu1,["view","edit","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $input = $request->all();
            
            // Validate 
            $validator = Validator::make($input, [
                'question'=>'required|max:200',
                'answer'=>'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'question'=> $input['question'],
                'answer'=> $input['answer'],
                'updated_by' => Auth::guard('admin')->user()->id
            );
            if(!empty($input['id'])){ //Edit mode
                Faq::where('id', '=', $input['id'])->update($data);
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                Faq::create($data);
            }
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function deleteFaq(Request $request){
      try{
          
          if(!has_permission($this->menu1,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){

            $input = $request->all();
            $validator = Validator::make($input, [
              'id' => 'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            Faq::where('id', '=', $input['id'])->delete();
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function setStatus(Request $request){
    try{
        if(!has_permission($this->menu1,array("view","edit"))){
            return json_encode(array("resp_error"=>trans('common.permission_denied')));
        }
        if($request->ajax()){
          $input = $request->all();
          
          // Validate 
          $validator = Validator::make($input, [
              'id' => 'required',
              'type' => 'required|numeric'
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(array("resp_error"=>$validate_first[0]));
          }
          $data = array(
              'status'=>$input['type'],
          );

          Faq::where('id', '=', $input['id'])->update($data);

          return json_encode(array("status"=>"success"));
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }


 
}
