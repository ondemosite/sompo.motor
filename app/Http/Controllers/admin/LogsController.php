<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\PaymentLogRepository;
use App\Repositories\UserManagementLogRepository;
use App\Repositories\ResendDocumentLogRepository;
use App\Repositories\EndorseLogRepository;
use App\Repositories\PasskitLogRepository;
use App\Repositories\SmsLogRepository;
use App\Repositories\OtpLogRepository;
use App\Repositories\BarcodeLogRepository;
use App\Repositories\SignatureLogRepository;
use App\Repositories\BlacklistLogRepository;
use App\Repositories\ChassisLogRepository;
use App\Repositories\CityzenLogRepository;
use App\Repositories\ResendScheduleLogRepository;
use App\Models\PaymentResponseLog;
use App\Models\UserManagementLog;
use App\Models\Privilege;
use App\Models\EndorseLog;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Validator;
use Auth;

class LogsController extends Controller
{

    private $menu1 = "motoradmins/log/payment_response";
    private $menu2 = "motoradmins/log/email_log";
    private $menu3 = "motoradmins/log/user_management";
    private $menu4 = "motoradmins/log/endorse_log";
    private $menu5 = "motoradmins/log/passkit_log";
    private $menu6 = "motoradmins/log/sms_log";
    private $menu7 = "motoradmins/log/otp_log";
    private $menu8 = "motoradmins/log/barcode_log";
    private $menu9 = "motoradmins/log/sign_log";
    private $menu10 = "motoradmins/log/blacklist_log";
    private $menu11 = "motoradmins/log/chassis_log";
    private $menu12 = "motoradmins/log/cityzen_log";
    private $menu13 = "motoradmins/log/resend_schedule_log";

    public function __construct(){
        $this->middleware('auth:admin');
        $this->payment_log = new PaymentLogRepository();
        $this->user_management_log = new UserManagementLogRepository();
        $this->resend_document_log = new ResendDocumentLogRepository();
        $this->endorse_log = new EndorseLogRepository();
        $this->passkit_log = new PasskitLogRepository();
        $this->sms_log = new SmsLogRepository();
        $this->otp_log = new OtpLogRepository();
        $this->barcode_log = new BarcodeLogRepository();
        $this->sign = new SignatureLogRepository();
        $this->blacklist = new BlacklistLogRepository();
        $this->chassis = new ChassisLogRepository();
        $this->cityzen = new CityzenLogRepository();
        $this->resendSchedule = new ResendScheduleLogRepository();
    }

    public function payment_log(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.payment_log');
    }

    public function emailLog(){
        if(!has_permission($this->menu2)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.email_log');
    }

    public function user_management_log(){
        if(!has_permission($this->menu3)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.user_management_log');
    }

    public function endorseLog(){
        if(!has_permission($this->menu4)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.endorse_log');
    }

    public function passkitLog(){
        if(!has_permission($this->menu5)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.passkit_log');
    }

    public function smsLog(){
        if(!has_permission($this->menu6)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.sms_log');
    }

    public function otpLog(){
        if(!has_permission($this->menu7)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.otp_log');
    }

    public function barcodeLog(){
        if(!has_permission($this->menu8)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.barcode_log');
    }

    public function signLog(){
        if(!has_permission($this->menu9)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.signature_log');
    }

    public function blacklistLog(){
        if(!has_permission($this->menu10)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.blacklist_log');
    }

    public function chassisLog(){
        if(!has_permission($this->menu11)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.chassis_log');
    }


    public function cityzenLog(){
        if(!has_permission($this->menu12)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.cityzen_log');
    }

    public function resendScheduleLog(){
        if(!has_permission($this->menu13)){
            return view('admin.common.permission_denied');
        }
        return view('admin.logs.resend_schedule');
    }



    public function getPaymentLogList(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->payment_log->list($input);
                return Datatables::of($data)->addColumn('action', function ($data) {
                    return '<a class="cursor-pointer view-modal" data-id="'.$data->id.'" data-toggle="modal" data-target="#modal"><i class="fa fa-search-plus"></i></a>';
                })->editColumn('payment_status',function($data){
                    $payment_status = Config::get("payment_code.payment_status");
                    return $payment_status[$data->payment_status];
                })->editColumn('amount',function($data){
                    return number_format($data->amount,2);
                })->editColumn('payment_channel',function($data){
                    $payment_channel_code = Config::get("payment_code.payment_channel_code");
                    return $payment_channel_code[$data->payment_channel];
                })->editColumn('payment_scheme',function($data){
                    $payment_scheme = Config::get("payment_code.payment_scheme");
                    return $payment_scheme[$data->payment_scheme];
                })->editColumn('request_at',function($data){
                    return getLocaleDate($data->request_at,true);
                })->editColumn('response_at',function($data){
                    return getLocaleDate($data->response_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function emailLogList(Request $request){
        try{
            if(!has_permission($this->menu2)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->resend_document_log->list($input);
                
                return Datatables::of($data)->editColumn('created_by',function($data){
                    $admin = $data->admin()->first();
                    if(!empty($admin)){
                        return $admin->username;
                    }else{
                        return "SYSTEM";
                    }
                })->addColumn('policy_number',function($data){
                    return $data->policy()->first()->policy_number;
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function resendScheduleLogList(Request $request){
        try{
            if(!has_permission($this->menu13)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->resendSchedule->list($input);
                
                return Datatables::of($data)->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function endorseLogList(Request $request){
        try{
            if(!has_permission($this->menu4)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->endorse_log->list($input);
                
                return Datatables::of($data)->editColumn('created_by',function($data){
                    $admin = $data->admin()->first();
                    if(!empty($admin)){
                        return $admin->username;
                    }else{
                        return "SYSTEM";
                    }
                })->addColumn('policy_number',function($data){
                    $policy = $data->policy()->first();
                    return !empty($policy)?$policy->policy_number:'';
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function endorseLogDetail($logId){
        try{
            $endorseData = EndorseLog::where('id',$logId)->first();
            if(!empty($endorseData)){
                $init = $endorseData;
                return view('admin.logs.endorse_detail',['init' => $init]);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return back()->withInput()->withErrors(['Error']);
        }
    }

    public function passkitLogList(Request $request){
        try{
            if(!has_permission($this->menu5)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->passkit_log->list($input);
                
                return Datatables::of($data)->editColumn('created_by',function($data){
                    $admin = $data->admin()->first();
                    if(!empty($admin)){
                        return $admin->username;
                    }else{
                        return "SYSTEM";
                    }
                })->addColumn('policy_number',function($data){
                    return $data->policy()->first()->policy_number;
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function smsLogList(Request $request){
        try{
            if(!has_permission($this->menu6)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->sms_log->list($input);
                
                return Datatables::of($data)->editColumn('created_by',function($data){
                    $admin = $data->admin()->first();
                    if(!empty($admin)){
                        return $admin->username;
                    }else{
                        return "SYSTEM";
                    }
                })->addColumn('policy_number',function($data){
                    $policy = $data->policy()->first();
                    return !empty($policy)?$policy->policy_number:'';
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function otpLogList(Request $request){
        try{
            if(!has_permission($this->menu7)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->otp_log->list($input);
                return Datatables::of($data)->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function barcodeLogList(Request $request){
        try{
            if(!has_permission($this->menu8)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->barcode_log->list($input);
                return Datatables::of($data)->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function signLogList(Request $request){
        try{
            if(!has_permission($this->menu9)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->sign->list($input);
                
                return Datatables::of($data)->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getUserManagementLogList(Request $request){
        try{
            if(!has_permission($this->menu3)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->user_management_log->list($input);
                return Datatables::of($data)->editColumn('created_by',function($data){
                    $admin = $data->admin()->first();
                    return !empty($admin)?$admin->username:'';
                })->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function blacklistLogList(Request $request){
        try{
            if(!has_permission($this->menu10)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->blacklist->list($input);
                
                return Datatables::of($data)->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function chassisLogList(Request $request){
        try{
            if(!has_permission($this->menu11)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->chassis->list($input);
                
                return Datatables::of($data)->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function cityzenLogList(Request $request){
        try{
            if(!has_permission($this->menu12)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                $data = $this->cityzen->list($input);
                
                return Datatables::of($data)->editColumn('created_at',function($data){
                    return getLocaleDate($data->created_at,true);
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getPaymentLogData(Request $request){
        try{
            $response = [
                'status' => 'success',
                'reason' => '',
                'data' => null
            ];
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                if(!empty($input['id'])){
                    $data = PaymentResponseLog::where('id',$input['id'])->first();
                    if(!empty($data)){
                        $payment_code = Config::get("payment_code");
                        $response['data'] = [
                            'order_number' => $data->order_number,
                            'amount' => number_format($data->amount,2),
                            'transaction_ref' => $data->transaction_ref,
                            'approval_code' => $data->approval_code,
                            'eci' => $data->eci.":<br/> ".(!empty($payment_code['eci'][$data->eci])?$payment_code['eci'][$data->eci]:'ไม่สามารถระบุ'),
                            'payment_channel' => $data->payment_channel.": <br/> ".(!empty($payment_code['payment_channel_code'][$data->payment_channel])?$payment_code['payment_channel_code'][$data->payment_channel]:'ไม่สามารถระบุ'),
                            'payment_status' => $data->payment_status.": <br/> ".(!empty($payment_code['payment_status'][$data->payment_status])?$payment_code['payment_status'][$data->payment_status]:'ไม่สามารถระบุ'),
                            'channel_response_code' => $data->channel_response_code.": <br/> ".(!empty($payment_code['channel_response_code'][$data->channel_response_code])?$payment_code['channel_response_code'][$data->channel_response_code]:'ไม่สามารถระบุ'),
                            'channel_response_desc' => $data->channel_response_desc,
                            'credit_number' => $data->masked_pan,
                            'payment_scheme' => $data->payment_scheme.": <br/> ".!empty($payment_code['payment_scheme'][$data->payment_scheme])?$payment_code['payment_scheme'][$data->payment_scheme]:"ไม่ามารถระบุ",
                            'browser_info' => $data->browser_info,
                            'request_at' => getLocaleDate($data->request_at,true),
                            'response_at' => getLocaleDate($data->response_at,true)
                        ]; 

                        return $response;
                    }else{
                        $response['status'] = "fail";
                        $response['reason'] = "No Data Found";
                    }
                }else{
                    $response['status'] = "fail";
                    $response['reason'] = "Not Found id";
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getUserManagementtLogData(Request $request){
        try{
            $response = [
                'status' => 'success',
                'reason' => '',
                'data' => null
            ];
            if(!has_permission($this->menu3)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $input = $request->all();
                if(!empty($input['id'])){
                    $data = UserManagementLog::where('id',$input['id'])->first();
                    if(!empty($data)){
                        $buffer = null;
                        if($input['type']=="edit"){
                            $buffer = json_decode($data->edit_data,true);
                        }else{
                            $buffer = json_decode($data->old_data,true);
                        }
                        $response['data'] = [
                            'username' => !empty($buffer['username'])?$buffer['username']:null,
                            'password' => !empty($buffer['password'])?$buffer['password']:null,
                            'name' => !empty($buffer['name'])?$buffer['name']:null,
                            'lastname' => !empty($buffer['lastname'])?$buffer['lastname']:null,
                            'email' => !empty($buffer['email'])?$buffer['email']:null,
                            'privilege' => !empty($buffer['privilege'])?Privilege::where("id",$buffer['privilege'])->first()->name:null,
                            'status' => !empty($buffer['status'] || $buffer['status']==0)?($buffer['status']==1?"ACTIVE":"INACTIVE"):null
                        ];
                        return $response;
                    }else{
                        $response['status'] = "fail";
                        $response['reason'] = "No Data Found";
                    }
                }else{
                    $response['status'] = "fail";
                    $response['reason'] = "Not Found id";
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }






 
}
