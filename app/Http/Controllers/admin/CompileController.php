<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Validator;
use DateTime;
use Illuminate\Support\Facades\Log;
use Auth;
use App\Models\TransactionLog;
use App\Models\Order;
use App\Models\Policy;
use App\Models\Receipt;
use App\Models\SmsLog;
use App\Models\EmailLog;
use App\Library\Compile;
use App\Library\SMS;
use App\Library\MyEmail;
use App\Library\Endorse;

class CompileController extends Controller
{


    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index($id){
        $orders = Order::where('id',$id)->first();
        if(!empty($orders)){
            $init['orders'] = $orders->toArray();

            //Payment
            $payment = $orders->payment()->first();
            if(!empty($payment)){
                $init['payment'] = $payment->toArray();
            }

            //Policy
            $policy = Policy::where('order_id',$orders->id)->first();
            if(!empty($policy)){
                $init['policy'] = $policy->toArray();
                //PDF
                $endorse = $policy->endorse()->first();
                $files = [
                    'policy_document_path' => $endorse->policy_document_path,
                    'policy_original_invoice_path' => $endorse->policy_original_invoice_path,
                    'policy_copy_invoice_path' => $endorse->policy_copy_invoice_path,
                    'compulsory_document_path' => $endorse->compulsory_document_path,
                ];
                $init['files'] = $files;

                //SMS
                $smsLog = SmsLog::where([
                    'policy_id' => $policy->id,
                    'endorse_no' => $endorse->endorse_no,
                    'status' => 'SUCCESS'
                ])->first();
                if(!empty($smsLog)){
                    $init['sms'] = $smsLog->toArray();
                }

                //EMAIL
                $emailLog = EmailLog::where([
                    'policy_id' => $policy->id,
                    'endorse_no' => $endorse->endorse_no,
                    'status' => 'SUCCESS'
                ])->first();
                if(!empty($emailLog)){
                    $init['email'] = $emailLog->toArray();
                }

                //Endorse
                $endorseLog = $policy->endorseLog()->first();
                Log::info($endorseLog);
                if(!empty($endorseLog)){
                    $init['endorseLog'] = $endorseLog->toArray();
                }
                
            }

            //Receipt
            $receipt = Receipt::where('order_number',$orders->order_number)->first();
            if(!empty($receipt)){
                $init['receipt'] = $receipt->toArray();
            }

            return view('admin.compile.index',['init'=> $init]);
        }else{
            return "ไม่พบใบสั่งซื้อ";
        }
        
    }

    public function getProcessLogs(Request $request){
        try{
            $input = $request->all();
            $orders = Order::where('id',$input['order_no'])->first();

            $processLogs = TransactionLog::where([
                'order_no' => $orders->order_number
            ])->orderBy('id','asc')->get();

            $result = [];
            if(!$processLogs->isEmpty()){
                $result['status'] = "SUCCESS";
                $result['data'] = $processLogs->toArray();
            }else{
                $result['status'] = "ERROR";
                $result['message'] = "ไม่พบข้อมูล";
            }
            return json_encode($result);

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    public function updateOrderStatus(Request $request){
        try{
            $input = $request->all();
            $orders = Order::where('id',$input['id'])->first();
            if(!empty($orders)){
                $orders->status = "PAID";
                if(!empty($orders->promotion_id)){
                    logTransaction($orders->order_number,"Update Promotion Remaining","SUCCESS");
                    $promotion = $orders->promotion()->first();
                    if(!empty($promotion)){
                        $promotion->receive_amount = $promotion->receive_amount + 1;
                        $promotion->save();
                    }
                }
                logTransaction($orders->order_number,"Update Order Status","SUCCESS");
                $orders->save();
            }
            
            return json_encode([
                'status' => "SUCCESS"
            ]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function updatePayment(Request $request){
        try{
            $input = $request->all();
            $orders = Order::where('id',$input['id'])->first();
            $payment = $orders->payment()->first();
            $result = [];

            if(!empty($payment)){
                //Update Payment
                $payment->status = "PAID";
                $payment->status_detail = "ชำระเงินแล้ว";
                $payment->paid_date = !empty($input['paid_date'])?$input['paid_date']:date('Y-m-d H:i:s');
                $payment->save();
                logTransaction($orders->order_number,"Update Payment Status: PAID","SUCCESS");
                $result['status'] = "SUCCESS";
            }else{
                $result['status'] = "ERROR";
            }

            return json_encode($result);

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function createPolicy(Request $request){
        try{
            $input = $request->all();
            $orders = Order::where('id',$input['id'])->first();
            $payment = $orders->payment()->first();
            $result = [];

            $compile = new Compile();
            $createPolicy = $compile->createPolicy($orders);
            if($createPolicy['status'] == "SUCCESS"){
                $policy = Policy::where('id',$createPolicy['policy_id'])->first();
                if(!empty($policy)){
                    //Update Payment
                    $payment->policy_number = $policy->policy_number;
                    $payment->save();
                    //Update Policy Date
                    $newDate = date("Y-m-d",strtotime($policy->insurance_start));
                    $policy->insurance_start = $newDate." 00:01:00";
                    $policy->insurance_expire = date('Y-m-d',strtotime("+1 year",strtotime($policy->insurance_start)))." 16:30:00";
                    if(!empty($policy->policy_com_number)){
                        $newDate = date("Y-m-d",strtotime($policy->compulsory_start));
                        $policy->compulsory_start = $newDate." 00:01:00";
                        $policy->compulsory_expire = date('Y-m-d',strtotime("+1 year",strtotime($policy->compulsory_start)))." 16:30:00";
                    }
                    $policy->save();
                }   
            }
            return json_encode($createPolicy);

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function createReceipt(Request $request){
        try{
            $input = $request->all();
            $orders = Order::where('id',$input['id'])->first();
            $policy = Policy::where('order_id',$input['id'])->first();
            $payment = $orders->payment()->first();
            $result = [];
            
            if(!empty($policy)){
                logTransaction($orders->order_number,"ดำเนินการสร้างใบเสร็จ","SUCCESS");
                $receipt_data = [
                    'receipt_number' => $policy->tax_note_number,
                    'order_number' => $orders->order_number,
                    'payment_number' => $payment->payment_no,
                    'policy_number' => $policy->policy_number,
                    'amount' => !empty($input['amount'])?floatval($input['amount']):$payment->amount,
                    'credit_number' => !empty($input['masked_pan'])?$input['masked_pan']:null,
                ];
                $receiptId = Receipt::create($receipt_data)->id;
                if(!empty($receiptId)){
                    logTransaction($orders->order_number,"สร้างใบเสร็จสำเร็จ","SUCCESS");
                    return json_encode([
                        'status' => 'SUCCESS',
                    ]);
                }else{
                    logTransaction($orders->order_number,"สร้างใบเสร็จล้มเหลว","FAIL");
                    return json_encode([
                        'status' => 'ERROR',
                        'message' => 'ไม่ทราบสาเหตุ กรุณาติดต่อผู้ดูแลระบบ'
                    ]);
                }
                
            }else{
                return json_encode([
                    'status' => 'ERROR',
                    'message' => 'ไม่พบกรมธรรม์'
                ]);
            }

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }


    public function createPdf(Request $request){
        try{
            $input = $request->all();
            $orders = Order::where('id',$input['id'])->first();
            $policy = Policy::where('order_id',$input['id'])->first();
            $payment = $orders->payment()->first();
            $compile = new Compile();
            return json_encode($compile->createPdf($policy,$input['type']));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function sendFTP(Request $request){
        try{
            $input = $request->all();
            $orders = Order::where('id',$input['id'])->first();
            $policy = Policy::where('order_id',$input['id'])->first();
            $compile = new Compile();
            return json_encode($compile->resendFTP($policy,$input['type']));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function sendSMS(Request $request){
        try{
            $input = $request->all();
            $policy = Policy::where('order_id',$input['id'])->first();
            $sms = new SMS();
            if($sms->sendSMS($policy,null)){
                return json_encode([
                    'status' => 'SUCCESS'
                ]);
            }else{
                return json_encode([
                    'status' => 'ERROR',
                    'message' => 'ส่ง SMS ไม่สำเร็จ'
                ]);
            }

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function sendEmail(Request $request){
        try{
            $input = $request->all();
            $policy = Policy::where('order_id',$input['id'])->first();
            $mail = new MyEmail();
            if($mail->sendEmail($policy,null)){
                return json_encode([
                    'status' => 'SUCCESS'
                ]);
            }else{
                return json_encode([
                    'status' => 'ERROR',
                    'message' => 'ส่ง Email ไม่สำเร็จ'
                ]);
            }

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function sendEndorse(Request $request){
        $input = $request->all();
        $policy = Policy::where('order_id',$input['id'])->first();
        $endorse = new Endorse();
        if($endorse->request($policy)!=false){
            $policy->update_core_status = 1;
            $policy->save();
            return json_encode([
                'status' => 'SUCCESS'
            ]);
        }else{
            return json_encode([
                'status' => 'ERROR',
                'message' => 'ส่ง Endorse ไม่สำเร็จ'
            ]);
        }
    }



}
