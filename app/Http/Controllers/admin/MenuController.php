<?php

namespace App\Http\Controllers\admin;

use App\Repositories\MenuRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Menus;
use App\Models\Permission;
use App\Models\Privilege;
use Validator;
use Auth;

class MenuController extends Controller
{
  private $menu1 = "motoradmins/menu";

  public function __construct(MenuRepository $menu){
    $this->menu = $menu;
    $this->middleware('auth:admin');
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    return view('admin.menu.menu');
  }

  public function genMenu(Request $request){
    try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $menu = Menus::orderBy('orders','asc')->get()->toArray();
            foreach ($menu as $key => $value) {
                if($menu[$key]['parent_id']=='0'){
                    $menu[$key]['sub_menu'] = $this->findSubMenu($menu,$menu[$key]['id']);
                    if(sizeof($menu[$key]['sub_menu'])>0){
                        foreach ($menu[$key]['sub_menu'] as $subkey => $subvalue){
                            $menu[$key]['sub_menu'][$subkey]['sub_menu'] = $this->findSubMenu($menu,$menu[$key]['sub_menu'][$subkey]['id']);
                        }
                    }
                    
                }
            }
            return json_encode($menu);
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  public function getParentMenu(Request $request){
    try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $menu = $this->menu->get_parent_menu();
            return $menu->toJson();
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  public function getMenuPrivilege(Request $request){
    try{
        if($request->ajax()){
            $data = Privilege::where('id','!=',1)->get();
            return $data->toJson();
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }

  public function saveMenu(Request $request){
      try{
          if(!has_permission($this->menu1,["view","add","edit"])){
             return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){
            $input = $request->all();
             // Validate 
            $validator = Validator::make($input, [
                'name' => (empty($input['menu_id'])?'required|max:100|unique:menus':'required|max:100'),
                'name_th' => (empty($input['menu_id'])?'required|max:200|unique:menus':'required|max:200'),
                'orders' => 'required|numeric'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'parent_id'=> !empty($input['parent_id'])?$input['parent_id']:0,
                'name'=> $input['name'],
                'name_th'=> $input['name_th'],
                'path'=> $input['path'],
                'icon'=> $input['icon'],
                'orders' => $input['orders'],
                'isshow' => !empty($input['isshow'])?1:0
            );
            $level = 0;
            if($data['parent_id']!=0){
                $parentData = Menus::where('id',$data['parent_id'])->first();
                if($parentData->parent_id==0) $level =1;
                else $level = 2;
            }
            $data['level'] = $level;


            if(empty($input['menu_id'])){
                $id = Menus::create($data)->id;
                if(!empty($id)){
                    $privilege = Privilege::all()->pluck('id')->toArray();
                    foreach ($privilege as $key => $privilege_id) {
                        $role_check = 0;
                        if($privilege_id==1 || $privilege_id==2){
                            $role_check = 1;
                        }
                        $permission = array(
                            'privilege_id'=>$privilege_id,
                            'menu_id'=>$id,
                            'view'=>$role_check,
                            'add'=>$role_check,
                            'edit'=>$role_check,
                            'delete'=>$role_check
                        );
                        Permission::create($permission);
                    }
                }
            }else{
                Menus::where('id', '=', $input['menu_id'])->update($data);
            }
            return json_encode(array("status"=>"success"));
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getMenuData(Request $request){
      try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $menu = Menus::where('id','=',$input['id'])->first();
            return $menu->toJson();
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  private function findSubMenu($menu,$parent_id){
    $result = array();
    foreach ($menu as $key => $value) {
        if($menu[$key]['parent_id']==$parent_id){
            array_push($result,$menu[$key]);
        }
    }
    return $result;
  }

  public function updateOrder(Request $request){
    try{
        if(!has_permission($this->menu1,["view","edit"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'orders' => 'required'
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(array("resp_error"=>$validate_first[0]));
            }
            $orders = $input['orders'][0];
            foreach ($orders as $key => $value) {
                $order = $key+1;
                $data = array(
                    'orders'=> $order,
                    'parent_id'=>0,
                    'level'=>0
                );
                Menus::where('id', '=', $orders[$key]['id'])->update($data);
                if(isset($orders[$key]['children'])){
                    $children = $orders[$key]['children'][0];
                    foreach ($children as $key2 => $value2) {
                        $order2 = $order+(($key2+1)/10);
                        $data2 = array(
                            'orders'=> $order2,
                            'parent_id'=> $orders[$key]['id'],
                            'level'=>1
                        );
                        Menus::where('id', '=', $children[$key2]['id'])->update($data2);
                        /* Level 3 */
                        if(isset($children[$key2]['children'])){
                            $children2 = $children[$key2]['children'][0];
                            foreach ($children2 as $key3 => $value3) {
                                $order3 = $order+(($key2+1)/10)+(($key3+1)/100);
                                $data3 = array(
                                    'orders'=> $order3,
                                    'parent_id'=> $children[$key2]['id'],
                                    'level'=>2
                                );
                                Menus::where('id', '=', $children2[$key3]['id'])->update($data3);
                            }
                        }
                        /************************/
                    }
                }
            }
            return json_encode(["status"=>"success"]);
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }

  public function deleteMenu(Request $request){
    try{
        if(!has_permission($this->menu1,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
            
            // Validate 
            $validator = Validator::make($input, [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(array("resp_error"=>$validate_first[0]));
            }

            Menus::where('id', '=', $input['id'])->delete();
            Permission::where('menu_id','=',$input['id'])->delete();
            $submenu = Menus::where('parent_id', '=', $input['id'])->get();
            //วนลบ sub_menu permission
            foreach ($submenu as $key => $value) {
                Menus::where('id', '=', $submenu[$key]['id'])->delete();
                Permission::where('menu_id','=',$submenu[$key]['id'])->delete();
            }
            return json_encode(array("status"=>"success"));
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  public function get_permission_menu(Request $request){
    try{
        if(!has_permission($this->menu1)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
           // Validate 
            $validator = Validator::make($input, [
              'privilege_id' => 'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = $this->menu->get_menu_permission($input);
            return $data->toJson();
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }

  public function setPermission(Request $request){
    try{
        if(!has_permission($this->menu1,["view","add","edit"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
            $input = $request->all();
           // Validate 
            $validator = Validator::make($input, [
              'name' => 'required',
              'menu_id' => 'required',
              'is_check' => 'required',
              'privilege_id' => 'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }

            if($input['privilege_id']==Auth::guard('admin')->user()->privilege){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if(($input['privilege_id']==1 || $input['privilege_id']==2) && Auth::guard('admin')->user()->privilege!=1){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $keys = array($input['name']);
            $data = array_fill_keys($keys,($input['is_check']=='true'?1:0));
            Permission::where('privilege_id','=',$input['privilege_id'])
                        ->where('menu_id','=',$input['menu_id'])
                        ->update($data);
            return json_encode(array("status"=>"success"));
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }



  
}
