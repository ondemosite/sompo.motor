<?php

namespace App\Http\Controllers\admin;

use App\Repositories\GarageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\GarageType;
use App\Models\Garage;
use App\Models\Province;
use App\Models\District;
use App\Models\SubDistrict;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image as Image;
use Yajra\Datatables\Datatables;

class GarageController extends Controller
{

  private $menu1 = "motoradmins/garage/garage";
  private $menu2 = "motoradmins/garage/garage_type";


  public function __construct(){
    $this->middleware('auth:admin');
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    
    $init = [];
    $init['type_class'] = ["GENERAL"=>"GENERAL","DEALER"=>"DEALER"];
    $init['type'] = GarageType::pluck('name','id')->toArray();
    $init['province'] = Province::pluck('name_th','id')->toArray();

    
    return view('admin.garage.garage',["init"=>$init]);
  }

  public function getDistrict(Request $request){
    try{
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'province_id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $response = District::where('province_id',$input['province_id'])->pluck('name_th','id')->toArray();
            if(!empty($response)){
                return json_encode($response);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }

  public function getSubDistrict(Request $request){
    try{
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'district_id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $response = SubDistrict::where('district_id',$input['district_id'])->pluck('name_th','id')->toArray();
            if(!empty($response)){
                return json_encode($response);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }

  public function garageType(){
    if(!has_permission($this->menu2)){
        return view('admin.common.permission_denied');
    }
    return view('admin.garage.garage_type');
  }

  public function getGarageType(Request $request){
      try{
        if(!has_permission($this->menu2)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $garageType = GarageType::All();
          return Datatables::of($garageType)->addColumn('action', function ($data) {
              return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
          })->make(true);
        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

  public function getDataGarageType(Request $request){
      try{
        if(!has_permission($this->menu2)){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){
          $input = $request->all();
          // Validate 
          $validator = Validator::make($input, [
            'id' => 'required',
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }

          $response = GarageType::where('id',$input['id'])->first();
          if(!empty($response)){
            return json_encode($response);
          }

        }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }


  public function saveGarageType(Request $request){
      try{
          if($request->ajax()){
            
            if(!has_permission($this->menu2,["view","edit","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $input = $request->all();
            
            // Validate 
            $validator = Validator::make($input, [
                'name'=>'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'name'=> $input['name'],
                'updated_by' => Auth::guard('admin')->user()->id
            );
            if(!empty($input['id'])){ //Edit mode
                GarageType::where('id', '=', $input['id'])->update($data);
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                GarageType::create($data);
            }
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function deleteGarageType(Request $request){
      try{
          
          if(!has_permission($this->menu1,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){

            $input = $request->all();
            $validator = Validator::make($input, [
              'id' => 'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            GarageType::where('id', '=', $input['id'])->delete();
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

/* ********************** Garage ****************** */
public function getGarage(Request $request){
    try{
      if(!has_permission($this->menu1)){
          return json_encode(["resp_error"=>trans('common.permission_denied')]);
      }
      if($request->ajax()){
        $input = $request->all();
        $repository = new GarageRepository();
        $data_resp = $repository->list_garage($input);
        return Datatables::of($data_resp)->addColumn('action', function ($data) {
            return '<a href="'.route('admin.garage_form').'/'.$data->id.'" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->name.'"><i class="fa fa-trash"></i></a>';
        })->editColumn('type',function($data){
            return $data->type_name()->first()->name;
        })->editColumn('province',function($data){
            return $data->province()->first()->name_th;
        })->editColumn('district',function($data){
            return $data->district()->first()->name_th;
        })->editColumn('sub_district',function($data){
            return $data->sub_district()->first()->name_th;
        })->make(true);
      }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}

public function getDataGarage(Request $request){
    try{
      if(!has_permission($this->menu1)){
          return json_encode(["resp_error"=>trans('common.permission_denied')]);
      }
      if($request->ajax()){
        $input = $request->all();
        // Validate 
        $validator = Validator::make($input, [
          'id' => 'required',
        ]);
        if ($validator->fails()) {
          $validate_first = $validator->errors()->all();
          return json_encode(["resp_error"=>$validate_first[0]]);
        }

        $response = Garage::where('id',$input['id'])->first();
        if(!empty($response)){
          return json_encode($response);
        }

      }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
    }
}

public function saveGarage(Request $request){
    try{
          if(!has_permission($this->menu1,["view","edit","add"])){
              return view('admin.common.permission_denied');
          }

          $input = $request->all();
          // Validate 
          $filter_validate = [
              'type_class'=>'required',
              'type'=>'required',
              'name'=>'required',
              'province'=>'required',
              'district'=>'required',
              'sub_district'=>'required',
              'lat'=>'required',
              'long'=>'required',
          ];

          $validator = Validator::make($input,$filter_validate);
          if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
          }

          $data = array(
            'type_class'=>$input['type_class'],
            'type'=>$input['type'],
            'name'=>$input['name'],
            'address'=>!empty($input['address'])?$input['address']:'',
            'province'=>$input['province'],
            'district'=>$input['district'],
            'sub_district'=>$input['sub_district'],
            'tel'=>!empty($input['tel'])?$input['tel']:'',
            'fax'=>!empty($input['fax'])?$input['fax']:'',
            'lat'=>$input['lat'],
            'long'=>$input['long'],
            'status'=>!empty($input['status'])?1:0,
            'updated_by' => Auth::guard('admin')->user()->id
          );
          if(!empty($input['garage_id'])){ //Edit mode
            Garage::where('id', '=', $input['garage_id'])->update($data);
            return redirect(route('admin.garage_form')."/".$input['garage_id'])->with('status', 'Updated Garage Success!!');
          }else{ //Add mode
            $data['created_by'] = Auth::guard('admin')->user()->id;
            $id = Garage::create($data)->id;
            if(!empty($id)){
                return redirect(route('admin.garage_form')."/".$id)->with('status', 'Created Garage Success!!');
            }
          }
          

    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return back()->withInput()->withErrors(['Error']);
    }
}

public function deleteGarage(Request $request){
    try{
        
        if(!has_permission($this->menu1,["view","delete"])){
          return json_encode(["resp_error"=>trans('common.permission_denied')]);
        }
        if($request->ajax()){

          $input = $request->all();
          $validator = Validator::make($input, [
            'id' => 'required'
          ]);
          if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
          }
          Garage::where('id', '=', $input['id'])->delete();
          return json_encode(["status"=>"success"]);
        }       
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
}


public function garageForm($id = null){
    try{
        if(!has_permission($this->menu1,["view","edit","add"])){
            return view('admin.common.permission_denied');
        }

        $return['id'] = $id;
        if(!empty($id)){ //Edit Mode
            $data = Garage::where('id','=',$id)->first();
            if($data){
                $return['id'] = !empty($data->id)?$data->id:'';
                $return['type_class'] = !empty($data->type_class)?$data->type_class:'';
                $return['type'] = !empty($data->type)?$data->type:null;
                $return['name'] = !empty($data->name)?$data->name:'';
                $return['address'] = !empty($data->address)?$data->address:'';
                $return['province'] = !empty($data->province)?$data->province:null;
                $return['district'] = !empty($data->district)?$data->district:null;
                $return['sub_district'] = !empty($data->district)?$data->district:null;
                $return['tel'] = !empty($data->tel)?$data->tel:'';
                $return['fax'] = !empty($data->fax)?$data->fax:'';
                $return['lat'] = !empty($data->lat)?$data->lat:'';
                $return['long'] = !empty($data->long)?$data->long:'';
                $return['status'] = !empty($data->status)?$data->status:0;
                $return['mode'] = "edit";
            }else{
                return back()->withInput()->withErrors(['No Data Found!']);
            }
        }else{ //Add Mode
            $return['mode'] = "add";
        }
        $init = [];
        $init['type_class'] = ["GENERAL"=>"GENERAL","DEALER"=>"DEALER"];
        $init['type'] = GarageType::pluck('name','id')->toArray();
        $init['province'] = Province::pluck('name_th','id')->toArray();
        return view('admin.garage.garage_form',["data"=>$return,"init"=>$init]);
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return back()->withInput()->withErrors(['Error']);
    }
}




public function findID($garage_type,$string){
    foreach($garage_type as $key => $value){
        if(trim($string)==$key) return $value;
    }
}
public function getProvinceData($string){
    try{
        return Province::where('name_th',trim($string))->first()->id;
    }catch (\Exception $e) {
        return -1;
    }
}
public function getDistrictData($string,$province){
    try{
        return District::where(['name_th'=>trim($string),'province_id'=>$province])->first()->id;
    }catch (\Exception $e) {
        return -1;
    }
}
public function getSubDistrictData($string,$province,$district){
    try{
        return  SubDistrict::where([
            'name_th'=>trim($string),
            'province_id'=>$province,
            'district_id' => $district
        ])->first()->id;
    }catch (\Exception $e) {
        return -1;
    }
    
}


public function setData(){
    
    set_time_limit(0);
    header('Content-Type: text/html; charset=UTF-8');
    if (($handle = fopen ( public_path () . '/import/garage.csv', 'r' )) !== FALSE) {
        $garage_type = GarageType::pluck('id','name')->toArray();
        $loop = 0;
        while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
            if(empty($data[0])) break;
            $type_class = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $data[0]);
            $type = $this->findID($garage_type,$data[1]);
            $name = $data[2];
            $address = $data[3];

            $province = $this->getProvinceData($data[6]);
            if($province==-1){
                echo $data[6];
                echo "Province";
                echo "<br/>".$loop;
                exit();
            }
            
            $district = $this->getDistrictData($data[5],$province);
            if($district==-1){
                echo $data[5];
                echo "District";
                echo "<br/>".$loop;
                exit();
            }

            $sub_district = $this->getSubDistrictData($data[4],$province,$district);
            if($sub_district==-1){
                echo $data[4];
                echo "Sub District";
                echo "<br/>".$loop;
                exit();
            }
            
            
            $tel = $data[7];
            $fax = $data[8];
            $lat = $data[9];
            $long = $data[10];

            $garage = new Garage();
            $garage->type_class = $type_class;
            $garage->type = $type;
            $garage->name = $name;
            $garage->address = $address;
            $garage->sub_district = $sub_district;
            $garage->district = $district;
            $garage->province = str_pad($province,2,"0",STR_PAD_LEFT);
            $garage->tel = $tel;
            $garage->fax = $fax;
            $garage->lat = $lat;
            $garage->long = $long;
            $garage->created_by = 1;
            $garage->updated_by = 1;
            $garage->save();
            $loop++;
        }
        fclose ( $handle );
    }
}








 
}
