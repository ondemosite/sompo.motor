<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Prefix;
use Validator;
use DateTime;
use Illuminate\Support\Facades\Log;
use Auth;

class PrefixController extends Controller
{

    private $menu1 = "motoradmins/prefix";

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        if(!has_permission($this->menu1)){
            return view('admin.common.permission_denied');
        }
        return view('admin.prefix.prefix');
    }



    public function list(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
                $query = Prefix::All();
                return Datatables::of($query)->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" class="m-a-1 action-edit" title="edit" data-id="'.$data->id.'"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="m-a-1 action-delete" title="delete" data-id="'.$data->id.'" data-name="'.$data->title.'"><i class="fa fa-trash"></i></a>';
                })->make(true);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function get(Request $request){
        try{
            if(!has_permission($this->menu1)){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }
            if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $response = Prefix::where('id',$input['id'])->first();
            return json_encode($response);

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
        }
    }


  public function save(Request $request){
      try{
          if($request->ajax()){
            
            if(!has_permission($this->menu1,["view","edit","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $input = $request->all();
            
            // Validate 
            $validator = Validator::make($input, [
                'name'=>'required',
                'name_en'=>'required',
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = array(
                'name' => $input['name'],
                'name_en'=> $input['name_en'],
                'is_default' => isset($input['is_default'])?'YES':'NO',
                'status' => isset($input['status'])?'ACTIVE':'INACTIVE',
                'gender' => $input['gender']
            );
            if(!empty($input['id'])){ //Edit mode
                Prefix::where('id', '=', $input['id'])->update($data);
            }else{ //Add mode
                $data['created_by'] = Auth::guard('admin')->user()->id;
                Prefix::create($data);
            }
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
      }
  }

  public function delete(Request $request){
      try{
          
          if(!has_permission($this->menu1,["view","delete"])){
            return json_encode(["resp_error"=>trans('common.permission_denied')]);
          }
          if($request->ajax()){

            $input = $request->all();
            $validator = Validator::make($input, [
              'id' => 'required'
            ]);
            if ($validator->fails()) {
              $validate_first = $validator->errors()->all();
              return json_encode(["resp_error"=>$validate_first[0]]);
            }
            Prefix::where('id', '=', $input['id'])->delete();
            return json_encode(["status"=>"success"]);
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(["resp_error"=>$e->getMessage()]);
      }
  }

 
}
