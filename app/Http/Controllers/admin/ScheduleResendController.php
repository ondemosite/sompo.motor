<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\ScheduleResend;
use App\Models\ScheduleResendLog;
use App\Models\Policy;
use App\Library\MyEmail;
use App\Library\Endorse;

class ScheduleResendController extends Controller
{


  public function __construct(){
    
  }

  public function resendEmail(){
    $scheduleResend = ScheduleResend::where('type','=','EMAIL')
    ->where('retry_remaining','>',0)
    ->where('complete_status','=','WAITING')
    ->where('process_status','=',0)
    ->orderBy('id','asc')->get();

    if(!$scheduleResend->isEmpty()){
        $mail = new MyEmail();
        foreach($scheduleResend as $record){
            $policy = Policy::where('id',$record->policy_id)->first();
            if(!empty($policy)){
                $record->process_status = 1;
                $record->complete_status = 'PENDING';
                $record->save();
                
                $tringAttemp = (3 - $record->retry_remaining) + 1;
                $this->createLog($record->id,$tringAttemp);
                
                if($mail->sendEmail($policy,null)){
                    $record->complete_status = 'SUCCESS';
                }else{
                    if($record->retry_remaining - 1 == 0){
                        $record->complete_status = 'FAILED';
                    }else{
                        $record->complete_status = 'WAITING';
                    }
                }
                
                $record->retry_remaining = $record->retry_remaining - 1;
                $record->process_status = 0;
                $record->save();

            }
        }
    }
  }

  public function resendEndorse(){
    $scheduleResend = ScheduleResend::where('type','=','ENDORSE')
    ->where('retry_remaining','>',0)
    ->where('complete_status','=','WAITING')
    ->where('process_status','=',0)
    ->orderBy('id','asc')->get();

    if(!$scheduleResend->isEmpty()){
        $endorse = new Endorse();
        foreach($scheduleResend as $record){
            $policy = Policy::where('id',$record->policy_id)->first();
            if(!empty($policy)){
                $record->process_status = 1;
                $record->complete_status = 'PENDING';
                $record->save();
                
                $tringAttemp = (3 - $record->retry_remaining) + 1;
                $this->createLog($record->id,$tringAttemp);
                
                if($endorse->request($policy)!=false){
                    $policy->update_core_status = 1;
                    $policy->save();
                    $record->complete_status = 'SUCCESS';
                    $this->updateLog($record->id,'SUCCESS');
                }else{
                    if($record->retry_remaining - 1 == 0){
                        $record->complete_status = 'FAILED';
                    }else{
                        $record->complete_status = 'WAITING';
                    }
                    $this->updateLog($record->id,'FAILED');
                }
                
                $record->retry_remaining = $record->retry_remaining - 1;
                $record->process_status = 0;
                $record->save();

            }
        }
    }
  }

  private function createLog($id,$try){
      ScheduleResendLog::create([
          'schedul_resend_id' => $id,
          'round_effort' => $try
      ]);
  }

  private function updateLog($id, $status){
    ScheduleResendLog::where('schedul_resend_id',$id)
    ->update([
        'status' => $status
    ]);
  }


 
}
