<?php

namespace App\Http\Controllers\admin;

use App\Repositories\GarageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Tax;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

class TaxController extends Controller
{

  private $menu1 = "motoradmins/tax/tax";

  public function __construct(){
    $this->middleware('auth:admin');
  }

  public function index(){
    if(!has_permission($this->menu1)){
        return view('admin.common.permission_denied');
    }
    
    $init = [];
    $init['tax'] = Tax::where('id',1)->first()->toArray();

    return view('admin.tax.tax',["init"=>$init]);
  }

  public function saveTax(Request $request){
      try{
            if(!has_permission($this->menu1,["view","edit","add"])){
                return json_encode(["resp_error"=>trans('common.permission_denied')]);
            }

            $input = $request->all();
            
            // Validate 
            $filter_validate = [
                'vat'=>'required|numeric',
                'stamp'=>'required|numeric'
            ];
            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $data = array(
                'vat'=> $input['vat'],
                'stamp'=> $input['stamp'],
                'compulsory_medical_fee'=>$input['compulsory_medical_fee'],
                'compulsory_death_disable'=>$input['compulsory_death_disable'],
                'updated_by' => Auth::guard('admin')->user()->id
            );
            Tax::where('id', '=', 1)->update($data);
            return redirect(route('admin.tax'))->with('status', 'Updated Tax Success!!');

      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return back()->withInput()->withErrors(['Error']);
      }
  }



 
}
