<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Promotion;
use App\Models\PromotionRegister;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use Illuminate\Support\Facades\Log;
use Validator;

class PromotionController extends Controller
{
  public function __construct(){
    //$this->middleware('auth:web');
  }

  public function index(Request $request){
    $input = $request->all();
    $init = [];
    if(!empty($input['ref'])){
      $promotion = Promotion::where('formular',$input['ref'])->first();
      if(!empty($promotion)){
        $init['cover'] = $promotion->cover_register;
        $init['status'] = $this->getPromotionStatus($promotion);
        $init['expire'] = ($promotion->register_expire_at > date('Y-m-d H:i:s'))?true:false;
        $init['promotion_id'] = $promotion->id;
        $init['brand_top'] = VehicleBrand::select('id','name','top_order')->whereNotNull('top_order')->orderBy('top_order','asc')->get()->toArray();
        $init['brand_order'] = VehicleBrand::select('id','name','top_order')->whereNull('top_order')->orderBy('name','asc')->get()->toArray();
        $init['brand'] = array_merge($init['brand_top'],$init['brand_order']);
        $init['ending_message'] = $promotion->ending_message;
        $init['fix_brand'] = $promotion->vehicle_brand;
        $init['fix_model'] = $promotion->vehicle_model;
      }else{
        $init['status'] = "INACTIVE";
      }
    }else{
      $init['status'] = "INACTIVE";
    }
    return view('web.promotion.promotion',[
      'init' => $init
    ]);
  }

  public function register(Request $request){
    try{
      $input = $request->all();

      if(!empty($input['promotion_id'])){
        $validator = Validator::make($input, [
          'input_name' => 'required|max:200',
          'input_lastname' => 'required|max:200',
          'input_email' => 'required|max:100',
          'input_tel' => 'required|max:20'
        ]);
        if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(["resp_error"=>$validate_first[0]]);
        }
        $promotion = Promotion::where('id',$input['promotion_id'])->first();
        if(!empty($promotion)){
          $status = $this->getPromotionStatus($promotion);
          if($status == "ACTIVE"){

            //Check Received
            $receivedStatus = $this->checkRegisterReceived($input);
            if(!$receivedStatus['status']){
              return json_encode(["resp_error"=> $receivedStatus['message']]);
            }
            
            $data = [
              'promotion_id' => $input['promotion_id'],
              'name' => $input['input_name'],
              'lastname' => $input['input_lastname'],
              'email' => $input['input_email'],
              'tel' => $input['input_tel'],
            ];
            $id = PromotionRegister::create($data)->id;
            return response()->json([
              'status' => 'SUCCESS'
            ]);
          }
        }
        return json_encode(array("resp_error"=>"Invalid Data"));
      }

    }catch (\Exception $e) {
      Log::error($e->getMessage());
      return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  private function checkRegisterReceived($input){
    $registered = PromotionRegister::where('promotion_id',$input['promotion_id'])
    ->where(function($query) use ($input){
        $query->where(function($query2) use ($input){
          $query2->where('name','=',$input['input_name'])
          ->where('lastname','=',$input['input_lastname']);
        });

        $query->orWhere('email','=',$input['input_email']);
    })->first();

    if(!empty($registered)){

      if($registered->name==$input['input_name'] && $registered->lastname==$input['input_lastname']){
        return [
          'status' => false,
          'message' => "ชื่อและนามสกุลนี้ ได้ลงทะเบียนกับโปรโมชั่นนี้ไปแล้ว"
        ];
      }else{
        return [
          'status' => false,
          'message' => "อีเมลนี้ ได้ลงทะเบียนโปรกับโมชันนี้ไปแล้ว"
        ];
      }

    }else{
      return [
        'status' => true
      ];
    }

  }

  private function getPromotionStatus($promotion){
    if($promotion->status==0 || $promotion->is_register==0){
      return "INACTIVE";
    }else if($promotion->expire_at < date('Y-m-d H:i:s')){
      return "PROMOTION_EXPIRE";
    }else if($promotion->register_expire_at < date('Y-m-d H:i:s')){
      return "REGISTER_EXPIRE";
    }else if($promotion->max_register > 0){
      $count_list = PromotionRegister::where('promotion_id',$promotion->id)->count();
      if($count_list >= $promotion->max_register){
        return "MAX_GRANT";
      }
    }
    return "ACTIVE";
  }

  public function getVehicleBrand(Request $request){
    try{
        if($request->ajax()){
            // Validate
            $brand = VehicleBrand::orderBy('top_order','asc')->get()->pluck('name','id');
            if(!empty($brand));
            return response()->json($brand->toArray());
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }


  public function getVehicleModel(Request $request){
      try{
          if($request->ajax()){
              // Validate
              $input = $request->all();
              $validator = Validator::make($input, [
                  'id'=>'required',
              ]);
              if ($validator->fails()) {
                  $validate_first = $validator->errors()->all();
                  return json_encode(["resp_error"=>$validate_first[0]]);
              }

              $model = VehicleModel::where('brand_id',$input['id'])->pluck('name','id')->toArray();
              if(is_array($model)){
                  return response()->json($model);
              }else{
                  return response()->json([]);
              }
          }
      }catch (\Exception $e) {
          Log::error($e->getMessage());
          return json_encode(array("resp_error"=>$e->getMessage()));
      }
  }



}
