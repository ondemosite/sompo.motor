<?php

namespace App\Http\Controllers\web;

use App\Repositories\AdminRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Policy;
use App\Models\OrderTest;
use App\Models\RunningNumber;
use App\Models\SmsLog;
use App\Models\Vehicle;
use App\Models\VehicleInfo;
use App\Models\Otp as OtpModel;
use App\Models\ConfirmPolicy;
use Illuminate\Support\Facades\DB;
use App\Library\insuranceManager;
use App\Library\Mpasskit\Mpasskit;
use App\Library\Mpasskit\PassKit;
use App\Library\SMS;
use App\Library\Endorse;
use App\Library\MyEmail;
use App\Library\OTP;
use App\Library\Blacklist;
use App\Library\Compulsory as Barcode;
use App\Library\Signature;
use Excel;

use PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Password;
use File;

class TestController extends Controller
{

  public function __construct(AdminRepository $admin){
    $this->admin = $admin;
  }

  public function locktable1(){
    return view('web.test.locktable');
  } 

  public function locktable2(){
    return view('web.test.locktable2');
  }

  public function removeLogFile(){
    $path = storage_path().'/logs/laravel.log';
    File::put($path,'');
  }

  public function openLogFile(){
    $logFile = file(storage_path().'/logs/laravel.log');
    $logCollection = [];
    // Loop through an array, show HTML source as HTML source; and line numbers too.
    foreach ($logFile as $line_num => $line) {
       $logCollection[] = array('line'=> $line_num, 'content'=> htmlspecialchars($line));
    }
    echo "<pre>";
    print_r($logCollection);
    echo "</pre>";
  }


  public function generateOrderNumber(Request $request){
      $input = $request->all();
      DB::beginTransaction();
      $order_result = null;
      try{  
          $state = "run";
          do{
            /* Get Runing Number */
            $number_record = DB::table('order_running_number')->where('year', '=', date('Y'))->lockForUpdate()->first();
            $now_running = intval($number_record->number);
            $order_number = "OD-".date('Y-m-').str_pad(++$now_running,7,"0",STR_PAD_LEFT);
            /* -------------------*/
            /* Create Order */
            $data = ['order_number' => $order_number];
            $set_response = $this->setOrder($data);
            if($set_response=="TRUE"){
                $runing_record = OrderRunningNumber::where('year',date('Y'))->first();
                $runing_record->number = $now_running;
                $runing_record->save();
                $state = "stop";
                $order_result = $order_number;
            }else{
                DB::rollBack();
            }
          }while($state=="run");
      }catch (\Exception $e) {
          DB::rollBack();
      }
      DB::commit();
      return "SUCCESS";
  }


  private function setOrder($data){
    try{
        $id = OrderTest::create($data)->id;
        if(!empty($id)){
            return "TRUE";
        }
        return "FALSE";
    }catch (\Exception $e) {
        DB::rollBack();
        return "FALSE";
    }
  }

  public function genRunningYear(){
      /*
    $start = 2018;
    $end = $start+100000;
    set_time_limit(0);
    for($i=$start;$i<=$end;$i++){
        RunningNumber::create([
          'order' => 0,
          'policy' => 0,
          'misc' => 0,
          'tax' => 0,
          'year' => $i,
        ]);
    }*/
    
      
  }

  public function testVehicle(){
    $policy = Policy::where('id',19)->first();
    $vehicle = $policy->vehicle_info()->first();
    print_r($vehicle->toArray());
  }

  public function testSendEmail(){
    $endorse = new Endorse();
    $result = $endorse->alertToAdmin(1);
    var_dump($result);
  }

  public function testEndorse(){
    $endorse = new Endorse();
    $policy = Policy::where('id',9)->first();
    $response = $endorse->request($policy);
    var_dump($response);
  }

  public function testPdf(){
    $data = Policy::where('id',2)->first();
    $payment = $data->payment()->first();
    $fix = ['tax_id_no' => '0107555000287'];

    $pdf = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data,'clean' => false])->setPaper('A4');
   //$pdf = PDF::loadView('admin.pdf.mt-compulsory.blade',['init'=>$data,'fix'=>$fix, "clean" => false])->setPaper('A4');
    return $pdf->stream();
  }

  public function logTransaction(){
    logTransaction(1,"What!","FAIL");
  }

  public function testBarcode($policyNumber){
      $barcode = new Barcode();
      $stickerNo = $barcode->getBarcode($policyNumber);
      echo $stickerNo;
  }

  public function testPdf2(){
    $data = Policy::where('id',2)->first();
    $pdf = PDF::loadView('admin.pdf.mt-compulsory',['init'=>$data,'sticker_no'=>'6210170247501','clean'=> false])->setPaper('A4');
    return $pdf->stream();
  }

  public function testDigitalSign(){
      $signature = new Signature();
      $fileName = uniqid().".pdf";
      $password = "1234";
        $requestParams = array();


        $fileContents = Storage::disk('pdf')->get('/user/policy/nimman.pdf'); 

        $requestParams[] = array(
            'filename' => $fileName,
            'password' => $password,
            'fileStream' => base64_encode($fileContents)
        );

        //Log::info($requestParams);
        $response = $signature->request($requestParams);
        $result = json_decode($response);
        var_dump($result);

        if (empty($result->responseHeader->statusCode)) {
            // SignatureLog::create([
            //     'order_id' => $order_id,
            //     'detail' => htmlspecialchars($response),
            //     'status' => 'ERROR'
            // ]);
            // return 'ERROR';
            echo "ERROR";
        }

        if ($result->responseHeader->statusCode == '2000') {
            foreach ($result->responseDetail as $item) {
                if (!empty($item->filename)) {
                    $responseContents = base64_decode($item->fileStream);
                    Storage::disk('pdf')->put('/user/policy/nimman.pdf',$responseContents); //Save
                }
            }

            // SignatureLog::create([
            //     'order_id' => $order_id,
            //     //'detail' => htmlspecialchars($response),
            //     'detail' => $filename,
            //     'status' => 'SUCCESS'
            // ]);

            return 'SUCCESS';
        } else {
            // SignatureLog::create([
            //     'order_id' => $order_id,
            //     'detail' => $response,
            //     'status' => 'ERROR'
            // ]);
            return 'ERROR';
        }
  }

  public function epassExample(){
    $_apiKey = "JbgXdAipwKeNyjnpelPfFFAzfKcFQtni";
    $_apkSecret = "ehUL1IUlSJtLmmu6TzCyUjt98sjMIhCkMF6Gm3mAftcyXyAGix4GqqhW3q9oV59sGeGv8lOV4AFwa1scmPaiQJAXgp9cPitOcLcp9rSRwHha4BDrIqud6wzEIEXB5Py0";
    $mk = new Mpasskit();
    $result = $mk->getTemplate("SOMPO_MO_FOR_B2C");
    $array = json_decode(json_encode($result), true);
    echo '<pre>';
        print_r($array);
    echo '</pre>';
  }

  public function epassCertList(){
    $_apiKey = "JbgXdAipwKeNyjnpelPfFFAzfKcFQtni";
    $_apkSecret = "ehUL1IUlSJtLmmu6TzCyUjt98sjMIhCkMF6Gm3mAftcyXyAGix4GqqhW3q9oV59sGeGv8lOV4AFwa1scmPaiQJAXgp9cPitOcLcp9rSRwHha4BDrIqud6wzEIEXB5Py0";
    $pk = new PassKit($_apiKey,$_apkSecret);
    $result = $pk->ListCertificates();
    echo '<pre>';
        print_r($result);
    echo '</pre>';
  }

  public function epassCampaignList(){
    $_apiKey = "JbgXdAipwKeNyjnpelPfFFAzfKcFQtni";
    $_apkSecret = "ehUL1IUlSJtLmmu6TzCyUjt98sjMIhCkMF6Gm3mAftcyXyAGix4GqqhW3q9oV59sGeGv8lOV4AFwa1scmPaiQJAXgp9cPitOcLcp9rSRwHha4BDrIqud6wzEIEXB5Py0";
    $pk = new PassKit($_apiKey,$_apkSecret);
    $result = $pk->ListCampaigns();
    echo '<pre>';
        print_r($result);
    echo '</pre>';
  }



  public function testEpass(){
    $_apiKey = "JbgXdAipwKeNyjnpelPfFFAzfKcFQtni";
    $_apkSecret = "ehUL1IUlSJtLmmu6TzCyUjt98sjMIhCkMF6Gm3mAftcyXyAGix4GqqhW3q9oV59sGeGv8lOV4AFwa1scmPaiQJAXgp9cPitOcLcp9rSRwHha4BDrIqud6wzEIEXB5Py0";

    $pk = new PassKit($_apiKey,$_apkSecret);
    $result = $this->createTemplate($pk);
    var_dump($result);
    /*
    $mk = new Mpasskit();
    $result = $mk->getTemplate("Sompo_TA_for_B2C");
    echo '<pre>';
        print_r($result);
    echo '</pre>';
    /*
    
    $result = $pk->ListCertificates();
    echo '<pre>';
        print_r($result);
    echo '</pre>';*/


  }

  public function epassUpdateTemplate(){
    $_apiKey = "JbgXdAipwKeNyjnpelPfFFAzfKcFQtni";
    $_apkSecret = "ehUL1IUlSJtLmmu6TzCyUjt98sjMIhCkMF6Gm3mAftcyXyAGix4GqqhW3q9oV59sGeGv8lOV4AFwa1scmPaiQJAXgp9cPitOcLcp9rSRwHha4BDrIqud6wzEIEXB5Py0";
    $pk = new PassKit($_apiKey,$_apkSecret);
    /*
    $dt = new \DateTime();
    $dt->setTimezone(new \DateTimeZone('Asia/Bangkok'));
    $startDate = $dt->format('Y-m-d\TH:i:s.u\Z');

    */
    
    $data = [
        'passbook' => [
            'back' => [
                [
                    'defaultValue' => [
                        [
                            'defaultLabel' => 'Motor Policy detail',
                            'defaultValue' => '#{policy_detail}
<a href="#{policy_pdf}">Download Motor Policy PDF</a>',
                            'format' => [
                                'type' => ''
                            ]
                        ],
                        [
                            'defaultLabel' => 'Emergency contact',
                            'defaultValue' => 'In case of emergency, please call SOMPO Assist at <a href="tel:+662-118-7400">+662-118-7400</a> 24 hours, every day.',
                            'format' => [
                                'type' => ''
                            ]
                        ],
                        [
                            'defaultLabel' => 'Contact us',
                            'defaultValue' => '990 Abdulrahim Place, 12th, 14th Floor, Rama IV Road, Silom, Bangkok 10500
Tel: <a href="tel:02-119-3000">02-119-3000 (Mon - Fri 8.30 - 17.00)</a>
Email: <a href="mailto:cs@sompo.co.th">cs@sompo.co.th</a>
Website: <a href="https://www.sompo.co.th">https://www.sompo.co.th</a>
Map: <a href="https://goo.gl/maps/5V4KTW1BaqC2">Map</a>',
                            'format' => [
                                'type' => ''
                            ]
                        ],
                        [   
                            'defaultLabel' => 'How to claim?',
                            'defaultValue' => '<a href="https://www.sompo.co.th/files/upload/gallery/5b233037bedc4.pdf">Download a claim form</a> and send to our office or email us at <a href="mailto:cs@sompo.co.th">cs@sompo.co.th</a>. Please see more detials in our claim guidance <a href="https://www.sompo.co.th/files/upload/gallery/m_5bac53f05f64b.pdf">here</a>.',
                            'format' => [
                                'type' => ''
                            ]
                        ],
                        [
                            'defaultLabel' => 'Notification & Information',
                            'defaultValue' => '#{notification_text}',
                            'format' => [
                                'type' => ''
                            ]
                        ]
                    ]
                ]
            ],
            'auxiliary' => [
                [
                    'defaultLabel' => 'POLICY START DATE',
                    'defaultValue' => '#{inception_date}',
                    'textAlign' => 'left',
                    'format' => [
                        'type' => 'text'
                    ]
                ],
                [
                    'defaultLabel' => 'POLICY END DATE',
                    'defaultValue' => '#{conclusion_date}',
                    'textAlign' => 'right',
                    'format' => [
                        'type' => 'text'
                    ]
                ]
            ],
        ],
    ];
    //exit();
    $result = $pk->UpdateTemplateData('SOMPO_MO_FOR_B2C',$data);
    var_dump($result); 
   /*
    $data = array('language' => 'th');
    $update_image_list = [
        'passbook-LogoFile' => public_path('/passkit/images/logo.png'),
        'passbook-StripFile' => public_path('/passkit/images/strip.png'),
    ];
    $delete_image_list = array('passbook-FooterFile');
    // refer to `https://dev.passkit.net/#update-a-template-with-images` for a complete attribute list.
    $result = $pk->UpdateTemplateDataImages('SOMPO_MO_FOR_B2C',$data,$update_image_list,$delete_image_list);
    var_dump($result);*/
  }

  public function updateCampaign(){
    $_apiKey = "JbgXdAipwKeNyjnpelPfFFAzfKcFQtni";
    $_apkSecret = "ehUL1IUlSJtLmmu6TzCyUjt98sjMIhCkMF6Gm3mAftcyXyAGix4GqqhW3q9oV59sGeGv8lOV4AFwa1scmPaiQJAXgp9cPitOcLcp9rSRwHha4BDrIqud6wzEIEXB5Py0";
    $pk = new PassKit($_apiKey,$_apkSecret);
      $dt = new \DateTime();
      $dt->setTimezone(new \DateTimeZone('Asia/Bangkok'));
      $startDate = '2017-03-07T05:02:33.914487Z';
      //$data = array('name' => 'SOMPO_MO_B2C','passbookCertId' => 'iDIlXUDbVJJM','startDate' => $startDate);
      //$result = $pk->CreateCampaign($data);
      //return $result;

      $data = array('startDate' => $startDate);
      $result = $pk->UpdateCampaign('SOMPO_MO_B2C',$data);
  }
  

  private function createTemplate($pk){
    /* Upload Image */
    //$icon_path = "images/dynamic/becf18dd7dd9358fd861d66770f37b56a0168e49/dec30b5dda9e6052e1ee64090f59e39762402b4b.png";
    //$logo_path = "images/dynamic/becf18dd7dd9358fd861d66770f37b56a0168e49/6bca962921cdfa7a77996001699a0718b6e3d268.png";
    /*
    $img = [ 'image' => public_path('/passkit/images/icon.png') ];
    $response = $pk->UploadImage($img);
    if(!empty($response->path)){
        $icon_path = $response->path;
    }
    $img = [ 'image' => public_path('/passkit/images/logo.png') ];
    $response = $pk->UploadImage($img);
    if(!empty($response->path)){
        $logo_path = $response->path;
    }*/
    
    $image_files = [
        'passbook-IconFile' => public_path('/passkit/images/icon.png'),
        'passbook-LogoFile' => public_path('/passkit/images/logo.png')
    ];
    /*
    $dt = new \DateTime();
    $dt->setTimezone(new \DateTimeZone('Asia/Bangkok'));
    $startDate = $dt->format('Y-m-d\TH:i:s\Z');

    $data_passbook = array('type' => 'generic','desc' => 'SOMPO THAILAND');
    $data = array('name' => 'SOMPO_MO_FOR_B2C','campaignName' => 'SOMPO_MO_B2C','language' => 'th','startDate' => '2018-08-06T00:00:00Z', 'passbook' => $data_passbook);
    return $pk->CreateTemplate($data,$image_files);

    /* ------------- */

        $passbook = $this->getPassBook();

        $dt = new \DateTime();
        $dt->setTimezone(new \DateTimeZone('Asia/Bangkok'));
        $startDate = $dt->format('Y-m-d\TH:i:s\Z');
        $data = [
            'name' => 'SOMPO_MO_FOR_B2C',
            'campaignName' => "SOMPO_MO_B2C",
            "status" => "ACTIVE",
            'language' => 'th',
            'startDate' => $startDate,
            'maxIssue' => 100000000,
            'dynamicKeys' => [
                [
                    'key' => 'insurance_type',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'insurance_name',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'insurance_plan',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'inception_date',
                    'type' => 'dateTime',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'conclusion_date',
                    'type' => 'dateTime',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'policy_detail',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'policy_addon_detail',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],  
                [
                    'key' => 'policy_pdf',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'mis_pdf',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
                [
                    'key' => 'notification_text',
                    'type' => 'string',
                    'defaultValue' => null,
                    'isEditable' => false
                ],
            ],
            'dynamicImageKeys' => [
                "passbook" => [
                    "background" => [
                        "isDynamic" => false,
                        "isEditable" => false
                    ],
                    "footer" => [
                        "isDynamic" => false,
                        "isEditable" => false
                    ],
                    "icon" => [
                        "isDynamic" => false,
                        "isEditable" => false
                    ],
                    "logo" => [
                        "isDynamic" => true,
                        "isEditable" => false
                    ],
                    "strip" => [
                        "isDynamic" => true,
                        "isEditable" => true
                    ],
                    "thumbnail" => [
                        "isDynamic" => false,
                        "isEditable" => false
                    ],
                ],
                "androidPay" => [
                    "strip" => [
                        "isDynamic" => false,
                        "isEditable" => false
                    ],
                ]
            ],
            'passbook' => $passbook
        ];
    
        //echo '<pre>';
        ///    print_r($data);
        //echo '</pre>';
        
        return $pk->CreateTemplate($data,$image_files);
  }


  private function getPassBook(){
      // Generate Passbook
      return $data_passbook = [
        'type' => 'generic',
        'orgName' => "Sompo Insurance (Thailand) Public Company Limited",
        'desc' => 'SOMPO THAILAND',
        'bgColor' => '#ffffff',
        'labelColor' => '#f00000',
        'fgColor' => '#575757',
        'header' => [
            [
                'defaultLabel' => '',
                'defaultValue' => 'Motor Insurance',
                'textAlign' => 'right',
                'format' => [
                    'type' => 'text'
                ]

            ]
        ],
        'secondary' => [
            [
                'defaultLabel' => 'INSURED NAME',
                'defaultValue' => '#{insurance_name}',
                'textAlign' => 'left',
                'format' => [
                    'type' => 'text'
                ]
            ],
            [
                'defaultLabel' => 'PLAN',
                'defaultValue' => '#{insurance_plan}',
                'textAlign' => 'right',
                'format' => [
                    'type' => 'text'
                ]
            ]
        ],
        'auxiliary' => [
            [
                'defaultLabel' => 'INCEPTION DATE',
                'defaultValue' => '#{inception_date}',
                'textAlign' => 'left',
                'format' => [
                    'type' => 'text'
                ]
            ],
            [
                'defaultLabel' => 'CONCLUSION DATE',
                'defaultValue' => '#{conclusion_date}',
                'textAlign' => 'right',
                'format' => [
                    'type' => 'text'
                ]
            ]
        ],
        'back' => [
            [
                'defaultValue' => [
                    [
                        'defaultLabel' => 'Motor Policy detail',
                        'defaultValue' => '#{policy_detail}<br/><a href="#{policy_pdf}">Download Motor Policy PDF</a>',
                        'format' => [
                            'type' => ''
                        ]
                    ],
                    [
                        'defaultLabel' => 'Miscellaneous Policy detail',
                        'defaultValue' => '#{policy_addon_detail}<br/><a href="#{policy_pdf}">Download Miscellaneous Policy PDF</a>',
                        'format' => [
                            'type' => ''
                        ]
                    ],
                    [
                        'defaultLabel' => 'Emergency contact',
                        'defaultValue' => 'In case of emergency, please call SOMPO Assist at <a href="tel:+662-205-7775">+662-205-7775</a> 24 hours, every day.',
                        'format' => [
                            'type' => ''
                        ]
                    ],
                    [
                        'defaultLabel' => 'Contact us',
                        'defaultValue' => '<p>990 Abdulrahim Place, 12th, 14th Floor, Rama IV Road, Silom, Bangkok 10500</p>
                        <p>Tel: <a href="tel:02-119-3088">02-119-3088</a></p>
                        <p>Email: <a href="mailto:crm@sompo.co.th">crm@sompo.co.th</a></p>
                        <p>Website: <a href="https://www.sompo.co.th">http://www.sompo.co.th</a></p>
                        <p>Map: <a href="https://goo.gl/maps/5V4KTW1BaqC2">Map</a></p>',
                        'format' => [
                            'type' => ''
                        ]
                    ],
                    [   
                        'defaultLabel' => 'How to claim?',
                        'defaultValue' => '<a href="https://www.sompo.co.th/files/upload/gallery/5b233037bedc4.pdf">Download a claim form</a> and send to our office or email us at <a href="mailto:crm@sompo.co.th">crm@sompo.co.th</a>. Please see more detials in our claim guidance <a href="http://www.sompo.co.th/pdf/service/claim_guidance_b2c.pdf">here</a>.',
                        'format' => [
                            'type' => ''
                        ]
                    ],
                    [
                        'defaultLabel' => 'Notification & Information',
                        'defaultValue' => '#{notification_text}',
                        'format' => [
                            'type' => ''
                        ]
                    ]
                ]
            ]
        ]
    ];
  }


  public function testSendEpass(){
    $mk = new Mpasskit();
    $passkit_link = config('sompo.passkit_url');
    $passkit_code = $mk->createPass(4);
    if($passkit_code != false){
        echo $passkit_link .= $passkit_code;
    }
  }

  public function testLoadPdf(){
    $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
    $path = "/user/policy/HQAV30000003WEB01201804/5adb0e3072577.pdf";
    return response()->download($directory_path.$path);
  }

  public function testEncrypt(){
    $token = Password::getRepository()->createNewToken();
    $array = [
        'app' => 'SOMPO_MOTOR',
        'created_at' => date('Y-m-d H:i:s'),
        'expire_at' => null,
        'token' => $token
    ];
    $encrypted = encrypt(json_encode($array));
    echo $encrypted;
    echo "<br/>";
    $decrypted = $this->testDecrypt($encrypted);
    $array_decrypted = json_decode($decrypted,true);
    var_dump($array_decrypted);
  }

  private function testDecrypt($string){
    try {
        $decrypted = decrypt($string);
        return $decrypted;
    } catch (DecryptException $e) {
        false;
    }
  }

  public function testSMS(){
    $sms = new SMS();
    $epass_link = "https://get-pass.io/p/lKmo7h3e3UKQMh";
    $policy_id = 24;
    $sms->sendSMS($policy_id,$epass_link);
    
  }

  public function testBlacklist(){
    $bl = new Blacklist();
    $param = [
        'firstname' => "นิตินัย",
        'lastname' => "รุจิพรพงศ์",
        'idcard' => "3101501163047"
    ];
    $response = $bl->checkBlacklist($param);
    echo $response;
    
  }

  public function testOtp(){
      
      $otp = new OTP();
      $result = $otp->sendOTP("66971830470","th",uniqid());
      var_dump($result);

      $array = [
        "requestId"=> "B619FDD3-58ED-4A2E-BAD7-CF9E41BB2033", 
        "referenceNo"=> "64D418" ,
        "otp"=> "389609" ,
        "expiryDate"=> "2018-08-29T10:07:11.39" 
      ];

  }

  public function checkOTP(){
    $params = [
        'session_id' => 'AF1',
        'reference_no' => '64D418',
        'otp_number' => '389609'
    ];
    $result = ['status' => "",'reason' => ""];
    try{
      $record = OtpModel::where([
        'session_id' => $params['session_id'],
        'reference_no' => $params['reference_no'],
      ])->orderBy('id','desc')->first();

      if(!empty($record)){
        if($record->otp_expire>=date('Y-m-d H:i:s')){
            if($params['otp_number']==$record->otp_number){
                $result['status'] = true;
                $result['reason'] = trans('common.otp_match');
                $record->verify = 1;
                $record->save();
            }else{
                $result['status'] = false;
                $result['reason'] = trans('common.otp_notmatch');
            }
        }else{
            $result['status'] = false;
            $result['reason'] = trans('common.otp_expire');
        }
      }else{
        $result['status'] = false;
        $result['reason'] = trans('common.otp_notfound');
      }
      var_dump($result);

    }catch (\Exception $e) {
        Log::error($e->getMessage());
        $result['status'] = false;
        $result['reason'] = "System Error";
        return $result;
    }
  }

  public function getAjax(Request $request){
      $input = $request->all();
      //Log::info($input);
      return "A";
  }

  public function vehicle(Request $request){
      $vehicle = new vehicle;
      $vehicle->setTable('vehicle');
      echo "<br/>".$vehicle->where('id',1)->first()->mortor_code_av;
      $vehicle->setTable('vehicle_second');
      echo "<br/>".$vehicle->where('id',1)->first()->mortor_code_av;

      echo "<br/>".Vehicle::where('id',1)->first()->mortor_code_av;
  }


  public function testApi(){
    $barcodeApi = new Barcode();
                $response = $barcodeApi->request();
                $sticker_no = null;
                if($response!=false){
                    $sticker_no = $response;
                }
    echo $sticker_no;

  }


  public function testPackageNumber(Request $request){
    $data = Policy::where('id',3)->first();
    $endorse = new Endorse();
    echo $endorse->generatePackageNumber($data);
  }

  public function testFtp(){
    $fileContents = Storage::disk('pdf')->get('/buffer/5d09a4ad3267d.pdf');
    Storage::disk('sftp')->put('testsompo.pdf', $fileContents);

  }


  public function testLoadExcel(){
    $directory = Storage::disk('upload-chunks')->getDriver()->getAdapter()->getPathPrefix();
    //echo $directory;
    
    Excel::load($directory."/pricing.xls",function($reader){
        $excelData = $reader->get();
        foreach($excelData as $key => $value){
            echo "<br/>Key: ".$key;
            echo "<br/>Value: ".$value;
        }
    });
    
  }

  public function createFolder(){
      //File::makeDirectory('/var/www/html/motorjoyuat/storage/app/private/personal', 0777, true, true);
      Storage::makeDirectory('app/private',0777);

    //var/www/html/motorjoyuat/storage/app/private/personal/release
  }

  public function truncateTable(){
    // $arrayStatement = ['access_files','barcode_log','api_log','blacklist_log','chassis_log','cityzen_id_log',
    // 'confirm_policy','driver_info','email_log','endorse','endorse_log','epass_token','otp','otp_log',
    // 'passkit_log','payment_response_log','payments','policy','policy_edit_doc_log','promotion_register',
    // 'receipt','schedule_resend','schedule_resend_log','signature_log','sms_log','transaction_log','user_management_log',
    // 'visitor_log'
    // ];
    // foreach($arrayStatement as $value){
    //     DB::statement('TRUNCATE TABLE '.$value);
    // }
      
  }


  public function clearStorage(){
      $directory = "app/files/pdf/user/compulsory";
      Storage::deleteDirectory($directory);
  }


    public function fileStorageRemove(){
        echo "remove";
        File::deleteDirectory('/var/www/html/motorjoyuat/storage/framework/cache/data');
    }


  
}
