<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Vehicle;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use App\Models\Article;
use App\Models\Faq;
use App\Models\Banner;
use App\Models\Coverage;

use Validator;
use App\Models\VisitorLog;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{

  public function __construct(){
    //$this->middleware('auth:web');
    $this->counter();
  }

  public function index(){
    if(session()->has('insurance_data')){
        session()->forget('insurance_data');
    } 
    if(session()->has('database_active')){
        session()->forget('database_active');
    } 
    
    $init['brand_top'] = VehicleBrand::select('id','name','top_order')->whereNotNull('top_order')->orderBy('top_order','asc')->get()->toArray();
    $init['brand_order'] = VehicleBrand::select('id','name','top_order')->whereNull('top_order')->orderBy('name','asc')->get()->toArray();
    $init['brand'] = array_merge($init['brand_top'],$init['brand_order']);
    $init['whysompo'] = Article::where('category','WHYSOMPO')->get()->toArray();
    $init['ourproduct'] = Article::where('category','OURPRODUCT')->where('status',1)->get()->toArray();
    $init['province'] = Province::pluck('name_th','id')->toArray();
    $init['faqs'] = Faq::where('status',1)->get()->toArray();
    $init['banner'] = Banner::where('status',1)->whereNotNull("cover_en")->get();
    $init['is_bkk'] = [
        'Y' => 'กรุงเทพฯ และปริมณฑล',
        'N' => 'ต่างจังหวัด'
    ];
    $init['plan'] = Coverage::where('status','ACTIVE')->get()->pluck('plan','id');
    return view('web.home.home',["init"=>$init]);
  }


  public function getVehicleModel(Request $request){
    try{
        if($request->ajax()){
            // Validate 
            $input = $request->all();
            $validator = Validator::make($input, [  
                'id'=>'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $model = VehicleModel::where('brand_id',$input['id'])->pluck('name','id')->toArray();
            if(is_array($model)){
                return response()->json($model);
            }else{
                return response()->json([]);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  public function getModelYear(Request $request){
    try{
        if($request->ajax()){
            // Validate 
            $input = $request->all();
            $validator = Validator::make($input, [  
                'id'=>'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $coverage = Coverage::where('id',1)->first();
            $year = Vehicle::where('model_id',$input['id'])->orderBy('year','desc')->pluck('year')->toArray();
            if(is_array($year)){
                $yearList = array_values(array_unique($year));
                $nowYear = intval(date('Y'));
                $yearResult = [];
                foreach($yearList as $index => $value){
                    $maxYear = $nowYear;
                    $minYear = $nowYear - $coverage->max_age;
                    if($value >= $minYear && $value <= $maxYear){
                        array_push($yearResult,$value);
                    }
                }
                //Log::info($yearList);

                return response()->json($yearResult);
            }else{
                return response()->json([]);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  public function getModelType(Request $request){
    try{
        if($request->ajax()){
            // Validate 
            $input = $request->all();
            $validator = Validator::make($input, [  
                'year'=>'required',
                'brand_id'=>'required',
                'model_id'=>'required'
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $model_type = Vehicle::where('year',$input['year'])->where('brand_id',$input['brand_id'])->where('model_id',$input['model_id'])->pluck('model_type','id')->toArray();
            if(is_array($model_type)){
                return response()->json(array_unique($model_type));
            }else{
                return response()->json([]);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>$e->getMessage()));
    }
  }

  private function counter(){
      $data = VisitorLog::whereDate('created_at','=',date('Y-m-d'))->first();
      if(!empty($data)){
          $data->counter = ++$data->counter;
          $data->save();
      }else{
          $data = [
              'counter' => 1
          ];
          VisitorLog::create($data);
      }
  }


}
