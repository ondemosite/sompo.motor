<?php

namespace App\Http\Controllers\web;

use App\Repositories\GarageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Garage;
use App\Models\Province;
use App\Models\District;
use Validator;
use Illuminate\Support\Facades\Log;

class GarageController extends Controller
{
  public function __construct(){
    //$this->middleware('auth:web');
  }

  public function index(Request $request){
    $input = $request->all();
    $init['selected_province'] = !empty($input['input_garage_prov'])?$input['input_garage_prov']:null;
    $init['selected_district'] = !empty($input['input_garage_district'])?$input['input_garage_district']:null;
    $init['selected_name'] = !empty($input['selected_name'])?$input['selected_name']:null;
    $init['input_garage_name'] = !empty($input['input_garage_name'])?$input['input_garage_name']:null;
    $init['province'] = Province::pluck('name_th','id')->toArray();
    return view('web.garage.garage',["init"=>$init]);
  }

  public function findGarage(Request $request){
    try{
      if($request->ajax()){
          $input = $request->all();
          $garage_repo = new GarageRepository();
          $data = $garage_repo->get_garage_locations($input);
          if(is_array($data)){
            return json_encode($data);
          }else{
            return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
          }
      }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>"Command failed please try again or contact administrator!! "]);
    }
  }

  public function getDistrict(Request $request){
    try{
        if($request->ajax()){
            $input = $request->all();
            // Validate 
            $validator = Validator::make($input, [
                'province_id' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $response = District::where('province_id',$input['province_id'])->orderBy('id','asc')->pluck('name_th','id')->toArray();
            if(!empty($response)){
                return json_encode($response);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }

  public function getDataGarage(Request $request){
    try{
        if($request->ajax()){
            $input = $request->all();
            $validator = Validator::make($input, [
                'search_text' => 'required',
            ]);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }
            $data = Garage::whereRaw('name like \'%' . $input['search_text'] . '%\'')->get()->toArray();
            if(!empty($data)){
                return json_encode($data);
            }else{
              return json_encode([]);
            }
        }
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(["resp_error"=>$e->getMessage()]);
    }
  }


}
