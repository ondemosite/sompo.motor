<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Library\insuranceManager;
use App\Library\insurancePayment;
use App\Library\OTP;
use App\Library\Blacklist;
use App\Library\CityzenValidation;
use App\Library\ChassisValidation;
use Validator;
use Illuminate\Support\Facades\Log;
use App\Models\Province;
use App\Models\District;
use App\Models\SubDistrict;
use App\Models\Order;
use App\Models\Policy;
use App\Models\ConfirmPolicy;
use App\Models\Payment;
use App\Models\Coverage;
use App\Models\DriverInfo;
use App\Models\TransactionLog;
use App\Models\Prefix;
use App\Repositories\InsuranceRepository;
use DateTime;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Config;
use App;

class InsuranceController extends Controller
{
    public function __construct(InsuranceRepository $insurance){
        //$this->middleware('auth:web');
        $this->insurance = $insurance;
        $this->insurance_manager = new insuranceManager();
        $this->insurance_payment = new insurancePayment();    
    }
    
    public function formInput(Request $request){
        try{
            $input = $request->all();
            //Log::info($input);
            // Validate 
            $filter_validate = [
                'input_brand'=>'required',
                'input_model'=>'required',
                'input_model_year'=>'required',
                'input_sub_model'=>'required',
                'input_is_bkk' => 'required',
                'input_plan' => 'required'
            ];

            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                return redirect()->route('web.dashboard')->withErrors($validator)->withInput();
            }

            $coverage = Coverage::where([
                'id' => $input['input_plan'],
                'status' => 'ACTIVE',
            ])->first();

            if(empty($coverage)){
                return redirect()->route('web.dashboard')->withInput()->withErrors(['Get some error plese try again']);
            }else{
                $maxAge = $coverage->max_age;
                //$minAge = $coverage->min_age - 1;
                $nowYear = date('Y');
                if($nowYear - $input['input_model_year'] > $maxAge){
                    return redirect()->route('web.dashboard')->withInput()->withErrors(['ซมโปะไม่รับประกันรถที่มีอายุเกิน '.$coverage->max_age.' ปี']);
                }
                // if($input['input_model_year'] > $nowYear - $minAge){
                //     return redirect()->route('web.dashboard')->withInput()->withErrors(['ซมโปะไม่รับประกันรถที่มีอายุน้อยกว่า '.$coverage->min_age.' ปี']);
                // }
            }
            
            if($this->insurance_manager->setUserInput($request)){
                return redirect()->route('web.insurance_step2');
            }else{
                if ($request->session()->has('insurance_data')) {
                    $request->session()->forget('insurance_data');
                    $request->session()->flush();
                }
                return redirect()->route('web.dashboard')->withInput()->withErrors(['Get some error plese try again']);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('web.dashboard')->withInput()->withErrors(['Error Please contact administrator']);
        }
        
    }

    public function initialStep2(Request $request){
        if ($request->isMethod('post')){ //Submit
            $input = $request->all();
            $filter_validate = [
                'input_ft_si' => 'required',
            ];


            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                return back()->withErrors(['Please select plan!'])->withInput();
            }
            if($this->insurance_manager->setSelectPlan($input['input_ft_si'])){
                return redirect()->route('web.insurance_step3');
            }
            return redirect()->route('web.dashboard')->withErrors("Get some error plese try again");

        }else{
            $init = $this->insurance_manager->getInitialStep2();
            if($init!=false){
                return view('web.insurance.step2',["init"=>$init]);
            }else{
                $init['notfound'] = true;
                return view('web.insurance.step2',["init"=>$init]);
            }
        }
    }

    public function initialStep3(Request $request){
        if ($request->isMethod('post')){ //Submit
            return redirect()->route('web.insurance_step4');
        }else{
            $init = $this->insurance_manager->getInitialStep3();
            if($init!=false){
                return view('web.insurance.step3',["init"=>$init]);
            }else{
                return redirect()->route('web.dashboard')->withErrors("Page Not Found");
            }
        }
    }

    public function initialStep4(Request $request){
        try{
            if ($request->isMethod('post')){ //Submit
                $input = $request->all();
                $filter_validate = [
                    'input_info_title' => 'required',
                    'input_info_name'=>'required',
                    'input_info_lastname'=>'required',
                    'input_info_id'=>'required',
                    'input_info_gender'=>'required',
                    'input_info_birth'=>'required',
                    'input_info_address'=>'required',
                    'input_info_province'=>'required',
                    'input_info_district'=>'required',
                    'input_info_subdistrict'=>'required',
                    'input_info_code'=>'required',
                    'input_info_tel'=>'required|max:10',
                    'input_info_email'=>'required|email',
                    'input_info_prefix'=>'required|max:3',
                    'input_info_licence'=>'required|max:4',
                    'input_info_car_province'=>'required',
                    'input_info_car_number'=> [
                        'max:20',
                        Rule::unique('policy','car_chassis_number')->where(function ($query) {
                            $query->where('insurance_expire','>=',date('Y-m-d H:i:s'));
                            $query->where('status','!=',"CANCEL");
                            $query->where('status','!=',"EXPIRE");
                        })
                    ],
                    'input_doc_id_val'=>'nullable|mimes:jpeg,bmp,png,pdf|max:2000'
                ];

                /* Custom Message */
                $messages = [
                    'input_doc_id_val.max'    => trans('step4.doc_id_card')." ".trans('validation.document_size'),
                    'input_doc_id_val.mimes'  => trans('step4.doc_id_card')." ".trans('validation.document_mimes'),
                    'input_doc_car_licence_val.max'    => trans('step4.doc_car_licence')." ".trans('validation.document_size'),
                    'input_doc_car_licence_val.mimes'  => trans('step4.doc_car_licence')." ".trans('validation.document_mimes'),
                    'input_doc_licence1_val.max'    => trans('step4.doc_driver1_licence')." ".trans('validation.document_size'),
                    'input_doc_licence1_val.mimes'  => trans('step4.doc_driver1_licence')." ".trans('validation.document_mimes'),
                    'input_doc_licence2_val.max'    => trans('step4.doc_driver2_licence')." ".trans('validation.document_size'),
                    'input_doc_licence2_val.mimes'  => trans('step4.doc_driver2_licence')." ".trans('validation.document_mimes'),
                    'input_doc_cctv_outside_val.max'    => trans('step4.cctv_photo_outside')." ".trans('validation.document_size'),
                    'input_doc_cctv_outside_val.mimes'  => trans('step4.cctv_photo_outside')." ".trans('validation.document_mimes'),
                    'input_doc_cctv_inside_val.max'    => trans('step4.cctv_photo_inside')." ".trans('validation.document_size'),
                    'input_doc_cctv_inside_val.mimes'  => trans('step4.cctv_photo_inside')." ".trans('validation.document_mimes'),
                ];

                $validator = Validator::make($input,$filter_validate,$messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }

                /* Save Image */
                if(!empty($input['input_doc_id_val'])){
                    $path_personal_id = Storage::disk('personal-release')->putFile('',$input['input_doc_id_val']);
                }else{
                    $path_personal_id = $input['input_doc_id_check'];
                }

                /* ----------Store Value----------*/
                $province = Province::where('id',$input['input_info_province'])->first();
                $district = District::where([
                    'province_id' => $input['input_info_province'],
                    'id' => $input['input_info_district']
                ])->first();
                $sudistrict = SubDistrict::where([
                    'province_id' => $input['input_info_province'],
                    'district_id' => $input['input_info_district'],
                    'subdistrict_id' => $input['input_info_subdistrict']
                ])->first();

                $is_post = !empty($input['is_post'])?"YES":"NO";
                if($input['is_post_disable']=="TRUE"){
                    $is_post = "YES";
                }

                $store_data = [
                    'info_main_prefix' => $input['input_info_title'],
                    'info_main_name' => preg_replace('/\s+/', '',$input['input_info_name']),
                    'info_main_lastname' => preg_replace('/\s+/', '', $input['input_info_lastname']),
                    'info_main_id_card' => $input['input_info_id'],
                    'info_main_gender' => $input['input_info_gender'],
                    'info_main_birthdate' => $input['input_info_birth'],
                    'info_main_birthdate_value' => correctDate($input['input_info_birth']),
                    'info_main_address' => $input['input_info_address'],
                    'info_main_province' => $input['input_info_province'],
                    'info_main_district' => $input['input_info_district'],
                    'info_main_subdistrict' => $input['input_info_subdistrict'],
                    'info_main_province_name' => $province->name,
                    'info_main_district_name' => $district->name,
                    'info_main_subdistrict_name' => $sudistrict->name,
                    'info_main_province_name_th' => $province->name_th,
                    'info_main_district_name_th' => $district->name_th,
                    'info_main_subdistrict_name_th' => $sudistrict->name_th,
                    'info_main_postcode' => $input['input_info_code'],
                    'info_main_tel' => $input['input_info_tel'],
                    'info_main_email' => $input['input_info_email'],
                    // 'info_driver1_name' => !empty($input['input_driver1_name'])?preg_replace('/\s+/', '', $input['input_driver1_name']):null,
                    // 'info_driver1_lastname' => !empty($input['input_driver1_lastname'])?preg_replace('/\s+/', '', $input['input_driver1_lastname']):null,
                    // 'info_driver1_id_card' => !empty($input['input_driver1_id'])?$input['input_driver1_id']:null,
                    // 'info_driver1_licence' => !empty($input['input_driver1_licence'])?$input['input_driver1_licence']:null,
                    // 'info_driver1_gender' => !empty($input['input_driver1_gender'])?$input['input_driver1_gender']:null,
                    // 'info_driver1_birthdate' => !empty($input['input_driver1_birth'])?$input['input_driver1_birth']:null,
                    // 'info_driver1_birthdate_value' => !empty($input['input_driver1_birth'])?correctDate($input['input_driver1_birth']):null,
                    // 'info_driver2_name' => !empty($input['input_driver2_name'])?preg_replace('/\s+/', '', $input['input_driver2_name']):null,
                    // 'info_driver2_lastname' => !empty($input['input_driver2_lastname'])?preg_replace('/\s+/', '', $input['input_driver2_lastname']):null,
                    // 'info_driver2_id_card' => !empty($input['input_driver2_id'])?$input['input_driver2_id']:null,
                    // 'info_driver2_licence' => !empty($input['input_driver2_licence'])?$input['input_driver2_licence']:null,
                    // 'info_driver2_gender' => !empty($input['input_driver2_gender'])?$input['input_driver2_gender']:null,
                    // 'info_driver2_birthdate' => !empty($input['input_driver2_birth'])?$input['input_driver2_birth']:null,
                    // 'info_driver2_birthdate_value' => !empty($input['input_driver2_birth'])?correctDate($input['input_driver2_birth']):null,
                    'info_car_licence' => $input['input_info_prefix']." ".$input['input_info_licence'],
                    'info_car_province_id' => str_pad(Province::where('id',$input['input_info_car_province'])->first()->id,2,"0",STR_PAD_LEFT),
                    'info_car_province' => App::isLocale('th')?(Province::where('id',$input['input_info_car_province'])->first()->name_th):(Province::where('id',$input['input_info_car_province'])->first()->name),
                    'info_car_chassis' => !empty($input['input_info_car_number'])?$input['input_info_car_number']:null,
                    'info_is_advice' => !empty($input['is_advice'])?"YES":"NO",
                    'info_is_post' => $is_post,
                    'info_is_personal' => !empty($input['is_personal'])?"YES":"NO",
                    'info_is_email' => !empty($input['is_email'])?"YES":"NO",
                    'info_is_disclosure' => !empty($input['is_disclosure'])?"YES":"NO",
                    'info_is_true' => !empty($input['is_true'])?"YES":"NO",
                    'info_is_agree' => !empty($input['is_agree'])?"YES":"NO",
                    'info_path_personal_id' => $path_personal_id,
                    // 'info_path_car_licence' => $path_car_licence,
                    // 'info_path_driver1_licence' => $path_driver1_licence,
                    // 'info_path_driver2_licence' => $path_driver2_licence,
                    // 'info_path_cctv_inside' => $path_cctv_inside,
                    // 'info_path_cctv_outside' => $path_cctv_outside,
                    'info_is_main_driver' => !empty($input['input_is_main_driver'])?$input['input_is_main_driver']:0,
                ];

                //Log::info($store_data);

                if($this->insurance_manager->setInsuranceInfomation($store_data)){
                    $sessions = session('insurance_data');
                    return redirect()->route('web.insurance_step5');
                }else{
                    return redirect()->back()->withErrors('Some error occurred. Please try again')->withInput();
                }
                return redirect()->back()->withInput();
                
            }else{
                $init = $this->insurance_manager->getInitialStep4();
                if($init!=false){
                    return view('web.insurance.step4',["init"=>$init]);
                }else{
                    return redirect()->route('web.dashboard')->withErrors("Page Not Found");
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('web.dashboard')->withInput()->withErrors(['Error please try again or contact administrator']);
        }
    }

    public function initialStep5(Request $request){
        try{
            if ($request->isMethod('post')){ //Submit
                if($id = $this->insurance_manager->createOrder()){
                    $order = Order::where('id',$id)->first();
                    if(!empty($order)){
                        $this->insurance_manager->clearData();
                        //Payment Process
                        $init = $this->insurance_payment->create($order);
                        if($init!=false){
                            $paymentId = $this->insurance_manager->createPayment($init,$order);
                            if($paymentId){
                                return view('web.payment.form',['init'=>$init]);
                            }
                        }else{
                            return redirect()->route('web.dashboard')->withInput()->withErrors(['Error please try again or contact administrator']);
                        }
                    }
                    //ทำรายการล้มเหลว
                }else{
                    return redirect()->route('web.dashboard')->withErrors("Create Order Failed, Please try again.");
                }
            }else{
                $init = $this->insurance_manager->getInitialStep5();
                if($init!=false){
                    return view('web.insurance.step5',["init"=>$init]);
                }else{
                    return redirect()->route('web.dashboard')->withErrors("Invalid Data Please try again");
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('web.dashboard')->withInput()->withErrors(['Error please try again or contact administrator']);
        }
    }

    public function testPayment(){
        $order_id = "5";
        $order = Order::where('id',$order_id)->first();
        $init = $this->insurance_payment->create($order);
        if($init!=false){
            return view('web.payment.form',['init'=>$init]);
        }else{
            return redirect()->route('web.dashboard')->withInput()->withErrors(['Error please try again or contact administrator']);
        }
    }

    public function paymentFail(){
        $code = "001";
        $reason = Config::get('payment_code.fail_code');
        return view('web.payment.fail',['reason'=>$reason[$code]]);
    }

    public function done(Request $request){
        if(session('done_order')){
            $done_order = session('done_order');
            $init = $this->insurance_manager->getInitialThank($done_order);
            session()->forget('done_order');
            return view('web.payment.done',['init'=>$init]);
        }else{
            return redirect()->route('web.dashboard');
        }
    }

    public function donetest(){
        $done_order = "ODA2-2019-02-0000012";
        $init = $this->insurance_manager->getInitialThank($done_order);
        return view('web.payment.done',['init'=>$init]);
    }

    public function fail(){
        try{
            if(session()->has('fail_code')){
                $code = session('fail_code');
                $reason = Config::get('payment_code.fail_code');
                session()->forget('fail_code');
                return view('web.payment.fail',['reason'=>$reason[$code]]);
            }else{
                return redirect()->route('web.dashboard')->withInput()->withErrors(['Error please contact administrator']);
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /************* API ****************/
    public function getDistrict(Request $request){
        try{
            //if($request->ajax()){
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'province_id' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                $name = "name_th";
                if(!empty($input['locale'])){
                    if($input['locale']=="en"){
                        $name = 'name';
                    }
                }
                $response = District::where('province_id',$input['province_id'])->orderBy($name,'asc')->pluck('id',$name)->toArray();
                if(!empty($response)){
                    return json_encode($response);
                }
            //}
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }
    
    public function getSubDistrict(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'district_id' => 'required',
                    'province_id' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                $name = "name_th";
                if(!empty($input['locale'])){
                    if($input['locale']=="en"){
                        $name = 'name';
                    }
                }
                $response = SubDistrict::where(['district_id'=>$input['district_id'],'province_id'=>$input['province_id']])->orderBy($name,'asc')->pluck('subdistrict_id',$name)->toArray();
                if(!empty($response)){
                    return json_encode($response);
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getPostalcode(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                // Validate 
                $validator = Validator::make($input, [
                    'district_id' => 'required',
                    'province_id' => 'required',
                    'subdistrict_id' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                $response = SubDistrict::where(['district_id'=>$input['district_id'],'province_id'=>$input['province_id'],'subdistrict_id'=>$input['subdistrict_id']])->first();
                if(!empty($response)){
                    return $response->postalcode;
                }
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(["resp_error"=>$e->getMessage()]);
        }
    }

    public function getTaxiGlossPremium(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                $validator = Validator::make($input, [
                    'si' => 'required',
                    'define_name' => 'required',
                    'cctv' => 'required'
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                return $this->insurance_manager->getTaxiGlossPremiumAPI($input);
            }
            return json_encode(array("resp_error"=>""));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }
    public function getTheftGlossPremium(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                $validator = Validator::make($input, [
                    'si' => 'required',
                    'define_name' => 'required',
                    'cctv' => 'required'
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                return $this->insurance_manager->getTheftGlossPremiumAPI($input);
            }
            return json_encode(array("resp_error"=>""));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }

    public function getCarlossGlossPremium(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                $validator = Validator::make($input, [
                    'ft_si' => 'required',
                    'define_name' => 'required',
                    'cctv' => 'required'
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                return $this->insurance_manager->getCarlossGlossPremiumAPI($input);
            }
            return json_encode(array("resp_error"=>""));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }

    public function getHbGlossPremium(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                $validator = Validator::make($input, [
                    'si' => 'required',
                    'define_name' => 'required',
                    'cctv' => 'required'
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                return $this->insurance_manager->getHbGlossPremiumAPI($input);
            }
            return json_encode(array("resp_error"=>""));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }

    public function getFloodGlossPremium(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                $validator = Validator::make($input, [
                    'ft_si' => 'required',
                    'define_name' => 'required',
                    'cctv' => 'required',
                    'garage' => 'required',
                    'deduct' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                
                return $this->insurance_manager->getFloodGlossPremiumAPI($input);
            }
            return json_encode(array("resp_error"=>""));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }

    public function checkPromotionCode(Request $request){
        try{
            if($request->ajax()){
                $input = $request->all();
                if(!empty($input['code']) && !empty($input['coverage'])){
                    $checkResult = $this->insurance_manager->checkPromotionCode($input);
                    return json_encode($checkResult);
                }
            }
            
            return json_encode([
                'status' => true
            ]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }
    public function calculateModified(Request $request){
        try{
            if($request->ajax()){

                $input = $request->all();
                
                $filter_validate = [
                    'input_start_insurance'=>'required',
                    'input_cctv'=>'required',
                    'input_garage'=>'required',
                    'input_prb'=>'required',
                ];
    
                $validator = Validator::make($input,$filter_validate);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }
                
                if(!empty($input['input_start_insurance'])){
                    $input['input_start_insurance'] = correctDate($input['input_start_insurance']);
                }
                if(!empty($input['input_start_compulsory'])){
                    $input['input_start_compulsory'] = correctDate($input['input_start_compulsory']);
                }
                if(!empty($input['input_end_insurance'])){
                    $input['input_end_insurance'] = correctDate($input['input_end_insurance']);
                }
                if(!empty($input['input_end_compulsory'])){
                    $input['input_end_compulsory'] = correctDate($input['input_end_compulsory']);
                }
                if($data = $this->insurance_manager->calculateModified($input)){
                    return json_encode($data);
                }else{
                    return json_encode(["resp_error"=>"Get Some Error please try again"]);
                }

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }

    public function requestOTP(Request $request){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            $input = $request->all();
            if(!empty($sessions['session_unique_id'])){

                $otp = new OTP();
                $receiver = $input['input_tel'];  
                $result = $otp->sendOTP($receiver,App::getLocale(),$sessions['session_unique_id']);
                //$result['status'] = true;
                if($result['status']==true){
                    $sessions['otp_round'] = $sessions['otp_round'] + 1;
                    session()->forget('insurance_data');
                    session(['insurance_data' => $sessions]);
                }
                return $result;
           
            }
        }
        return json_encode([
            'status' => false,
            'reason' => "Request Failed."
        ]);
    }


    public function submitOTP(Request $request){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            $input = $request->all();
            if(!empty($sessions['session_unique_id'])){
                $data = [
                    'session_id' => $sessions['session_unique_id'],
                    'reference_no' => !empty($input['input_ref'])?$input['input_ref']:null,
                    'otp_number' => $input['input_otp']
                ];
                $otp = new OTP();
                $result = $otp->checkOTP($data);
                //$result['status'] = false;
                //$result['reason'] = "รหัสไม่ถูกต้อง";
                if($result['status']){
                    $sessions['otp_verify'] = 1;
                    session()->forget('insurance_data');
                    session(['insurance_data' => $sessions]);
                }
                return json_encode($result);
            }
        }
        return json_encode([
            'status' => false,
            'reason' => "Session Error"
        ]);
    }

    public function checkOTP(Request $request){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            if(!empty($sessions['session_unique_id'])){
                if($sessions['otp_verify']==0){
                    return json_encode([
                        'status' => true,
                        'reason' => "Does not verify"
                    ]);
                }else if($sessions['otp_verify']==1){
                    return json_encode([
                        'status' => false,
                        'reason' => "Has been verify"
                    ]);
                }
            }
        }
        return json_encode([
            'status' => false,
            'reason' => "Session Error"
        ]);
    }

    public function roundOTP(Request $request){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            if(!empty($sessions['session_unique_id'])){
                return json_encode([
                    'status' => true,
                    'round' => $sessions['otp_round']
                ]);
            }
        }
        return json_encode([
            'status' => false,
            'reason' => "Session Error"
        ]);
    }


    public function confirmPolicy($id = null){
        $result = [
            'status' => 'FAIL',
            'reason' => '',
            'data' => []
        ];
        if(!empty($id)){
            $record = ConfirmPolicy::where('confirm_code',$id)->first();
            if(!empty($record)){
                
                $startDate = $record->created_at->addDays(7);
                $endDate = date('Y-m-d H:i:s');
                if($record->status==0){
                    if($endDate < $startDate){
                        $policy = $record->policy()->first(); 
                        $endorse = $policy->endorse()->first();
                        $owner = $endorse->owner()->first();
                        $result['status'] = 'SUCCESS';
                        $result['data']['code'] = $record->confirm_code;
                        $result['data']['name'] = $owner->name." ".$owner->lastname;
                    }else{
                        $result['status'] = 'FAIL';
                        $result['reason'] = trans('confirm_policy.expired');
                    }
                }else{
                    $result['status'] = 'FAIL';
                    $result['reason'] = trans('confirm_policy.confirmed');
                }
            }else{
                $result['status'] = 'FAIL';
                $result['reason'] = trans('confirm_policy.notfound');
            }
        }else{
            $result['status'] = 'FAIL';
            $result['reason'] = trans('confirm_policy.incorect');
        }
        return view('web.insurance.confirm_policy',["init"=>$result]);

    }
    
    public function submitConfirmPolicy(Request $request){
        $input = $request->all();
        if(!empty($input['code'])){
            $record = ConfirmPolicy::where('confirm_code',$input['code'])->first();
            if(!empty($record)){
                $record->status = 1;
                $record->confirm_date = date('Y-m-d H:i:s');
                $record->save();
                return redirect()->back()->withSuccess(trans('confirm_policy.success'));
            }else{
                return back()->withErrors(['Failed to confirm policy please contact customer service'])->withInput();
            }
        }
        return back()->withErrors(['Failed to confirm policy please contact customer service'])->withInput();
    }

    public function checkBlacklist(Request $request){
        try{
            if($request->ajax()){

                $input = $request->all();
                // return json_encode([
                //     'status' => 'TSTUA',
                //     'message' => trans('common.blacklist')
                // ]);
                $filter_validate = [
                    'firstname'=>'required',
                    'lastname'=>'required'
                ];
                
                foreach($input['input'] as $record){
                    $validateInput = [
                        'firstname'=> $record['firstname'],
                        'lastname'=> $record['lastname']
                    ];
                    $validator = Validator::make($validateInput,$filter_validate);
                    if ($validator->fails()) {
                        $validate_first = $validator->errors()->all();
                        return json_encode(["resp_error"=>$validate_first[0]]);
                    }
                }
                
                
                $bl = new Blacklist();
                $resultStatus = true;
                $message = null;
                foreach($input['input'] as $record){
                    $param = [
                        'firstname' => $record['firstname'],
                        'lastname' => $record['lastname'],
                        'idcard' => !empty($record['idcard'])?$record['idcard']:''
                    ];
                    $response = $bl->checkBlacklist($param);
                    if(!$response){
                        $resultStatus = false;
                        break;
                    }
                    $resultStatus = $response['is_pass'];
                    $message = $response['message'];
                    
                }

                return json_encode([
                    'status' => $resultStatus,
                    'message' => $message
                ]);

            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
        
        
    }

    public function checkMaxPreOrder(Request $request){
        try{
            $result = [
                'status' => 'FAIL',
                'message' => 'ขออภัยค่ะ ขณะนี้ไม่สามารถดำเนินการส่งคำสั่งซื้อประกันรถยนต์ของท่านได้ กรุณาติดต่อ Call Center 02-119-3088 (โดยแจ้งรหัส <b>MONL90</b>) เพื่อทำการตรวจสอบรายการ'
            ];
            $input = $request->all();
            if(session()->has('insurance_data')){
                $sessions = session('insurance_data');
                $inputData = $sessions['input_data'];
                $coverage = Coverage::where('id',$inputData['input_plan'])->first();
                if(!empty($coverage)){
                    $maxPre = $coverage->max_pre;
                    $nowDate = date('Y-m-d');
                    $startDate = correctDate($input['start_date']);
                    $dateNow = new DateTime($nowDate);
                    $dateInput = new DateTime($startDate);
                    $diff = date_diff($dateNow,$dateInput);
                    $diffDays = $diff->format("%a");
                    if($diffDays <= $maxPre){
                        $result['status'] = "SUCCESS";
                    }
                }
            }
            return json_encode($result);


        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }  
    }

    public function checkDuplicate(Request $request){
        try{
            $input = $request->all();
            $filter_validate = [
                'id_card'=>'required',
            ];

            $validator = Validator::make($input,$filter_validate);
            if ($validator->fails()) {
                $validate_first = $validator->errors()->all();
                return json_encode(["resp_error"=>$validate_first[0]]);
            }

            $result = [
                'status' => 'SUCCESS'
            ];

            $driverInfo = DriverInfo::where('idcard',$input['id_card'])->orderBy('id','desc')->first();
            if(!empty($driverInfo)){
                $policy = Policy::where('owner',$driverInfo->id)->orderBy('id','desc')->first();
                if(!empty($policy)){
                    $dateNow = strtotime(date('Y-m-d H:i:s'));
                    $dateCheck = strtotime($policy->insurance_expire);
                    if($dateNow <= $dateCheck){
                        $result['status'] = "FAIL";
                        $result['message'] = "ขออภัยค่ะ ขณะนี้ไม่สามารถดำเนินการส่งคำสั่งซื้อประกันรถยนต์ของท่านได้ กรุณาติดต่อ Call Center 02-119-3088 (โดยแจ้งรหัส <b>MONL-AP</b>) เพื่อทำการตรวจสอบรายการ";
                    }
                }
            }

            return json_encode($result);


        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }  
    }

    public function validateUniqeChassisNumber(Request $request){
        try{
            $result = [
                'status' => 'FAIL',
                'message' => '”ขออภัยค่ะ ขณะนี้ไม่สามารถดำเนินการส่งคำสั่งซื้อประกันรถยนต์ของท่านได้ กรุณาติดต่อ Call Center 02-119-3088 (โดยแจ้งรหัส <b>MONL-AP</b>) เพื่อทำการตรวจสอบรายการ”'
            ];
            $input = $request->all();
            if(!empty($input['input_info_car_number'])){
                $check = Policy::where('car_chassis_number',$input['input_info_car_number'])->
                where('status','!=',"CANCEL")->
                where('status','!=',"EXPIRE")->
                where('insurance_expire', '>=', date('Y-m-d H:i:s'))->get()->count();
                if($check==0){
                   $result['status'] = "SUCCESS";
                }
            }
            return json_encode($result);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return "false";
        }

    }

    public function checkWebService(Request $request){
        try{
            if(session()->has('insurance_data')){
                $sessions = session('insurance_data');
                $inputData = $sessions['input_data'];
                $input = $request->all();

                $validator = Validator::make($input, [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'idcard' => 'required',
                    'chassis' => 'required',
                ]);
                if ($validator->fails()) {
                    $validate_first = $validator->errors()->all();
                    return json_encode(["resp_error"=>$validate_first[0]]);
                }

                //Check AMLO
                $blacklist = new Blacklist();
                $blacklistParams = [
                    'firstname' => $input['firstname'],
                    'lastname' => $input['lastname'],
                    'idcard' => !empty($input['idcard'])?$input['idcard']:''
                ];
                $blacklistResponse = $blacklist->checkBlacklist($blacklistParams);
                if(!$blacklistResponse['is_pass']){
                    return json_encode([
                        'status' => $blacklistResponse['is_pass'],
                        'message' => $blacklistResponse['message']
                    ]);
                }

                //Check CityZen
                $cityZen = new CityzenValidation();
                $cityzenResponse = $cityZen->validate(['idcard' => $input['idcard']]);
                if(!$cityzenResponse['is_pass']){
                    return json_encode([
                        'status' => $cityzenResponse['is_pass'],
                        'message' => $cityzenResponse['message']
                    ]);
                }

                //Check Chassis AV
                $chassis = new ChassisValidation();
                $chassisParams = [
                    'type' => 'AV',
                    'number' => $input['chassis'],
                    'effective' => $inputData['start_insurance'],
                    'expiration' => $inputData['end_insurance'],
                ];
                $chassisResponse = $chassis->validate($chassisParams);
                if(!$chassisResponse['is_pass']){
                    return json_encode([
                        'status' => $chassisResponse['is_pass'],
                        'message' => $chassisResponse['message']
                    ]);
                }

                if($inputData['is_com']!=0 && !empty($inputData['is_com'])){
                    //Check Chassis AC
                    $chassisParams = [
                        'type' => 'AC',
                        'number' => $input['chassis'],
                        'effective' => $inputData['start_compulsory'],
                        'expiration' => $inputData['end_compulsory'],
                    ];
                    $chassisResponse = $chassis->validate($chassisParams);
                    if(!$chassisResponse['is_pass']){
                        return json_encode([
                            'status' => $chassisResponse['is_pass'],
                            'message' => $chassisResponse['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => true
                ]);
            }

            return json_encode(array("resp_error"=> 'Session Unavailable'));

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }

    }

    public function getPrefix(Request $request){
        try{
            $input = $request->all();
            $prefix = [];
            if(!empty($input['gender'])){
                $prefix = Prefix::whereIn('gender',[$input['gender'],'NONE'])->get()->pluck('id','name')->toArray();
            }else{
                $prefix = Prefix::get()->pluck('id','name')->toArray();
            }
            return json_encode($prefix);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }

    public function getGender(Request $request){
        try{
            $input = $request->all();
            $prefix = Prefix::where('id',$input['id'])->first();
            if(!empty($prefix)){
                return json_encode($prefix->toArray());
            }
            return json_encode([]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }

    public function getTransactionLog($orderNo){
        $transaction = TransactionLog::where('order_no',$orderNo)->get();
        foreach($transaction as $record){
            $color = $record->type == "SUCCESS"?"green":"red";
            echo "<span style=\"color: ".$color.";width: 100px;display:inline-block;\">".$record->type."</span>: ".$record->descriptions." ---- created_at:".$record->created_at;
            echo "<br/>";
        }
    }

    public function saveInformation(Request $request){
        try{
            $input = $request->all();
            /* Save Image */
            if(!empty($input['input_doc_id_val'])){
                $path_personal_id = Storage::disk('personal-release')->putFile('',$input['input_doc_id_val']);
            }else{
                $path_personal_id = !empty($input['input_doc_id_check'])?$input['input_doc_id_check']:'';
            }

            /* ----------Store Value----------*/
            if(!empty($input['input_info_province'])){
                $province = Province::where('id',$input['input_info_province'])->first();
            }
            if(!empty($input['input_info_district'])){
                $district = District::where([
                    'province_id' => $input['input_info_province'],
                    'id' => $input['input_info_district']
                ])->first();
            }
            if(!empty($input['input_info_subdistrict'])){
                $sudistrict = SubDistrict::where([
                    'province_id' => $input['input_info_province'],
                    'district_id' => $input['input_info_district'],
                    'subdistrict_id' => $input['input_info_subdistrict']
                ])->first();
            }
            

            $store_data = [];
            if(!empty($input['input_info_title'])){
                $store_data['info_main_prefix'] = $input['input_info_title'];
            }
            if(!empty($input['input_info_name'])){
                $store_data['info_main_name'] = preg_replace('/\s+/', '',$input['input_info_name']);
            }
            if(!empty($input['input_info_lastname'])){
                $store_data['info_main_lastname'] = $input['input_info_lastname'];
            }
            if(!empty($input['input_info_id'])){
                $store_data['info_main_id_card'] = $input['input_info_id'];
            }
            if(!empty($input['input_info_gender'])){
                $store_data['info_main_gender'] = $input['input_info_gender'];
            }
            if(!empty($input['input_info_birth'])){
                $store_data['info_main_birthdate'] = $input['input_info_birth'];
            }
            if(!empty($input['input_info_birth'])){
                $store_data['info_main_birthdate_value'] = $input['input_info_birth'];
            }
            if(!empty($input['input_info_address'])){
                $store_data['info_main_address'] = $input['input_info_address'];
            }
            if(!empty($input['input_info_province'])){
                $store_data['info_main_province'] = $input['input_info_province'];
                $store_data['info_main_province_name'] = $province->name;
                $store_data['info_main_province_name_th'] = $province->name_th;
            }
            if(!empty($input['input_info_district'])){
                $store_data['info_main_district'] = $input['input_info_district'];
                $store_data['info_main_district_name'] = $district->name;
                $store_data['info_main_district_name_th'] = $district->name_th;
            }
            if(!empty($input['input_info_subdistrict'])){
                $store_data['info_main_subdistrict'] = $input['input_info_subdistrict'];
                $store_data['info_main_subdistrict_name'] = $sudistrict->name;
                $store_data['info_main_subdistrict_name_th'] = $sudistrict->name_th;
            }
            if(!empty($input['input_info_code'])){
                $store_data['info_main_postcode'] = $input['input_info_code'];
            }
            if(!empty($input['input_info_tel'])){
                $store_data['info_main_tel'] = $input['input_info_tel'];
            }
            if(!empty($input['input_info_email'])){
                $store_data['info_main_email'] = $input['input_info_email'];
            }
            if(!empty($input['input_info_prefix']) && !empty($input['input_info_licence'])){
                $store_data['info_car_licence'] = $input['input_info_prefix']." ".$input['input_info_licence'];
            }
            if(!empty($input['input_info_car_province'])){
                $store_data['info_car_province_id'] = str_pad(Province::where('id',$input['input_info_car_province'])->first()->id,2,"0",STR_PAD_LEFT);
            }
            if(!empty($input['input_info_car_province'])){
                $store_data['info_car_province'] = App::isLocale('th')?(Province::where('id',$input['input_info_car_province'])->first()->name_th):(Province::where('id',$input['input_info_car_province'])->first()->name);
            }
            if(!empty($input['input_info_car_number'])){
                $store_data['info_car_chassis'] = $input['input_info_car_number'];
            }

            $store_data['info_is_advice'] = !empty($input['is_advice'])?"YES":"NO";
            $store_data['info_is_post'] = !empty($input['is_post'])?"YES":"NO";
            $store_data['info_is_personal'] = !empty($input['is_personal'])?"YES":"NO";
            $store_data['info_is_disclosure'] = !empty($input['is_disclosure'])?"YES":"NO";
            $store_data['info_is_email'] = !empty($input['is_email'])?"YES":"NO";
            $store_data['info_is_true'] = !empty($input['is_true'])?"YES":"NO";
            $store_data['info_is_agree'] = !empty($input['is_agree'])?"YES":"NO";
            $store_data['info_is_agree'] = !empty($input['is_agree'])?"YES":"NO";
            $store_data['info_path_personal_id'] = $path_personal_id;
            $store_data['info_is_main_driver'] = !empty($input['input_is_main_driver'])?$input['input_is_main_driver']:0;

            $this->insurance_manager->setInsuranceInfomation($store_data);

            return json_encode([]);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return json_encode(array("resp_error"=>$e->getMessage()));
        }
    }




}
