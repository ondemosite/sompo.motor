<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Users;
use App\Models\SocialSettings;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Validation\Rule;


class UserController extends Controller
{

  public function __construct(){
      $this->middleware('guest:web',['except' => 'logout']);
  }

  public function register(Request $request){
      if ($request->ajax()) {
        return $this->ajaxRegister($request->all());
      }else{
        return $this->normalRegister($request->all());
      }
  }

  private function ajaxRegister($input){
    try{
        $validate_input = array(
            'email' => [
                'required',
                'email',
                Rule::unique('users'),
            ],
            'password'=> 'required|min:4|max:20',
            'retype_password' => 'required|min:4|max:20|same:password',
        );
        $validator = Validator::make($input,$validate_input);
        if ($validator->fails()) {
          $validate_first = $validator->errors()->all();
          return json_encode(array(
                'auth'=>false,
                "reason"=>$validate_first[0]
          ));
        }
        $data = array(
            'email'=>$input['email'],
            'password'=>bcrypt($input['password']),
        );
        $user = Users::create($data);
        if($user){
            Auth::guard('web')->login($user);
            return json_encode(array(
                'auth'=>true
            ));
        }else{
            return json_encode(array(
                'auth'=>false,
                'reason' => "can not register please contact callcenter"
            ));
        }
    }catch(\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>"can not login please contact callcenter"));
    }
  }

  private function normalRegister($input){
    try{
        $validate_input = array(
            'email' => [
                'required',
                'email',
                Rule::unique('users'),
            ],
            'password'=> 'required|min:4|max:20',
            'retype_password' => 'required|min:4|max:20|same:password',
        );
        $validator = Validator::make($input,$validate_input);
        if ($validator->fails()) {
          $validate_first = $validator->errors()->all();
          return redirect()->back()->withErrors(['register'=>$validate_first[0]]);
        }
        $data = array(
            'email'=>$input['email'],
            'password'=>bcrypt($input['password']),
        );
        $user = Users::create($data);
        if($user){
            Auth::guard('web')->login($user);
            return redirect()->intended(route('web.dashboard'));
        }else{
            return redirect()->back()->withErrors(['register'=>'can not register please contact callcenter']);
        }
    }catch(\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>"can not login please contact callcenter"));
    }
  }

  //-----------------------------

  public function logout(Request $request){
        Auth::guard('web')->logout();
        //$request->session()->flush();
        //$request->session()->regenerate();
        return redirect(route('web.dashboard'));
  }


}
