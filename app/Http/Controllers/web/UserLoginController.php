<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Users;
use App\Models\SocialSettings;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Log;
use Validator;


class UserLoginController extends Controller
{

  public function __construct(){
      $this->middleware('guest:web',['except' => 'logout']);
  }

  public function showLoginForm(){
      return view('web.login.login');
  }

  public function login(Request $request){
      if ($request->ajax()) {
        return $this->ajaxLogin($request->all());
      }else{
        return $this->normalLogin($request->all());
      }
    
  }

  private function ajaxLogin($input){
    try{
        $validator = Validator::make($input, [
            'email' => 'required',
            'password' => 'required|min:4'
        ]);
        if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return json_encode(array(
                'auth'=>false,
                "reason"=>"Input Data is not valid"
            ));
        }else{
            if(Auth::guard('web')->attempt(['email'=>$input['email'],'password'=>$input['password']])){
                return json_encode(array(
                    'auth'=>true
                ));
            }
            return json_encode(array(
                'auth'=>false,
                'reason' => "อีเมล หรือ รหัสผ่าน ไม่ถูกต้อง"
            ));
        }
    }catch(\Exception $e) {
        Log::error($e->getMessage());
        return json_encode(array("resp_error"=>"can not login please contact callcenter"));
    }
  }

  private function normalLogin($input){
    try{
        $validator = Validator::make($input, [
            'email' => 'required',
            'password' => 'required|min:4'
        ]);
        if ($validator->fails()) {
            $validate_first = $validator->errors()->all();
            return redirect()->back()->withErrors(["login"=>$validate_first[0]]);
        }
        if(Auth::guard('web')->attempt(['email'=>$input['email'],'password'=>$input['password']])){
            return redirect()->intended(route('web.dashboard'));
        }
        return redirect()->back()->withErrors(["login"=>"email or password incorrect!"]);
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return redirect()->back()->withErrors(["login"=>"can not login please contact callcenter"]);
    }
  }

  //Social login

  public function redirectToProvider($type = null)
  {
      if($type!=null){
        $setting = SocialSettings::select('client_id','client_secret')->where('type',strtoupper($type))->first()->toArray();
        return $this->customDriver($setting,$type)->redirect();
      }
      return redirect()->back();
  }

  public function handleProviderCallback($type = null)
  {
      try{
        $setting = SocialSettings::select('client_id','client_secret')->where('type',strtoupper($type))->first()->toArray();
        if($type=="google"){
            $social_user = $this->customDriver($setting,$type)->stateless()->user();
        }else{
            $social_user = $this->customDriver($setting,$type)->user();
        }
        $auth_user = $this->findOrCreateUser($type,$social_user);
        if($auth_user){
            Auth::guard('web')->login($auth_user);
            return redirect()->intended(route('web.dashboard'));
        }
        return redirect()->back()->withErrors(["username or password incorrect!"]);
      }catch(\Exception $e) {
        Log::error($e->getMessage());
        return redirect()->back()->withErrors($e->getMessage());
      }
  }



  
  //--------------------------

  private function customDriver($config,$type){ 
    if($type=="facebook"){
        $config['redirect'] = env('FACEBOOK_CALLBACK_URL');
        return  Socialite::buildProvider(\Laravel\Socialite\Two\FacebookProvider::class, $config);
    }else if($type=="google"){
        $config['redirect'] = env('GOOGLE_CALLBACK_URL');
        return  Socialite::buildProvider(\Laravel\Socialite\Two\GoogleProvider::class, $config);
    }
  }

  private function findOrCreateUser($type,$userObj){
    try{
        $findUser = Users::where('social_id','=',$userObj->id)->first();
        if($findUser){
            return $findUser;
        }else{
            $first_name = null;
            $last_name = null;
            if(!empty($userObj->name)){
                $splitName = explode(' ', $userObj->name, 2); 
                $first_name = $splitName[0];
                $last_name = !empty($splitName[1]) ? $splitName[1] : '';
            }
            
            $data = array(
                'password'=>bcrypt($userObj->id),
                'name'=>$first_name,
                'lastname'=>$last_name,
                'social_id'=>$userObj->id,
                'avatar'=>$userObj->avatar,
                'type'=>strtoupper($type)
            );
            if(!empty($userObj->email)) $data['email']=$userObj->email;
            return Users::create($data);
        }
    }catch(\Exception $e) {
        Log::error($e->getMessage());
        return false;
    }
  }

  //-----------------------------

  public function logout(Request $request){
        Auth::guard('web')->logout();
        //$request->session()->flush();
        //$request->session()->regenerate();
        return redirect(route('web.dashboard'));
  }


}
