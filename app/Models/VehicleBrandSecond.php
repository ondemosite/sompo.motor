<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleBrandSecond extends BaseModel {

    protected $table = 'vehicle_brand_second';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','top_order','created_by','updated_by','created_at','updated_at'
    ];

}
