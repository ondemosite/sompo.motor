<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends BaseModel {

    protected $table = 'payments';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_number','policy_number','payment_no','merchant_id','currency_code','description','amount','status','status_detail',
        'expire_date','paid_date',
        'created_by','updated_by','created_at','updated_at'
    ];


}
