<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends BaseModel {

    protected $table = 'transaction_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_no','descriptions','type',
        'created_at','updated_at','created_by','updated_by'
    ];

}
