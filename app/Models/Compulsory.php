<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compulsory extends BaseModel {

    protected $table = 'compulsory';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','body_type','motor_code_ac','net_premium','stamp','vat','gross_premium','created_at','updated_at','created_by','updated_by'
    ];

}
