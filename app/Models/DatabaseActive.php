<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DatabaseActive extends BaseModel {

    protected $table = 'database_active';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','vehicle_active','pricing_active','created_at','updated_at','created_by','updated_by'
    ];


}
