<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends BaseModel {

    protected $table = 'order';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_number','payment_result','is_otp','status','gross_premium','theft_gross_premium','taxi_gross_premium','hb_gross_premium','carloss_gross_premium','net_discount','discount','insurance_net_premium','stamp','vat',
        'based_premium','od_based_premium','basic_premium_cover','additional_premium_cover','fleet','fleet_percent','ncb','ncb_percent','od_si','od_total_premium','deduct','cctv_percent','cctv_discount','direct',
        'direct_percent','flood_gross_premium','flood_net_premium','flood_stamp','flood_vat','promotion_id','created_by',
        'updated_by','created_at','updated_at','order_expire','car_licence','car_province','vehicle_info','promotion_code',
        'car_chassis_number','car_cctv','insurance_plan_id','insurance_plan_name','insurance_start','insurance_expire','insurance_ft_si',
        'insurance_garage_type','insurance_deduct','insurance_person_damage_once','insurance_person_damage_person','insurance_stuff_damage',
        'insurance_death_disabled','insurance_medical_fee','insurance_bail_driver','insurance_driver_amount','compulsory_net_premium','compulsory_stamp','compulsory_vat',
        'compulsory_start','compulsory_expire','addon_flood','addon_theft','addon_taxi','addon_hb','addon_carloss','main_driver','driver1','driver2','document_path_personal_id',
        'document_path_car_licence','document_path_cctv_inside','document_path_cctv_outside','document_path_driver1_licence','document_path_driver2_licence',
        'is_advice','is_post','is_personal','is_email','is_disclosure','is_true','is_agree',
        'theft_net_premium','taxi_net_premium','hb_net_premium','carloss_net_premium','b_net_premium','b_stamp','b_vat','is_bangkok'
    ];

    public function vehicle_info(){
        return $this->belongsTo('App\Models\VehicleInfo','vehicle_info','id');
    }

    public function car_province(){
        return $this->belongsTo('App\Models\Province','car_province','id');
    }

    public function main_driver(){
        return $this->belongsTo('App\Models\DriverInfo','main_driver','id');
    }

    public function driver_1(){
        return $this->belongsTo('App\Models\DriverInfo','driver1','id');
    }

    public function driver_2(){
        return $this->belongsTo('App\Models\DriverInfo','driver2','id');
    }

    public function promotion(){
        return $this->belongsTo('App\Models\Promotion','promotion_id','id');
    }

    public function payment(){
        return $this->hasOne('App\Models\Payment','order_number', 'order_number');
    }

    public function receipt(){
        return $this->hasOne('App\Models\Receipt','order_number', 'order_number');
    }

}
