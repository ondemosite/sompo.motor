<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends BaseModel {

    protected $table = 'vehicle_model';
    protected $primaryKey = "id";
    private $vehicleActive = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->vehicleActive = getDatabaseActive('vehicle');
        $this->vehicleActive==1?($this->table = 'vehicle_model'):($this->table = 'vehicle_model_second');
    }

    protected $fillable = [
        'id','name','brand_id','created_by','updated_by','created_at','updated_at'
    ];

    public function brand_name() {
        if($this->vehicleActive==1){
            return $this->belongsTo('App\Models\VehicleBrand','brand_id','id');
        }else{
            return $this->belongsTo('App\Models\VehicleBrandSecond','brand_id','id');
        }
    }

}
