<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleRecommend extends BaseModel {

    protected $table = 'article_recommend';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','article_id','email','recommend','created_at','updated_at'
    ];

}
