<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menus extends BaseModel {

    protected $table = 'menus';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','name_th','parent_id','path','icon','orders','isshow','level'
    ];

}
