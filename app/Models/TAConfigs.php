<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TAConfigs extends BaseModel {

    /*
    protected $connection = 'mysql_ta';*/
    protected $table = 'configs';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'config_code','config_value',
        'created_at', 'created_by','updated_at','updated_by'
    ];
    
    /*
    public function createdby() {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function updatedby() {
        return $this->belongsTo(User::class, 'updated_by');
    }*/

}
