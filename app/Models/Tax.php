<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends BaseModel {

    protected $table = 'tax';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','stamp','vat','compulsory_medical_fee','compulsory_death_disable','created_at','updated_at','created_by','updated_by'
    ];

}
