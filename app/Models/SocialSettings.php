<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialSettings extends BaseModel {

    protected $table = 'social_settings';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id','client_secret','type'
    ];

}
