<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends BaseAuthModel {

    protected $table = 'users';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','password','email','name','lastname','cover','social_id','avatar','type','created_at','status'
    ];

    protected $hidden = [
        'remember_token',
    ];

}
