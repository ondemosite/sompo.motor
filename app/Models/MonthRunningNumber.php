<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonthRunningNumber extends BaseModel {

    protected $table = 'month_running_number';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','tax','com_tax','year','month','created_at','updated_at'
    ];

}
