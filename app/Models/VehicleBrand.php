<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleBrand extends BaseModel {

    protected $table = 'vehicle_brand';
    protected $primaryKey = "id";
    private $vehicleActive = null;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->vehicleActive = getDatabaseActive('vehicle');
        $this->vehicleActive==1?($this->table = 'vehicle_brand'):($this->table = 'vehicle_brand_second');
    }

    protected $fillable = [
        'id','name','top_order','created_by','updated_by','created_at','updated_at'
    ];

}
