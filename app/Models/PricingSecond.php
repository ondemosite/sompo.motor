<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricingSecond extends BaseModel {

    protected $table = 'pricing_second';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id','coverage_id','coverage_name','ft_si','car_code','car_engine','define_name','cctv','garage_type','deductible','additional_coverage','mortor_package_code',
        'based_prem','based_prem_percent','name_policy','basic_premium_cover','add_premium_cover','fleet_percent','fleet','ncb_percent','ncb','total_premium','od_si','od_based_prem',
        'od_total_premium','deduct_percent','deduct','cctv_discount_percent','cctv_discount','direct_percent','direct','net_premium','stamp','vat','gross_premium',
        'flood_net_premium','flood_stamp','flood_vat','flood_gross_premium','is_bangkok','created_by','updated_by','created_at','updated_at'
    ];
}
