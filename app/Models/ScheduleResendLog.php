<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleResendLog extends BaseModel {

    protected $table = 'schedule_resend_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','schedul_resend_id','round_effort','status','created_at','updated_at'
    ];

}
