<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends BaseModel {

    protected $table = 'article';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','category','title_en','title_th','link','order','status','cover_path_en','cover_path_th','detail_en','detail_th','viewer','created_by','updated_by','created_at','updated_at'
    ];

}
