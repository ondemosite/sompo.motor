<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiLog extends BaseModel {

    protected $table = 'api_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_number','response_data','created_at','updated_at'
    ];

}
