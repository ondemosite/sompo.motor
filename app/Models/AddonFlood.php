<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonFlood extends BaseModel {

    protected $table = 'addon_flood';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','sum_insured','net_premium','stamp','vat','gross_premium','max_rate',
        'created_at','updated_at','created_by','updated_by'
    ];


}
