<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Addon extends BaseModel {

    protected $table = 'addon';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','taxi','theft','misc_package_code','sum_insured','net_premium','stamp','vat','gross_premium','created_at','updated_at','created_by','updated_by'
    ];

}
