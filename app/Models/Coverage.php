<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coverage extends BaseModel {

    protected $table = 'coverage';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','plan','person_damage_person','person_damage_once','stuff_damage','death_disabled',
        'medical_fee','bail_driver','max_age','min_age','max_pre','status',
        'created_at','updated_at','created_by','updated_by'
    ];

}
