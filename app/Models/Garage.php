<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Garage extends BaseModel {

    protected $table = 'garage';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','type_name','type','name','address','province','district','sub_district','tel','fax','lat','long','status','created_by','updated_by','created_at','updated_at'
    ];

    public function type_name() {
        return $this->belongsTo('App\Models\GarageType','type','id');
    }

    public function province() {
        return $this->belongsTo('App\Models\Province','province','id');
    }

    public function district() {
        return $this->belongsTo('App\Models\District','district','id');
    }

    public function sub_district() {
        return $this->belongsTo('App\Models\SubDistrict','sub_district','id');
    }


}
