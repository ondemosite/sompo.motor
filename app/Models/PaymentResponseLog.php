<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentResponseLog extends BaseModel {

    protected $table = 'payment_response_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_number','amount','transaction_ref','approval_code','eci','payment_channel','payment_status','channel_response_code',
        'channel_response_desc','masked_pan','payment_scheme','process_by','browser_info','request_at','response_at',
        'created_by','updated_by','created_at','updated_at'
    ];

}
