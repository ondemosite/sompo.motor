<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SignatureLog extends BaseModel {

    protected $table = 'signature_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','policy_id','policy_number','data','response','status','created_at','updated_at'
    ];


}
