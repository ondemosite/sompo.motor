<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleModelSecond extends BaseModel {

    protected $table = 'vehicle_model_second';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','brand_id','created_by','updated_by','created_at','updated_at'
    ];

    public function brand_name() {
        return $this->belongsTo('App\Models\VehicleBrandSecond','brand_id','id');
    }

}
