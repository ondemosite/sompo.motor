<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends BaseModel {

    protected $table = 'banner';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','title','cover_en','cover_th','cover_en_mobile','cover_th_mobile','sync_type','promotion_id','link','description','orders','view','status','created_at','updated_at','created_by','updated_by'
    ];

    public function promotion() {
        return $this->belongsTo(Promotion::class, 'promotion_id');
    }

}
