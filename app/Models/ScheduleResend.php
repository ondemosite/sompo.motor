<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleResend extends BaseModel {

    protected $table = 'schedule_resend';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','type','policy_id','retry_remaining','complete_status','process_status','created_at','updated_at'
    ];

}
