<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receipt extends BaseModel {

    protected $table = 'receipt';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','receipt_number','order_number','payment_number','policy_number','amount','payment_channel_code','credit_number','payment_scheme'
        ,'created_at','updated_at'
    ];

}
