<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarcodeLog extends BaseModel {

    protected $table = 'barcode_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','data','response','status','created_at','updated_at'
    ];


}
