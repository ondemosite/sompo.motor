<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubDistrict extends BaseModel {

    protected $table = 'subdistricts';
    protected $primaryKey = ['province_id','district_id','subdistrict_id'];
    public $incrementing = false;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','province_id','district_id','subdistrict_id','name','name_th','postalcode','code'
    ];
    
    public function province() {
        return $this->belongsTo(Province::class, 'province_id');
    }
    
    public function district() {
        return $this->belongsTo(District::class,'district_id','id')
                ->where('province_id', $this->province_id);
    }

}
