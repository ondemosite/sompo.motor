<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RunningNumber extends BaseModel {

    protected $table = 'running_number';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order','payment','policy_av5','policy_av3','com','misc','tax','year','created_at','updated_at'
    ];

}
