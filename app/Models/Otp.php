<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends BaseModel {

    protected $table = 'otp';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','session_id','reference_no','otp_number','otp_expire','verify','created_at','updated_at'
    ];


}
