<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends BaseModel {

    protected $table = 'faq';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','question','answer','status','created_at','updated_at','created_by','updated_by'
    ];

}
