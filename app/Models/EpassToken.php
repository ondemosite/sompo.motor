<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpassToken extends BaseModel {

    protected $table = 'epass_token';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','policy_id','token','expire_at','created_at','updated_at'
    ];

}
