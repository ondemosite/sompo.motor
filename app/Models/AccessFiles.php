<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessFiles extends BaseModel {

    protected $table = 'access_files';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','ref_id','token','allow_files','expire_date',
        'created_at','updated_at','created_by','updated_by'
    ];

}
