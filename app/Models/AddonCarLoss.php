<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonCarLoss extends BaseModel {

    protected $table = 'addon_carloss';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','ft_si','total_si',
        'net_normal','stamp_normal','vat_normal','normal',
        'net_under29','stamp_under29','vat_under29','under29',
        'net_over29','stamp_over29','vat_over29','over29',
        'net_cctv','stamp_cctv','vat_cctv','cctv','max_rate',
        'created_at','updated_at','created_by','updated_by'
    ];

    public function admin() {
        return $this->belongsTo('App\Models\Admins','updated_by','id');
    }

}
