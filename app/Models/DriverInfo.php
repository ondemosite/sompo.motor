<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverInfo extends BaseModel {

    protected $table = 'driver_info';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','prefix','prefix_name','name','lastname','idcard','licence','birth_date','gender','address','province','district','subdistrict','postalcode','tel','email',
        'province_name','district_name','subdistrict_name','province_name_th','district_name_th','subdistrict_name_th',
        'created_at','updated_at'
    ];

    public function province() {
        return $this->belongsTo(Province::class, 'province');
    }
    public function district() {
        return $this->belongsTo(District::class, 'district','id')
                ->where('province_id', $this->province);
    }
    public function subdistrict() {
        return $this->belongsTo(SubDistrict::class, 'subdistrict','subdistrict_id')
                ->where('province_id', $this->province)
                ->where('district_id', $this->district);
    }
    public function prefix() {
        return $this->belongsTo(Prefix::class, 'prefix');
    }

}
