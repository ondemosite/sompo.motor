<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlacklistLog extends BaseModel {

    protected $table = 'blacklist_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','data','response','status','created_by','updated_by','created_at','updated_at'
    ];


}
