<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleInfo extends BaseModel {

    protected $table = 'vehicle_info';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','brand','model','year','body_type','model_type','model_type_full','mortor_code_av','mortor_code_ac','cc','tons','car_seat','driver_passenger','red_plate'
        ,'used_car','car_age','status','created_by','modified_by','created_at','updated_at'
    ];

}
