<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends BaseModel {

    protected $table = 'provinces';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','name_th','ordering','code'
    ];

}
