<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfirmPolicy extends BaseModel  {

    protected $table = 'confirm_policy';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','confirm_code','policy_id','status','confirm_date','created_at','updated_at'
    ];

    public function policy(){
        return $this->belongsTo('App\Models\Policy','policy_id','id');
    }

}
