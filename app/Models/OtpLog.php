<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OtpLog extends BaseModel {

    protected $table = 'otp_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','receiver','data','response','status','created_at','updated_at'
    ];


}
