<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cutoff extends BaseModel {

    protected $table = 'cutoff';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','years','months','cutoff_date','created_at','updated_at','created_by','updated_by'
    ];

    public function admin() {
        return $this->belongsTo('App\Models\Admins','updated_by','id');
    }

}
