<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Policy extends BaseModel {

    protected $table = 'policy';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','policy_number','policy_addon_number','policy_com_number','tax_note_number','com_tax_note_number','order_id','status','note','owner',
        'created_by','updated_by','created_at','updated_at','car_licence','car_province','vehicle_info',
        'car_chassis_number','car_cctv','car_seat','car_cc','insurance_plan_id','insurance_plan_name','insurance_start','insurance_expire','insurance_ft_si',
        'insurance_garage_type','insurance_deduct','insurance_person_damage_once','insurance_person_damage_person','insurance_stuff_damage',
        'insurance_death_disabled','insurance_medical_fee','insurance_bail_driver','insurance_driver_amount','sticker_no','compulsory_start','compulsory_expire',
        'addon_theft','addon_taxi','addon_hb','addon_carloss','document_path_personal_id',
        'document_path_car_licence','document_path_cctv_inside','document_path_cctv_outside','document_path_driver1_licence','document_path_driver2_licence',
        'is_advice','is_post','is_personal','is_email','is_disclosure','is_true','is_agree',
        'note','locale','update_core_status'
    ];

    public function order(){
        return $this->belongsTo('App\Models\Order','order_id','id');
    }

    public function payment(){
        return $this->belongsTo('App\Models\Payment','policy_number','policy_number');
    }

    public function receipt(){
        return $this->belongsTo('App\Models\Receipt','policy_number','policy_number');
    }

    public function vehicle_info(){
        return $this->belongsTo('App\Models\VehicleInfo','vehicle_info','id');
    }

    public function car_province(){
        return $this->belongsTo('App\Models\Province','car_province','id');
    }

    public function endorse(){
        return $this->hasOne('App\Models\Endorse','policy_id','id')->orderBy('endorse_no','desc');
    }

    public function endorseLog(){
        return $this->hasOne('App\Models\EndorseLog','policy_id','id')->orderBy('endorse_no','desc');
    }

    public function endorses(){
        return $this->hasMany('App\Models\Endorse','policy_id','id');
    }

    public function confirmation(){
        return $this->hasOne('App\Models\ConfirmPolicy','policy_id','id')->orderBy('id','desc');
    }
    

}
