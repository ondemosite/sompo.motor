<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleComment extends BaseModel {

    protected $table = 'article_comment';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','admin_id','user_id','article_id','refer_id','refer_name','name','lastname','email','comment','level','status','reply_status','created_at','updated_at'
    ];

}
