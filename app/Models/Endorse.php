<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Endorse extends BaseModel {

    protected $table = 'endorse';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','policy_id','endorse_no','owner','driver1','driver2',
        'policy_document_path','policy_original_invoice_path','policy_copy_invoice_path',
        'compulsory_document_path','compulsory_original_invoice_path','compulsory_copy_invoice_path',
        'is_print_policy','is_print_original_invoice','is_print_copy_invoice','is_print_compulsory',
        'policy_document_name','policy_original_invoice_name','policy_copy_invoice_name','compulsory_document_name',
        'compulsory_original_invoice_name','compulsory_copy_invoice_name',
        'created_by','updated_by','created_at','updated_at'
    ];

    public function policy(){
        return $this->belongsTo('App\Models\Policy', 'policy_id', 'id');
    }

    public function owner(){
        return $this->belongsTo('App\Models\DriverInfo', 'owner', 'id');
    }

    public function driver1(){
        return $this->belongsTo('App\Models\DriverInfo', 'driver1', 'id');
    }

    public function driver2(){
        return $this->belongsTo('App\Models\DriverInfo', 'driver2', 'id');
    }
}
