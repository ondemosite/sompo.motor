<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PolicyEditDocLog extends BaseModel {

    protected $table = 'policy_edit_doc_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','policy_id','data','created_at','updated_at','created_by','updated_by'
    ];

    public function admin() {
        return $this->belongsTo('App\Models\Admins','created_by','id');
    }


}
