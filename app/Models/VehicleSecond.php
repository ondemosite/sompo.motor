<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleSecond extends BaseModel {

    protected $table = 'vehicle_second';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->vehicleActive = getDatabaseActive('vehicle');
    }
    
    protected $fillable = [
        'id','brand_id','model_id','year','body_type','model_type','model_type_full','mortor_code_av','mortor_code_ac','cc','tons','car_seat','driver_passenger','red_plate'
        ,'used_car','car_age','status','created_by','modified_by','created_at','updated_at'
    ];

    public function brand_name() {
        if($this->vehicleActive==1){
            return $this->belongsTo('App\Models\VehicleBrand','brand_id','id');
        }else{
            return $this->belongsTo('App\Models\VehicleBrandSecond','brand_id','id');
        }
        
    }


    public function model_name() {
        if($this->vehicleActive==1){
            return $this->belongsTo('App\Models\VehicleModel','model_id','id');
        }else{
            return $this->belongsTo('App\Models\VehicleModelSecond','model_id','id');
        }
    }

}
