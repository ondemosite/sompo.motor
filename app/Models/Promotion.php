<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends BaseModel
{
    protected $table = 'promotion';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','formular','title','code','discount','start_at','expire_at','coverage_id','maximum_grant','receive_amount',
        'vehicle_brand','vehicle_model','ending_message',
        'status','created_by','updated_by','created_at','updated_at'
    ];

    public function registerList(){
        return $this->hasMany('App\Models\PromotionRegister','promotion_id');
    }


}