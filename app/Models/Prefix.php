<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prefix extends BaseModel {

    protected $table = 'prefix';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','name_en','status','is_default','gender',
        'created_at','updated_at','created_by','updated_by'
    ];

}
