<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends BaseModel {

    protected $table = 'service';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','description','status','created_at','updated_at'
    ];

}
