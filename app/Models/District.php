<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends BaseModel {

    protected $table = 'districts';
    protected $primaryKey = ['id', 'province_id'];
    public $incrementing = false;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','province_id','name','name_th','code'
    ];
    
    public function province() {
        return $this->belongsTo(Province::class, 'province_id');
    }

}
