<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configs extends BaseModel {

    protected $table = 'configs';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','config_code','config_value','start_date','expire_date','status',
        'created_at', 'created_by','updated_at','updated_by'
    ];
    
    
    public function createdby() {
        return $this->belongsTo('App\Models\Admins','created_by','id');
    }
    
    public function updatedby() {
        return $this->belongsTo('App\Models\Admins','created_by','id');
    }

}
