<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends BaseModel {

    protected $table = 'settings';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'facebook_client_id','facebook_client_secret','google_client_id','google_client_secret'
    ];

}
