<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admins extends BaseAuthModel {

    protected $guard = 'admin';
    protected $table = 'admins';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','username','password','name','lastname','email','privilege','cover','status','created_at','updated_at',
    ];

    protected $hidden = [
        'remember_token',
    ];

    public function privilege_name() {
        return $this->belongsTo('App\Models\Privilege','privilege','id');
    }

}
