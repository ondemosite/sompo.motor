<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends BaseModel {

    protected $table = 'bank';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','logo','bank','status','mlr','mor','mrr','created_at','updated_at'
    ];

}
