<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Privilege extends BaseModel {

    protected $table = 'privilege';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name'
    ];

}
