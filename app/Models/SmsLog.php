<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsLog extends BaseModel {

    protected $table = 'sms_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','policy_id','endorse_no','data','response','status','created_by','updated_by','created_at','updated_at'
    ];

    public function admin(){
        return $this->belongsTo('App\Models\Admins','created_by','id');
    }

    public function policy(){
        return $this->belongsTo('App\Models\Policy','policy_id','id');
    }

}
