<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionRegister extends BaseModel
{
    protected $table = 'promotion_register';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','promotion_id','name','lastname','email','tel','vehicle_brand','vehicle_model','created_by','updated_by','created_at','updated_at'
    ];
}