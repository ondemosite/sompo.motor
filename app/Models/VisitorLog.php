<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitorLog extends BaseModel {

    protected $table = 'visitor_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','counter','created_at','updated_at','created_by','updated_by'
    ];

}
