<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserManagementLog extends BaseModel {

    protected $table = 'user_management_log';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','edit_data','old_data','created_at','updated_at','created_by','updated_by'
    ];

    public function admin() {
        return $this->belongsTo('App\Models\Admins','created_by','id');
    }


}
