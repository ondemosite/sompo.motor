<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GarageType extends BaseModel {

    protected $table = 'garage_type';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','created_by','updated_by','created_at','updated_at'
    ];

}
