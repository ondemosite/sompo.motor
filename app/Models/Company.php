<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends BaseModel {

    protected $table = 'company';
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','title','description','created_at','updated_at'
    ];

}
