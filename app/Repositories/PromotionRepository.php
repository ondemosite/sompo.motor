<?php

namespace App\Repositories;

use App\Models\Promotion;
use DB;
use Illuminate\Support\Facades\Log;

class PromotionRepository {
    public function list_promotion($input = null) {
        $sql = Promotion::select("id","title","maximum_grant","code","discount","status","receive_amount",'start_at','expire_at');

        if(!empty($input['search_title'])){
            $sql->whereRaw('title like \'%' . $input['search_title'] . '%\'');
        }
        if(!empty($input['search_code'])){
            $sql->whereRaw('code like \'%' . $input['search_code'] . '%\'');
        }
        if(!empty($input['search_discount'])){
            $sql->where('discount',$input['search_discount']);
        }
        if(!empty($input['search_start_date'])){
            $sql->whereDate('start_at', '>=', $input['search_start_date']);
        }
        if(!empty($input['search_expire_date'])){
            $sql->whereDate('expire_at', '<=', $input['search_expire_date']);
        }
        if(!empty($input['search_status'])){
            $sql->where('status',$input['search_status']);
        }
       return $sql->orderBy('id')->get();
    }

}
