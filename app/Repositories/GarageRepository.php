<?php

namespace App\Repositories;

use App\Models\Garage;
use DB;
use Illuminate\Support\Facades\Log;

class GarageRepository {
    
    public function list_garage($input = null) {
       $sql = Garage::select("*");
       if(!empty($input['search_type_class'])){
           $sql->where('type_class',$input['search_type_class']);
       }
       if(!empty($input['search_type'])){
           $sql->where('type',$input['search_type']);
       }
       if(!empty($input['search_provice'])){
           $sql->where('province',$input['search_provice']);
       }
       if(!empty($input['search_district'])){
           $sql->where('district',$input['search_district']);
       }
       if(!empty($input['search_subdistrict'])){
           $sql->where('sub_district',$input['search_subdistrict']);
       }
       if(!empty($input['search_name'])){
           $sql->whereRaw('name like \'%' . $input['search_name'] . '%\'');
       }

       return $sql->orderBy('id')->get();
    }

    public function get_garage_locations($input = null){
        $sql = Garage::select("lat","long","id","name","address");

        if(!empty($input['input_garage_id'])){
            $sql->where('id',$input['input_garage_id']);
        }
        if(!empty($input['input_garage_prov'])){
            $sql->where('province',$input['input_garage_prov']);
        }
        if(!empty($input['input_garage_district'])){
            $sql->where('district',$input['input_garage_district']);
        }
        if(!empty($input['input_name'])){
            $sql->whereRaw('name like \'%' . $input['input_name'] . '%\'');
        }
        //Log::info($input);
        $data = $sql->orderBy('province','asc')->orderBy('id','asc')->get()->toArray();
        return $this->generateMarker($data);

    }


    private function generateMarker($data){
        $label = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        $thai_label = ['ก','ข','ค','ง','จ','ฉ','ช','ซ','ฌ','ญ','ฐ','ฑ','ฒ','ณ','ด','ต','ถ','ท','ธ','น','บ','ป','ผ','ฝ','พ','ฟ','ภ','ม','ย','ร','ล','ว','ศ','ษ','ส','ห','ฬ','อ','ฮ'];
        $counter = 0; //Limit 25;
        $level2_counter = 0;
        $level3_counter = 0;
        $level = 1;
        $key = 0;
        foreach($data as $key => $item){
            if($counter>25){
                if($level==1){
                    $counter = 0;
                    $level+=1;
                }else if($level==2){
                    $counter = 0;
                    $level2_counter+=1;
                    if($level2_counter>25){
                        $level+=1;
                    }
                }else{
                    $counter = 0;
                    $level3_counter+=1;
                }
            }

            $alphabet = null;
            if($level==1){
                $alphabet = $label[$counter];
            }
            else if($level==2){
                $alphabet = $label[$level2_counter].$label[$counter];
            }else{ //Level3
                $alphabet = $label[$counter].$thai_label[$level3_counter];
            }

            $data[$key]['alpabet'] = $alphabet;
            $data[$key]['svg'] = '<svg xmlns="http://www.w3.org/2000/svg" width="36" height="48" viewBox="-18 -48 36 48" ><defs><style>.cx {font-size: 16px;letter-spacing:1px;font-family: \'PSLOmyimBold\';}</style></defs><path fill="#c80027" fill-opacity="0.85" d="M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z" /><g><text style="font-family=\'PSLOmyimBold\';" fill="#FFFFFF"  text-anchor="middle" y="-22" class="cx">'.$alphabet.'<br/></text></g></svg>';
            $data[$key]['svg_focus'] = '<svg xmlns="http://www.w3.org/2000/svg" width="36" height="48" viewBox="-18 -48 36 48" ><defs><style>.cx {font-size: 16px;letter-spacing:1px;font-family: \'PSLOmyimBold\';}</style></defs><path fill="#59cb31" fill-opacity="0.85" d="M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z" /><g><text style="font-family=\'PSLOmyimBold\';" fill="#FFFFFF"  text-anchor="middle" y="-22" class="cx">'.$alphabet.'<br/></text></g></svg>';
            $counter++;


        }
        
        return $data;
    }


}
