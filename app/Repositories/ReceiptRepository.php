<?php

namespace App\Repositories;

use App\Models\Receipt;
use DB;
use Illuminate\Support\Facades\Log;

class ReceiptRepository {

    public function list($input){
        $sql =  Receipt::select('*');

        if(!empty($input['search_receipt_number'])){
            $sql->where('receipt_number','like','%'.$input['search_receipt_number'].'%');
        }

        if(!empty($input['search_payment_number'])){
            $sql->where('payment_number','like','%'.$input['search_payment_number'].'%');
        }

        if(!empty($input['search_credit_number'])){
            $sql->where('credit_number','like','%'.$input['search_credit_number'].'%');
        }  

        if(!empty($input['search_from_date'])){
            $sql->whereDate('created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('created_at', '<=', $input['search_to_date']);
        }

        return $sql->orderBy('created_at','desc')->get();
    }

}
