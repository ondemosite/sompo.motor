<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\Admins;
use DB;

class AdminRepository {
    
    public function list_admin($input = null) {
        $sql = DB::table('admins');
        $sql->select('admins.*',DB::raw("IFNULL((select role.name from role where role.id = admins.role),'-') as role_name"));
        if(!empty($input['role'])){
            $sql->where('admins.role','=',$input['role']);
        }
        $sql->orderBy('admins.id','asc');
        return $sql->get();
    }

    public function list_team($input = null) {
        $sql = DB::table('admins');
        $sql->select('admins.*',DB::raw("IFNULL((select role.name from role where role.id = admins.role),'-') as role_name"));
        $sql->where('admins.id','!=',$input['id']);
        if(!empty($input['role'])){
            $sql->where('admins.role','=',$input['role']);
        }
        $sql->orderBy('admins.id','asc');
        return $sql->get();
    }
    
    public function get_profile($id){
        if(!empty($id)){
            $sql = DB::table('admins');
            $sql->select('admins.*',DB::raw("IFNULL((select role.name from role where role.id = admins.role),'-') as role_name"));
            $sql->where('admins.id','=',$id);
            return $sql->first();
        }
        return false;
    }   

}
