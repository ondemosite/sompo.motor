<?php

namespace App\Repositories;

use App\Models\BarcodeLog;
use Illuminate\Support\Facades\Log;
use DB;

class BarcodeLogRepository {
    
    public function list($input = null) {
       $sql = BarcodeLog::select("*");

        if(!empty($input['search_status'])){
            $sql->where('status',$input['search_status']);
        }

        //Search Request Date
        if(!empty($input['search_from_date'])){
            $sql->whereDate('created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('created_at', '<=', $input['search_to_date']);
        }

        return $sql->orderBy('id','desc')->get();
    }


}
