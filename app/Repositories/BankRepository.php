<?php

namespace App\Repositories;

use App\Models\Bank;
use DB;
use Illuminate\Support\Facades\Log;

class BankRepository {

    public function getBankList($input = null) {
        $sql = Bank::select('*');
        
        if(!empty($input['searchPhrase'])){
            $key = addslashes(trim($input['searchPhrase']));
            $sql->whereRaw('(bank like \'%' . $key . '%\')');
        }

        $total = $sql->count('id');
        $start = ($input['current'] - 1) * $input['rowCount'];
        $length = $input['rowCount'];
        
        //sort
        if(!empty($input['sort'])){
            foreach($input['sort'] as $col => $direction){
                $sql->orderBy($col, $direction);
            }
        }
        else{
            $sql->orderBy('id');
        }

        return array(
            'total' => $total,
            'items' => $sql->take($length)->skip($start)->get()
        );
    }

}
