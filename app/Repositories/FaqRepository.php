<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\FaqCategory;
use App\Models\Faq;
use DB;
use Illuminate\Support\Facades\Log;

class FaqRepository {

    public function getFaqList($input){
        $sql = DB::table('faq as f')->select('*',DB::raw("(select c.category from faq_category as c where c.id = f.category ) as category_name"));
        if(!empty($input['category'])){
            $sql->where('category','=',$input['category']);
        }

        if(!empty($input['searchPhrase'])){
            $key = addslashes(trim($input['searchPhrase']));
            $sql->whereRaw('(question like \'%' . $key . '%\')');
        }

        $total = $sql->count('id');
        $start = ($input['current'] - 1) * $input['rowCount'];
        $length = $input['rowCount'];
        
        //sort
        if(!empty($input['sort'])){
            foreach($input['sort'] as $col => $direction){
                $sql->orderBy($col, $direction);
            }
        }
        else{
            $sql->orderBy('f.category', 'asc')
            ->orderBy('f.id', 'ASC');
        }

        return array(
            'total' => $total,
            'items' => $sql->take($length)->skip($start)->get()
        );
    }

}
