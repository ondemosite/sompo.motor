<?php

namespace App\Repositories;

use App\Models\Policy;
use App\Models\DriverInfo;
use Illuminate\Support\Facades\Log;
use DB;

class ReportRepository {

    public function getPolicyList($input){
        $sql =  DB::table('policy')->select('policy.*',
                'vehicle_info.brand','vehicle_info.model','vehicle_info.year','vehicle_info.model_type',
                'driver_info.prefix_name','driver_info.name','driver_info.lastname','driver_info.idcard','driver_info.birth_date','driver_info.tel',
                'driver_info.email','driver_info.address','driver_info.subdistrict_name','driver_info.district_name','driver_info.province_name','driver_info.postalcode',
                DB::raw('(select max(endorse_no) from endorse where policy.id = endorse.policy_id) as endorse_no'),
                'payments.payment_no as payment_number','payments.paid_date',
                'receipt.payment_channel_code','receipt.credit_number','receipt.payment_scheme',
                'order.order_number','order.promotion_code','order.b_net_premium','order.b_stamp','order.b_vat','order.net_discount','order.compulsory_net_premium','order.compulsory_stamp','order.compulsory_vat'
                )
                ->leftJoin('order', 'order.id', '=', 'policy.order_id')
                ->leftJoin('payments', 'policy.policy_number', '=', 'payments.policy_number')
                ->leftJoin('receipt', 'policy.policy_number', '=', 'receipt.policy_number')
                ->join('vehicle_info', 'policy.vehicle_info', '=', 'vehicle_info.id')
                ->join('driver_info', 'policy.owner', '=', 'driver_info.id');

        if(!empty($input['search_driver_name']) || !empty($input['input_driver_name'])){
            if(!empty($input['input_driver_name'])){
                $input['search_driver_name'] = $input['input_driver_name'];
            }
            $sql->where(function($sql) use ($input){
                $sql->where('driver_info.name', 'like', '%' . $input['search_driver_name'] . '%')
                      ->orWhere('driver_info.lastname', 'like', '%' . $input['search_driver_name']  . '%');
            });
        }

        if(!empty($input['search_car_licence']) || !empty($input['input_car_licence'])){
            if(!empty($input['input_car_licence'])){
                $input['search_car_licence'] = $input['input_car_licence'];
            }
            $sql->where('policy.car_licence','like','%'.$input['search_car_licence'].'%');
        }

        if(!empty($input['search_policy_number']) || !empty($input['input_policy_number'])){
            if(!empty($input['input_policy_number'])){
                $input['search_policy_number'] = $input['input_policy_number'];
            }
            $sql->where('policy.policy_number','like','%'.$input['search_policy_number'].'%');
        }

        if(!empty($input['search_status']) || !empty($input['input_status'])){
            if(!empty($input['input_status'])){
                $input['search_status'] = $input['input_status'];
            }
            $sql->where('policy.status',$input['search_status']);
        }
        

        if(!empty($input['search_from_date']) || !empty($input['input_from_date'])){
            if(!empty($input['input_from_date'])){
                $input['search_from_date'] = $input['input_from_date'];
            }
            $sql->whereDate('policy.created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date']) || !empty($input['input_to_date'])){
            if(!empty($input['input_to_date'])){
                $input['search_to_date'] = $input['input_to_date'];
            }
            $sql->whereDate('policy.created_at', '<=', $input['search_to_date']);
        }

        if(!empty($input['item_per_page']) && !empty($input['page_number']) ){
            $skip = ($input['page_number']-1) * $input['item_per_page'];
            $limit = $input['item_per_page'];
            $sql->skip($skip)->take($limit);
        }

        return $sql->orderBy('policy.created_at','desc')->get();
    }

    public function getOwnerList($input){
        $sql =  DB::table('policy')
                ->join('driver_info', 'policy.main_driver', '=', 'driver_info.id')
                ->select('policy.policy_number','policy.status','policy.car_licence','policy.insurance_expire','policy.is_advice','driver_info.*',
                    DB::raw('(select pv.name_th from provinces pv where pv.id = policy.car_province) as car_province')
                );

        if(!empty($input['search_policy_number']) || !empty($input['input_policy_number'])){
            if(!empty($input['input_policy_number'])){
                $input['search_policy_number'] = $input['input_policy_number'];
            }
            $sql->whereRaw('(policy.policy_number like \'%' . $input['search_policy_number'] . '%\')');
        }

        if(!empty($input['search_driver_name']) || !empty($input['input_driver_name'])){
            if(!empty($input['input_driver_name'])){
                $input['search_driver_name'] = $input['input_driver_name'];
            }
            $sql->whereRaw('(driver_info.name like \'%' . $input['search_driver_name'] . '%\' or driver_info.lastname like \'%' . $input['search_driver_name'] . '%\')');
        }

        if(!empty($input['search_email']) || !empty($input['input_email'])){
            if(!empty($input['input_email'])){
                $input['search_email'] = $input['input_email'];
            }
            $sql->whereRaw('(driver_info.email like \'%' . $input['search_email'] . '%\')');
        }

        if(!empty($input['search_tel']) || !empty($input['input_tel'])){
            if(!empty($input['input_tel'])){
                $input['search_tel'] = $input['input_tel'];
            }
            $sql->whereRaw('(driver_info.tel like \'%' . $input['search_tel'] . '%\')');
        }

        if(!empty($input['search_car_licence']) || !empty($input['input_car_licence'])){
            if(!empty($input['input_car_licence'])){
                $input['search_car_licence'] = $input['input_car_licence'];
            }
            $sql->whereRaw('(policy.car_licence like \'%' . $input['search_car_licence'] . '%\')');
        }

        if(!empty($input['search_from_date']) || !empty($input['input_from_date'])){
            if(!empty($input['input_from_date'])){
                $input['search_from_date'] = $input['input_from_date'];
            }
            $sql->whereDate('driver_info.created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date']) || !empty($input['input_to_date'])){
            if(!empty($input['input_to_date'])){
                $input['search_to_date'] = $input['input_to_date'];
            }
            $sql->whereDate('driver_info.created_at', '<=', $input['search_to_date']);
        }

        if(!empty($input['search_advice']) || !empty($input['input_advice']) ){
            $input['search_advice'] = ($input['search_advice']=="YES")?1:0;
            if(!empty($input['input_advice'])){
                $input['search_advice'] = ($input['input_advice']=="YES")?1:0;
            }
            $sql->where('policy.is_advice','=',$input['search_advice']);
        }

        if(!empty($input['search_status']) || !empty($input['input_status'])){
            if(!empty($input['input_status'])){
                $input['search_status'] = $input['input_status'];
            }
            $sql->where('policy.status',$input['search_status']);
        }

        if(!empty($input['item_per_page']) && !empty($input['page_number']) ){
            $skip = ($input['page_number']-1) * $input['item_per_page'];
            $limit = $input['item_per_page'];
            $sql->skip($skip)->take($limit);
        }

        return $sql->orderBy('driver_info.created_at','desc')->get();

    }


    public function getOrderList($input){
        $sql =  DB::table('order')->select('order.*','driver_info.name','driver_info.lastname','vehicle_info.year',
                'vehicle_info.brand','vehicle_info.model',
                    DB::raw('(select pv.name_th from provinces pv where pv.id = order.car_province) as car_province')
                )->join('vehicle_info', 'order.vehicle_info', '=', 'vehicle_info.id')
                ->join('driver_info', 'order.main_driver', '=', 'driver_info.id');
                
        if(!empty($input['search_driver_name']) || !empty($input['input_driver_name'])){
            if(!empty($input['input_driver_name'])){
                $input['search_driver_name'] = $input['input_driver_name'];
            }
            $fullname = explode(" ",$input['search_driver_name']);
            $sql->where(function($query) use ($fullname){
                $query->where('driver_info.name', 'like', '%'.$fullname[0].'%');
                if(isset($fullname[1])){
                    $query->orWhere('driver_info.lastname','like','%'.$fullname[1].'%');
                }
            });
        
        }

        if(!empty($input['search_car_licence']) || !empty($input['input_car_licence'])){
            if(!empty($input['input_car_licence'])){
                $input['search_car_licence'] = $input['input_car_licence'];
            }
            $sql->where('order.car_licence', 'like', '%'.$input['search_car_licence'].'%');
        }

        if(!empty($input['search_insurance_ft_si']) || !empty($input['input_insurance_ft_si'])){
            if(!empty($input['input_insurance_ft_si'])){
                $input['search_car_licence'] = $input['input_insurance_ft_si'];
            }
            $sql->where('insurance_ft_si',$input['search_insurance_ft_si']);
        }

        if(!empty($input['search_status']) || !empty($input['input_status'])){
            if(!empty($input['input_status'])){
                $input['search_status'] = $input['input_status'];
            }
            $sql->where('order.status',$input['search_status']);
        }
        
        if(!empty($input['search_order_number']) || !empty($input['input_order_number'])){
            if(!empty($input['input_order_number'])){
                $input['search_order_number'] = $input['input_order_number'];
            }
            $sql->where('order.order_number', 'like', '%'.$input['search_order_number'].'%');
        }

        if(!empty($input['search_from_date']) || !empty($input['input_from_date'])){
            if(!empty($input['input_from_date'])){
                $input['search_from_date'] = $input['input_from_date'];
            }
            $sql->whereDate('order.created_at','>=',$input['search_from_date']);
        }
        if(!empty($input['search_to_date']) || !empty($input['input_to_date'])){
            if(!empty($input['input_to_date'])){
                $input['search_to_date'] = $input['input_to_date'];
            }
            $sql->whereDate('order.created_at','<=',$input['search_to_date']);
        }

        return $sql->orderBy('order.created_at')->get();
    }

}
