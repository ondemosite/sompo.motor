<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Article;
use DB;
use Illuminate\Support\Facades\Log;

class MemberRepository {

    public function getMemberList($input = null) {
        $sql = DB::table('users')->select('users.*');
        
        if(!empty($input['searchPhrase'])){
            $key = addslashes(trim($input['searchPhrase']));
            $sql->whereRaw('(email like \'%' . $key . '%\''
            . ' or name like \'%' . $key . '%\''
            . ' or lastname like \'%' . $key . '%\''
            . ')');
        }

        //Log::info($input);

        if($input['search_type']!='0'){
            $sql->where('type','=',$input['search_type']);
        }
        if($input['search_status']!='-1'){
            $sql->where('status','=',$input['search_status']);
        }

        if(!empty($input['search_from_date'])){
            $sql->whereDate('created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('created_at', '<=', $input['search_to_date']);
        }

        $total = $sql->count('id');
        $start = ($input['current'] - 1) * $input['rowCount'];
        $length = $input['rowCount'];
        
        //sort
        if(!empty($input['sort'])){
            foreach($input['sort'] as $col => $direction){
                $sql->orderBy($col, $direction);
            }
        }
        else{
            $sql->orderBy('id');
        }

        //Log::info($sql->toSql());

        return array(
            'total' => $total,
            'items' => $sql->take($length)->skip($start)->get()
        );
    }
    
    public function getCategoryList($input = null) {
        $data = Category::select('*');
        
        if(!empty($input['searchPhrase'])){
            $key = addslashes(trim($input['searchPhrase']));
            $data->whereRaw('(name like \'%' . $key . '%\')');
            
        }
        $total = $data->count('id');
        $start = ($input['current'] - 1) * $input['rowCount'];
        $length = $input['rowCount'];
        
        //sort
        if(!empty($input['sort'])){
            foreach($input['sort'] as $col => $direction){
                $data->orderBy($col, $direction);
            }
        }
        else{
            $data->orderBy('id');
        }

        return array(
            'total' => $total,
            'items' => $data->take($length)->skip($start)->get()
        );
    }

    public function getCommentList($input = null){
        $sql = DB::table('article_comment as ac');
        $sql->select('ac.*',DB::raw("(select article.title from article where ac.article_id = article.id) as article_title"),
        DB::raw("(select CONCAT(users.name,' ',users.lastname) from users where ac.user_id = users.id) as second_name"));
        if(!empty($input['searchPhrase'])){
            $key = addslashes(trim($input['searchPhrase']));
            $sql->whereRaw('(ac.comment like \'%' . $key . '%\')');
        }

        if(!empty($input['search'])){
            $key = $input['search'];
            $sql->where('ac.id', '=', $key)
            ->orWhere(function ($query) use ($key) {
                $query->where('ac.refer_id', '=', $key);
            });
        }
        

        if(!empty($input['select_date'])){
            $sql->whereDate('ac.created_at', '=', $input['select_date']);
        }
        if($input['reply_status']!=-1){
            $sql->where('ac.reply_status','=',$input['reply_status']);
        }
        if($input['approve_status']!=-1){
            $sql->where('ac.status','=',$input['approve_status']);
        }
        //Log::info($sql->toSql());
        $total = $sql->count('id');
        $start = ($input['current'] - 1) * $input['rowCount'];
        $length = $input['rowCount'];
        
        //sort
        if(!empty($input['sort'])){
            foreach($input['sort'] as $col => $direction){
                $sql->orderBy($col, $direction);
            }
        }
        else{
            $sql->orderBy('ac.id','desc');
        }

        return array(
            'total' => $total,
            'items' => $sql->take($length)->skip($start)->get()
        );

    }

}
