<?php

namespace App\Repositories;

use App\Models\UserManagementLog;
use Illuminate\Support\Facades\Log;
use DB;

class UserManagementLogRepository {
    
    public function list($input = null) {
        $sql = UserManagementLog::select("*");

        //Search Request Date
        if(!empty($input['search_from_date'])){
            $sql->whereDate('created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('created_at', '<=', $input['search_to_date']);
        }

        return $sql->orderBy('id')->get();
    }


}
