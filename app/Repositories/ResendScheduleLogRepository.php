<?php

namespace App\Repositories;

use App\Models\EmailLog;
use Illuminate\Support\Facades\Log;
use DB;

class ResendScheduleLogRepository {
    
    public function list($input = null) {
        $sql = DB::table('schedule_resend_log')
        ->select('schedule_resend_log.id','schedule_resend_log.round_effort','schedule_resend_log.status'
        ,'schedule_resend_log.created_at','schedule_resend.type','policy.policy_number')
        ->join('schedule_resend','schedule_resend_log.schedul_resend_id','=','schedule_resend.id')
        ->join('policy', 'schedule_resend.policy_id', '=', 'policy.id');

        //Policy Number
        if(!empty($input['search_policy_number'])){
            $sql->where('policy_number','like','%'.$input['search_policy_number'].'%');
        }

        //Search Request Date
        if(!empty($input['search_from_date'])){
            $sql->whereDate('schedule_resend_log.created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('schedule_resend_log.created_at', '<=', $input['search_to_date']);
        }

        return $sql->orderBy('schedule_resend_log.id','desc')->get();
    }


}
