<?php

namespace App\Repositories;

use App\Models\Order;
use DB;
use Illuminate\Support\Facades\Log;

class OrderRepository {

    public function getOrderList($input){
        $sql =  DB::table('order')->select('order.*','driver_info.name','driver_info.lastname','vehicle_info.year','vehicle_info.brand','vehicle_info.model')
                ->join('vehicle_info', 'order.vehicle_info', '=', 'vehicle_info.id')
                ->join('driver_info', 'order.main_driver', '=', 'driver_info.id');

        if(!empty($input['search_car_licence'])){
            $sql->where('car_licence','like','%'.$input['search_car_licence'].'%');
        }

        if(!empty($input['search_insurance_ft_si'])){
            $sql->where('insurance_ft_si',$input['search_insurance_ft_si']);
        }

        if(!empty($input['search_status'])){
            $sql->where('order.status',$input['search_status']);
        }
        
        if(!empty($input['search_order_number'])){
            $sql->where('order_number','like','%'.$input['search_order_number'].'%');
        }

        if(!empty($input['search_from_date'])){
            $sql->whereDate('order.created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('order.created_at', '<=', $input['search_to_date']);
        }



        // //sorting
        $sort = array(
            'order.id',
            'order_number',
            'driver_info.name',
            'vehicle_info.brand',
            'insurance_ft_si',
            'is_addon',
            'is_compulsory',
            'payment_result',
            'status',
            'created_at'
        );
        $order = empty($input['order'][0]['column']) ? 0 : $input['order'][0]['column'];
        $dir = empty($input['order'][0]['dir']) ? 'desc' : $input['order'][0]['dir'];
        if ($order == 0){
            $dir = "asc"; //default
        }

        if (!empty($input['select_page'])) {
            $length = empty($input['item_perpage']) ? 50 : $input['item_perpage'];

            $start = ($input['select_page'] - 1) * $length;
        } else {
            $start = $input['start'];
            $length = $input['length'];
        }

        return array(
            'total' => $sql->count('order.id'),
            'items' => $sql->take($length)->skip($start)->orderBy($sort[$order], $dir)->get()
        );

    }

}
