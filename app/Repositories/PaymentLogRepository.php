<?php

namespace App\Repositories;

use App\Models\PaymentResponseLog;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use DB;


class PaymentLogRepository {
    
    public function list($input = null) {
       $sql = PaymentResponseLog::select("*");

        //Search order_number
        if(!empty($input['search_order_number'])){
            $sql->whereRaw('order_number like \'%' . $input['search_order_number'] . '%\'');
        }

        //Search transaction_ref
        if(!empty($input['search_transaction_ref'])){
            $sql->whereRaw('transaction_ref like \'%' . $input['search_transaction_ref'] . '%\'');
        }

        //Search payment_status
        if(!empty($input['search_status'])){
            $sql->where('payment_status',$input['search_status']);
        }

        //Search masked_pan
        if(!empty($input['search_credit_number'])){
            $sql->whereRaw('masked_pan like \'%' . $input['search_credit_number'] . '%\'');
        }

        //Search Request Date
        if(!empty($input['search_from_date'])){
            $sql->whereDate('request_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('request_at', '<=', $input['search_to_date']);
        }

       return $sql->orderBy('id','desc')->get();
    }


}
