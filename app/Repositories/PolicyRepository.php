<?php

namespace App\Repositories;

use App\Models\Policy;
use DB;
use Illuminate\Support\Facades\Log;

class PolicyRepository {

    public function getPolicyList($input){
        $sql = Policy::select('policy.*','vehicle_info.year','vehicle_info.brand','vehicle_info.model')
               ->join('vehicle_info', 'policy.vehicle_info', '=', 'vehicle_info.id');
    

        if(!empty($input['search_car_licence'])){
            $sql->where('car_licence','like','%'.$input['search_car_licence'].'%');
        }

        if(!empty($input['search_insurance_ft_si'])){
            $sql->where('insurance_ft_si',$input['search_insurance_ft_si']);
        }

        if(!empty($input['search_status'])){
            $sql->where('policy.status',$input['search_status']);
        }
        
        if(!empty($input['search_policy_number'])){
            $sql->where('policy_number','like','%'.$input['search_policy_number'].'%');
        }

        if(!empty($input['search_policy_com_number'])){
            $sql->where('policy_com_number','like','%'.$input['search_policy_com_number'].'%');
        }

        if(!empty($input['search_from_date'])){
            $sql->whereDate('policy.created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('policy.created_at', '<=', $input['search_to_date']);
        }
        if(!empty($input['search_post'])){
            $sql->where('policy.is_post', '=', $input['search_post']=="YES"?1:0 );
        }

        // //sorting
        $sort = array(
            'policy.id',
            'policy_number',
            'policy_com_number',
            'name',
            'brand',
            'insurance_ft_si',
            'status',
            'created_at'
        );
        $order = empty($input['order'][0]['column']) ? 0 : $input['order'][0]['column'];
        $dir = empty($input['order'][0]['dir']) ? 'desc' : $input['order'][0]['dir'];
        if ($order == 0){
            $dir = "desc"; //default
        }

        if (!empty($input['select_page'])) {
            $length = empty($input['item_perpage']) ? 50 : $input['item_perpage'];

            $start = ($input['select_page'] - 1) * $length;
        } else {
            $start = $input['start'];
            $length = $input['length'];
        }

        return array(
            'total' => $sql->count('policy.id'),
            'items' => $sql->take($length)->skip($start)->orderBy($sort[$order], $dir)->get()
        );

    }

}
