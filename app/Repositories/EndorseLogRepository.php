<?php

namespace App\Repositories;

use App\Models\EndorseLog;
use Illuminate\Support\Facades\Log;
use DB;

class EndorseLogRepository {
    
    public function list($input = null) {
       $sql = EndorseLog::select("*");

        //Policy Number
        if(!empty($input['search_policy_number'])){
            $sql->where('policy_number','like','%'.$input['search_policy_number'].'%');
        }

        //Policy Status
        if(!empty($input['search_status'])){
            $sql->where('status',$input['search_status']);
        }

        //AV Ref
        if(!empty($input['search_av_ref_no'])){
            $sql->where('av_reference_no','like','%'.$input['search_av_ref_no'].'%');
        }
        if(!empty($input['search_ac_ref_no'])){
            $sql->where('ac_reference_no','like','%'.$input['search_ac_ref_no'].'%');
        }

        //Search Request Date
        if(!empty($input['search_from_date'])){
            $sql->whereDate('created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('created_at', '<=', $input['search_to_date']);
        }

        return $sql->orderBy('id','desc')->get();
    }


}
