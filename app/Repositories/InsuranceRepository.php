<?php

namespace App\Repositories;

use App\Models\Vehicle;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use App\Models\VehicleSecond;
use App\Models\VehicleBrandSecond;
use App\Models\VehicleModelSecond;
use App\Models\Pricing;
use App\Models\Coverage;
use App\Models\Addon;
use App\Models\AddonCarLoss;
use App\Models\AddonHb;
use App\Models\AddonTheft;
use App\Models\AddonTaxi;
use App\Models\Compulsory;
use App\Models\Tax;
use App\Models\Order;
use App\Models\Province;
use App\Models\District;
use App\Models\SubDistrict;
use App\Models\AddonFlood;
use App\Models\Prefix;
use DateTime;
use File;
use App;
use Illuminate\Support\Facades\Log;
use App\Library\AccessFile;

class InsuranceRepository {


    public function __construct(){
    }
    
    public function getVehicleInfoRecord($input = null) {
        $sql = Vehicle::select('*');
        if(!empty($input['input_brand'])){
            $sql->where('brand_id',$input['input_brand']);
        }
        if(!empty($input['input_model'])){
            $sql->where('model_id',$input['input_model']);
        }
        if(!empty($input['input_model_year'])){
            $sql->where('year',$input['input_model_year']);
        }
        if(!empty($input['input_sub_model'])){
            $sql->where('model_type',$input['input_sub_model']);
        }

        $sql->orderBy('used_car','desc');
        $vehicleRecord = $sql->first();
        if(!empty($vehicleRecord)){
            return $vehicleRecord->toArray();
        }
        return null;
        
    }

    public function getBasicInitial(){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            $vehicle_info = Vehicle::where('id',$sessions['vehicleInfoRecord'])->first();
            /* Data For Search Pricing */
            $cc = null;
            if($vehicle_info->mortor_code_av==110){
                if($vehicle_info->cc <= 2000){
                    $cc = "<=2000CC";
                }else{
                    $cc = ">2000CC";
                }
            }else{
                if($vehicle_info->cc <= 2000){
                    $cc = "<=2000CC";
                }else if($vehicle_info->cc > 2000 && $vehicle_info->cc < 4000){
                    $cc = "<=4TONS";
                }else{
                    $cc = ">4TONS";
                }
            }
            $vehicle_info->cc = $cc;

            $defind_name = "UNNAMED";

            return[
                'vehicle_info' => $vehicle_info,
                'define_name' => $defind_name,
            ];
        }
        return null;
    }

    public function getInputData(){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            return $sessions['input_data'];
        }
        return null;
    }

    public function getInformation(){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            if(isset($sessions['information'])){
                return $sessions['information'];
            }
        }
        return null;
    }

    public function getDefaultInitialPlan(){
        if(session()->has('insurance_data')){

            $sessions = session('insurance_data');
            $basic_info = $this->getBasicInitial();
            $input_data = $this->getInputData();
            //Log::info("default plan");
            //Log::info($basic_info['vehicle_info']);

            $vehicle_brand = VehicleBrand::where('id',$basic_info['vehicle_info']->brand_id)->pluck('name');
            $vehicle_model = VehicleModel::where('id',$basic_info['vehicle_info']->model_id)->pluck('name');
            $coverage = Coverage::where('id',$input_data['input_plan'])->first()->toArray();
            $response = [];
            $queryWhere = [
                'car_code'=>$basic_info['vehicle_info']->mortor_code_av,
                'car_engine'=>$basic_info['vehicle_info']->cc,
                'define_name'=>"UNNAMED",
                'additional_coverage'=>'NO',
                'cctv' => 1,
                'deductible' => 0,
                'coverage_id' => $input_data['input_plan'],
                'is_bangkok' => $input_data['input_is_bkk']=='Y'?1:0,
            ];

            $queryPricing = Pricing::where($queryWhere)->where('ft_si','<=',$basic_info['vehicle_info']->used_car)->get();
            if(sizeof($queryPricing) > 0){
                $response['plan_info'] = $queryPricing->toArray();
                $response['summary'] = [
                    'car' => $basic_info['vehicle_info']->year." ".$vehicle_brand[0]." ".$vehicle_model[0],
                ];
                $response['coverage'] = $coverage;
            }else{
                return false;
            }
            if(!empty($input_data['input_ft_si'])){
                $response['input_ft_si'] = $input_data['input_ft_si'];
            }
            return $response;
        }else{
            return false;
        }
    }


    public function getInitialStep3(){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            $basic_info = $this->getBasicInitial();
            $input_data = $this->getInputData();
            $modyfied = null;
            $vehicle_brand = VehicleBrand::where('id',$basic_info['vehicle_info']->brand_id)->pluck('name');
            $vehicle_model = VehicleModel::where('id',$basic_info['vehicle_info']->model_id)->pluck('name');
            $coverage = Coverage::where('id',$input_data['input_plan'])->first();
            $input = [];
            if(isset($sessions['modyfied_data'])){
                $modyfied = $sessions['modyfied_data'];
            }

            // -------------------------------------------- Input change ----------------------------------------
            if(!empty($input_data)){
                // Car Info
                $input = [
                    'car_info' => [
                        'prefix' => !empty($input_data['car_info_prefix'])?$input_data['car_info_prefix']:'',
                        'licence' => !empty($input_data['car_info_licence'])?$input_data['car_info_licence']:'',
                        'province' => !empty($input_data['car_info_province'])?$input_data['car_info_province']:'',
                    ]
                ];

                //Start Insurance 
                if(!empty($input_data['start_insurance'])){
                    $input['start_insurance'] = getLocaleDate($input_data['start_insurance']);
                }else{
                    $input['start_insurance'] = null;
                }
                //End Insurance 
                if(!empty($input_data['end_insurance'])){
                    $input['end_insurance'] = getLocaleDate($input_data['end_insurance']);
                }else{
                    $input['end_insurance'] = null;
                }
                //Start Compulsory
                if(!empty($input_data['start_compulsory'])){
                    $input['start_compulsory'] = getLocaleDate($input_data['start_compulsory']);
                }else{
                    $input['start_compulsory'] = null;
                }
                //End Compulsory
                if(!empty($input_data['end_compulsory'])){
                    $input['end_compulsory'] = getLocaleDate($input_data['end_compulsory']);
                }else{
                    $input['end_compulsory'] = null;
                }
                
                //GARAGE TYPE
                if($input_data['input_plan']!="3"){
                    if(!empty($input_data['garage_type'])){
                        $input['garage_type'] = $input_data['garage_type'];
                    }else{
                        $input['garage_type'] = 'GENERAL'; //Default
                    }
                }else{
                    $input['garage_type'] = 'GENERAL'; //Default
                }

                // Defind Name
                if(!empty($input_data['define_name'])){
                    $input['define_name'] = $input_data['define_name'];
                }else{
                    $input['define_name'] = 'UNNAMED';
                }
                if(!empty($input_data['define_name2'])){
                    $input['define_name2'] = $input_data['define_name2'];
                }else{
                    $input['define_name2'] = 'UNNAMED';
                }
                
                // CCTV
                if(!empty($input_data['cctv'])){
                    $input['cctv'] = $input_data['cctv'];
                }else{
                    $input['cctv'] = 1; //Default
                }

                // Deductible
                if(!empty($input_data['deductible'])){
                    $input['deductible'] = $input_data['deductible'];
                }else{
                    $input['deductible'] = 0; //Default
                }

                $input['additional_coverage'] = 'NO';

                $is_select_addition = "NO";
                //Is Select Carloss
                if(!empty($input_data['is_carloss'])){
                    $input['is_carloss'] = $input_data['is_carloss'];
                    $is_select_addition = "YES";
                }else{
                    $input['is_carloss'] = 0;
                }
                //Is Select robbery
                if(!empty($input_data['is_robbery'])){
                    $input['is_robbery'] = $input_data['is_robbery'];
                    $is_select_addition = "YES";
                }else{
                    $input['is_robbery'] = 0;
                }
                //Is Select travel
                if(!empty($input_data['is_travel'])){
                    $input['is_travel'] = $input_data['is_travel'];
                    $is_select_addition = "YES";
                }else{
                    $input['is_travel'] = 0;
                }
                //Is Select travel
                if(!empty($input_data['is_hb'])){
                    $input['is_hb'] = $input_data['is_hb'];
                    $is_select_addition = "YES";
                }else{
                    $input['is_hb'] = 0;
                }
                //Is Select Flood
                if(!empty($input_data['is_flood'])){
                    $input['is_flood'] = $input_data['is_flood'];
                }else{
                    $input['is_flood'] = 0;
                }
                //Is Select Compulsory
                if(!empty($input_data['is_com'])){
                    $input['is_com'] = $input_data['is_com'];
                }else{
                    $input['is_com'] = 0;
                }

                if(!empty($input_data['promotion_code'])){
                    $input['promotion_code'] = $input_data['promotion_code'];
                }else{
                    $input['promotion_code'] = null;
                }

                if(!empty($input_data['is_personal'])){
                    $input['is_personal'] = $input_data['is_personal'];
                }else{
                    $input['is_personal'] = 1;
                }

                if(!empty($input_data['input_hb'])){
                    $input['input_hb'] = $input_data['input_hb'];
                }else{
                    $input['input_hb'] = null;
                }
                if(!empty($input_data['input_theft'])){
                    $input['input_theft'] = $input_data['input_theft'];
                }else{
                    $input['input_theft'] = null;
                }
                if(!empty($input_data['input_taxi'])){
                    $input['input_taxi'] = $input_data['input_taxi'];
                }else{
                    $input['input_taxi'] = null;
                }

                $input['is_select_addition'] = $is_select_addition;
                $input['ft_si'] = $input_data['input_ft_si'];

                $response['input_data'] = $input;
            }
            // ---------------------------------------- Input Change -----------------------------------------------

            $response['full_car'] = $vehicle_brand[0]." ".$vehicle_model[0]." ".trans('common.model')." ".$basic_info['vehicle_info']->model_type." ".trans('common.year')." ".$basic_info['vehicle_info']->year;
            $response['taxi'] =  AddonTaxi::pluck('sum_insured')->toArray();
            $response['plan_name'] = strtoupper($coverage->plan);
            $response['coverage_id'] = $input_data['input_plan'];
            $response['hb'] =  AddonHb::pluck('sum_insured')->toArray();
            $response['theft'] =  AddonTheft::pluck('sum_insured')->toArray();
            $response['taxi'] =  AddonTaxi::pluck('sum_insured')->toArray();
            $response['flood_button'] =  AddonFlood::where('id',1)->first()->gross_premium;

            //Calculate Carloss Button
            if(!empty($modyfied['carloss_gross_premium'])){
                $response['carloss_button'] = $modyfied['carloss_gross_premium'];
            }else{
                $carloss_bt_calculate = [
                    'ft_si' => $input_data['input_ft_si'],
                    'is_cctv' => $input['cctv'],
                    'define_name1' => $input['define_name'],
                    'define_name2' => null,
                ];
                $response['carloss_button'] = $this->getCarlossPremium($carloss_bt_calculate);
            }
            
            //------------------------
            //Calculate Hb Button
            if(!empty($modyfied['hb_gross_premium'])){
                $response['hb_button'] = $modyfied['hb_gross_premium'];
            }else{
                $hb_bt_calculate = [
                    'si' => null,
                    'is_cctv' => $input['cctv'], //default
                    'define_name1' => $input['define_name'],
                    'define_name2' => null,
                ];
                $response['hb_button'] = $this->getHbPremium($hb_bt_calculate);
            }
            
            
            //------------------------
            //Calculate Theft Button
            if(!empty($modyfied['theft_gross_premium'])){
                $response['theft_button'] = $modyfied['theft_gross_premium'];
            }else{
                $theft_bt_calculate = [
                    'si' => null,
                    'is_cctv' => $input['cctv'],
                    'define_name1' => $input['define_name'],
                    'define_name2' => null,
                ];
                $response['theft_button'] = $this->getTheftPremium($theft_bt_calculate);
            }
            
            //------------------------
            //Calculate Taxi Button
            if(!empty($modyfied['taxi_gross_premium'])){
                $response['taxi_button'] = $modyfied['taxi_gross_premium'];
            }else{
                $taxi_bt_calculate = [
                    'si' => null,
                    'is_cctv' => $input['cctv'],
                    'define_name1' => $input['define_name'],
                    'define_name2' => null,
                ];
                $response['taxi_button'] = $this->getTaxiPremium($taxi_bt_calculate);   
            }
            
            //------------------------

            $response['compulsory'] = Compulsory::where('motor_code_ac',$basic_info['vehicle_info']->mortor_code_ac)->first()->gross_premium;
            $response['summary'] = [
                'car' => $basic_info['vehicle_info']->year." ".$vehicle_brand[0]." ".$vehicle_model[0],
            ];

            //Enable Dealer Garage
            $now_year = intval(date('Y'));
            $car_year =  intval($basic_info['vehicle_info']->year);
            if($car_year >= ($now_year-2)){
                $response['enable_garage_dealer'] = 1;
            }else{
                $response['enable_garage_dealer'] = 0;
            }

            if(isset($input_data['define_name_amount'])){
                $response['define_name_amount'] = $input_data['define_name_amount'];
            }

            if($input_data['input_is_bkk'] == 'Y'){
                $response['province'] = Province::whereIn('id',[
                    '00','18','23','27','56','58'
                ])->orderBy('ordering','asc')->orderBy('name_th','asc')->pluck('name_th','id')->toArray();
            }else{
                $response['province'] = Province::whereNotIn('id',[
                    '00','18','23','27','56','58'
                ])->orderBy('ordering','asc')->orderBy('name_th','asc')->pluck('name_th','id')->toArray();
            }
            
            //Max Preorder
            $maxPre = $coverage->max_pre;
            if(empty($maxPre)) $maxPre = 0;
            
            $response['max_pre'] = $maxPre;
            return $response;

        }else{
            return false;
        }
    }

    public function getInitialStep4(){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            $modyfied = $sessions['modyfied_data'];
            $basic_info = $this->getBasicInitial();
            $input_data = $this->getInputData();
            $information = $this->getInformation();
            if(!empty($information)){
                $response['inf'] = $information;

                /** ******************************* DOCUMENT ******************************** */
                if(!empty($information['info_path_personal_id'])){
                    $documentFileList = [$information['info_path_personal_id']];
                    $accessFile = new AccessFile();
                    $accessId = $accessFile->setToken([
                        'ref_id' => $sessions['session_unique_id'],
                        'files' => $documentFileList
                    ]);
                    $response['inf']['document'] = [
                        'personal_id' => $accessFile->getUrl($accessId,$sessions['session_unique_id'],$information['info_path_personal_id'],'personal-release')
                    ];
                }

            }
            
            $vehicle_brand = VehicleBrand::where('id',$basic_info['vehicle_info']->brand_id)->pluck('name');
            $vehicle_model = VehicleModel::where('id',$basic_info['vehicle_info']->model_id)->pluck('name');

            $response['summary'] = [
                'full_car' => $vehicle_brand[0]." ".$vehicle_model[0]." ".trans('common.model')." ".$basic_info['vehicle_info']->model_type." ".trans('common.year')." ".$basic_info['vehicle_info']->year,
                'car_brand' => $vehicle_brand[0],
                'car_model' => $vehicle_model[0],
                'car_year' => $basic_info['vehicle_info']->year,
                'car_info_prefix' => $modyfied['car_info_prefix'],
                'car_info_licence' => $modyfied['car_info_licence'],
                'car_info_province' => $modyfied['car_info_province'],
                'ft_si' => $modyfied['pricing_info']->ft_si,
                'od_si' => $modyfied['pricing_info']->od_si,
                'plan' => $input_data['input_plan'],
                'plan_name' => strtoupper(Coverage::where('id',$input_data['input_plan'])->first()->plan),
                'payment_result' => $modyfied['payment_result'],
                'cctv' => $input_data['cctv'],
                'driver_amount' => $modyfied['define_name_amount'],
                'driver1_age_val'=> ($modyfied['define_name_amount']>0)?$modyfied['define_name1']:null,
                'driver2_age_val'=> ($modyfied['define_name_amount']>1)?$modyfied['define_name2']:null,
                'driver1_age'=> ($modyfied['define_name_amount']>0)?$this->matchDriverAge($modyfied['define_name1']):"ไม่ระบุ",
                'driver2_age'=> ($modyfied['define_name_amount']>0)?$this->matchDriverAge($modyfied['define_name2']):"ไม่ระบุ",
                'start_insurance' => getLocaleDate($modyfied['start_insurance']),
                'end_insurance' => getLocaleDate($modyfied['end_insurance']),
                'garage_type' => $modyfied['pricing_info']->garage_type,
                'deductible' => $modyfied['pricing_info']->deductible,
                'flood_cost' => $modyfied['flood_cost'],
                'theft_cost' => $modyfied['theft_cost'],
                'taxi_cost' => $modyfied['taxi_cost'], 
                'hb_cost' => $modyfied['hb_cost'],
                'carloss_cost' => $modyfied['carloss_cost'], 
            ];

            $response['summary_gross'] = [
                'insurance' => $modyfied['insurance_gross_premium'],
                'compulsory' => $modyfied['compulsory_gross_premium'],
                'flood' => $modyfied['flood_gross_premium'],
                'theft_gross_premium' => $modyfied['theft_gross_premium'],
                'taxi_gross_premium' => $modyfied['taxi_gross_premium'],
                'hb_gross_premium' => $modyfied['hb_gross_premium'],
                'carloss_gross_premium' => $modyfied['carloss_gross_premium'],
                'discount' => $modyfied['gross_discount'],
                'payment_result' => $modyfied['payment_result']
            ];

            $response['prefix'] = Prefix::where('status','ACTIVE')->orderBy('name','asc')->pluck('name','id')->toArray();
            $response['prefix_default'] = Prefix::where('status','ACTIVE')->where('is_default','YES')->orderBy('name','asc')->first();
            $response['province'] = Province::orderBy('ordering','asc')->orderBy('name_th','asc')->pluck('name_th','id')->toArray();
            
            return $response;
        }else{
            return false;
        }
    }

    public function getInitialStep5(){
        if(session()->has('insurance_data')){
            $sessions = session('insurance_data');
            $modyfied = $sessions['modyfied_data'];
            $basic_info = $this->getBasicInitial();
            $input_data = $this->getInputData();
            $vehicle_brand = VehicleBrand::where('id',$basic_info['vehicle_info']->brand_id)->first()->name;
            $vehicle_model = VehicleModel::where('id',$basic_info['vehicle_info']->model_id)->first()->name;
            $coverage = Coverage::where('id',$input_data['input_plan'])->first()->toArray();
            $tax = Tax::where('id',1)->first();
            $information = $sessions['information'];

    
            $response['header'] = [
                'plan_name' => strtoupper(Coverage::where('id',$input_data['input_plan'])->first()->plan),
                'ft_si' => $modyfied['pricing_info']->ft_si,
                'od_si' => $modyfied['pricing_info']->od_si,
                'start_insurance' => getLocaleDate($modyfied['start_insurance']),
                'end_insurance' => getLocaleDate($modyfied['end_insurance']),
                'full_car' => $basic_info['vehicle_info']->year." ".$vehicle_brand." ".$vehicle_model,
                'motor_code' => $basic_info['vehicle_info']->mortor_code_av,
                'is_cctv' => $modyfied['pricing_info']->cctv,
                'addon'=>[
                    'compulsory' => !empty($modyfied['compulsory_gross_premium'])?getLocaleDate($modyfied['compulsory_start']):null,
                    'is_flood' => $modyfied['is_flood']?1:0,
                    'is_theft' => !empty($modyfied['theft_gross_premium'])?1:0,
                    'is_taxi'  => !empty($modyfied['taxi_gross_premium'])?1:0,
                    'is_hb' => !empty($modyfied['hb_gross_premium'])?1:0,
                    'is_carloss'  => !empty($modyfied['carloss_gross_premium'])?1:0,
                ],
                'payment_result' => $modyfied['payment_result'],
            ];

            //Find Addon
            $is_addon = false;
            foreach($response['header']['addon'] as $index => $value){
                if(!empty($value)){
                    $is_addon = true;
                }
            }
            if(!$is_addon){
                $response['header']['addon'] = null;
            }

            $province_prefix = trans('step5.province_prefix');
            $district_prefix = ($information['info_main_province']=="00"?trans('step5.district_prefix_c'):trans('step5.district_prefix'));
            $subdistrict_prefix = ($information['info_main_province']=="00"?trans('step5.subdistrict_prefix_c'):trans('step5.subdistrict_prefix'));
            

            $province = $province_prefix."".Province::where('id',$information['info_main_province'])->first()->name_th;
            $district = $district_prefix."".District::where([
                'id'=> $information['info_main_district'],
                'province_id' => $information['info_main_province']
            ])->first()->name_th;
            $subdistrict = $subdistrict_prefix."".SubDistrict::where([
                'subdistrict_id'=> $information['info_main_subdistrict'],
                'province_id' => $information['info_main_province'],
                'district_id' => $information['info_main_district']
            ])->first()->name_th;
            $postcode = $information['info_main_postcode'];
            $address = $information['info_main_address']." ".$subdistrict." ".$district." ".$province." ".$postcode;

        
            $response['detail'] = [
                'plan_id' => $input_data['input_plan'],
                'ft_si' => $modyfied['pricing_info']->ft_si,
                'flood_si' => $modyfied['flood_cost'],
                'person_damage_once' => $coverage['person_damage_once'],
                'person_damage_person' => $coverage['person_damage_person'],
                'stuff_damage' => $coverage['stuff_damage'],
                'death_disabled' => $coverage['death_disabled'],
                'medical_fee' => $coverage['medical_fee'],
                'bail_driver' => $coverage['bail_driver'],
                'compulsory_medical_free' => $tax->compulsory_medical_fee,
                'compulsory_death_disable' => $tax->compulsory_death_disable,
                'theft_cost' => $modyfied['theft_cost'],
                'taxi_cost' => $modyfied['taxi_cost'],
                'hb_cost' => $modyfied['hb_cost'],
                'carloss_cost' => $modyfied['carloss_cost'],
                'start_insurance' => getLocaleDate($modyfied['start_insurance']),
                'end_insurance' => getLocaleDate($modyfied['end_insurance']),
                'compulsory' => $modyfied['compulsory_gross_premium'],
                'compulsory_start' => getLocaleDate($modyfied['compulsory_start']),
                'compulsory_end' => getLocaleDate($modyfied['compulsory_end']),
                'define_name_amount' => $modyfied['define_name_amount'],
                'garage_type'=> $modyfied['pricing_info']->garage_type,
                'deduct' => $modyfied['pricing_info']->deductible,
                'main_prefix' => Prefix::where('id',$information['info_main_prefix'])->first()->name,
                'main_name' => $information['info_main_name'],
                'main_lastname' => $information['info_main_lastname'],
                'main_id_card' => $information['info_main_id_card'],
                'main_gender' => $information['info_main_gender'],
                'main_birthdate' => $information['info_main_birthdate'],
                'main_age' => $this->getAge($information['info_main_birthdate']),
                'main_address' => $address,
                'main_tel' => $information['info_main_tel'],
                'main_email' => $information['info_main_email'],

                // 'driver1_name' => $information['info_driver1_name'],
                // 'driver1_lastname' => $information['info_driver1_lastname'],
                // 'driver1_id_card' => $information['info_driver1_id_card'],
                // 'driver1_gender' => $information['info_driver1_gender'],
                // 'driver1_birthdate' => $information['info_driver1_birthdate'],
                // 'driver1_age' => $this->getAge($information['info_driver1_birthdate']),

                // 'driver2_name' => $information['info_driver2_name'],
                // 'driver2_lastname' => $information['info_driver2_lastname'],
                // 'driver2_id_card' => $information['info_driver2_id_card'],
                // 'driver2_gender' => $information['info_driver2_gender'],
                // 'driver2_birthdate' => $information['info_driver2_birthdate'],
                // 'driver2_age' => $this->getAge($information['info_driver2_birthdate']),
            ];

            $response['car'] = [
                'brand' => $vehicle_brand,
                'model' => $vehicle_model,
                'model_type' => $basic_info['vehicle_info']->model_type,
                'year' => $basic_info['vehicle_info']->year,
                'licence' => $information['info_car_licence'],
                'province' => $information['info_car_province'],
                'chassis' => $information['info_car_chassis'],
            ];  


            /** ******************************* DOCUMENT ******************************** */
            if(!empty($information['info_path_personal_id'])){
                $documentFileList = [$information['info_path_personal_id']];
                $accessFile = new AccessFile();
                $accessId = $accessFile->setToken([
                    'ref_id' => $sessions['session_unique_id'],
                    'files' => $documentFileList
                ]);
                $response['document'] = [
                    'personal_id' => $accessFile->getUrl($accessId,$sessions['session_unique_id'],$information['info_path_personal_id'],'personal-release')
                ];
                $response['document']['personal_id_extension'] = !empty($response['document']['personal_id'])?File::extension($response['document']['personal_id']):"";
            }

            /*************************************** DOCUMENT ************************************* */

            $response['summary'] = [
                'gross_premium' => $modyfied['insurance_gross_premium'],
                'compulsory'=>$modyfied['compulsory_gross_premium'],
                'flood' => $modyfied['flood_gross_premium'],
                'theft_gross_premium' => $modyfied['theft_gross_premium'],
                'taxi_gross_premium' => $modyfied['taxi_gross_premium'],
                'hb_gross_premium' => $modyfied['hb_gross_premium'],
                'carloss_gross_premium' => $modyfied['carloss_gross_premium'],
                'discount' => $modyfied['gross_discount'],
                'payment_result' => $modyfied['payment_result']
            ];

            return $response;

        }else{
            return false;
        }
    }

    public function getInitialThank($order_id){
        $order = Order::where("order_number",$order_id)->first();
        if(!empty($order)){
            $vehicle_info = $order->vehicle_info()->first();
            $response['header'] = [
                'plan_name' => strtoupper(Coverage::where('id',$order->insurance_plan_id)->first()->plan),
                'ft_si' => $order->insurance_ft_si,
                'start_insurance' => getLocaleDate($order->insurance_start),
                'end_insurance' => getLocaleDate($order->insurance_expire),
                'full_car' => $vehicle_info->year." ".$vehicle_info->brand." ".$vehicle_info->model,
                'motor_code' => $vehicle_info->mortor_code_av,
                'is_cctv' => $order->car_cctv,
                'addon'=>[
                    'is_flood' => $order->flood_gross_premium>0?1:0,
                    'is_theft' => $order->theft_gross_premium>0?1:0,
                    'is_taxi'  => $order->taxi_gross_premium>0?1:0,
                    'is_hb' => $order->hb_gross_premium>0?1:0,
                    'is_carloss'  => $order->carloss_gross_premium>0?1:0,
                ],
                'payment_result' => $order->payment_result,
                'is_post' => $order->is_post,
                'is_compulsory' => $order->compulsory_net_premium>0?true:false
            ];
            //Find Addon
            $is_noaddon = true;
            if($order->compulsory_net_premium>0 && !empty($order->compulsory_start)){
                $response['header']['addon']['compulsory'] = getLocaleDate($order->compulsory_start);
                $is_noaddon = false;
            }
            if($order->flood_gross_premium>0){
                $response['header']['addon']['is_flood'] = 1;
                $is_noaddon = false;
            }
            if($order->theft_gross_premium>0){
                $response['header']['addon']['is_theft'] = 1;
                $is_noaddon = false;
            }
            if($order->taxi_gross_premium>0){
                $response['header']['addon']['is_taxi'] = 1;
                $is_noaddon = false;
            }
            if($order->hb_gross_premium>0){
                $response['header']['addon']['is_hb'] = 1;
                $is_noaddon = false;
            }
            if($order->carloss_gross_premium>0){
                $response['header']['addon']['is_carloss'] = 1;
                $is_noaddon = false;
            }
            if($is_noaddon){
                $response['header']['addon'] = null;
            }


            $response['summary'] = [
                'gross_premium' => $order->gross_premium,
                'compulsory'=>$order->compulsory_net_premium + $order->compulsory_stamp + $order->compulsory_vat ,
                'flood' => $order->flood_gross_premium,
                'theft_gross_premium' => $order->theft_gross_premium,
                'taxi_gross_premium' => $order->taxi_gross_premium,
                'hb_gross_premium' => $order->hb_gross_premium,
                'carloss_gross_premium' => $order->carloss_gross_premium,
                'discount' => $order->discount,
                'payment_result' => $order->payment_result,
            ];
            return $response;
        }else{
            return false;
        }
    }

    public function getAge($string){
        if($string!=null){
            $date = correctDate($string);
            $d1 = new DateTime();
            $d2 = new DateTime($date);
            $diff = $d2->diff($d1);
            return $diff->y;
        }
        return null;
    }

    public function getFloodPremium($input){
        // $basic_info = $this->getBasicInitial();
        // $input_data = $this->getInputData();
        // $pricing_info = null;

        // if(!empty($input['define_name'])){
        //     $buffer_pricing = Pricing::where([
        //         'coverage_id'=> $input_data['input_plan'],
        //         'ft_si'=> intval($input['ft_si']),
        //         'car_code'=>$basic_info['vehicle_info']->mortor_code_av,
        //         'car_engine'=>$basic_info['vehicle_info']->cc,
        //         'define_name'=>!empty($input['define_name1'])?$input['define_name1']:$input['define_name'],
        //         'cctv'=>intval($input['cctv']),
        //         'garage_type'=>$input['garage_type'],
        //         'deductible'=>$input['deductible'],
        //         'additional_coverage'=>'FLOOD'
        //     ])->first();
        //     if(!empty($input['define_name2'])){
        //         $buffer_pricing2 = Pricing::where([
        //             'coverage_id'=> $input_data['input_plan'],
        //             'ft_si'=> intval($input['ft_si']),
        //             'car_code'=>$basic_info['vehicle_info']->mortor_code_av,
        //             'car_engine'=>$basic_info['vehicle_info']->cc,
        //             'define_name'=>$input['define_name2'],
        //             'cctv'=>intval($input['cctv']),
        //             'garage_type'=>$input['garage_type'],
        //             'deductible'=>$input['deductible'],
        //             'additional_coverage'=>'FLOOD'
        //         ])->first();
        //         if($buffer_pricing->gross_premium > $buffer_pricing2->gross_premium){
        //             $pricing_info = $buffer_pricing;
        //         }else{
        //             $pricing_info = $buffer_pricing2;
        //         }
        //     }else{
        //         $pricing_info = $buffer_pricing;
        //     }
        // }
        // if(!empty($pricing_info)){
        //     return $pricing_info->flood_gross_premium;
        // }else{
        //     return null;
        // }
        $flood = AddonFlood::where("id",1)->first();
        if(!empty($flood)){
            return $flood->gross_premium;
        }
        return null;

    }



    public function getCarlossPremium($input,$field = "gross"){
        $addon = AddonCarLoss::where('ft_si',$input['ft_si'])->first();
        if(!empty($addon)){

            if($input['is_cctv']==1){
                // Mode CCTV ----------
                if($field=="net"){
                    $mode = "net_cctv";
                }else if($field=="stamp"){
                    $mode = "stamp_cctv";
                }else if($field=="vat"){
                    $mode = "vat_cctv";
                }else{
                    $mode = "cctv";
                }
                //-------------------
            }else if(!empty($input['define_name1']) || !empty($input['define_name2'])){
                $min_result = $this->getMinAge($input);
                if($min_result<29){
                    // Under29 Mode ---------
                    if($field=="net"){
                        $mode = "net_under29";
                    }else if($field=="stamp"){
                        $mode = "stamp_under29";
                    }else if($field=="vat"){
                        $mode = "vat_under29";
                    }else{
                        $mode = "under29";
                    }
                    //-----------------------
                }else{
                    // Over29 Mode ----------
                    if($field=="net"){
                        $mode = "net_over29";
                    }else if($field=="stamp"){
                        $mode = "stamp_over29";
                    }else if($field=="vat"){
                        $mode = "vat_over29";
                    }else{
                        $mode = "over29";
                    }
                    //-----------------------
                }
            }else{
                //Normal Mode -----------
                if($field=="net"){
                    $mode = "net_normal";
                }else if($field=="stamp"){
                    $mode = "stamp_normal";
                }else if($field=="vat"){
                    $mode = "vat_normal";
                }else{
                    $mode = "normal";
                }
                //----------------------
            }
            $data = $addon->toArray();
            return $data[$mode];
        }else{
            return null;
        }
    }

    public function getHbPremium($input,$field = "gross"){
        if(empty($input['si'])){
            $input['si'] = AddonHb::min('sum_insured');
        }
        $addon = AddonHb::where('sum_insured',$input['si'])->first();
        if(!empty($addon)){

            if($input['is_cctv']==1){
                // Mode CCTV ----------
                if($field=="net"){
                    $mode = "net_cctv";
                }else if($field=="stamp"){
                    $mode = "stamp_cctv";
                }else if($field=="vat"){
                    $mode = "vat_cctv";
                }else{
                    $mode = "cctv";
                }
                //-------------------
            }else if(!empty($input['define_name1']) || !empty($input['define_name2'])){
                $min_result = $this->getMinAge($input);
                if($min_result<29){
                    // Under29 Mode ---------
                    if($field=="net"){
                        $mode = "net_under29";
                    }else if($field=="stamp"){
                        $mode = "stamp_under29";
                    }else if($field=="vat"){
                        $mode = "vat_under29";
                    }else{
                        $mode = "under29";
                    }
                    //-----------------------
                }else{
                    // Over29 Mode ----------
                    if($field=="net"){
                        $mode = "net_over29";
                    }else if($field=="stamp"){
                        $mode = "stamp_over29";
                    }else if($field=="vat"){
                        $mode = "vat_over29";
                    }else{
                        $mode = "over29";
                    }
                    //-----------------------
                }
            }else{
                //Normal Mode -----------
                if($field=="net"){
                    $mode = "net_normal";
                }else if($field=="stamp"){
                    $mode = "stamp_normal";
                }else if($field=="vat"){
                    $mode = "vat_normal";
                }else{
                    $mode = "normal";
                }
                //----------------------
            }
            $data = $addon->toArray();
            return $data[$mode];
        }else{
            return null;
        }
    }

    public function getTheftPremium($input,$field = "gross"){
        if(empty($input['si'])){
            $input['si'] = AddonTheft::min('sum_insured');
        }
        $addon = AddonTheft::where('sum_insured',$input['si'])->first();
        if(!empty($addon)){

            if($input['is_cctv']==1){
                // Mode CCTV ----------
                if($field=="net"){
                    $mode = "net_cctv";
                }else if($field=="stamp"){
                    $mode = "stamp_cctv";
                }else if($field=="vat"){
                    $mode = "vat_cctv";
                }else{
                    $mode = "cctv";
                }
                //-------------------
            }else if(!empty($input['define_name1']) || !empty($input['define_name2'])){
                $min_result = $this->getMinAge($input);
                if($min_result<29){
                    // Under29 Mode ---------
                    if($field=="net"){
                        $mode = "net_under29";
                    }else if($field=="stamp"){
                        $mode = "stamp_under29";
                    }else if($field=="vat"){
                        $mode = "vat_under29";
                    }else{
                        $mode = "under29";
                    }
                    //-----------------------
                }else{
                    // Over29 Mode ----------
                    if($field=="net"){
                        $mode = "net_over29";
                    }else if($field=="stamp"){
                        $mode = "stamp_over29";
                    }else if($field=="vat"){
                        $mode = "vat_over29";
                    }else{
                        $mode = "over29";
                    }
                    //-----------------------
                }
            }else{
                //Normal Mode -----------
                if($field=="net"){
                    $mode = "net_normal";
                }else if($field=="stamp"){
                    $mode = "stamp_normal";
                }else if($field=="vat"){
                    $mode = "vat_normal";
                }else{
                    $mode = "normal";
                }
                //----------------------
            }
            $data = $addon->toArray();
            return $data[$mode];
        }else{
            return null;
        }
    }

    public function getTaxiPremium($input,$field = "gross"){
        if(empty($input['si'])){
            $input['si'] = AddonTaxi::min('sum_insured');
        }
        $addon = AddonTaxi::where('sum_insured',$input['si'])->first();
        if(!empty($addon)){

            if($input['is_cctv']==1){
                // Mode CCTV ----------
                if($field=="net"){
                    $mode = "net_cctv";
                }else if($field=="stamp"){
                    $mode = "stamp_cctv";
                }else if($field=="vat"){
                    $mode = "vat_cctv";
                }else{
                    $mode = "cctv";
                }
                //-------------------
            }else if(!empty($input['define_name1']) || !empty($input['define_name2'])){
                $min_result = $this->getMinAge($input);
                if($min_result<29){
                    // Under29 Mode ---------
                    if($field=="net"){
                        $mode = "net_under29";
                    }else if($field=="stamp"){
                        $mode = "stamp_under29";
                    }else if($field=="vat"){
                        $mode = "vat_under29";
                    }else{
                        $mode = "under29";
                    }
                    //-----------------------
                }else{
                    // Over29 Mode ----------
                    if($field=="net"){
                        $mode = "net_over29";
                    }else if($field=="stamp"){
                        $mode = "stamp_over29";
                    }else if($field=="vat"){
                        $mode = "vat_over29";
                    }else{
                        $mode = "over29";
                    }
                    //-----------------------
                }
            }else{
                //Normal Mode -----------
                if($field=="net"){
                    $mode = "net_normal";
                }else if($field=="stamp"){
                    $mode = "stamp_normal";
                }else if($field=="vat"){
                    $mode = "vat_normal";
                }else{
                    $mode = "normal";
                }
                //----------------------
            }
            $data = $addon->toArray();
            return $data[$mode];
        }else{
            return null;
        }
    }

    private function matchDriverAge($string){
        $locale = App::getLocale();
        $correct_th = ["18-24Y"=>"18 - 24 ปี","25-28Y"=>"25 - 28 ปี","29-35Y"=>"29 - 35 ปี","36-50Y"=>"36 - 50 ปี","50UP"=>"50 ปีขึนไป","UNNAMED"=>"ไม่ระบุ"];
        $correct_en = ["18-24Y"=>"18 - 24 Years","25-28Y"=>"25 - 28 Years","29-35Y"=>"29 - 35 Years","36-50Y"=>"36 - 50 Years","50UP"=>"Over 50 Years","UNNAMED"=>"Anonymous"];
        if($locale=='th') return $correct_th[$string];
        else return $correct_en[$string];
    }

    private function getMinAge($input){
        $min1 = null;
        $min2 = null;
        $min_result = null;
        if(!empty($input['define_name1'])){
            if($input['define_name1']=="50UP"){
                $min1 = 50;
            }else{
                $explode = explode("-",str_replace("Y","",$input['define_name1']));
                $min1 = intval($explode[0]);
            }
        }
        if(!empty($input['define_name2'])){
            if($input['define_name2']=="50UP"){
                $min2 = 50;
            }else{
                $explode = explode("-",str_replace("Y","",$input['define_name2']));
                $min2 = intval($explode[0]);
            }
        }
        if(empty($min2)){
            $min_result = $min1;
        }else{
            if($min1 <= $min2){
                $min_result = $min1;
            }else{
                $min_result = $min2;
            }
        }
        return $min_result;
    }


}
