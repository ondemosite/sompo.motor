<?php

namespace App\Repositories;

use App\Models\Vehicle;
use App\Models\VehicleSecond;
use App\Models\VehicleModel;
use DB;
use Illuminate\Support\Facades\Log;

class VehicleRepository {
    
    public function list_model($input = null) {
       $sql = VehicleModel::select("*");
       if(!empty($input['search_name'])){
           $sql->whereRaw('name like \'%' . $input['search_name'] . '%\'');
       }
       if(!empty($input['search_brand'])){
           $sql->where('brand_id',$input['search_brand']);
       }
       return $sql->orderBy('id')->get();
    }

    public function list_vehicle($input = null) {
        $tableActive = getDatabaseActive('vehicle',true);
        if($tableActive==1){
            $sql = Vehicle::select("*");
        }else{
            $sql = VehicleSecond::select("*");
        }
        if(!empty($input['search_body_type'])){
            $sql->where('body_type',$input['search_body_type']);
        }
        if(!empty($input['search_year'])){
            $sql->where('year',$input['search_year']);
        }
        if(!empty($input['search_brand'])){
            $sql->where('brand_id',$input['search_brand']);
        }
        if(!empty($input['search_model'])){
            $sql->where('model_id',$input['search_model']);
        }
        if(!empty($input['search_model_type'])){
            $sql->whereRaw('model_type like \'%' . $input['search_model_type'] . '%\'');
        }

        // //sorting
        $sort = array(
            'id',
            'brand_id',
            'model_id',
            'year',
            'body_type',
            'model_type',
            'mortor_code_av',
            'mortor_code_ac'
        );
        $order = empty($input['order'][0]['column']) ? 0 : $input['order'][0]['column'];
        $dir = empty($input['order'][0]['dir']) ? 'desc' : $input['order'][0]['dir'];
        if ($order == 0){
            $dir = "asc"; //default
        }

        if (!empty($input['select_page'])) {
            $length = empty($input['item_perpage']) ? 50 : $input['item_perpage'];

            $start = ($input['select_page'] - 1) * $length;
        } else {
            $start = $input['start'];
            $length = $input['length'];
        }

        return array(
            'total' => $sql->count('id'),
            'items' => $sql->take($length)->skip($start)->orderBy($sort[$order], $dir)->get()
        );
 
     }


     public function list_imported_vehicle($input = null){
        $vehicleTableActive = getDatabaseActive('vehicle',true);
        $sql = null;
        if($vehicleTableActive==1){
            $sql = VehicleSecond::select('*');
        }else{
            $sql = Vehicle::select('*');
        }

        // //sorting
        $sort = array(
            'id',
            'brand_name',
            'model_name',
            'year',
            'body_type',
            'model_type',
            'mortor_code_av',
            'mortor_code_ac',
            'cc',
            'tons',
            'car_seat',
            'driver_passenger',
            'red_plate',
            'used_car',
            'car_age'
        );
        $order = empty($input['order'][0]['column']) ? 0 : $input['order'][0]['column'];
        $dir = empty($input['order'][0]['dir']) ? 'desc' : $input['order'][0]['dir'];
        if ($order == 0){
            $dir = "asc"; //default
        }

        if (!empty($input['select_page'])) {
            $length = empty($input['item_perpage']) ? 50 : $input['item_perpage'];

            $start = ($input['select_page'] - 1) * $length;
        } else {
            $start = $input['start'];
            $length = $input['length'];
        }

        return array(
            'total' => $sql->count('id'),
            'items' => $sql->take($length)->skip($start)->orderBy($sort[$order], $dir)->get()
        );
     }

     public function list_export(){
        $tableActive = getDatabaseActive('vehicle',true);
        if($tableActive==1){
            $sql = Vehicle::select("*");
        }else{
            $sql = VehicleSecond::select("*");
        }

        return $sql->get();
     }

}
