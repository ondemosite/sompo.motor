<?php

namespace App\Repositories;

use App\Models\PasskitLog;
use Illuminate\Support\Facades\Log;
use DB;

class PasskitLogRepository {
    
    public function list($input = null) {
       $sql = PasskitLog::select("*");

        //Policy Number
        if(!empty($input['search_policy_number'])){
            $sql->where('policy_number','like','%'.$input['search_policy_number'].'%');
        }

        //Policy Number
        if(!empty($input['search_status'])){
            $sql->where('status',$input['search_status']);
        }

        //Search Request Date
        if(!empty($input['search_from_date'])){
            $sql->whereDate('created_at', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('created_at', '<=', $input['search_to_date']);
        }

        return $sql->orderBy('id','desc')->get();
    }


}
