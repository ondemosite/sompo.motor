<?php

namespace App\Repositories;

use App\Models\Payment;
use DB;
use Illuminate\Support\Facades\Log;

class PaymentRepository {

    public function list($input){
        $sql =  Payment::select('*');

        if(!empty($input['search_payment_number'])){
            $sql->where('payment_no','like','%'.$input['search_payment_number'].'%');
        }

        if(!empty($input['search_order_number'])){
            $sql->where('order_number','like','%'.$input['search_order_number'].'%');
        }

        if(!empty($input['search_policy_number'])){
            $sql->where('policy_number','like','%'.$input['search_policy_number'].'%');
        }

        if(!empty($input['search_status'])){
            $sql->where('status',$input['search_status']);
        }
        

        if(!empty($input['search_from_date'])){
            $sql->whereDate('paid_date', '>=', $input['search_from_date']);
        }
        if(!empty($input['search_to_date'])){
            $sql->whereDate('paid_date', '<=', $input['search_to_date']);
        }

        return $sql->orderBy('created_at','desc')->get();
    }

}
