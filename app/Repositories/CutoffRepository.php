<?php

namespace App\Repositories;

use App\Models\Cutoff;
use DB;

class CutoffRepository {

    
    public function list($input){
        $sql = Cutoff::select("*");
        if(!empty($input['search_months'])){
            $sql->where('months','=',$input['search_months']);
        }
        if(!empty($input['search_years'])){
            $sql->where('years','=',$input['search_years']);
        }


        return $sql->orderBy('years', 'asc')->orderBy('months', 'ASC')->get();

    }
    
}
