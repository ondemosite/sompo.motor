<?php

namespace App\Repositories;

use App\Models\Pricing;
use App\Models\PricingSecond;
use DB;
use Illuminate\Support\Facades\Log;

class PricingRepository {
    
    public function list_pricing($input = null) {
        $this->pricingActive = getDatabaseActive('pricing',true);
        if($this->pricingActive==1){
            $sql = Pricing::select("*");
        }else{
            $sql = PricingSecond::select("*");
        }
       
       if(!empty($input['coverage_id'])){
           $sql->where('coverage_id',$input['coverage_id']);
       }
       if(!empty($input['ft_si'])){
           $sql->where('ft_si',$input['ft_si']);
       }
       if(!empty($input['car_code'])){
           $sql->where('car_code',$input['car_code']);
       }
       if(!empty($input['car_engine'])){
           $sql->where('car_engine',$input['car_engine']);
       }
       if(!empty($input['define_name'])){
           $sql->where('define_name',$input['define_name']);
       }
       if(!empty($input['mortor_package_code'])){
           $sql->where('mortor_package_code',$input['mortor_package_code']);
       }
       return $sql->orderBy('id')->get();
    }

    public function list_pricing_imported(){
        $pricingTableActive = getDatabaseActive('pricing',true);
        $pricing = new Pricing;
        if($pricingTableActive==1){
            $pricing->setTable('pricing_second');
        }else{
            $pricing->setTable('pricing');
        }

        $sql = $pricing->select("*")->orderBy('id')->get();
        return $sql;
    }

}
