<?php

namespace App\Repositories;

use App\Models\Menus;
use App\Models\Permission;
use DB;

class MenuRepository {
    
    public function get_parent_menu($input = null) {
        $sql = DB::table('menus');
        $sql->select('menus.*');
        $sql->where('level','<','2');
        $sql->orderBy('menus.id','asc');
        return $sql->get();
    }

    
    public function get_menu_permission($input = null){
        $sql = DB::table('permission as pm')->select('pm.*','m.name','m.parent_id','m.icon','m.orders','m.isshow');
        $sql->join('menus as m','pm.menu_id','=','m.id');
        if(!empty($input['privilege_id'])){
            $sql->where('pm.privilege_id','=',$input['privilege_id']);
        }
        $sql->orderBy('m.orders','asc');
        return $sql->get();
    }
    
}
