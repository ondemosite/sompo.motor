<?php

namespace App\Library;
use App;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Log;
use App\Repositories\InsuranceRepository;
use App\Models\Promotion;
use Carbon\Carbon;
use MicrosoftAzure\Storage\Blob\Models\PageWriteOption;
use App\Models\Pricing;
use App\Models\Addon;
use App\Models\AddonCarLoss;
use App\Models\AddonHb;
use App\Models\AddonTheft;
use App\Models\AddonTaxi;
use App\Models\Compulsory;
use App\Models\Tax;
use App\Models\Order;
use App\Models\DriverInfo;
use App\Models\Coverage;
use App\Models\Policy;
use App\Models\Vehicle;
use App\Models\VehicleSecond;
use App\Models\Receipt;
use App\Models\RunningNumber;
use App\Models\MonthRunningNumber;
use App\Models\Endorse;
use App\Models\VehicleInfo;
use App\Models\Cutoff;
use App\Models\DatabaseActive;
use App\Models\AddonFlood;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use PDF;
use FPDI_Protection;
use DateTime;
use DateTimeZone;
use App\Library\signature;


class InsuranceManager extends Controller
{

    private $activeTable = null;

    public function __construct(){
        $this->activeTable = getDatabaseActive();
    }

    public function setUserInput($request){
        try{
            $input = $request->all();

            if(!empty($input['input_cost'])){
                $input['input_cost'] = str_replace(",","",$input['input_cost']);
                if(!is_numeric($input['input_cost'])){
                    $input['input_cost'] = null;
                }
            }


            $sessions = array(
                'session_unique_id' => uniqid(),
                'otp_request_remain' => 2,
                'otp_verify' => 0,
                'input_brand'=>$input['input_brand'],
                'input_model'=>$input['input_model'],
                'input_model_year'=>$input['input_model_year'],
                'input_sub_model'=>$input['input_sub_model'],
                'input_cost'=>!empty($input['input_cost'])?$input['input_cost']:null
            );

            session(['insurance_data' => $sessions]);
            return true;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function setVehicleInfo($data){
        try{
            if(session()->has('insurance_data')){
                $sessions = session('insurance_data');
                $sessions['vehicleInfoRecord']=$data;
                session()->forget('insurance_data');
                session(['insurance_data' => $sessions]);
                return true;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function setSelectPlan($plan){
        try{
            if(session()->has('insurance_data')){
                $sessions = session('insurance_data');
                $sessions['input_data']['input_plan'] = $plan;
                session()->forget('insurance_data');
                session(['insurance_data' => $sessions]);
                return true;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function clearData(){
        session()->forget('insurance_data');
    }


    public function getInitialStep2(){
        try{
            $sessions = session('insurance_data');
            if(is_array($sessions)){
                //Validate Input Session
                $filter_validate = [
                    'input_brand'=>'required',
                    'input_model'=>'required',
                    'input_model_year'=>'required',
                    'input_sub_model'=>'required',
                ];
                $validator = Validator::make($sessions,$filter_validate);
                if ($validator->fails()) {
                    return redirect()->back()->withInput()->withErrors(['Invalid Input Data!']);
                }

                $insuranceRepository =  new InsuranceRepository();
                $vehicle_info = $insuranceRepository->getVehicleInfoRecord($sessions);
                if(is_array($vehicle_info) && sizeof($vehicle_info)>0){
                    //set vehicle record
                    $this->setVehicleInfo($vehicle_info['id']);
                    return $insuranceRepository->getDefaultInitialPlan();
                }
                return false;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function getInitialStep3(){
        try{
            $sessions = session('insurance_data');
            if(is_array($sessions)){

                $insuranceRepository =  new InsuranceRepository();
                $initial_plan = $insuranceRepository->getInitialStep3();
                return $initial_plan;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function getInitialStep4(){
        try{
            $sessions = session('insurance_data');
            if(is_array($sessions)){

                $insuranceRepository =  new InsuranceRepository();
                $initial_plan = $insuranceRepository->getInitialStep4();
                return $initial_plan;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function getInitialStep5(){
        try{
            $sessions = session('insurance_data');
            if(is_array($sessions)){
                $insuranceRepository =  new InsuranceRepository();
                $initial_plan = $insuranceRepository->getInitialStep5();
                return $initial_plan;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function getInitialThank($order_id){
        $insuranceRepository =  new InsuranceRepository();
        return $insuranceRepository->getInitialThank($order_id);
    }

    public function calculateModified($input){
        try{
            $response = [
                'status'=>false,
                'reason'=>null,
                'data'=>null
            ];
            if(session()->has('insurance_data')){
                if($this->verifyModifiedData($input)){
                    //Log::info("Verify Passed");
                    $sessions = session('insurance_data');
                    
                    $tax = Tax::where('id',1)->first();
                    $insuranceRepository =  new InsuranceRepository();
                    $basic_info = $insuranceRepository->getBasicInitial();
                    $input_data = $insuranceRepository->getInputData();
                    $pricing_info = null;
                    $addon_info = null;
                    $compulsory = null;
                    $taxi_row = [];
                    $theft_row = [];
                    $hb_row = [];
                    $carloss_row = [];
                    $flood_row = AddonFlood::where('id',1)->first();

                    // Initial Addition Data -----------------------------
                    if(!empty($input['input_travel']) && $input['input_travel']==1){
                        $params_addon = [
                            'si' => $input['input_taxi'],
                            'is_cctv' => $input['input_cctv'],
                        ];
                        if($input['input_define_name']!="0"){
                            $params_addon['define_name1'] = $input['input_define_name1'];
                            $params_addon['define_name2'] = !empty($input['input_define_name2'])?$input['input_define_name2']:null;
                        }
                        $taxi_row['gross_premium'] = $insuranceRepository->getTaxiPremium($params_addon);
                        $taxi_row['net_premium'] = $insuranceRepository->getTaxiPremium($params_addon,"net");
                        $taxi_row['stamp'] = $insuranceRepository->getTaxiPremium($params_addon,"stamp");
                        $taxi_row['vat'] = $insuranceRepository->getTaxiPremium($params_addon,"vat");
                        $taxi_row['max_rate'] = $insuranceRepository->getTaxiPremium($params_addon,"max_rate");
                    }
                    if(!empty($input['input_robbery']) && $input['input_robbery']==1){
                        $params_addon = [
                            'si' => $input['input_theft'],
                            'is_cctv' => $input['input_cctv'],
                        ];
                        if($input['input_define_name']!="0"){
                            $params_addon['define_name1'] = $input['input_define_name1'];
                            $params_addon['define_name2'] = !empty($input['input_define_name2'])?$input['input_define_name2']:null;
                        }
                        $theft_row['gross_premium'] = $insuranceRepository->getTheftPremium($params_addon);
                        $theft_row['net_premium'] = $insuranceRepository->getTheftPremium($params_addon,"net");
                        $theft_row['stamp'] = $insuranceRepository->getTheftPremium($params_addon,"stamp");
                        $theft_row['vat'] = $insuranceRepository->getTheftPremium($params_addon,"vat");
                        $theft_row['max_rate'] = $insuranceRepository->getTheftPremium($params_addon,"max_rate");
                    }
                    if(!empty($input['input_is_hb']) && $input['input_is_hb']==1){
                        $params_addon = [
                            'si' => $input['input_hb'],
                            'is_cctv' => $input['input_cctv'],
                        ];
                        if($input['input_define_name']!="0"){
                            $params_addon['define_name1'] = $input['input_define_name1'];
                            $params_addon['define_name2'] = !empty($input['input_define_name2'])?$input['input_define_name2']:null;
                        }
                        $hb_row['gross_premium'] = $insuranceRepository->getHbPremium($params_addon);
                        $hb_row['net_premium'] = $insuranceRepository->getHbPremium($params_addon,"net");
                        $hb_row['stamp'] = $insuranceRepository->getHbPremium($params_addon,"stamp");
                        $hb_row['vat'] = $insuranceRepository->getHbPremium($params_addon,"vat");
                        $hb_row['max_rate'] = $insuranceRepository->getHbPremium($params_addon,"max_rate");
                    }
                    if(!empty($input['input_carloss']) && $input['input_carloss']==1){
                        $params_addon = [
                            'ft_si' => $input['input_ft_si'],
                            'is_cctv' => $input['input_cctv'],
                        ];
                        if($input['input_define_name']!="0"){
                            $params_addon['define_name1'] = $input['input_define_name1'];
                            $params_addon['define_name2'] = !empty($input['input_define_name2'])?$input['input_define_name2']:null;
                        }
                        $carloss_row['gross_premium'] = $insuranceRepository->getCarlossPremium($params_addon);
                        $carloss_row['net_premium'] = $insuranceRepository->getCarlossPremium($params_addon,"net");
                        $carloss_row['cost'] = AddonCarLoss::where('ft_si',$input['input_ft_si']>100000?$input['input_ft_si']:100000)->first()->total_si;
                        $carloss_row['stamp'] = $insuranceRepository->getCarlossPremium($params_addon,"stamp");
                        $carloss_row['vat'] = $insuranceRepository->getCarlossPremium($params_addon,"vat");
                        $carloss_row['max_rate'] = $insuranceRepository->getCarlossPremium($params_addon,"max_rate");
                    }

                    // Initial Compulsory Data ---------------------------------
                    if(!empty($input['input_prb']) && $input['input_prb']==1){
                        $compulsory = Compulsory::where('motor_code_ac',$basic_info['vehicle_info']->mortor_code_ac)->first();
                    }

                    // Initial Pricing Record ---------------------------------
                    if(!empty($input['input_define_name']) && $input['input_define_name']=="1"){
                        $buffer_pricing = Pricing::where([
                            'coverage_id'=>$input_data['input_plan'],
                            'ft_si'=> intval($input['input_ft_si']),
                            'car_code'=>$basic_info['vehicle_info']->mortor_code_av,
                            'car_engine'=>$basic_info['vehicle_info']->cc,
                            'define_name'=>$input['input_define_name1'],
                            'cctv'=>intval($input['input_cctv']),
                            'garage_type'=>$input['input_garage'],
                            'deductible'=>empty($input['input_deductible'])?0:intval($input['input_deductible']),
                            'additional_coverage'=>empty($input['input_flood'])?'NO':'FLOOD'
                        ])->first();
                        if(!empty($input['input_define_name2'])){
                            $buffer_pricing2 = Pricing::where([
                                'coverage_id'=>$input_data['input_plan'],
                                'ft_si'=> intval($input['input_ft_si']),
                                'car_code'=>$basic_info['vehicle_info']->mortor_code_av,
                                'car_engine'=>$basic_info['vehicle_info']->cc,
                                'define_name'=>$input['input_define_name2'],
                                'cctv'=>intval($input['input_cctv']),
                                'garage_type'=>$input['input_garage'],
                                'deductible'=>empty($input['input_deductible'])?0:intval($input['input_deductible']),
                                'additional_coverage'=>empty($input['input_flood'])?'NO':'FLOOD'
                            ])->first();
                            if($buffer_pricing->gross_premium > $buffer_pricing2->gross_premium){
                                $pricing_info = $buffer_pricing;
                            }else{
                                $pricing_info = $buffer_pricing2;
                            }
                        }else{
                            $pricing_info = $buffer_pricing;
                        }
                    }else{
                        $pricing_info = Pricing::where([
                            'coverage_id'=>$input_data['input_plan'],
                            'ft_si'=> intval($input['input_ft_si']),
                            'car_code'=>$basic_info['vehicle_info']->mortor_code_av,
                            'car_engine'=>$basic_info['vehicle_info']->cc,
                            'define_name'=>'UNNAMED',
                            'cctv'=>intval($input['input_cctv']),
                            'garage_type'=>$input['input_garage'],
                            'deductible'=>empty($input['input_deductible'])?0:intval($input['input_deductible']),
                            'additional_coverage'=>empty($input['input_flood'])?'NO':'FLOOD'
                        ])->first();
                    }
                    // --------------------------------------------------

                    // Start Calculate ----------------------------------
                    if(!empty($pricing_info)){
                        //Log::info("pricing record: ".$pricing_info->id);
                        // Package Premium
                        $packageNetPremium = $pricing_info->net_premium;
                        $packageStamp = $pricing_info->stamp;
                        $packageVat = $pricing_info->vat;
                        $packageGrossPremium = $packageNetPremium + $packageStamp + $packageVat;

                        // Carloss Premium
                        $carlossNetPremium = !empty($carloss_row)?$carloss_row['net_premium']:0;
                        $carlossStamp = !empty($carloss_row)?$carloss_row['stamp']:0;
                        $carlossVat = !empty($carloss_row)?$carloss_row['vat']:0;
                        $carlossGrossPremium = $carlossNetPremium + $carlossStamp + $carlossVat;

                        // HB Premium
                        $hbNetPremium = !empty($hb_row)?$hb_row['net_premium']:0;
                        $hbStamp = !empty($hb_row)?$hb_row['stamp']:0;
                        $hbVat = !empty($hb_row)?$hb_row['vat']:0;
                        $hbGrossPremium = $hbNetPremium + $hbStamp + $hbVat;

                        // Theft Premium
                        $theftNetPremium = !empty($theft_row)?$theft_row['net_premium']:0;
                        $theftStamp = !empty($theft_row)?$theft_row['stamp']:0;
                        $theftVat = !empty($theft_row)?$theft_row['vat']:0;
                        $theftGrossPremium = $theftNetPremium + $theftStamp + $theftVat;

                        // Taxi Premium
                        $taxiNetPremium = !empty($taxi_row)?$taxi_row['net_premium']:0;
                        $taxiStamp = !empty($taxi_row)?$taxi_row['stamp']:0;
                        $taxiVat = !empty($taxi_row)?$taxi_row['vat']:0;
                        $taxiGrossPremium = $taxiNetPremium + $taxiStamp + $taxiVat;

                        //Result Premium
                        $resultNetPremium = $packageNetPremium + $carlossNetPremium + $hbNetPremium + $theftNetPremium + $taxiNetPremium;
                        $resultStamp = $packageStamp + $carlossStamp + $hbStamp + $theftStamp + $taxiStamp;
                        $resultVat = $packageVat + $carlossVat + $hbVat + $theftVat + $taxiVat;
                        $resultGrossPremium = $packageGrossPremium + $carlossGrossPremium + $hbGrossPremium + $theftGrossPremium + $taxiGrossPremium;

                        /******************* Calculate Backward *******************/
                        $backwardGrossPremium = $resultGrossPremium;
                        $backwardVat = round($backwardGrossPremium * $tax->vat / 107,2);
                        $backwardGrossRmVat = $backwardGrossPremium - $backwardVat;
                        $backwardStamp = ceil($backwardGrossRmVat * $tax->stamp / 100.4);
                        $backwardNetPremium = $backwardGrossPremium - $backwardVat - $backwardStamp;
                        $diffNetPremium = round($backwardNetPremium - $resultNetPremium,2);
                        $carlossNetPremium_bw = $carlossNetPremium;
                        $hbNetPremium_bw = $hbNetPremium;
                        $taxiNetPremium_bw = $taxiNetPremium;
                        $theftNetPremium_bw = $theftNetPremium;


                        /** Diff Adjust **/
                        if($diffNetPremium != 0){
                            $remaining = $diffNetPremium;
                            if(!empty($carloss_row)){
                                if(($carlossNetPremium_bw + $remaining) > $carloss_row['max_rate']){
                                    $diffRemaining = $carloss_row['max_rate'] - $carlossNetPremium_bw;
                                    $carlossNetPremium_bw += $diffRemaining;
                                    $remaining -= $diffRemaining;
                                }else{
                                    $carlossNetPremium_bw  += $remaining;
                                    $remaining = 0;
                                }
                            }
                            if(!empty($hb_row)){
                                if(($hbNetPremium_bw + $remaining) > $hb_row['max_rate']){
                                    $diffRemaining = $hb_row['max_rate'] - $hbNetPremium_bw;
                                    $hbNetPremium_bw += $diffRemaining;
                                    $remaining -= $diffRemaining;
                                }else{
                                    $hbNetPremium_bw  += $remaining;
                                    $remaining = 0;
                                }
                            }
                            if(!empty($taxi_row)){
                                if(($taxiNetPremium_bw + $remaining) > $taxi_row['max_rate']){
                                    $diffRemaining = $taxi_row['max_rate'] - $taxiNetPremium_bw;
                                    $taxiNetPremium_bw += $diffRemaining;
                                    $remaining -= $diffRemaining;
                                }else{
                                    $taxiNetPremium_bw  += $remaining;
                                    $remaining = 0;
                                }
                            }
                            if(!empty($theft_row)){
                                if(($theftNetPremium_bw + $remaining) > $theft_row['max_rate']){
                                    $diffRemaining = $theft_row['max_rate'] - $theftNetPremium_bw;
                                    $theftNetPremium_bw += $diffRemaining;
                                    $remaining -= $diffRemaining;
                                }else{
                                    $theftNetPremium_bw  += $remaining;
                                    $remaining = 0;
                                }
                            }
                        }

                        /***************** Calculate Forward *******************/
                        $newResultNetPremium = $packageNetPremium + $carlossNetPremium_bw + $hbNetPremium_bw + $theftNetPremium_bw + $taxiNetPremium_bw;
                        $forwardNetPremium = $newResultNetPremium;
                        $forwardStamp = ceil($forwardNetPremium*($tax->stamp/100));
                        $forwardVat = round(($forwardNetPremium + $forwardStamp) * ($tax->vat/100),2);
                        $forwardGrossPremium = $forwardNetPremium + $forwardStamp + $forwardVat;


                        /***************** Calculate Start **********************/
                        $sipr = $pricing_info->basic_premium_cover + $pricing_info->od_total_premium; // SIPR
                        if(!empty($input['input_flood'])){
                            $sipr -= $flood_row->net_premium;
                        }

                        $additionalPremium = $pricing_info->add_premium_cover;
                        $deduct = $pricing_info->deduct;
                        $fleet = $pricing_info->fleet;
                        $ncb = $pricing_info->ncb;
                        $cctv = $pricing_info->cctv_discount;
                        $direct = $pricing_info->direct;
                        $flood = !empty($input['input_flood'])?$flood_row->net_premium:0;
                        
                        //Calculate Premium No Coupon No Backward
                        $netPremium_no_coupon_no_bw = $additionalPremium + $deduct + $fleet + $ncb + $cctv + $flood
                                                + $carlossNetPremium + $hbNetPremium + $taxiNetPremium + $theftNetPremium
                                                + $direct + $sipr;
                        $stamp_no_coupon_no_bw = ceil($netPremium_no_coupon_no_bw * ($tax->stamp/100));
                        $vat_no_coupon_no_bw =  round(($netPremium_no_coupon_no_bw + $stamp_no_coupon_no_bw) * ($tax->vat/100),2);
                        $grossPremium_no_coupon_no_bw = $netPremium_no_coupon_no_bw + $stamp_no_coupon_no_bw + $vat_no_coupon_no_bw;

                        //Calculate Premium No Coupon With Backward
                        $netPremium_no_coupon = $additionalPremium + $deduct + $fleet + $ncb + $cctv + $flood
                                                + $carlossNetPremium_bw + $hbNetPremium_bw + $taxiNetPremium_bw + $theftNetPremium_bw
                                                + $direct + $sipr;
                        $stamp_no_coupon = ceil($netPremium_no_coupon * ($tax->stamp/100));
                        $vat_no_coupon =  round(($netPremium_no_coupon + $stamp_no_coupon) * ($tax->vat/100),2);
                        $grossPremium_no_coupon = $netPremium_no_coupon + $stamp_no_coupon + $vat_no_coupon;

                        // Calculate Premium With Coupon
                        $directDiscount = 0;
                        $promotionDiscount = 0;
                        if(!empty($input['input_promotion'])){
                            $promotion = Promotion::where('code',trim($input['input_promotion']))->first();
                            if(!empty($promotion)){
                                $promotionDiscount = $promotion->discount;
                            }
                        }
                        if($promotionDiscount > 0){
                            if(($carlossNetPremium + $hbNetPremium + $theftNetPremium + $taxiNetPremium) == 0){
                                $directDiscount = round(($additionalPremium + $deduct + $fleet + $ncb + $cctv + $sipr) * ($promotionDiscount / 100),0);
                            }else{
                                $directDiscount = round($netPremium_no_coupon_no_bw * ($promotionDiscount / 100),0);
                            }
                        }
                        $netPremium_coupon = $netPremium_no_coupon_no_bw - $directDiscount;
                        $stamp_coupon = ceil($netPremium_coupon * ($tax->stamp/100));
                        $vat_coupon =  round(($netPremium_coupon + $stamp_coupon) * ($tax->vat/100),2);
                        $grossPremium_coupon = $netPremium_coupon + $stamp_coupon + $vat_coupon;

                        // Result Discount
                        $netDiscount = $directDiscount;
                        $grossDiscount = $netDiscount!=0?$grossPremium_no_coupon - $grossPremium_coupon:0;

                        // Compulsory
                        $compulsoryGrossPremium = !empty($compulsory)?$compulsory->gross_premium:0; //เบี้ยพรบ
                        
                        //Result Payment
                        $paymentResult = $netDiscount!=0?($grossPremium_coupon + $compulsoryGrossPremium):( $grossPremium_no_coupon + $compulsoryGrossPremium) ; //$result_discount ติดลบ


                        /********Set Modified Session*********/

                        //จำนวนผู้ขับขี่
                        $define_name_amount = 0;
                        if(!empty($input['input_define_name']) && $input['input_define_name']=="1"){
                            if(!empty($input['input_define_name1'])) $define_name_amount++;
                            if(!empty($input['input_define_name2'])) $define_name_amount++;
                        }

                        $modyfied = [
                            'define_name_amount' => $define_name_amount,
                            'define_name1'=>!empty($input['input_define_name1'])?$input['input_define_name1']:'UNNAMED',
                            'define_name2'=>!empty($input['input_define_name2'])?$input['input_define_name2']:'UNNAMED',
                            'car_info_prefix' => !empty($input['input_info_prefix'])?$input['input_info_prefix']:null,
                            'car_info_licence' => !empty($input['input_info_licence'])?$input['input_info_licence']:null,
                            'car_info_province' => !empty($input['input_info_car_province'])?$input['input_info_car_province']:null,
                            'is_flood' => empty($input['input_flood'])?false:true,
                            'payment_result' => $paymentResult,
                            'pricing_info' => $pricing_info,
                            'start_insurance' => $input['input_start_insurance'],
                            'end_insurance' => $input['input_end_insurance'],
                            'flood_gross_premium' => !empty($input['input_flood'])?$flood_row->gross_premium:0,
                            'theft_gross_premium' => !empty($theft_row['gross_premium'])?$theft_row['gross_premium']:0, //ก่อนหักส่วนลด
                            'taxi_gross_premium' => !empty($taxi_row['gross_premium'])?$taxi_row['gross_premium']:0, //ก่อนหักส่วนลด
                            'hb_gross_premium' => !empty($hb_row['gross_premium'])?$hb_row['gross_premium']:0, //ก่อนหักส่วนลด
                            'carloss_gross_premium' => !empty($carloss_row['gross_premium'])?$carloss_row['gross_premium']:0, //ก่อนหักส่วนลด
                            'theft_cost' => (!empty($input['input_robbery']) && $input['input_robbery']==1)?floatval($input['input_theft']):0,
                            'taxi_cost' => (!empty($input['input_travel']) && $input['input_travel']==1)?floatval($input['input_taxi']):0,
                            'hb_cost' => (!empty($input['input_is_hb']) && $input['input_is_hb']==1)?floatval($input['input_hb']):0,
                            'carloss_cost' => !empty($carloss_row['cost'])?floatval($carloss_row['cost']):0,
                            'compulsory_net_premium' => !empty($compulsory)?$compulsory->net_premium:0,
                            'compulsory_stamp' => !empty($compulsory)?$compulsory->stamp:0,
                            'compulsory_vat' => !empty($compulsory)?$compulsory->vat:0,
                            'compulsory_gross_premium' => $compulsoryGrossPremium,
                            'compulsory_start' => !empty($input['input_start_compulsory'])?$input['input_start_compulsory']:null,
                            'compulsory_end' => !empty($input['input_end_compulsory'])?$input['input_end_compulsory']:null,
                            'net_discount' => $netDiscount,
                            'gross_discount' => $grossDiscount,
                            'promotion_id'=>!empty($promotion)?$promotion->id:null,
                            'insurance_gross_premium' => $packageGrossPremium, //ก่อนหักส่วนลด
                            'b_net_premium' => $forwardNetPremium,
                            'b_stamp' => $forwardStamp,
                            'b_vat' => $forwardVat,
                            'r_net_premium' => $netPremium_coupon, //หลังหักส่วนลด และ คำนวน BW
                            'r_stamp' => $stamp_coupon, //หลังหักส่วนลด และ คำนวน BW
                            'r_vat' => $vat_coupon, //หลังหักส่วนลด และ คำนวน BW
                            'r_carloss_net_premium' => $carlossNetPremium_bw,
                            'r_hb_net_premium' => $hbNetPremium_bw,
                            'r_theft_net_premium' => $theftNetPremium_bw,
                            'r_taxi_net_premium' => $taxiNetPremium_bw
                        ];


                        //Update Input Data
                        $input_data = $sessions['input_data'];
                        $input_data['car_info_prefix'] = !empty($input['input_info_prefix'])?$input['input_info_prefix']:null;
                        $input_data['car_info_province'] = !empty($input['input_info_car_province'])?$input['input_info_car_province']:null;
                        $input_data['car_info_licence'] = !empty($input['input_info_licence'])?$input['input_info_licence']:null;

                        $input_data['start_insurance'] = !empty($input['input_start_insurance'])?$input['input_start_insurance']:null;
                        $input_data['end_insurance'] = !empty($input['input_end_insurance'])?$input['input_end_insurance']:null;
                        $input_data['start_compulsory'] = !empty($input['input_start_compulsory'])?$input['input_start_compulsory']:null;
                        $input_data['end_compulsory'] = !empty($input['input_end_compulsory'])?$input['input_end_compulsory']:null;

                        $input_data['ft_si'] = !empty($input['input_ft_si'])?$input['input_ft_si']:null;
                        $input_data['garage_type'] = !empty($input['input_garage'])?$input['input_garage']:null;
                        $input_data['define_name'] = (!empty($input['input_define_name1']) && $input['input_define_name']!=0)?$input['input_define_name1']:null;
                        $input_data['define_name2'] = (!empty($input['input_define_name2']) && $input['input_define_name']!=0)?$input['input_define_name2']:null;
                        $input_data['cctv'] = !empty($input['input_cctv'])?$input['input_cctv']:0;
                        $input_data['deductible'] = !empty($input['input_deductible'])?$input['input_deductible']:0;
                        $input_data['is_flood'] = !empty($input['input_flood'])?$input['input_flood']:0;
                        $input_data['is_carloss'] = !empty($input['input_carloss'])?$input['input_carloss']:0;
                        $input_data['is_robbery'] = !empty($input['input_robbery'])?$input['input_robbery']:0;
                        $input_data['is_travel'] = !empty($input['input_travel'])?$input['input_travel']:0;
                        $input_data['is_hb'] = !empty($input['input_is_hb'])?$input['input_is_hb']:0;
                        $input_data['is_com'] = !empty($input['input_prb'])?$input['input_prb']:0;
                        $input_data['input_hb'] = $input_data['is_hb']!=0?$input['input_hb']:null;
                        $input_data['input_theft'] = $input_data['is_robbery']!=0?$input['input_theft']:null;
                        $input_data['input_taxi'] = $input_data['is_travel']!=0?$input['input_taxi']:null;
                        $input_data['promotion_code'] = !empty($input['input_promotion'])?$input['input_promotion']:0;
                        $input_data['is_personal'] = !empty($input['input_personal'])?$input['input_personal']:0;

                        $sessions['modyfied_data'] = $modyfied;
                        $sessions['input_data'] = $input_data;
                        session()->forget('insurance_data');
                        session(['insurance_data' => $sessions]);    

                        /**************************************/     
                        
                        $response['status'] = true;
                        $response['payment'] = number_format($paymentResult,2);

                    }else{
                        $response['status'] = false;
                        $response['reason'] = "Invalid Data!";
                    }
                }else{
                    $response['status'] = false;
                    $response['reason'] = "Invalid Data!";
                }
            }
            return $response;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    private function verifyModifiedData($input){
        $sessions = session('insurance_data');
        $insuranceRepository =  new InsuranceRepository();
        $input_data = $insuranceRepository->getInputData();
        $basic_info = $insuranceRepository->getBasicInitial();
        // Verify Start insurance
        $select_date = date('Y-m-d',strtotime($input['input_start_insurance']));
        $tomorrow = date("Y-m-d", strtotime("+1 day"));
        if($select_date < $tomorrow){
            //Log::info("Verify Start insurance Faild");
            return false;
        }
        //---------------
        // Verify FTSI
        $sql = Pricing::where([
            'coverage_id'=>$input_data['input_plan'],
            'car_code'=>$basic_info['vehicle_info']->mortor_code_av,
            'car_engine'=>$basic_info['vehicle_info']->cc,
            'define_name'=>($input_data['input_plan']==3?'UNNAMED':$basic_info['define_name']),
            'cctv'=>0,
            'garage_type'=>($input_data['input_plan']==3?'NO':'GENERAL'),
            'deductible'=>0, //แก้ไข'deductible'=>($sessions['input_plan']==3?0:2000)
            'additional_coverage'=>'NO'
        ]);
        if($input_data['input_plan']=="3"){
            $other_fi = $sql->groupBy('ft_si')->pluck('ft_si');
        }else{
            if($basic_info['vehicle_info']->used_car <=100000){
                $other_fi = $sql->where('ft_si','<=',100000)->pluck('ft_si');
            }else{
                $other_fi = $sql->where('ft_si','<',$basic_info['vehicle_info']->used_car)->pluck('ft_si');
            }
        }
        if(!in_array(intval($input['input_ft_si']),$other_fi->toArray())){
            //Log::info("Verify FTSI Faild");
            return false;
        }
        
        // Verify Define Name
        if(!empty($input['input_define_name']) && $input['input_define_name']=="1"){
            if(empty($input['input_define_name1'])){
                //Log::info("Verify input_define_name1 Faild");
                return false;
            }
        }

        
        // Vefify Dealer Garage
        if($input['input_garage']=="DEALER" || $input['input_garage']=="GENERAL"){
            if($input['input_garage']=="DEALER"){
                //Enable Dealer Garage
                $now_year = intval(date('Y'));
                $car_year =  intval($basic_info['vehicle_info']->year);
                if($car_year < ($now_year-2)){
                    //Log::info("Verify Dealer Garage Faild");
                    return false;
                }
            }
        }
        
        
        //-----------------
        // Verify Robber
        if(!empty($input['input_robbery']) && $input['input_robbery']=="1"){
            $theft = AddonTheft::pluck('sum_insured')->toArray();
            if(!in_array(intval($input['input_theft']),$theft)){
                //Log::info("Verify Robber Faild");
                return false;
            }
        }
        // Verify Taxi
        if(!empty($input['input_travel']) && $input['input_travel']=="1"){
            $taxi = AddonTaxi::pluck('sum_insured')->toArray();
            if(!in_array(intval($input['input_taxi']),$taxi)){
                //Log::info("Verify Taxi Faild");
                return false;
            }
        }
        // Verify Hb
        if(!empty($input['input_is_hb']) && $input['input_is_hb']=="1"){
            $hb = AddonHb::pluck('sum_insured')->toArray();
            if(!in_array(intval($input['input_hb']),$hb)){
                //Log::info("Verify Hb Faild");
                return false;
            }
        }
        // Verify Start Compulsory
        //Log::info("COMPULSORY:".!empty($input['input_prb'])?$input['input_prb']:'');
        if(!empty($input['input_prb']) && $input['input_prb']=="1"){
            $select_date = date('Y-m-d',strtotime($input['input_start_compulsory']));
            $tomorrow = date("Y-m-d", strtotime("+1 day"));
            if($select_date < $tomorrow){
                //Log::info("Verify Compulsory Faild");
                return false;
            }
        }
        //---------------

        // Verify Promotion
        if(!empty($input['input_promotion'])){
            $promotion = Promotion::where([
                'code'=>trim($input['input_promotion']),
                'status'=>1
            ])->where(function($query) {
                $query->whereDate('start_at', '<=', date('Y-m-d'));
                $query->orWhereNull('start_at');
            })->where(function($query){
                $query->whereDate('expire_at', '>=', date('Y-m-d'));
                $query->orWhereNull('expire_at');
            })->first();
            
            if(!empty($promotion)){
                $select_plan = $input_data['input_plan'];
                $support_plan = explode(",",$promotion->coverage_id);
                if(!in_array($select_plan,$support_plan)) return false;
            }else{
                //Log::info("Verify Promotion Faild");
                return false;   
            }
        }

        return true;
    }

    public function verifyBirthDate($input){
        $response = [
            'process' => true,
            'driver1_status' => true,
            'driver2_status' => true
        ];
        $sessions = session('insurance_data');
        if(empty($sessions['modyfied_data'])){
            $response['process'] = false;
        }else{
            $modyfied = $sessions['modyfied_data'];
            $period_age1 = $modyfied['define_name1'];
            $period_age2 = $modyfied['define_name2'];
            if($period_age1!="UNNAMED" && isset($input['driver1_age_value'])){
                if($period_age1=="50UP"){
                    $string_buffer = str_replace("UP","",$period_age1);
                    $start_at = intval($string_buffer);
                    $correct_date = correctDate($input['input_driver1_birth']);

                    $dtz = new DateTimeZone("Asia/Bangkok");
                    $d1 = new DateTime(); 
                    $d1->setTimezone($dtz);
                    $d2 = new DateTime($correct_date,$dtz);
                    $diff = $d2->diff($d1);

                    if($start_at>=$diff->y){
                        $response['driver1_status'] = false;
                    }
                }else{
                    $string_buffer = str_replace("Y","",$period_age1);
                    $between_date = explode("-",$string_buffer);
                    $start_at = intval($between_date[0]);
                    $end_at = intval($between_date[1]);
                    $correct_date = correctDate($input['input_driver1_birth']);

                    $dtz = new DateTimeZone("Asia/Bangkok");
                    $d1 = new DateTime(); 
                    $d1->setTimezone($dtz);
                    $d2 = new DateTime($correct_date,$dtz);
                    $diff = $d2->diff($d1);

                    if($start_at>$diff->y || $end_at<$diff->y){
                        $response['driver1_status'] = false;
                    }
                }
            }
            if($period_age2!="UNNAMED" && isset($input['driver2_age_value'])){
                if($period_age2=="50UP"){
                    $string_buffer = str_replace("UP","",$period_age2);
                    $start_at = intval($string_buffer);
                    $correct_date = correctDate($input['input_driver2_birth']);

                    $dtz = new DateTimeZone("Asia/Bangkok");
                    $d1 = new DateTime(); 
                    $d1->setTimezone($dtz);
                    $d2 = new DateTime($correct_date,$dtz);
                    $diff = $d2->diff($d1);

                    if($start_at<$diff->y){
                        $response['driver2_status'] = false;
                    }
                }else{
                    $string_buffer = str_replace("Y","",$period_age2);
                    $between_date = explode("-",$string_buffer);
                    $start_at = intval($between_date[0]);
                    $end_at = intval($between_date[1]);
                    $correct_date = correctDate($input['input_driver2_birth']);
                    
                    $dtz = new DateTimeZone("Asia/Bangkok");
                    $d1 = new DateTime(); 
                    $d1->setTimezone($dtz);
                    $d2 = new DateTime($correct_date,$dtz);
                    $diff = $d2->diff($d1);
                    
                    if($start_at>$diff->y || $end_at<$diff->y){
                        $response['driver2_status'] = false;
                    }
                }
            }
        }

        return $response;
    }

    public function setInsuranceInfomation($data){
        //try{
            if(session()->has('insurance_data')){
                $sessions = session('insurance_data');
                session()->forget('insurance_data');

                $sessions['information']= $data;
                session(['insurance_data' => $sessions]);    
                return true;
            }
            return false;
        //}catch (\Exception $e) {
        //    Log::error($e->getMessage());
        //    return false;
        //}
    }

    public function createOrder(){
        $this->checkRunningRecord();
        DB::beginTransaction();
        $order_result = null;
        try{  
            if(session()->has('insurance_data')){
                $sessions = session('insurance_data');
                $modyfied = $sessions['modyfied_data'];
                $pricing = $modyfied['pricing_info'];
                $information = $sessions['information'];
                $coverage = Coverage::where("id",$pricing->coverage_id)->first();
                $vehicle_info = $this->activeTable['vehicle']==1?Vehicle::where('id',$sessions['vehicleInfoRecord'])->first():VehicleSecond::where('id',$sessions['vehicleInfoRecord'])->first();
                $driver_main_id = null;
                $driver1_id = null;
                $driver2_id = null; 

                //Create Driver Info --------------------
                $driver_main = [
                    'name' => preg_replace('/\s+/', '', $information['info_main_name']),
                    'lastname' => preg_replace('/\s+/', '', $information['info_main_lastname']),
                    'idcard' => $information['info_main_id_card'],
                    'birth_date' => $information['info_main_birthdate_value'],
                    'gender' => $information['info_main_gender'],
                    'address' => $information['info_main_address'],
                    'province' => $information['info_main_province'],
                    'district' => $information['info_main_district'],
                    'subdistrict' => $information['info_main_subdistrict'],
                    'province_name' => $information['info_main_province_name'],
                    'district_name' => $information['info_main_district_name'],
                    'subdistrict_name' => $information['info_main_subdistrict_name'],
                    'province_name_th' => $information['info_main_province_name_th'],
                    'district_name_th' => $information['info_main_district_name_th'],
                    'subdistrict_name_th' => $information['info_main_subdistrict_name_th'],
                    'postalcode' => $information['info_main_postcode'],
                    'tel' => $information['info_main_tel'],
                    'email' => $information['info_main_email'],
                ];
                $driver_main_id = DriverInfo::create($driver_main)->id;
                if($modyfied['define_name_amount']>0){
                    if(!empty($information['info_driver1_name'])){
                        $driver1 = [
                            'name' => preg_replace('/\s+/', '', $information['info_driver1_name']),
                            'lastname' => preg_replace('/\s+/', '', $information['info_driver1_lastname']),
                            'idcard' => $information['info_driver1_id_card'],
                            'licence' => $information['info_driver1_licence'], //null
                            'birth_date' => $information['info_driver1_birthdate_value'],
                            'gender' => $information['info_driver1_gender'],
                        ];
                        $driver1_id = DriverInfo::create($driver1)->id;
                    }
                    if(!empty($information['info_driver2_name'])){
                        $driver2 = [
                            'name' => preg_replace('/\s+/', '', $information['info_driver2_name']),
                            'lastname' => preg_replace('/\s+/', '', $information['info_driver2_lastname']),
                            'idcard' => $information['info_driver2_id_card'],
                            'licence' => $information['info_driver2_licence'], //null
                            'birth_date' => $information['info_driver2_birthdate_value'],
                            'gender' => $information['info_driver2_gender'],
                        ];
                        $driver2_id = DriverInfo::create($driver2)->id;
                    }
                }
                // End Create Driver Info -----------------
                
                // Create Vehicle Info --------------------
                $vehicle_info_id = VehicleInfo::create([
                    'brand' => $vehicle_info->brand_name()->first()->name,
                    'model' => $vehicle_info->model_name()->first()->name,
                    'year' => $vehicle_info->year,
                    'body_type' => $vehicle_info->body_type,
                    'model_type' => $vehicle_info->model_type,
                    'mortor_code_av' => $vehicle_info->mortor_code_av,
                    'mortor_code_ac' => $vehicle_info->mortor_code_ac,
                    'cc' => $vehicle_info->cc,
                    'tons' => $vehicle_info->tons,
                    'car_seat' => $vehicle_info->car_seat,
                    'driver_passenger' => $vehicle_info->driver_passenger,
                    'red_plate' => $vehicle_info->red_plate,
                    'used_car' => $vehicle_info->used_car,
                    'car_age' => $vehicle_info->car_age,
                ])->id;
                //End Create Vehicle Info -----------------


                $order_expire = date('Y-m-d H:i:s', strtotime('+1 day',time()));
                $state = "run";
                do{
                    /* Get Runing Number */
                    $running_year = date('Y');
                    $running_month = date('m');
                    $cutoff = Cutoff::where([
                        'years' => $running_year,
                        'months' => $running_month
                    ])->first();
                    if(!empty($cutoff)){
                        if(date('Y-m-d') > date('Y-m-d',strtotime($cutoff->cutoff_date))){
                            $running_month+=1;
                            if($running_month>12){
                                $running_month = 1;
                                $running_year+=1;

                            }
                        }
                    }

                    $number_record = RunningNumber::where('year',$running_year)->lockForUpdate()->first();
                    $now_running = intval($number_record->order);
                    $order_number = "ODA1-".$running_year."-".$running_month."-".str_pad(++$now_running,7,"0",STR_PAD_LEFT);
                    $addTime = date('H:i:s');
                    /* -------------------*/

                    /* Create Order */
                    $data = [
                        'order_number' => $order_number,
                        'order_expire' => $order_expire,
                        'payment_result' => $modyfied['payment_result'],
                        'is_otp' => $sessions['otp_verify'], 
                        'status'=>'WAITING',
                        'gross_premium'=> $modyfied['insurance_gross_premium'], //ก่อนหักส่วนลด
                        'theft_gross_premium'=>$modyfied['theft_gross_premium'], //ก่อนหักส่วนลด
                        'taxi_gross_premium'=>$modyfied['taxi_gross_premium'], //ก่อนหักส่วนลด
                        'hb_gross_premium'=>$modyfied['hb_gross_premium'], //ก่อนหักส่วนลด
                        'carloss_gross_premium'=>$modyfied['carloss_gross_premium'], //ก่อนหักส่วนลด
                        'theft_net_premium'=>$modyfied['r_theft_net_premium'], //หลัง ฺBackward
                        'taxi_net_premium'=>$modyfied['r_taxi_net_premium'], //หลัง ฺBackward
                        'hb_net_premium'=>$modyfied['r_hb_net_premium'], //หลัง ฺBackward
                        'carloss_net_premium'=>$modyfied['r_carloss_net_premium'], //หลัง ฺBackward
                        'net_discount' => $modyfied['net_discount'],
                        'discount'=>abs($modyfied['gross_discount']),
                        'insurance_net_premium'=>$modyfied['r_net_premium'], //หลังหักส่วนลด
                        'stamp' => $modyfied['r_stamp'],  //หลังหักส่วนลด
                        'vat' => $modyfied['r_vat'],  //หลังหักส่วนลด
                        'b_net_premium' => $modyfied['b_net_premium'],
                        'b_stamp' => $modyfied['b_stamp'],
                        'b_vat' => $modyfied['b_vat'],
                        'based_premium' => $pricing->based_prem,
                        'od_based_premium' => $pricing->od_based_prem,
                        'basic_premium_cover' => $pricing->basic_premium_cover,
                        'additional_premium_cover' => $pricing->add_premium_cover,
                        'fleet_percent' => $pricing->fleet_percent,
                        'fleet' => $pricing->fleet,
                        'ncb_percent' => $pricing->ncb_percent,
                        'ncb' => $pricing->ncb,
                        'od_si' => $pricing->od_si,
                        'od_total_premium' => $pricing->od_total_premium,
                        'deduct' => $pricing->deduct,
                        'cctv_percent' => $pricing->cctv_discount_percent,
                        'cctv_discount' => $pricing->cctv_discount,
                        'direct_percent' => $pricing->direct_percent,
                        'direct' => $pricing->direct,
                        'flood_net_premium' => $pricing->flood_net_premium,
                        'flood_stamp' => $pricing->flood_stamp,
                        'flood_vat' => $pricing->flood_vat,
                        'flood_gross_premium'=>$pricing->flood_gross_premium,
                        'promotion_id'=>$modyfied['promotion_id'],
                        'car_licence'=>$information['info_car_licence'],
                        'car_province'=>$information['info_car_province_id'],
                        'car_chassis_number'=>$information['info_car_chassis'],
                        'car_cctv'=>$pricing->cctv,
                        'vehicle_info' => $vehicle_info_id,
                        'insurance_plan_id'=>$pricing->coverage_id,
                        'insurance_plan_name'=>$pricing->coverage_name,
                        'insurance_start'=>$modyfied['start_insurance']." ".$addTime,
                        'insurance_expire'=>$modyfied['end_insurance']." ".$addTime,
                        'insurance_ft_si'=>$pricing->ft_si,
                        'insurance_garage_type'=>$pricing->garage_type,
                        'insurance_deduct'=>$pricing->deductible,
                        'insurance_person_damage_once'=>$coverage->person_damage_once,
                        'insurance_person_damage_person'=>$coverage->person_damage_person,
                        'insurance_stuff_damage'=>$coverage->stuff_damage,
                        'insurance_death_disabled'=>$coverage->death_disabled,
                        'insurance_medical_fee'=>$coverage->medical_fee,
                        'insurance_bail_driver'=>$coverage->bail_driver,
                        'insurance_driver_amount'=>$modyfied['define_name_amount'],
                        'compulsory_net_premium'=>$modyfied['compulsory_net_premium'],
                        'compulsory_stamp'=>$modyfied['compulsory_stamp'],
                        'compulsory_vat'=>$modyfied['compulsory_vat'],
                        'compulsory_start'=>$modyfied['compulsory_net_premium']!=0?$modyfied['compulsory_start']." ".$addTime:null,
                        'compulsory_expire'=>$modyfied['compulsory_net_premium']!=0?$modyfied['compulsory_end']." ".$addTime:null,
                        'addon_theft'=>!empty($modyfied['theft_cost'])?$modyfied['theft_cost']:0,
                        'addon_taxi'=>!empty($modyfied['taxi_cost'])?$modyfied['taxi_cost']:0,
                        'addon_hb'=>!empty($modyfied['hb_cost'])?$modyfied['hb_cost']:0,
                        'addon_carloss'=>!empty($modyfied['carloss_cost'])?$modyfied['carloss_cost']:0,
                        'main_driver'=>$driver_main_id,
                        'driver1'=>$driver1_id, //null
                        'driver2'=>$driver2_id, //null
                        'document_path_personal_id'=>$information['info_path_personal_id'], 
                        'document_path_car_licence'=>$information['info_path_car_licence'],
                        'document_path_cctv_inside'=>!empty($information['info_path_cctv_inside'])?$information['info_path_cctv_inside']:null, //null
                        'document_path_cctv_outside'=>!empty($information['info_path_cctv_outside'])?$information['info_path_cctv_outside']:null, //null
                        'document_path_driver1_licence'=>!empty($information['info_path_driver1_licence'])?$information['info_path_driver1_licence']:null, //null
                        'document_path_driver2_licence'=>!empty($information['info_path_driver2_licence'])?$information['info_path_driver2_licence']:null, //null
                        'is_advice'=>$information['info_is_advice']=="YES"?1:0,
                        'is_post'=>$information['info_is_post']=="YES"?1:0,
                    ];
                    $set_response = $this->setOrder($data);
                    if($set_response['status']==true){
                        $number_record->order = $now_running;
                        $number_record->save();
                        $state = "stop";
                        $order_result = $set_response['order_id'];
                    }else{
                        DB::rollBack();
                    }
                }while($state=="run");
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $order_result;
    }

    private function setOrder($data){
        $result = [
            'status' => false,
            'order_id' => null
        ];
        try{
            $id = Order::create($data)->id;
            if(!empty($id)){
                $result['status'] = true;
                $result['order_id'] = $id;
            }else{
                $result['status'] = false;
            }
        }catch (\Exception $e) {
            $result['status'] = false;
        }
        return $result;
    }

    public function createPayment($paymentData,$order){
        $this->checkRunningRecord();
        DB::beginTransaction();
        try{  
            $payment_expire = date('Y-m-d H:i:s', strtotime('+1 day',time()));
            $state = "run";
            do{
                /* Get Runing Number */
                $running_year = date('Y');
                $running_month = date('m');
                $cutoff = Cutoff::where([
                    'years' => $running_year,
                    'months' => $running_month
                ])->first();
                if(!empty($cutoff)){
                    if(date('Y-m-d') > date('Y-m-d',strtotime($cutoff->cutoff_date))){
                        $running_month+=1;
                        if($running_month>12){
                            $running_month = 1;
                            $running_year+=1;

                        }
                    }
                }

                $number_record = RunningNumber::where('year',$running_year)->lockForUpdate()->first();
                $now_running = intval($number_record->payment);
                $payment_no = "PM-".$running_year."-".$running_month."-".str_pad(++$now_running,7,"0",STR_PAD_LEFT);
                /* -------------------*/

                /* Create Payment */
                $data = [
                    'order_number' => $order->order_number,
                    'payment_no' => $payment_no,
                    'merchant_id' => $paymentData['merchant_id'],
                    'currency_code' => $paymentData['currency'],
                    'description' => $paymentData['payment_description'],
                    'amount' => $order->payment_result,
                    'status' => 'PROCESSING',
                    'status_detail' => 'อยู่ในขั้นตอนดำเนินการ ยังไม่มีการตอบกลับ',
                    'expire_date' => $payment_expire
                ];

                $set_response = $this->setPayment($data);
                if($set_response['status']==true){
                    $running_record = RunningNumber::where('year',date('Y'))->first();
                    $running_record->payment = $now_running;
                    $running_record->save();
                    $state = "stop";
                    $payment_result = $set_response['payment_id'];
                }else{
                    DB::rollBack();
                }
            }while($state=="run");
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $payment_result;
    }

    private function setPayment($data){
        $result = [
            'status' => false,
            'payment_id' => null
        ];
        try{
            $id = Payment::create($data)->id;
            if(!empty($id)){
                $result['status'] = true;
                $result['payment_id'] = $id;
            }else{
                $result['status'] = false;
            }
        }catch (\Exception $e) {
            $result['status'] = false;
        }
        return $result;
    }

    public function createReceipt($data){
        if(!empty($data)){
            return Receipt::create($data)->id;
        }
        return false;
    }

    public function createPolicy($order_number){
        $order = Order::where("order_number",$order_number)->first();
        if(!empty($order)){
            $this->checkRunningRecord();
            $this->checkMonthRunningRecord();
            $policy_result = null;
            DB::beginTransaction();
            try{
                $state = "run";
                do{
                    /* Get Runing Number */
                    $running_year = date('Y');
                    $running_month = date('m');
                    $cutoff = Cutoff::where([
                        'years' => $running_year,
                        'months' => $running_month
                    ])->first();
                    if(!empty($cutoff)){
                        if(date('Y-m-d') > date('Y-m-d',strtotime($cutoff->cutoff_date))){
                            $running_month+=1;
                            if($running_month>12){
                                $running_month = 1;
                                $running_year+=1;

                            }
                        }
                    }
                    $number_record = RunningNumber::where('year',$running_year)->first();
                    $month_number_record = MonthRunningNumber::where([
                        'year' => $running_year,
                        'month' => $running_month
                    ])->first();

                    $policy_running = $order->insurance_plan_id==3?intval($number_record->policy_av3):intval($number_record->policy_av5);
                    $policy_runing_number = str_pad(++$policy_running,7,"0",STR_PAD_LEFT);
                    //$misc_running = intval($number_record->misc);
                    //$misc_running_number = str_pad(++$misc_running,7,"0",STR_PAD_LEFT);
                    $tax_running = intval($month_number_record->tax);
                    $tax_running_number = str_pad(++$tax_running,7,"0",STR_PAD_LEFT);
                    $com_running = intval($number_record->com);
                    $com_running_number = str_pad(++$com_running,7,"0",STR_PAD_LEFT);
                    $com_tax_running = intval($month_number_record->com_tax);
                    $com_tax_running_number = str_pad(++$com_tax_running,7,"0",STR_PAD_LEFT);
                    

                    $policy_number = $this->generatePolicyNumber($order->insurance_plan_id,$policy_runing_number,$running_year,$running_month);
                    $tax_note_number = $this->generateTaxNoteNumber($tax_running_number,$running_year,$running_month);
                    $policy_com_number = $this->generatePolicyNumber("com",$com_running_number,$running_year,$running_month);
                    $com_tax_note_number = $this->generateTaxNoteNumber($com_tax_running_number,$running_year,$running_month);

                    if($policy_number!=false){
                        $data = [
                            'policy_number' => $policy_number,
                            'tax_note_number' => $tax_note_number,
                            'order_id'=>$order->id,
                            'status'=>'NORMAL',
                            'car_licence'=>$order->car_licence,
                            'car_province'=>$order->car_province,
                            'car_chassis_number'=>$order->car_chassis_number,
                            'car_cctv'=>$order->car_cctv,
                            'vehicle_info' => $order->vehicle_info,
                            'owner' => $order->main_driver,
                            'insurance_plan_id'=>$order->insurance_plan_id,
                            'insurance_plan_name'=>$order->insurance_plan_name,
                            'insurance_start'=>$order->insurance_start,
                            'insurance_expire'=>$order->insurance_expire,
                            'insurance_ft_si'=>$order->insurance_ft_si,
                            'insurance_garage_type'=>$order->insurance_garage_type,
                            'insurance_deduct'=>$order->insurance_deduct,
                            'insurance_person_damage_once'=>$order->insurance_person_damage_once,
                            'insurance_person_damage_person'=>$order->insurance_person_damage_person,
                            'insurance_stuff_damage'=>$order->insurance_stuff_damage,
                            'insurance_death_disabled'=>$order->insurance_death_disabled,
                            'insurance_medical_fee'=>$order->insurance_medical_fee,
                            'insurance_bail_driver'=>$order->insurance_bail_driver,
                            'insurance_driver_amount'=>$order->insurance_driver_amount,
                            'compulsory_start'=>$order->compulsory_start,
                            'compulsory_expire'=>$order->compulsory_expire,
                            'addon_theft'=>$order->addon_theft,
                            'addon_taxi'=>$order->addon_taxi,
                            'addon_hb'=>$order->addon_hb,
                            'addon_carloss'=>$order->addon_carloss,
                            'document_path_personal_id'=>$order->document_path_personal_id,
                            'document_path_car_licence'=>$order->document_path_car_licence,
                            'document_path_cctv_inside'=>$order->document_path_cctv_inside,
                            'document_path_cctv_outside'=>$order->document_path_cctv_outside,
                            'document_path_driver1_licence'=>$order->document_path_driver1_licence,
                            'document_path_driver2_licence'=>$order->document_path_driver2_licence,
                            'locale'=> App::getLocale(),
                            'is_advice'=>$order->is_advice,
                            'is_post'=>$order->is_post,
                            
                        ];
                        //Create Compulsory Policy
                        if(!empty($order->compulsory_net_premium)){
                            $data['policy_com_number'] = $policy_com_number;
                            $data['com_tax_note_number'] = $com_tax_note_number;
                        }
                        $set_response = $this->setPolicy($data,$order);

                        //Update Runing Record
                        if($set_response['status']==true){
                            if($order->insurance_plan_id==3){
                                $number_record->policy_av3 = $policy_running;
                            }else{
                                $number_record->policy_av5 = $policy_running;
                            }
                            
                            if(!empty($data['policy_com_number'])){
                                $number_record->com = $com_running;
                                $month_number_record->com_tax = $com_tax_running;
                            }

                            $month_number_record->tax = $tax_running;
                            $month_number_record->save();
                            $number_record->save();

                            $state = "stop";
                            $policy_result = $set_response['policy_id'];
                        }else{
                            DB::rollBack();
                        }
                    }
                }while($state=="run");
            }catch (\Exception $e) {
                Log::error($e->getMessage());
                return false;
            }
            DB::commit();
            return $policy_result;
        }else{
            return false;
        }
    }

    private function setPolicy($data,$order){
        $result = [
            'status' => false,
            'policy_id' => null
        ];
        try{
            $id = Policy::create($data)->id;
            if(!empty($id)){
                // Create Endorse
                $endorse = [
                    'policy_id' => $id,
                    'endorse_no' => '00',
                    'owner' => $order->main_driver,
                    'driver1' => $order->driver1,
                    'driver2' => $order->driver2,
                ];
                Endorse::create($endorse);
                $result['status'] = true;
                $result['policy_id'] = $id;
            }else{
                $result['status'] = false;
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            $result['status'] = false;
        }
        return $result;
    }

    public function generatePDF($policy_id,$endorse_no = "00"){
        
            $policy = Policy::where('id',$policy_id)->first();
            $endorse = $policy->endorse()->first();
            $main_driver_info = $endorse->owner()->first();
            $directory = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();

            // Policy
            if(!empty($policy->policy_number)){
                if($path = $this->generatePolicyPDF($policy,$endorse_no)){ // Policy
                    /* Encryoted */
                    $file_path = $directory.$path;
                    $this->pdfEncryptLocal($main_driver_info,$file_path);

                    $endorse->policy_document_path = $path;
                    $endorse->save();           
                }
                if($path = $this->generateOriginalReceiptPDF($policy,$endorse_no)){ //Original Copy
                    /* Encryoted */
                    $file_path = $directory.$path;
                    $this->pdfEncryptLocal($main_driver_info,$file_path);

                    $endorse->original_invoice_path = $path;
                    $endorse->save();  
                }
                if($path = $this->generateInvoiceCopyPDF($policy,$endorse_no)){ //Invoice Copy
                    /* Encryoted */
                    $file_path = $directory.$path;
                    $this->pdfEncryptLocal($main_driver_info,$file_path);

                    $endorse->invoice_copy_path = $path;
                    $endorse->save();  
                }
                if($path = $this->generateReceiptCopyPDF($policy,$endorse_no)){ //Receipt Copy
                    /* Encryoted */
                    $file_path = $directory.$path;
                    $this->pdfEncryptLocal($main_driver_info,$file_path);

                    $endorse->receipt_copy_path = $path;
                    $endorse->save(); 
                }
            }

            if(!empty($policy->policy_com_number)){
                //get Sticker No
                $sticker_no = date('YmdHis');
                //save Sticker No In Policy
                $policy->sticker_no = $sticker_no;
                $policy->save();
                if($path = $this->generateCompulsoryPDF($policy,$sticker_no)){ // Policy
                    /* Encryoted */
                    $file_path = $directory.$path;
                    $this->pdfEncryptLocal($main_driver_info,$file_path);

                    $endorse->compulsory_document_path = $path;
                    $endorse->save();           
                }
            }
            
            
            // // Misc เดี๋ยวต้อง ไปรวมกับ Policy แล้ว ทำไฟล์ Receipt ใหม่
            // if(!empty($policy->policy_addon_number)){
            //     if($path = $this->generateMiscPDF($policy,$endorse_no)){ // Policy
            //         /* Encryoted */
            //         $file_path = $directory.$path;
            //         //$this->pdfEncryptLocal($main_driver_info,$file_path);

            //         $endorse->misc_policy_document_path = $path;
            //         $endorse->save();           
            //     }
            //     if($path = $this->generateOriginalReceiptMiscPDF($policy,$endorse_no)){ // Policy
            //         /* Encryoted */
            //         $file_path = $directory.$path;
            //         //$this->pdfEncryptLocal($main_driver_info,$file_path);

            //         $endorse->misc_original_invoice_path = $path;
            //         $endorse->save();           
            //     }
            //     if($path = $this->generateInvoiceCopyMiscPDF($policy,$endorse_no)){ //Invoice Copy
            //         /* Encryoted */
            //         $file_path = $directory.$path;
            //         //$this->pdfEncryptLocal($main_driver_info,$file_path);

            //         $endorse->misc_invoice_copy_path = $path;
            //         $endorse->save();  
            //     }
            //     if($path = $this->generateReceiptCopyMiscPDF($policy,$endorse_no)){ //Receipt Copy
            //         /* Encryoted */
            //         $file_path = $directory.$path;
            //         //$this->pdfEncryptLocal($main_driver_info,$file_path);

            //         $endorse->misc_receipt_copy_path = $path;
            //         $endorse->save(); 
            //     }
            // }
    }


    private function generatePolicyPDF($data,$endorse_no){
        ini_set('memory_limit', '-1');
        header('Content-Type: text/html; charset=UTF-8');
        $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
        $pdfMerge = new \LynX39\LaraPdfMerger\PdfManage;
        $unsset_file = [];
        $order = $data->order()->first();
        if($data->insurance_plan_id==1){ /*************** 2+ ***********************/

            //Schedule File 
            $pdf = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data])->setPaper('A4');
            $mt_schedule_name = getUniqueFilename($directory_path."/buffer/","pdf");
            Storage::disk('pdf')->put('/buffer/'.$mt_schedule_name, $pdf->output()); //Save
            $mt_schedule_file = $directory_path."/buffer/".$mt_schedule_name;
            $pdfMerge->addPDF($mt_schedule_file, 'all');
            $pdf = null;
            array_push($unsset_file,$mt_schedule_name);
            //----------------

            //Wordding File 
            $wordingList = ['/plan/2.pdf','/plan/3.pdf','/plan/4.pdf','/plan/5.pdf','/plan/6.pdf','/plan/7.pdf','/plan/8.pdf','/plan/9.pdf'];
            $this->mergeFilePdf($pdfMerge,$wordingList);
            //---------------

            //Misc File
            $this->mergeMiscPdf($pdfMerge,$order);
            //---------------

            //Create Uniqe Directory
            $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
            Storage::disk('pdf')->makeDirectory('/user/policy/'.$desc_directory);
            //----------------------'

            //Merge File
            $desc_path = "/user/policy/".$desc_directory."/";
            $desc_file  = getUniqueFilename($desc_path,"pdf");
            $desc_full_path = $directory_path.$desc_path.$desc_file;
            $pdfMerge->merge('file',$desc_full_path, 'P');
            //---------------------
 
            //Remove Buffer File
            foreach($unsset_file as $file){
                Storage::disk('pdf')->delete('/buffer/'.$file);
            }
            //---------------------

            //Save Output
            return $desc_path.$desc_file;

        }else if($data->insurance_plan_id==2){ /***************** 3+ ***********************/
            
            $pdf = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data])->setPaper('A4');
            // Schedule File 
            $mt_schedule_name = getUniqueFilename($directory_path."/buffer/","pdf");
            Storage::disk('pdf')->put('/buffer/'.$mt_schedule_name, $pdf->output()); //Save
            $mt_schedule_file = $directory_path."/buffer/".$mt_schedule_name;
            $pdfMerge->addPDF($mt_schedule_file, 'all');
            $pdf = null;
            array_push($unsset_file,$mt_schedule_name);
            //----------------
            
            //Wordding File 
            $wordingList = ['/plan/2.pdf','/plan/3.pdf','/plan/5.pdf','/plan/6.pdf','/plan/7.pdf','/plan/8.pdf','/plan/9.pdf'];
            $this->mergeFilePdf($pdfMerge,$wordingList);
            //---------------

            //Misc File
            $this->mergeMiscPdf($pdfMerge,$order);
            //---------------

            //Create Uniqe Directory
            $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
            Storage::disk('pdf')->makeDirectory('/user/policy/'.$desc_directory);
            //----------------------

            //Merge File
            $desc_path = "/user/policy/".$desc_directory."/";
            $desc_file  = getUniqueFilename($desc_path,"pdf");
            $desc_full_path = $directory_path.$desc_path.$desc_file;
            $pdfMerge->merge('file',$desc_full_path, 'P');
            //---------------------

            //Remove Buffer File
            foreach($unsset_file as $file){
                Storage::disk('pdf')->delete('/buffer/'.$file);
            }
            //---------------------

            //Save Output
            return $desc_path.$desc_file;

        }else{ /***************** Type3 *********************/
            
            $pdf = PDF::loadView('admin.pdf.mt-schedule_3',['init'=>$data])->setPaper('A4');
            // Schedule File 
            $mt_schedule_name = getUniqueFilename($directory_path."/buffer/","pdf");
            Storage::disk('pdf')->put('/buffer/'.$mt_schedule_name, $pdf->output()); //Save
            $mt_schedule_file = $directory_path."/buffer/".$mt_schedule_name;
            $pdfMerge->addPDF($mt_schedule_file, 'all');
            $pdf = null;
            array_push($unsset_file,$mt_schedule_name);
            //----------------
            
            //Wordding File 
            $wordingList = ['/plan/2.pdf','/plan/3.pdf','/plan/5.pdf','/plan/6.pdf','/plan/7.pdf','/plan/8.pdf','/plan/9.pdf'];
            $this->mergeFilePdf($pdfMerge,$wordingList);
            //---------------

            //Create Uniqe Directory
            $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
            Storage::disk('pdf')->makeDirectory('/user/policy/'.$desc_directory);
            //----------------------

            //Merge File
            $desc_path = "/user/policy/".$desc_directory."/";
            $desc_file  = getUniqueFilename($desc_path,"pdf");
            $desc_full_path = $directory_path.$desc_path.$desc_file;
            $pdfMerge->merge('file',$desc_full_path, 'P');
            //----------------------

            //Remove Buffer File
            foreach($unsset_file as $file){
                Storage::disk('pdf')->delete('/buffer/'.$file);
            }

            //Save Output
            return $desc_path.$desc_file;
        }

        
    }

    private function generateOriginalReceiptPDF($data,$endorse_no){
        $fix = ['tax_id_no' => '0107555000287'];
        $pdf = PDF::loadView('admin.pdf.mt-schedule-original-receipt',['init'=>$data,'fix'=>$fix])->setPaper('A4');

        $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
        $desc_path = "/user/policy/".$desc_directory."/Receipt-Invoice-Tax/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output()); //Save

        return $desc_path.$desc_file;
    }

    private function generateInvoiceCopyPDF($data,$endorse_no){
        $fix = ['tax_id_no' => '0107555000287'];
        $pdf = PDF::loadView('admin.pdf.mt-schedule-invoice',['init'=>$data,'fix'=>$fix,'doc_type'=>"invoice"])->setPaper('A4');

        $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
        $desc_path = "/user/policy/".$desc_directory."/Receipt-Invoice-Tax/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output()); //Save

        return $desc_path.$desc_file;
    }

    private function generateReceiptCopyPDF($data,$endorse_no){
        $fix = ['tax_id_no' => '0107555000287'];
        $pdf = PDF::loadView('admin.pdf.mt-schedule-invoice',['init'=>$data,'fix'=>$fix,'doc_type'=>'receipt'])->setPaper('A4');

        $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
        $desc_path = "/user/policy/".$desc_directory."/Receipt-Invoice-Tax/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output()); //Save

        return $desc_path.$desc_file;
    }

    private function generateMiscPDF($data,$endorse_no){
        $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
        $pdfMerge = new \LynX39\LaraPdfMerger\PdfManage;
        $unsset_file = [];
        $pdf = null;
        $order = $data->order()->first();

        if(!empty($order->theft_gross_premium) && !empty($order->taxi_gross_premium)){
            $pdf = PDF::loadView('admin.pdf.mt-addon-taxi-theft',['init'=>$data])->setPaper('A4');
        }else if(!empty($data->order()->first()->theft_gross_premium)){
            $pdf = PDF::loadView('admin.pdf.mt-addon-theft',['init'=>$data])->setPaper('A4');
        }else if(!empty($data->order()->first()->taxi_gross_premium)){
            $pdf = PDF::loadView('admin.pdf.mt-addon-taxi',['init'=>$data])->setPaper('A4');
        }

        // Schedule File 
        $mt_misc_name = getUniqueFilename($directory_path."/buffer/","pdf");
        Storage::disk('pdf')->put('/buffer/'.$mt_misc_name, $pdf->output()); //Save
        $mt_misc_file = $directory_path."/buffer/".$mt_misc_name;
        $pdfMerge->addPDF($mt_misc_file, 'all');
        $pdf = null;
        array_push($unsset_file,$mt_misc_name);
        //----------------

        // Wordding File
        if(Storage::disk('pdf')->exists('/plan/addon_wording.pdf')){
            $wording_file = $directory_path."/plan/addon_wording.pdf";
            $pdfMerge->addPDF($wording_file, 'all');
        }else{
            Log::error("Create PDF Faild: /plan/addon_wording.pdf not found!");
        }
        //---------------
        if(!empty($order->taxi_gross_premium) && $order->taxi_gross_premium!=0){
            // Taxi Wordding File
            if(Storage::disk('pdf')->exists('/plan/addon_taxi.pdf')){
                $wording_file = $directory_path."/plan/addon_taxi.pdf";
                $pdfMerge->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/addon_taxi.pdf not found!");
            }
            //---------------
        }
        if(!empty($order->theft_gross_premium) && $order->theft_gross_premium!=0){
            // Theft Wordding File
            if(Storage::disk('pdf')->exists('/plan/addon_theft.pdf')){
                $wording_file = $directory_path."/plan/addon_theft.pdf";
                $pdfMerge->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/addon_theft.pdf not found!");
            }
            //---------------
        }
        if(!empty($order->carloss_gross_premium) && $order->carloss_gross_premium!=0){
            // Carloss Wordding File
            if(Storage::disk('pdf')->exists('/plan/addon_carloss.pdf')){
                $wording_file = $directory_path."/plan/addon_carloss.pdf";
                $pdfMerge->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/addon_carloss.pdf not found!");
            }
            //---------------
        }
        if(!empty($order->hb_gross_premium) && $order->hb_gross_premium!=0){
            // HB Wordding File
            if(Storage::disk('pdf')->exists('/plan/addon_hb.pdf')){
                $wording_file = $directory_path."/plan/addon_hb.pdf";
                $pdfMerge->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/addon_hb.pdf not found!");
            }
            //---------------
        }
        //-----------------
        if( (!empty($order->theft_gross_premium) && $order->theft_gross_premium!=0)  && (!empty($order->taxi_gross_premium) && $order->taxi_gross_premium!=0)){
            // Taxi Theft Summary Wordding File
            if(Storage::disk('pdf')->exists('/plan/addon_summary_taxi_theft.pdf')){
                $wording_file = $directory_path."/plan/addon_summary_taxi_theft.pdf";
                $pdfMerge->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/addon_summary_taxi_theft.pdf not found!");
            }
            //---------------
        }else if(!empty($order->theft_gross_premium) && $order->theft_gross_premium!=0){
            // Theft Summary Wordding File
            if(Storage::disk('pdf')->exists('/plan/addon_summary_theft.pdf')){
                $wording_file = $directory_path."/plan/addon_summary_theft.pdf";
                $pdfMerge->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/addon_summary_theft.pdf not found!");
            }
            //---------------
        }else if(!empty($order->taxi_gross_premium) && $order->taxi_gross_premium!=0){
            // Taxi Summary Wordding File
            if(Storage::disk('pdf')->exists('/plan/addon_summary_taxi.pdf')){
                $wording_file = $directory_path."/plan/addon_summary_taxi.pdf";
                $pdfMerge->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/addon_summary_taxi.pdf not found!");
            }
            //---------------
        }

        

        $desc_directory = str_replace("-","",$data->policy_addon_number)."-".$endorse_no; // Add Endorse Version 00
        //Create Uniqe Directory
        Storage::disk('pdf')->makeDirectory('/user/misc/'.$desc_directory);
        //----------------------
        $desc_path = "/user/misc/".$desc_directory."/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        $desc_full_path = $directory_path.$desc_path.$desc_file;
        $pdfMerge->merge('file',$desc_full_path, 'P');

        //Remove Buffer File
        foreach($unsset_file as $file){
            Storage::disk('pdf')->delete('/buffer/'.$file);
        }

        //Save Output
        return $desc_path.$desc_file;
    }

    private function generateOriginalReceiptMiscPDF($data,$endorse_no){
        $fix = ['tax_id_no' => '0107555000287'];
        $pdf = PDF::loadView('admin.pdf.mt-original-addon-receipt',['init'=>$data,'fix'=>$fix])->setPaper('A4');

        $desc_directory = str_replace("-","",$data->policy_addon_number)."-".$endorse_no; // Add Endorse Version 00
        $desc_path = "/user/misc/".$desc_directory."/Receipt-Invoice-Tax/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output()); //Save

        return $desc_path.$desc_file;
    }

    private function generateInvoiceCopyMiscPDF($data,$endorse_no){
        $fix = ['tax_id_no' => '0107555000287'];
        $pdf = PDF::loadView('admin.pdf.mt-addon-invoice',['init'=>$data,'doc_type'=>"invoice",'fix'=>$fix])->setPaper('A4');
        $desc_directory = str_replace("-","",$data->policy_addon_number)."-".$endorse_no; // Add Endorse Version 00
        $desc_path = "/user/misc/".$desc_directory."/Receipt-Invoice-Tax/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output()); //Save

        return $desc_path.$desc_file;
    }

    private function generateReceiptCopyMiscPDF($data,$endorse_no){
        $fix = ['tax_id_no' => '0107555000287'];
        $pdf = PDF::loadView('admin.pdf.mt-addon-invoice',['init'=>$data,'doc_type'=>"receipt",'fix'=>$fix])->setPaper('A4');
        $desc_directory = str_replace("-","",$data->policy_addon_number)."-".$endorse_no; // Add Endorse Version 00
        $desc_path = "/user/misc/".$desc_directory."/Receipt-Invoice-Tax/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output()); //Save

        return $desc_path.$desc_file;
    }

    private function generateCompulsoryPDF($data,$sticker_no){
        $pdf = PDF::loadView('admin.pdf.mt-compulsory',['init'=>$data,'sticker_no'=>$sticker_no])->setPaper('A4');

        $desc_directory = str_replace("-","",$data->policy_com_number); 
        $desc_path = "/user/compulsory/".$desc_directory."/";
        $desc_file  = getUniqueFilename($desc_path,"pdf");
        Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output()); //Save

        return $desc_path.$desc_file;
    }



    public function generatePDFFileName($prefix,$data,$encrypted = false){
        $date_start = strtotime($data->insurance_start);
        $date_end = strtotime($data->insurance_expire);
        $date_start = date('Ymd',$date_start);
        $date_end = date('Ymd',$date_end);
        $endorse = $data->endorse()->first();
        if($encrypted){
            $result = $prefix."_".$data->policy_number."_".iconv('UTF-8','TIS-620',$endorse->owner()->first()->name)." ".iconv('UTF-8','TIS-620',$endorse->owner()->first()->lastname).
        "_".$data->car_chassis_number."_".iconv('UTF-8','TIS-620',$data->car_licence)."_".$date_start."_".$date_end."_".$data->tax_note_number.".pdf";
        }else{
            $result = $prefix."_".$data->policy_number."_".$endorse->owner()->first()->name." ".$endorse->owner()->first()->lastname.
        "_".$data->car_chassis_number."_".$data->car_licence."_".$date_start."_".$date_end."_".$data->tax_note_number.".pdf";
        }
        
        return $result;
    }

    public function generatePDFFileNameAddon($prefix,$data,$encrypted = false){
        $date_start = strtotime($data->insurance_start);
        $date_end = strtotime($data->insurance_expire);
        $date_start = date('Ymd',$date_start);
        $date_end = date('Ymd',$date_end);
        $endorse = $data->endorse()->first();
        if($encrypted){
            $result = $prefix."_".$data->policy_addon_number."_".iconv('UTF-8','TIS-620',$endorse->owner()->first()->name)." ".iconv('UTF-8','TIS-620',$endorse->owner()->first()->lastname).
        "_".$data->car_chassis_number."_".iconv('UTF-8','TIS-620',$data->car_licence)."_".$date_start."_".$date_end."_".$data->tax_note_number.".pdf";
        }else{
            $result = $prefix."_".$data->policy_addon_number."_".$endorse->owner()->first()->name." ".$endorse->owner()->first()->lastname.
        "_".$data->car_chassis_number."_".$data->car_licence."_".$date_start."_".$date_end."_".$data->tax_note_number.".pdf";
        }
        
        return $result;
    }

    private function mergeFilePdf($pdfHandle,$filepath = []){
        $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
        foreach($filepath as $path){
            if(Storage::disk('pdf')->exists($path)){
                $wording_file = $directory_path.$path;
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: ".$path." not found!");
            }
        }
    }

    private function mergeMiscPdf($pdfHandle,$order){
        $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();

        //Flood Wording File
        if(!empty($order->flood_gross_premium) && $order->flood_gross_premium!=0){ 
            if(Storage::disk('pdf')->exists('/plan/10.pdf')){
                $wording_file = $directory_path."/plan/10.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/10.pdf not found!");
            }
        }
        // HB Wordding File
        if(!empty($order->hb_gross_premium) && $order->hb_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/11.pdf')){
                $wording_file = $directory_path."/plan/11.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/11.pdf not found!");
            }
        }
        // Taxi Wordding File
        if(!empty($order->taxi_gross_premium) && $order->taxi_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/12.pdf')){
                $wording_file = $directory_path."/plan/12.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/12.pdf not found!");
            }
        }
        // Theft Wordding File
        if(!empty($order->theft_gross_premium) && $order->theft_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/13.pdf')){
                $wording_file = $directory_path."/plan/13.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/13.pdf not found!");
            }
        }
        // Carloss Wordding File
        if(!empty($order->carloss_gross_premium) && $order->carloss_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/14.pdf')){
                $wording_file = $directory_path."/plan/14.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/14.pdf not found!");
            }
        }
        
    }

    /*
    public function sendEmail($policy_id,$files = "all"){
        try{
            ini_set('max_execution_time',300);
            ini_set('memory_limit', '-1');
            $response = ['status' => 'FAIL','reason' => null,];
            $policy = Policy::where('id',$policy_id)->first();
            if($policy){
                if($this->validateEmail($policy)){
                    Mail::send(new sendDocument($policy,$files));
                    if( count(Mail::failures()) > 0 ) {
                        $response['reason'] = "Process Sending Fail";
                    } else {
                        $response['status'] = 'SUCCESS';
                        $response['reason'] = "Success";
                    }
                }else{
                    $response['reason'] = "Invalid Driver Data";
                }
            }else{
                $response['reason'] = "not found policy data";
            }
            return $response;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }*/

    

    private function validateEmail($policy){
        $endorse = $policy->endorse()->first();
        $driver_info = $endorse->owner()->first();
        if(!empty($driver_info)){
            $validator = Validator::make($driver_info->toArray(), [
                'birth_date' => 'required|date',
                'email' => 'required|email'
            ]);
            if ($validator->fails()) {
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
        
    }


    /************* API **************/
    public function getFloodGlossPremiumAPI($input){
        try{
            // if(!empty($input)){
            //     $flood_bt_calculate = [
            //         'ft_si' => $input['ft_si'],
            //         'define_name'=>$input['define_name'],
            //         'cctv'=>$input['cctv'],
            //         'garage_type'=>$input['garage'],
            //         'deductible'=>$input['deduct'],
            //         'additional_coverage'=>'FLOOD'
            //     ];
            //     if(!empty($input['define_name'])){
            //         if(!empty($input['age1']) && $input['age1']!="UNNAMED"){
            //             $flood_bt_calculate['define_name1'] = $input['age1'];
            //         }
            //         if(!empty($input['age2']) && $input['age2']!="UNNAMED"){
            //             $flood_bt_calculate['define_name2'] = $input['age2'];
            //         }
            //     }else{
            //         $flood_bt_calculate['define_name'] = "UNNAMED";
            //     }
            //     $insuranceRepository =  new InsuranceRepository();
            //     return $insuranceRepository->getFloodPremium($flood_bt_calculate);
            // }
            $flood = AddonFlood::where("id",1)->first();
            if(!empty($flood)){
                return $flood->gross_premium;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function getTaxiGlossPremiumAPI($input){
        try{
            if(!empty($input)){
                $taxi_bt_calculate = [
                    'si' => $input['si'],
                    'is_cctv' => $input['cctv'],
                ];
                if(!empty($input['define_name'])){
                    if(!empty($input['age1']) && $input['age1']!="UNNAMED"){
                        $taxi_bt_calculate['define_name1'] = $input['age1'];
                    }
                    if(!empty($input['age2']) && $input['age2']!="UNNAMED"){
                        $taxi_bt_calculate['define_name2'] = $input['age2'];
                    }
                }
                $insuranceRepository =  new InsuranceRepository();
                return $insuranceRepository->getTaxiPremium($taxi_bt_calculate);
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
    public function getTheftGlossPremiumAPI($input){
        try{
            if(!empty($input)){
                $theft_bt_calculate = [
                    'si' => $input['si'],
                    'is_cctv' => $input['cctv'],
                ];
                if(!empty($input['define_name'])){
                    if(!empty($input['age1']) && $input['age1']!="UNNAMED"){
                        $theft_bt_calculate['define_name1'] = $input['age1'];
                    }
                    if(!empty($input['age2']) && $input['age2']!="UNNAMED"){
                        $theft_bt_calculate['define_name2'] = $input['age2'];
                    }
                }
                $insuranceRepository =  new InsuranceRepository();
                return $insuranceRepository->getTheftPremium($theft_bt_calculate);
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        
    }
    public function getCarlossGlossPremiumAPI($input){
        try{
            if(!empty($input)){
                $carloss_bt_calculate = [
                    'ft_si' => $input['ft_si'],
                    'is_cctv' => $input['cctv'],
                ];
                if(!empty($input['define_name'])){
                    if(!empty($input['age1']) && $input['age1']!="UNNAMED"){
                        $carloss_bt_calculate['define_name1'] = $input['age1'];
                    }
                    if(!empty($input['age2']) && $input['age2']!="UNNAMED"){
                        $carloss_bt_calculate['define_name2'] = $input['age2'];
                    }
                }
                $insuranceRepository =  new InsuranceRepository();
                return $insuranceRepository->getCarlossPremium($carloss_bt_calculate);
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
    public function getHbGlossPremiumAPI($input){
        try{
            if(!empty($input)){
                $hb_bt_calculate = [
                    'si' => $input['si'],
                    'is_cctv' => $input['cctv'],
                ];
                if(!empty($input['define_name'])){
                    if(!empty($input['age1']) && $input['age1']!="UNNAMED"){
                        $hb_bt_calculate['define_name1'] = $input['age1'];
                    }
                    if(!empty($input['age2']) && $input['age2']!="UNNAMED"){
                        $hb_bt_calculate['define_name2'] = $input['age2'];
                    }
                }
                $insuranceRepository =  new InsuranceRepository();
                return $insuranceRepository->getHbPremium($hb_bt_calculate);
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
    public function checkPromotionCode($input){
        try{
            $promotion = Promotion::where([
                'code'=>trim($input['code']),
                'status'=>1
            ])->where(function($query) {
                $query->whereDate('start_at', '<=', date('Y-m-d'));
                $query->orWhereNull('start_at');
            })->where(function($query){
                $query->whereDate('expire_at', '>=', date('Y-m-d'));
                $query->orWhereNull('expire_at');
            })->first();
            
            if(!empty($promotion)){
                $select_plan = $input['coverage'];
                $support_plan = explode(",",$promotion->coverage_id);
                if(in_array($select_plan,$support_plan)) return true;
            }
            return false;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function pdfEncrypt(){
        ini_set('max_execution_time',300);
        $files = Storage::disk('pdf')->get('/user/policy/HQAV50000023WEB01201808-00/5b69513d3149c.pdf');


        $signature = new Signature();
        $requestParams = array();
        $filename = date('dmYHis');
        $requestParams[] = array(
            'filename' => $filename,
            'password' => '123456789',
            'fileStream' => base64_encode($files)
        );

        $response = $signature->request($requestParams);
        $result = json_decode($response);

        var_dump($response);

        if($result->responseHeader->statusCode == '2000'){
            foreach($result->responseDetail as $item){
                if(!empty($item->filename)){
                    Storage::disk('pdf')->put('/user/policy/HQAV50000023WEB01201808-00/'.$item->filename,base64_decode($item->fileStream));
                }
            }
            print '<pre>';
            print_r($result);
            print '</pre>';
        }
        else{
            echo 'invalid file';
        }
        
    }


    // Local Method -------------------------------------------------
    private function pdfEncryptLocal ($driver,$path){
        if(!empty($driver)){
            $pdf = new FPDI_Protection();
            $pagecount = $pdf->setSourceFile($path);
            $password = date('dMY', strtotime($driver->birth_date)).substr($driver->idcard,0,4);
            for ($loop = 1; $loop <= $pagecount; $loop++) {
                $tplidx = $pdf->importPage($loop);
                $pdf->addPage();
                $pdf->useTemplate($tplidx);
            }

            $pdf->SetProtection(\FPDI_Protection::FULL_PERMISSIONS,$password,$password);
            $pdf->Output($path, 'F');
            return true;
        }
        return false;
    }

    private function generateOrderNumber(){
        $counter = 1;
        $number = Order::select(DB::raw('max(id) as id'))->whereYear('created_at', '=', date('Y'))->first()->id;
        
        $order_number = "OD-".date('Y-m-').str_pad(++$number,7,"0",STR_PAD_LEFT);
        while(!$this->checkOrderUniqeNumber($order_number)){
            $number++;
            $order_number = "OD-".date('Y-m-').str_pad($number,7,"0",STR_PAD_LEFT);
        }
        return $order_number;
    }

    private function checkOrderUniqeNumber($number){
        $order = Order::where('order_number',$number)->first();
        if(!empty($order)){
            return false;
        }
        return true;
    }

    public function generatePolicyNumber($type,$runing_number,$years,$months){
        $prefix = "";
        $postfix = "-MOL01-".$years."-".$months;
        $result = "";
        if($type==1 || $type==2){
            $prefix = "HQ-AV5-";
        }else if($type==3){
            $prefix = "HQ-AV3-";
        }else if($type=="addon"){
            $prefix = "HQ-SMA-";
        }else if($type=="com"){
            $prefix = "HQ-AC3-";
        }
        return $prefix.$runing_number.$postfix;
    }

    public function generateTaxNoteNumber($runing_number,$years,$months){
        $prefix = "HQ";
        $new_runing_number = str_pad(intval($runing_number),6,"0",STR_PAD_LEFT);
        return $prefix."-".$years."-".$months."-T-V-".$new_runing_number;
    }

    private function checkRunningUniqeNumber($number,$type){
        $policy = null;
        if($type=="addon"){
            $policy = Policy::where('policy_addon_number',$number)->first();
        }else{
            $policy = Policy::where('policy_number',$number)->first();
        }
        if(!empty($policy)){
            return false;
        }
        return true;
    }

    private function checkRunningRecord(){
        try{

            /* Get Runing Number */
            $running_year = date('Y');
            $running_month = date('m');
            $cutoff = Cutoff::where([
                'years' => $running_year,
                'months' => $running_month
            ])->first();
            if(!empty($cutoff)){
                if(date('Y-m-d') > date('Y-m-d',strtotime($cutoff->cutoff_date))){
                    $running_month+=1;
                    if($running_month>12){
                        $running_month = 1;
                        $running_year+=1;

                    }
                }
            }

            $record = RunningNumber::where('year',$running_year)->first();
            if(empty($record)){
                RunningNumber::create([
                    'order' => 0,
                    'payment' => 0,
                    'policy_av5' => 0,
                    'policy_av3' => 0,
                    'misc' => 0,
                    'com' => 0,
                    'tax' => 0,
                    'year' => $running_year,
                ]);
            }
            return true;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    private function checkMonthRunningRecord(){
        try{

            /* Get Runing Number */
            $running_year = date('Y');
            $running_month = date('m');
            $cutoff = Cutoff::where([
                'years' => $running_year,
                'months' => $running_month
            ])->first();
            if(!empty($cutoff)){
                if(date('Y-m-d') > date('Y-m-d',strtotime($cutoff->cutoff_date))){
                    $running_month+=1;
                    if($running_month>12){
                        $running_month = 1;
                        $running_year+=1;
                    }
                }
            }

            $record = MonthRunningNumber::where([
                'year' => $running_year,
                'month' => $running_month
            ])->first();
            if(empty($record)){
                MonthRunningNumber::create([
                    'tax' => 0,
                    'year' => $running_year,
                    'month' => $running_month,
                ]);
            }
            return true;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }






    


    /*
    session(['key' => 'value']);
    // Or
    session()->put('key', 'value')
    $value = session()->get('key');
    // Alternatively you can pass in a default value
    $value = session()->get('key', 'default);
    //Delete
    session()->forget('key');
    */
    

}