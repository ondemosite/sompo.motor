<?php

namespace App\Library;
use App;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Log;
use App\Repositories\InsuranceRepository;
use App\Models\Promotion;
use Carbon\Carbon;
use MicrosoftAzure\Storage\Blob\Models\PageWriteOption;
use App\Models\Pricing;
use App\Models\Addon;
use App\Models\AddonCarLoss;
use App\Models\AddonHb;
use App\Models\AddonTheft;
use App\Models\AddonTaxi;
use App\Models\Compulsory;
use App\Models\Tax;
use App\Models\Order;
use App\Models\DriverInfo;
use App\Models\Coverage;
use App\Models\Policy;
use App\Models\Vehicle;
use App\Models\VehicleSecond;
use App\Models\Receipt;
use App\Models\RunningNumber;
use App\Models\MonthRunningNumber;
use App\Models\Endorse;
use App\Models\VehicleInfo;
use App\Models\Cutoff;
use App\Models\DatabaseActive;
use App\Models\AddonFlood;
use App\Models\Payment;
use App\Models\SignatureLog;
use App\Models\Prefix;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use PDF;
use FPDI_Protection;
use DateTime;
use DateTimeZone;
use App\Library\signature;
use App\Library\Compulsory as Barcode;



class Compile extends Controller
{

    private $activeTable = null;

    public function __construct(){

    }

    public function createPolicy($order){
        $this->checkRunningRecord();
        $policy_result = null;
        logTransaction($order->order_number,"ดำเนินการสร้างกรมธรรม์","SUCCESS");
        DB::beginTransaction();
        try{
            $state = "run";
            do{
                /* Get Runing Number */
                $running_year = date('Y');
                $running_month = date('m');
                $cutoff = Cutoff::where([
                    'years' => $running_year,
                    'months' => $running_month
                ])->first();
                if(!empty($cutoff)){
                    if(date('Y-m-d') > date('Y-m-d',strtotime($cutoff->cutoff_date))){
                        $running_month+=1;
                        if($running_month>12){
                            $running_month = 1;
                            $running_year+=1;

                        }
                    }
                }
                $number_record = RunningNumber::where('year',$running_year)->first();
                $policy_running = $order->insurance_plan_id==3?intval($number_record->policy_av3):intval($number_record->policy_av5);
                $policy_runing_number = str_pad(++$policy_running,7,"0",STR_PAD_LEFT);
                $tax_running = intval($number_record->tax);
                $tax_running_number = str_pad(++$tax_running,7,"0",STR_PAD_LEFT);
                $com_running = intval($number_record->com);
                $com_running_number = str_pad(++$com_running,7,"0",STR_PAD_LEFT);
                $com_tax_running_number = "";
                if(!empty($order->compulsory_net_premium) && $order->compulsory_net_premium > 0){
                    $com_tax_running_number = str_pad(++$tax_running,7,"0",STR_PAD_LEFT);
                }
                
                $policy_number = $this->generatePolicyNumber($order->insurance_plan_id,$policy_runing_number,$running_year,$running_month);
                $tax_note_number = $this->generateTaxNoteNumber($tax_running_number,$running_year,$running_month);
                $policy_com_number = $this->generatePolicyNumber("com",$com_running_number,$running_year,$running_month);
                $com_tax_note_number = $this->generateTaxNoteNumber($com_tax_running_number,$running_year,$running_month);

                if($policy_number!=false){
                    $data = [
                        'policy_number' => $policy_number,
                        'tax_note_number' => $tax_note_number,
                        'order_id'=>$order->id,
                        'status'=>'NORMAL',
                        'car_licence'=>$order->car_licence,
                        'car_province'=>$order->car_province,
                        'car_chassis_number'=>$order->car_chassis_number,
                        'car_cctv'=>$order->car_cctv,
                        'vehicle_info' => $order->vehicle_info,
                        'owner' => $order->main_driver,
                        'insurance_plan_id'=>$order->insurance_plan_id,
                        'insurance_plan_name'=>$order->insurance_plan_name,
                        'insurance_start'=>$order->insurance_start,
                        'insurance_expire'=>$order->insurance_expire,
                        'insurance_ft_si'=>$order->insurance_ft_si,
                        'insurance_garage_type'=>$order->insurance_garage_type,
                        'insurance_deduct'=>$order->insurance_deduct,
                        'insurance_person_damage_once'=>$order->insurance_person_damage_once,
                        'insurance_person_damage_person'=>$order->insurance_person_damage_person,
                        'insurance_stuff_damage'=>$order->insurance_stuff_damage,
                        'insurance_death_disabled'=>$order->insurance_death_disabled,
                        'insurance_medical_fee'=>$order->insurance_medical_fee,
                        'insurance_bail_driver'=>$order->insurance_bail_driver,
                        'insurance_driver_amount'=>$order->insurance_driver_amount,
                        'compulsory_start'=>$order->compulsory_start,
                        'compulsory_expire'=>$order->compulsory_expire,
                        'addon_theft'=>$order->addon_theft,
                        'addon_taxi'=>$order->addon_taxi,
                        'addon_hb'=>$order->addon_hb,
                        'addon_carloss'=>$order->addon_carloss,
                        'document_path_personal_id'=>$order->document_path_personal_id,
                        'document_path_car_licence'=>$order->document_path_car_licence,
                        'document_path_cctv_inside'=>$order->document_path_cctv_inside,
                        'document_path_cctv_outside'=>$order->document_path_cctv_outside,
                        'document_path_driver1_licence'=>$order->document_path_driver1_licence,
                        'document_path_driver2_licence'=>$order->document_path_driver2_licence,
                        'locale'=> App::getLocale(),
                        'is_advice'=>$order->is_advice,
                        'is_post'=>$order->is_post,
                        'is_personal'=>$order->is_personal,
                        'is_email'=>$order->is_email,
                        'is_disclosure'=>$order->is_disclosure,
                        'is_true'=>$order->is_true,
                        'is_agree'=>$order->is_agree
                    ];
                    //Create Compulsory Policy
                    if(!empty($order->compulsory_net_premium) && $order->compulsory_net_premium > 0){
                        $data['policy_com_number'] = $policy_com_number;
                        $data['com_tax_note_number'] = $com_tax_note_number;
                    }
                    $set_response = $this->setPolicy($data,$order);

                    //Update Runing Record
                    if($set_response['status']==true){
                        if($order->insurance_plan_id==3){
                            $number_record->policy_av3 = $policy_running;
                        }else{
                            $number_record->policy_av5 = $policy_running;
                        }
                        
                        if(!empty($data['policy_com_number'])){
                            $number_record->com = $com_running;
                        }
                        
                        $number_record->tax = $tax_running;
                        $number_record->save();

                        $state = "stop";
                        $policy_result = $set_response['policy_id'];
                    }else{
                        DB::rollBack();
                    }
                }
            }while($state=="run");
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            logTransaction($order->order_number,"CATCH สร้างกรมธรรม์ล้มเหลว ".$e->getMessage(),"FAIL");
            return [
                'status' => "ERROR",
                'message' => $e->getMessage()
            ];
        }
        DB::commit();

        if($policy_result!=null){
            logTransaction($order->order_number,"สร้างกรมธรรม์สำเร็จ หมายเลข: ".$policy_result,"SUCCESS");
            return [
                'status' => "SUCCESS",
                'policy_id' => $policy_result
            ];
        }else{
            logTransaction($order->order_number,"สร้างกรมธรรม์ล้มเหลว","FAIL");
            return [
                'status' => "ERROR",
                'message' => "ไม่ทราบสาเหตุ กรุณาติดต่อผู้ดูแลระบบ"
            ];
        }
    }


    public function createPdf($policy,$type){
        try{
            if($type == "voluntary"){
                return $this->generatePolicyPDF($policy);
            }else if($type == "ori_invoice"){
                return $this->generateOriginalReceiptPDF($policy);
            }else if($type == "copy_invoice"){
                return $this->generateCopyReceiptPDF($policy);
            }else if($type == "compulsory" && !empty($policy->policy_com_number)){

                //get Sticker No
                $barcodeApi = new Barcode();
                $response = $barcodeApi->getBarcode($policy->policy_com_number);
                $sticker_no = null;
                if($response!=false){
                    $sticker_no = $response;
                }
                if(!empty($sticker_no)){
                    $policy->sticker_no = $sticker_no;
                    $policy->save();
                    return $this->generateCompulsoryPDF($policy,$sticker_no);
                }else{
                    return [
                        'status' => 'ERROR',
                        'message' => 'สร้าง sticker number ไม่สำเร็จ'
                    ];
                }
            }else{
                return [
                    'status' => 'ERROR',
                    'message' => 'ไม่พบรูปแบบกรมธรรม์'
                ];
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => 'ERROR',
                'message' => $e->getMessage()
            ];
        }
    }

    public function resendFTP($policy,$type){
        try{

            if($type == "voluntary"){
                $endorse = $policy->endorse()->first();
                $desc_directory = str_replace("-","",$policy->policy_number)."-".$endorse->endorse_no;
                $desc_path = "/user/policy/".$desc_directory."/";
                $desc_file = $endorse->policy_document_name;
                $newFileName = $this->generatePDFFileName("Ori",$policy,false);
                $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);
                $newFileNameCopy = $this->generatePDFFileName("Copy",$policy,false);
                $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);
                return [
                    'status' => 'SUCCESS'
                ];

            }else if($type == "ori_invoice"){
                $endorse = $policy->endorse()->first();
                $desc_directory = str_replace("-","",$policy->policy_number)."-".$endorse->endorse_no;
                $desc_path = "/user/policy/".$desc_directory."/Receipt-Invoice-Tax/";
                $desc_file = $endorse->policy_original_invoice_name;
                $newFileName = $this->generatePDFFileName("RecOri",$policy,false);
                $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);
                return [
                    'status' => 'SUCCESS'
                ];
            }else if($type == "copy_invoice"){
                $endorse = $policy->endorse()->first();
                $desc_directory = str_replace("-","",$policy->policy_number)."-".$endorse->endorse_no;
                $desc_path = "/user/policy/".$desc_directory."/Receipt-Invoice-Tax/";
                $desc_file = $endorse->policy_copy_invoice_name;
                $newFileNameCopy = $this->generatePDFFileName("RecCopy",$policy,false);
                $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);
                return [
                    'status' => 'SUCCESS'
                ];
            }else if($type == "compulsory" && !empty($policy->policy_com_number)){
                $endorse = $policy->endorse()->first();
                $desc_directory = str_replace("-","",$policy->policy_com_number)."-".$endorse->endorse_no;
                $desc_path = "/user/compulsory/".$desc_directory."/";
                $desc_file = $endorse->compulsory_document_name;
               
                //Send SFTP
                $newFileName = $this->generatePDFCompulsoryFileName("Ori",$policy,false);
                $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);
                $newFileNameCopy = $this->generatePDFCompulsoryFileName("Copy",$policy,false);
                $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);
                return [
                    'status' => 'SUCCESS'
                ];
            }else{
                return [
                    'status' => 'ERROR',
                    'message' => 'ไม่พบรูปแบบกรมธรรม์'
                ];
            }

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => 'ERROR',
                'message' => $e->getMessage()
            ];
        }
    }


    private function generatePolicyPDF($data){
        ini_set('memory_limit', '-1');
        header('Content-Type: text/html; charset=UTF-8');
        try{
            
            $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
            $pdfMerge = new \LynX39\LaraPdfMerger\PdfManage;
            $pdfMergeCopy = new \LynX39\LaraPdfMerger\PdfManage;
            $pdfMergeClean = new \LynX39\LaraPdfMerger\PdfManage;
            $unsset_file = [];
            $order = $data->order()->first();
            $endorse = $data->endorse()->first();
            $endorse_no = $endorse->endorse_no;
            logTransaction($order->order_number,"ดำเนินการสร้าง Policy PDF","SUCCESS");
            if($data->insurance_plan_id==1){ /*************** 2+ ***********************/

                //Schedule File 
                $mt_schedule_name = getUniqueFilename($directory_path."/buffer/","pdf");
                $mt_schedule_file = $directory_path."/buffer/".$mt_schedule_name;
                $mt_schedule_file_copy = $directory_path."/buffer/"."copy_".$mt_schedule_name;
                $mt_schedule_file_clean = $directory_path."/buffer/"."clean_".$mt_schedule_name;

                $pdf = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data,'copy' => false])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'.$mt_schedule_name, $pdf->output());
                $pdfCopy = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data,'copy' => true])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'."copy_".$mt_schedule_name, $pdfCopy->output());
                $pdfClean = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data,'clean' => true])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'."clean_".$mt_schedule_name, $pdfClean->output());

                $pdf = null;
                $pdfCopy = null;
                $pdfClean = null;

                $pdfMerge->addPDF($mt_schedule_file, 'all');
                $pdfMergeCopy->addPDF($mt_schedule_file_copy, 'all');
                $pdfMergeClean->addPDF($mt_schedule_file_clean, 'all');
                
                array_push($unsset_file,$mt_schedule_name); //push buffer file for delete
                //----------------

                //Wordding File 
                $wordingList = ['/plan/2.pdf','/plan/3.pdf','/plan/4.pdf','/plan/5.pdf','/plan/6.pdf','/plan/7.pdf','/plan/8.pdf','/plan/9.pdf'];
                $this->mergeFilePdf($pdfMerge,$wordingList);
                $this->mergeFilePdf($pdfMergeCopy,$wordingList);
                $this->mergeFilePdf($pdfMergeClean,$wordingList);
                //---------------

                //Misc File
                $this->mergeMiscPdf($pdfMerge,$order);
                $this->mergeMiscPdf($pdfMergeCopy,$order);
                $this->mergeMiscPdf($pdfMergeClean,$order);
                //---------------

                //Create Uniqe Directory
                $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
                Storage::disk('pdf')->makeDirectory('/user/policy/'.$desc_directory);
                //----------------------'

                //Merge File
                $desc_path = "/user/policy/".$desc_directory."/";
                $desc_file  = getUniqueFilename($desc_path,"pdf");
                $desc_full_path = $directory_path.$desc_path.$desc_file;
                $desc_full_path_copy = $directory_path.$desc_path."copy_".$desc_file;
                $desc_full_path_clean = $directory_path.$desc_path."clean_".$desc_file;

                $pdfMerge->merge('file',$desc_full_path, 'P');
                $pdfMergeCopy->merge('file',$desc_full_path_copy, 'P');
                $pdfMergeClean->merge('file',$desc_full_path_clean, 'P');
                //---------------------

                //Send SFTP
                $newFileName = $this->generatePDFFileName("Ori",$data,false);
                $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);
                $newFileNameCopy = $this->generatePDFFileName("Copy",$data,false);
                $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);

                //Remove Buffer File
                foreach($unsset_file as $file){
                    Storage::disk('pdf')->delete('/buffer/'.$file);
                    Storage::disk('pdf')->delete('/buffer/'."copy_".$file);
                    Storage::disk('pdf')->delete('/buffer/'."clean_".$file);
                }
                //---------------------

                //Save Output
                $endorse->policy_document_name = $desc_file;
                $endorse->policy_document_path = $desc_path.$desc_file;
                $endorse->save();
                //Encrypt
                $main_driver_info = $endorse->owner()->first();
                $fileName = $data->policy_number . '-' .$endorse_no."-"."Schedule.pdf";
                $encryptResult = $this->pdfEncrypt($data,$main_driver_info,$desc_path.$desc_file,$fileName);
                if(!$encryptResult){ //เข้ารหัสล้มเหลว
                    $this->pdfEncryptLocal($main_driver_info,$directory_path.$desc_path.$desc_file); //เข้ารหัสด้วยตัวเอง
                    logTransaction($order->order_number,"เข้ารหัส Policy PDF ล้มเหลว","FAIL");         
                }
                logTransaction($order->order_number,"สร้าง Policy PDF สำเร็จ","SUCCESS");        
                return [
                    'status' => 'SUCCESS'
                ];

            }else if($data->insurance_plan_id==2){ /***************** 3+ ***********************/
                // Schedule File 
                $mt_schedule_name = getUniqueFilename($directory_path."/buffer/","pdf");
                $mt_schedule_file = $directory_path."/buffer/".$mt_schedule_name;
                $mt_schedule_file_copy = $directory_path."/buffer/"."copy_".$mt_schedule_name;
                $mt_schedule_file_clean = $directory_path."/buffer/"."clean_".$mt_schedule_name;

                $pdf = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data,'copy' => false])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'.$mt_schedule_name, $pdf->output());
                $pdfCopy = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data,'copy' => true])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'."copy_".$mt_schedule_name, $pdfCopy->output());
                $pdfClean = PDF::loadView('admin.pdf.mt-schedule_2',['init'=>$data,'clean' => true])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'."clean_".$mt_schedule_name, $pdfClean->output());
                $pdf = null;
                $pdfCopy = null;
                $pdfClean = null;
                
                $pdfMerge->addPDF($mt_schedule_file, 'all');
                $pdfMergeCopy->addPDF($mt_schedule_file_copy, 'all');
                $pdfMergeClean->addPDF($mt_schedule_file_clean, 'all');
                
                array_push($unsset_file,$mt_schedule_name);
                //----------------
                
                //Wordding File 
                $wordingList = ['/plan/2.pdf','/plan/3.pdf','/plan/5.pdf','/plan/6.pdf','/plan/7.pdf','/plan/8.pdf','/plan/9.pdf'];
                $this->mergeFilePdf($pdfMerge,$wordingList);
                $this->mergeFilePdf($pdfMergeCopy,$wordingList);
                $this->mergeFilePdf($pdfMergeClean,$wordingList);
                //---------------

                //Misc File
                $this->mergeMiscPdf($pdfMerge,$order);
                $this->mergeMiscPdf($pdfMergeCopy,$order);
                $this->mergeMiscPdf($pdfMergeClean,$order);
                //---------------

                //Create Uniqe Directory
                $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
                Storage::disk('pdf')->makeDirectory('/user/policy/'.$desc_directory);
                //----------------------

                //Merge File
                $desc_path = "/user/policy/".$desc_directory."/";
                $desc_file  = getUniqueFilename($desc_path,"pdf");
                $desc_full_path = $directory_path.$desc_path.$desc_file;
                $desc_full_path_copy = $directory_path.$desc_path."copy_".$desc_file;
                $desc_full_path_clean = $directory_path.$desc_path."clean_".$desc_file;
                $pdfMerge->merge('file',$desc_full_path, 'P');
                $pdfMergeCopy->merge('file',$desc_full_path_copy, 'P');
                $pdfMergeClean->merge('file',$desc_full_path_clean, 'P');
                //---------------------

                //Send SFTP
                $newFileName = $this->generatePDFFileName("Ori",$data,false);
                $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);
                $newFileNameCopy = $this->generatePDFFileName("Copy",$data,false);
                $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);

                //Remove Buffer File
                foreach($unsset_file as $file){
                    Storage::disk('pdf')->delete('/buffer/'.$file);
                    Storage::disk('pdf')->delete('/buffer/'."copy_".$file);
                    Storage::disk('pdf')->delete('/buffer/'."clean_".$file);
                }
                //---------------------

                //Save Output
                $endorse->policy_document_name = $desc_file;
                $endorse->policy_document_path = $desc_path.$desc_file;
                $endorse->save();
                //Encrypt
                $main_driver_info = $endorse->owner()->first();
                $fileName = $data->policy_number . '-' .$endorse_no."-"."Schedule.pdf";
                $encryptResult = $this->pdfEncrypt($data,$main_driver_info,$desc_path.$desc_file,$fileName);
                if(!$encryptResult){ //เข้ารหัสล้มเหลว
                    $this->pdfEncryptLocal($main_driver_info,$directory_path.$desc_path.$desc_file); //เข้ารหัสด้วยตัวเอง
                    logTransaction($order->order_number,"เข้ารหัส Policy PDF ล้มเหลว","FAIL");         
                }
                logTransaction($order->order_number,"สร้าง Policy PDF สำเร็จ","SUCCESS");        
                return [
                    'status' => 'SUCCESS'
                ];

            }else{ /***************** Type3 *********************/
                
                // Schedule File 
                $mt_schedule_name = getUniqueFilename($directory_path."/buffer/","pdf");
                $mt_schedule_file = $directory_path."/buffer/".$mt_schedule_name;
                $mt_schedule_file_copy = $directory_path."/buffer/"."copy_".$mt_schedule_name;
                $mt_schedule_file_clean = $directory_path."/buffer/"."clean_".$mt_schedule_name;

                $pdf = PDF::loadView('admin.pdf.mt-schedule_3',['init'=>$data,'copy' => false])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'.$mt_schedule_name, $pdf->output()); //Save
                $pdfCopy = PDF::loadView('admin.pdf.mt-schedule_3',['init'=>$data,'copy' => true])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'."copy_".$mt_schedule_name, $pdfCopy->output()); //Save
                $pdfClean = PDF::loadView('admin.pdf.mt-schedule_3',['init'=>$data,'clean' => true])->setPaper('A4');
                Storage::disk('pdf')->put('/buffer/'."copy_".$mt_schedule_name, $pdfClean->output()); //Save
                $pdf = null;
                $pdfCopy = null;
                $pdfClean = null;
                
                $pdfMerge->addPDF($mt_schedule_file, 'all');
                $pdfMergeCopy->addPDF($mt_schedule_file_copy, 'all');
                $pdfMergeClean->addPDF($mt_schedule_file_clean, 'all');

                array_push($unsset_file,$mt_schedule_name);
                //----------------
                
                //Wordding File 
                $wordingList = ['/plan/2.pdf','/plan/3.pdf','/plan/5.pdf','/plan/6.pdf','/plan/7.pdf','/plan/8.pdf','/plan/9.pdf'];
                $this->mergeFilePdf($pdfMerge,$wordingList);
                $this->mergeFilePdf($pdfMergeCopy,$wordingList);
                $this->mergeFilePdf($pdfMergeClean,$wordingList);
                //---------------

                //Create Uniqe Directory
                $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no; // Add Endorse Version 00
                Storage::disk('pdf')->makeDirectory('/user/policy/'.$desc_directory);
                //----------------------

                //Merge File
                $desc_path = "/user/policy/".$desc_directory."/";
                $desc_file  = getUniqueFilename($desc_path,"pdf");
                $desc_full_path = $directory_path.$desc_path.$desc_file;
                $desc_full_path_copy = $directory_path.$desc_path."copy_".$desc_file;
                $desc_full_path_clean = $directory_path.$desc_path."clean_".$desc_file;
                $pdfMerge->merge('file',$desc_full_path, 'P');
                $pdfMergeCopy->merge('file',$desc_full_path_copy, 'P');
                $pdfMergeClean->merge('file',$desc_full_path_clean, 'P');
                //----------------------

                //Send SFTP
                $newFileName = $this->generatePDFFileName("Ori",$data,false);
                $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);
                $newFileNameCopy = $this->generatePDFFileName("Copy",$data,false);
                $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);

                //Remove Buffer File
                foreach($unsset_file as $file){
                    Storage::disk('pdf')->delete('/buffer/'.$file);
                    Storage::disk('pdf')->delete('/buffer/'."copy_".$file);
                    Storage::disk('pdf')->delete('/buffer/'."clean_".$file);
                }

                //Save Output
                $endorse->policy_document_name = $desc_file;
                $endorse->policy_document_path = $desc_path.$desc_file;
                $endorse->save();
                //Encrypt
                $main_driver_info = $endorse->owner()->first();
                $fileName = $data->policy_number . '-' .$endorse_no."-"."Schedule.pdf";
                $encryptResult = $this->pdfEncrypt($data,$main_driver_info,$desc_path.$desc_file,$fileName);
                if(!$encryptResult){ //เข้ารหัสล้มเหลว
                    $this->pdfEncryptLocal($main_driver_info,$directory_path.$desc_path.$desc_file); //เข้ารหัสด้วยตัวเอง
                    logTransaction($order->order_number,"เข้ารหัส Policy PDF ล้มเหลว","FAIL");         
                }
                logTransaction($order->order_number,"สร้าง Policy PDF สำเร็จ","SUCCESS");        
                return [
                    'status' => 'SUCCESS'
                ];
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => 'ERROR',
                'message' => $e->getMessage()
            ];
        }
        

    }

    private function generateOriginalReceiptPDF($data){
        try{
            $endorse = $data->endorse()->first();
            $order = $data->order()->first();
            $endorse_no = $endorse->endorse_no;
            logTransaction($order->order_number,"ดำเนินการสร้าง Original Invoice","SUCCESS");
            $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
            $fix = ['tax_id_no' => config('company.tax_id_no')];
            $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no;
            $desc_path = "/user/policy/".$desc_directory."/Receipt-Invoice-Tax/";
            $desc_file  = getUniqueFilename($desc_path,"pdf");
            $pdf = PDF::loadView('admin.pdf.mt-schedule-original-receipt',['init'=>$data,'fix'=>$fix, "copy" => false])->setPaper('A4');
            $pdfClean = PDF::loadView('admin.pdf.mt-schedule-original-receipt',['init'=>$data,'fix'=>$fix, "clean" => true])->setPaper('A4');

            Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output());
            Storage::disk('pdf')->put($desc_path."clean_".$desc_file, $pdfClean->output());

            //Send SFTP
            $newFileName = $this->generatePDFFileName("RecOri",$data,false);
            $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);

            //Save Output
            $endorse->policy_original_invoice_path = $desc_path.$desc_file;
            $endorse->policy_original_invoice_name = $desc_file;
            $endorse->save();
            //Encrypt
            $main_driver_info = $endorse->owner()->first();
            $fileName = $data->policy_number . '-' .$endorse_no."-"."Original-Invoice.pdf";
            $encryptResult = $this->pdfEncrypt($data,$main_driver_info,$desc_path.$desc_file,$fileName);
            if(!$encryptResult){ //เข้ารหัสล้มเหลว
                $this->pdfEncryptLocal($main_driver_info,$directory_path.$desc_path.$desc_file); //เข้ารหัสด้วยตัวเอง
                logTransaction($order->order_number,"เข้ารหัส Original Invoice PDF ล้มเหลว","FAIL");       
            }
            logTransaction($order->order_number,"สร้าง Original Invoice PDF สำเร็จ","SUCCESS");    
            return [
                'status' => 'SUCCESS'
            ];
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => 'ERROR',
                'message' => $e->getMessage()
            ];
        }
    }

    private function generateCopyReceiptPDF($data){
        try{
            $endorse = $data->endorse()->first();
            $order = $data->order()->first();
            logTransaction($order->order_number,"ดำเนินการสร้าง Copy Invoice","SUCCESS");
            $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
            $endorse_no = $endorse->endorse_no;
            $fix = ['tax_id_no' => config('company.tax_id_no')];
            $desc_directory = str_replace("-","",$data->policy_number)."-".$endorse_no;
            $desc_path = "/user/policy/".$desc_directory."/Receipt-Invoice-Tax/";
            $desc_file  = getUniqueFilename($desc_path,"pdf");
            $pdf = PDF::loadView('admin.pdf.mt-schedule-copy-receipt',['init'=>$data,'fix'=>$fix, "copy" => false])->setPaper('A4');
            $pdfCopy = PDF::loadView('admin.pdf.mt-schedule-copy-receipt',['init'=>$data,'fix'=>$fix, "copy" => false])->setPaper('A4');

            Storage::disk('pdf')->put($desc_path.$desc_file, $pdf->output());
            Storage::disk('pdf')->put($desc_path."copy_".$desc_file, $pdfCopy->output());

            //Send SFTP
            $newFileNameCopy = $this->generatePDFFileName("RecCopy",$data,false);
            $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);

            //Save Output
            $endorse->policy_copy_invoice_path = $desc_path.$desc_file;
            $endorse->policy_copy_invoice_name = $desc_file;
            $endorse->save();
            //Encrypt
            $main_driver_info = $endorse->owner()->first();
            $fileName = $data->policy_number . '-' .$endorse_no."-"."Copy-Invoice.pdf";
            $encryptResult = $this->pdfEncrypt($data,$main_driver_info,$desc_path.$desc_file,$fileName);
            if(!$encryptResult){ //เข้ารหัสล้มเหลว
                $this->pdfEncryptLocal($main_driver_info,$directory_path.$desc_path.$desc_file); //เข้ารหัสด้วยตัวเอง
                logTransaction($order->order_number,"เข้ารหัส Copy Invoice PDF ล้มเหลว","FAIL");      
            }
            logTransaction($order->order_number,"สร้าง Copy Invoice PDF สำเร็จ","SUCCESS"); 
            return [
                'status' => 'SUCCESS'
            ];
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => 'ERROR',
                'message' => $e->getMessage()
            ];
        }
    }

    private function generateCompulsoryPDF($data,$sticker_no){
        try{
            
            $endorse = $data->endorse()->first();
            $order = $data->order()->first();
            $endorse_no = $endorse->endorse_no;
            logTransaction($order->order_number,"ดำเนินการสร้าง Compulsory PDF","SUCCESS");

            $pdfMerge = new \LynX39\LaraPdfMerger\PdfManage;
            $pdfMergeCopy = new \LynX39\LaraPdfMerger\PdfManage;
            $pdfMergeClean = new \LynX39\LaraPdfMerger\PdfManage;
            $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
            $mt_schedule_name = getUniqueFilename($directory_path."/buffer/","pdf");

            //Schedule File 
            $pdf = PDF::loadView('admin.pdf.mt-compulsory',['init'=>$data,'sticker_no'=>$sticker_no, 'copy' => false])->setPaper('A4');
            $pdfCopy = PDF::loadView('admin.pdf.mt-compulsory',['init'=>$data,'sticker_no'=>$sticker_no, 'copy' => true])->setPaper('A4');
            $pdfClean = PDF::loadView('admin.pdf.mt-compulsory',['init'=>$data,'sticker_no'=>$sticker_no, 'clean' => true])->setPaper('A4');

            Storage::disk('pdf')->put('/buffer/'.$mt_schedule_name, $pdf->output());
            Storage::disk('pdf')->put('/buffer/'."copy_".$mt_schedule_name, $pdfCopy->output());
            Storage::disk('pdf')->put('/buffer/'."clean_".$mt_schedule_name, $pdfClean->output());

            $mt_schedule_file = $directory_path."/buffer/".$mt_schedule_name;
            $mt_schedule_file_copy = $directory_path."/buffer/"."copy_".$mt_schedule_name;
            $mt_schedule_file_clean = $directory_path."/buffer/"."clean_".$mt_schedule_name;

            $pdfMerge->addPDF($mt_schedule_file, 'all');
            $pdfMergeCopy->addPDF($mt_schedule_file_copy, 'all');
            $pdfMergeClean->addPDF($mt_schedule_file_clean, 'all');
            $pdf = null;
            $pdfCopy = null;
            $pdfClean = null;
            //----------------

            //Wordding File 
            $wordingList = ['/plan/15.pdf'];
            $this->mergeFilePdf($pdfMerge,$wordingList);
            $this->mergeFilePdf($pdfMergeCopy,$wordingList);
            $this->mergeFilePdf($pdfMergeClean,$wordingList);
            //---------------

            //Create Uniqe Directory
            $desc_directory = str_replace("-","",$data->policy_com_number)."-".$endorse_no; 
            Storage::disk('pdf')->makeDirectory('/user/compulsory/'.$desc_directory);
            //----------------------'

            //Merge File And Save
            $desc_path = "/user/compulsory/".$desc_directory."/";
            $desc_file  = getUniqueFilename($desc_path,"pdf");
            $desc_full_path = $directory_path.$desc_path.$desc_file;
            $desc_full_path_copy = $directory_path.$desc_path."copy_".$desc_file;
            $desc_full_path_clean = $directory_path.$desc_path."clean_".$desc_file;
            $pdfMerge->merge('file',$desc_full_path, 'P');
            $pdfMergeCopy->merge('file',$desc_full_path_copy, 'P');
            $pdfMergeClean->merge('file',$desc_full_path_clean, 'P');
            //---------------------

            //Send SFTP
            $newFileName = $this->generatePDFCompulsoryFileName("Ori",$data,false);
            $this->sendFtp($desc_path."clean_".$desc_file,$newFileName);
            $newFileNameCopy = $this->generatePDFCompulsoryFileName("Copy",$data,false);
            $this->sendFtp($desc_path."copy_".$desc_file,$newFileNameCopy);

            //Delete Buffer File
            Storage::disk('pdf')->delete('/buffer/'.$mt_schedule_name);
            Storage::disk('pdf')->delete('/buffer/'."clean_".$mt_schedule_name);
            Storage::disk('pdf')->delete('/buffer/'."copy_".$mt_schedule_name);

            //Save Output
            $endorse->compulsory_document_name = $desc_file;
            $endorse->compulsory_document_path = $desc_path.$desc_file;
            $endorse->save();
            //Encrypt
            $main_driver_info = $endorse->owner()->first();
            $fileName = $data->policy_com_number . '-' .$endorse_no."-"."Schedule.pdf";
            $encryptResult = $this->pdfEncrypt($data,$main_driver_info,$desc_path.$desc_file,$fileName);
            if(!$encryptResult){ //เข้ารหัสล้มเหลว
                $this->pdfEncryptLocal($main_driver_info,$directory_path.$desc_path.$desc_file); //เข้ารหัสด้วยตัวเอง
                logTransaction($order->order_number,"เข้ารหัส Compulsory PDF ล้มเหลว","FAIL");  
            }
            logTransaction($order->order_number,"สร้าง Compulsory PDF สำเร็จ","SUCCESS");    
            return [
                'status' => 'SUCCESS'
            ];


        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => 'ERROR',
                'message' => $e->getMessage()
            ];
        }
        
    }




    private function checkRunningRecord(){
        try{
            /* Get Runing Number */
            $running_year = date('Y');
            $running_month = date('m');
            $cutoff = Cutoff::where([
                'years' => $running_year,
                'months' => $running_month
            ])->first();
            if(!empty($cutoff)){
                if(date('Y-m-d') > date('Y-m-d',strtotime($cutoff->cutoff_date))){
                    $running_month+=1;
                    if($running_month>12){
                        $running_month = 1;
                        $running_year+=1;

                    }
                }
            }
            $record = RunningNumber::where('year',$running_year)->first();
            if(empty($record)){
                RunningNumber::create([
                    'order' => 0,
                    'payment' => 0,
                    'policy_av5' => 0,
                    'policy_av3' => 0,
                    'misc' => 0,
                    'com' => 0,
                    'tax' => 0,
                    'year' => $running_year,
                ]);
            }
            return true;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function generatePolicyNumber($type,$runing_number,$years,$months){
        $months = str_pad($months,2,"0",STR_PAD_LEFT);
        $prefix = "";
        $postfix = "-MOL01-".$years."-".$months;
        $result = "";
        if($type==1 || $type==2){
            $prefix = "HQ-AV5-";
        }else if($type==3){
            $prefix = "HQ-AV3-";
        }else if($type=="addon"){
            $prefix = "HQ-SMA-";
        }else if($type=="com"){
            $prefix = "HQ-AC3-";
        }
        return $prefix.$runing_number.$postfix;
    }

    public function generateTaxNoteNumber($runing_number,$years,$months){
        $months = str_pad($months,2,"0",STR_PAD_LEFT);
        $prefix = "HQ";
        $new_runing_number = str_pad(intval($runing_number),6,"0",STR_PAD_LEFT);
        return $prefix."-".$years."-".$months."-T-V-".$new_runing_number;
    }

    private function setPolicy($data,$order){
        $result = [
            'status' => false,
            'policy_id' => null
        ];
        try{
            $id = Policy::create($data)->id;
            if(!empty($id)){
                // Create Endorse
                $endorse = [
                    'policy_id' => $id,
                    'endorse_no' => '00',
                    'owner' => $order->main_driver,
                    'driver1' => $order->driver1,
                    'driver2' => $order->driver2,
                ];
                $endorse = Endorse::create($endorse);
                logTransaction($order->order_number,"สร้าง Endorse: ".$endorse->id,"SUCCESS");
                $result['status'] = true;
                $result['policy_id'] = $id;
            }else{
                $result['status'] = false;
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            $result['status'] = false;
        }
        return $result;
    }

    private function mergeMiscPdf($pdfHandle,$order){
        $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();

        //Flood Wording File
        if(!empty($order->flood_gross_premium) && $order->flood_gross_premium!=0){ 
            if(Storage::disk('pdf')->exists('/plan/10.pdf')){
                $wording_file = $directory_path."/plan/10.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/10.pdf not found!");
            }
        }
        // HB Wordding File
        if(!empty($order->hb_gross_premium) && $order->hb_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/11.pdf')){
                $wording_file = $directory_path."/plan/11.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/11.pdf not found!");
            }
        }
        // Taxi Wordding File
        if(!empty($order->taxi_gross_premium) && $order->taxi_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/12.pdf')){
                $wording_file = $directory_path."/plan/12.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/12.pdf not found!");
            }
        }
        // Theft Wordding File
        if(!empty($order->theft_gross_premium) && $order->theft_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/13.pdf')){
                $wording_file = $directory_path."/plan/13.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/13.pdf not found!");
            }
        }
        // Carloss Wordding File
        if(!empty($order->carloss_gross_premium) && $order->carloss_gross_premium!=0){
            if(Storage::disk('pdf')->exists('/plan/14.pdf')){
                $wording_file = $directory_path."/plan/14.pdf";
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: /plan/14.pdf not found!");
            }
        }
        
    }

    private function mergeFilePdf($pdfHandle,$filepath = []){
        $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
        foreach($filepath as $path){
            if(Storage::disk('pdf')->exists($path)){
                $wording_file = $directory_path.$path;
                $pdfHandle->addPDF($wording_file, 'all');
            }else{
                Log::error("Create PDF Faild: ".$path." not found!");
            }
        }
    }

    private function generatePDFFileName($prefix,$data,$encrypted = false){
        $date_start = strtotime($data->insurance_start);
        $date_end = strtotime($data->insurance_expire);
        $date_start = date('Ymd',$date_start);
        $date_end = date('Ymd',$date_end);
        $endorse = $data->endorse()->first();
        if($encrypted){
            $result = $prefix."_".$data->policy_number."_".iconv('UTF-8','TIS-620',$endorse->owner()->first()->name)." ".iconv('UTF-8','TIS-620',$endorse->owner()->first()->lastname).
        "_".$data->car_chassis_number."_".iconv('UTF-8','TIS-620',str_replace(" ","-",$data->car_licence))."_".$date_start."_".$date_end."_".$data->tax_note_number.".pdf";
        }else{
            $result = $prefix."_".$data->policy_number."_".$endorse->owner()->first()->name." ".$endorse->owner()->first()->lastname.
        "_".$data->car_chassis_number."_".str_replace(" ","-",$data->car_licence)."_".$date_start."_".$date_end."_".$data->tax_note_number.".pdf";
        }
        
        return $result;
    }

    public function sendFtp($filePath,$fileName){
        $fileContents = Storage::disk('pdf')->get($filePath);
        Storage::disk('sftp')->put($fileName, $fileContents);
    }
    
    private function generatePDFCompulsoryFileName($prefix,$data,$encrypted = false){
        $date_start = strtotime($data->compulsory_start);
        $date_end = strtotime($data->compulsory_expire);
        $date_start = date('Ymd',$date_start);
        $date_end = date('Ymd',$date_end);
        $endorse = $data->endorse()->first();
        if($encrypted){
            $result = $prefix."_".$data->policy_com_number."_".iconv('UTF-8','TIS-620',$endorse->owner()->first()->name)." ".iconv('UTF-8','TIS-620',$endorse->owner()->first()->lastname).
        "_".$data->car_chassis_number."_".iconv('UTF-8','TIS-620',str_replace(" ","-",$data->car_licence))."_".$date_start."_".$date_end."_".$data->com_tax_note_number.".pdf";
        }else{
            $result = $prefix."_".$data->policy_com_number."_".$endorse->owner()->first()->name." ".$endorse->owner()->first()->lastname.
        "_".$data->car_chassis_number."_".str_replace(" ","-",$data->car_licence)."_".$date_start."_".$date_end."_".$data->com_tax_note_number.".pdf";
        }
        
        return $result;
    }

    private function pdfEncrypt($policy,$driver,$path,$fileName){
        try{
            ini_set('max_execution_time',300);
            $resultStatus = false;
            $password = date('dMY', strtotime($driver->birth_date)).substr($driver->idcard,0,4);
            $filesContents = Storage::disk('pdf')->get($path);
            $signature = new Signature();
            $requestParams[] = array(
                'filename' => $fileName,
                'password' => $password,
                'fileStream' => base64_encode($filesContents)
            );
            $logData = [
                'policy_id' => $policy->id,
                'policy_number' => $policy->policy_number,
                'data' => $fileName,
                'response' => null
            ];

            $response = $signature->request($requestParams);
            $result = json_decode($response);

            if (empty($result->responseHeader->statusCode)) {
                $logData['status'] = 'FAIL';
                $logData['response'] = htmlspecialchars($response);

            }else{
                if($result->responseHeader->statusCode == '2000'){
                    foreach($result->responseDetail as $item){
                        if(!empty($item->filename)){
                            Storage::disk('pdf')->put($path,base64_decode($item->fileStream));
                        }
                    }
                    $logData['status'] = 'SUCCESS';
                    $logData['response'] = $fileName;
                    $resultStatus = true;
                }else{
                    $logData['status'] = 'FAIL';
                    $logData['response'] = htmlspecialchars($response);
                    $resultStatus = false;
                }
            }

            SignatureLog::create($logData);
            return $resultStatus;
        }catch (\Exception $e) {
            Log::error("Error:".$e->getMessage());
            return false;
        }
    }


    // Local Method -------------------------------------------------
    private function pdfEncryptLocal ($driver,$path){
        if(!empty($driver)){
            $pdf = new FPDI_Protection();
            $pagecount = $pdf->setSourceFile($path);
            $password = date('dMY', strtotime($driver->birth_date)).substr($driver->idcard,0,4);
            for ($loop = 1; $loop <= $pagecount; $loop++) {
                $tplidx = $pdf->importPage($loop);
                $pdf->addPage();
                $pdf->useTemplate($tplidx);
            }

            $pdf->SetProtection(\FPDI_Protection::FULL_PERMISSIONS,$password,$password);
            $pdf->Output($path, 'F');
            return true;
        }
        return false;
    }
    

}