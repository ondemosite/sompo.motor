<?php

namespace App\Library;
use App\Models\Policy;
use App\Models\Configs;
use App\Models\BarcodeLog;
use Illuminate\Support\Facades\Log;

class Compulsory {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */
   //uat
   protected $header = null;
   
   
    // protected $_urlRefreshToken = 'https://services.sompo.co.th/fw010/v1/auth';
    // protected $_urlAccessToken = 'https://services.sompo.co.th/fw010/v1/auth/token';
    // protected $_endPoint = 'https://services.sompo.co.th/fw011/v1/barcode';
    protected $_urlRefreshToken = 'https://partner.sompo.co.th/fw010_uat/v1/auth';
    protected $_urlAccessToken = 'https://partner.sompo.co.th/fw010_uat/v1/auth/token';
    protected $_endPoint = 'https://partner.sompo.co.th/fw011_uat/v1/barcode';

   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */

    function __construct() {
      $this->_get_header();
    }

    function _get_header(){
        $this->header = array(
          'Content-Type: application/json',
        );
    }

    public function getBarcode($policyNumber){
      $refreshToken = $this->getRefreshToken();
      if($refreshToken){
        $accessToken = $this->getAccessToken($refreshToken);
        if($accessToken){
          $stickerNo = $this->request($accessToken,$policyNumber);
          if($stickerNo) return $stickerNo;
        }
      }
      return false;
    }

    public function getRefreshToken(){
        $header = [
          'Content-Type: application/json',
          'X-USERNAME: user@sompo.co.th', 
          'X-PASSWORD: p@ssw0rd'
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->_urlRefreshToken,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $header,
          CURLOPT_HEADER => 1
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if(isset($info['http_code'])){
          if($info['http_code'] == 200){
              $headers = $this->getResponseHeader($response);
              if(!empty($headers['X-REFRESH-TOKEN'])){
                return $headers['X-REFRESH-TOKEN'];
              }
          }
        }
        
        if ($err) {
          BarcodeLog::create([
            'data' => '',
            'response' => $err,
            'status' => 'FAIL',
          ]);
          Log::info("cURL Error #:" . $err);
        }
        return false;
    }

    public function getAccessToken($refreshToken){
        $header = [
          'Authorization: '.$refreshToken
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->_urlAccessToken,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $header,
          CURLOPT_HEADER => 1
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if(isset($info['http_code'])){
          if($info['http_code'] == 200){
              $headers = $this->getResponseHeader($response);
              if(!empty($headers['X-ACCESS-TOKEN'])){
                return $headers['X-ACCESS-TOKEN'];
              }
          }
        }
        
        if ($err) {
          BarcodeLog::create([
            'data' => '',
            'response' => $err,
            'status' => 'FAIL',
          ]);
          Log::info("cURL Error #:" . $err);
        }
        return false;
    }


   private function request($accessToken,$policyNumber) {

      $header = [
        'Authorization: '.$accessToken
      ];
      $query = http_build_query([
        'ref' => $policyNumber,
      ]);

      //cURL settings
      $curlOptions = array (
        CURLOPT_URL => $this->_endPoint."?".$query,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => $header,
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);

      //Checking for cURL errors
      if (curl_errno($ch)) {
        curl_close($ch);
        BarcodeLog::create([
          'data' => $this->_endPoint."?".$query,
          'response' => $response,
          'status' => 'FAIL',
        ]);
        return false;
      } else  {
         curl_close($ch);
         $sticker_no = null;
         $responseArray = json_decode($response,true);
         if(!empty($responseArray['result']['number'])){
             $sticker_no = $responseArray['result']['number'];
         }
         BarcodeLog::create([
          'data' => $this->_endPoint."?".$query,
          'response' => $response,
          'status' => 'SUCCESS',
        ]);

         return $sticker_no;
      }
   }

   private function getResponseHeader($response){
    $headers = array();
    $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
    foreach (explode("\r\n", $header_text) as $i => $line)
        if ($i === 0)
            $headers['http_code'] = $line;
        else
        {
            list ($key, $value) = explode(': ', $line);

            $headers[$key] = $value;
        }
    return $headers;
   }
}
?>