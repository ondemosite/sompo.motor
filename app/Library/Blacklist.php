<?php

namespace App\Library;
use App\Models\BlacklistLog;
use App\Models\Configs;
use Illuminate\Support\Facades\Log;

class Blacklist {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */
   //uat
   protected $header = null;
   
   
    protected $_endPoint = 'https://services.sompo.co.th/A0012/v1/Validate/All';
    protected $_lang = 'th';
    protected $_channel = 'motor';
    
   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */

    function __construct() {
      $this->_get_header();
    }

    function _get_header(){
        $this->header = array(
          'Content-Type: application/json',
        );
    }


   public function checkBlacklist($param = null){
    try{
      if(!empty($param)){
        $request = [
            'ssn' => $param['idcard'],
            'firstName' => $param['firstname'],
            'lastName' => $param['lastname'],
        ];

        $response = $this->request($request);
        $decode = json_decode($response,true);

        $returnResponse = [
          'is_pass' => false,
          'message' => ''
        ];
        $connectStatus = false;
        if(isset($decode['responseHeader'])){
          $connectStatus = true;
          if($decode['responseHeader']['statusCode']==2000){
            $responseStatus = $decode['responseDetail']['isPass'];
            $returnResponse['is_pass'] = $responseStatus;
            if(!$responseStatus){
              $returnResponse['message'] = $decode['responseDetail']['message'];
            }
          }else{
            $returnResponse['message'] = 'ขออภัยขณะนี้ไม่สามารถให้บริการได้';
          }
        }
        
        BlacklistLog::create([
          'data' => json_encode($request),
          'response' => $response,
          'status' => $connectStatus==true?"SUCCESS":"FAIL"
        ]);

        
        return $returnResponse;
      }
      return false;
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return false;
    }
      
   }

   private function request($request) {
      //Building our NVP string
      $request = json_encode($request);

      //cURL settings
      $curlOptions = array (
         CURLOPT_URL => $this ->_endPoint."?lang=".$this->_lang."&channel=".$this->_channel,
         CURLOPT_VERBOSE => 1,
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_POST => 1,
         CURLOPT_POSTFIELDS => $request,
         CURLOPT_HTTPHEADER => $this->header,
         CURLOPT_SSL_VERIFYPEER => false, // ไม่แนะนำ
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);
      
      //Checking for cURL errors
      if (curl_errno($ch)) {
         $this -> _errors = curl_error($ch);
         curl_close($ch);
         return false;
         //Handle errors
      } else  {
         curl_close($ch);
         return $response;
      }
   }
}
?>