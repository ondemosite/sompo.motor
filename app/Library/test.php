<?php

namespace App\Lib\Signature;

use DB;

class Signature {

    /**
     * Last error message(s)
     * @var array
     */
    protected $_errors = array();

    /**
     * API Credentials
     * Use the correct credentials for the environment in use (Live / Sandbox)
     * @var array
     */
    //uat
    protected $header = array(
        'Content-Type: application/json',
        'X-AUTH-ID: F19958A3-3748-4A6B-97E3-160758F45EA8',
        'X-TOKEN-KEY: 0x00FDCDC15C454F4BAF0B7F29E9C54C270100000050EF2C4A43B66498D799DEE41BF04EA19F572E529949AC42E1788B61EF4C813873E38B5C3E5C3299025F29D3D58B85238A14E57D1C5208825363BF3B8EF3763847E04C690AED7EAFF7753C49154673DC8C0D002A0477A5E1A1873AD91E3A7637312F40A101699C5FE95BA8678C492F209D8B8E0D1C85AD601705D23446CFF9EA435841D68FD3645E867AF4962241AA88087F0C594EFCFD0852126C306ABA9DB721F69A0D7F0762B493C19406CBBCCCD4'
    );
    //uat
    //protected $_endPoint = 'https://services.sompo.co.th/FW005/v1/DigitalSignature/SignDocument';
    protected $_endPoint = 'https://partner.sompo.co.th/FW005/v1/DigitalSignature/SignDocument'; //test

    /**
     * Make API request
     *
     * @param string $method string API method to request
     * @param array $params Additional request parameters
     * @return array / boolean Response array / boolean false on failure
     */
    public function request($params = array()) {

        // get signature config
        $this->_get_header();

        //Building our NVP string
        $request = json_encode($params);

        //cURL settings
        $curlOptions = array(
            CURLOPT_URL => $this->_endPoint,
            CURLOPT_VERBOSE => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_HTTPHEADER => $this->header,
            //CURLOPT_SSL_VERIFYPEER => false,
            //CURLOPT_SSL_VERIFYHOST => false
        );

        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);

        //Sending our request - $response will hold the API response
        $response = curl_exec($ch);

        //Checking for cURL errors
        if (curl_errno($ch)) {
            $this->_errors = curl_error($ch);
            curl_close($ch);
            return false;
            //Handle errors
        } else {
            curl_close($ch);
            return $response;
        }
    }

    function _get_header() {
        $auth_id = null;
        $token = null;
        $data = DB::table('tokens')->where('token_name', '=', 'signature_id');
        $data->whereRaw('start_date <= \'' . date('Y-m-d') . '\'');
        $data->whereRaw('end_date >= \'' . date('Y-m-d') . '\'');

        $config = $data->first();
        if (!empty($config)) {
            $auth_id = $config->token_value;
        }
        $data = DB::table('tokens')->where('token_name', '=', 'signature_token');
        $data->whereRaw('start_date <= \'' . date('Y-m-d') . '\'');
        $data->whereRaw('end_date >= \'' . date('Y-m-d') . '\'');

        $config = $data->first();
        if (!empty($config)) {
            $token = $config->token_value;
        }
        $this->header = array(
            'Content-Type: application/json',
            'X-AUTH-ID: ' . trim($auth_id),
            'X-TOKEN-KEY: ' . trim($token)
        );
    }

}

?>