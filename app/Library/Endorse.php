<?php

namespace App\Library;
use App\Models\Policy;
use App\Models\EndorseLog;
use App\Models\Configs;
use App\Models\ScheduleResend;
use Illuminate\Support\Facades\Log;
use PHPMailer\PHPMailer;

class Endorse {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */
   //uat
   protected $header = null;
   
   //uat
    protected $_endPoint = 'https://partner.sompo.co.th/A0048_UAT/Policy/GenPolicy';
    //protected $_endPoint = 'https://services.sompo.co.th/A0048/Policy/GenPolicy';
    
   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */
    function __construct() {
        $this->_get_header();
    }

    protected function _get_header(){
        $auth_id = null;
        $token = null;
        $authorize = null;

        $configId = Configs::where('config_code','=','endorse_id')
        ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
        if(!empty($configId)){
           $auth_id = $configId->config_value;
        }
        $configToken = Configs::where('config_code','=','endorse_token')
        ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
        if(!empty($configToken)){
            $token = $configToken->config_value;
        }
        $configAuth = Configs::where('config_code','=','endorse_authorize')
        ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
        if(!empty($configAuth)){
                $authorize = $configAuth->config_value;
        }
        $this->header = array(
            'Content-Type: application/json',
            'X-USERNAME: ' . trim($auth_id), 
            'X-PASSWORD: ' . trim($token),
            'Authorization: ' . trim($authorize)
        );
    }

   public function request($policy,$admin = null,$force = false) {

      if(!empty($policy)){
        $result = null;
        $generate_number = "MO".date('Ymdhis');
        $order = $policy->order()->first();
        $request = $this->formData($policy,$generate_number,$force);
        //return $request;
        //cURL settings
        $curlOptions = array (
           CURLOPT_URL => $this->_endPoint,
           CURLOPT_VERBOSE => 1,
           CURLOPT_RETURNTRANSFER => 1,
           CURLOPT_POST => 1,
           CURLOPT_POSTFIELDS => $request,
           CURLOPT_HTTPHEADER => $this->header,
        );

        $ch = curl_init();
        curl_setopt_array($ch,$curlOptions);

        //Sending our request - $response will hold the API response
        $response = curl_exec($ch);  

        //$response = true;
        
        //Checking for cURL errors
        if (curl_errno($ch)) { //Handle errors
          $this -> _errors = curl_error($ch);
          curl_close($ch);
          $result = false;
        } else  { 
          curl_close($ch);
          $result = $response;
        }

        $status = 'FAIL';
        $responseArray = json_decode($response,true);
        if(!empty($responseArray['responseHeader']['statusCode'])){
          if($responseArray['responseHeader']['statusCode'] == "2000"){
            $status = "SUCCESS";
          }
        }

        //SetLog
        EndorseLog::create([
          'policy_id' => $policy->id,
          'endorse_no' => $policy->endorse()->first()->endorse_no,
          'av_reference_no' => ($policy->insurance_plan_id==3?'AV3':'AV5').$generate_number,
          'ac_reference_no' => "AC3".$generate_number,
          'data' => $request,
          'response' => $response,
          'status' => $status,
          'created_by' => $admin
        ]);

        if($status == "SUCCESS"){
          logTransaction($order->order_number,"ส่ง Endrose สำเร็จ","SUCCESS");
          return true;
        }else{
          logTransaction($order->order_number,"ส่ง Endrose ล้มเหลว","FAIL");

          $scheduleResend = ScheduleResend::where([
            'type' => 'ENDORSE',
            'policy_id' => $policy->id,
          ])->first();
          if(empty($scheduleResend)){
            ScheduleResend::create([
                'type' => 'ENDORSE',
                'policy_id' => $policy->id,
                'retry_remaining' => 3,
                'complete_status' => 'WAITING',
                'process_status' => 0
            ]);
          }
          
          return false;
        }

        
      }
   }
   
   public function get($endPoint = null) {

      //cURL settings
      $curlOptions = array (
         CURLOPT_URL => $endPoint,
         CURLOPT_VERBOSE => 1,
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_HTTPHEADER => $this->header,
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);
      
      //Checking for cURL errors
      if (curl_errno($ch)) {
         $this -> _errors = curl_error($ch);
         curl_close($ch);
         return false;
         //Handle errors
      } else  {
         curl_close($ch);
         return $response;
      }
   }

   private function formData($policy,$generate_number,$force = false){
        $order = $policy->order()->first();
        $endorse = $policy->endorse()->first();
        $owner = $endorse->owner()->first();
        $driver1 = $endorse->driver1()->first();
        $driver2 = $endorse->driver2()->first();
        $vehicle = $policy->vehicle_info()->first();
        $promotion = $order->promotion()->first();
        $payment = $order->payment()->first();
        $car_licence = explode(" ",$policy->car_licence);
        $passenger = explode("+",$vehicle->driver_passenger);
        $car_province_name = $policy->car_province()->first();
        $product_code = "";

        logTransaction($order->order_number,"ดำเนินการสร้าง Endorse Form","SUCCESS");

        if($policy->insurance_plan_id==1){
          $product_code = "MOL 2+";
        }else if($policy->insurance_plan_id==2){
          $product_code = "MOL 3+";
        }else{
          $product_code = "MOL 3";
        }

        $contractData = [
          'package_number' => $this->generatePackageNumber($policy),
          'thai_english_policy_flag' => "T",
          'beneficairy_name' => $owner->prefix_name.$owner->name." ".$owner->lastname,
        ];

        $vehicleData = [
          'motor_code'=> strval($vehicle->mortor_code_av),
          'vehicle_make' => strval($vehicle->brand),
          'vehicle_model' => strval($vehicle->model),
          'vehicle_model_type' => strval($vehicle->model_type_full),
          'vehicle_body_type' => str_replace(' ', '',$vehicle->body_type),
          'auto_type' => "C",
          'vehicle_prefix_license_no' => strval($car_licence[0]),
          'vehicle_license_no' => strval($car_licence[1]),
          'vehicle_license_province_name' => !empty($car_province_name)?$car_province_name->name_th:'',
          'vehicle_chassis_no' => strtoupper($policy->car_chassis_number),
          'vehicle_engine_no' => '',
          'vehicle_color' => '',
          'vehicle_engine_capacity' => $vehicle->cc,
          'vehicle_reg_year' => strval($vehicle->year)
        ];

        $policyData = [
          'reference_no' => strval(($policy->insurance_plan_id==3?'AV3':'AV5').$generate_number),
          'policy_line_of_business' => $policy->insurance_plan_id==3?'AV3':'AV5',
          'policy_no' => strval($policy->policy_number),
          'policy_endorsement_no' => strval($endorse->endorse_no),
          'link_reference_no' => '',
          'policy_effective_date' => strval(date('Y-m-d',strtotime($policy->insurance_start))),
          'policy_effective_time' => strval(date('H:i',strtotime($policy->insurance_start))),
          'policy_expiration_date' => strval(date('Y-m-d',strtotime($policy->insurance_expire))),
          'entry_date' => $force?date('Y-m-d'):strval(date('Y-m-d',strtotime($payment->created_at))),
          'sign_date' => $force?date('Y-m-d'):date('Y-m-d',strtotime($payment->paid_date)),
          'product_code' => $product_code,
          'campaign_code' => !empty($promotion)?strval($promotion->code):'',
          'campaign_percent' => !empty($promotion)?round($promotion->discount,2):0,
          'campaign_amount' => round($order->net_discount + $order->direct,2),
          'vehicle_stricker_no' => "",
          'policy_unit_si_amount' => round($order->od_si,2),
          'basic_premium' => round($order->based_premium,2),
          'av5_basic_premium' => round($order->od_based_premium,2),
          'av5_sum_insured' => round($order->insurance_ft_si,2),
          'acovod_amount' => round($order->insurance_deduct,2),
          'acovod_amount_discount' => round($order->deduct,2),
          'acoved_percent' => round($order->ncb_percent,2),
          'acoved_amount' => round($order->ncb,2),
          'acovfd_percent' => round($order->fleet_percent,2),
          'acovfd_amount' => round($order->fleet,2),
          'acovtv_percent' => round($order->cctv_percent,2),
          'acovtv_amount' => round($order->cctv_discount,2),
          'acovdd_percent' => round($order->direct_percent,2),
          'acovdd_amount' => round($order->direct,2),
          'acovot_percent' => 0,
          'acovot_amount' => 0,
          'addi_ttl_loss_premium' => round($order->carloss_net_premium,2),
          'addi_ttl_loss_si' => round($order->addon_carloss,2),
          'addi_hb_premium' => round($order->hb_net_premium,2),
          'addi_hb_si' => round($order->addon_hb,2),
          'addi_flood_premium' => round($order->flood_net_premium,2),
          'addi_flood_si' => round($order->addon_flood,2),
          'addi_theft_premium' => round($order->theft_net_premium,2),
          'addi_theft_si' => round($order->addon_theft,2),
          'addi_taxi_premium' => round($order->taxi_net_premium,2),
          'addi_taxi_si' => round($order->addon_taxi,2),
          'od_premium_amount' => round($order->od_based_premium,2),
          'basic_premium_cover' => round($order->basic_premium_cover,2),
          'odplus_premium' => round($order->od_total_premium,2),
          'gprm' => round($order->insurance_net_premium,2),
          'stamp' => round($order->stamp,2),
          'tax' => round($order->vat,2),
          'tax_note_no' => $policy->tax_note_number,
          'payment_no' => $payment->payment_no,
          'tax_note_date' => $force?date('Y-m-d'):date('Y-m-d',strtotime($payment->paid_date)),
          'endt_reason_code' => "",
          'endt_type' => "",
        ];

        $compulsoryData = [];
        if(!empty($policy->policy_com_number)){
          $compulsoryData = [
            'reference_no' => strval("AC3".$generate_number),
            'policy_line_of_business' => "AC3",
            'policy_no' => strval($policy->policy_com_number),
            'policy_endorsement_no' => strval($endorse->endorse_no),
            'link_reference_no' => $policy->policy_number,
            'policy_effective_date' => strval(date('Y-m-d',strtotime($policy->compulsory_start))),
            'policy_effective_time' => strval(date('H:i',strtotime($policy->compulsory_start))),
            'policy_expiration_date' => strval(date('Y-m-d',strtotime($policy->compulsory_expire))),
            'entry_date' => $force?date('Y-m-d'):strval(date('Y-m-d',strtotime($payment->created_at))),
            'sign_date' => $force?date('Y-m-d'):date('Y-m-d',strtotime($payment->paid_date)),
            'product_code' => "",
            'campaign_code' => "",
            'campaign_percent' => 0,
            'campaign_amount' => 0,
            'vehicle_stricker_no' => $policy->sticker_no,
            'policy_unit_si_amount' => 0,
            'basic_premium' => 0,
            'av5_basic_premium' => 0,
            'av5_sum_insured' => 0,
            'acovod_amount' => 0,
            'acovod_amount_discount' => 0,
            'acoved_percent' => 0,
            'acoved_amount' => 0,
            'acovfd_percent' => 0,
            'acovfd_amount' => 0,
            'acovtv_percent' => 0,
            'acovtv_amount' => 0,
            'acovdd_percent' => 0,
            'acovdd_amount' => 0,
            'acovot_percent' => 0,
            'acovot_amount' => 0,
            'addi_ttl_loss_premium' => 0,
            'addi_ttl_loss_si' => 0,
            'addi_hb_premium' => 0,
            'addi_hb_si' => 0,
            'addi_flood_premium' => 0,
            'addi_theft_premium' => 0,
            'addi_theft_si' => 0,
            'addi_taxi_premium' => 0,
            'addi_taxi_si' => 0,
            'od_premium_amount' => 0,
            'basic_premium_cover' => 0,
            'odplus_premium' => 0,
            'gprm' => round($order->compulsory_net_premium,2),
            'stamp' => round($order->compulsory_stamp,2),
            'tax' => round($order->compulsory_vat,2),
            'tax_note_no' => $policy->com_tax_note_number,
            'payment_no' => $payment->payment_no,
            'tax_note_date' => $force?date('Y-m-d'):date('Y-m-d',strtotime($payment->paid_date)),
            'endt_reason_code' => "",
            'endt_type' => "",
          ];
        }

        $insuredData = [
          'salutation' => $owner->prefix_name,
          'insured_first_name' => $owner->name,
          'insured_last_name' => $owner->lastname,
          'insured_type' => "P",
          'insured_sex' => ($owner->gender=="MALE"?"M":"F"),
          'id_card_no' => strval($owner->idcard),
          'passport_no' => "",
          'birth_date' => date('Y-m-d',strtotime($owner->birth_date)),
          'address_line1' => strval($owner->address),
          'tumbon_code' => strval($owner->subdistrict),
          'district_code' => strval($owner->district),
          'province_code' => strval($owner->province),
          'postal_code' => strval($owner->postalcode),
          'mobile_phone_no_1' => strval($owner->tel),
          'email_address' => strval($owner->email),
        ];

        $payeeData = [
          'payee_salutation' => $owner->prefix_name,
          'payee_first_name' => $owner->name,
          'payee_last_name' => $owner->lastname,
          'payee_type' => "P",
          'payee_branch_tax_number' => "",
          'payee_sex' => ($owner->gender=="MALE"?"M":"F"),
          'payee_id_card_no' => strval($owner->idcard),
          'payee_address_line1' => strval($owner->address),
          'payee_tumbon_code' => strval($owner->subdistrict),
          'payee_district_code' => strval($owner->district),
          'payee_province_code' => strval($owner->province),
          'payee_postal_code' => strval($owner->postalcode),
          'payee_mobile_phone_no_1' => strval($owner->tel),
          'payee_email_address' => strval($owner->email),
        ];

        $driver = [
          "driver1_name" => !empty($driver1)?('คุณ'.$driver1->name.' '.$driver1->lastname):"",
          "driver1_birth_date" => !empty($driver1)?date('Y-m-d',strtotime($driver1->birth_date)):"",
          "driver1_sex" => !empty($driver1)?($driver1->gender=="MALE"?"M":"F"):"",
          "driver1_id_card_no" => !empty($driver1)?$driver1->idcard:"",
          "driver1_license_no" => !empty($driver1)?$driver1->licence:"",
          "driver2_name" => !empty($driver2)?('คุณ'.$driver2->name.' '.$driver2->lastname):"",
          "driver2_birth_date" => !empty($driver2)?date('Y-m-d',strtotime($driver2->birth_date)):"",
          "driver2_sex" => !empty($driver2)?($driver2->gender=="MALE"?"M":"F"):"",
          "driver2_id_card_no" => !empty($driver2)?$driver2->idcard:"",
          "driver2_license_no" => !empty($driver2)?$driver2->licence:"",
        ];
        

        $formData = [];
        $formData['contract'] = $contractData;
        $formData['vehicle'] = $vehicleData;
        $formData['policy'] = !empty($compulsoryData)?[$compulsoryData,$policyData]:[$policyData];
        $formData['insured'] = $insuredData;
        $formData['payee'] = $payeeData;
        $formData['driver'] = $driver;

        // echo "<pre>";
        // echo json_encode(array($formData),JSON_PRETTY_PRINT);
        // echo "</pre>";

        return json_encode(array($formData),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
   }

   private function generatePackageNumber($policy){
    // Base Data
    $vehicle = $policy->vehicle_info()->first();
    $order = $policy->order()->first();
    $endorse = $policy->endorse()->first();
    $driver1 = $endorse->driver1()->first();
    $driver2 = $endorse->driver2()->first();
    //----------
    //$project = "MO";
    $project = "";
    $coverageType = "";
    if($policy->insurance_plan_id == 1) $coverageType = "2P";
    else if($policy->insurance_plan_id == 2) $coverageType = "3P";
    else $coverageType = "3_";
    $motorCode = $vehicle->mortor_code_av;
    $garage = $order->insurance_garage_type == "GENERAL"?"N":"Y";
    $cctv = $order->car_cctv==1?'Y':'N';

    $cc = null;
    if($vehicle->mortor_code_av==110){
        if($vehicle->cc <= 2000){
            $cc = "<=2000CC";
        }else{
            $cc = ">2000CC";
        }
    }else{
        if($vehicle->cc <= 2000){
            $cc = "<=2000CC";
        }else if($vehicle->cc > 2000 && $vehicle->cc < 4000){
            $cc = "<=4TONS";
        }else{
            $cc = ">4TONS";
        }
    }

    $vehicleKey = "";
    if($cc == "<=2000CC") $vehicleKey = "1"; //01
    else if($cc == ">2000CC") $vehicleKey = "2"; //02
    else if($cc == "<=4TONS") $vehicleKey = "1"; //01
    else $vehicleKey = "2"; //02


    $minimunAge = "";
    $driver1Age = !empty($driver1)?getAge($driver1->birth_date):0;
    $driver2Age = !empty($driver2)?getAge($driver2->birth_date):0;
    if($driver1Age == 0 && $driver2Age==0){
      $minimunAge = "00";
    }else{
      if($driver1Age > $driver2Age && $driver2Age!=0){
        $minimunAge = $driver2Age;
      }else{
        $minimunAge = $driver1Age;
      }

      if($minimunAge <= 24) $minimunAge = 18;
      else if($minimunAge <= 28) $minimunAge = 25;
      else if($minimunAge <= 35) $minimunAge = 29;
      else if($minimunAge <= 50) $minimunAge = 36;
      else $minimunAge = 51;
    }
    $deduct = $order->insurance_deduct>0?"Y":"N";
    $flood = 'N';//$order->flood_gross_premium>0?"Y":"N";
    $bkk = $order->is_bangkok==1?'Y':'N';


    $od = "";
    if($policy->insurance_plan_id == 3){
      $od = "PD".str_pad(strval($order->insurance_ft_si/1000),4,"0",STR_PAD_LEFT); //ต้องแก้
    }else{
      $odN = "";
      if($order->od_si != null){
        if($order->od_si > 0){
          $odN = "O".str_pad(strval($order->od_si/1000),3,"0",STR_PAD_LEFT);
        }
      }
      $od .= $odN.str_pad(strval($order->insurance_ft_si/1000),3,"0",STR_PAD_LEFT);
    }

    $format = $project.$coverageType.$motorCode.$garage.$cctv.$vehicleKey.$minimunAge.$deduct.$flood.$bkk.$od;
    return $format;
 }

 public function alertToAdmin($policy_id){
  ini_set('max_execution_time',300);
  ini_set('memory_limit', '-1');
  $config = config('mail.endorse');
  $policy = Policy::where('id',$policy_id)->first();
  if(!empty($policy)){
    $mail = new PHPMailer\PHPMailer();
    $mail->SMTPDebug = 0;                                 
    $mail->isSMTP();           
    $mail->IsHTML(true);                          
    $mail->Host = config('mail.host');
    $mail->SMTPAuth = true;                             
    $mail->Username = config('mail.username');     
    $mail->Password = config('mail.password');    
    $mail->SMTPSecure = 'tls';                     
    $mail->Port = config('mail.port');      
    $mail->CharSet = "utf-8";
    $mail->SetFrom(config('mail.from.address'),config('mail.from.name'));
    $mail->AddAddress($config['admin']['email'],$config['admin']['name']);
    $mail->Subject = $config['subject'];

    $init = [
        'policy' => $policy,
        'date' => date('Y-m-d H:i:s'),
        'subject' => $config['subject']
    ];

    $html = view('emails.endorse',['init'=>$init]);
    $mail->Body  = $html;

    if ($mail->Send()) {
        return json_encode(['status'=>"SUCCESS"]);
    } else {
        return json_encode(['status'=>"FAIL",'reason'=>$mail->ErrorInfo]);
    }
  }
}

}
?>