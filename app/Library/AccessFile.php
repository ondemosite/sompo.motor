<?php

namespace App\Library;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Models\AccessFiles;
use Auth;

class AccessFile {

    public function setToken($params){
        try{
            $data = [
                'ref_id' => $params['ref_id'],
                'allow_files' => implode(',',$params['files']),
                'token' => $this->generateToken(50),
                'expire_date' => date('Y-m-d H:i:s', strtotime("+1 day"))
            ];
            return AccessFiles::create($data)->id;
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public function getUrl($id,$refId,$fileName,$disk){
        try{
            $record = AccessFiles::where('id',$id)->first();
            if(!empty($record)){
                $string = $record->token.",".$refId.",".$fileName.",".$disk;
                $url = route('access_file')."/".base64_encode($string);
                return $url;
            }else{
                return false;
            }
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    private function generateToken($length){
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
    
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    
        return $key;
    }

    public function checkAccess($encryptString){
        try{
            $stringDecode = base64_decode($encryptString);
            $explode = explode(",",$stringDecode);
            if(is_array($explode)){
                if(sizeof($explode) == 4){
                    $token = $explode[0];
                    $refId = $explode[1];
                    $fileName = $explode[2];
                    $disk = $explode[3];
                    $query = AccessFiles::where([
                        'ref_id' => $refId,
                        'token' => $token,
                    ]);
                    $record = $query->whereDate('expire_date','>=',date('Y-m-d H:i:s'))->first();
                    if(!empty($record)){
                        $allowFile = explode(',',$record->allow_files);
                        if(in_array($fileName,$allowFile)){
                            return true;
                        }
                    }
                }
            }
            return false;
               
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

 
   
}
?>