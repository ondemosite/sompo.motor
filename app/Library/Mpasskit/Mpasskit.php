<?php

namespace App\Library\Mpasskit;
use Illuminate\Support\Facades\Password;
use App\Library\Mpasskit\PassKit;
use App\Models\EpassToken;
use App\Models\PasskitLog;
use App\Models\Policy;
use Illuminate\Support\Facades\Log;

class Mpasskit {
    
    protected $_apiKey = "JbgXdAipwKeNyjnpelPfFFAzfKcFQtni";
    protected $_apkSecret = "ehUL1IUlSJtLmmu6TzCyUjt98sjMIhCkMF6Gm3mAftcyXyAGix4GqqhW3q9oV59sGeGv8lOV4AFwa1scmPaiQJAXgp9cPitOcLcp9rSRwHha4BDrIqud6wzEIEXB5Py0";
    
    /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */
   public function createPass($policy,$admin = null) {

        $order = $policy->order()->first();
        $endorse = $policy->endorse()->first();
        $owner = $endorse->owner()->first();
        $vehicle = $policy->vehicle_info()->first();

        try{
            $pk = new PassKit($this->_apiKey,$this->_apkSecret);
            logTransaction($order->order_number,"ดำเนินการสร้าง Passkit","SUCCESS");

            $motor_pdf = $this->generateDownloadLink("motor",$policy->id);
            $misc_pdf = $this->generateDownloadLink("misc",$policy->id);

            //Generate Strip Image
            $image_path = public_path('/passkit/images').'/strip.png';
            $image_uploaded = $this->_upload_image($pk,$image_path);

            // //Policy Addon
            // $policy_addon_detail = "";
            // if(!empty($policy->policy_addon_number)){
            //     $policy_addon_detail .= 'Policy No :  '.$policy->policy_addon_number;
            //     if(!empty($order->flood_gross_premium)) $policy_addon_detail.=($policy->locale=="th"?"\n- คุ้มครองน้ำท่วม":"\n- Flood Coverage</p>");
            //     if(!empty($policy->addon_carloss)) $policy_addon_detail.=($policy->locale=="th"?"\n- เงินชดเชยกรณีรถยนต์สูญหายหรือเสียหายสิ้นเชิงจากอุบัติเหตุ":"\n- The compensation in case of lost or damaged cars completely from the accident");
            //     if(!empty($policy->addon_hb)) $policy_addon_detail.=($policy->locale=="th"?"\n- เงินชดเชยรายได้รายวันกรณ๊รถยนต์เกิดอุบัติเหตุ":"\n- Daily income compensation for car accident cases, to admitted as a patients in the hospital");
            //     if(!empty($policy->addon_theft)) $policy_addon_detail.=($policy->locale=="th"?"\n- คุ้มครองโจรกรรมทรัพย์สินภายในรถยนต์":"\n- Coverage for theft of property inside the car");
            //     if(!empty($policy->addon_taxi)) $policy_addon_detail.=($policy->locale=="th"?"\n- ชดเชยค่าเดินทางต่อการเข้าซ่อม":"\n- Compensate for travel expenses during the car repair into garage");
            //     $policy_addon_detail.="\n<a href=\"".$misc_pdf."\">Download Miscellaneous Policy PDF</a>";
            // }else{
            //     $policy_addon_detail = ($policy->locale=="th"?"ไม่เลือกซื้อความคุ้มครองเสริม":"No extra coverage.");
            // }
            
            $data = array(
                'templateName' => config('passkit.template_name'),
                'dynamicData' => array(
                    'insurance_name' => (!empty($owner->prefix_name)?$owner->prefix_name:'คุณ').$owner->name.' '.$owner->lastname,
                    'insurance_type' => config('passkit.app'),
                    'insurance_plan' => strtoupper($policy->insurance_plan_name),
                    'inception_date' => date('d-M-Y',strtotime($policy->insurance_start)),
                    'conclusion_date' => date('d-M-Y',strtotime($policy->insurance_expire)),
                    'policy_detail' => 'Policy No :  '.$policy->policy_number.'
Insured name : '.$owner->name.' '.$owner->lastname.'
Insurance type : '.config('passkit.app').'
Insurance plan : '.strtoupper($policy->insurance_plan_name).'
Coverage : '.number_format($policy->insurance_ft_si)." ".($policy->locale=="th"?"บาท":"Baht").'
Car Info : '.$vehicle->brand.' '.$vehicle->model.' '.$vehicle->model_type.' '.$vehicle->year.'
Car Licence : '.str_replace(" ","-",$policy->car_licence).' '.$policy->car_province()->first()->name_th,
                    // 'policy_addon_detail' => $policy_addon_detail,
                    'policy_pdf' => $motor_pdf,
                    'mis_pdf' => $misc_pdf,
                    'notification_text' => ''
                ),
                'dynamicImages' => array(
                    'passbook' => array(
                        'strip' => $image_uploaded,
                    )
                )
            );

            $result = $pk->CreatePass($data);
            $passkit_link = null;
            $result_status = false;
            if(!empty($result)){
                $passkit_link = config("passkit.url").$result->id;  
                $result_status = true;
            }
            
            
            PasskitLog::create([
                'policy_id' => $policy->id,
                'endorse_no' => $endorse->endorse_no,
                'data' => json_encode($data),
                'response' => json_encode($result),
                'status' => $result_status?'SUCCESS':'FAIL',
                'created_by' => $admin
            ]);

            logTransaction($order->order_number,"สร้าง Passkit สำเร็จ","SUCCESS");
            return $passkit_link;

            
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            logTransaction($order->order_number,"CATCH สร้าง Passkit ล้มเหลว: ".$e->getMessage(),"FAIL");
            return null;
        }
       
   }
   
   function _upload_image($pk,$path){

       $img = array(
           'image' => $path
       );
       
       $response = $pk->UploadImage($img);
       if(!empty($response->path)){
           return $response->path;
       }
       return null;
   }

   public function getTemplate($name){
        $pk = new PassKit($this->_apiKey,$this->_apkSecret);

       return $pk->GetTemplate($name);
   }

   private function generateDownloadLink($type,$policy_id){
        $epass_data = [
            'type' => $type,
            'policy_id' => $policy_id,
            'token' => Password::getRepository()->createNewToken(),
            'expire_at' => null,
        ];
        $epass = EpassToken::create($epass_data)->id;
        if(!empty($epass)){
            $encrypted = encrypt(json_encode($epass_data));
            return route("api.web.download_epass")."?token=".$encrypted;
        }
        return false;
   }

}
?>