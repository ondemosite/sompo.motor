<?php

namespace App\Library;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Library\insuranceManager;
use App\Models\Policy;
use App\Models\EmailLog;
use App\Models\Endorse;
use App\Models\ConfirmPolicy;
use App\Models\ScheduleResend;
use App\Models\ScheduleResendLog;
use PHPMailer\PHPMailer;
use Auth;

class MyEmail {

    public function sendEmail($policy,$files = [],$admin = null){

        $order = $policy->order()->first();

        try{
            $imd = new insuranceManager();
            $endorse = $policy->endorse()->first();
            $directory_path = Storage::disk('pdf')->getDriver()->getAdapter()->getPathPrefix();
            $owner = $endorse->owner()->first();
            $ownerPrefix = $owner->prefix()->first();
            $sent_files = [];
            $log_files = [];

            logTransaction($order->order_number,"ดำเนินการส่ง EMAIL","SUCCESS");

            $policy_doc_path = $endorse->policy_document_path;
            $policy_original_invoice_path = $endorse->policy_original_invoice_path;
            $compulsory_doc_path = !empty($endorse->compulsory_document_path)?$endorse->compulsory_document_path:null;
            //$compulsory_original_invoice_path = !empty($endorse->compulsory_original_invoice_path)?$endorse->compulsory_original_invoice_path:null;


            if($this->checkSelectedFile($files,"policy")){
                if(!empty($policy_doc_path)){
                    $sent_files[] = [
                        'path' => $directory_path.$policy_doc_path,
                        'name' => $imd->generatePDFFileName("Ori",$policy,false),
                        'type' => 'application/pdf'
                    ];
                    $log_files[] = "Policy Motor";
                }
            }
            if($this->checkSelectedFile($files,"policy_ori_invoice")){
                if(!empty($policy_original_invoice_path)){
                    $sent_files[] = [
                        'path' => $directory_path.$policy_original_invoice_path,
                        'name' => $imd->generatePDFFileName("RecOri",$policy,false),
                        'type' => 'application/pdf'
                    ];
                    $log_files[] = "Policy Motor- Original Invoice";
                }
            }

            if($this->checkSelectedFile($files,"compulsory")){
                if(!empty($compulsory_doc_path)){
                    $sent_files[] = [
                        'path' => $directory_path.$compulsory_doc_path,
                        'name' => $imd->generatePDFCompulsoryFileName("Ori",$policy,false),
                        'type' => 'application/pdf'
                    ];
                    $log_files[] = "Policy Compulsory";
                }
            }
            
            // if($this->checkSelectedFile($files,"compulsory_ori_invoice")){
            //     if(!empty($compulsory_original_invoice_path)){
            //         $sent_files[] = [
            //             'path' => $directory_path.$compulsory_original_invoice_path,
            //             'name' => $imd->generatePDFCompulsoryFileName("RecOri",$policy,false),
            //             'type' => 'application/pdf'
            //         ];
            //         $log_files[] = "Compulsory - Original Invoice";
            //     }
            // }

            $params = [
                'info' => [
                    'to' => $owner->email,
                    'name' => (!empty($ownerPrefix)?$ownerPrefix->name:'คุณ').$owner->name." ".$owner->lastname,
                    'fullname_th' => (!empty($ownerPrefix)?$ownerPrefix->name:'คุณ').$owner->name." ".$owner->lastname,
                    'fullname_en' => (!empty($ownerPrefix)?$ownerPrefix->name_en:'Khun').$owner->name." ".$owner->lastname,
                ],
                'files' => $sent_files,
                'confirm_link' => $this->generateConfirmLink($policy),
                'policy' => $policy

            ];
            $response = $this->request($params);
            //Log Email ----------------------------
            $response_array = json_decode($response,true);
            EmailLog::create([
                'policy_id' => $policy->id,
                'endorse_no' => $endorse->endorse_no,
                'data' => json_encode($log_files), //Generate File
                'response' => $response,
                'status' => $response_array['status'],
                'created_by' => $admin
            ]);
            //---------------------------------------
            if($response_array['status']=="SUCCESS"){
                logTransaction($order->order_number,"ส่ง EMAIL สำเร็จ","SUCCESS");
                return true;
            }else{
                logTransaction($order->order_number,"ส่ง EMAIL ล้มเหลว","FAIL");

                $scheduleResend = ScheduleResend::where([
                    'type' => 'EMAIL',
                    'policy_id' => $policy->id,
                ])->first();
                if(empty($scheduleResend)){
                    ScheduleResend::create([
                        'type' => 'EMAIL',
                        'policy_id' => $policy->id,
                        'retry_remaining' => 3,
                        'complete_status' => 'WAITING',
                        'process_status' => 0
                    ]);
                }
                return false;
            } 
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            logTransaction($order->order_number,"CATCH ส่ง EMAIL ล้มเหลว: ".$e->getMessage(),"FAIL");

            $scheduleResend = ScheduleResend::where([
                'type' => 'EMAIL',
                'policy_id' => $policy->id,
            ])->first();
            if(empty($scheduleResend)){
                ScheduleResend::create([
                    'type' => 'EMAIL',
                    'policy_id' => $policy->id,
                    'retry_remaining' => 3,
                    'complete_status' => 'WAITING',
                    'process_status' => 0
                ]);
            }
            return false;
        }
    }

    private function request($params){
        ini_set('max_execution_time',300);
        ini_set('memory_limit', '-1');

        $mail = new PHPMailer\PHPMailer();
        $mail->SMTPDebug = 0;                                 
        $mail->isSMTP();           
        $mail->IsHTML(true);                          
        $mail->Host = config('mail.host');
        $mail->SMTPAuth = true;                             
        $mail->Username = config('mail.username');     
        $mail->Password = config('mail.password');    
        $mail->SMTPSecure = 'tls';                     
        $mail->Port = config('mail.port');      
        $mail->CharSet = "utf-8";
        $mail->SetFrom(config('mail.from.address'),config('mail.from.name'));
        $mail->AddAddress($params['info']['to'],$params['info']['name']);
        $mail->Subject = config('mail.subject')." ".$params['policy']->policy_number;


        // Add Attachment Files
        foreach($params['files'] as $file){
            $mail->addAttachment($file['path'], $file['name'],'base64', $file['type']);
        }

        $init = [
            'fullname_th' => $params['info']['fullname_th'],
            'fullname_en' => $params['info']['fullname_en'],
            'confirm_link' => $params['confirm_link'],
            'is_compulsory' => !empty($params['policy']->com_tax_note_number)?true:false,
            'car_licence' => $params['policy']->car_licence
        ];
        $html = view('emails.policy',['init'=>$init]);
        $mail->Body  = $html;
        if ($mail->Send()) {
            return json_encode(['status'=>"SUCCESS"]);
        } else {
            return json_encode(['status'=>"FAIL",'reason'=>$mail->ErrorInfo]);
        }

    }

    private function generatePassword(){
        $policy_name = $this->endorse->owner()->first();
        $birthdate = $policy_name->birth_date;
        $month = date("m",strtotime($birthdate));
        $day = date("d",strtotime($birthdate));
        return $day.$month;
    }

    private function checkSelectedFile($files,$name){
        if(empty($files)){
            return true;
        }else{
            if(in_array($name,$files)) return true;
        }
        return false;
    }

    private function generateConfirmLink($policy){
        $url = route('web.confirm_policy');
        $generateLink = uniqid().str_replace("-","",$policy->policy_numbers);
        $id = ConfirmPolicy::create([
            'confirm_code' => $generateLink,
            'policy_id' => $policy->id,
            'status' => 0
        ])->id;
        if(!empty($id)){
            return $url."/".$generateLink;
        }else{
            return false;
        }
    }
   
}
?>