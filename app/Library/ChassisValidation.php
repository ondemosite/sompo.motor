<?php

namespace App\Library;
use App\Models\Configs;
use App\Models\ChassisLog;
use Illuminate\Support\Facades\Log;

class ChassisValidation {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */
   //uat
   protected $header = null;
   
   
    protected $_endPoint = 'https://partner.sompo.co.th/fw019_uat/v1/validate/chassisno'; //UAT
    //protected $_endPoint = 'https://services.sompo.co.th/fw019/v1/validate/chassisNo';
    
   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */

    function __construct() {
      $this->_get_header();
    }

    function _get_header(){
        $this->header = array(
          'Content-Type: application/json',
        );
    }


   public function validate($param = null){
    try{
      if(!empty($param)){
        $request = [
            'type' => $param['type'],
            'number' => $param['number'],
            'effective' => $param['effective'],
            'expiration' => $param['expiration'],
        ];

        $response = $this->request($request);
        $decode = json_decode($response,true);

        $connectStatus = false;
        $returnResponse = [
            'is_pass' => false,
            'message' => ''
        ];

        if(isset($decode['result']) || isset($decode['error'])){
            $connectStatus = true;
            if(isset($decode['result'])){
                if($decode['result']['isAvailable']){
                    $returnResponse['is_pass'] = true;
                }
            }else{
                $returnResponse['message'] = $decode['error']['message'];
            }
        }
        
        ChassisLog::create([
            'data' => json_encode($request),
            'response' => $response,
            'status' => $connectStatus==true?"SUCCESS":"FAIL"
        ]);

        
        return $returnResponse;
      }
      return false;
    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return false;
    }
      
   }

   private function request($request) {
      //Building our NVP string

      //cURL settings
      $curlOptions = array (
         CURLOPT_URL => $this ->_endPoint."?Type=".$request['type']."&Number=".$request['number']."&Effective=".$request['effective']."&Expiration=".$request['expiration'],
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_HTTPHEADER => $this->header,
         CURLOPT_SSL_VERIFYPEER => false, // ไม่แนะนำ
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);
      
      //Checking for cURL errors
      if (curl_errno($ch)) {
         $this -> _errors = curl_error($ch);
         curl_close($ch);
         return false;
         //Handle errors
      } else  {
         curl_close($ch);
         return $response;
      }
   }
}
?>