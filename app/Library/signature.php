<?php

namespace App\Library;
use App\Models\Configs;
class Signature {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */
   //uat
   protected $header = null;
   
   //uat
   protected $_endPoint = 'https://partner.sompo.co.th/FW005/v1/DigitalSignature/SignDocument';
   //protected $_endPoint = 'https://services.sompo.co.th/FW005/v1/DigitalSignature/SignDocument';
    
   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */

    function __construct() {
        $this->_get_header();
    }

    function _get_header(){
        $auth_id = null;
        $token = null;
        $configId = Configs::where('config_code','=','signature_id')
        ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
        if(!empty($configId)){
            $auth_id = $configId->config_value;
        }
        $configToken = Configs::where('config_code','=','signature_token')
        ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
        if(!empty($configToken)){
            $token = $configToken->config_value;
        }
        $this->header = array(
            'Content-Type: application/json',
            'X-AUTH-ID: ' . trim($auth_id), 
            'X-TOKEN-KEY: ' . trim($token)
        );
   }


   public function request($params = array()) {

      //Building our NVP string
      $request = json_encode($params);

      //cURL settings
      $curlOptions = array (
         CURLOPT_URL => $this ->_endPoint,
         CURLOPT_VERBOSE => 1,
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_POST => 1,
         CURLOPT_POSTFIELDS => $request,
         CURLOPT_HTTPHEADER => $this->header
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);
      
      //Checking for cURL errors
      if (curl_errno($ch)) {
         $this -> _errors = curl_error($ch);
         curl_close($ch);
         return false;
         //Handle errors
      } else  {
         curl_close($ch);
         return $response;
      }
   }
}
?>