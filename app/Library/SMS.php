<?php

namespace App\Library;
use App\Models\Policy;
use App\Models\SmsLog;
use App\Models\Configs;
use Illuminate\Support\Facades\Log;

class SMS {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */
   //uat
   protected $header = null;
   
    //protected $_endPoint = 'https://services.sompo.co.th/fw006/api/v1/SmsService/SendMessage'; //PRODUCTIONS
    protected $_endPoint = 'https://partner.sompo.co.th/FW006_UAT/api/v1/SmsService/SendMessage';
    
   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */

    function __construct() {
      $this->_get_header();
    }

    function _get_header(){
        $auth_id = null;
        $token = null;
        $configAuth = Configs::where('config_code','=','sms_id')
        ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
        if(!empty($configAuth)){
                $auth_id = $configAuth->config_value;
        }
        $configToken = Configs::where('config_code','=','sms_token')
        ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
        if(!empty($configToken)){
                $token = $configToken->config_value;
        }
        $this->header = array(
          'Content-Type: application/json',
          'X-AUTH-ID: ' . trim($auth_id), 
          'X-TOKEN-KEY: ' . trim($token)
        );
    }


   public function sendSMS($policy,$epass_link,$admin = null){
    $endorse = $policy->endorse()->first();
    $order = $policy->order()->first();
    try{

        logTransaction($order->order_number,"ดำเนินการส่ง SMS","SUCCESS");
        $device_list = [];
        $message = "";
        if($policy->locale=="th"){
            $message = "Sompo Thailand ขอขอบคุณท่านที่ไว้วางใจให้เราเป็นผู้ให้ความคุ้มครองท่านสำหรับประกันภัยรถยนต์ในครั้งนี้ กรมธรรม์ของท่านได้ถูกจัดส่งให้แล้วทาง E-mail ที่แจ้งไว้ สายด่วนแจ้งอุบัติเหตุ ตลอด 24 ชม. โทร 02-118-7400";
        }else{
            $message = "Thank you for choosing Sompo Thailand for your motor insurance. Your policy with coverage details has been sent to your e-mail for reference. In case of emergency, please contact us 02-118-7400 for 24 hours";
        }
        
        $data = [
            'to' => array($endorse->owner()->first()->tel),
            'text' => $message
        ];
        array_push($device_list,$data);

        $response = $this->request($device_list);
        $decode = json_decode($response,true);
        $status = false;
        
        if(isset($decode['responses'][0]['requestId'])){
          if(!empty($decode['responses'][0]['requestId'])){
            $status = true;
          }
        }
        SmsLog::create([
          'policy_id' => $policy->id,
          'endorse_no' => $endorse->endorse_no,
          'data' => json_encode($device_list),
          'response' => $response,
          'status' => $status==true?"SUCCESS":"FAIL",
          'created_by' => $admin
        ]);

        if($status){
          logTransaction($order->order_number,"ส่ง SMS สำเร็จ","SUCCESS");
        }else{
          logTransaction($order->order_number,"ส่ง SMS ล้มเหลว","FAIL");
        }
        return $status;

    }catch (\Exception $e) {
        Log::error($e->getMessage());
        logTransaction($order->order_number,"CATCH ส่ง SMS ล้มเหลว: ".$e->getMessage(),"FAIL");
        return false;
    }
      
   }

   private function request($params = array()) {
      $request = [
        'serviceAccount' => config('sms.account'),
        'messages' => $params
      ];

      //Building our NVP string
      $request = json_encode($request);

      //cURL settings
      $curlOptions = array (
         CURLOPT_URL => $this ->_endPoint,
         CURLOPT_VERBOSE => 1,
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_POST => 1,
         CURLOPT_POSTFIELDS => $request,
         CURLOPT_HTTPHEADER => $this->header
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);
      //return true;
      
      //Checking for cURL errors
      if (curl_errno($ch)) {
         $this -> _errors = curl_error($ch);
         curl_close($ch);
         return false;
         //Handle errors
      } else  {
         curl_close($ch);
         return $response;
      }
   }
}
?>