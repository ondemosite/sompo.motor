<?php

namespace App\Library;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Models\Payment;
use Config;

class InsurancePayment extends Controller
{
    public function create($order){
        if(!empty($order)){
            logTransaction($order->order_number,"ดำเนินการสั่งซื้อ","SUCCESS");
            $config = Config::get('payment.prd'); //UAT
            //Merchant's account information
            $merchant_id = $config['merchant_id'];
            $secret_key = $config['secret_key'];
            $pay_category_id = $config['pay_category_id'];
            $payment_url = $config['payment_url'];

            //Request information
            $version = $config['version'];
            $frontend_url = $config['frontend_url'];
            $backend_url = $config['backend_url'];
            $payment_option = $config['payment_option'];
            $currency = $config['currency']; //THAILAND BAHT 

            //Transaction information
            $payment_description  = "Motor Joy Order: ".$order->insurance_plan_name;
            $order_id  = $order->order_number;
            $amount  = str_pad(number_format((float)$order->payment_result,2,'.','')*100,12,"0",STR_PAD_LEFT);

            //Construct signature string
            $params = $version.$merchant_id.$payment_description.$order_id.$currency.$amount.$pay_category_id.$frontend_url.$backend_url.$payment_option;
            $hash_value = hash_hmac('sha1',$params, $secret_key,false);	//Compute hash value

            $response = [
                'version' => $version,
                'payment_url' => $payment_url,
                'merchant_id' => $merchant_id,
                'currency' => $currency,
                'pay_category_id' => $pay_category_id,
                'result_url_1' => $frontend_url,
                'result_url_2' => $backend_url,
                'payment_option' => $payment_option,
                'hash_value' => $hash_value,
                'payment_description' => $payment_description,
                'order_id' => $order_id,
                'amount' => $amount,
            ];

            return $response;
        }
        return false;
    }     
    
    public function checkHash($input){
        $config = Config::get('payment.prd'); //UAT
        $secret_key = $config['secret_key'];

        $version = !empty($input["version"])?$input["version"]:'';
        $request_timestamp = !empty($input["request_timestamp"])?$input["request_timestamp"]:'';
        $merchant_id = !empty($input["merchant_id"])?$input["merchant_id"]:'';
        $currency = !empty($input["currency"])?$input["currency"]:'';
        $order_id = !empty($input["order_id"])?$input["order_id"]:'';
        $amount = !empty($input["amount"])?$input["amount"]:'';
        $invoice_no = !empty($input["invoice_no"])?$input["invoice_no"]:'';
        $transaction_ref = !empty($input["transaction_ref"])?$input["transaction_ref"]:'';
        $approval_code = !empty($input["approval_code"])?$input["approval_code"]:'';
        $eci = !empty($input["eci"])?$input["eci"]:'';
        $transaction_datetime = !empty($input["transaction_datetime"])?$input["transaction_datetime"]:'';
        $payment_channel = !empty($input["payment_channel"])?$input["payment_channel"]:'';
        $payment_status = !empty($input["payment_status"])?$input["payment_status"]:'';
        $channel_response_code = !empty($input["channel_response_code"])?$input["channel_response_code"]:'';
        $channel_response_desc = !empty($input["channel_response_desc"])?$input["channel_response_desc"]:'';
        $masked_pan = !empty($input["masked_pan"])?$input["masked_pan"]:'';
        $stored_card_unique_id = !empty($input["stored_card_unique_id"])?$input["stored_card_unique_id"]:'';
        $backend_invoice = !empty($input["backend_invoice"])?$input["backend_invoice"]:'';
        $paid_channel = !empty($input["paid_channel"])?$input["paid_channel"]:'';
        $paid_agent = !empty($input["paid_agent"])?$input["paid_agent"]:'';
        $ippMerchantAbsorbRate = !empty($input['ippMerchantAbsorbRate'])?$input['ippMerchantAbsorbRate']:'';
        $ippInterestRate = !empty($input['ippInterestRate'])?$input['ippInterestRate']:'';
        $ippInterestType = !empty($input['ippInterestType'])?$input['ippInterestType']:'';
        $ippPeriod = !empty($input['ippPeriod'])?$input['ippPeriod']:'';
        $recurring_unique_id = null;
        $payment_scheme = !empty($input["payment_scheme"])?$input["payment_scheme"]:'';
        $process_by = !empty($input["process_by"])?$input["process_by"]:'';
        $user_defined_1 = !empty($input["user_defined_1"])?$input["user_defined_1"]:'';
        $user_defined_2 = !empty($input["user_defined_2"])?$input["user_defined_2"]:'';
        $user_defined_3 = !empty($input["user_defined_3"])?$input["user_defined_3"]:'';
        $user_defined_4 = !empty($input["user_defined_4"])?$input["user_defined_4"]:'';
        $user_defined_5 = !empty($input["user_defined_5"])?$input["user_defined_5"]:'';
        $browser_info = !empty($input["browser_info"])?$input["browser_info"]:'';
        $hash_value = !empty($input["hash_value"])?$input["hash_value"]:'';

        $checkHashStr = $version.$request_timestamp.$merchant_id.$order_id.$invoice_no.$currency.$amount.$transaction_ref.$approval_code.$eci.$transaction_datetime.$payment_channel.$payment_status.$channel_response_code.$channel_response_desc.$masked_pan.$stored_card_unique_id.$backend_invoice.$paid_channel.$paid_agent.$recurring_unique_id.$user_defined_1.$user_defined_2.$user_defined_3.$user_defined_4.$user_defined_5.$browser_info.$ippPeriod.$ippInterestType.$ippInterestRate.$ippMerchantAbsorbRate.$payment_scheme.$process_by; 
        $checkHash = hash_hmac('sha1',$checkHashStr, $secret_key,false);	//Compute hash value  
        if(strcmp(strtolower($hash_value), strtolower($checkHash))==0){
           return true;
        }
        else{
           return false;
        }
    }
}