<?php

namespace App\Library;
use App\Models\OtpLog;
use App\Models\Otp as OtpModel;
use App\Models\Configs;
use Illuminate\Support\Facades\Log;

class OTP {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */
   //uat
   protected $header = null;
   
    //protected $_endPoint = 'https://services.sompo.co.th/fw006/api/v1/SmsService/SendOtpMessage'; //PRODUCTIONS
    protected $_endPoint = 'https://partner.sompo.co.th/FW006_UAT/api/v1/SmsService/SendOtpMessage';
    
   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */

    function __construct() {
      $this->_get_header();
    }

  function _get_header(){
      $auth_id = null;
      $token = null;
      $configAuth = Configs::where('config_code','=','sms_id')
      ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
      if(!empty($configAuth)){
          $auth_id = $configAuth->config_value;
      }
      $configToken = Configs::where('config_code','=','sms_token')
      ->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('expire_date', '>=', date('Y-m-d'))->first();
      if(!empty($configToken)){
          $token = $configToken->config_value;
      }
      $this->header = array(
        'Content-Type: application/json',
        'X-AUTH-ID: ' . trim($auth_id), 
        'X-TOKEN-KEY: ' . trim($token)
      );
  }


   public function sendOTP($receiver,$locale,$sessionID){
    try{
        $message = "";
        if($locale=="th"){
            $message = "ขอบคุณที่สมัครบริการของเรา หมายเลข OTP คือ {?OTP} รหัสจะหมดอายุภายใน 1 นาที";
        }else{
            $message = "Thank you for register, Your OTP is {?OTP} Please resend back with in 1 minute.";
        }

        $data = [
          'serviceAccount' => config('otp.account'),
          "receiver" => $receiver,
          "message" => $message,
          "expiry" => config('otp.expire_duration')
        ];

        $response = $this->request($data);
        $decode = json_decode($response,true);
        $status = false;
        
        if(isset($decode['requestId'])){
          if(!empty($decode['requestId'])){
            $status = true;
            OtpModel::create([
              'session_id' => $sessionID,
              'reference_no' => $decode['referenceNo'],
              'otp_number' => $decode['otp'],
              'otp_expire' => date('Y-m-d H:i:s',strtotime($decode['expiryDate']))
            ]);
          }
        }
        
        OtpLog::create([
          'receiver' => $receiver,
          'data' => json_encode($data),
          'response' => $response,
          'status' => $status==true?"SUCCESS":"FAIL",
        ]);
        
        
        $result = [
          'status' => $status,
          'referenceNo' => !empty($decode['referenceNo'])?$decode['referenceNo']:null,
        ];

        return $result;

    }catch (\Exception $e) {
        Log::error($e->getMessage());
        return [
          'status' => false,
        ];
    }
      
   }

   public function checkOTP($params = array()){
    $result = ['status' => "",'reason' => ""];
    try{
      $record = OtpModel::where([
        'session_id' => $params['session_id'],
        'reference_no' => $params['reference_no'],
      ])->orderBy('id','desc')->first();

      if(!empty($record)){
        if($record->otp_expire>=date('Y-m-d H:i:s')){
            if($params['otp_number']==$record->otp_number){
                $result['status'] = true;
                $result['reason'] = trans('common.otp_match');
                $record->verify = 1;
                $record->save();
            }else{
                $result['status'] = false;
                $result['reason'] = trans('common.otp_notmatch');
            }
        }else{
            $result['status'] = false;
            $result['reason'] = trans('common.otp_expire');
        }
      }else{
        $result['status'] = false;
        $result['reason'] = trans('common.otp_notfound');
      }
      return $result;

    }catch (\Exception $e) {
        Log::error($e->getMessage());
        $result['status'] = false;
        $result['reason'] = "System Error";
        return $result;
    }
   }

   private function request($request) {
      //Building our NVP string
      $request = json_encode($request);

      //cURL settings
      $curlOptions = array (
         CURLOPT_URL => $this -> _endPoint,
         CURLOPT_VERBOSE => 1,
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_POST => 1,
         CURLOPT_POSTFIELDS => $request,
         CURLOPT_HTTPHEADER => $this->header
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);

      
      // return json_encode($array = [
      //   "requestId"=> "B619FDD3-58ED-4A2E-BAD7-CF9E41BB2033", 
      //   "referenceNo"=> sprintf("%06d", mt_rand(1, 999999)),
      //   "otp"=> sprintf("%06d", mt_rand(1, 999999)),
      //   "expiryDate"=> date('Y-m-d H:i:s', strtotime("+01 minutes", strtotime(date('Y-m-d H:i:s'))))
      // ]);

      
      //Checking for cURL errors
      if (curl_errno($ch)) {
         $this -> _errors = curl_error($ch);
         curl_close($ch);
         return false;
         //Handle errors
      } else  {
         curl_close($ch);
         return $response;
      }
   }
}
?>