<?php

return [
    'local' => [
        'version' => "7.5",
        'merchant_id' =>"764764000000428",
        'secret_key'=> "oYadRKqX3E9A",
        'pay_category_id'=>"TJ001",
        'payment_url'=>"https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment",
        'frontend_url' => "http://sompo.local:8080/get_api/payment/front_url",
        'backend_url' => "http://sompo.local:8080/api/payment/back_url",
        'payment_option' => "C",
        'currency' => "764"
    ],
    'pre' => [
        'version' => "7.5",
        'merchant_id' =>"764764000000428",
        'secret_key'=> "oYadRKqX3E9A",
        'pay_category_id'=>"TJ001",
        'payment_url'=>"https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment",
        'frontend_url' => "https://motor.abovecorp.co.th/get_api/payment/front_url",
        'backend_url' => "https://motor.abovecorp.co.th/api/payment/back_url",
        'payment_option' => "C",
        'currency' => "764"
    ],
    'uat' => [
        'version' => "7.5",
        'merchant_id' =>"764764000000428",
        'secret_key'=> "oYadRKqX3E9A",
        'pay_category_id'=>"TJ001",
        'payment_url'=>"https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment",
        'frontend_url' => "https://motorjoyuat.sompo.co.th/get_api/payment/front_url",
        'backend_url' => "https://motorjoyuat.sompo.co.th/api/payment/back_url",
        'payment_option' => "C",
        'currency' => "764"
    ],
    'prd' => [
        'version' => "7.5",
        'merchant_id' =>"764764000001963",
        'secret_key'=> "5CB2D7C3AC2D57D711135EF102924AE34BBE09E46309EF1E3B1B09606ADD6DA0", 
        'pay_category_id'=>"TJ001",
        'payment_url'=>"https://t.2c2p.com/RedirectV3/payment",
        'frontend_url' => "https://sompo.co.th/motorjoy/get_api/payment/front_url",
        'backend_url' => "https://sompo.co.th/motorjoy/api/payment/back_url",
        'payment_option' => "C",
        'currency' => "764"
    ]
    
];