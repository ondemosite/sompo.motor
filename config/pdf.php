<?php

return [
    'mode'                 => 'UTF-8',
    'format'               => 'A4',
    'default_font_size'    => '14',
    'default_font'         => 'sans-serif',
    'custom_font_path'     => public_path('/fonts/'), // don't forget the trailing slash!
    'direction'            => 'rtl',
    'margin_left'          => 10,
    'margin_right'         => 10,
    'margin_top'           => 10,
    'margin_bottom'        => 10,
    'margin_header'        => 0,
    'margin_footer'        => 0,
    'orientation'          => 'P',
    'title'                => 'Zany Soft - Laravel PDF',
    'author'               => '',
    'watermark'            => 'Zany Soft',
    'show_watermark'       => false,
    'watermark_font'       => 'sans-serif',
    'display_mode'         => 'fullpage',
    'watermark_text_alpha' => 0.1,
    'font_path' => public_path('/fonts/'),
	'font_data' => [
		'angsana' => [
			'R'  => 'ANGSA.ttf',    // regular font
			'B'  => 'angsab.ttf',       // optional: bold font
			'I'  => 'ANGSAI.ttf',     // optional: italic font
			'BI' => 'AngsanaNewBoldItalic.ttf' // optional: bold-italic font
			//'useOTL' => 0xFF,    // required for complicated langs like Persian, Arabic and Chinese
			//'useKashida' => 75,  // required for complicated langs like Persian, Arabic and Chinese
		]
		// ...add as many as you want.
	]
];