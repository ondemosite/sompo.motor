<?php

return [
    'fail_code' => [
        '001' => "No Response Data",
        '002' => "Incorrect Hash Value",
        '003' => "Transaction Fail",
        '004' => "Order Not Found Or Expired"
    ],
    'payment_channel_code' => [
        '001' => "Credit and debit cards",
        '002' => "Cash payment channel",
        '003' => "Direct debit",
        "004" => "Others",
        "005" => "IPP transaction"
    ],
    'payment_scheme' => [
        'AL' => "ALIPAY",
        'AM' => "AMEX",
        'AP' => "ALTERNATIVE PAYMENT",
        'DI' => "DISCOVER",
        'DN' => "DINNER",
        'JC' => "JCB",
        'KP' => "KCP",
        'LP' => "LINEPAY",
        'MA' => "MASTER CARD",
        'MP' => "MPU",
        'PA' => "PAYPAL",
        'UP' => "CHINA UNION PAY",
        'VI' => "VISA",
        'WC' => "WECHAT",
    ],
    'payment_status' => [
        '000' => "Payment Successful",
        '001' => "Payment Pending",
        '002' => "Payment Rejected",
        '003' => "Payment was canceled by user",
        '999' => "Payment Failed",
    ],
    'eci' => [
        '05' => "Card holder and issuing bank are 3D Secure. 3D Secure authentication successful.",
        '06' => "One of card holder or issuing bank not registered as a 3D Secure.",
        "07" => "Card holder and issuing bank not registered as a 3D Secure.",
        "02" => "Card holder and issuing bank are 3D Secure. 3D Secure authentication successful.",
        "01" => "One of card holder or issuing bank not registered as a 3D Secure.",
        "00" => "Card holder and issuing bank not registered as a 3D Secure."
    ],
    'channel_response_code' => [
        '00' => "Success",
        '9000' => "Payment Failed.",
        '9001' => "unrecognized version number.",
        '9002' => "authentication failed.",
        '9003' => "The http request must be POST method.",
        '9004' => "The invalid request.",
        '9005' => "missing mandatory fields or parameters.",
        '9006' => "The string length of the input parameters has exceeded more than it's specified.",
        '9007' => "merchant_id is not found.",
        '9008' => "The currency code is invalid or incorrect.",
        '9009' => "invalid amount.",
        '9010' => "invalid email format.",
        '9011' => "Invalid url.",
        '9012' => "The value of invoice_no is invalid.",
        '9018' => "The duplicate order_id request.",
        '9019' => "The current request has inconsistent parameters' value with regard to the previous request with the same order_id.",
        '9020' => "Duplicate payment request. The payment has been processed before.",
        '9021' => "Transaction reject: The payment is currently in process for this same transaction.",
        '9022' => "transaction has expired.",
        '9023' => "The credit card number can't be blank.",
        '9024' => "The credit card number is invalid.",
        '9025' => "The credit card expiry can't be blank.",
        '9026' => "The credit card expiry date is invalid. Enter a non-expired card.",
        '9027' => "The credit card expiry date is invalid. Enter a non-expired card.",
        '9028' => "The credit card verification code (cvc/cvv) can't be blank.",
        '9029' => "The CVV is invalid. It must be a number.",
        '9030' => "The credit card holder name can't be blank.",
        '9031' => "The card holder name can't be more than 50 characters.",
        '9032' => "The card holder name only accept characters -_,'.A-Za-z&",
        '9033' => "The issuing bank name can't be blank.",
        '9034' => "The issuing bank name has unaccepted characters - ~;!@#$%^&*<> {}/|:",
        '9035' => "The issuing bank name can't be more than 50 characters.",
        '9036' => "The issuing bank country can't be blank.",
        '9037' => "The selected issuing bank country is invalid.",
        '9038' => "invalid merchant configuration.",
        '9039' => "User 2 Factors (3D) authentication failed.",
        '9040' => "The request is invalid. The payment_token is invalid.",
        '9041' => "invalid transaction_id.",
        '9042' => "Invalid hash value.",
        '9043' => "Payment authorization failed.",
        '9044' => "Invalid order id.",
        '9050' => "MPI server unable to check.",
        '9051' => "MPI server host error.",
        '9052' => "The duplicate payment authorization request.",
        '9054' => "Routing Failed.",
        '9055' => "Session has been expired due to idle over time limit.",
        '9056' => "Invalid promotion code value.",
        '9057' => "Invalid payment option.",
        '9058' => "nvalid IPP interest type.",
        '9059' => "Invalid payment expiry.",
        '9060' => "QuickPay does not exists.",
        '9061' => "Stored card unique id or masked card number are invalid.",
        '9062' => "Invalid request 3DS value.",
        '9063' => "Non-3DS transaction is not allowed.",
        '9064' => "Invalid next charge date.",
        '9065' => "nvalid recurring interval.",
        '9066' => "Invalid recurring count.",
        '9067' => "Invalid recurring amount.",
        '9068' => "Invalid recurring accumulate amount.",
        '9069' => "Invalid recurring flag.",
        '9070' => "Invalid recurring accumulate flag.",
        '9071' => "Invalid recurring order prefix.",
        '9072' => "Invalid charge on date.",
        '9073' => "Invalid next recurring charge date.",
        '9074' => "Invalid Statement Descriptor Value.",
        '9079' => "Stored card unique id is invalid.",
        '9080' => "Merchant not allowed for tokenization.",
        '9081' => "Merchant not allowed for tokenization without authorization.",
        '0034' => "Fraud system reject.",
        '0035' => "Payment failed.",
        '0036' => "Payment is cancelled.",
        '0037' => "Invalid merchant configuration or merchant is not registered.",
        '0055' => "MPI reject.",
        '0062' => "Corporate Bin Block.",
        '0096' => "Bank Host not available.",
        '0099' => "reserved error code.",
    ]
    
];

