<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],
        'personal-buffer' => [
            'driver' => 'local',
            'root' => storage_path('app/private/personal/buffer'),
            'url' => env('APP_URL').'/storage',
        ],
        'personal-release' => [
            'driver' => 'local',
            'root' => storage_path('app/private/personal/release'),
            'url' => env('APP_URL').'/storage',
        ],
        'upload-chunks' => [
            'driver' => 'local',
            'root' => storage_path('app/private/chunks'),
            'url' => env('APP_URL').'/storage',
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],
        // Custom
        'pdf' => [
            'driver' => 'local',
            'root' => storage_path('app/files/pdf'),
        ],
        'sftp' => [
            'driver' => 'sftp',
            'host' => '192.168.204.11',
            'port' => 22,
            'username' => 'motor_online_uat_usr', //uat
            'password' => 'x@aQ8C_3mDxH',
            'root' => '',
            'timeout' => 10,
        ],
        // 'sftp' => [
        //     'driver' => 'sftp',
        //     'host' => '192.168.204.11',
        //     'port' => 22,
        //     'username' => 'motor_online_usr', //productions
        //     'password' => 'd9mz9Mr3',
        //     'root' => '',
        //     'timeout' => 10,
        // ]
    ],

];
