<?php

return [
    'cover_path' =>'/files/upload/cover/',
    'admin_cover'=> '/files/upload/admin_cover/',
    'logo_path'=>'/files/upload/logo/',
    'upload_path'=>'/files/upload/gallery',
    'banner_path'=>'/files/upload/banner/',
    'import_path_chunks'=> '/import/chunks/',
    'import_path_backup'=> '/import/backup/',
];